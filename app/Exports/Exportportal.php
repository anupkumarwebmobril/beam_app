<?php

namespace App\Exports;

use App\Models\ImportPostal;
use Maatwebsite\Excel\Concerns\FromCollection;

class Exportportal implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return ImportPostal::all();
    }
}
