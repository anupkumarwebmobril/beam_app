<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reminder extends Model
{
    protected $table = 'reminders';
    protected $fillable = [
        'title','image','date','time','remind_me','user_id'
    ];

    use SoftDeletes;
}
