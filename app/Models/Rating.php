<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rating extends Model
{
    protected $table = 'rating';
    protected $primaryKey = 'id';
    protected $fillable = ['user_id', 'order_item_id', 'product_id', 'order_id', 'rate', 'review'];
    
    public $timestamps = false;
    
    use SoftDeletes;

    function orderItem(){
        return $this->belongsTo(OrderItem::class, 'order_item_id', 'id');
    }
}
