<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderStatus extends Model
{
    protected $table = "order_statuses";

    protected $primaryKey = 'id';
    protected $fillable = ['order_id', 'order_status_name', 'description', 'status'];

    protected $hidden = ['created_at','updated_at'];

    function orderStatusMaster(){
        return $this->belongsTo(OrderStatusMaster::class, 'order_status_name', 'order_status_name');
    }
}
