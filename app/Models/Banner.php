<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Banner extends Model
{
    protected $table = 'banner';
    protected $primaryKey = 'id';
    protected $fillable = ['name', 'image', 'status', 'created_by', 'updated_by','order_by'];
    protected $hidden = ['created_at','updated_at'];

    use SoftDeletes;
}
