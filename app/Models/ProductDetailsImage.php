<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductDetailsImage extends Model
{
    protected $table = 'product_details_image';
}
