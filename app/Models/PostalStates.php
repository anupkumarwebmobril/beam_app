<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostalStates extends Model
{
    protected $table = 'postcode';
  
    protected $fillable = ['postcode','area_name', 'post_office', 'state_code'];
}
