<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImportPostal extends Model
{
    protected $table = 'import_postals';
  
    protected $fillable = ['postal_code','email'];
}
