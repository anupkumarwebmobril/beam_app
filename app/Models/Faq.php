<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Faq extends Model
{
    protected $table = 'faq';
    protected $primaryKey = 'id';
    protected $fillable = ['question', 'answer'];
   
    public $timestamps = false;
    
    use SoftDeletes;

   
}
