<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NeDesigns extends Model
{
    protected $table = 'new_designs';
    protected $primaryKey = 'id';
    protected $fillable = ['image', 'status', 'created_by', 'updated_by'];
    protected $hidden = ['created_at','updated_at'];

    use SoftDeletes;
}
