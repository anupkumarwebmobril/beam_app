<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;

class Product extends Model
{
    protected $table = 'product';
    protected $primaryKey = 'id';
    protected $fillable = ['type', 'occasion_id', 'category_id', 'name','product_code','min_order_qty','shipping_fee', 'price_without_shipping', 'price', 'image','image_2' ,'image_3','image_4','image_5','image_6','image_7','image_8','image_9','image_10','product_specification', 'product_description', 'status', 'created_by', 'updated_by','active_by_admin'];
    protected $hidden = ['created_at','updated_at'];

    use SoftDeletes;

    function occasion(){
        return $this->belongsTo(Occasion::class, 'occasion_id', 'id');
    }

    function category(){
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    function user(){
        return $this->belongsTo(User::class, 'created_by', 'id');
    }
}
