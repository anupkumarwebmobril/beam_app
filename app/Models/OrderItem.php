<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderItem extends Model
{
    protected $table = 'order_item';
    protected $primaryKey = 'id';
    protected $fillable = ['easy_parcel_order_no', 'product_type', 'section', 'custom_order', 'product_id', 'cart_id', 'price', 'designer_charge', 'print_charge', 'item_total_price', 'quantity', 'print_type', 'to_text', 'write_your_text','vendor_id', 'image', 'image_2', 'image_3', 'image_4', 'admin_reminder_to_vendor', 'created_by', 'updated_by', 'wishesh', 'host', 'mr_groom_person', 'mrs_bride', 'date_time_of_wishes', 'venue1', 'venue2','cancel_reason','status','refund_reason','refund_email'];
    // protected $hidden = ['created_at','updated_at'];

    use SoftDeletes;

    function product(){
        return $this->belongsTo(Product::class,'product_id', 'id');
    }
}
