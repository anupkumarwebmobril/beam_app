<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PrintTypes extends Model
{
    protected $table = 'print_type';
    protected $primaryKey = 'id';
    protected $fillable = ['name', 'status','price',];
    protected $hidden = ['created_at','updated_at'];
}
