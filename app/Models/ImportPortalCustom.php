<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImportPortalCustom extends Model
{
    protected $table = 'import_postals';
  
    protected $fillable = ['postal_code','state_name'];
}
