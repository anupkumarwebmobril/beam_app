<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Query extends Model
{
    protected $table = 'query';
    protected $primaryKey = 'id';
    protected $fillable = ['email', 'query','is_admin_seen','is_highlight'];

    public $timestamps = false;

    use SoftDeletes;


}
