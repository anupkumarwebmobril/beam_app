<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CardDetails extends Model
{
    protected $table = 'card_details';
    protected $primaryKey = 'id';
    protected $fillable = ['card_holder_name', 'card_number', 'expires', 'cvv', 'user_id'];
  
    use SoftDeletes;
}
