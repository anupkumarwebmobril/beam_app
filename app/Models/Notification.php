<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
class Notification extends Model
{
    protected $table = 'notifications';
    protected $primaryKey = 'id';
    protected $fillable = ['user_id', 'message', 'created_by'];
   
    public $timestamps = false;
    
    use SoftDeletes;

    function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
