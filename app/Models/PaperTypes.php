<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaperTypes extends Model
{
    protected $table = 'paper_type';
    protected $primaryKey = 'id';
    protected $fillable = ['name', 'status','price','order_by'];
    protected $hidden = ['created_at','updated_at'];
}
