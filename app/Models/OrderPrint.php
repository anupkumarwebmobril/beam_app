<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderPrint extends Model
{
    protected $table = 'order_print';
    protected $primaryKey = 'id';
    protected $fillable = ['mail_type', 'print_type', 'image', 'status', 'created_by', 'updated_by'];
    protected $hidden = ['created_at','updated_at'];

    use SoftDeletes;
}
