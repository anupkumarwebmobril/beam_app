<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'settings';    
    Protected $fillable = ['name','pending_order_days_reminder', 'payment_merchant_code'];
    public $timestamps = false;
}
