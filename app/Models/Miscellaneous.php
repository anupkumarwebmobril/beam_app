<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Miscellaneous extends Model
{
    Protected $fillable = ['name','price'];
}
