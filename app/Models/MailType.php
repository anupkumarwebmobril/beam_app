<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MailType extends Model
{
    protected $table = 'mail_type';
    protected $primaryKey = 'id';
    protected $fillable = ['name', 'price', 'colored_price', 'shipping_fee', 'status', 'image'];

    public $timestamps = false;
    use SoftDeletes;
}
