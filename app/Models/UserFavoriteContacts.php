<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserFavoriteContacts extends Model
{
    protected $table = 'user_favorite_contacts';
    protected $primaryKey = 'id';
    protected $fillable = ['user_id', 'contact_id'];
 
    use SoftDeletes;

    public $timestamps = false;
}
