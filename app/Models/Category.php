<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    protected $table = 'category';
    protected $primaryKey = 'id';
    protected $fillable = ['type', 'occasion_id', 'name', 'status', 'created_by', 'updated_by'];
    protected $hidden = ['created_at','updated_at'];

    use SoftDeletes;

    function occasion(){
        return $this->belongsTo(Occasion::class, 'occasion_id', 'id');
    }
}
