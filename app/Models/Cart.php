<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cart extends Model
{
    protected $table = 'cart';
    protected $primaryKey = 'id';
   // protected $fillable = ['best_wishes_for_you', 'send_to', 'msg', 'same_day_delivery', 'status', 'paper_type_id', 'additional_interests', 'total', 'created_by', 'updated_by'];
    protected $fillable = ['send_to', 'msg', 'same_day_delivery', 'status', 'add_premium_service', 'paper_type_id', 'additional_interests', 'total', 'additional_interests_msg', 'created_by', 'updated_by'];
    protected $hidden = ['created_at','updated_at'];

    use SoftDeletes;
}
