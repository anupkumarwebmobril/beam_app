<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CartItem extends Model
{
    protected $table = 'cart_item';
    protected $primaryKey = 'id';
    protected $fillable = ['product_type', 'section', 'custom_order', 'product_id', 'cart_id', 'mail_type_id','price', 'designer_charge', 'print_charge', 'quantity', 'print_type', 'to_text', 'write_your_text', 'image', 'image_2', 'image_3', 'image_4', 'cart_item_ids', 'pdf_page_count','name', 'created_by', 'updated_by', 'wishesh', 'host', 'mr_groom_person', 'mrs_bride', 'date_time_of_wishes', 'venue1', 'venue2'];
    protected $hidden = ['created_at','updated_at'];

    use SoftDeletes;
}
