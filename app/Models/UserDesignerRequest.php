<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserDesignerRequest extends Model
{
    protected $table = 'user_designer_request';
    protected $primaryKey = 'id';
    protected $fillable = ['from_user_id', 'to_user_id', 'is_request_accepted','image','image_2','image_3','image_4','attachment_original_name'];
    // protected $hidden = ['created_at','updated_at'];

    use SoftDeletes;
}
