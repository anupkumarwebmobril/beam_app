<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PostCard extends Model
{
    protected $table = 'postcard';
    protected $primaryKey = 'id';
    protected $fillable = ['category_id', 'name', 'shipping_fee', 'price_without_shipping', 'price','product_code','min_order_qty','image','image_2' ,'image_3','image_4', 'product_specification', 'product_description', 'status', 'created_by', 'updated_by'];
    protected $hidden = ['created_at','updated_at'];

    use SoftDeletes;
}
