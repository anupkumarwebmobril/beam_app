<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EnvelopeColor extends Model
{
    protected $table = 'envelope_colors';
    protected $fillable = [
        'name','price','status'
    ];
}
