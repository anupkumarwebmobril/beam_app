<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contacts extends Model
{
    protected $table = 'contacts';
    protected $primaryKey = 'id';
    protected $fillable = ['name', 'image', 'phone_number', 'house_number', 'land_mark', 'city', 'zip_code', 'state', 'full_address', 'status', 'created_by', 'updated_by'];
    protected $hidden = ['created_at','updated_at'];

    use SoftDeletes;
}
