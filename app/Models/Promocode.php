<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Promocode extends Model
{
    protected $table = 'promo_code';
    protected $primaryKey = 'id';
    protected $fillable = ['promocode', 'discount_type', 'discount_value', 'expiration_date', 'usage_limits', 'type', 'product_type', 'product_id', 'occasion_id', 'associated_products_categories', 'usage_count', 'user_restrictions', 'status', 'created_by', 'updated_by'];
    protected $hidden = ['created_at','updated_at'];

    use SoftDeletes;

    function occasion(){
        return $this->belongsTo(Occasion::class, 'occasion_id', 'id');
    }
}
