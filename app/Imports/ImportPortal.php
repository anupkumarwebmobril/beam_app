<?php

namespace App\Imports;

use App\Models\ImportPostal;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use DB;



class ImportPortal implements ToModel , WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
       
        $bin = DB::table('import_postals')->get();
    
        // Get all bin number from the $bin collection
        $bin_number = $bin->pluck('postal_code');
      
        
        
        // Checking if the bin number is already in the database
        if ($bin_number->contains($row['postal_code']) == false) 
        {
           return new ImportPostal([
               "postal_code"     => $row['postal_code'],
                "email"    => $row['email'], 
            ]);
        }
        else null; // if the bin number is already in the database, return null
    }
}
