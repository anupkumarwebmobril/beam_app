<?php

namespace App\Http\Middleware;

use Closure;

class IsVendor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // dd(123);
        if ($request->user() && $request->user()->user_type == 4 || $request->user()->user_type == 5 || $request->user()->user_type == 6) { // Check if user is admin
            return $next($request);
        }

        // dd(123);
        //return redirect()->url('/')->withErrors("You are not allowed to this section!");
        return redirect('/vendor/login');
    }
}
