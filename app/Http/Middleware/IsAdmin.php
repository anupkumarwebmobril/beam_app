<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Route;

use Closure;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user() && $request->user()->user_type == 1) { // Check if user is admin
            return $next($request);
        }

        //return redirect()->url('/')->withErrors("You are not allowed to this section!");
        return redirect('adminlogin');
    }
}
