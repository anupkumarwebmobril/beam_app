<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\DB;
use Session;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Models\Category;
use App\Helpers\Helper;
use App\Models\Promocode;
use App\Models\Type;
use App\Models\SubCategory;
use App\Models\Unit;
use App\Models\Product;
use URL;
use App\Models\Occasion;
use App\Models\PostCard;

class PromocodeStandRegiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = Promocode::query();
        $query->whereIn('product_type', [1,3]);
        $query->orderBy('id', 'DESC');
        $data = $query->get();

        foreach($data as $key => $val){
            $subcategory = [];
            if($val->type == 3){
                $subCatids = explode(',', $val->product_id);
                $subcategory = PostCard::whereIn('id', $subCatids)->get();
            }            
            
            $i = 1;
            $catNames = null;  
            foreach($subcategory as $cat){                              
                $catNames = $catNames.$cat->name;
                
                if($i != count($subcategory)){
                    $catNames = $catNames.', ';
                }
                $i++;
            
            }
            $data[$key]['sub_cat_name'] = $catNames;            
        }
        //dd($data);
      
        return view('admin.promocodestandregis.index', ['data'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $query = PostCard::query();
        $query->select('postcard.*','users.name as vendor_name');
        $query->leftJoin('users','users.id','=','postcard.created_by');
        $query->orderBy('postcard.id', 'DESC');
        $postcardData = $query->get();
        //$promocodes = Promocode::where('status','=','1')->get();
        //$category = Category::where('status','=','1')->get();
        $promocodes = [];
        $category = [];
        return view('admin.promocodestandregis.create', ['postcards' => $postcardData, 'category' => $category]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Promocode $promocode)
    {

        try {

            $url = URL::to('admin/promocodestandregis/index');

            $validation = Validator::make($request->all(), [
               // 'promocode' => 'required|unique:promo_code,promocode',
                'promocode' => 'required',
            ]);
            if($validation->fails()){
                return response()->json(array('status' => false, 'msg'=> 'Promo code is required'));
            }

            $user = auth()->user();
            $data = $request->all();
           
            $validatePromocode = Promocode::where('promocode', $data['promocode'])->first();
           
            if(!empty ($validatePromocode->id)){
                return response()->json(array('status' => false, 'msg'=> 'Promo code already exist'));
            }
            
            if($data['type'] == 3){
                $data['product_type'] = 1;   
                
                $postcardIds = null;
                $x = 1;
                if( !empty($request->postcard_id)){
                    foreach ($request->postcard_id as $ocIds){
                        $postcardIds = $postcardIds.$ocIds;
                        
                        if($x != count($request->postcard_id)){
                            $postcardIds = $postcardIds.',';
                        }
                        $x++;
                    }
                }
                $data['product_id'] = $postcardIds;           
            } else {
                $data['product_type'] = 3;
                $data['product_id'] = $data['type'];
            }
            unset($data['postcard_id']);
           // dd($data);
            $promocode->fill($data)->save();
            $url = URL::to('/admin/promocodestandregis');
            //$request->session()->flash('success', 'Product added successfully');
            return response()->json(array('status' => true, 'redirect'=>$url, 'msg' => 'Promo code added successfully'));
        } catch (\Exception $e) {
            //$request->session()->flash('error', $e->getMessage());
            return response()->json(array('status' => false, 'msg'=> $e->getMessage()));
        }
        return redirect(route('promocodestandregis.create'));
       // return redirect(route('vendorproduct.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        //
        return view('brand.show', ['brand' => $brand]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Promocode $promocode)
    {
        $query = PostCard::query();
        $query->select('postcard.*','users.name as vendor_name');
        $query->leftJoin('users','users.id','=','postcard.created_by');
        $query->orderBy('postcard.id', 'DESC');
        $postcardData = $query->get();
        
        //$occasions = Occasion::where('status','=','1')->where('type', $promocode->type)->get();
        //$subCatIds = explode(',', $promocode->occasion_id);        
        //$subcategory = Category::whereIn('occasion_id', $subCatIds)->get();
        
        return view('admin.promocodestandregis.edit', ['data' => $promocode, 'postcards' => $postcardData,]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Promocode $promocode)
    {
        //try {

           $data = $request->all();
           $user = auth()->user();
          // $data['update_by'] = $user->id;

            // $ocationIds = null;
            // $x = 1;
            // foreach ($request->occasion_id as $ocIds){
            //     $ocationIds = $ocationIds.$ocIds;
                
            //     if($x != count($request->occasion_id)){
            //         $ocationIds = $ocationIds.',';
            //     }
            //     $x++;
            // }
            // $data['occasion_id'] = $ocationIds;
            if($data['type'] == 3){
                $data['product_type'] = 1; 
                
                $postcardIds = null;
                $x = 1;
                if( !empty($request->postcard_id)){
                    foreach ($request->postcard_id as $ocIds){
                        $postcardIds = $postcardIds.$ocIds;
                        
                        if($x != count($request->postcard_id)){
                            $postcardIds = $postcardIds.',';
                        }
                        $x++;
                    }
                }
                $data['product_id'] = $postcardIds;
            } else {
                $data['product_type'] = 3;
                $data['product_id'] = $data['type'];
            }
            unset($data['postcard_id']);
           
            $promocode->fill($data);
            $promocode->save();
            $request->session()->flash('success', 'Promo code updated successfully.');
        /* } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
        } */
        return redirect(route('Admin.promocodestandregis.index'));
        //return redirect(route('vendorproduct.edit', [$product->id]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Promocode $promocode)
    {
        try {
            Promocode::where('id', $request->id)->delete();
            Category::where('promocode_id', $request->id)->delete();
            $request->session()->flash('danger', 'Promo code deleted successfully.');

        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            throw new \Exception('Error while deleteing in the database.');
        }
        return redirect(route('promocodestandregis.index'));
    }




}


