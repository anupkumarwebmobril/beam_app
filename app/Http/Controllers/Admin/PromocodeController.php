<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\DB;
use Session;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Models\Category;
use App\Helpers\Helper;
use App\Models\Promocode;
use App\Models\Type;
use App\Models\SubCategory;
use App\Models\Unit;
use App\Models\Product;
use URL;
use App\Models\Occasion;

class PromocodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = Promocode::query();
        $query->whereIn('product_type', [2]);
        $query->orderBy('id', 'DESC');
        $data = $query->get();

        foreach($data as $key => $val){
            $subCatids = explode(',', $val->associated_products_categories);
            $subcategory = Category::whereIn('id', $subCatids)->get();
            
            $i = 1;
            $catNames = null;  
            foreach($subcategory as $cat){                              
                $catNames = $catNames.$cat->name;
                
                if($i != count($subcategory)){
                    $catNames = $catNames.', ';
                }
                $i++;
            
            }
            $data[$key]['sub_cat_name'] = $catNames;            
        }
        //dd($data);
      
        return view('admin.promocode.index', ['data'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$promocodes = Promocode::where('status','=','1')->get();
        //$category = Category::where('status','=','1')->get();
        $promocodes = [];
        $category = [];
        return view('admin.promocode.create', ['occasions' => $promocodes, 'category' => $category]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Promocode $promocode)
    {

        try {
            $url = URL::to('admin/promocode/index');

            $validation = Validator::make($request->all(), [
              //  'promocode' => 'required|unique:promo_code,promocode',
                'promocode' => 'required',
            ]);

            if($validation->fails()){
                return response()->json(array('status' => false, 'msg'=> 'Promo code already exist'));
            }

            $checkDuplicate = Promocode::where('promocode', $request->promocode)->first();
           // dd($checkDuplicate);
            if(!empty($checkDuplicate->id)){
                return response()->json(array('status' => false, 'msg'=> 'Promo code already exist'));
            }
            
            $user = auth()->user();
            $data = $request->all();
            
            $ocationIds = null;
            $x = 1;
            foreach ($request->occasion_id as $ocIds){
                $ocationIds = $ocationIds.$ocIds;
                
                if($x != count($request->occasion_id)){
                    $ocationIds = $ocationIds.',';
                }
                $x++;
            }
            $data['occasion_id'] = $ocationIds;
            $data['product_type'] = 2;

            $catIds = null;
            $i = 1;
            foreach ($request->category_id as $catId){
                $catIds = $catIds.$catId;
                
                if($i != count($request->category_id)){
                    $catIds = $catIds.',';
                }
                $i++;
            }
            $data['associated_products_categories'] = $catIds;
           // dd($data);

            $promocode->fill($data)->save();
            $url = URL::to('/admin/promocode');
            //$request->session()->flash('success', 'Product added successfully');
            return response()->json(array('status' => true, 'redirect'=>$url, 'msg' => 'Promo code added successfully'));
        } catch (\Exception $e) {
            //$request->session()->flash('error', $e->getMessage());
            return response()->json(array('status' => false, 'msg'=> $e->getMessage()));
        }
        return redirect(route('promocode.create'));
       // return redirect(route('vendorproduct.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        //
        return view('brand.show', ['brand' => $brand]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Promocode $promocode)
    {
        $occasions = Occasion::where('status','=','1')->where('type', $promocode->type)->get();
        $subCatIds = explode(',', $promocode->occasion_id);        
        $subcategory = Category::whereIn('occasion_id', $subCatIds)->get();
        
        return view('admin.promocode.edit', ['data' => $promocode, 'occasions' => $occasions, 'subcategory' => $subcategory]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Promocode $promocode)
    {
        //try {

           $data = $request->all();
           $user = auth()->user();
          // $data['update_by'] = $user->id;

            $ocationIds = null;
            $x = 1;
            foreach ($request->occasion_id as $ocIds){
                $ocationIds = $ocationIds.$ocIds;
                
                if($x != count($request->occasion_id)){
                    $ocationIds = $ocationIds.',';
                }
                $x++;
            }
            $data['occasion_id'] = $ocationIds;

          $catIds = null;
          $i = 1;
          foreach ($request->category_id as $catId){
              $catIds = $catIds.$catId;
              
              if($i != count($request->category_id)){
                  $catIds = $catIds.',';
              }
              $i++;
          }
          $data['associated_products_categories'] = $catIds;
           
            $promocode->fill($data);
            $promocode->save();
            $request->session()->flash('success', 'Promo code updated successfully.');
        /* } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
        } */
        return redirect(route('Admin.promocode.index'));
        //return redirect(route('vendorproduct.edit', [$product->id]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Promocode $promocode)
    {
        try {
            Promocode::where('id', $request->id)->delete();
            Category::where('promocode_id', $request->id)->delete();
            $request->session()->flash('danger', 'Promo code deleted successfully.');

        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            throw new \Exception('Error while deleteing in the database.');
        }
        return redirect(route('promocode.index'));
    }




}


