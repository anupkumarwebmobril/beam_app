<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Exception;
use Illuminate\Support\Facades\Storage;
use Auth;

class AdminController extends Controller
{
    public function profile()
    {

        try {

            $countUsers = User::where(['user_type' => 2, 'status' => 1])->count();


            return view('admin/profile')
                ->withCountUsers($countUsers);

        } catch (Exception $e) {

            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function changePasswordView()
    {

        return view('admin/change_password');
    }

    public function changePassword(Request $request)
    {

        try {

            $rules = array(
                'old_password' => 'required|max:20|min:3',
               'password' => 'required|min:8|different:old_password',
               'confirm_password' => 'required|same:password',
            );

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);

            } else {

                $getdata = auth()->user();
                if (!Hash::check($request['old_password'], Auth::user()->password)) {

                    $message = 'Sorry! Your old password has incorrect.';
                    return redirect()->back()->withMessage($message);
                    // return Redirect::to('password_setting')
                    //     ->withMessage('Sorry! Your old password has incorrect.')
                    //     ->withInput($request->input());
                }

                $update = User::find($getdata->id);
                $update->update([
                    'password' => bcrypt($request->password),
                ]);
                $message = "Password updated successfully";
                    return redirect()->back()->withMessage($message);

                // $user = auth()->user();

                // if ($user) {

                //     $user->fill([
                //         'password' => Hash::make($request->password)
                //     ])->save();

                //     $message = "Password updated successfully";
                //     return redirect()->back()->withMessage($message);
                // }

            }

        } catch (Exception $e) {

            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function getProfile()
    {

        try{

            $user = auth()->user();

            return view('admin.update_profile')->withUser($user);

        }catch(Exception $e){

            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function updateProfile(Request $request)
    {
        try{


            $rules = array(
                'name'    => 'required',
                'profile_image' => 'sometimes|mimes:jpg,jpeg,png,gif'
            );

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);

            } else {

                $user = auth()->user();

                $user->name = $request->input('name');

                if ($request->hasFile('profile_image')) {

                    try {

                        $profile_image = $request->file('profile_image');

                        $name = $profile_image->getClientOriginalName();

                        $destinationFolder = public_path('/profile_pictures/');

                        // Delete file
                        if (file_exists($user->profile_image)) {

                            unlink($user->profile_image);
                        }

                        $extention = \File::extension($name);
                        $imageNameWithExtention = $user->id.'.'.$extention;

                        //$profile_image->move($destinationFolder, $imageNameWithExtention);
                        Storage::putFileAs('/profile_pictures/', $request->file('profile_image'), $imageNameWithExtention);
                        $user->profile_image =  $imageNameWithExtention;
                    } catch (Exception $e) {

                        return redirect()->back()->withErrors($e->getMessage());
                    }
                }

                $user->update();

                return Redirect::to('admin/profile');
            }

        }catch(Exception $e){

            return redirect()->back()->withErrors($e->getMessage());
        }
    }
}
