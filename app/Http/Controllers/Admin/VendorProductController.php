<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\DB;
use Session;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Models\Category;
use App\Helpers\Helper;
use App\Models\Occasion;
use App\Models\Type;
use App\Models\SubCategory;
use App\Models\Unit;
use App\Models\Product;
use App\Models\PostCard;
use App\Models\CartItem;

use URL;
class VendorProductController extends Controller
{
    public function index()
    {
        $query = Product::query();
        $query->with(['occasion' => function ($query) {
            $query->select('occasion.*');
        }]);
        $query->with(['category' => function ($query) {
            $query->select('category.*');
        }]);
        $query->where('status', 1);
        $query->where('type', 1);
        $query->where('created_by', auth()->user()->id);
        $query->orderBy('id', 'DESC');
        $data = $query->get();

        return view('admin.vendorproduct.index', ['data'=>$data]);
    }

    public function inacitve()
    {
        // dd(5433);
        $query = Product::query();
        $query->with(['occasion' => function ($query) {
            $query->select('occasion.*');
        }]);
        $query->with(['category' => function ($query) {
            $query->select('category.*');
        }]);
        $query->where('status', 0);
        $query->where('type', 1);
        $query->where('created_by', auth()->user()->id);
        $query->orderBy('id', 'DESC');
        $data = $query->get();

        return view('admin.vendorproduct.inactive', ['data'=>$data]);
    }

    public function gifthub()
    {
        // dd(5433);
        $query = Product::query();
        $query->with(['occasion' => function ($query) {
            $query->select('occasion.*');
        }]);
        $query->with(['category' => function ($query) {
            $query->select('category.*');
        }]);
        $query->where('status', 1);
        $query->where('type', 2);
        $query->where('created_by', auth()->user()->id);
        $query->orderBy('id', 'DESC');
        $data = $query->get();

        return view('admin.vendorproduct.gifthub.index', ['data'=>$data]);
    }
    public function gifthub_inactive()
    {
        // dd(5433);
        $query = Product::query();
        $query->with(['occasion' => function ($query) {
            $query->select('occasion.*');
        }]);
        $query->with(['category' => function ($query) {
            $query->select('category.*');
        }]);
        $query->where('status', 0);
        $query->where('type', 2);
        $query->where('created_by', auth()->user()->id);
        $query->orderBy('id', 'DESC');
        $data = $query->get();

        return view('admin.vendorproduct.gifthub.inacitve', ['data'=>$data]);
    }

         public function create()
    {
        // dd(3434);
        $occasions = Occasion::where('status','=','1')->whereIn('type',[2,3])->get();
        // dd($occasions);
        //$category = Category::where('status','=','1')->get();
        // $occasions = [];
        $category = [];
        return view('admin.vendorproduct.vendorcreate', ['occasions' => $occasions, 'category' => $category]);
    }

    public function githubcreate()
    {
        // dd(3434);
        $occasions = Occasion::where('status','=','1')->whereIn('type',[2,3])->get();
        $product = User::where(['user_type' => 4, 'status' => 1])->get();
        // dd($occasions);
        //$category = Category::where('status','=','1')->get();
        // $occasions = [];
        $category = [];
        return view('admin.vendorproduct.gifthub.create', ['occasions' => $occasions, 'category' => $category, 'product' => $product]);
    }
     public function getCategoryList(Request $request)
    {

        $id = $request->input('id');
        // dd($id);
        $data = Category::where(['status' => 1, 'occasion_id' => $request->id])->get();
// dd($data);
        echo json_encode($data);
    }

    public function occasionListBasedTypeId(Request $request)
    {
        $id = $request->input('id');
        if($id == 2){
            $data = Occasion::where(['status' => 1])
                ->whereIn('type', [2,3])
                ->get();
        } else {
            $data = Occasion::where(['status' => 1, 'type' => $request->id])->get();
        }

        echo json_encode($data);
    }
    public function gifthubedit(Product $product)
    {
         $checks = User::where(['user_type' => 4, 'status' => 1])->get();

        //$occasions = Occasion::where('status','=','1')->where('type', $product->type)->get();
        $occasions = Occasion::where('status','=','1')->whereIn('type',[2,3])->get();
        if(!empty($product->occasion_id)){
            $category = Category::where('occasion_id','=', $product->occasion_id)
                ->get();
        } else {
            $category = [];
        }


        return view('admin.vendorproduct.gifthub.edit', ['product' => $product, 'occasions' => $occasions, 'category' => $category,'checks' => $checks]);
    }



      public function store(Request $request, Product $product)
    {

        try {

            $url = URL::to('vendor/gifthub/product');

            $validation = Validator::make($request->all(), [
                'product_code' => 'required|between:8,12|alpha_num|unique:product,product_code',
                'image'  => 'image|mimes:jpeg,png,jpg',
                'image_2'=> 'image|mimes:jpeg,png,jpg',
                'image_3'=> 'image|mimes:jpeg,png,jpg',
                'image_4'=> 'image|mimes:jpeg,png,jpg',
            ]);
            if($validation->fails()){
                return response()->json(array('status' => false, 'msg'=>$validation->errors()->first()));

                // return response()->json(array('status' => false, 'msg'=> 'Image should be PNG formats only'));
            }

            $user = auth()->user();
            $data = $request->all();
            $data['created_by'] = $user->id;
            $data['status'] = 0;
            // dd($data);
            //first image

            //get vendor.

            //get category and subcategory.
            $occation = Occasion::where('id', $data['occasion_id'])->first();
            $category = Category::where('id', $data['category_id'])->first();

            if ($request->hasFile('image')) {
                $validateImage = $this->imageValidation($request['image'], $request);
                if($validateImage['error'] == false){
                    return response()->json($validateImage);
                }

                $user = Helper::addProductImageOne($request, $user);
                if(!empty($user->image)){
                    $data['image'] = $user->image;
                }
            }
            if ($request->hasFile('image_2')) {
                $validateImage = $this->imageValidation($request['image_2'], $request);
                if($validateImage['error'] == false){
                    return response()->json($validateImage);
                }

                $user = Helper::addProductImageTwo($request, $user);
                if(!empty($user->image)){
                    $data['image_2'] = $user->image;
                }
            }
             if ($request->hasFile('image_3')) {
                $validateImage = $this->imageValidation($request['image_3'], $request);
                if($validateImage['error'] == false){
                    return response()->json($validateImage);
                }

                $user = Helper::addProductImageThree($request, $user);
                if(!empty($user->image)){
                    $data['image_3'] = $user->image;
                }
            }
             if($request->hasFile('image_4')) {
                $validateImage = $this->imageValidation($request['image_4'], $request);
                if($validateImage['error'] == false){
                    return response()->json($validateImage);
                }

                $user = Helper::addProductImageFour($request, $user);
                if(!empty($user->image)){
                    $data['image_4'] = $user->image;
                }
            }
            if ($request->hasFile('image_5')) {
                $validateImage = $this->imageValidation($request['image_5'], $request);
                if($validateImage['error'] == false){
                    return response()->json($validateImage);
                }

                $imageDynamicParameter = [
                    'folder_path' => "product",
                    'input_name' => "image_5",
                ];
                $imageName = Helper::addDynamicUploadImage($request, $imageDynamicParameter);
                if (!empty($imageName)) {
                    $data['image_5'] = $imageName;
                }
            }

            if ($request->hasFile('image_6')) {
                $validateImage = $this->imageValidation($request['image_6'], $request);
                if($validateImage['error'] == false){
                    return response()->json($validateImage);
                }

                $imageDynamicParameter = [
                    'folder_path' => "product",
                    'input_name' => "image_6",
                ];
                $imageName = Helper::addDynamicUploadImage($request, $imageDynamicParameter);
                if (!empty($imageName)) {
                    $data['image_6'] = $imageName;
                }
            }

            if ($request->hasFile('image_7')) {
                $validateImage = $this->imageValidation($request['image_7'], $request);
                if($validateImage['error'] == false){
                    return response()->json($validateImage);
                }

                $imageDynamicParameter = [
                    'folder_path' => "product",
                    'input_name' => "image_7",
                ];
                $imageName = Helper::addDynamicUploadImage($request, $imageDynamicParameter);
                if (!empty($imageName)) {
                    $data['image_7'] = $imageName;
                }
            }

            if ($request->hasFile('image_8')) {
                $validateImage = $this->imageValidation($request['image_8'], $request);
                if($validateImage['error'] == false){
                    return response()->json($validateImage);
                }

                $imageDynamicParameter = [
                    'folder_path' => "product",
                    'input_name' => "image_8",
                ];
                $imageName = Helper::addDynamicUploadImage($request, $imageDynamicParameter);
                if (!empty($imageName)) {
                    $data['image_8'] = $imageName;
                }
            }

            if ($request->hasFile('image_9')) {
                $validateImage = $this->imageValidation($request['image_9'], $request);
                if($validateImage['error'] == false){
                    return response()->json($validateImage);
                }

                $imageDynamicParameter = [
                    'folder_path' => "product",
                    'input_name' => "image_9",
                ];
                $imageName = Helper::addDynamicUploadImage($request, $imageDynamicParameter);
                if (!empty($imageName)) {
                    $data['image_9'] = $imageName;
                }
            }

            if ($request->hasFile('image_10')) {
                $validateImage = $this->imageValidation($request['image_10'], $request);
                if($validateImage['error'] == false){
                    return response()->json($validateImage);
                }

                $imageDynamicParameter = [
                    'folder_path' => "product",
                    'input_name' => "image_10",
                ];
                $imageName = Helper::addDynamicUploadImage($request, $imageDynamicParameter);
                if (!empty($imageName)) {
                    $data['image_10'] = $imageName;
                }
            }
            $admin = User::where('user_type',1)->first();

            $getrowdata = auth()->user();


            $product->setIncrementing(true);

            $product->fill($data)->save();
            //dd($product);
            if ($product->id) {
                $input_arr = array(
                    'name' => $getrowdata->name,
                    'email' => $admin->email,
                    'contactNo' => $getrowdata->phone,
                    'message' => $product->name ." Product Add by Vendor",
                    'subject' => "New Product Activation Request",
                    'product_name' => $product->name,
                    'vendor_name' => ucfirst($user->name),
                    'category' => ($occation->name) ? $occation->name : null,
                    'sub_category' => ($category->name) ? $category->name : null,

                );

                $message = $product->name."plase active this product";
                $input_arr['msg'] = $message;
                Mail::send('emails.VendorAddProduct', $input_arr, function ($message) use ($input_arr) {
                    $message->to($input_arr['email'], 'admin')
                        ->subject($input_arr['subject']);
                    $message->from('support@letsbeam.it', 'Beam App');
                });
            }

           // die('ak');


            Session::flash('message','gifthub product added Successfully and Admin approval is required for the added product   ');
            // $request->session()->flash('success', 'Product added successfully');
            return response()->json(array('status' => true, 'redirect'=>$url, 'msg' => 'Product added successfully'));
        } catch (\Exception $e) {
            // dd($e->getMessage());/
            //$request->session()->flash('error', $e->getMessage());
            return response()->json(array('status' => false, 'msg'=> $e->getMessage()));
        }
        Session::flash('message','gifthub product added Successfully');
        return redirect(route('vendorproduct.index'));
       // return redirect(route('vendorproduct.index'));
    }
      public function edit(Product $product)
    {

        $occasions = Occasion::where('status','=','1')->where('type', $product->type)->get();
        if(!empty($product->occasion_id)){
            $category = Category::where('occasion_id','=', $product->occasion_id)
                ->get();
        } else {
            $category = [];
        }


        return view('admin.vendorproduct.vendoredit', ['product' => $product, 'occasions' => $occasions, 'category' => $category]);
    }
     public function update(Request $request, Product $product)
    {
        try {
            $url = URL::to('vendor/gifthub/product');

            $productcheck = Product::where('id', $request->id)->first();
            if($productcheck->product_code != $request->product_code) {
                $validation = Validator::make($request->all(), [
                    'product_code' => 'required|unique:product,product_code|between:8,12|alpha_num',

                ]);
                if($validation->fails()){
                    // return response()->json(array('status' => false, 'msg'=> 'Image should be PNG formats only'));
                    Session::flash('message',$validation->errors()->first());
                    return Redirect::back();
                }
            }

            if($request->status == 1){
                $productcheck->update(['status'=>1]);
            }else{
                $productcheck->update(['status'=>0]);
            }


           $data = $request->all();
           $user = auth()->user();
           $data['update_by'] = $user->id;
           $data['created_by'] = auth()->user()->id;
        //    $data['status'] = 1;

            /* if(!empty($request->redirect_url) && ($request->redirect_url == 'inactive')){
                $redirectUrl = "vendor/gifthub/product";
            } else {
                $redirectUrl = "vendor/gifthub/product/inactive";
            } */

            if(!empty($data->page) && ($request->data == 'inactive')){
                $redirectUrl = "vendor/gifthub/product/inactive";
            } else {
                $redirectUrl = "vendor/gifthub/product";
            }

            //first image
            if ($request->hasFile('image')) {
                $validateImage = $this->imageValidation($request['image'], $request);
                if($validateImage['error'] == false){
                    $request->session()->flash('error', $validateImage['msg']);
                    if(!empty($request->redirect_url) && ($request->redirect_url == 'inactive')){
                        return Redirect::to($redirectUrl);
                    } else {
                        return Redirect::to($redirectUrl);
                    }
                }

                $user = Helper::addProductImageOne($request, $user);
                if(!empty($user->image)){
                    $data['image'] = $user->image;
                }
            }
              if ($request->hasFile('image_2')) {
                $user = Helper::addProductImageTwo($request, $user);
                if(!empty($user->image)){
                    $data['image_2'] = $user->image;
                }
            }
             if ($request->hasFile('image_3')) {
                $user = Helper::addProductImageThree($request, $user);
                if(!empty($user->image)){
                    $data['image_3'] = $user->image;
                }
            }
             if($request->hasFile('image_4')) {
                $user = Helper::addProductImageFour($request, $user);
                if(!empty($user->image)){
                    $data['image_4'] = $user->image;
                }
            }
            if ($request->hasFile('image_5')) {
                $imageDynamicParameter = [
                    'folder_path' => "product",
                    'input_name' => "image_5",
                ];
                $imageName = Helper::addDynamicUploadImage($request, $imageDynamicParameter);
                if (!empty($imageName)) {
                    $data['image_5'] = $imageName;
                }
            }

            if ($request->hasFile('image_6')) {
                $imageDynamicParameter = [
                    'folder_path' => "product",
                    'input_name' => "image_6",
                ];
                $imageName = Helper::addDynamicUploadImage($request, $imageDynamicParameter);
                if (!empty($imageName)) {
                    $data['image_6'] = $imageName;
                }
            }

            if ($request->hasFile('image_7')) {
                $imageDynamicParameter = [
                    'folder_path' => "product",
                    'input_name' => "image_7",
                ];
                $imageName = Helper::addDynamicUploadImage($request, $imageDynamicParameter);
                if (!empty($imageName)) {
                    $data['image_7'] = $imageName;
                }
            }

            if ($request->hasFile('image_8')) {
                $imageDynamicParameter = [
                    'folder_path' => "product",
                    'input_name' => "image_8",
                ];
                $imageName = Helper::addDynamicUploadImage($request, $imageDynamicParameter);
                if (!empty($imageName)) {
                    $data['image_8'] = $imageName;
                }
            }

            if ($request->hasFile('image_9')) {
                $imageDynamicParameter = [
                    'folder_path' => "product",
                    'input_name' => "image_9",
                ];
                $imageName = Helper::addDynamicUploadImage($request, $imageDynamicParameter);
                if (!empty($imageName)) {
                    $data['image_9'] = $imageName;
                }
            }

            if ($request->hasFile('image_10')) {
                $imageDynamicParameter = [
                    'folder_path' => "product",
                    'input_name' => "image_10",
                ];
                $imageName = Helper::addDynamicUploadImage($request, $imageDynamicParameter);
                if (!empty($imageName)) {
                    $data['image_10'] = $imageName;
                }
            }

            $product->fill($data);
            $product->save();
            $request->session()->flash('success', 'Product updated successfully.');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
        }

        if(!empty($request->redirect_url) && ($request->redirect_url == 'inactive')){
            return redirect('vendor/gifthub/product');
        } else {
            return redirect('vendor/gifthub/product/inactive');
        }
        //return redirect(route('vendorproduct.edit', [$product->id]));
    }

    public function imageValidation($image, $request = null){
        $data = getimagesize($image);
        $width = $data[0];
        $height = $data[1];
        //1 as post 2 as gifthub.
        if(!empty($request->type) && $request->type == 1){
            if($width >= 1748 && $height >= 2480){
                $result['error'] = true;
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['status'] = false;
                $result['msg'] = "The Card image should be in 437:620 ratio , with minimum 1748x2480 px, Image dpi 300";
            }
        } else {
            if($width >= 800 && $height >= 800){
                $result['error'] = true;
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['status'] = false;
                $result['msg'] = "The gift card image should be 1:1 ( Square ) with minimum 800x800px, Image dpi of 100dpi";
            }
        }
        return $result;
    }

    public function deleteproduct($id)
    {
         {
    $treatment = Product::findOrfail($id);
    $treatment->delete();
    CartItem::where(['product_id' => $id, 'product_type' => '2'])->delete();
    $message = "Product Deleted Successfully!";
        return redirect()->back()->withMessage($message);
    }
    }

     public function postcardindex()
    {
        $query = PostCard::query();
        $query->where('status', 1);
        $query->orderBy('id', 'DESC');
        $data = $query->get();
        return view('admin.vendorpostcard.index', ['data'=>$data]);
    }
     public function postcardcreate()
    {
        return view('admin.vendorpostcard.create');
    }
    public function postcardstore(Request $request)
    {
        try {
            $validation = Validator::make($request->all(), [
                'product_code' => 'required|between:8,12|alpha_num|unique:product,product_code',
            ]);
            if($validation->fails()){

                return response()->json(array('status' => false, 'msg'=>$validation->errors()->first()));

            }


            $user = auth()->user();

            $postcard = new Postcard();
            $postcard->category_id = 1;
            $postcard->created_by = $user->id;
            $postcard->name = $request->name;
            $postcard->price = $request->price;
            //first image
            if ($request->hasFile('image')) {
                $user = Helper::addPostcardImageOne($request, $user);
                if(!empty($user->image)){
                    $postcard->image = $user->image;
                }
            }
              if ($request->hasFile('image_2')) {
                $user = Helper::addPostcardImageTwo($request, $user);
                if(!empty($user->image)){
                    $postcard->image_2 = $user->image;
                }
            }

            //  if ($request->hasFile('image_3')) {
            //     $user = Helper::addProductImageThree($request, $user);
            //     if(!empty($user->image)){
            //         $postcard->image_3 = $user->image;
            //     }
            // }
            //  if($request->hasFile('image_4')) {
            //     $user = Helper::addProductImageFour($request, $user);
            //     if(!empty($user->image)){
            //         $postcard->image_4 = $user->image;
            //     }
            // }
            $postcard->setIncrementing(true);
            $postcard->save();
           return Redirect::to('vendor/postcard')->withMessage("Postcard Created Successfully.");
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            // return response()->json(array('status' => false, 'msg'=> $e->getMessage()));
        }
    }
     public function editpostcard (Postcard $postcard){
    return view('admin.vendorpostcard.edit', ['postcard' => $postcard]);
    }
    public function postcardupdate(Request $request, Postcard $postcard)
    {
        try {
            $postcardcheck = Postcard::where('id',$request->id)->first();
            if($postcardcheck->product_code != $request->product_code){
                $validation = Validator::make($request->all(), [
                    'product_code' => 'required|unique:postcard,product_code|between:8,12|alpha_num',
                ]);
                if($validation->fails()){
                    $request->session()->flash('error', 222);
                    return redirect()->back();
                    // return response()->json(array('status' => false, 'msg'=>$validation->errors()->first()));

                }
            }
            $validation = Validator::make($request->all(), [
                'product_code' => 'required|between:8,12|alpha_num',
            ]);
            if($validation->fails()){
                $request->session()->flash('error', 222);
                return redirect()->back();
                // return response()->json(array('status' => false, 'msg'=>$validation->errors()->first()));

            }

           $data = $request->all();
           $user = auth()->user();
           $data['update_by'] = $user->id;

            //first image
            if ($request->hasFile('image')) {
                $user = Helper::addPostcardImageOne($request, $user);
                if(!empty($user->image)){
                    $data['image'] = $user->image;
                }
            }
              if ($request->hasFile('image_2')) {
                $user = Helper::addPostcardImageTwo($request, $user);
                if(!empty($user->image)){
                    $data['image_2'] = $user->image;
                }
            }
            //  if ($request->hasFile('image_3')) {
            //     $user = Helper::addProductImageThree($request, $user);
            //     if(!empty($user->image)){
            //         $data['image_3'] = $user->image;
            //     }
            // }
            //  if($request->hasFile('image_4')) {
            //     $user = Helper::addProductImageFour($request, $user);
            //     if(!empty($user->image)){
            //         $data['image_4'] = $user->image;
            //     }
            // }

            $postcard->fill($data);
            $postcard->save();
           return Redirect::to('vendor/postcard')->withMessage("Postcard Updated Successfully.");
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
        }
    }
    public function delete_postcard($id)
    {
        // dd(78);
        $postcard = PostCard::findorfail($id);
        CartItem::where(['product_id' => $id, 'product_type' => '1'])->delete();
        // dd($postcard);
       $postcard->delete();
       $message = "Postcard Category Deleted Successfully!";
       return redirect()->back()->withMessage($message);
    }

}
