<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Exception;
use App\User;
use Validator;
use App\Models\EnvelopeColor;
class EnvelopeController extends Controller
{
    public function index()
    {
        $envelope = EnvelopeColor::orderBy('id','desc')->get();
        return view('admin.envelope.envelope_list')->withEnvelope($envelope);

    }
      public function envelopeForm()
    {
        return view('admin.envelope.add_envelope');
    }
    public function addenvelope(Request $request)
    {
        // print_r($request->all()); die();
        try{

            $rules = array(
            'name' => 'required|regex:/^[\pL\s\-]+$/u',
            'price' => 'required',
            'status'=>'',
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
            {

            return redirect()->back()->withErrors($validator)->withInput();
            }

            $envelope = new EnvelopeColor();
            $envelope->name = $request->name;
            $envelope->price = $request->price;
            $envelope->status = $request->status;

            $envelope->save();
            // dd($excercise);
            return Redirect::to('admin/envelope-list')->withMessage("Envelope Color Created Successfully.");
            }catch(Exception $e){
            return redirect()->back()->withErrors($e->getMessage());
           }
    }
    public function editenvelope(Request $request, $id){
    try{
       $envelope = EnvelopeColor::query()->where(['id' => $id])->first();
        // dd($envelope);
       return view('admin.envelope.update_envelope',compact('envelope'));
      }catch(Exception $e){
        return redirect()->back()
        ->withErrors($e->getMessage());
      }
    }
     public function updateenvelope(Request $request){
    try{
       $rules = array(
            'name' => 'required|regex:/^[\pL\s\-]+$/u',
            'price' => 'required',
            'status'=>'',

            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
            {

            return redirect()->back()->withErrors($validator)->withInput();
            }

            $envelope = EnvelopeColor::findOrfail($request->id);
            $envelope->name = $request->name;
            $envelope->price = $request->price;
            $envelope->status = $request->status;
            $envelope->save();
        // dd($excercise);

        return Redirect::to('admin/envelope-list')->withMessage("EnvelopeColor Updated Successfully.");
        }catch(Exception $e){
        return redirect()->back()->withErrors($e->getMessage());
       }
     }
    public function deleteenvelope($id)
    {
    $excercise = EnvelopeColor::findOrfail($id);
    $excercise->delete();
    $message = "Envelope Deleted Successfully";
        return redirect()->back()->withMessage($message);
    }

}
