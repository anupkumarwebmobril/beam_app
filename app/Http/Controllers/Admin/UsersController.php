<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\User;
use App\Models\PassportToken;
use App\Models\MembershipPlan;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Models\EmailTemplate;
use App\Models\ImportPostal;

use DB;

use Illuminate\Support\Facades\Redirect;
use Exception;
// use Validator;

class UsersController extends Controller
{
    public function index(){

        try{

            $users = User::where('user_type', 2)->get();
            //dd($users);

            //dd($usersubs);
            //$nurses=User::where('user_type',3)->get();

            /* foreach ($usersubs as $key => $value)
            {
                if(!empty($value->plan_id))
                {
                    $plan = MembershipPlan::where('id', $value->plan_id)->first();
                    if(!empty($plan->plan_name)){
                        $usersubs[$key]->plan_name = $plan->plan_name;
                    }
                }
            } */
            //return $nurses;
            return view('admin.user.user_list',compact('users'));

        }catch(Exception $e){

            return redirect()->back()
                ->withErrors($e->getMessage());
        }
    }

    public function vendor_list(){

        try{
            $page = "vendor";
            $users = User::whereIn('user_type',[4])->get();

            return view('admin.user.user_list',compact('users','page'));

        }catch(Exception $e){

            return redirect()->back()
                ->withErrors($e->getMessage());
        }
    }

    public function printer_vendor_list(){

        try{
            $page = "printervendor";
            $users = User::whereIn('user_type',[5])->get();

            return view('admin.user.user_list',compact('users','page'));

        }catch(Exception $e){

            return redirect()->back()
                ->withErrors($e->getMessage());
        }
    }

    public function securePrinterList(){

        try{
            $page = "securePrinterList";
            $users = User::whereIn('user_type',[6])->get();

            return view('admin.user.user_list',compact('users','page'));

        }catch(Exception $e){

            return redirect()->back()
                ->withErrors($e->getMessage());
        }
    }

    public function addUserForm(){

        return view('admin.user.add_user');

    }

    public function getUser(Request $request, $id)
    {
        try {

            $page = !empty($request->page) ? $request->page : null;

            $user = User::query()->where('id', $id)->first();
            $nurse=User::where('user_type',4)->get();
            //dd($nurse);
            $postalState = DB::table('import_postals')->distinct()->select('state_name')->get();
        //dd($postalState);
            $postalCodes = ImportPostal::get();
            return view('admin.user.update_user',compact('user','nurse', 'page','postalState','postalCodes'));

        }catch(Exception $e){

            return redirect()->back()
                ->withErrors($e->getMessage());

        }
    }

    public function addUser(Request $request){

        if($request->isMethod('post')){

            try{

                $requestData = $request->all();

                $requestData['password'] = bcrypt($requestData['password']);

                $requestData['user_type'] = 2;

                User::create($requestData);

                return Redirect::to('admin/users-list')->withMessage("Patient Created Successfully.");

            }catch(Exception $e){

                return redirect()->back()
                    ->withErrors($e->getMessage());
            }
        }
    }

    public function updateUser(Request $request){
        $rules=array(
           'name'=>'required',
        //    'email'=>'required',
            'phone'=>'required|min:6|max:15',
            // 'house_number'=>'required',
            // 'land_mark'=>'required',
            // 'city'=>'required|alpha',
            // 'zip_code'=>'required|numeric',
            // 'state'=>'required|alpha',

            //'assign_nurse'=>'required'
        );
        // dd(34343);

        $validator=Validator::make($request->all(), $rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator);
        }

        if($request->isMethod('post')){

            try{

                $userId = $request->input('id');

                $user = User::query()->where('id', $userId)->first();
                $user->name=$request->name;
                $user->email=$request->email;
                $user->phone=$request->phone;
                //$user->password=bcrypt($request->password);
                //$user->assign_nurse=$request->assign_nurse;
                $user->status=$request->status;
                $user->house_number=$request->house_number;
                $user->land_mark=$request->land_mark;
                $user->city=$request->city;
                $user->zip_code=$request->zip_code;
                $user->state=$request->state;
                $user->update();

                if(!empty($request->status == 1)){
                    $mail =EmailTemplate::where('id',2)->first();


                    $body = $mail->message;
                    $to_name = ucfirst($user->name);
                    $to_email = $user->email;
                    $data = array('name' => "$to_name", 'body' => "$body");

                    Mail::send('emails.mail', $data, function ($message)use($to_name, $to_email) {

                        $message->to($to_email, $to_name)
                            ->subject('Admin approves');
                        $message->from(config('mail.username'), "Beam App");
                    });
                }

                if(!empty($request->page) && ($request->page == 'vendor')){
                    return Redirect::to('admin/vendor-list')->withMessage("Vendor Updated Successfully.");
                } else if(!empty($request->page) && ($request->page == 'printervendor')){
                    return Redirect::to('admin/printer-vendor-list')->withMessage("Vendor Updated Successfully.");
                } else if(!empty($request->page) && ($request->page == 'securePrinterList')){
                    return Redirect::to('admin/securePrinterList')->withMessage("Vendor Updated Successfully.");
                } else {
                    return Redirect::to('admin/users-list')->withMessage("User Updated Successfully.");
                }

            }catch(Exception $e){

                return redirect()->back()
                    ->withErrors($e->getMessage());
            }
        }
    }

    public function deleteUser(Request $request, $id){
        try{

            $user = User::where('id', $id)->first();

            if(isset($user)){

                $user->delete();

                $message = "User Deleted Successfully!";

                if(!empty($request->page) && ($request->page == 'vendor')){
                    return Redirect::to('admin/vendor-list')->withMessage("Vendor deleted Successfully.");
                } else if(!empty($request->page) && ($request->page == 'printervendor')){
                    return Redirect::to('admin/printer-vendor-list')->withMessage("Vendor deleted Successfully.");
                } else if(!empty($request->page) && ($request->page == 'securePrinterList')){
                    return Redirect::to('admin/securePrinterList')->withMessage("Vendor deleted Successfully.");
                } else {
                    return Redirect::to('admin/users-list')->withMessage("User deleted Successfully.");
                }

               // return redirect()->back()->withMessage($message);
            }

            else{

                return redirect()->back()->withErrors("User could not be deleted!");
            }

        }catch(Exception $e){

            return redirect()->back()
                ->withErrors($e->getMessage());
        }
    }
}
