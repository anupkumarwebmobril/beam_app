<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\DB;
use Session;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Models\Category;
use App\Helpers\Helper;
use App\Models\Occasion;
use App\Models\Type;
use App\Models\SubCategory;
use App\Models\Unit;
use App\Models\Product;
use App\Models\Banner;
use URL;
use App\Imports\ImportPortal;
use App\Exports\Exportportal;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\ImportPortalCustom;
class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = Banner::query();
        $query->orderBy('id', 'DESC');
        $data = $query->get();

        return view('admin.banner.index', ['data'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$occasions = Occasion::where('status','=','1')->get();
        //$category = Category::where('status','=','1')->get();
        $occasions = [];
        $category = [];
        return view('admin.banner.create', ['occasions' => $occasions, 'category' => $category]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Banner $banner)
    {

        try {
            $rules = array(
                'image' =>'mimes:png|required|max:10000',
            );

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                // return response()->json(array('status' =>200,'msg'=>'Image should be PNG formats only'));
                return response()->json(array('status' => false, 'msg'=> 'Image should be PNG formats only'));

            }
            $user = auth()->user();
            $data = $request->all();
            $data['created_by'] = $user->id;

            //first image
            if ($request->hasFile('image')) {
                $request['folder_name'] = 'banner';
                $user = Helper::commonAddUpdateImageMethod($request, $user);
                if(!empty($user->image)){
                    $data['image'] = $user->image;
                }
            }


            $banner->setIncrementing(true);

            $banner->fill($data)->save();
            $url = URL::to('/admin/banner');
            //$request->session()->flash('success', 'Product added successfully');
            return response()->json(array('status' => true, 'redirect'=>$url, 'msg' => 'Banner added successfully'));
        } catch (\Exception $e) {
            //$request->session()->flash('error', $e->getMessage());
            return response()->json(array('status' => false, 'msg'=> $e->getMessage()));
        }
        return redirect(route('banner.index'));
       // return redirect(route('vendorproduct.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        //
        return view('brand.show', ['brand' => $brand]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Banner $banner)
    {

        return view('admin.banner.edit', ['data' => $banner]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Banner $banner)
    {
        try {
           $data = $request->all();
           $user = auth()->user();
           $data['update_by'] = $user->id;

            //first image
            if ($request->hasFile('image')) {
                //check if image is png only.
                $image = $request->file('image');
                $name = $image->getClientOriginalName();
                $extention = \File::extension($name);
               
                if($extention != "png" && $extention != "jpg" && $extention != "jpeg"){
                    return redirect()->back()->withErrors('Image should be .jpeg .png and .jpg formats only.');
                }

                $request['folder_name'] = 'banner';
                $user = Helper::commonAddUpdateImageMethod($request, $user);
                if(!empty($user->image)){
                    $data['image'] = $user->image;
                }
            }

            $banner->fill($data);
            $banner->save();
            $request->session()->flash('success', 'Banner updated successfully.');
            return Redirect::to('admin/banner');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            return redirect(route('Admin.banner.index'));
        }

        //return redirect(route('vendorproduct.edit', [$product->id]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Banner $banner)
    {
        try {
            Banner::where('id', $request->id)->delete();
            $request->session()->flash('danger', 'Banner deleted successfully.');

        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            throw new \Exception('Error while deleteing in the database.');
        }
        return redirect(route('category.index'));
    }

    public function import()
    {

        return view('admin.banner.import');
    }

      public function storeImport(Request $request)
    {
        $request->validate([
            'file' => 'required|max:10000',
        ]);
        // $ram = new ImportPortal;
        // dd($ram);

        if(isset($_FILES["file"])){
                
            $filename=$_FILES["file"]["tmp_name"];    
            if($_FILES["file"]["size"] > 0)
            {
                
                $file = fopen($filename, "r");
                while (($getData = fgetcsv($file, 100000, ",")) !== FALSE)
                {
                    if(!empty($getData[0]) && !empty($getData[1]) ){
                        //get destrict id
                       
                        $data = [
                            'postal_code' => $getData[0],
                            'state_name' => $getData[1],
                        ];
                        //check if recored already exist then skip
                        $count = ImportPortalCustom::where($data)->count(); 
                        if($count < 1){
                            ImportPortalCustom::insert($data);
                        } else {
                            
                        }
                        
                    }                        
                               
                }                   
                fclose($file);

                $message = "Postal data imported Successfully!";

                return redirect()->back()->withMessage($message);
            }
        }  

        dd('ak');
        Excel::import(new ImportPortal, request()->file('file'));

         return view('admin.banner.import');
    }

     public function export()
    {
       /* return Excel::download(new Exportportal, 'postalcode.csv');*/
        $fileName = 'postal-samples.csv';
        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        $columns = array('postal_code','email');

        $callback = function() use($columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            for($i=1;$i<=1;$i++) {
                $row['postal_code']        = '123456';
                $row['email']              = 'test@gmail.com';




                fputcsv($file, array($row['postal_code'], $row['email']));
            }

            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }


}


