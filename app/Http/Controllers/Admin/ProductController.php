<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\DB;
use Session;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Models\Category;
use App\Helpers\Helper;
use App\Models\CartItem;
use App\Models\Occasion;
use App\Models\Type;
use App\Models\SubCategory;
use App\Models\Unit;
use App\Models\Product;
use App\Models\PostCard;
use App\Models\OrderItem;


use URL;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd(123);
        $query = Product::query();
        $query->with(['occasion' => function ($query) {
            $query->select('occasion.*');
        }]);
        $query->with(['category' => function ($query) {
            $query->select('category.*');
        }]);
        $query->with(['user' => function ($query) {
            $query->select('users.*');
        }]);
        $query->where('status', 1);
        $query->where('type',1);
        $query->orderBy('id', 'DESC');
        $data = $query->get();
        // dd($data);
        return view('admin.product.index', ['data'=>$data]);
    }
    public function Inacitveindex()
    {
        // dd(123);
        $query = Product::query();
        $query->with(['occasion' => function ($query) {
            $query->select('occasion.*');
        }]);
        $query->with(['category' => function ($query) {
            $query->select('category.*');
        }]);
        $query->with(['user' => function ($query) {
            $query->select('users.*');
        }]);
        $query->where('status', 0);
        $query->where('type',1);
        $query->orderBy('id', 'DESC');
        $data = $query->get();
        // dd($data);
        return view('admin.product.inacitve', ['data'=>$data]);
    }

    public function gifthub()
    {
        // dd(123);
        $query = Product::query();
        $query->with(['occasion' => function ($query) {
            $query->select('occasion.*');
        }]);
        $query->with(['category' => function ($query) {
            $query->select('category.*');
        }]);
        $query->with(['user' => function ($query) {
            $query->select('users.*');
        }]);
        $query->where('status', 1);
        $query->where('type',2);
        $query->orderBy('id', 'DESC');
        $data = $query->get();
        // dd($data);
        return view('admin.gifthub-product.index', ['data'=>$data]);
    }

    public function gifthub_inactive()
    {
        // dd(123);
        $query = Product::query();
        $query->with(['occasion' => function ($query) {
            $query->select('occasion.*');
        }]);
        $query->with(['category' => function ($query) {
            $query->select('category.*');
        }]);
        $query->with(['user' => function ($query) {
            $query->select('users.*');
        }]);
        $query->where('status', 0);
        $query->where('type',2);
        $query->orderBy('id', 'DESC');
        $data = $query->get();
        // dd($data);
        return view('admin.gifthub-product.inactive', ['data'=>$data]);
    }

    public function active_all(Request $request)
    {
        $query = Product::query();
        // $query->with(['occasion' => function ($query) {
        //     $query->select('occasion.*');
        // }]);
        // $query->with(['category' => function ($query) {
        //     $query->select('category.*');
        // }]);
        // $query->with(['user' => function ($query) {
        //     $query->select('users.*');
        // }]);
        $query->where('status', 0);
        $query->where('type',1);
        $query->orderBy('id', 'DESC');
        $data = $query->get();
        // dd($data);

        foreach ($data as $key => $value) {

            $value->update(['status'=>1,'active_by_admin'=>1]);

        }
         return redirect('admin/product/inactive');
    }

    public function active_all_gifthub(Request $request)
    {
        $query = Product::query();
        // $query->with(['occasion' => function ($query) {
        //     $query->select('occasion.*');
        // }]);
        // $query->with(['category' => function ($query) {
        //     $query->select('category.*');
        // }]);
        // $query->with(['user' => function ($query) {
        //     $query->select('users.*');
        // }]);
        $query->where('status', 0);
        $query->where('type',2);
        $query->orderBy('id', 'DESC');
        $data = $query->get();
        // dd($data);

        foreach ($data as $key => $value) {

            $value->update(['status'=>1,'active_by_admin'=>1]);

        }
         return redirect('admin/gifthub/product/inactive');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$occasions = Occasion::where('status','=','1')->get();
        //$category = Category::where('status','=','1')->get();
        $occasions = [];
        $category = [];

         $product = User::where(['user_type' => 4, 'status' => 1])->get();

         // dd($product);
        return view('admin.product.create', ['occasions' => $occasions, 'category' => $category,'product' => $product]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Product $product)
    {
       /* // dd($request->file('image'));
        $filename = $request->file('image');
      //  $filename = "http://localhost/beam_app/storage/product/7333_2023_02_10_10_10_32.jpg";
        $filename = "http://localhost/beam_app/storage/product/7348_2023_02_09_12_28_00.jpg";
        $a = fopen($filename,'r');
        $string = fread($a,20);
        fclose($a);

        $data = bin2hex(substr($string,14,4));
        $x = substr($data,0,4);
        $y = substr($data,4,4);
        dd(array(hexdec($x),hexdec($y)));
         dd('anjay');
        // dd($request->all()); */
        try {

            $validation = Validator::make($request->all(), [
                'product_code' => 'required|unique:product,product_code,abc232eff|between:8,12|alpha_num',
                'image'=> 'image|mimes:jpeg,png,jpg',
                'image_2'=> 'image|mimes:jpeg,png,jpg',
                'image_3'=> 'image|mimes:jpeg,png,jpg',
                'image_4'=> 'image|mimes:jpeg,png,jpg',
            ]);
            if($validation->fails()){
                // return response()->json(array('status' => false, 'msg'=> 'Image should be PNG formats only'));
                return response()->json(array('status' => false, 'msg'=>$validation->errors()->first()));

            }

            $user = auth()->user();
            $data = $request->all();
            if(!empty($data['vendor'])){
                $data['created_by'] = $data['vendor'];
            } else {
                $data['created_by'] = Auth::user()->id;
            }


            // if(!empty($data['product_specification'])){
            //     if (strlen($data['product_specification']) != strlen(utf8_decode($data['product_specification'])))
            //     {
            //         return response()->json(array('status' => false, 'msg' => 'Only string characters are allowed in product specification.'));
            //     }
            // }

            // if(!empty($data['product_description'])){
            //     if (strlen($data['product_description']) != strlen(utf8_decode($data['product_description'])))
            //     {
            //         return response()->json(array('status' => false, 'msg' => 'Only string characters are allowed in product description.'));
            //     }
            // }

            //first image
            if ($request->hasFile('image')) {
                $validateImage = $this->imageValidation($request['image'], $request);
                if($validateImage['error'] == false){
                    return response()->json($validateImage);
                }

                $user = Helper::addProductImageOne($request, $user);
                if(!empty($user->image)){
                    $data['image'] = $user->image;
                }
            }

              if ($request->hasFile('image_2')) {
                $validateImage = $this->imageValidation($request['image_2'], $request);
                if($validateImage['error'] == false){
                    return response()->json($validateImage);
                }

                $user = Helper::addProductImageTwo($request, $user);
                if(!empty($user->image)){
                    $data['image_2'] = $user->image;
                }
            }
             if ($request->hasFile('image_3')) {
                $validateImage = $this->imageValidation($request['image_3'], $request);
                if($validateImage['error'] == false){
                    return response()->json($validateImage);
                }

                $user = Helper::addProductImageThree($request, $user);
                if(!empty($user->image)){
                    $data['image_3'] = $user->image;
                }
            }

            if($request->hasFile('image_4')) {
                $validateImage = $this->imageValidation($request['image_4'], $request);
                if($validateImage['error'] == false){
                    return response()->json($validateImage);
                }

                $user = Helper::addProductImageFour($request, $user);
                if(!empty($user->image)){
                    $data['image_4'] = $user->image;
                }
            }

            if ($request->hasFile('image_5')) {
                $validateImage = $this->imageValidation($request['image_5'], $request);
                if($validateImage['error'] == false){
                    return response()->json($validateImage);
                }

                $imageDynamicParameter = [
                    'folder_path' => "product",
                    'input_name' => "image_5",
                ];
                $imageName = Helper::addDynamicUploadImage($request, $imageDynamicParameter);
                if (!empty($imageName)) {
                    $data['image_5'] = $imageName;
                }
            }

            if ($request->hasFile('image_6')) {
                $validateImage = $this->imageValidation($request['image_6'], $request);
                if($validateImage['error'] == false){
                    return response()->json($validateImage);
                }

                $imageDynamicParameter = [
                    'folder_path' => "product",
                    'input_name' => "image_6",
                ];
                $imageName = Helper::addDynamicUploadImage($request, $imageDynamicParameter);
                if (!empty($imageName)) {
                    $data['image_6'] = $imageName;
                }
            }

            if ($request->hasFile('image_7')) {
                $validateImage = $this->imageValidation($request['image_7'], $request);
                if($validateImage['error'] == false){
                    return response()->json($validateImage);
                }

                $imageDynamicParameter = [
                    'folder_path' => "product",
                    'input_name' => "image_7",
                ];
                $imageName = Helper::addDynamicUploadImage($request, $imageDynamicParameter);
                if (!empty($imageName)) {
                    $data['image_7'] = $imageName;
                }
            }

            if ($request->hasFile('image_8')) {
                $validateImage = $this->imageValidation($request['image_8'], $request);
                if($validateImage['error'] == false){
                    return response()->json($validateImage);
                }

                $imageDynamicParameter = [
                    'folder_path' => "product",
                    'input_name' => "image_8",
                ];
                $imageName = Helper::addDynamicUploadImage($request, $imageDynamicParameter);
                if (!empty($imageName)) {
                    $data['image_8'] = $imageName;
                }
            }

            if ($request->hasFile('image_9')) {
                $validateImage = $this->imageValidation($request['image_9'], $request);
                if($validateImage['error'] == false){
                    return response()->json($validateImage);
                }

                $imageDynamicParameter = [
                    'folder_path' => "product",
                    'input_name' => "image_9",
                ];
                $imageName = Helper::addDynamicUploadImage($request, $imageDynamicParameter);
                if (!empty($imageName)) {
                    $data['image_9'] = $imageName;
                }
            }

            if ($request->hasFile('image_10')) {
                $validateImage = $this->imageValidation($request['image_10'], $request);
                if($validateImage['error'] == false){
                    return response()->json($validateImage);
                }

                $imageDynamicParameter = [
                    'folder_path' => "product",
                    'input_name' => "image_10",
                ];
                $imageName = Helper::addDynamicUploadImage($request, $imageDynamicParameter);
                if (!empty($imageName)) {
                    $data['image_10'] = $imageName;
                }
            }


            $product->setIncrementing(true);
            $product->product_code = $request->product_code;
            $product->fill($data)->save();
            // dd($product->type);
            $user_type = Auth::user();
            if($product->type == 1){
                // dd(3434);
                $url = URL::to('/admin/product');
            }else{
                $url = URL::to('admin/gifthub/product');
            }
            // if($user->user_type == 4){
            //     $url = URL::to('/vendor/product');
            // }else{
            //     $url = URL::to('/admin/product');

            // }

            //$request->session()->flash('success', 'Product added successfully');
            return response()->json(array('status' => true, 'redirect'=>$url, 'msg' => 'Product added successfully'));
        } catch (\Exception $e) {
            // dd($e->getMessage());
            //$request->session()->flash('error', $e->getMessage());
            return response()->json(array('status' => false, 'msg'=> $e->getMessage()));
        }
        return redirect(route('vendorproduct.create'));
       // return redirect(route('vendorproduct.index'));
    }

    public function imageValidation($image, $request = null){

        $data = getimagesize($image);
        $width = $data[0];
        $height = $data[1];
        //1 as post 2 as gifthub.
        if(!empty($request->type) && $request->type == 1){
            if($width >= 1000 && $height >= 1422){
                $result['error'] = true;
            } else {
                //dd('else');
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['status'] = false;
                $result['msg'] = "The Card image should be in 500:711 ratio , with minimum 1000x1422 px, Image dpi 72";
            }
        } else {
            if($width >= 800 && $height >= 800){
                $result['error'] = true;
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['status'] = false;
                $result['msg'] = "The gift card image should be 1:1 ( Square ) with minimum 800x800px, Image dpi of 100dpi";
            }
        }
        return $result;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        //
        return view('brand.show', ['brand' => $brand]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Product $product)
    {
        $page = !empty($request->page) ? $request->page : null;

        $checks = User::where(['user_type' => 4, 'status' => 1])->get();

        $occasions = Occasion::where('status','=','1')->where('type', $product->type)->get();
        if(!empty($product->occasion_id)){
            $category = Category::where('occasion_id','=', $product->occasion_id)
                ->get();
        } else {
            $category = [];
        }

        return view('admin.product.edit', ['product' => $product, 'occasions' => $occasions, 'category' => $category,'checks' => $checks, 'page' => $page]);
    }

    public function gifthubedit(Request $request, Product $product)
    {
        $page = !empty($request->page) ? $request->page : null;

        $checks = User::where(['user_type' => 4, 'status' => 1])->get();

        $occasions = Occasion::where('status','=','1')->whereIn('type', [2,3])->get();
        if(!empty($product->occasion_id)){
            $category = Category::where('occasion_id','=', $product->occasion_id)
                ->get();
        } else {
            $category = [];
        }


        return view('admin.gifthub-product.edit', ['product' => $product, 'occasions' => $occasions, 'category' => $category,'checks' => $checks, 'page' => $page]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        try {
            $prodcutcheckagain = Product::where('id',$request->id)->first();

            if($request->status ==1 ){
                $prodcutcheckagain->update(['active_by_admin'=>1]);
            }

            if($prodcutcheckagain->product_code != $request->product_code){
                $validation = Validator::make($request->all(), [
                    'product_code' => 'required|unique:product,product_code,abc232eff|between:8,12|alpha_num',

                ]);
                if($validation->fails()){
                    Session::flash('message',$validation->errors()->first());
                    return Redirect::back();
                }
            }
           $data = $request->all();

           $user = auth()->user();
           $data['created_by'] = auth()->user()->id;


            if(!empty($request->redirect_url) && ($request->redirect_url == 'inactive')){
                $redirectUrl = "admin/product/inactive";
            } else {
                $redirectUrl = "admin/product/";
            }

            //first image
            if ($request->hasFile('image')) {
                //check if image is png only.
                $image = $request->file('image');
                $name = $image->getClientOriginalName();
                $extention = \File::extension($name);

                if($extention != "png" && $extention != "jpg" && $extention != "jpeg"){
                    return redirect()->back()->withErrors('Image should be .jpeg .png and .jpg formats only.');
                }

                $validateImage = $this->imageValidation($request['image'], $request);
                if($validateImage['error'] == false){
                    $request->session()->flash('error', $validateImage['msg']);
                    if(!empty($request->redirect_url) && ($request->redirect_url == 'inactive')){
                        return Redirect::to($redirectUrl);
                    } else {
                        return Redirect::to($redirectUrl);
                    }
                }

                $user = Helper::addProductImageOne($request, $user);
                if(!empty($user->image)){
                    $data['image'] = $user->image;
                }
            }
              if ($request->hasFile('image_2')) {
                //check if image is png only.
                $image = $request->file('image_2');
                $name = $image->getClientOriginalName();
                $extention = \File::extension($name);

                if($extention != "png" && $extention != "jpg" && $extention != "jpeg"){
                    return redirect()->back()->withErrors('Image should be .jpeg .png and .jpg formats only.');
                }

                $validateImage = $this->imageValidation($request['image_2'], $request);
                if($validateImage['error'] == false){
                    $request->session()->flash('error', $validateImage['msg']);
                    if(!empty($request->redirect_url) && ($request->redirect_url == 'inactive')){
                        return Redirect::to($redirectUrl);
                    } else {
                        return Redirect::to($redirectUrl);
                    }
                }

                $user = Helper::addProductImageTwo($request, $user);
                if(!empty($user->image)){
                    $data['image_2'] = $user->image;
                }
            }
             if ($request->hasFile('image_3')) {
                 //check if image is png only.
                $image = $request->file('image_3');
                $name = $image->getClientOriginalName();
                $extention = \File::extension($name);

                if($extention != "png" && $extention != "jpg" && $extention != "jpeg"){
                    return redirect()->back()->withErrors('Image should be .jpeg .png and .jpg formats only.');
                }

                $validateImage = $this->imageValidation($request['image_3'], $request);
                if($validateImage['error'] == false){
                    $request->session()->flash('error', $validateImage['msg']);
                    if(!empty($request->redirect_url) && ($request->redirect_url == 'inactive')){
                        return Redirect::to($redirectUrl);
                    } else {
                        return Redirect::to($redirectUrl);
                    }
                }

                $user = Helper::addProductImageThree($request, $user);
                if(!empty($user->image)){
                    $data['image_3'] = $user->image;
                }
            }
             if($request->hasFile('image_4')) {
                 //check if image is png only.
                $image = $request->file('image_4');
                $name = $image->getClientOriginalName();
                $extention = \File::extension($name);

                if($extention != "png" && $extention != "jpg" && $extention != "jpeg"){
                    return redirect()->back()->withErrors('Image should be .jpeg .png and .jpg formats only.');
                }

                $validateImage = $this->imageValidation($request['image_4'], $request);
                if($validateImage['error'] == false){
                    $request->session()->flash('error', $validateImage['msg']);
                    if(!empty($request->redirect_url) && ($request->redirect_url == 'inactive')){
                        return Redirect::to($redirectUrl);
                    } else {
                        return Redirect::to($redirectUrl);
                    }
                }

                $user = Helper::addProductImageFour($request, $user);
                if(!empty($user->image)){
                    $data['image_4'] = $user->image;
                }
            }

            if ($request->hasFile('image_5')) {
                //check if image is png only.
                $image = $request->file('image_5');
                $name = $image->getClientOriginalName();
                $extention = \File::extension($name);

                if($extention != "png" && $extention != "jpg" && $extention != "jpeg"){
                    return redirect()->back()->withErrors('Image should be .jpeg .png and .jpg formats only.');
                }

                $validateImage = $this->imageValidation($request['image_5'], $request);
                if($validateImage['error'] == false){
                    $request->session()->flash('error', $validateImage['msg']);
                    if(!empty($request->redirect_url) && ($request->redirect_url == 'inactive')){
                        return Redirect::to($redirectUrl);
                    } else {
                        return Redirect::to($redirectUrl);
                    }
                }

                $imageDynamicParameter = [
                    'folder_path' => "product",
                    'input_name' => "image_5",
                ];
                $imageName = Helper::addDynamicUploadImage($request, $imageDynamicParameter);
                if (!empty($imageName)) {
                    $data['image_5'] = $imageName;
                }
            }

            if ($request->hasFile('image_6')) {
                //check if image is png only.
                $image = $request->file('image_6');
                $name = $image->getClientOriginalName();
                $extention = \File::extension($name);

                if($extention != "png" && $extention != "jpg" && $extention != "jpeg"){
                    return redirect()->back()->withErrors('Image should be .jpeg .png and .jpg formats only.');
                }

                $validateImage = $this->imageValidation($request['image_6'], $request);
                if($validateImage['error'] == false){
                    $request->session()->flash('error', $validateImage['msg']);
                    if(!empty($request->redirect_url) && ($request->redirect_url == 'inactive')){
                        return Redirect::to($redirectUrl);
                    } else {
                        return Redirect::to($redirectUrl);
                    }
                }

                $imageDynamicParameter = [
                    'folder_path' => "product",
                    'input_name' => "image_6",
                ];
                $imageName = Helper::addDynamicUploadImage($request, $imageDynamicParameter);
                if (!empty($imageName)) {
                    $data['image_6'] = $imageName;
                }
            }

            if ($request->hasFile('image_7')) {
                //check if image is png only.
                $image = $request->file('image_7');
                $name = $image->getClientOriginalName();
                $extention = \File::extension($name);

                if($extention != "png" && $extention != "jpg" && $extention != "jpeg"){
                    return redirect()->back()->withErrors('Image should be .jpeg .png and .jpg formats only.');
                }

                $validateImage = $this->imageValidation($request['image_7'], $request);
                if($validateImage['error'] == false){
                    $request->session()->flash('error', $validateImage['msg']);
                    if(!empty($request->redirect_url) && ($request->redirect_url == 'inactive')){
                        return Redirect::to($redirectUrl);
                    } else {
                        return Redirect::to($redirectUrl);
                    }
                }

                $imageDynamicParameter = [
                    'folder_path' => "product",
                    'input_name' => "image_7",
                ];
                $imageName = Helper::addDynamicUploadImage($request, $imageDynamicParameter);
                if (!empty($imageName)) {
                    $data['image_7'] = $imageName;
                }
            }

            if ($request->hasFile('image_8')) {
                //check if image is png only.
                $image = $request->file('image_8');
                $name = $image->getClientOriginalName();
                $extention = \File::extension($name);

                if($extention != "png" && $extention != "jpg" && $extention != "jpeg"){
                    return redirect()->back()->withErrors('Image should be .jpeg .png and .jpg formats only.');
                }

                $validateImage = $this->imageValidation($request['image_8'], $request);
                if($validateImage['error'] == false){
                    $request->session()->flash('error', $validateImage['msg']);
                    if(!empty($request->redirect_url) && ($request->redirect_url == 'inactive')){
                        return Redirect::to($redirectUrl);
                    } else {
                        return Redirect::to($redirectUrl);
                    }
                }

                $imageDynamicParameter = [
                    'folder_path' => "product",
                    'input_name' => "image_8",
                ];
                $imageName = Helper::addDynamicUploadImage($request, $imageDynamicParameter);
                if (!empty($imageName)) {
                    $data['image_8'] = $imageName;
                }
            }

            if ($request->hasFile('image_9')) {
                //check if image is png only.
                $image = $request->file('image_9');
                $name = $image->getClientOriginalName();
                $extention = \File::extension($name);

                if($extention != "png" && $extention != "jpg" && $extention != "jpeg"){
                    return redirect()->back()->withErrors('Image should be .jpeg .png and .jpg formats only.');
                }

                $validateImage = $this->imageValidation($request['image_9'], $request);
                if($validateImage['error'] == false){
                    $request->session()->flash('error', $validateImage['msg']);
                    if(!empty($request->redirect_url) && ($request->redirect_url == 'inactive')){
                        return Redirect::to($redirectUrl);
                    } else {
                        return Redirect::to($redirectUrl);
                    }
                }

                $imageDynamicParameter = [
                    'folder_path' => "product",
                    'input_name' => "image_9",
                ];
                $imageName = Helper::addDynamicUploadImage($request, $imageDynamicParameter);
                if (!empty($imageName)) {
                    $data['image_9'] = $imageName;
                }
            }

            if ($request->hasFile('image_10')) {
                //check if image is png only.
                $image = $request->file('image_10');
                $name = $image->getClientOriginalName();
                $extention = \File::extension($name);

                if($extention != "png" && $extention != "jpg" && $extention != "jpeg"){
                    return redirect()->back()->withErrors('Image should be .jpeg .png and .jpg formats only.');
                }

                $validateImage = $this->imageValidation($request['image'], $request);
                if($validateImage['error'] == false){
                    $request->session()->flash('error', $validateImage['msg']);
                    if(!empty($request->redirect_url) && ($request->redirect_url == 'inactive')){
                        return Redirect::to($redirectUrl);
                    } else {
                        return Redirect::to($redirectUrl);
                    }
                }

                $imageDynamicParameter = [
                    'folder_path' => "product",
                    'input_name' => "image_10",
                ];
                $imageName = Helper::addDynamicUploadImage($request, $imageDynamicParameter);
                if (!empty($imageName)) {
                    $data['image_10'] = $imageName;
                }
            }

            $product->fill($data);
            $product->save();

            $request->session()->flash('message', 'Product updated successfully.');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
        }


        if(!empty($request->redirect_url) && ($request->redirect_url == 'inactive')){
            return Redirect::to('admin/product/inactive');
        } else {
            return redirect(route('Admin.product.index'));
        }

    }
    public function gifthub_update(Request $request, Product $product)
    {
        // dd($request->all());

        $product = Product::where('id',$request->id)->first();


        try {
            if($product->product_code != $request->product_code){
                $validation = Validator::make($request->all(), [
                    'product_code' => 'required|unique:product,product_code,abc232eff|between:8,12|alpha_num',
                    'image'=> 'image|mimes:jpeg,png,jpg',
                    'image_2'=> 'image|mimes:jpeg,png,jpg',
                    'image_3'=> 'image|mimes:jpeg,png,jpg',
                    'image_4'=> 'image|mimes:jpeg,png,jpg',
                ]);
                if($validation->fails()){
                    Session::flash('message',$validation->errors()->first());
                    return Redirect::back();
                }
            }
            $data = $request->all();

            $user = auth()->user();


            if(!empty($request->redirect_url) && ($request->redirect_url == 'inactive')){
                $redirectUrl = "admin/gifthub/product/inactive";
            } else {
                $redirectUrl = "admin/gifthub/product";
            }

            // if(!empty($data['product_specification'])){
            //     if (strlen($data['product_specification']) != strlen(utf8_decode($data['product_specification'])))
            //     {
            //         return response()->json(array('status' => false, 'msg' => 'Only string characters are allowed in product specification.'));
            //     }
            // }

            // if(!empty($data['product_description'])){
            //     if (strlen($data['product_description']) != strlen(utf8_decode($data['product_description'])))
            //     {
            //         return response()->json(array('status' => false, 'msg' => 'Only string characters are allowed in product description.'));
            //     }
            // }

             //first image
             if ($request->hasFile('image')) {
                 //check if image is png only.
                $image = $request->file('image');
                $name = $image->getClientOriginalName();
                $extention = \File::extension($name);

                if($extention != "png" && $extention != "jpg" && $extention != "jpeg"){
                    return redirect()->back()->withErrors('Image should be .jpeg .png and .jpg formats only.');
                }

                $validateImage = $this->imageValidation($request['image'], $request);
                if($validateImage['error'] == false){
                    $request->session()->flash('error', $validateImage['msg']);
                    if(!empty($request->redirect_url) && ($request->redirect_url == 'inactive')){
                        return Redirect::to($redirectUrl);
                    } else {
                        return Redirect::to($redirectUrl);
                    }
                }

                 $user = Helper::addProductImageOne($request, $user);
                 if(!empty($user->image)){
                     $product->image = $user->image;
                 }
             }

             if ($request->hasFile('image_2')) {
                 //check if image is png only.
                $image = $request->file('image_2');
                $name = $image->getClientOriginalName();
                $extention = \File::extension($name);

                if($extention != "png" && $extention != "jpg" && $extention != "jpeg"){
                    return redirect()->back()->withErrors('Image should be .jpeg .png and .jpg formats only.');
                }

                $validateImage = $this->imageValidation($request['image_2'], $request);
                if($validateImage['error'] == false){
                    $request->session()->flash('error', $validateImage['msg']);
                    if(!empty($request->redirect_url) && ($request->redirect_url == 'inactive')){
                        return Redirect::to($redirectUrl);
                    } else {
                        return Redirect::to($redirectUrl);
                    }
                }

                 $user = Helper::addProductImageTwo($request, $user);
                 if(!empty($user->image)){
                    $product->image_2 = $user->image;
                 }
             }
              if ($request->hasFile('image_3')) {
                //check if image is png only.
                $image = $request->file('image_3');
                $name = $image->getClientOriginalName();
                $extention = \File::extension($name);

                if($extention != "png" && $extention != "jpg" && $extention != "jpeg"){
                    return redirect()->back()->withErrors('Image should be .jpeg .png and .jpg formats only.');
                }

                $validateImage = $this->imageValidation($request['image_3'], $request);
                if($validateImage['error'] == false){
                    $request->session()->flash('error', $validateImage['msg']);
                    if(!empty($request->redirect_url) && ($request->redirect_url == 'inactive')){
                        return Redirect::to($redirectUrl);
                    } else {
                        return Redirect::to($redirectUrl);
                    }
                }

                 $user = Helper::addProductImageThree($request, $user);
                 if(!empty($user->image)){
                    $product->image_3 = $user->image;
                 }
             }
              if($request->hasFile('image_4')) {
                //check if image is png only.
                $image = $request->file('image_4');
                $name = $image->getClientOriginalName();
                $extention = \File::extension($name);

                if($extention != "png" && $extention != "jpg" && $extention != "jpeg"){
                    return redirect()->back()->withErrors('Image should be .jpeg .png and .jpg formats only.');
                }

                $validateImage = $this->imageValidation($request['image_4'], $request);
                if($validateImage['error'] == false){
                    $request->session()->flash('error', $validateImage['msg']);
                    if(!empty($request->redirect_url) && ($request->redirect_url == 'inactive')){
                        return Redirect::to($redirectUrl);
                    } else {
                        return Redirect::to($redirectUrl);
                    }
                }

                 $user = Helper::addProductImageFour($request, $user);
                 if(!empty($user->image)){
                    $product->image_4 = $user->image;
                 }
             }

             if ($request->hasFile('image_5')) {
                //check if image is png only.
                $image = $request->file('image_5');
                $name = $image->getClientOriginalName();
                $extention = \File::extension($name);

                if($extention != "png" && $extention != "jpg" && $extention != "jpeg"){
                    return redirect()->back()->withErrors('Image should be .jpeg .png and .jpg formats only.');
                }

                $validateImage = $this->imageValidation($request['image_5'], $request);
                if($validateImage['error'] == false){
                    $request->session()->flash('error', $validateImage['msg']);
                    if(!empty($request->redirect_url) && ($request->redirect_url == 'inactive')){
                        return Redirect::to($redirectUrl);
                    } else {
                        return Redirect::to($redirectUrl);
                    }
                }

                $imageDynamicParameter = [
                    'folder_path' => "product",
                    'input_name' => "image_5",
                ];
                $imageName = Helper::addDynamicUploadImage($request, $imageDynamicParameter);
                if (!empty($imageName)) {
                    $product->image_5 = $imageName;
                }
            }

            if ($request->hasFile('image_6')) {
                //check if image is png only.
                $image = $request->file('image_6');
                $name = $image->getClientOriginalName();
                $extention = \File::extension($name);

                if($extention != "png" && $extention != "jpg" && $extention != "jpeg"){
                    return redirect()->back()->withErrors('Image should be .jpeg .png and .jpg formats only.');
                }

                $validateImage = $this->imageValidation($request['image_6'], $request);
                if($validateImage['error'] == false){
                    $request->session()->flash('error', $validateImage['msg']);
                    if(!empty($request->redirect_url) && ($request->redirect_url == 'inactive')){
                        return Redirect::to($redirectUrl);
                    } else {
                        return Redirect::to($redirectUrl);
                    }
                }

                $imageDynamicParameter = [
                    'folder_path' => "product",
                    'input_name' => "image_6",
                ];
                $imageName = Helper::addDynamicUploadImage($request, $imageDynamicParameter);
                if (!empty($imageName)) {
                    $product->image_6 = $imageName;
                }
            }

            if ($request->hasFile('image_7')) {
                //check if image is png only.
                $image = $request->file('image_7');
                $name = $image->getClientOriginalName();
                $extention = \File::extension($name);

                if($extention != "png" && $extention != "jpg" && $extention != "jpeg"){
                    return redirect()->back()->withErrors('Image should be .jpeg .png and .jpg formats only.');
                }

                $validateImage = $this->imageValidation($request['image_7'], $request);
                if($validateImage['error'] == false){
                    $request->session()->flash('error', $validateImage['msg']);
                    if(!empty($request->redirect_url) && ($request->redirect_url == 'inactive')){
                        return Redirect::to($redirectUrl);
                    } else {
                        return Redirect::to($redirectUrl);
                    }
                }

                $imageDynamicParameter = [
                    'folder_path' => "product",
                    'input_name' => "image_7",
                ];
                $imageName = Helper::addDynamicUploadImage($request, $imageDynamicParameter);
                if (!empty($imageName)) {
                    $product->image_7 = $imageName;
                }
            }

            if ($request->hasFile('image_8')) {
                //check if image is png only.
                $image = $request->file('image_8');
                $name = $image->getClientOriginalName();
                $extention = \File::extension($name);

                if($extention != "png" && $extention != "jpg" && $extention != "jpeg"){
                    return redirect()->back()->withErrors('Image should be .jpeg .png and .jpg formats only.');
                }

                $validateImage = $this->imageValidation($request['image_8'], $request);
                if($validateImage['error'] == false){
                    $request->session()->flash('error', $validateImage['msg']);
                    if(!empty($request->redirect_url) && ($request->redirect_url == 'inactive')){
                        return Redirect::to($redirectUrl);
                    } else {
                        return Redirect::to($redirectUrl);
                    }
                }

                $imageDynamicParameter = [
                    'folder_path' => "product",
                    'input_name' => "image_8",
                ];
                $imageName = Helper::addDynamicUploadImage($request, $imageDynamicParameter);
                if (!empty($imageName)) {
                    $product->image_8 = $imageName;
                }
            }

            if ($request->hasFile('image_9')) {
                //check if image is png only.
                $image = $request->file('image_9');
                $name = $image->getClientOriginalName();
                $extention = \File::extension($name);

                if($extention != "png" && $extention != "jpg" && $extention != "jpeg"){
                    return redirect()->back()->withErrors('Image should be .jpeg .png and .jpg formats only.');
                }

                $validateImage = $this->imageValidation($request['image_9'], $request);
                if($validateImage['error'] == false){
                    $request->session()->flash('error', $validateImage['msg']);
                    if(!empty($request->redirect_url) && ($request->redirect_url == 'inactive')){
                        return Redirect::to($redirectUrl);
                    } else {
                        return Redirect::to($redirectUrl);
                    }
                }

                $imageDynamicParameter = [
                    'folder_path' => "product",
                    'input_name' => "image_9",
                ];
                $imageName = Helper::addDynamicUploadImage($request, $imageDynamicParameter);
                if (!empty($imageName)) {
                    $product->image_9 = $imageName;
                }
            }

            if ($request->hasFile('image_10')) {
                //check if image is png only.
                $image = $request->file('image_10');
                $name = $image->getClientOriginalName();
                $extention = \File::extension($name);

                if($extention != "png" && $extention != "jpg" && $extention != "jpeg"){
                    return redirect()->back()->withErrors('Image should be .jpeg .png and .jpg formats only.');
                }

                $validateImage = $this->imageValidation($request['image_10'], $request);
                if($validateImage['error'] == false){
                    $request->session()->flash('error', $validateImage['msg']);
                    if(!empty($request->redirect_url) && ($request->redirect_url == 'inactive')){
                        return Redirect::to($redirectUrl);
                    } else {
                        return Redirect::to($redirectUrl);
                    }
                }

                $imageDynamicParameter = [
                    'folder_path' => "product",
                    'input_name' => "image_10",
                ];
                $imageName = Helper::addDynamicUploadImage($request, $imageDynamicParameter);
                if (!empty($imageName)) {
                    $product->image_10 = $imageName;
                }
            }

             $product->type = 2;
             $product->shipping_fee = $data['shipping_fee'];
             $product->price_without_shipping = $data['price_without_shipping'];
             $product->price = $data['price'];

             $product->occasion_id = $data['occasion_id'];
             $product->category_id = $data['category_id'];
             $product->name = $data['name'];
             $product->created_by  = $data['created_by'];
             $product->product_specification = $data['product_specification'];
             $product->product_description = $data['product_description'];
             $product->status = $data['status'];
             $product->product_code = $data['product_code'];
            // dd($product);
             $product->update();
             $request->session()->flash('message', 'Product updated successfully.');
         } catch (\Exception $e) {
             $request->session()->flash('error', $e->getMessage());
         }

         if(!empty($request->redirect_url) && ($request->redirect_url == 'inactive')){
            return Redirect::to('admin/gifthub/product/inactive');
         } else {
            return Redirect::to('admin/gifthub/product');
         }

         //  return redirect(route('Vendor.gifthub.product.index'));
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
           Product::where('id', $request->id)->delete();
           CartItem::where(['product_id' => $request->id, 'product_type' => '2'])->delete();

            $request->session()->flash('danger', 'Product deleted successfully.');
            return Redirect::back();

        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            throw new \Exception('Error while deleteing in the database.');
        }

    }

    public function getCategoryList(Request $request)
    {
        $id = $request->input('id');
        $data = Category::where(['status' => 1, 'occasion_id' => $request->id])->get();

        echo json_encode($data);
    }

    public function multiCategoryList(Request $request)
    {
        $id = $request->input('id');
        if(empty($id)){
            $data = [];
        } else {
            $data = Category::where(['status' => 1])
            ->whereIn('occasion_id', $id)
            ->get();
        }
        

        echo json_encode($data);
    }

    public function occasionListBasedTypeId(Request $request)
    {
        $id = $request->input('id');
        if($id == 2){
            // $data = Occasion::where(['status' => 1])
            //     ->whereIn('type', [2,3])
            //     ->get();
            $data = Occasion::whereIn('type', [2,3])
                ->get();
        } else {
            //$data = Occasion::where(['status' => 1, 'type' => $request->id])->get();
            $data = Occasion::where(['type' => $request->id])->get();
        }

        echo json_encode($data);
    }


    //Postcard Product

    public function postcardindex()
    {
        $query = PostCard::query();
        $query->select('postcard.*','users.name as vendor_name');
        $query->leftJoin('users','users.id','=','postcard.created_by');
        // $query->where('postcard.status', 1);
        $query->orderBy('postcard.id', 'DESC');
        $data = $query->get();
        // dd($data);
        return view('admin.postcard.index', ['data'=>$data]);
    }
     public function postcardcreate()
    {
         $product = User::where(['user_type' => 4, 'status' => 1])->get();

        return view('admin.postcard.create')->withProduct($product);
    }
    public function postcardstore(Request $request)
    {
        try {
            $validation = Validator::make($request->all(), [
                'name' => 'required|regex:/^[\pL\s\-]+$/u',
                'price' => 'required',
                'product_code' => 'required|between:8,12|alpha_num|unique:postcard,product_code',
                'image'=> 'image|mimes:jpeg,png,jpg|max:2048',
                'image_2'=> 'image|mimes:jpeg,png,jpg|max:2048',

            ]);
            if($validation->fails()){

                Session::flash ('message', $validation->errors()->first());
                return Redirect::back()->withInput();

            }
           $user = auth()->user();

            $postcard = new Postcard();
            $postcard->category_id = 1;
            $postcard->created_by = 1;
            $postcard->name = $request->name;
            $postcard->shipping_fee = $request->shipping_fee;
            $postcard->price_without_shipping = $request->price_without_shipping;
            $postcard->product_code = $request->product_code;
            $postcard->min_order_qty = $request->min_order_qty;
            $postcard->price = $request->price;
            $postcard->status = $request->status;

            //first image
            if ($request->hasFile('image')) {
                $user = Helper::addPostcardImageOne($request, $user);
                if(!empty($user->image)){
                    $postcard->image = $user->image;
                }
            }
              if ($request->hasFile('image_2')) {
                $user = Helper::addPostcardImageTwo($request, $user);
                if(!empty($user->image)){
                    $postcard->image_2 = $user->image;
                }
            }
            //  if ($request->hasFile('image_3')) {
            //     $user = Helper::addProductImageThree($request, $user);
            //     if(!empty($user->image)){
            //         $postcard->image_3 = $user->image;
            //     }
            // }
            //  if($request->hasFile('image_4')) {
            //     $user = Helper::addProductImageFour($request, $user);
            //     if(!empty($user->image)){
            //         $postcard->image_4 = $user->image;
            //     }
            // }
            $postcard->setIncrementing(true);
            $postcard->save();
           return Redirect::to('admin/postcard')->withMessage("Postcard Created Successfully.");
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            // return response()->json(array('status' => false, 'msg'=> $e->getMessage()));
        }
    }
    public function editpostcard (Postcard $postcard){
    $checks = User::where(['user_type' => 4, 'status' => 1])->get();

    return view('admin.postcard.edit', ['postcard' => $postcard,'checks' => $checks]);
    }
    public function postcardupdate(Request $request, Postcard $postcard)
    {
        try {

            $postcardcheck = Postcard::where('id',$request->id)->first();

            if($postcardcheck->product_code != $request->product_code){
                $validation = Validator::make($request->all(), [
                    'product_code' => 'required|unique:postcard,product_code|between:8,12|alpha_num',
                ]);
                if($validation->fails()){

                    Session::flash('message',$validation->errors()->first());
                    return Redirect::back();

                }
            }

           $data = $request->all();
        //    dd($data);
           $user = auth()->user();
           $data['update_by'] = $user->id;

            //first image
            if ($request->hasFile('image')) {
                //check if image is png only.
                $image = $request->file('image');
                $name = $image->getClientOriginalName();
                $extention = \File::extension($name);

                //dd($extention);

                if($extention != "png" && $extention != "jpeg" && $extention != "jpg"){

                    return redirect()->back()->withErrors('Image should be PNG,JPEG and JPG formats only.');
                }



                $user = Helper::addPostcardImageOne($request, $user);
                if(!empty($user->image)){
                    $data['image'] = $user->image;
                }
            }
              if ($request->hasFile('image_2')) {
                //check if image is png only.
                $image = $request->file('image_2');
                $name = $image->getClientOriginalName();
                $extention = \File::extension($name);

                if($extention != "png" && $extention != "jpeg" && $extention != "jpg"){

                    return redirect()->back()->withErrors('Image should be PNG,JPEG and JPG formats only.');
                }

                $user = Helper::addPostcardImageTwo($request, $user);
                if(!empty($user->image)){
                    $data['image_2'] = $user->image;
                }
            }
            //  if ($request->hasFile('image_3')) {
            //     $user = Helper::addProductImageThree($request, $user);
            //     if(!empty($user->image)){
            //         $data['image_3'] = $user->image;
            //     }
            // }
            //  if($request->hasFile('image_4')) {
            //     $user = Helper::addProductImageFour($request, $user);
            //     if(!empty($user->image)){
            //         $data['image_4'] = $user->image;
            //     }
            // }

            $postcard->fill($data);
            $postcard->save();
           return Redirect::to('admin/postcard')->withMessage("Postcard Updated Successfully.");
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
        }
    }

    public function delete_postcard($id)
    {
        // dd(78);
        $postcard = Postcard::findorfail($id);
        CartItem::where(['product_id' => $id, 'product_type' => '1'])->delete();

        // dd($postcard);
       $postcard->delete();
       $message = "Postcard Category Deleted Successfully!";
       return redirect()->back()->withMessage($message);
    }


    public function activateUser(Request $request)
    {

        $userId = $request->userId;

        $updateStatus = Product::where('id', $userId)->update(['status' => 1,'active_by_admin'=>1]);
        Session::flash('message','Product Activated successfully');

    }

    public function deactivateUser(Request $request)
    {

        $userId = $request->userId;

        $tokens = PassportTokens::where('user_id', $userId)->delete();

        $updateStatus = Product::where('id', $userId)->update(['status' => 0]);
        Session::flash('message','Product De-Actived successfully');


    }

    public function githubcreate()
    {
        // dd(3443);

        $occasions = [];
        $category = [];

         $product = User::where(['user_type' => 4, 'status' => 1])->get();

       return view('admin.gifthub-product.create',['occasions' => $occasions, 'category' => $category,'product' => $product]);
    }

    public function recommended_for_you()
    {
        $query = Product::query();
        $query->with(['occasion' => function ($query) {
            $query->select('occasion.*');
        }]);
        $query->with(['category' => function ($query) {
            $query->select('category.*');
        }]);
        $query->with(['user' => function ($query) {
            $query->select('users.*');
        }]);
        $query->where('status', 1);
        $query->where('type',1);
        $query->orderBy('created_at', 'DESC');
        $query->limit(10);
        $data = $query->get();
        // dd($data);
        return view('admin.product.recommended', ['data'=>$data]);
    }

    public function bestseller()
    {
        $query = $query = OrderItem::query();
        $query->select('product_id');
        $query->count('product_id');
        $query->where('section', '=', 2);
        $query->groupBy('product_id');
        $query->orderBy('product_id', 'DESC');
        $query->limit(10);
        $besetSelling = $query->pluck('product_id');

        $data = null;
        if(!empty($besetSelling)){
            $query = Product::query();
            $query->with(['occasion' => function ($query) {
                $query->select('occasion.*');
            }]);
            $query->with(['category' => function ($query) {
                $query->select('category.*');
            }]);
            $query->with(['user' => function ($query) {
                $query->select('users.*');
            }]);
            $query->whereIn('id', $besetSelling);
            $data = $query->get();
        }
        // dd($data);
        return view('admin.product.bestseller', ['data'=>$data]);
    }

    public function newgiftnearyou()
    {
        $query = Product::query();
        $query->with(['occasion' => function ($query) {
            $query->select('occasion.*');
        }]);
        $query->with(['category' => function ($query) {
            $query->select('category.*');
        }]);
        $query->with(['user' => function ($query) {
            $query->select('users.*');
        }]);
        $query->where('status', 1);
        $query->where('type',2);
        $query->orderBy('created_at', 'DESC');
        $query->limit(10);
        $data = $query->get();
        // dd($data);
        return view('admin.product.newgiftnearyou', ['data'=>$data]);
    }
}


