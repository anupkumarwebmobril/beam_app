<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\DB;
use Session;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Models\Category;
use App\Helpers\Helper;
use App\Models\Occasion;
use App\Models\Type;
use App\Models\SubCategory;
use App\Models\Unit;
use App\Models\Product;
use URL;
use App\Models\Order;
use App\Models\OrderStatus;
use App\Models\OrderStatusMaster;
use App\Models\OrderItem;
use App\Models\PaperTypes;
use App\Models\AddRecipient;
use Storage;
use App\Models\PostCard;
use App\Models\Contacts;
use App\Models\Rating;
use App\Models\MailType;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];

        $orderItem = OrderItem::where('section', 2)
            ->whereNotIn('status', [2,3])
            ->orderBy('id', 'Desc')
            ->get();



        $returnArray=[];

        foreach($orderItem as $key => $item) {
            $image_array=[];
            $query = Order::query();
            $query->orderBy('id', 'DESC');
            $query->where('id', $item->order_id);
            $val = $query->first();

            $val->order_item_id = $item->id;
            // dd($val);

            $product = Product::where('id', $item->product_id)->withTrashed()->first();
            // dd($product->category_id);
            // dd($product->image);
            if(!empty($product->category_id)){
                $category = Occasion::where('id', $product->category_id)->withTrashed()->first();
            }
            
            // dd($category);

            if(!empty($product->image_1)) {
                $image_array[] = Storage::url('/product/'.$product->image);
            }

            if(!empty($product->image_2)) {
                $image_array[] = Storage::url('/product/'.$product->image_2);
            }

            if(!empty($product->image_3)) {
                $image_array[] = Storage::url('/product/'.$product->image_3);
            }
            if(!empty($product->image_4)) {
                $image_array[] = Storage::url('/product/'.$product->image_4);
            }


            $val->images = $image_array;
            // dd($item->easy_parcel_order_no);
            //get order status from easy parcel.
            if(!empty($item->easy_parcel_order_no) && $item->easy_parcel_order_no != null) {
                $domain = env('EASY_PARCEL_BASE_URL');
                // dd($domain);

                $action = "EPTrackingBulk";
                $postparam = array(
                    'authentication'    => env('EASY_PARCEL_AUTHENTICATION_KEY'),
                    'api'    => 'EP-AoBYqujRY',
                    'bulk'    => array(
                        array(
                            'awb_no'    => $item->easy_parcel_order_no,
                        ),

                    ),
                );

                $url = $domain . $action;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postparam));
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                ob_start();
                $return = curl_exec($ch);
                ob_end_clean();
                curl_close($ch);

                $json = json_decode($return);
                // dd($json);
                // dd($json->result[0]->latest_status);
                if(!empty($json->result[0]->latest_status)) {
                    $val->order_status_name = $json->result[0]->latest_status;
                } else {
                    $val->order_status_name = null;
                }

            } else {
                $orderStatus = OrderStatus::with(['orderStatusMaster'])->where('order_id', $val->id)->orderBy('id', 'DESC')->first();
                if(!empty($orderStatus->orderStatusMaster['name'])) {

                    $val->order_status_name = $orderStatus->orderStatusMaster['name'];
                } else {
                    $val->order_status_name = null;
                }
            }


            //get paper type listing
            $paperTypeIds = explode(',', $val->paper_type_id);
            $paperTypes = PaperTypes::whereIn('id', $paperTypeIds)->get();
            $val['paperTypes'] = $paperTypes;

            //get order name (Product name).
            $product = Product::where('id', $item->product_id)->first();

            //get rating and reveiw
            $rating = Rating::where('order_item_id', $item->id)->first();
            $val->rating = $rating;

            if(!empty($product->name)) {
                $val->product_name = $product->name;
            } else {
                $val->product_name = null;
            }
            if(!empty($category->name)) {
                $val->category_name = $category->name;
            } else {
                $val->category_name = null;
            }
            //get sender list.
            if(!empty($val->send_to)) {
                $recepientUsers = explode(',', $val->send_to);
                $recepient = Contacts::whereIn('id', $recepientUsers)->get();

                $val->recepients = $recepient;
            }
            // end get sender list.

            //get vendor name.
            $vendor = User::where('id', $item->vendor_id)->first();
            if(!empty($vendor->name)) {
                $val->vendor_name = $vendor->name;
            } else {
                $val->vendor_name = null;
            }

            //get sender name.
            $sender = User::where('id', $item->created_by)->withTrashed()->first();
            if(!empty($sender->name)) {
                $val->sender_name = $sender->name;
            } else {
                $val->sender_name = null;
            }

            $val->to_text = $item->to_text;
            $val->write_your_text = $item->write_your_text;
            $val->quantity = $item->quantity;
            $val->order_item = $item;
            $returnArray[] = $val;

        }
        // dd($returnArray);
        $paperTypes = paperTypes::where('status', 1)->get();

        return view('admin.order.index', ['data'=>$returnArray, 'paper_type' => $paperTypes]);
    }

    public function zip()
    {
        // dd(34);
        //get zip based orders
        $paperTypes = paperTypes::where('status', 1)->get();

        $data = [];
        $orderItemBasedZipCode = [];
        /* $orderItems = OrderItem::where('section', '!=', 2)->where('id', 10)
            ->get(); */
        $orderItems = OrderItem::where('section', '!=', 2)
            ->whereNotIn('status', [2,3])
            ->orderBy('id', 'Desc')
            ->get();

        //dd($orderItems);


        //dd($orderItems);
        // foreach($orderItems as $items){
        //     // $recepientUsers = Helper::checkVendorAndUserZipcode($items);

        //     // if(!empty($recepientUsers['recipientIds'][0])){
        //     //     $items->recipientIds = $recepientUsers;
        //     //     $orderItemBasedZipCode[] = $items;
        //     // }
        // }

        $returnArray=[];
        foreach($orderItems as $key => $item) {


            // dd($item);
            $image_array = [];

            if($item->product_type == 2) {
                if($item->custom_order != 1) {
                    $product = Product::where('id', $item->product_id)->withTrashed()->first();
                    // dd($product->image);

                    if(!empty($item->image)) {
                        $image_array[] = Storage::url('/order_print/'.$item->image);
                    } else {
                        $image_array[] = Storage::url('/product/'.$product->image);
                    }

                    if(!empty($product->image_2)) {
                        $image_array[] = Storage::url('/product/'.$product->image_2);
                    }

                    if(!empty($product->image_3)) {
                        $image_array[] = Storage::url('/product/'.$product->image_3);
                    }
                    if(!empty($product->image_4)) {
                        $image_array[] = Storage::url('/product/'.$product->image_4);
                    }
                } else {

                    if(!empty($item->image)) {
                        $image_array[] = Storage::url('/order_print/'.$item->image);
                    }

                    if(!empty($item->image_2)) {
                        $image_array[] = Storage::url('/order_print/'.$item->image_2);
                    }

                    if(!empty($item->image_3)) {
                        $image_array[] = Storage::url('/order_print/'.$item->image_3);
                    }

                    if(!empty($item->image_4)) {
                        $image_array[] = Storage::url('/order_print/'.$item->image_4);
                    }


                }
            }
            if($item->product_type == 1) {
                if($item->custom_order != 1) {
                    $post_card = PostCard::where('id', $item->product_id)->first();

                    if($item->image == null) {
                        $item->image = Storage::url('/postcard/'.$post_card->image);
                    } else {
                        $image_array[] = Storage::url('/order_print/'.$item->image);
                    }
                    if(!empty($post_card->image_2)) {
                        $image_array[] =Storage::url('/postcard/'.$post_card->image_2);
                    }
                } else {
                    if(!empty($item->image)) {
                        $image_array[] = Storage::url('/order_print/'.$item->image);
                    }

                    if(!empty($item->image_2)) {
                        $image_array[] = Storage::url('/order_print/'.$item->image_2);
                    }

                }
            }
            if($item->product_type == 3) {
                if(!empty($item->image)) {
                    $image_array[] = Storage::url('/order_print/'.$item->image);
                } else {
                    // $image_array[] = Storage::url('/product/'.$item->image);
                }
            }

            $query = Order::query();
            $query->orderBy('id', 'DESC');
            $query->where('id', $item->order_id);
            $val = $query->first();

            $val->order_item_id = $item->id;

            //get order status from easy parcel.
            if(!empty($item->easy_parcel_order_no) && $item->easy_parcel_order_no != null) {
               $domain = env('EASY_PARCEL_BASE_URL');
                // dd($domain);

                $action = "EPTrackingBulk";
                $postparam = array(
                    'authentication'    => env('EASY_PARCEL_AUTHENTICATION_KEY'),
                    'api'    => 'EP-AoBYqujRY',
                    'bulk'    => array(
                        array(
                            'awb_no'    => $item->easy_parcel_order_no,
                        ),

                    ),
                );

                $url = $domain . $action;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postparam));
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                ob_start();
                $return = curl_exec($ch);
                ob_end_clean();
                curl_close($ch);

                $json = json_decode($return);
                if(!empty($json->result[0]->latest_status)) {
                    $val->order_status_name = $json->result[0]->latest_status;
                } else {
                    $val->order_status_name = null;
                }

            } else {
                $orderStatus = OrderStatus::with(['orderStatusMaster'])->where('order_id', $val->id)->orderBy('id', 'DESC')->first();
                if(!empty($orderStatus->orderStatusMaster['name'])) {
                    $val->order_status_name = $orderStatus->orderStatusMaster['name'];
                } else {
                    $val->order_status_name = null;
                }
            }
            // dd(3434);
            //get paper type listing
            $paperTypeIds = explode(',', $val->paper_type_id);
            $paperTypes = PaperTypes::whereIn('id', $paperTypeIds)->get();
            $val['paperTypes'] = $paperTypes;

            //get order name (Product name).
            if($item->product_type == 1) {
                $product = PostCard::where('id', $item->product_id)->withTrashed()->first();
            } elseif($item->product_type == 3) {
                $product = MailType::where('id', $item->mail_type_id)->withTrashed()
                ->first();
            } else {
                $product = Product::where('id', $item->product_id)->withTrashed()->first();
            }

            if(!empty($product->name)) {
                if($item->product_type == 3) {
                    $val->product_name = $item->attachment_original_name." - ".$product->name;
                } else {
                    $val->product_name = $product->name;
                }
            } else {
                $val->product_name = null;
            }

            //get sender list.
            if(!empty($val->send_to)) {
                $recepientUsers = explode(',', $val->send_to);
                $recepient = Contacts::whereIn('id', $recepientUsers)->get();

                $val->recepients = $recepient;
            }

            if(!empty($recepientUsers['recipientIds'][0])) {
                $val->recepients = $recepientUsers['recipientIds'];
            }

            //get rating and review
            $rating = Rating::where('order_item_id', $item->id)->withTrashed()->first();
            $val->rating = $rating;

            //get sender name.
            $sender = User::where('id', $item->created_by)->withTrashed()->first();
            if(!empty($sender->name)) {
                $val->sender_name = $sender->name;
            } else {
                $val->sender_name = null;
            }

            // dd($val);
            // end get sender list.
            $item->printer_vendor_name = null;
            if(!empty($item->multi_vendor_id)) {
                $printerVendor = User::where('id', $item->multi_vendor_id)->withTrashed()->first();
                if(!empty($printerVendor->name)) {
                    $item->printer_vendor_name = $printerVendor->name;
                }
            }



            if ($item->product_type == 1) {
                $shipping_fee = \App\Models\PostCard::where('id', $item->product_id)->withTrashed()->first();
                // $productType = !empty($shipping_fee->name) ? $shipping_fee->name : null;
                $productType = "Postal Product";

            } elseif ($item->product_type == 3) {
                $shipping_fee = \App\Models\MailType::where('id', $item->product_id)->withTrashed()->first();
                $productType = !empty($shipping_fee->name) ? $shipping_fee->name : null;
            } else {
                $shipping_fee = \App\Models\Product::where('id', $item->product_id)->withTrashed()->first();
                $productType = "Greeting";

            }

            // dd($item);
            // if($item->id == 28){
            //   dd($shipping_fee);
            //  }
            $val->productType =$productType;
            $val->to_text = $item->to_text;
            $val->write_your_text = $item->write_your_text;
            $val->quantity = $item->quantity;
            $val->images = $image_array;
            $val->order_item = $item;
            $val->shipping_fee = $shipping_fee->shipping_fee ?? "0.0";

            //

            $returnArray[] = $val;

            //}
        }
        // dd($returnArray);
            //  dd($paperTypes);

        return view('admin.order.vendororder', ['data' => $returnArray, 'paper_type' => $paperTypes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$occasions = Occasion::where('status','=','1')->get();
        $occasions = [];
        $order = [];
        return view('admin.order.create', ['occasions' => $occasions, 'order' => $order]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Occasion $occasion)
    {

        try {
            $user = auth()->user();
            $data = $request->all();
            $data['created_by'] = $user->id;

            //first image
            if ($request->hasFile('image')) {
                $request['folder_name'] = 'occasion';
                $user = Helper::commonAddUpdateImageMethod($request, $user);
                if(!empty($user->image)) {
                    $data['image'] = $user->image;
                }
            }


            $occasion->setIncrementing(true);

            $occasion->fill($data)->save();
            $url = URL::to('/admin/order');
            //$request->session()->flash('success', 'Product added successfully');
            return response()->json(array('status' => true, 'redirect'=>$url, 'msg' => 'Order added successfully'));
        } catch (\Exception $e) {
            //$request->session()->flash('error', $e->getMessage());
            return response()->json(array('status' => false, 'msg'=> $e->getMessage()));
        }
        return redirect(route('order.create'));
        // return redirect(route('vendorproduct.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        // dd($order);
        //
        return view('admin.order.view', ['data' => $order]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        /* $orderStatus = OrderStatus::with(['orderStatusMaster'])->where('order_id', $order->id)->orderBy('id', 'DESC')->first();

        if(!empty($orderStatus['order_status_name'])){
            $order->order_status_name = $orderStatus['order_status_name'];
        }
        $orderStatusMaster = OrderStatusMaster::get(); */

        $page = !empty($request['page']) ? $request['page'] : null;

        $orderItem = OrderItem::where('id', $id)->first();
        return view('admin.order.edit', ['order_item' => $orderItem, 'page' => $page]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            $data = $request->all();
            $user = auth()->user();

            $insert = ['id'=>$data['id']];

            $oldStatus = OrderItem::where($insert)->first();

            if($oldStatus->id) {
                OrderItem::where('id', $data['id'])->update(['easy_parcel_order_no'=>$request->easy_parcel_order_no]);
                Session::flash('message', 'Order updated successfully');

                return Redirect::to('admin/'.$data['page']);

                /* if($oldStatus->section == 2){
                    return Redirect::to('admin/order');
                } else {
                    return Redirect::to('admin/order-printer');
                } */

            }
            //$request->session()->flash('success', 'Order Status updated successfully.');
        } catch (\Exception $e) {
            Session::flash('message', $e->getMessage());
            return Redirect::back();

        }

        //return redirect(route('vendorproduct.edit', [$product->id]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Occasion $occasion)
    {
        try {
            Occasion::where('id', $request->id)->delete();
            $request->session()->flash('danger', 'Order deleted successfully.');

        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            throw new \Exception('Error while deleteing in the database.');
        }
        return redirect(route('order.index'));
    }

    public function cancelordervendor()
    {
        $data = [];

        $orderItem = OrderItem::where('section', 2)
            ->whereIn('status', [2])
            ->get();

        $returnArray=[];

        foreach($orderItem as $key => $item) {
            $image_array=[];
            $query = Order::query();
            $query->orderBy('id', 'DESC');
            $query->where('id', $item->order_id);
            $val = $query->first();

            $val->order_item_id = $item->id;
            // dd($val);

            $product = Product::where('id', $item->product_id)->withTrashed()->first();
            // dd($product->image);

            if(!empty($product->image_1)) {
                $image_array[] = Storage::url('/product/'.$product->image);
            }

            if(!empty($product->image_2)) {
                $image_array[] = Storage::url('/product/'.$product->image_2);
            }

            if(!empty($product->image_3)) {
                $image_array[] = Storage::url('/product/'.$product->image_3);
            }
            if(!empty($product->image_4)) {
                $image_array[] = Storage::url('/product/'.$product->image_4);
            }

            $val->images = $image_array;
            $val->order_status_name = "Cancelled";



            //get paper type listing
            $paperTypeIds = explode(',', $val->paper_type_id);
            $paperTypes = PaperTypes::whereIn('id', $paperTypeIds)->get();
            $val['paperTypes'] = $paperTypes;

            //get order name (Product name).
            $product = Product::where('id', $item->product_id)->first();

            //get rating and reveiw
            $rating = Rating::where('order_item_id', $item->id)->first();
            $val->rating = $rating;

            if(!empty($product->name)) {
                $val->product_name = $product->name;
            } else {
                $val->product_name = null;
            }
            //get sender list.
            if(!empty($val->send_to)) {
                $recepientUsers = explode(',', $val->send_to);
                $recepient = Contacts::whereIn('id', $recepientUsers)->get();

                $val->recepients = $recepient;
            }
            // end get sender list.

            //get vendor name.
            $vendor = User::where('id', $item->vendor_id)->first();
            if(!empty($vendor->name)) {
                $val->vendor_name = $vendor->name;
            } else {
                $val->vendor_name = null;
            }

            //get sender name.
            $sender = User::where('id', $item->created_by)->withTrashed()->first();
            if(!empty($sender->name)) {
                $val->sender_name = $sender->name;
            } else {
                $val->sender_name = null;
            }

            $val->to_text = $item->to_text;
            $val->write_your_text = $item->write_your_text;
            $val->quantity = $item->quantity;
            $val->order_item = $item;
            $returnArray[] = $val;

        }
        // dd($returnArray);
        $paperTypes = paperTypes::where('status', 1)->get();

        return view('admin.vendororder.vendorcancelorder', ['data'=>$returnArray, 'paper_type' => $paperTypes]);
    }

    public function printercancelorder()
    {
        //get zip based orders
        $paperTypes = paperTypes::where('status', 1)->get();

        $data = [];
        $orderItemBasedZipCode = [];
        /* $orderItems = OrderItem::where('section', '!=', 2)->where('id', 10)
            ->get(); */
        $orderItems = OrderItem::where('section', '!=', 2)
            ->whereIn('status', [2])
            ->get();



        //dd($orderItems);
        // foreach($orderItems as $items){
        //     // $recepientUsers = Helper::checkVendorAndUserZipcode($items);

        //     // if(!empty($recepientUsers['recipientIds'][0])){
        //     //     $items->recipientIds = $recepientUsers;
        //     //     $orderItemBasedZipCode[] = $items;
        //     // }
        // }

        $returnArray=[];
        foreach($orderItems as $key => $item) {


            // dd($item);
            $image_array = [];

            if($item->product_type == 2) {
                if($item->custom_order != 1) {
                    $product = Product::where('id', $item->product_id)->withTrashed()->first();
                    // dd($product->image);

                    if(!empty($item->image)) {
                        $image_array[] = Storage::url('/order_print/'.$item->image);
                    } else {
                        $image_array[] = Storage::url('/product/'.$product->image);
                    }

                    if(!empty($product->image_2)) {
                        $image_array[] = Storage::url('/product/'.$product->image_2);
                    }

                    if(!empty($product->image_3)) {
                        $image_array[] = Storage::url('/product/'.$product->image_3);
                    }
                    if(!empty($product->image_4)) {
                        $image_array[] = Storage::url('/product/'.$product->image_4);
                    }
                } else {

                    if(!empty($item->image)) {
                        $image_array[] = Storage::url('/order_print/'.$item->image);
                    }

                    if(!empty($item->image_2)) {
                        $image_array[] = Storage::url('/order_print/'.$item->image_2);
                    }

                    if(!empty($item->image_3)) {
                        $image_array[] = Storage::url('/order_print/'.$item->image_3);
                    }

                    if(!empty($item->image_4)) {
                        $image_array[] = Storage::url('/order_print/'.$item->image_4);
                    }


                }
            }
            if($item->product_type == 1) {
                if($item->custom_order != 1) {
                    $post_card = PostCard::where('id', $item->product_id)->first();

                    if($item->image == null) {
                        $item->image = Storage::url('/postcard/'.$post_card->image);
                    } else {
                        $image_array[] = Storage::url('/order_print/'.$item->image);
                    }
                    if(!empty($post_card->image_2)) {
                        $image_array[] =Storage::url('/postcard/'.$post_card->image_2);
                    }
                } else {
                    if(!empty($item->image)) {
                        $image_array[] = Storage::url('/order_print/'.$item->image);
                    }

                    if(!empty($item->image_2)) {
                        $image_array[] = Storage::url('/order_print/'.$item->image_2);
                    }

                }
            }
            if($item->product_type == 3) {
                if(!empty($item->image)) {
                    $image_array[] = Storage::url('/order_print/'.$item->image);
                } else {
                    // $image_array[] = Storage::url('/product/'.$item->image);
                }
            }

            $query = Order::query();
            $query->orderBy('id', 'DESC');
            $query->where('id', $item->order_id);
            $val = $query->first();

            $val->order_item_id = $item->id;
            $val->order_status_name = "Cancelled";


            //get paper type listing
            $paperTypeIds = explode(',', $val->paper_type_id);
            $paperTypes = PaperTypes::whereIn('id', $paperTypeIds)->get();
            $val['paperTypes'] = $paperTypes;

            //get order name (Product name).
            if($item->product_type == 1) {
                $product = PostCard::where('id', $item->product_id)->first();
            } elseif($item->product_type == 3) {
                $product = MailType::where('id', $item->mail_type_id)
                ->first();
            } else {
                $product = Product::where('id', $item->product_id)->first();
            }
            $item->printer_vendor_name = null;
            if(!empty($item->multi_vendor_id)) {
                $printerVendor = User::where('id', $item->multi_vendor_id)->withTrashed()->first();
                if(!empty($printerVendor->name)) {
                    $item->printer_vendor_name = $printerVendor->name;
                }
            }

            if(!empty($product->name)) {
                if($item->product_type == 3) {
                    $val->product_name = $item->attachment_original_name." - ".$product->name;
                } else {
                    $val->product_name = $product->name;
                }
            } else {
                $val->product_name = null;
            }

            //get sender list.
            if(!empty($val->send_to)) {
                $recepientUsers = explode(',', $val->send_to);
                $recepient = Contacts::whereIn('id', $recepientUsers)->get();

                $val->recepients = $recepient;
            }

            if(!empty($recepientUsers['recipientIds'][0])) {
                $val->recepients = $recepientUsers['recipientIds'];
            }

            //get rating and review
            $rating = Rating::where('order_item_id', $item->id)->withTrashed()->first();
            $val->rating = $rating;

            //get sender name.
            $sender = User::where('id', $item->created_by)->withTrashed()->first();
            if(!empty($sender->name)) {
                $val->sender_name = $sender->name;
            } else {
                $val->sender_name = null;
            }

            // dd($val);
            // end get sender list.
            $val->to_text = $item->to_text;
            $val->write_your_text = $item->write_your_text;
            $val->quantity = $item->quantity;
            $val->images = $image_array;
            $val->order_item = $item;
            $returnArray[] = $val;

            //}
        }


    //   dd($returnArray);

        return view('admin.vendororder.vendorcancelorder', ['data' => $returnArray, 'paper_type' => $paperTypes]);
    }


    public function giftCompleted()
    {
        $data = [];

        //Successfully delivered
        $includingTotalORderDelivered = array(
            'Successfully Delivered',
        );

        $orderItem = OrderItem::where('section', 2)
            ->whereNotNull('easy_parcel_order_no')
            ->whereNotIn('status', [2,3])
            // ->where('id',70)
            ->limit(500)
            ->orderBy('id', 'DESC')
            ->get();

            // dd($orderItem);

        $returnArray=[];
        $statusArry=[];

        foreach($orderItem as $key => $item) {

            $domain = "https://connect.easyparcel.my/?ac=";
            // dd($domain);

            $action = "EPTrackingBulk";
            $postparam = array(
                'authentication'    => "71dc1965276e33718db087d9d35227c3",
                'api'    => 'EP-AoBYqujRY',
                'bulk'    => array(
                    array(
                        'awb_no'    => $item->easy_parcel_order_no,
                    ),

                ),
            );

            $url = $domain . $action;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postparam));
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            ob_start();
            $return = curl_exec($ch);
            ob_end_clean();
            curl_close($ch);

            $json = json_decode($return);
            //  dd($json);
            if(!empty($json->result[0]->status_list)) {
                // dd($json->result[0]->status_list);
                foreach($json->result[0]->status_list as $key => $v) {
                    if(!empty($v->status)) {
                        $statusArry['status'][] = $v;
                    }
                }
                //  $returnArray['status'][] = $json->result[0]->status_list;
            } else {
                $statusArry['status'][] = null;
            }



           // $easyParcelStatus = Helper::getEasyParcelOrderStatus($item);
            //dd($returnArray['status'][0]->status);
            //if(!empty($easyParcelStatus->result[0]->parcel[0]->ship_status)) {
              //  if(in_array($easyParcelStatus->result[0]->parcel[0]->ship_status, $includingTotalORderDelivered)) {
            if(!empty($statusArry['status'][0]->status)){
                if($statusArry['status'][0]->status == "Delivered"){
                    $image_array=[];
                    $query = Order::query();
                    $query->orderBy('id', 'DESC');
                    $query->where('id', $item->order_id);
                    $val = $query->first();

                    // echo "<pre>";
                    // print_r($val);
                    // echo "</pre>";

                    $val->order_item_id = $item->id;

                    $product = Product::where('id', $item->product_id)->withTrashed()->first();

                    $image_array[] = Storage::url('/product/'.$product->image);

                    if(!empty($product->image_2)) {
                        $image_array[] = Storage::url('/product/'.$product->image_2);
                    }

                    if(!empty($product->image_3)) {
                        $image_array[] = Storage::url('/product/'.$product->image_3);
                    }
                    if(!empty($product->image_4)) {
                        $image_array[] = Storage::url('/product/'.$product->image_4);
                    }

                    $val->images = $image_array;

                    //get order status from easy parcel.
                   // $val->order_status_name = $easyParcelStatus->result[0]->parcel[0]->ship_status;
                    $val->order_status_name = $statusArry['status'][0]->status;


                    //get paper type listing
                    $paperTypeIds = explode(',', $val->paper_type_id);
                    $paperTypes = PaperTypes::whereIn('id', $paperTypeIds)->get();
                    $val['paperTypes'] = $paperTypes;

                    //get order name (Product name).
                    $product = Product::where('id', $item->product_id)->first();

                    //get rating and reveiw
                    $rating = Rating::where('order_item_id', $item->id)->first();
                    $val->rating = $rating;

                    if(!empty($product->name)) {
                        $val->product_name = $product->name;
                    } else {
                        $val->product_name = null;
                    }
                    //get sender list.
                    if(!empty($val->send_to)) {
                        $recepientUsers = explode(',', $val->send_to);
                        $recepient = Contacts::whereIn('id', $recepientUsers)->get();

                        $val->recepients = $recepient;
                    }
                    // end get sender list.

                    //get vendor name.
                    $vendor = User::where('id', $item->vendor_id)->first();
                    if(!empty($vendor->name)) {
                        $val->vendor_name = $vendor->name;
                    } else {
                        $val->vendor_name = null;
                    }

                    //get sender name.
                    $sender = User::where('id', $item->created_by)->withTrashed()->first();
                    if(!empty($sender->name)) {
                        $val->sender_name = $sender->name;
                    } else {
                        $val->sender_name = null;
                    }

                    $val->to_text = $item->to_text;
                    $val->write_your_text = $item->write_your_text;
                    $val->quantity = $item->quantity;
                    $val->order_item = $item;
                    $returnArray[] = $val;
                }
            }
            // dd(3243434);

        }

        $paperTypes = paperTypes::where('status', 1)->get();
        //  dd($returnArray);

        return view('admin.order.index', ['data'=>$returnArray, 'paper_type' => $paperTypes]);
    }

    public function postalCompleted()
    {
        $data = [];

        //Successfully delivered
        $includingTotalORderDelivered = array(
            'Successfully Delivered',
        );

        $orderItem = OrderItem::where('section', '!=', 2)
            ->whereNotNull('easy_parcel_order_no')
            ->whereNotIn('status', [2,3])
            ->limit(500)
            ->orderBy('id', 'DESC')
            ->get();

        $returnArray=[];

        foreach($orderItem as $key => $item) {
            $domain = "https://connect.easyparcel.my/?ac=";
            // dd($domain);

            $action = "EPTrackingBulk";
            $postparam = array(
                'authentication'    => "71dc1965276e33718db087d9d35227c3",
                'api'    => 'EP-AoBYqujRY',
                'bulk'    => array(
                    array(
                        'awb_no'    => $item->easy_parcel_order_no,
                    ),

                ),
            );

            $url = $domain . $action;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postparam));
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            ob_start();
            $return = curl_exec($ch);
            ob_end_clean();
            curl_close($ch);

            $json = json_decode($return);
            //  dd($json);
            if(!empty($json->result[0]->status_list)) {
                // dd($json->result[0]->status_list);
                foreach($json->result[0]->status_list as $key => $v) {
                    if(!empty($v->status)) {
                        $statusArry['status'][] = $v;
                    }
                }
                //  $returnArray['status'][] = $json->result[0]->status_list;
            } else {
                $statusArry['status'][] = null;
            }

            //$easyParcelStatus = Helper::getEasyParcelOrderStatus($item);
            
            //if(!empty($easyParcelStatus->result[0]->parcel[0]->ship_status)) {
            //    if(in_array($easyParcelStatus->result[0]->parcel[0]->ship_status, $includingTotalORderDelivered)) {
            if(!empty($statusArry['status'][0]->status)){
                if($statusArry['status'][0]->status == "Delivered"){
                   // dd('ak')
                    $image_array=[];
                    $query = Order::query();
                    $query->orderBy('id', 'DESC');
                    $query->where('id', $item->order_id);
                    $val = $query->first();

                    $val->order_item_id = $item->id;

                    $product = Product::where('id', $item->product_id)->withTrashed()->first();

                    if($item->product_type == 1) {
                        if($item->custom_order != 1) {
                            $post_card = PostCard::where('id', $item->product_id)->first();

                            if($item->image == null) {
                                $item->image = Storage::url('/postcard/'.$post_card->image);
                            } else {
                                $image_array[] = Storage::url('/order_print/'.$item->image);
                            }
                            if(!empty($post_card->image_2)) {
                                $image_array[] =Storage::url('/postcard/'.$post_card->image_2);
                            }
                        } else {
                            if(!empty($item->image)) {
                                $image_array[] = Storage::url('/order_print/'.$item->image);
                            }

                            if(!empty($item->image_2)) {
                                $image_array[] = Storage::url('/order_print/'.$item->image_2);
                            }

                        }
                    }
                    if($item->product_type == 3) {
                        if(!empty($item->image)) {
                            $image_array[] = Storage::url('/order_print/'.$item->image);
                        } else {
                            // $image_array[] = Storage::url('/product/'.$item->image);
                        }
                    }

                    $val->images = $image_array;

                    //get order status from easy parcel.
                    //$val->order_status_name = $easyParcelStatus->result[0]->parcel[0]->ship_status;
                    $val->order_status_name = $statusArry['status'][0]->status;

                    //get paper type listing
                    $paperTypeIds = explode(',', $val->paper_type_id);
                    $paperTypes = PaperTypes::whereIn('id', $paperTypeIds)->get();
                    $val['paperTypes'] = $paperTypes;

                    //get order name (Product name).
                    $product = Product::where('id', $item->product_id)->first();

                    //get rating and reveiw
                    $rating = Rating::where('order_item_id', $item->id)->first();
                    $val->rating = $rating;

                    if(!empty($product->name)) {
                        $val->product_name = $product->name;
                    } else {
                        $val->product_name = null;
                    }
                    //get sender list.
                    if(!empty($val->send_to)) {
                        $recepientUsers = explode(',', $val->send_to);
                        $recepient = Contacts::whereIn('id', $recepientUsers)->get();

                        $val->recepients = $recepient;
                    }
                    // end get sender list.

                    //get vendor name.
                    $vendor = User::where('id', $item->vendor_id)->first();
                    if(!empty($vendor->name)) {
                        $val->vendor_name = $vendor->name;
                    } else {
                        $val->vendor_name = null;
                    }

                    //get sender name.
                    $sender = User::where('id', $item->created_by)->withTrashed()->first();
                    if(!empty($sender->name)) {
                        $val->sender_name = $sender->name;
                    } else {
                        $val->sender_name = null;
                    }

                    $val->to_text = $item->to_text;
                    $val->write_your_text = $item->write_your_text;
                    $val->quantity = $item->quantity;
                    $val->order_item = $item;
                    $returnArray[] = $val;
                }
            }

        }

        $paperTypes = paperTypes::where('status', 1)->get();

        return view('admin.order.index', ['data'=>$returnArray, 'paper_type' => $paperTypes]);
    }

    public function giftPending()
    {
        $data = [];

        // $orderItem = OrderItem::where('section', 2)
        //     ->whereNull('easy_parcel_order_no')
        //     ->whereNotIn('status', [2,3])
        //     ->orderBy('id', 'DESC')
        //     ->get();

        $orderItem = OrderItem::where('section', 2)
        ->whereNull('easy_parcel_order_no')
        ->whereNotIn('status', [2, 3])
        ->whereDate('created_at', '<=', now()->subDays(3))
        ->orderBy('id', 'DESC')
        ->get();



        $returnArray=[];

        foreach($orderItem as $key => $item) {
            $image_array=[];
            $query = Order::query();
            $query->orderBy('id', 'DESC');
            $query->where('id', $item->order_id);
            $val = $query->first();

            $val->order_item_id = $item->id;

            $product = Product::where('id', $item->product_id)->withTrashed()->first();

            if(!empty($product->image_1)) {
                $image_array[] = Storage::url('/product/'.$product->image);
            }

            if(!empty($product->image_2)) {
                $image_array[] = Storage::url('/product/'.$product->image_2);
            }

            if(!empty($product->image_3)) {
                $image_array[] = Storage::url('/product/'.$product->image_3);
            }
            if(!empty($product->image_4)) {
                $image_array[] = Storage::url('/product/'.$product->image_4);
            }

            $val->images = $image_array;

            //get paper type listing
            $paperTypeIds = explode(',', $val->paper_type_id);
            $paperTypes = PaperTypes::whereIn('id', $paperTypeIds)->get();
            $val['paperTypes'] = $paperTypes;

            //get order name (Product name).
            $product = Product::where('id', $item->product_id)->first();

            //get rating and reveiw
            $rating = Rating::where('order_item_id', $item->id)->first();
            $val->rating = $rating;

            if(!empty($product->name)) {
                $val->product_name = $product->name;
            } else {
                $val->product_name = null;
            }
            //get sender list.
            if(!empty($val->send_to)) {
                $recepientUsers = explode(',', $val->send_to);
                $recepient = Contacts::whereIn('id', $recepientUsers)->get();

                $val->recepients = $recepient;
            }
            // end get sender list.

            //get vendor name.
            $vendor = User::where('id', $item->vendor_id)->first();
            if(!empty($vendor->name)) {
                $val->vendor_name = $vendor->name;
            } else {
                $val->vendor_name = null;
            }

            //get sender name.
            $sender = User::where('id', $item->created_by)->withTrashed()->first();
            if(!empty($sender->name)) {
                $val->sender_name = $sender->name;
            } else {
                $val->sender_name = null;
            }

            $val->to_text = $item->to_text;
            $val->write_your_text = $item->write_your_text;
            $val->quantity = $item->quantity;
            $val->order_item = $item;
            $returnArray[] = $val;


        }

        $paperTypes = paperTypes::where('status', 1)->get();

        return view('admin.order.index', ['data'=>$returnArray, 'paper_type' => $paperTypes]);
    }

    public function postalPending()
    {
        $data = [];

        $orderItem = OrderItem::where('section', '!=', 2)
            ->whereNull('easy_parcel_order_no')
            ->whereNotIn('status', [2,3])
            ->orderBy('id', 'DESC')
            ->get();

        $returnArray=[];

        foreach($orderItem as $key => $item) {
            $image_array=[];
            $query = Order::query();
            $query->orderBy('id', 'DESC');
            $query->where('id', $item->order_id);
            $val = $query->first();

            $val->order_item_id = $item->id;

            $product = Product::where('id', $item->product_id)->withTrashed()->first();

            if($item->product_type == 1) {
                if($item->custom_order != 1) {
                    $post_card = PostCard::where('id', $item->product_id)->first();

                    if($item->image == null) {
                        $item->image = Storage::url('/postcard/'.$post_card->image);
                    } else {
                        $image_array[] = Storage::url('/order_print/'.$item->image);
                    }
                    if(!empty($post_card->image_2)) {
                        $image_array[] =Storage::url('/postcard/'.$post_card->image_2);
                    }
                } else {
                    if(!empty($item->image)) {
                        $image_array[] = Storage::url('/order_print/'.$item->image);
                    }

                    if(!empty($item->image_2)) {
                        $image_array[] = Storage::url('/order_print/'.$item->image_2);
                    }

                }
            }
            if($item->product_type == 3) {
                if(!empty($item->image)) {
                    $image_array[] = Storage::url('/order_print/'.$item->image);
                } else {
                    // $image_array[] = Storage::url('/product/'.$item->image);
                }
            }

            $val->images = $image_array;

            //get paper type listing
            $paperTypeIds = explode(',', $val->paper_type_id);
            $paperTypes = PaperTypes::whereIn('id', $paperTypeIds)->get();
            $val['paperTypes'] = $paperTypes;

            //get order name (Product name).
            $product = Product::where('id', $item->product_id)->first();

            //get rating and reveiw
            $rating = Rating::where('order_item_id', $item->id)->first();
            $val->rating = $rating;

            if(!empty($product->name)) {
                $val->product_name = $product->name;
            } else {
                $val->product_name = null;
            }
            //get sender list.
            if(!empty($val->send_to)) {
                $recepientUsers = explode(',', $val->send_to);
                $recepient = Contacts::whereIn('id', $recepientUsers)->get();

                $val->recepients = $recepient;
            }
            // end get sender list.

            //get vendor name.
            $vendor = User::where('id', $item->vendor_id)->first();
            if(!empty($vendor->name)) {
                $val->vendor_name = $vendor->name;
            } else {
                $val->vendor_name = null;
            }

            //get sender name.
            $sender = User::where('id', $item->created_by)->withTrashed()->first();
            if(!empty($sender->name)) {
                $val->sender_name = $sender->name;
            } else {
                $val->sender_name = null;
            }

            $val->to_text = $item->to_text;
            $val->write_your_text = $item->write_your_text;
            $val->quantity = $item->quantity;
            $val->order_item = $item;
            $returnArray[] = $val;


        }

        $paperTypes = paperTypes::where('status', 1)->get();

        return view('admin.order.index', ['data'=>$returnArray, 'paper_type' => $paperTypes]);
    }

    public function giftReview()
    {
        $data = [];

        $ratings = Rating::with(['OrderItem'])->orderBy('id', 'DESC')->get();

        $returnArray=[];

        foreach($ratings as $key => $rating) {

            if(!empty($rating['OrderItem']->section) && ($rating['OrderItem']->section == 2)) {

                $item =  $rating['OrderItem'];

                $image_array=[];
                $query = Order::query();
                $query->orderBy('id', 'DESC');
                $query->where('id', $item->order_id);
                $val = $query->first();

                $val->order_item_id = $item->id;

                $product = Product::where('id', $item->product_id)->withTrashed()->first();

                $image_array[] = Storage::url('/product/'.$product->image);

                if(!empty($product->image_2)) {
                    $image_array[] = Storage::url('/product/'.$product->image_2);
                }

                if(!empty($product->image_3)) {
                    $image_array[] = Storage::url('/product/'.$product->image_3);
                }
                if(!empty($product->image_4)) {
                    $image_array[] = Storage::url('/product/'.$product->image_4);
                }

                $val->images = $image_array;

                //get paper type listing
                $paperTypeIds = explode(',', $val->paper_type_id);
                $paperTypes = PaperTypes::whereIn('id', $paperTypeIds)->get();
                $val['paperTypes'] = $paperTypes;

                //get order name (Product name).
                $product = Product::where('id', $item->product_id)->first();

                //get rating and reveiw
                $rating = Rating::where('order_item_id', $item->id)->first();
                $val->rating = $rating;

                if(!empty($product->name)) {
                    $val->product_name = $product->name;
                } else {
                    $val->product_name = null;
                }
                //get sender list.
                if(!empty($val->send_to)) {
                    $recepientUsers = explode(',', $val->send_to);
                    $recepient = Contacts::whereIn('id', $recepientUsers)->get();

                    $val->recepients = $recepient;
                }
                // end get sender list.

                //get vendor name.
                $vendor = User::where('id', $item->vendor_id)->first();
                if(!empty($vendor->name)) {
                    $val->vendor_name = $vendor->name;
                } else {
                    $val->vendor_name = null;
                }

                //get sender name.
                $sender = User::where('id', $item->created_by)->withTrashed()->first();
                if(!empty($sender->name)) {
                    $val->sender_name = $sender->name;
                } else {
                    $val->sender_name = null;
                }

                $val->to_text = $item->to_text;
                $val->write_your_text = $item->write_your_text;
                $val->quantity = $item->quantity;
                $val->order_item = $item;
                $returnArray[] = $val;
            }

        }

        $paperTypes = paperTypes::where('status', 1)->get();

        return view('admin.order.index', ['data'=>$returnArray, 'paper_type' => $paperTypes]);
    }

    public function postalReview()
    {
        $data = [];

        $ratings = Rating::with(['OrderItem'])->orderBy('id', 'DESC')->get();

        $returnArray=[];

        foreach($ratings as $key => $rating) {

            if(!empty($rating['OrderItem']->section) && ($rating['OrderItem']->section == 1)) {

                $item =  $rating['OrderItem'];

                $image_array=[];
                $query = Order::query();
                $query->orderBy('id', 'DESC');
                $query->where('id', $item->order_id);
                $val = $query->first();

                $val->order_item_id = $item->id;

                $product = Product::where('id', $item->product_id)->withTrashed()->first();

                if($item->product_type == 1) {
                    if($item->custom_order != 1) {
                        $post_card = PostCard::where('id', $item->product_id)->first();

                        if($item->image == null) {
                            $item->image = Storage::url('/postcard/'.$post_card->image);
                        } else {
                            $image_array[] = Storage::url('/order_print/'.$item->image);
                        }
                        if(!empty($post_card->image_2)) {
                            $image_array[] =Storage::url('/postcard/'.$post_card->image_2);
                        }
                    } else {
                        if(!empty($item->image)) {
                            $image_array[] = Storage::url('/order_print/'.$item->image);
                        }

                        if(!empty($item->image_2)) {
                            $image_array[] = Storage::url('/order_print/'.$item->image_2);
                        }

                    }
                }
                if($item->product_type == 3) {
                    if(!empty($item->image)) {
                        $image_array[] = Storage::url('/order_print/'.$item->image);
                    } else {
                        // $image_array[] = Storage::url('/product/'.$item->image);
                    }
                }

                $val->images = $image_array;

                //get paper type listing
                $paperTypeIds = explode(',', $val->paper_type_id);
                $paperTypes = PaperTypes::whereIn('id', $paperTypeIds)->get();
                $val['paperTypes'] = $paperTypes;

                //get order name (Product name).
                $product = Product::where('id', $item->product_id)->first();

                //get rating and reveiw
                $rating = Rating::where('order_item_id', $item->id)->first();
                $val->rating = $rating;

                if(!empty($product->name)) {
                    $val->product_name = $product->name;
                } else {
                    $val->product_name = null;
                }
                //get sender list.
                if(!empty($val->send_to)) {
                    $recepientUsers = explode(',', $val->send_to);
                    $recepient = Contacts::whereIn('id', $recepientUsers)->get();

                    $val->recepients = $recepient;
                }
                // end get sender list.

                //get vendor name.
                $vendor = User::where('id', $item->vendor_id)->first();
                if(!empty($vendor->name)) {
                    $val->vendor_name = $vendor->name;
                } else {
                    $val->vendor_name = null;
                }

                //get sender name.
                $sender = User::where('id', $item->created_by)->withTrashed()->first();
                if(!empty($sender->name)) {
                    $val->sender_name = $sender->name;
                } else {
                    $val->sender_name = null;
                }

                $val->to_text = $item->to_text;
                $val->write_your_text = $item->write_your_text;
                $val->quantity = $item->quantity;
                $val->order_item = $item;
                $returnArray[] = $val;

            }
        }

        $paperTypes = paperTypes::where('status', 1)->get();

        return view('admin.order.index', ['data'=>$returnArray, 'paper_type' => $paperTypes]);
    }
    public function refundGift()
    {
        try {
            $data = [];

            $orderItem = OrderItem::where('section', 2)
                ->whereIn('status', [3])
                ->get();

            $returnArray=[];

            foreach($orderItem as $key => $item) {
                $image_array=[];
                $query = Order::query();
                $query->orderBy('id', 'DESC');
                $query->where('id', $item->order_id);
                $val = $query->first();

                $val->order_item_id = $item->id;
                // dd($val);

                $product = Product::where('id', $item->product_id)->withTrashed()->first();
                // dd($product->image);

                $image_array[] = Storage::url('/product/'.$product->image);

                if(!empty($product->image_2)) {
                    $image_array[] = Storage::url('/product/'.$product->image_2);
                }

                if(!empty($product->image_3)) {
                    $image_array[] = Storage::url('/product/'.$product->image_3);
                }
                if(!empty($product->image_4)) {
                    $image_array[] = Storage::url('/product/'.$product->image_4);
                }

                $val->images = $image_array;
                $val->order_status_name = "Cancelled";



                //get paper type listing
                $paperTypeIds = explode(',', $val->paper_type_id);
                $paperTypes = PaperTypes::whereIn('id', $paperTypeIds)->get();
                $val['paperTypes'] = $paperTypes;

                //get order name (Product name).
                $product = Product::where('id', $item->product_id)->first();

                //get rating and reveiw
                $rating = Rating::where('order_item_id', $item->id)->first();
                $val->rating = $rating;

                if(!empty($product->name)) {
                    $val->product_name = $product->name;
                } else {
                    $val->product_name = null;
                }
                //get sender list.
                if(!empty($val->send_to)) {
                    $recepientUsers = explode(',', $val->send_to);
                    $recepient = Contacts::whereIn('id', $recepientUsers)->get();

                    $val->recepients = $recepient;
                }
                // end get sender list.

                //get vendor name.
                $vendor = User::where('id', $item->vendor_id)->first();
                if(!empty($vendor->name)) {
                    $val->vendor_name = $vendor->name;
                } else {
                    $val->vendor_name = null;
                }

                //get sender name.
                $sender = User::where('id', $item->created_by)->withTrashed()->first();
                if(!empty($sender->name)) {
                    $val->sender_name = $sender->name;
                } else {
                    $val->sender_name = null;
                }

                $val->to_text = $item->to_text;
                $val->write_your_text = $item->write_your_text;
                $val->quantity = $item->quantity;
                $val->order_item = $item;
                $returnArray[] = $val;

            }

            $paperTypes = paperTypes::where('status', 1)->get();

            return view('admin.order.refund_gift', ['data'=>$returnArray, 'paper_type' => $paperTypes]);

        } catch(Exception $e) {
            dd($e->getMessage());
        }
    }

}
