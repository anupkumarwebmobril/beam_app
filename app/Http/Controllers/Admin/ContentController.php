<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ContentManagement;
use DB;
use Session;

class ContentController extends Controller
{
    public function Terms_and_Conditions(){
        try {
            $dd  = DB::table('content_managements')->get();
            foreach ($dd as $key => $value) {
               $details = $value->term_and_condition;

            }
          return view('admin.content.terms-conditions')->with('details',$details);

        } catch (\Exceptions $e) {
           dd($e->getMessage());
        }
    }
    public function update_terms(Request $request)
    {
        // dd($request->all());
        try {

            DB::table('content_managements')->update(['term_and_condition' => $request->term_and_condition]);
            Session::flash('success', "Data has been successfully updated.");
            return redirect('admin/term_and_condition/list');
        } catch (\Exceptions $e) {

            Session::flash('message', $e->getMessage());
            return redirect('admin/term_and_condition/list');
        }
    }

    public function privacy_policy(Request $request)
    {
        $data = [];
        $data['title'] = "Privacy Policy";
        $data['details'] = ContentManagement::Select('privacy_policy')->first();

        return view('admin.content.privacy-policy', $data);
    }

    public function update_privacy(Request $request)
    {
        // dd(3443);
        try {
            DB::table('content_managements')->update(['privacy_policy' => $request->privacy_policy]);
            Session::flash('success', "Data has been successfully updated.");
            return redirect('admin/privacy-policy/list');
        } catch (\Exceptions $e) {

            Session::flash('message', $e->getMessage());
            return redirect('admin/privacy-policy/list');
        }
    }
    public function refund_policy(){
        try {
            $data = [];
            $data['title'] = "Refund Policy";
            $data['details'] = ContentManagement::Select('refund_policy')->first();
            return view('admin.content.refund', $data);
        } catch (\Exceptions $e) {

            Session::flash('message', $e->getMessage());
            return redirect('terms-conditions');
        }
    }
    public function update_refund(Request $request){
        // dd(3443);
        try {
            DB::table('content_managements')->update(['refund_policy' => $request->refund_policy]);
            Session::flash('success', "Data has been successfully updated.");
            return redirect('admin/refund-policy/list');
        } catch (\Exception $e) {
            Session::flash('message', $e->getMessage());
            return redirect('admin/refund-policy/list');
        }
    }


}
