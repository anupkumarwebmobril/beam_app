<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserSubscription;
use App\Models\MembershipPlan;
use App\User;

class UserSubscriptionController extends Controller
{
    public function index(){

        try{

            $usersubscriptions= UserSubscription::get();

            foreach($usersubscriptions as $key => $value)
            {
            	if(!empty($value->user_id))

            	{
            		$user = User::where('id', $value->user_id)->first();
                    if(!empty($user->name)){
            		$usersubscriptions[$key]->user_name = $user->name;
                }
            		//dd($usersubscriptions[$key]->user_name);
            	}

            	if(!empty($value->plan_id))
            	{
            		$plan = MembershipPlan::where('id', $value->plan_id)->first();
            		$usersubscriptions[$key]->plan_name = $plan->plan_name;
            	}
            }

            return view('admin.usersubscription.user_subscription_list')->withUsersubscriptions($usersubscriptions);
        }catch(Exception $e){

            return redirect()->back()
                ->withErrors($e->getMessage());
        }
    }
}
