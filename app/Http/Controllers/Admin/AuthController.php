<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Exception;
use App\User;
use App\Helpers\Helper;
use App\Models\ContentManagement;
use App\Models\Occasion;
use App\Models\OrderItem;
use Carbon\Carbon;
use DB;
use Mail;

use App\Models\UserDesignerRequest;
use App\Models\Product;
use App\Models\PostCard;

class AuthController extends Controller
{
    public function index(){

        try{
            // return "ts";
            $todayDate = Date('Y-m-d');
            //dd(Carbon::today());
            //dd($todayDate);
            $postal_mails = OrderItem::query()->where('product_type', 3)->whereDate('created_at', Carbon::today())->count();
            $postcard = OrderItem::query()->where('product_type', 1)->whereDate('created_at', Carbon::today())->count();
            $gifthub = OrderItem::query()->where('section', 2)->whereDate('created_at', Carbon::today())->count();
            $greetings = OrderItem::query()->where('section', 1)->whereNotIn('product_type', [1,3])->whereDate('created_at', Carbon::today())->count();

            $countUsers = User::query()->where('user_type', 2)->count();
            $countVendors = User::query()->whereIn('user_type', [4,5,6])->count();
            $designer = User::query()->where('user_type', 3)->count();
            $category = Occasion::query()->count();

            //Top 10 selling postal
            $postalTopTenSellingProducts = [];
            $postalTopTenSellingProduct = OrderItem::select('product_id', DB::raw('count(*) as total_product_sale'))->where('product_type', 2)->where('section', 1)->whereNotNull('product_id')->groupBy('product_id')->limit(10)->orderBy('total_product_sale', 'DESC')->get();
            foreach($postalTopTenSellingProduct as $row){
                $product = Product::where('id', $row->product_id)->withTrashed()->first();
                if(!empty($product->name)){
                    $postalTopTenSellingProducts[] = array($product->name, $row->total_product_sale);
                }
            }

            //Top 10 selling postcard
            $postcardTopTenSellingProduct = OrderItem::select('product_id', DB::raw('count(*) as total_product_sale'))->where('product_type', 1)->whereNotNull('product_id')->groupBy('product_id')->limit(10)->orderBy('total_product_sale', 'DESC')->get();
            //$postcardTopTenSellingProducts[] = array('Task', 'Postcard Top Ten Selling Products');
            $postcardTopTenSellingProducts = [];
            foreach($postcardTopTenSellingProduct as $row1){
                $product = PostCard::where('id', $row1->product_id)->withTrashed()->first();
                if(!empty($product->name)){
                    $postcardTopTenSellingProducts[] = array($product->name, $row1->total_product_sale);
                }
            }

            //Top 10 selling gifthub.
            $gifthubTopTenSellingProduct = OrderItem::select('product_id', DB::raw('count(*) as total_product_sale'))->where('section', 2)->whereNotNull('product_id')->groupBy('product_id')->limit(10)->orderBy('total_product_sale', 'DESC')->get();
            //$gifthubTopTenSellingProducts[] = array('Task', 'Gifthub Top Ten Selling Products');
            $gifthubTopTenSellingProducts = [];
            foreach($gifthubTopTenSellingProduct as $row){
                $product = Product::where('id', $row->product_id)->withTrashed()->first();
                if(!empty($product->name)){
                    $gifthubTopTenSellingProducts[] = array($product->name, $row->total_product_sale);
                }
            }

            return view('admin.admin_dashboard', [
                'postal_mails' => $postal_mails,
                'postcard' => $postcard,
                'gifthub' => $gifthub,
                'greetings' => $greetings,

                'countUsers' => $countUsers,
                'countVendors' => $countVendors,
                'designer' => $designer,
                'category' => $category,
                'postalTopTenSellingProducts' => $postalTopTenSellingProducts,
                'postcardTopTenSellingProducts' => $postcardTopTenSellingProducts,
                'gifthubTopTenSellingProducts' => $gifthubTopTenSellingProducts
                ]);

            /* return view('admin.admin_dashboard')
                ->withCountUsers($countUsers)
                ->withCountVendors($countVendors); */

        }catch(Exception $e){

            return redirect()->back()
                ->withErrors($e->getMessage());
        }
    }

    public function weeklyOrderTotal(){
        try {
            $now = Carbon::now();

            $lastWeek = $now->startOfWeek(Carbon::SUNDAY);
            $lastMonth = Carbon::now()->startOfMonth()->subDays(1);
            //$lastWeek = Carbon::now()->subWeek();
            //$lastMonth = Carbon::now()->subDays(30);
            $todayDate = Date('Y-m-d');
            //today total amount
            $postal_mails_amount_today = OrderItem::select(DB::raw('sum(item_grand_total_price) as total'))->where('product_type', 3)->whereDate('created_at', Carbon::today())->first();
            $postal_mails_amount_today = ($postal_mails_amount_today->total > 0) ? round($postal_mails_amount_today->total, 2) : 0;

            $postcard_amount_today = OrderItem::select(DB::raw('sum(item_grand_total_price) as total'))->where('product_type', 1)->whereDate('created_at', Carbon::today())->first();
            $postcard_amount_today = ($postcard_amount_today->total > 0) ? round($postcard_amount_today->total, 2) : 0;

            $gifthub_amount_today = OrderItem::select(DB::raw('sum(item_grand_total_price) as total'))->where('section', 2)->whereDate('created_at', Carbon::today())->first();
            $gifthub_amount_today = ($gifthub_amount_today->total > 0) ? round($gifthub_amount_today->total, 2) : 0;

            $greetings_amount_today = OrderItem::select(DB::raw('sum(item_grand_total_price) as total'))->where('section', 1)->whereNotIn('product_type', [1,3])->whereDate('created_at', Carbon::today())->first();
            $greetings_amount_today = ($greetings_amount_today->total > 0) ? round($greetings_amount_today->total, 2) : 0;

            //Weekly count
            $postal_mails_weekly = OrderItem::query()->where('product_type', 3)->whereDate('created_at', '>', $lastWeek)->count();
            $postcard_weekly = OrderItem::query()->where('product_type', 1)->whereDate('created_at', '>', $lastWeek)->count();
            $gifthub_weekly = OrderItem::query()->where('section', 2)->whereDate('created_at', '>', $lastWeek)->count();
            $greetings_weekly = OrderItem::query()->where('section', 1)->whereNotIn('product_type', [1,3])->whereDate('created_at', '>', $lastWeek)->count();

            //weekly total
            $postal_mails_weekly_amount = OrderItem::select(DB::raw('sum(item_grand_total_price) as total'))->where('product_type', 3)->whereDate('created_at', '>', $lastWeek)->first();
            $postal_mails_weekly_amount = ($postal_mails_weekly_amount->total > 0) ? round($postal_mails_weekly_amount->total, 2) : 0;

            $postcard_weekly_amount = OrderItem::select(DB::raw('sum(item_grand_total_price) as total'))->where('product_type', 1)->whereDate('created_at', '>', $lastWeek)->first();
            $postcard_weekly_amount = ($postcard_weekly_amount->total > 0) ? round($postcard_weekly_amount->total, 2) : 0;

            $gifthub_weekly_amount = OrderItem::select(DB::raw('sum(item_grand_total_price) as total'))->where('section', 2)->whereDate('created_at', '>', $lastWeek)->first();
            $gifthub_weekly_amount = ($gifthub_weekly_amount->total > 0) ? round($gifthub_weekly_amount->total, 2) : 0;

            $greetings_weekly_amount = OrderItem::select(DB::raw('sum(item_grand_total_price) as total'))->where('section', 1)->whereNotIn('product_type', [1,3])->whereDate('created_at', '>', $lastWeek)->first();
            $greetings_weekly_amount = ($greetings_weekly_amount->total > 0) ? round($greetings_weekly_amount->total, 2) : 0;

            //Monthly count
            $postal_mails_monthly = OrderItem::query()->where('product_type', 3)->whereDate('created_at', '>', $lastMonth)->count();
            $postcard_monthly = OrderItem::query()->where('product_type', 1)->whereDate('created_at', '>', $lastMonth)->count();
            $gifthub_monthly = OrderItem::query()->where('section', 2)->whereDate('created_at', '>', $lastMonth)->count();
            $greetings_monthly = OrderItem::query()->where('section', 1)->whereNotIn('product_type', [1,3])->whereDate('created_at', '>', $lastMonth)->count();

            //Monthly total
            $postal_mails_monthly_amount = OrderItem::select(DB::raw('sum(item_grand_total_price) as total'))->where('product_type', 3)->whereDate('created_at', '>', $lastMonth)->first();
            $postal_mails_monthly_amount = ($postal_mails_monthly_amount->total > 0) ? round($postal_mails_monthly_amount->total, 2) : 0;

            $postcard_monthly_amount = OrderItem::select(DB::raw('sum(item_grand_total_price) as total'))->where('product_type', 1)->whereDate('created_at', '>', $lastMonth)->first();
            $postcard_monthly_amount = ($postcard_monthly_amount->total > 0) ? round($postcard_monthly_amount->total, 2) : 0;

            $gifthub_monthly_amount = OrderItem::select(DB::raw('sum(item_grand_total_price) as total'))->where('section', 2)->whereDate('created_at', '>', $lastMonth)->first();
            $gifthub_monthly_amount = ($gifthub_monthly_amount->total > 0) ? round($gifthub_monthly_amount->total, 2) : 0;

            $greetings_monthly_amount = OrderItem::select(DB::raw('sum(item_grand_total_price) as total'))->where('section', 1)->whereNotIn('product_type', [1,3])->whereDate('created_at', '>', $lastMonth)->first();
            $greetings_monthly_amount = ($greetings_monthly_amount->total > 0) ? round($greetings_monthly_amount->total, 2) : 0;

            $monthly_total_amount = OrderItem::select(DB::raw('sum(item_grand_total_price) as total'))->whereDate('created_at', '>', $lastMonth)->first();
            $monthly_total_amount = ($monthly_total_amount->total > 0) ? round($monthly_total_amount->total, 2) : 0;
            //dd($amount[0]['total']);

            $designRequest =  UserDesignerRequest::select('users.name','user_designer_request.*')
            ->leftjoin('users','users.id','=','user_designer_request.from_user_id')
            ->count();
            // $designRequest = UserDesignerRequest::query()->whereDate('create_at', '>', $lastMonth)->count();


            $return = [
                'postal_mails_weekly' => $postal_mails_weekly,
                'postcard_weekly' => $postcard_weekly,
                'gifthub_weekly' => $gifthub_weekly,
                'greetings_weekly' => $greetings_weekly,

                'postal_mails_weekly_amount' => $postal_mails_weekly_amount,
                'postcard_weekly_amount' => $postcard_weekly_amount,
                'gifthub_weekly_amount' => $gifthub_weekly_amount,
                'greetings_weekly_amount' => $greetings_weekly_amount,

                'postal_mails_amount_today' => $postal_mails_amount_today,
                'postcard_amount_today' => $postcard_amount_today,
                'gifthub_amount_today' => $gifthub_amount_today,
                'greetings_amount_today' => $greetings_amount_today,

                //Monthly
                'postal_mails_monthly' => $postal_mails_monthly,
                'postcard_monthly' => $postcard_monthly,
                'gifthub_monthly' => $gifthub_monthly,
                'greetings_monthly' => $greetings_monthly,

                'postal_mails_monthly_amount' => $postal_mails_monthly_amount,
                'postcard_monthly_amount' => $postcard_monthly_amount,
                'gifthub_monthly_amount' => $gifthub_monthly_amount,
                'greetings_monthly_amount' => $greetings_monthly_amount,

                'monthly_total_amount' => $monthly_total_amount,

                'design_request' => $designRequest,

            ];
            return response()->json(array('status' => true,  'data' => $return));

        } catch (\Exception $e) {
            return response()->json(array('status' => false, 'msg'=> $e->getMessage()));
        }
    }

    public function orderData(){
        try {
            $now = Carbon::now();
            $lastWeek = $now->startOfWeek(Carbon::SUNDAY);
            $lastMonth = Carbon::now()->startOfMonth()->subDays(1);
            //$lastWeek = Carbon::now()->subWeek();
            //$lastMonth = Carbon::now()->subDays(30);
            $todayDate = Date('Y-m-d');

            $notIncludingStatusArray = array(
                'Cancel',
                'Returned',
                'Successfully Delivered',
                'Pending For Collection'
            );

            //Order shipped and waiting
            $totalORderItem = OrderItem::query()->whereNotNull('easy_parcel_order_no')->get();

            $totalORderShippedAndAwaitingForDelivery = 0;
            foreach($totalORderItem as $orderItem){
                $easyParcelStatus = Helper::getEasyParcelOrderStatus($orderItem);

                // dd($easyParcelStatus->result[0]->latest_status);
                if(!empty($easyParcelStatus->result[0]->latest_status)){
                    if(in_array($easyParcelStatus->result[0]->latest_status, $notIncludingStatusArray)){
                        //No need to count.
                    } else {
                        $totalORderShippedAndAwaitingForDelivery = $totalORderShippedAndAwaitingForDelivery+1;
                    }
                }
            }

            //Successfully delivered
            // $includingTotalORderDelivered = array(
            //     'Successfully Delivered',
            // );
            $includingTotalORderDelivered = array(
                'Delivered',
            );
            $totalORderDelivered = 0;
            foreach($totalORderItem as $orderItem){
                $easyParcelStatus = Helper::getEasyParcelOrderStatus($orderItem);
                if(!empty($easyParcelStatus->result[0]->latest_status)){
                    if(in_array($easyParcelStatus->result[0]->latest_status, $includingTotalORderDelivered)){
                        //No need to count.
                        $totalORderDelivered = $totalORderDelivered+1;
                    }
                }
            }

            //Total pending order.
            /* $includingPendingOrder = array(
                'Pending For Collection',
            );
            $totalPedningOrder = 0;
            foreach($totalORderItem as $orderItem){
                $easyParcelStatus = Helper::getEasyParcelOrderStatus($orderItem);
                if(!empty($easyParcelStatus->result[0]->status_list[0]->status)){
                    if(in_array($easyParcelStatus->result[0]->parcel[0]->ship_status, $includingPendingOrder)){
                        //No need to count.
                        $totalPedningOrder = $totalPedningOrder+1;
                    }
                }
            } */
            $totalPedningOrder = OrderItem::
                whereNull('easy_parcel_order_no')
                ->whereNotIn('status', [2,3])
                ->count();


            //Return / Refund request.
            $includingReturnRefund = array(
                'Returned',
            );
            $totalReturnRefund = 0;
            // foreach($totalORderItem as $orderItem){
            //     $easyParcelStatus = Helper::getEasyParcelOrderStatus($orderItem);
            //     if(!empty($easyParcelStatus->result[0]->parcel[0]->ship_status)){
            //         if(in_array($easyParcelStatus->result[0]->parcel[0]->ship_status, $includingReturnRefund)){
            //             //No need to count.
            //             $totalReturnRefund = $totalReturnRefund+1;
            //         }
            //     }
            // }

            $totalReturnRefund = OrderItem::where('section', 2)
                ->whereIn('status', [3])
                ->count();

            //Cancel order.
            //This was for easy parcel
            // $includingCancel = array(
            //     'Cancel',
            //     'Cancel By Admin'
            // );
            // $totalCancelOrder = 0;
            // foreach($totalORderItem as $orderItem){
            //     $easyParcelStatus = Helper::getEasyParcelOrderStatus($orderItem);
            //     if(!empty($easyParcelStatus->result[0]->parcel[0]->ship_status)){
            //         if(in_array($easyParcelStatus->result[0]->parcel[0]->ship_status, $includingCancel)){
            //             //No need to count.
            //             $totalCancelOrder = $totalCancelOrder+1;
            //         }
            //     }
            // }
            $totalCancelOrder = OrderItem::select('id')->where('status', '=', 2)->count();

            $return = [
                'totalORderShippedAndAwaitingForDelivery' => $totalORderShippedAndAwaitingForDelivery,
                'totalORderDelivered' => $totalORderDelivered,
                'totalPedningOrder' => $totalPedningOrder,
                'totalReturnRefund' => $totalReturnRefund,
                'totalCancelOrder' => $totalCancelOrder,

            ];
            return response()->json(array('status' => true,  'data' => $return));

        } catch (\Exception $e) {
            return response()->json(array('status' => false, 'msg'=> $e->getMessage()));
        }
    }

    public function productDataForDashboard(){
        try {
            $now = Carbon::now();
            $lastWeek = $now->startOfWeek(Carbon::SUNDAY);
            $lastMonth = Carbon::now()->startOfMonth()->subDays(1);
            // $lastWeek = Carbon::now()->subWeek();
            // $lastMonth = Carbon::now()->subDays(30);
            $todayDate = Date('Y-m-d');

            //postal
            $postalProductCount = Product::where('type',  1)->count();
            $gifthubProductCount = Product::where('type',  2)->count();
            $postcardProductCount = PostCard::count();

            $gifthubProductCountInactive = Product::where('type',  2)->where('status',0)->count();


            $postalProductActiveCount = Product::where('type',  1)->where('status', 1)->count();
            $postalProductInActiveCount = Product::where('type',  1)->where('status', 0)->count();

            //Top 10 selling postal
            $postalTopTenSellingProduct = OrderItem::select('product_id', DB::raw('count(*) as total_product_sale'))->whereNotIn('status', [2,3])->where('product_type', 2)->where('section', 1)->whereNotNull('product_id')->groupBy('product_id')->limit(10)->orderBy('total_product_sale', 'DESC')->get();

            $postalTopTenSellingProducts[] = array('Task', 'Postal Top Ten Selling Products');
            foreach($postalTopTenSellingProduct as $row){
                $product = Product::where('id', $row->product_id)->withTrashed()->first();
                if(!empty($product->name)){
                    $postalTopTenSellingProducts[] = array($product->name, $row->total_product_sale);
                }
            }


            //Top 10 selling postcard
            $postcardTopTenSellingProduct = OrderItem::select('product_id', DB::raw('count(*) as total_product_sale'))->whereNotIn('status', [2,3])->where('product_type', 1)->whereNotNull('product_id')->groupBy('product_id')->limit(10)->orderBy('total_product_sale', 'DESC')->get();
            $postcardTopTenSellingProducts[] = array('Task', 'Postcard Top Ten Selling Products');
            foreach($postcardTopTenSellingProduct as $row1){
                $product = PostCard::where('id', $row1->product_id)->withTrashed()->first();
                if(!empty($product->name)){
                    $postcardTopTenSellingProducts[] = array($product->name, $row1->total_product_sale);
                }
            }

            //Top 10 selling gifthub.
            $gifthubTopTenSellingProduct = OrderItem::select('product_id', DB::raw('count(*) as total_product_sale'))->whereNotIn('status', [2,3])->where('section', 2)->whereNotNull('product_id')->groupBy('product_id')->limit(10)->orderBy('total_product_sale', 'DESC')->get();
            $gifthubTopTenSellingProducts[] = array('Task', 'Gifthub Top Ten Selling Products');
            foreach($gifthubTopTenSellingProduct as $row){
                $product = Product::where('id', $row->product_id)->withTrashed()->first();
                if(!empty($product->name)){
                    $gifthubTopTenSellingProducts[] = array($product->name, $row->total_product_sale);
                }
            }

            $return = [
                'postalProductCount' => $postalProductCount,
                'gifthubProductCount' => $gifthubProductCount,
                'postcardProductCount' => $postcardProductCount,
                'postalProductActiveCount' => $postalProductActiveCount,
                'postalProductInActiveCount' => $postalProductInActiveCount,
                'postalTopTenSellingProducts' => $postalTopTenSellingProducts,
                'postcardTopTenSellingProducts' => $postcardTopTenSellingProducts,
                'gifthubTopTenSellingProducts' => $gifthubTopTenSellingProducts,
                'gifthubProductCountInactive' => $gifthubProductCountInactive,
            ];
            return response()->json(array('status' => true,  'data' => $return));

        } catch (\Exception $e) {
            return response()->json(array('status' => false, 'msg'=> $e->getMessage()));
        }
    }


	public function loginPage(){

        return view('admin.auth.login');

    }

    public function login(Request $request)
    {
        try{

            $rules = array(
                'email' => 'required|email',
                'password' => 'required|'
            );


            $remember_me = $request->has('remember') ? true : false;

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {

                return Redirect::to('/adminlogin')
                    ->withErrors("Email and Password is required to login.")
                    ->withInput(Request::get('password'));
            }

            else {

                $userdata = array(
                    'email' => $request->get('email'),
                    'password' => $request->get('password'),
                );

                 $input_arr = array(
                    'name' => "sagar",
                    'email' => "sagar741@yopmail.com",
                    'contactNo' => "12345678",
                    'message' => "sagar" ." Admin Add you.",
                    'subject' => "testing"
                );
                $message = "Hii sagar this test mail";

                Mail::send('emails.demo', $input_arr, function ($message) use ($input_arr) {
                    $message->to($input_arr['email'], 'admin')
                        ->subject($input_arr['subject']);
                    $message->from('support@letsbeam.it', 'Beam App');
                });

                if (Auth::attempt($userdata, $remember_me)) {

                    if(auth()->user()->user_type != 1){

                        Auth::logout();
                        return redirect()->back()
                            ->withErrors("Forbidden! You do not have permission to access this route.");
                    }

                    return Redirect::to('admin/dashboard');

                } else {

                    return Redirect::to('/adminlogin')

                        ->withErrors("The credentials do not match our records");
                }

            }

        }catch(Exception $e){

            return redirect()->back()
                ->withErrors($e->getMessage());
        }
    }

    public function logout(Request $request) {

        Auth::logout();

        return Redirect::to('/adminlogin')->withMessage('Logged out successfully.');
    }

    public function payment(){
        return view('admin.auth.payment');

    }

    public function designer_registration(){
        return view('admin.auth.designer_registration');

    }

    public function privacy_policy(){
        $contenteManagement = ContentManagement::first();
        return view('admin.auth.privacy_policy', ['privacy_policy' => $contenteManagement]);

    }
    // sagar code for chart in admin dashboard
    public function monthyreporter()
     {
       try {
                $lastdate = Carbon::now();
                $lastMonth = Carbon::now()->subDays(30);
                $todayDate = Date('Y-m-d');
                //today total amount
                $authUser = Auth::user();

                $total_qty = [];
                $lastdate = Carbon::now()->subDays(30);
                $lastdate->format('Y-m-d');
                $todaydate = Carbon::now();
                $todaydate->format('Y-m-d');

                $aa[] =  array('Date', 'Qty', 'Sell');

                for ($i=1; $i <= 30 ; $i++) {

                    $adddays = $lastdate->addDays(1);
                    $adddays->format('Y-m-d');
                    $orders = OrderItem::select(DB::raw('DATE(created_at) as date'),DB::raw('Count(quantity) as qty'))
                            // ->where('vendor_id', $authUser->id)
                            ->where('section', 2)->whereDate('created_at','=',$adddays)
                            ->groupBy('date')
                            ->get();
                            // dd($orders);

                    $selling =  OrderItem::select(DB::raw('sum(item_grand_total_price) as total'),DB::raw('DATE(created_at) as date'))
                            // ->where('vendor_id', $authUser->id)
                            ->where('section', 2)->whereDate('created_at', '=', $adddays)
                            ->groupBy('date')
                            ->get();

                            foreach($selling as $sell2){
                                foreach ($orders as $key => $value) {

                                 $aa[] = array($adddays->format('Y-m-d'), $value->qty, $sell2->total);

                                }

                            }

                    $total_qty[]['date'] = $adddays;
                    $total_qty[]['qty'] = $orders->count();
            }

                $return = [
                    'monthlyreport' => $aa,


                ];
               return response()->json(array('status' => true,  'data' => $return));

       } catch (\Exception $e) {

        return response()->json(array('status' => false, 'msg'=> $e->getMessage()));

       }
     }
    //  for geeting in admin chart for 1 month day sells
    public function greeting_monthly()
     {
       try {
                $lastdate = Carbon::now();
                $lastMonth = Carbon::now()->subDays(30);
                $todayDate = Date('Y-m-d');
                //today total amount
                $authUser = Auth::user();

                $total_qty = [];
                $lastdate = Carbon::now()->subDays(30);
                $lastdate->format('Y-m-d');
                $todaydate = Carbon::now();
                $todaydate->format('Y-m-d');

                $gg[] = array('Date', 'Qty', 'Sell');

                for ($i=1; $i <= 30 ; $i++) {

                    $adddays = $lastdate->addDays(1);
                    $adddays->format('Y-m-d');


                    $greetings_orders = OrderItem::select(DB::raw('DATE(created_at) as date'),DB::raw('Count(quantity) as qty'))
                                        ->where('section', 1)->whereNotIn('product_type', [1,3])
                                        ->whereDate('created_at', '=', $adddays)
                                        ->groupBy('date')
                                        ->get();
                                        // dd($greetings_orders);

                    $greetings_sell =  OrderItem::select(DB::raw('sum(item_grand_total_price) as total'),DB::raw('DATE(created_at) as date'))
                                        ->where('section', 1)->whereNotIn('product_type', [1,3])
                                        ->whereDate('created_at', '=', $adddays)
                                        ->groupBy('date')
                                        ->get();

                            foreach ($greetings_sell as $key => $value) {
                                foreach ($greetings_orders as $key => $value1) {

                                    $gg[] = array($adddays->format('Y-m-d'), $value1->qty, $value->total);
                                }

                            }
                }


                $return = [

                    'greetingreport' => $gg,

                ];
               return response()->json(array('status' => true,  'data' => $return));

       } catch (\Exception $e) {

        return response()->json(array('status' => false, 'msg'=> $e->getMessage()));

       }
     }

     public function postmail_monthly()
     {
       try {
                $lastdate = Carbon::now();
                $lastMonth = Carbon::now()->subDays(30);
                $todayDate = Date('Y-m-d');
                //today total amount
                $authUser = Auth::user();

                $total_qty = [];
                $lastdate = Carbon::now()->subDays(30);
                $lastdate->format('Y-m-d');
                $todaydate = Carbon::now();
                $todaydate->format('Y-m-d');

                $pp[] = array('Date', 'Qty', 'Sell');

                for ($i=1; $i <= 30 ; $i++) {

                    $adddays = $lastdate->addDays(1);
                    $adddays->format('Y-m-d');


                    $postmail_orders = OrderItem::select(DB::raw('DATE(created_at) as date'),DB::raw('Count(quantity) as qty'))
                                        ->where('section', 1)->whereNotIn('product_type', [1,2])
                                        // ->where('product_type', 3)
                                        ->whereDate('created_at', '=', $adddays)
                                        ->groupBy('date')
                                        ->get();


                    $postmail_sell =  OrderItem::select(DB::raw('sum(item_grand_total_price) as total'),DB::raw('DATE(created_at) as date'))
                                        ->where('section', 1)->whereNotIn('product_type', [1,2])
                                        // >where('product_type', 3)
                                        ->whereDate('created_at', '=', $adddays)
                                        ->groupBy('date')
                                        ->get();

                            foreach ($postmail_sell as $key => $value) {
                                foreach ($postmail_orders as $key => $value1) {

                                    $pp[] = array($adddays->format('Y-m-d'), $value1->qty, $value->total);

                                }

                            }
                }

                $return = [

                    'postmailreport' => $pp,

                ];
               return response()->json(array('status' => true,  'data' => $return));

       } catch (\Exception $e) {

        return response()->json(array('status' => false, 'msg'=> $e->getMessage()));

       }
     }
     public function postcard_monthly()
     {
       try {
                $lastdate = Carbon::now();
                $lastMonth = Carbon::now()->subDays(30);
                $todayDate = Date('Y-m-d');
                //today total amount
                $authUser = Auth::user();

                $total_qty = [];
                $lastdate = Carbon::now()->subDays(30);
                $lastdate->format('Y-m-d');
                $todaydate = Carbon::now();
                $todaydate->format('Y-m-d');

                $pp[] = array('Date', 'Qty', 'Sell');

                for ($i=1; $i <= 30 ; $i++) {

                    $adddays = $lastdate->addDays(1);
                    $adddays->format('Y-m-d');


                    $postcard_orders = OrderItem::select(DB::raw('DATE(created_at) as date'),DB::raw('Count(quantity) as qty'))
                                        ->where('section', 1)->whereNotIn('product_type', [3,2])

                                        ->whereDate('created_at', '=', $adddays)
                                        ->groupBy('date')
                                        ->get();


                    $postcard_sell =  OrderItem::select(DB::raw('sum(item_grand_total_price) as total'),DB::raw('DATE(created_at) as date'))
                                        ->where('section', 1)->whereNotIn('product_type', [2,3])
                                        // >where('product_type', 3)
                                        ->whereDate('created_at', '=', $adddays)
                                        ->groupBy('date')
                                        ->get();

                            foreach ($postcard_sell as $key => $value) {
                                foreach ($postcard_orders as $key => $value1) {

                                    $pp[] = array($adddays->format('Y-m-d'), $value1->qty, $value->total);

                                }

                            }
                }

                $return = [

                    'postcardreport' => $pp,

                ];
               return response()->json(array('status' => true,  'data' => $return));

       } catch (\Exception $e) {

        return response()->json(array('status' => false, 'msg'=> $e->getMessage()));

       }
     }
     public function designer_request_report()
     {
       // dd('test');
       try {
            // $designRequest = UserDesignerRequest::query()->whereDate('create_at', '>', $lastMonth)->count();
            // dd($designRequest);

            $lastdate = Carbon::now();
            $lastMonth = Carbon::now()->subDays(30);
            $todayDate = Date('Y-m-d');
            //today total amount
            $authUser = Auth::user();

            $total_qty = [];
            $lastdate = Carbon::now()->subDays(30);
            $lastdate->format('Y-m-d');
            $todaydate = Carbon::now();
            $todaydate->format('Y-m-d');

            $pp[] = array('Date', 'request','days');

            for ($i=1; $i <= 30 ; $i++) {

                $adddays = $lastdate->addDays(1);
                $adddays->format('Y-m-d');


                $designer_request = UserDesignerRequest::select(DB::raw('DATE(create_at) as date'))
                                    ->whereDate('create_at', '=', $adddays)
                                  //  ->groupBy('date')
                                    ->get();
                // dd($designer_request);
                                    $pp[] = array($adddays->format('Y-m-d'), $designer_request->count(),0);
                                    // foreach ($designer_request as $key => $value) {
                        // }
            }

            $return = [

                'designer_request' => $pp,

            ];
           return response()->json(array('status' => true,  'data' => $return));
       } catch (\Exception $e) {
        return response()->json(array('status' => true,  'data' => $e->getMessage()));

       }
     }

}
