<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\DB;
use Session;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Models\Category;
use App\Helpers\Helper;
use App\Models\MailType;
use App\Models\Occasion;
use App\Models\Type;
use App\Models\SubCategory;
use App\Models\Unit;
use App\Models\Product;
use URL;

class FormalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = MailType::query();        
        $query->orderBy('id', 'DESC');   
        $data = $query->get();
      
        return view('admin.formal.index', ['data'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
        //$occasions = Occasion::where('status','=','1')->get();
        //$category = Category::where('status','=','1')->get();
        $occasions = [];
        $category = [];
        return view('admin.formal.create', ['occasions' => $occasions, 'category' => $category]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, MailType $MailType)
    {
     
        try {
            $user = auth()->user();
            $data = $request->all();
            $data['created_by'] = $user->id;

            //first image
            if ($request->hasFile('image')) {
                //check if image is png only.
                $image = $request->file('image');
                $name = $image->getClientOriginalName();
                $extention = \File::extension($name);
               
                if($extention != "png"){
                    return redirect()->back()->withErrors('Image should be PNG formats only.');
                }

                $request['folder_name'] = 'mail_type';
                $user = Helper::commonAddUpdateImageMethod($request, $user);
                if(!empty($user->image)){
                    $data['image'] = $user->image;
                }                
            }
            
                  
            $MailType->setIncrementing(true);
           
            $MailType->fill($data)->save();           
            $url = URL::to('/admin/formal');
            //$request->session()->flash('success', 'Product added successfully');
            return response()->json(array('status' => true, 'redirect'=>$url, 'msg' => 'Formal added successfully')); 
        } catch (\Exception $e) {
            //$request->session()->flash('error', $e->getMessage());
            return response()->json(array('status' => false, 'msg'=> $e->getMessage()));
        }
        return redirect(route('formal.create'));
       // return redirect(route('vendorproduct.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        //
        return view('brand.show', ['brand' => $brand]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(MailType $MailType)
    {       
         
        return view('admin.formal.edit', ['data' => $MailType]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MailType $MailType)
    {      
        try {
           $data = $request->all();
           $user = auth()->user();
           $data['update_by'] = $user->id;

            //first image
            if ($request->hasFile('image')) {
                //check if image is png only.
                $image = $request->file('image');
                $name = $image->getClientOriginalName();
                $extention = \File::extension($name);
               
                if($extention != "png"){
                    return redirect()->back()->withErrors('Image should be PNG formats only.');
                }
                    
                $request['folder_name'] = 'mail_type';
                $user = Helper::commonAddUpdateImageMethod($request, $user);
                
                if(!empty($user->image)){
                    $data['image'] = $user->image;
                }                
            }  
                     
            $MailType->fill($data); 
            $MailType->save();
            
            $request->session()->flash('success', 'Formal updated successfully.');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
        }
        return redirect(route('Admin.formal.index'));
        //return redirect(route('vendorproduct.edit', [$product->id]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Occasion $occasion)
    {
        try {
            MailType::where('id', $request->id)->delete();
            $request->session()->flash('danger', 'Formal deleted successfully.');

        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            throw new \Exception('Error while deleteing in the database.');
        }
        return redirect(route('formal.index'));
    }

    

    
}


            