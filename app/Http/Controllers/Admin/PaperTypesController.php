<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Exception;
use App\User;
use Validator;
use App\Models\PaperTypes;

class PaperTypesController extends Controller
{
    public function index()
    {
        $paper = PaperTypes::orderBy('id','asc')->get();
        return view('admin.paper.paper_list')->withPaper($paper);

    }
      public function papereForm ()
    {
        return view('admin.paper.add_paper');
    }
    public function addpaper(Request $request)
    {
        // print_r($request->all()); die();
        try{

            $rules = array(
            'name' => 'required|max:400',
            'price' => 'required',
            'order_by'=>'required'
            // 'status'=>'',
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
            {

            return redirect()->back()->withErrors($validator)->withInput();
            }

            $paper = new PaperTypes();
            $paper->name = $request->name;
            $paper->price = $request->price;
            $paper->status = $request->status;
            $paper->order_by = $request->order_by;
            $paper->save();
            // dd($excercise);
            return Redirect::to('admin/paper-list')->withMessage("Other Services Created Successfully.");
            }catch(Exception $e){
            return redirect()->back()->withErrors($e->getMessage());
           }
    }
    public function editpaper(Request $request, $id){
    try{
       $paper = PaperTypes::query()->where(['id' => $id])->first();
        // dd($envelope);
       return view('admin.paper.update_paper',compact('paper'));
      }catch(Exception $e){
        return redirect()->back()
        ->withErrors($e->getMessage());
      }
    }
     public function updatepaper(Request $request){
    try{

        $rules = array(
           // 'name' => 'required|regex:/^[\pL\s\-]+$/u',
            'name' => 'required',
            'price' => 'required',
            'order_by'=>'required'
            // 'status'=>'',

            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
            {

            return redirect()->back()->withErrors($validator)->withInput();
            }

            $paper = PaperTypes::findOrfail($request->id);
            $paper->name = $request->name;
            $paper->price = $request->price;
            $paper->status = $request->status;
            $paper->order_by = $request->order_by;
            $paper->save();
        // dd($excercise);

        return Redirect::to('admin/paper-list')->withMessage("Other Services Updated Successfully.");
        }catch(Exception $e){
        return redirect()->back()->withErrors($e->getMessage());
       }
     }
    public function deletepaper($id)
    {
    $paper = PaperTypes::findOrfail($id);
    $paper->delete();
    $message = "Other Services Deleted Successfully!";
        return redirect()->back()->withMessage($message);
    }
}
