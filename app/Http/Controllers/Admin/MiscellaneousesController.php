<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Miscellaneous;
use Validator;
use Session;
use Redirect;
use App\Models\Setting;
use App\Models\Order;
use App\Models\OrderItem;
use App\User;
use DB;
use App\Models\Contacts;
use Mail;
use App\Models\EmailTemplate;
class MiscellaneousesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mis =Miscellaneous::all();
        return view('admin.miscellaneouses.index')->with('mis', $mis);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // dd(3434);
        return view('admin.miscellaneouses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $validation = Validator::make($request->all(), [
            'name' => 'required|regex:/^[\pL\s\-]+$/u',
            'price'=>'required'
        ]);
        if($validation->fails()){
            // dd($validation->errors()->first());
            Session::flash('message',$validation->errors()->first());
            return Redirect::back()->withInput();
        }
        $input = $request->all();
        // dd($input);
        $mis = Miscellaneous::create($input);
        if($mis){
            Session::flash('message','miscellaneous added successfully');
            return Redirect::to('admin/miscellaneous');
        }else {
            Session::flash('message','miscellaneous added successfully');
            return Redirect::to('admin/miscellaneous');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // dd($id);
        $mis = Miscellaneous::find($id);
        return view('admin.miscellaneouses.edit')->with('mis',$mis);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(), [
            'name' => 'required|regex:/^[\pL\s\-]+$/u',
            'price'=>'required'
        ]);
        if($validation->fails()){
            // dd($validation->errors()->first());
            Session::flash('message',$validation->errors()->first());
            return Redirect::back()->withInput();
        }
        $mis = Miscellaneous::find($id);
        $mis->name = $request->name;
        $mis->price = $request->price;
        $mis->update();

        Session::flash('message','miscellaneous updated successfully');
            return Redirect::to('admin/miscellaneous');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dd($id);
        $mis = Miscellaneous::find($id);
            $mis->delete();
            Session::flash('error','miscellaneous deleted successfully');
            return Redirect::to('admin/miscellaneous');

    }

    public function settings()
    {
        $setting = Setting::all();
        return view('admin.miscellaneouses.setting')->with('setting', $setting);
    }

    public function editSetting($id)
    {
        // dd($id);
        $setting = Setting::find($id);
        return view('admin.miscellaneouses.setting_edit')->with('setting',$setting);
    }

    public function updateSetting(Request $request, $id)
    {
        $validation = Validator::make($request->all(), [
            'name' => 'required|regex:/^[\pL\s\-]+$/u',
            'pending_order_days_reminder'=>'required'
        ]);
        if($validation->fails()){
            // dd($validation->errors()->first());
            Session::flash('message',$validation->errors()->first());
            return Redirect::back()->withInput();
        }
        $mis = Setting::find($id);
        $mis->name = $request->name;
        $mis->pending_order_days_reminder = $request->pending_order_days_reminder;
        $mis->update();

        Session::flash('message','Setting updated successfully');
            return Redirect::to('admin/setting');

    }

    public function sendReminderToVendor(){
        $forPostal = EmailTemplate::where('id', 7)->first();    
        $forGifthub = EmailTemplate::where('id', 8)->first();

        $forPostalMessage = !empty($forPostal->message) ? $forPostal->message : null;
        $forGifthubMessage = !empty($forGifthub->message) ? $forGifthub->message : null;

        $forPostalSubject = !empty($forPostal->subject) ? $forPostal->subject : null;
        $forGifthubSubject = !empty($forGifthub->subject) ? $forGifthub->subject : null;
        
        //Pending orders that passed the deadline ( ship by date ) and system should trigger merchant or printer by email to process this order ASAP.
        //if there is easy parcel order number exit in order table means order has been shipped. //No need to send reminder mail.
        $order = Order::get();
        //$order = Order::where('id', 70)->get();

        foreach($order as $order){

                $giftHubOrderItem = DB::table('order_item')->select('vendor_id')->where('admin_reminder_to_vendor', 0)->where('easy_parcel_order_no', '=', null)->where('order_id', $order->id)->where('section', 2)->groupBy('vendor_id')->get();
                $printerOrderItem = OrderItem::where('order_id', $order->id)->where('admin_reminder_to_vendor', 0)->where('easy_parcel_order_no', '=', null)->where('section', 1)->get();
                foreach($printerOrderItem as $printerOrderItem){
                    if(!empty($printerOrderItem)){
                        $postalLateDays = Setting::where('id', 1)->first();

                        $lateDate = date('Y-m-d', strtotime($order->created_at. ' +'.$postalLateDays->pending_order_days_reminder.' days'));

                        $todayDate = Date('Y-m-d');
                        if($lateDate < $todayDate){
                          //  dd('printer');
                            //send email to all "send_to" user's vendor
                            $sendUserIds = explode(',', $order->send_to);
                            foreach($sendUserIds as $receiverId){
                                //get receiver zip code
                                $recipient = Contacts::where('id', $receiverId)->first();
                                $vendor = User::where('zip_code', $recipient->zip_code)->where('user_type', 5)->first();
                                if(!empty($vendor->id)){
                                    if(!empty($vendor->email)){
                                        $emailData = [
                                            'name' => $vendor->name,
                                            'msg' => "Order number $order->order_number is delay to dispatch.",
                                            'msg_from_db' => $forPostalMessage,
                                        ];
                                        $to_email = $vendor->email;
                                        $to_name = $vendor->name;
                                        
                                        //$to_email = "anjay.webmobril@gmail.com";
                                      
                                        Mail::send('emails.order_delay', $emailData, function ($message) use ($to_name, $to_email, $forPostalSubject) {
                                            $message->from(env('MAIL_USERNAME'), env('MAIL_FROM_NAME'));
                                            $message->sender(env('MAIL_USERNAME'), $to_name);
                                            $message->to($to_email, $to_name);
                                            $message->subject('Order Delay ');
                                        });
                                        
                                    }
                                }
                            }
                            OrderItem::where('id', $printerOrderItem->id)->update([
                                'admin_reminder_to_vendor' => 1
                            ]);
                        }
                    }
                }
               
                foreach($giftHubOrderItem as $giftHubOrderItem){
                    if(!empty($giftHubOrderItem->vendor_id)){
                        $item = DB::table('order_item')->select('*')->where('admin_reminder_to_vendor', 0)->where('easy_parcel_order_no', '=', null)->where('order_id', $order->id)->where('section', 2)->first();

                        $gifthubLateDays = Setting::where('id', 2)->first();

                        $lateDate = date('Y-m-d', strtotime($order->created_at. ' +'.$gifthubLateDays->pending_order_days_reminder.' days'));
                        $todayDate = Date('Y-m-d');
                        if($lateDate < $todayDate){
                           // dd('gifthub');
                                //get vendor email address
                                $vendor = User::where('id', $giftHubOrderItem->vendor_id)->first();
                                if(!empty($vendor->email)){
                                    //send order dely emails
                                    $emailData = [
                                        'name' => $vendor->name,
                                        'msg' => "Order number $order->order_number is delayed to dispatch.",
                                        'msg_from_db' => $forGifthubMessage,
                                    ];
                                    
                                    $to_email = $vendor->email;
                                    //$to_email = "anjay.webmobril@gmail.com";
                                    $to_name = $vendor->name;
                                    Mail::send('emails.order_delay', $emailData, function ($message) use ($to_name, $to_email, $forGifthubSubject) {
                                        $message->from(env('MAIL_USERNAME'), env('MAIL_FROM_NAME'));
                                        $message->sender(env('MAIL_USERNAME'), $to_name);
                                        $message->to($to_email, $to_name);
                                        $message->subject('Order Delayed ');
                                    });
                                }
                                

                            OrderItem::where('id', $item->id)->update([
                                'admin_reminder_to_vendor' => 1
                            ]);

                        }
                    }
                }



        }


    }

    public function emailTemplateIndex()
    {
        $mis =EmailTemplate::all();
        return view('admin.emailtemplate.index')->with('mis', $mis);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function emailTemplateView()
    {
        // dd(3434);
        return view('admin.emailtemplate.create');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editMailTemplate($id)
    {
        // dd($id);
        $mis = EmailTemplate::find($id);

        return view('admin.emailtemplate.edit')->with('mis',$mis);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateMailTemplate(Request $request, $id)
    {
        $validation = Validator::make($request->all(), [
            'subject' => 'required',
            'message'=>'required'
        ]);
        if($validation->fails()){
            // dd($validation->errors()->first());
            Session::flash('message',$validation->errors()->first());
            return Redirect::back()->withInput();
        }
        $mis = EmailTemplate::find($id);
        $mis->subject = $request->subject;
        $mis->message = $request->message;
        $mis->update();

        Session::flash('message','Email Template updated successfully');
            return Redirect::to('admin/emailTemplateIndex');

    }
}
