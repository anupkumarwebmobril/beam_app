<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Helper;
use Session;
use Redirect;
use Mail;
use App\Models\ImportPostal;
use App\Models\UserDesignerRequest;

use Carbon\Carbon;

use App\User;


class DesignerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd(4334);
        try{

            $users = User::where('user_type',3)->get();
            //dd($users);

            //dd($usersubs);
            //$nurses=User::where('user_type',3)->get();

            /* foreach ($usersubs as $key => $value)
            {
                if(!empty($value->plan_id))
                {
                    $plan = MembershipPlan::where('id', $value->plan_id)->first();
                    if(!empty($plan->plan_name)){
                        $usersubs[$key]->plan_name = $plan->plan_name;
                    }
                }
            } */
            //return $nurses;
            return view('admin.designer.user_list',compact('users'));

        }catch(Exception $e){

            return redirect()->back()
                ->withErrors($e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $postalCodes = ImportPostal::get();

        return view('admin.designer.add_user')->with('postalCodes',$postalCodes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $emailTemplate = Helper::getEmailTemplate();
      //  dd(!empty($emailTemplate[0]->subject) ? $emailTemplate[0]->subject : null);
       try {
        $validation = Validator::make($request->all(), [
            //'name' => 'required|regex:/^[\pL\s\-]+$/u',
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'experience'=> 'required',
            'designer_charge'=> 'required',
            'profile_image'=> 'mimes:jpeg,jpg,png,gif|required|max:10000',
            'sample_design_image'=> 'mimes:jpeg,jpg,png,gif|required|max:10000',
            'country_code'=> 'required',
            // 'sample_design_image_2'=> 'mimes:jpeg,jpg,png,gif|required|max:10000',
            // 'sample_design_image_3'=> 'mimes:jpeg,jpg,png,gif|required|max:10000',
            //'zip_code' => 'required',

        ]);
        if($validation->fails()){
            Session::flash ('message', $validation->errors()->first());
            return Redirect::back()->withInput();

        }

        $data = $request->all();

        if(!empty($data['phone']) && $data['phone'][0] == 0){
            Session::flash('message','Phone number can not start with zero.');
            return Redirect::back();
        }

        $data['phone'] = $data['country_code'].$data['phone'];
        $data['contact_number_1'] = $data['country_code_1'].$data['contact_number_1'];

        //check duplicate phone.
        if(!empty($data['phone'])){
            $checkDuplicatePhone = User::where('phone', $data['phone'])->count();
            if ($checkDuplicatePhone > 0) {
                Session::flash('message','Phone number has already taken.');
                return Redirect::back();
            }
        }

        if ($request->hasFile('profile_image')) {
            $imageName = Helper::addDesignerImage($request, $data);
            if(!empty($imageName)){
                $data['profile_image'] = "$imageName";
            }
        }
        if ($request->hasFile('sample_design_image')) {
             $imageName = Helper::addsampleImage($request, $data);
             if(!empty($imageName)){
                 $data['sample_design_image'] = "$imageName";
             }
         }
         if ($request->hasFile('sample_design_image_2')) {
             $imageName = Helper::addsampleImage_2($request, $data);
             if(!empty($imageName)){
                 $data['sample_design_image_2'] = "$imageName";
             }
         }
         if ($request->hasFile('sample_design_image_3')) {
             $imageName = Helper::addsampleImage_3($request, $data);
             if(!empty($imageName)){
                 $data['sample_design_image_3'] = "$imageName";
             }
         }

        $data['user_type'] = 3;

        $designer = User::create($data);

        if($designer){
            $getrowdata = User::where('id',$designer->id)->first();
            if ($getrowdata) {
                $input_arr = array(
                    'name' => $designer->name,
                    'email' => $designer->email,
                    'contactNo' => $designer->phone,
                    'message' => $designer->name ." Admin Add you.",
                    'subject' => !empty($emailTemplate[0]->subject) ? $emailTemplate[0]->subject : null
                );
                $message = "Hii ".!empty($emailTemplate[0]->message) ? $emailTemplate[0]->message : null;
                $input_arr['msg'] = $message;
                Mail::send('emails.Product', $input_arr, function ($message) use ($input_arr) {
                    $message->to($input_arr['email'], 'admin')
                        ->subject($input_arr['subject']);
                    $message->from('support@letsbeam.it', 'Beam App');
                });
            }

        }

        Session::flash('message','Designer added Successfully');
        return Redirect::to('admin/designer-list');

       } catch (\Exception $e) {
        Session::flash('message',$e->getMessage());
        return Redirect::back();
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user =User::find($id);
        $postalCodes = ImportPostal::get();
        return view('admin.designer.update_user')->with('user', $user)->with('postalCodes', $postalCodes);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request)
    {
        // dd(3434);
        try {
            // dd($request->all());
            $validation = Validator::make($request->all(), [
               // 'name' => 'required|regex:/^[\pL\s\-]+$/u',
                'name' => 'required',
                'email' => 'required|email',
                'phone' => 'required',

            ]);
            if($validation->fails()){
                Session::flash ('message', $validation->errors()->first());
                return Redirect::back();
            }
            $data = $request->all();
            $user = User::find($request->id);
            // dd($user);

            if($request->hasFile('profile_image')){
                $imageName = Helper::addDesignerImage($request, $data);
                // dd($imageName);
                if(!empty($imageName)){
                    // dd(34);
                    $data['profile_image'] = "$imageName";
                }
            }else{
                $data['profile_image'] = $user->profile_image;
            }
            if($request->hasFile('sample_design_image')){
                // dd(344);
                $imageName = Helper::addsampleImage($request, $data);
                if(!empty($imageName)){
                    $data['sample_design_image'] = "$imageName";
                }
            }else{
                $data['sample_design_image'] = $user->sample_design_image;
            }
            if($request->hasFile('sample_design_image_2')){
                $imageName = Helper::addsampleImage_2($request, $data);
                if(!empty($imageName)){
                    $data['sample_design_image_2'] = "$imageName";
                }
            }else{
                $data['sample_design_image_2'] = $user->sample_design_image_2;
            }
            if($request->hasFile('sample_design_image_3')){
                $imageName = Helper::addsampleImage_3($request, $data);
                if(!empty($imageName)){
                    $data['sample_design_image_3'] = "$imageName";
                }
            }else{
                $data['sample_design_image_3'] = $user->sample_design_image_3;
            }

            $data['phone'] = $data['country_code'].$data['phone'];
            $data['contact_number_1'] = $data['country_code_1'].$data['contact_number_1'];

            $update = $user->update($data);

            if($update){
                Session::flash('message','Designer updated successfully');
                return redirect('admin/designer-list');
            }else{
                Session::flash('message','oops something went wrong');
                return Redirect::back();
            }
        } catch (\Exception $e) {
            Session::flash('message',$e->getMessage());
            return Redirect::back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if($user){
            $delete = $user->delete();
            Session::flash('message','Designer Deleted Successfully');
            return redirect()->back();
        }else{
            Session::flash('message','oops something went wrong');
            return redirect()->back();
        }
    }
    public function DesginerRequest(){
        // dd(4545);
       // $lastMonth = Carbon::now()->startOfMonth()->subDays(1);
        $data = UserDesignerRequest::select('users.name','user_designer_request.*')
        ->leftjoin('users','users.id','=','user_designer_request.from_user_id')
        // ->leftjoin('users','users.id','=','user_designer_request.to_user_id')
        //->whereDate('create_at', '>', $lastMonth)
        //->groupBy(['from_user_id', 'to_user_id'])
        ->get();
        $finalArr = [];
        foreach ($data as $key => $value){
            $finalArr[$value->from_user_id.'-'.$value->to_user_id] = $value;
            // echo "<pre>";
            // print_r($value);
            // echo "<pre>";
        }
        //dd($finalArr);
       $data = $finalArr;
        return view('admin.desgner-request.index',compact('data'));
    }
}
