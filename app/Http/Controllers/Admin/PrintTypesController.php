<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Exception;
use App\User;
use Validator;
use App\Models\PrintTypes;

class PrintTypesController extends Controller
{
    public function index()
    {
        $printtype = PrintTypes::orderBy('id','desc')->get();
        return view('admin.printtype.printtype_list')->withPrinttype($printtype);

    }
      public function printtypeForm ()
    {
        return view('admin.printtype.add_printtype');
    }
    public function addprinttype(Request $request)
    {
        // print_r($request->all()); die();
        try{

            $rules = array(
            'name' => 'required',
            'price' => 'required',
            // 'status'=>'',
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
            {

            return redirect()->back()->withErrors($validator)->withInput();
            }

            $printtype = new PrintTypes();
            $printtype->name = $request->name;
            $printtype->price = $request->price;
            $printtype->status = 1;

            $printtype->save();
            // dd($excercise);
            return Redirect::to('admin/printtype-list')->withMessage("Print Types Created Successfully.");
            }catch(Exception $e){
            return redirect()->back()->withErrors($e->getMessage());
           }
    }
    public function editprinttype(Request $request, $id){
    try{
       $printtype = PrintTypes::query()->where(['id' => $id])->first();
        // dd($envelope);
       return view('admin.printtype.update_printtype',compact('printtype'));
      }catch(Exception $e){
        return redirect()->back()
        ->withErrors($e->getMessage());
      }
    }
     public function updateprinttype(Request $request){
    try{

        $rules = array(
            'name' => 'required',
            'price' => 'required',
            // 'status'=>'',

            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
            {

            return redirect()->back()->withErrors($validator)->withInput();
            }

            $printtype = PrintTypes::findOrfail($request->id);
            $printtype->name = $request->name;
            $printtype->price = $request->price;
            $printtype->status = 1;
            $printtype->save();
        // dd($excercise);

        return Redirect::to('admin/printtype-list')->withMessage("Print type Updated Successfully.");
        }catch(Exception $e){
        return redirect()->back()->withErrors($e->getMessage());
       }
     }
    public function deleteprinttype($id)
    {
    $printtype = PrintTypes::findOrfail($id);
    $printtype->delete();
    $message = "Print type Deleted Successfully!";
        return redirect()->back()->withMessage($message);
    }
}
