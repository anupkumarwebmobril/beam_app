<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Exception;
use App\User;
use App\Helpers\Helper;
use App\Models\ImportPostal;
use App\Models\PostalStates;
use App\Models\OrderItem;
use App\Models\Order;
use Carbon\Carbon;
use DB;
use App\Models\UserDesignerRequest;
use App\Models\Product;
use App\Models\PostCard;
use App\Models\EmailTemplate;

class VendorAuthController extends Controller
{
    public function index(){

        try{
            $todayDate = Date('Y-m-d');
            $authUser = Auth::user();
            $gifthubTopTenSellingProducts = [];
            if($authUser->user_type == 4){
                $gifthub = OrderItem::query()->where('vendor_id', $authUser->id)->where('section', 2)->whereDate('created_at', Carbon::today())->count();

                //Top 10 selling gifthub.
                $gifthubTopTenSellingProduct = OrderItem::select('product_id', DB::raw('count(*) as total_product_sale'))->where('vendor_id', $authUser->id)->where('section', 2)->whereNotNull('product_id')->groupBy('product_id')->limit(10)->orderBy('total_product_sale', 'DESC')->get();
                foreach($gifthubTopTenSellingProduct as $row){
                    $product = Product::where('id', $row->product_id)->withTrashed()->first();
                    if(!empty($product->name)){
                        $gifthubTopTenSellingProducts[] = array($product->name, $row->total_product_sale);
                    }
                }

                return view('admin.admin_dashboard_for_vendor', [
                    'gifthub' => $gifthub,
                    'gifthubTopTenSellingProducts' => $gifthubTopTenSellingProducts,
                ]);
            } else if($authUser->user_type == 5){

                $postcard = OrderItem::query()->where('product_type', 1)->whereDate('created_at', Carbon::today())
                    ->whereRaw('FIND_IN_SET(?, multi_vendor_id)', [Auth::user()->id])
                    ->count();

                $greetings = OrderItem::query()->where('section', 1)->whereNotIn('product_type', [1,3])
                    ->whereDate('created_at', Carbon::today())
                    ->whereRaw('FIND_IN_SET(?, multi_vendor_id)', [Auth::user()->id])
                    ->count();

                return view('admin.admin_dashboard_for_vendor', [
                    'postcard' => $postcard,
                    'greetings' => $greetings,

                    ]);
            } else if($authUser->user_type == 6){
                $postal_mails = OrderItem::query()->where('product_type', 3)
                    ->whereDate('created_at', Carbon::today())
                    ->whereRaw('FIND_IN_SET(?, multi_vendor_id)', [Auth::user()->id])
                    ->count();

                return view('admin.admin_dashboard_for_vendor', [
                    'postal_mails' => $postal_mails,

                    ]);
            } else {
                return view('admin.admin_dashboard_for_vendor', [

                    ]);
            }

        }catch(Exception $e){

            return redirect()->back()
                ->withErrors($e->getMessage());
        }
    }
    // sagar code
    public function all_product_count()
    {
       try {
            $now = Carbon::now();
            $lastWeek = $now->startOfWeek(Carbon::SUNDAY);
            $lastMonth = Carbon::now()->startOfMonth()->subDays(1);

            //$lastWeek = Carbon::now()->subWeek();
            //$lastMonth = Carbon::now()->subDays(30);

            $todayDate = Date('Y-m-d');
            //today total amount
            $authUser = Auth::user();
            if($authUser->user_type == 5){
                $greetings_monthly_product = OrderItem::where('section', 1)
                                        ->whereNotIn('product_type', [1,3])->whereDate('created_at', '>', $lastMonth)
                                        ->get();


                $greetings_monthly_amount = $this->returnCommonOrderAmountbydays($greetings_monthly_product);

                return response()->json(array('status' => true,  'data' => $greetings_monthly_amount));
            }
            if($authUser->user_type == 6){
                $greetings_monthly_product = OrderItem::where('section', 1)
                                            ->whereNotIn('product_type', [1,2])->whereDate('created_at', '>', $lastMonth)
                                            ->get();

                $greetings_monthly_amount = $this->returnCommonOrderAmountbydays($greetings_monthly_product);

            return response()->json(array('status' => true,  'data' => $greetings_monthly_amount));
            }

       } catch (\Exception $e) {

            return response()->json(array('status' => false,  'data' => $e->getMessage()));

       }
    }
    public function postcard()
    {
       try {
        $now = Carbon::now();
        $lastWeek = $now->startOfWeek(Carbon::SUNDAY);
        $lastMonth = Carbon::now()->startOfMonth()->subDays(1);

        //$lastWeek = Carbon::now()->subWeek();
        //$lastMonth = Carbon::now()->subDays(30);
        $todayDate = Date('Y-m-d');
        //today total amount
        $authUser = Auth::user();
        $returnArry = [];
        if($authUser->user_type == 5){

            $postcard = OrderItem::whereNotIn('product_type', [3,2])->where('product_type',1)->whereDate('created_at', '>', $lastMonth)
                                        ->whereRaw('FIND_IN_SET(?, multi_vendor_id)', [Auth::user()->id])
                                        ->get();
            $returnArry['postcard'] = $this->returnCommonOrderAmountbydays($postcard);


            $greeting = OrderItem::where('section', 1)
                                        ->whereNotIn('product_type', [1,3])->whereDate('created_at', '>', $lastMonth)
                                        ->whereRaw('FIND_IN_SET(?, multi_vendor_id)', [Auth::user()->id])
                                        ->get();

            $returnArry['greeting'] = $this->returnCommonOrderAmountbydays($greeting);

        }

        if($authUser->user_type == 6){
            //get secure printer data for email attachment.
            $postcard = OrderItem::where('product_type',3)->whereDate('created_at', '>', $lastMonth)
                                        ->whereRaw('FIND_IN_SET(?, multi_vendor_id)', [Auth::user()->id])
                                        ->get();
            $returnArry['securePrinterMonthlyReport'] = $this->returnCommonOrderAmountbydays($postcard);

        }

        return response()->json(array('status' => true,  'data' => $returnArry));
       } catch (\Exception $e) {
            return response()->json(array('status' => false,  'data' => $e->getMessage()));

       }
    }
    public function returnCommonOrderAmountbydays($orderItems)
    {

        $orderItemTotal = 0;
        $returnArray = [];
        $authUser = Auth::user();
        $aa[] =  array('Date', 'Qty', 'Sell');


        foreach($orderItems as $items){

            $order = Order::where('id', $items->order_id)->first();
            $recipientsCount = 0;

            if(!empty($order->send_to)){
                    $recipientsCount = explode(',', $order->send_to);
                    $recipientsCount = count($recipientsCount);

                    $date = date('Y-m-d', strtotime($items->created_at));
                    if(empty($returnArray[$date]['sale'])){
                        $returnArray[$date]['sale'] = 0;
                        $returnArray[$date]['itemqty'] = 0;
                    }

                    $temTotal = ($items->quantity * $items->price) * $recipientsCount;

                        $returnArray[$date]['sale'] = ($returnArray[$date]['sale'] + $temTotal);
                        $returnArray[$date]['itemqty']++;
            }else{
                $return = [
                    'printer' => $aa,

                ];
            }
        }
        //dd($returnArray);
        $return = [];
        if(count($returnArray) > 0){
            foreach($returnArray as $key => $dd){
                $aa[] = array($key, $returnArray[$key]['itemqty'], $returnArray[$key]['sale']);
            }
            $return = [
                'printer' => $aa,

            ];
        }else{
            $return = [
                'printer' => $aa,

            ];
        }
        return($return);

    }

    public function weeklyOrderTotal(){
        try {
           // $lastWeek = Carbon::now()->subWeek();
           // $lastMonth = Carbon::now()->subDays(30);

            $now = Carbon::now();
            $lastWeek = $now->startOfWeek(Carbon::SUNDAY);
            // dd($lastWeek);
            $lastMonth = Carbon::now()->startOfMonth()->subDays(1);


            $todayDate = Date('Y-m-d');
            //today total amount
            $authUser = Auth::user();
            if($authUser->user_type == 4){
                $gifthub_amount_today = OrderItem::select(DB::raw('sum(item_grand_total_price) as total'))->where('vendor_id', $authUser->id)->where('section', 2)->whereDate('created_at', Carbon::today())->first();
                $gifthub_amount_today = ($gifthub_amount_today->total > 0) ? round($gifthub_amount_today->total, 2) : 0;

                $gifthub_weekly = OrderItem::query()->where('vendor_id', $authUser->id)->where('section', 2)->whereDate('created_at', '>', $lastWeek)->count();

                $gifthub_weekly_amount = OrderItem::select(DB::raw('sum(item_grand_total_price) as total'))->where('vendor_id', $authUser->id)->where('section', 2)->whereDate('created_at', '>', $lastWeek)->first();
                $gifthub_weekly_amount = ($gifthub_weekly_amount->total > 0) ? round($gifthub_weekly_amount->total, 2) : 0;

                $gifthub_monthly = OrderItem::query()->where('vendor_id', $authUser->id)->where('section', 2)->whereDate('created_at', '>', $lastMonth)->count();

                $gifthub_monthly_amount = OrderItem::select(DB::raw('sum(item_grand_total_price) as total'))->where('vendor_id', $authUser->id)->where('section', 2)->whereDate('created_at', '>', $lastMonth)->first();
                $gifthub_monthly_amount = ($gifthub_monthly_amount->total > 0) ? round($gifthub_monthly_amount->total, 2) : 0;

                $total_order_recieved_till_date = OrderItem::query()->where('vendor_id', $authUser->id)->where('section', 2)->count();



            }

            if($authUser->user_type == 6){
                $postal_mails_product_today = OrderItem::where('product_type', 3)->whereDate('created_at', Carbon::today())->whereRaw('FIND_IN_SET(?, multi_vendor_id)', [Auth::user()->id])->get();
                $postal_mails_amount_today = $this->returnCommonOrderAmount($postal_mails_product_today);

                $all_postal_mails = OrderItem::where('product_type', 3)->whereDate('created_at', '>', $lastWeek)->whereRaw('FIND_IN_SET(?, multi_vendor_id)', [Auth::user()->id])->get();

                $postal_mails_weekly_amount = $this->returnCommonOrderAmount($all_postal_mails);

                $postal_mails_weekly = OrderItem::query()->where('product_type', 3)->whereDate('created_at', '>', $lastWeek)->whereRaw('FIND_IN_SET(?, multi_vendor_id)', [Auth::user()->id])->get();
                $postal_mails_weekly = $this->returnCommonOrderCount($postal_mails_weekly);
                //dd($postal_mails_weekly);

                $postal_mails_monthly = OrderItem::query()->where('product_type', 3)->whereDate('created_at', '>', $lastMonth)->whereRaw('FIND_IN_SET(?, multi_vendor_id)', [Auth::user()->id])->get();
                $postal_mails_monthly = $this->returnCommonOrderCount($postal_mails_monthly);

                $postal_mails_monthly_product = OrderItem::where('product_type', 3)->whereDate('created_at', '>', $lastMonth)->whereRaw('FIND_IN_SET(?, multi_vendor_id)', [Auth::user()->id])->get();
                $postal_mails_monthly_amount = $this->returnCommonOrderAmount($postal_mails_monthly_product);

            }

            if($authUser->user_type == 5){
                $allPpostcard = OrderItem::where('product_type', 1)->whereDate('created_at', Carbon::today())->whereRaw('FIND_IN_SET(?, multi_vendor_id)', [Auth::user()->id])->get();
                $postcard_amount_today = $this->returnCommonOrderAmount($allPpostcard);

                $greetingsAllProducts = OrderItem::where('section', 1)->whereNotIn('product_type', [1,3])->whereDate('created_at', Carbon::today())->whereRaw('FIND_IN_SET(?, multi_vendor_id)', [Auth::user()->id])->get();
                $greetings_amount_today = $this->returnCommonOrderAmount($greetingsAllProducts);

                //Weekly count
                $postcard_weekly = OrderItem::query()->where('product_type', 1)->whereDate('created_at', '>', $lastWeek)->whereRaw('FIND_IN_SET(?, multi_vendor_id)', [Auth::user()->id])->count();
                // $postcard_weekly = $this->returnCommonOrderCount($postcard_weekly);
                $greetings_weekly = OrderItem::query()->where('section', 1)->whereNotIn('product_type', [1,3])->whereDate('created_at', '>', $lastWeek)->whereRaw('FIND_IN_SET(?, multi_vendor_id)', [Auth::user()->id])->count();
                // $greetings_weekly = $this->returnCommonOrderCount($greetings_weekly);

                //weekly total
                $all_postcard_weekly_product = OrderItem::where('product_type', 1)->whereDate('created_at', '>', $lastWeek)->whereRaw('FIND_IN_SET(?, multi_vendor_id)', [Auth::user()->id])->get();
                $postcard_weekly_amount = $this->returnCommonOrderAmount($all_postcard_weekly_product);

                $greetings_weekly_product = OrderItem::where('section', 1)->whereNotIn('product_type', [1,3])->whereDate('created_at', '>', $lastWeek)->whereRaw('FIND_IN_SET(?, multi_vendor_id)', [Auth::user()->id])->get();
                $greetings_weekly_amount = $this->returnCommonOrderAmount($greetings_weekly_product);

                //Monthly count
                $postcard_monthly = OrderItem::query()->where('product_type', 1)->whereDate('created_at', '>', $lastMonth)->whereRaw('FIND_IN_SET(?, multi_vendor_id)', [Auth::user()->id])->count();
                // dd($postcard_monthly);
                // $postcard_monthly = $this->returnCommonOrderCount($postcard_monthly);
                $greetings_monthly = OrderItem::query()->where('section', 1)->whereNotIn('product_type', [1,3])->whereDate('created_at', '>', $lastMonth)->whereRaw('FIND_IN_SET(?, multi_vendor_id)', [Auth::user()->id])->count();

                //    $greetings_monthly = $this->returnCommonOrderCount($greetings_monthly);
                //  dd($greetings_monthly);
                //Monthly total
                $postcard_monthly_product = OrderItem::where('product_type', 1)->whereDate('created_at', '>', $lastMonth)->whereRaw('FIND_IN_SET(?, multi_vendor_id)', [Auth::user()->id])->get();
                $postcard_monthly_amount = $this->returnCommonOrderAmount($postcard_monthly_product);

                $greetings_monthly_product = OrderItem::where('section', 1)->whereNotIn('product_type', [1,3])->whereDate('created_at', '>', $lastMonth)->whereRaw('FIND_IN_SET(?, multi_vendor_id)', [Auth::user()->id])->get();
                $greetings_monthly_amount = $this->returnCommonOrderAmount($greetings_monthly_product);

                $monthly_total_product = OrderItem::whereDate('created_at', '>', $lastMonth)->whereRaw('FIND_IN_SET(?, multi_vendor_id)', [Auth::user()->id])->get();
                $monthly_total_amount = $this->returnCommonOrderAmount($monthly_total_product);
                //dd($amount[0]['total']);

                // $designRequest = UserDesignerRequest::query()->whereDate('create_at', '>', $lastMonth)->count();
                $designRequest = UserDesignerRequest::count();

            }

            if($authUser->user_type == 4){
                $return = [
                    'gifthub_weekly' => $gifthub_weekly,
                    'gifthub_amount_today' => $gifthub_amount_today,
                    'gifthub_weekly_amount' => $gifthub_weekly_amount,
                    'gifthub_monthly' => $gifthub_monthly,
                    'gifthub_monthly_amount' => $gifthub_monthly_amount,
                    'total_order_recieved_till_date' => $total_order_recieved_till_date
                ];
            }

            if($authUser->user_type == 6){
                $return = [
                    'postal_mails_weekly' => $postal_mails_weekly,
                    'postal_mails_weekly_amount' => $postal_mails_weekly_amount,
                    'postal_mails_amount_today' => $postal_mails_amount_today,
                    'postal_mails_monthly' => $postal_mails_monthly,
                    'postal_mails_monthly_amount' => $postal_mails_monthly_amount,
                ];
            }

            if($authUser->user_type == 5){
                $return = [
                    'postcard_weekly' => $postcard_weekly,
                    'postcard_weekly_amount' => $postcard_weekly_amount,
                    'greetings_weekly' => $greetings_weekly,
                    'greetings_weekly_amount' => $greetings_weekly_amount,
                    'postcard_amount_today' => $postcard_amount_today,
                    'greetings_amount_today' => $greetings_amount_today,

                    //Monthly

                    'postcard_monthly' => $postcard_monthly,

                    'greetings_monthly' => $greetings_monthly,


                    'postcard_monthly_amount' => $postcard_monthly_amount,

                    'greetings_monthly_amount' => $greetings_monthly_amount,

                    'monthly_total_amount' => $monthly_total_amount,

                    'design_request' => $designRequest,

                ];
            }
            return response()->json(array('status' => true,  'data' => $return));

        } catch (\Exception $e) {
            return response()->json(array('status' => false, 'msg'=> $e->getMessage()));
        }
    }

    public function returnCommonOrderCount($orderItems){
        $orderItemBasedZipCode = [];
        foreach($orderItems as $items){
            $recepientUsers = Helper::checkVendorAndUserZipcode($items);

            if(!empty($recepientUsers['recipientIds'][0])){
                $items->recipientIds = $recepientUsers;
                $orderItemBasedZipCode[] = $items;
            }
        }
        return count($orderItemBasedZipCode);
    }

    public function returnCommonOrderAmount($orderItems){
        $orderItemTotal = 0;

        foreach($orderItems as $items){
            $order = Order::where('id', $items->order_id)->first();
            $recipientsCount = 0;
            if(!empty($order->send_to)){
                $recipientsCount = explode(',', $order->send_to);
                $recipientsCount = count($recipientsCount);

            }

            $temTotal = ($items->quantity * $items->price) * $recipientsCount;
            $orderItemTotal = (number_format($orderItemTotal) + number_format($temTotal));
        }

        return round($orderItemTotal, 2);
    }

    public function orderData(){
        try {
            $now = Carbon::now();
            $lastWeek = $now->startOfWeek(Carbon::SUNDAY);
            $lastMonth = Carbon::now()->startOfMonth()->subDays(1);

            //$lastWeek = Carbon::now()->subWeek();
            //$lastMonth = Carbon::now()->subDays(30);


            $todayDate = Date('Y-m-d');
            $authUser = Auth::user();

            $notIncludingStatusArray = array(
                'Cancel',
                'Returned',
                'Successfully Delivered',
                'Pending For Collection'
            );

            //Order shipped and waiting

            if($authUser->user_type == 4){
                $totalORderItem = OrderItem::query()->where('vendor_id', $authUser->id)->whereNotNull('easy_parcel_order_no')->get();

                $totalPedningOrder = OrderItem::
                    whereNull('easy_parcel_order_no')
                    ->whereNotIn('status', [2,3])
                    ->where('section', 2)
                    ->where('vendor_id', $authUser->id)
                    ->count();
            }

            if($authUser->user_type == 5){
                $totalORderItem = OrderItem::query()->where('section', 1)->where('product_type', '!=', 3)->whereNotNull('easy_parcel_order_no')->whereRaw('FIND_IN_SET(?, multi_vendor_id)', [Auth::user()->id])->get();

                $totalPedningOrder = OrderItem::
                    whereNull('easy_parcel_order_no')
                    ->whereNotIn('status', [2,3])
                    ->where('section', 1)
                    ->where('product_type', '!=', 3)
                    ->whereRaw('FIND_IN_SET(?, multi_vendor_id)', [Auth::user()->id])
                    ->count();


                    /* $totalPedningOrder = 0;
                    foreach($totalPedningOrderList as $orderItem){
                        $recepientUsers = Helper::checkVendorAndUserZipcode($orderItem);

                        if(!empty($recepientUsers['recipientIds'][0])){
                            $easyParcelStatus = Helper::getEasyParcelOrderStatus($orderItem);
                            if(!empty($easyParcelStatus->result[0]->parcel[0]->ship_status)){
                                if(in_array($easyParcelStatus->result[0]->parcel[0]->ship_status, $notIncludingStatusArray)){
                                    //No need to count.
                                } else {
                                    $totalPedningOrder = $totalPedningOrder+1;
                                }
                            }
                        }
                    } */
            }

            if($authUser->user_type == 6){
                $totalORderItem = OrderItem::query()->where('product_type', '=', 3)->whereNotNull('easy_parcel_order_no')->get();

                $totalPedningOrder = OrderItem::
                    whereNull('easy_parcel_order_no')
                    ->whereNotIn('status', [2,3])
                    ->where('product_type', '=', 3)
                    ->whereRaw('FIND_IN_SET(?, multi_vendor_id)', [Auth::user()->id])
                    ->count();



                /*
                 $totalPedningOrder = 0;
                 foreach($totalPedningOrderList as $orderItem){
                    $recepientUsers = Helper::checkVendorAndUserZipcode($orderItem);

                    if(!empty($recepientUsers['recipientIds'][0])){
                        $easyParcelStatus = Helper::getEasyParcelOrderStatus($orderItem);
                        if(!empty($easyParcelStatus->result[0]->parcel[0]->ship_status)){
                            if(in_array($easyParcelStatus->result[0]->parcel[0]->ship_status, $notIncludingStatusArray)){
                                //No need to count.
                            } else {
                                $totalPedningOrder = $totalPedningOrder+1;
                            }
                        }
                    }
                } */

            }

            $totalORderShippedAndAwaitingForDelivery = 0;


            foreach($totalORderItem as $orderItem){
                if($authUser->user_type == 4){
                    $easyParcelStatus = Helper::getEasyParcelOrderStatus($orderItem);
                    if(!empty($easyParcelStatus->result[0]->latest_status)){
                        if(in_array($easyParcelStatus->result[0]->latest_status, $notIncludingStatusArray)){
                            //No need to count.
                        } else {
                            $totalORderShippedAndAwaitingForDelivery = $totalORderShippedAndAwaitingForDelivery+1;
                        }
                    }
                } else {
                        $easyParcelStatus = Helper::getEasyParcelOrderStatus($orderItem);
                        if(!empty($easyParcelStatus->result[0]->latest_status)){
                            if(in_array($easyParcelStatus->result[0]->latest_status, $notIncludingStatusArray)){
                                //No need to count.
                            } else {
                                $totalORderShippedAndAwaitingForDelivery = $totalORderShippedAndAwaitingForDelivery+1;
                            }
                        }

                }
            }

            //Successfully delivered
            $includingTotalORderDelivered = array(
                'Delivered',
            );
            $totalORderDelivered = 0;
            foreach($totalORderItem as $orderItem){
                if($authUser->user_type == 4){
                    $easyParcelStatus = Helper::getEasyParcelOrderStatus($orderItem);
                    // dd($easyParcelStatus->result[0]->latest_status);
                    if(!empty($easyParcelStatus->result[0]->latest_status)){
                        if(in_array($easyParcelStatus->result[0]->latest_status, $includingTotalORderDelivered)){
                            //No need to count.
                            $totalORderDelivered = $totalORderDelivered+1;
                        }
                    }
                } else {
                        $easyParcelStatus = Helper::getEasyParcelOrderStatus($orderItem);
                        if(!empty($easyParcelStatus->result[0]->latest_status)){
                            if(in_array($easyParcelStatus->result[0]->latest_status, $includingTotalORderDelivered)){
                                //No need to count.
                                $totalORderDelivered = $totalORderDelivered+1;
                            }
                        }

                }
            }

            //Total pending order.
            /* $includingPendingOrder = array(
                'Pending For Collection',
            );
            $totalPedningOrder = 0;
            foreach($totalORderItem as $orderItem){
                $easyParcelStatus = Helper::getEasyParcelOrderStatus($orderItem);
                if(!empty($easyParcelStatus->result[0]->parcel[0]->ship_status)){
                    if(in_array($easyParcelStatus->result[0]->parcel[0]->ship_status, $includingPendingOrder)){
                        //No need to count.
                        $totalPedningOrder = $totalPedningOrder+1;
                    }
                }
            } */


            //Return / Refund request.
            $includingReturnRefund = array(
                'Returned',
            );
            $totalReturnRefund = 0;
            foreach($totalORderItem as $orderItem){
                if($authUser->user_type == 4){
                    $easyParcelStatus = Helper::getEasyParcelOrderStatus($orderItem);
                    if(!empty($easyParcelStatus->result[0]->latest_status)){
                        if(in_array($easyParcelStatus->result[0]->latest_status, $includingReturnRefund)){
                            //No need to count.
                            $totalReturnRefund = $totalReturnRefund+1;
                        }
                    }
                } else {
                        $easyParcelStatus = Helper::getEasyParcelOrderStatus($orderItem);
                        if(!empty($easyParcelStatus->result[0]->latest_status)){
                            if(in_array($easyParcelStatus->result[0]->latest_status, $includingReturnRefund)){
                                //No need to count.
                                $totalReturnRefund = $totalReturnRefund+1;
                            }
                        }

                }
            }

            //Cancel order.
            $includingCancel = array(
                'Cancel',
                'Cancel By Admin'
            );
            $totalCancelOrder = 0;
            foreach($totalORderItem as $orderItem){
                if($authUser->user_type == 4){
                    $easyParcelStatus = Helper::getEasyParcelOrderStatus($orderItem);
                    if(!empty($easyParcelStatus->result[0]->latest_status)){
                        if(in_array($easyParcelStatus->result[0]->latest_status, $includingCancel)){
                            //No need to count.
                            $totalCancelOrder = $totalCancelOrder+1;
                        }
                    }
                } else {

                        $easyParcelStatus = Helper::getEasyParcelOrderStatus($orderItem);
                        if(!empty($easyParcelStatus->result[0]->latest_status)){
                            if(in_array($easyParcelStatus->result[0]->latest_status, $includingCancel)){
                                //No need to count.
                                $totalCancelOrder = $totalCancelOrder+1;
                            }
                        }
                }
            }


            $return = [
                'totalORderShippedAndAwaitingForDelivery' => $totalORderShippedAndAwaitingForDelivery,
                'totalORderDelivered' => $totalORderDelivered,
                'totalPedningOrder' => $totalPedningOrder,
                'totalReturnRefund' => $totalReturnRefund,
                'totalCancelOrder' => $totalCancelOrder,

            ];
            return response()->json(array('status' => true,  'data' => $return));

        } catch (\Exception $e) {
            return response()->json(array('status' => false, 'msg'=> $e->getMessage()));
        }
    }
    //sgar code for chart
     public function monthyreporter()
     {
       try {
                $lastdate = Carbon::now();
                $lastMonth = Carbon::now()->subDays(30);
                $todayDate = Date('Y-m-d');
                //today total amount
                $authUser = Auth::user();
            if($authUser->user_type == 4){

                $total_qty = [];
                $lastdate = Carbon::now()->subDays(30);
                $lastdate->format('Y-m-d');
                $todaydate = Carbon::now();
                $todaydate->format('Y-m-d');
                // //  top 10 selling products
                // $gifthubTopTenSellingProduct = OrderItem::select('product_id', DB::raw('count(*) as total_product_sale'))->where('vendor_id', $authUser->id)->where('section', 2)->whereNotNull('product_id')->groupBy('product_id')->limit(10)->orderBy('total_product_sale', 'DESC')->get();
                // foreach($gifthubTopTenSellingProduct as $row){
                //     $product = Product::where('id', $row->product_id)->withTrashed()->first();
                //     if(!empty($product->name)){
                //         $gifthubTopTenSellingProducts[] = array($product->name, $row->total_product_sale);
                //     }
                // }
                // dd($gifthubTopTenSellingProducts);

                $aa[] =  array('Date', 'Qty', 'Sell');
                for ($i=1; $i <= 30 ; $i++) {

                    $adddays = $lastdate->addDays(1);
                    $adddays->format('Y-m-d');
                    $orders = OrderItem::select(DB::raw('DATE(created_at) as date'),DB::raw('Count(quantity) as qty'))
                            ->where('vendor_id', $authUser->id)
                            ->where('section', 2)->whereDate('created_at','=',$adddays)
                            ->groupBy('date')
                            ->get();
                            // dd($orders);

                    $selling =  OrderItem::select(DB::raw('sum(item_grand_total_price) as total'),DB::raw('DATE(created_at) as date'))
                            ->where('vendor_id', $authUser->id)
                            ->where('section', 2)->whereDate('created_at', '=', $adddays)
                            ->groupBy('date')
                            ->get();
                            // dd($selling);

                            foreach($selling as $sell2){
                                foreach ($orders as $key => $value) {

                                 $aa[] = array($adddays->format('Y-m-d'), $value->qty, $sell2->total);

                                }

                            }

                    $total_qty[]['date'] = $adddays;
                    $total_qty[]['qty'] = $orders->count();
                }
            }

            if($authUser->user_type == 4){
                $return = [
                    'monthlyreport' => $aa,
                    // 'top_ten_product' => $gifthubTopTenSellingProducts,

                ];
                return response()->json(array('status' => true,  'data' => $return));
            }

       } catch (\Exception $e) {

        return response()->json(array('status' => false, 'msg'=> $e->getMessage()));

       }
     }

       //  for geeting in admin chart for 1 month day sells
    public function greeting_monthly()
    {
      try {
               $lastdate = Carbon::now();
               $lastMonth = Carbon::now()->subDays(30);
               $todayDate = Date('Y-m-d');
               //today total amount
               $authUser = Auth::user();

               $total_qty = [];
               $lastdate = Carbon::now()->subDays(30);
               $lastdate->format('Y-m-d');
               $todaydate = Carbon::now();
               $todaydate->format('Y-m-d');

               $gg[] = array('Date', 'Qty', 'Sell');

               for ($i=1; $i <= 30 ; $i++) {

                   $adddays = $lastdate->addDays(1);
                   $adddays->format('Y-m-d');


                   $greetings_orders = OrderItem::select(DB::raw('DATE(created_at) as date'),DB::raw('Count(quantity) as qty'))
                                       ->where('section', 1)->whereNotIn('product_type', [1,3])
                                       ->whereDate('created_at', '=', $adddays)
                                       ->groupBy('date')
                                      // ->whereRaw('FIND_IN_SET(?, multi_vendor_id)', [Auth::user()->id])
                                       ->get();
                                       // dd($greetings_orders);

                   $greetings_sell =  OrderItem::select(DB::raw('sum(item_grand_total_price) as total'),DB::raw('DATE(created_at) as date'))
                                       ->where('section', 1)->whereNotIn('product_type', [1,3])
                                       ->whereDate('created_at', '=', $adddays)
                                       ->groupBy('date')
                                     //  ->whereRaw('FIND_IN_SET(?, multi_vendor_id)', [Auth::user()->id])
                                       ->get();

                           foreach ($greetings_sell as $key => $value) {
                               foreach ($greetings_orders as $key => $value1) {

                                   $gg[] = array($adddays->format('Y-m-d'), $value1->qty, $value->total);
                               }

                           }
               }


               $return = [

                   'greetingreport' => $gg,

               ];
              return response()->json(array('status' => true,  'data' => $return));

      } catch (\Exception $e) {

       return response()->json(array('status' => false, 'msg'=> $e->getMessage()));

      }
    }

    public function productDataForDashboard(){
        try {
            $now = Carbon::now();
            $lastWeek = $now->startOfWeek(Carbon::SUNDAY);
            $lastMonth = Carbon::now()->startOfMonth()->subDays(1);

            //$lastWeek = Carbon::now()->subWeek();
            //$lastMonth = Carbon::now()->subDays(30);

            $todayDate = Date('Y-m-d');
            $authUser = Auth::user();

            if($authUser->user_type == 4){
                $gifthubProductCount = Product::where('created_by', $authUser->id)->where('type',  2)->count();
                $gifthubActiveProductCount = Product::where('created_by', $authUser->id)->where('status', 1)->where('type',  2)->count();
                $gifthubInActiveProductCount = Product::where('created_by', $authUser->id)->where('status', 0)->where('type',  2)->count();

              //Top 10 selling gifthub.
                $gifthubTopTenSellingProduct = OrderItem::select('product_id', DB::raw('count(*) as total_product_sale'))->whereNotIn('status', [2,3])->where('created_by', $authUser->id)->where('section', 2)->whereNotNull('product_id')->groupBy('product_id')->limit(10)->orderBy('total_product_sale', 'DESC')->get();
                $gifthubTopTenSellingProducts[] = array('Task', 'Gifthub Top Ten Selling Products');
                foreach($gifthubTopTenSellingProduct as $row){
                    $product = Product::where('id', $row->product_id)->withTrashed()->first();
                    if(!empty($product->name)){
                        $gifthubTopTenSellingProducts[] = array($product->name, $row->total_product_sale);
                    }
                }


            }

            if($authUser->user_type == 5){
                //postal
                $postalProductCount = Product::where('type',  1)->count();

                $postcardProductCount = PostCard::count();

                $postalProductActiveCount = Product::where('type',  1)->where('status', 1)->count();
                $postalProductInActiveCount = Product::where('type',  1)->where('status', 0)->count();

                //Top 10 selling postal
                $postalTopTenSellingProduct = OrderItem::select('product_id', DB::raw('count(*) as total_product_sale'))->whereNotIn('status', [2,3])->where('product_type', 2)->where('section', 1)->whereNotNull('product_id')->groupBy('product_id')->limit(10)->orderBy('total_product_sale', 'DESC')->get();
                $postalTopTenSellingProducts[] = array('Task', 'Postal Top Ten Selling Products');
                foreach($postalTopTenSellingProduct as $row){
                    $product = Product::where('id', $row->product_id)->withTrashed()->first();
                    if(!empty($product->name)){
                        $postalTopTenSellingProducts[] = array($product->name, $row->total_product_sale);
                    }
                }

                //Top 10 selling postcard
                $postcardTopTenSellingProduct = OrderItem::select('product_id', DB::raw('count(*) as total_product_sale'))->whereNotIn('status', [2,3])->where('product_type', 1)->whereNotNull('product_id')->groupBy('product_id')->limit(10)->orderBy('total_product_sale', 'DESC')->get();
                $postcardTopTenSellingProducts[] = array('Task', 'Postcard Top Ten Selling Products');
                foreach($postcardTopTenSellingProduct as $row1){
                    $product = PostCard::where('id', $row1->product_id)->withTrashed()->first();
                    if(!empty($product->name)){
                        $postcardTopTenSellingProducts[] = array($product->name, $row1->total_product_sale);
                    }
                }


            }

            if($authUser->user_type == 4){
                $return = [
                     'gifthubProductCount' => $gifthubProductCount,
                     'gifthubActiveProductCount' => $gifthubActiveProductCount,
                     'gifthubInActiveProductCount' => $gifthubInActiveProductCount,
                    'gifthubTopTenSellingProducts' => $gifthubTopTenSellingProducts,
                ];
            }

            if($authUser->user_type == 5){
                $return = [
                    'postalProductCount' => $postalProductCount,
                    'postcardProductCount' => $postcardProductCount,
                    'postalProductActiveCount' => $postalProductActiveCount,
                    'postalProductInActiveCount' => $postalProductInActiveCount,
                    'postalTopTenSellingProducts' => $postalTopTenSellingProducts,
                    'postcardTopTenSellingProducts' => $postcardTopTenSellingProducts,

                ];
            }
            return response()->json(array('status' => true,  'data' => $return));

        } catch (\Exception $e) {
            return response()->json(array('status' => false, 'msg'=> $e->getMessage()));
        }
    }

	public function loginPage(){

        return view('admin.vendor.login');

    }

    public function login(Request $request)
    {
        try{

            $rules = array(
                'email' => 'required|email',
                'password' => 'required|'
            );


            $remember_me = $request->has('remember') ? true : false;

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {

                return Redirect::to('/')
                    ->withErrors("Email and Password is required to login.")
                    ->withInput(Request::get('password'));
            }

            else {
                $userdata = array(
                    'email' => $request->get('email'),
                    'password' => $request->get('password'),
                );

                if (Auth::attempt($userdata, $remember_me)) {
                    if((auth()->user()->user_type != 4) && (auth()->user()->user_type != 5) && (auth()->user()->user_type != 6)){
                        Auth::logout();
                        return redirect()->back()
                            ->withErrors("Forbidden! You do not have permission to access this route.");
                    }

                    if((auth()->user()->status == 0)){
                        Auth::logout();
                        return redirect()->back()
                            ->withErrors("Your account is in inactive status.");
                    }

                    return Redirect::to('vendor/dashboard');

                } else {

                    return Redirect::to('/vendor/login')

                        ->withErrors("The credentials do not match our records");
                }

            }

        }catch(Exception $e){

            return redirect()->back()
                ->withErrors($e->getMessage());
        }
    }


    public function forgot(Request $request) {

        return view('admin.vendor.forgot');
    }

    public function forgotPassword(Request $request) {

         $rules = array(
            'email' => 'required|email',
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('/')
                ->withErrors("Email id is required.");

        } else {

            $userdata = array(
                'email' => $request->email,
            );

           $user=User::query()->where('email',$request->email)->first();

           if(!empty($user->user_type)){
                if ($user->user_type==4 OR $user->user_type==5 OR $user->user_type==6) {

                    $to_name =  $user->name;
                    $to_email = $request->email;
                    $password=uniqid();

                    $user->password = bcrypt($password);

                    $body = 'Your new password is: ' .$password;
                    $data = array('name' => "$to_name", 'body' => "$body");

                    Mail::send('emails.mail', $data, function ($message)use($to_name, $to_email) {

                        $message->to($to_email, $to_name)
                            ->subject('Reset Password');
                        $message->from(config('mail.username'), "Beam App");
                    });
                    $user->update();
                    return Redirect::to('/vendor/login')->withMessage('Your password has been sent successfully.');
                }
            } else {

                return Redirect::to('/forgot_pass')
                    ->withErrors("The Email id do not match with our records.");

            }

        }
    }



    public function logout(Request $request) {

        Auth::logout();

        return Redirect::to('/vendor/login')->withMessage('Logged out successfully.');
    }

    public function payment(){
        return view('admin.auth.payment');

    }

    public function designer_registration(){
        return view('admin.auth.designer_registration');

    }

    public function registerPage(){
        //$postalState = DB::table('postcode')->groupBy('postcode')->count();
        $postalState = DB::table('import_postals')->distinct()->select('state_name')->get();
        //dd($postalState);
        $postalCodes = ImportPostal::get();

        return view('admin.vendor.register', ['postalState' => $postalState, 'postalCodes' => $postalCodes]);

    }
    //sagar code
    public function termandprivacy()
    {
        return view('admin.vendor.termandprivacy');
    }
    public function privacy_policay()
    {
        return view('admin.vendor.privacy-policay');
    }
    public function prohibited_items()
    {
        return view('admin.vendor.prohibited-items');
    }

    public function printerZipBasedState(Request $request, $id)
    {
        $postalCodes = ImportPostal::where('state_name', $id)->get();

        echo json_encode($postalCodes);
    }

    public function save_registration(Request $request)
    {
        try{
            $rules = array(
                'email' => 'required|email',
                'name' => 'required|',
                'phone' => 'required',
                'password' => 'required|',
            );
            $validation = Validator::make($request->all(),$rules);
            if($validation->fails()){
                return Redirect::to('/vendor/registration')->withMessage($validation->errors()->first());
            }

            $input = $request->all();
            if($request->user_type == 5 OR $request->user_type == 6){
                $input['state'] = $request->state_printer;
                $input['zip_code'] = $request->zip_code_printer;
            }

            //check duplicate email.
            $checkDuplicateEmail = User::where('email', $request['email'])->count();
            if($checkDuplicateEmail > 0){
                return Redirect::to('/vendor/registration')->withMessage('Email has already taken.');
            }

            //check duplicate zip_code for printer vendor.
            if($request['user_type'] == 5){
                $checkDuplicatPrinter = User::where('zip_code', $input['zip_code'])->where('user_type', 5)->count();
                if($checkDuplicatPrinter > 0){
                    return Redirect::to('/vendor/registration')->withMessage('Printer vendor already exist on this postal code.');
                }
            }

            //check duplicate zip_code for secure printer vendor.
            if($request['user_type'] == 6){
                $checkDuplicatPrinter = User::where('zip_code', $input['zip_code'])->where('user_type', 6)->count();
                if($checkDuplicatPrinter > 0){
                    return Redirect::to('/vendor/registration')->withMessage('Printer vendor already exist on this postal code.');
                }
            }

            //check duplicate phone.
            $checkDuplicatePhone = User::where('phone', $request['phone'])->count();
            if($checkDuplicatePhone > 0){
                return Redirect::to('/vendor/registration')->withMessage('Phone number has already taken.');
            }

            $input['password'] = bcrypt($input['password']);

            $sixDigitRandomOtp = mt_rand(1000, 9999);

            $input['otp'] = $sixDigitRandomOtp;

            $input['user_type'] = $request->user_type;

            $input['status'] = 0;


            if($save_user = User::create($input)){
                    //vendorwelcome
                    //vendormarketplaceguidelience
                    $emailTemplate = EmailTemplate::where('id', 4)->first();
                    $emailTemplateMarketGuidlience = EmailTemplate::where('id', 3)->first();

                    //Mail to admin
                    $admin = User::where('user_type',1)->first();
                    $body = 'You had received a request from Vendor '.ucfirst($save_user->name).' to approve the account';
                    $to_name = $admin->name;
                    $to_email = $admin->email;
                    $data = array('name' => "$to_name", 'body' => "$body");

                    Mail::send('emails.mail', $data, function ($message)use($to_name, $to_email) {
                        $message->to($to_email, $to_name)
                            ->subject('New Vendor verification');
                        $message->from(config('mail.username'), "Beam App");
                    });

                    //Mail to vendor for welcome
                    $input_arr = array(
                        'name' => $save_user->name,
                        'email' => $save_user->email,
                        'body' => $emailTemplate->message,
                        'subject' => !empty($emailTemplate->subject) ? $emailTemplate->subject : null
                    );

                    Mail::send(['html' => 'emails.vendorwelcome'], $input_arr, function ($message) use ($input_arr) {
                        $message->to($input_arr['email'], 'admin')
                            ->subject($input_arr['subject']);
                        $message->from(config('mail.username'), 'Beam App');
                    });

                    //Mail to vendor for market place guidlience
                    $input_arr_market_guidlience = array(
                        'name' => $save_user->name,
                        'email' => $save_user->email,
                        'body' => $emailTemplateMarketGuidlience->message,
                        'subject' => !empty($emailTemplateMarketGuidlience->subject) ? $emailTemplateMarketGuidlience->subject : null
                    );

                    Mail::send(['html' => 'emails.vendormarketplaceguidelience'], $input_arr_market_guidlience, function ($message) use ($input_arr_market_guidlience) {
                        $message->to($input_arr_market_guidlience['email'], 'admin')
                            ->subject($input_arr_market_guidlience['subject']);
                        $message->from(config('mail.username'), 'Beam App');
                    });


                return Redirect::to('/vendor/login')->withMessage('Your account is created, but waiting for admin approval');
            } else {
                return redirect()->back()
                            ->withErrors("Something went wrong.");
            }

        }catch(Exception $e){

            return redirect()->back()
                ->withErrors($e->getMessage());
        }
    }
}
