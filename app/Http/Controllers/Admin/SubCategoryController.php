<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\DB;
use Session;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Models\Category;
use App\Helpers\Helper;
use App\Models\Occasion;
use App\Models\Type;
use App\Models\SubCategory;
use App\Models\Unit;
use App\Models\Product;
use URL;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = Category::query();        
        $query->with('occasion');        
        $query->orderBy('id', 'DESC');   
        $data = $query->get();
       // Helper::pr($data);die;
        return view('admin.subcategory.index', ['data'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
        //$occasions = Occasion::where('status','=','1')->get();
        //$category = Category::where('status','=','1')->get();
        $occasions = [];
        $category = [];
        return view('admin.subcategory.create', ['occasions' => $occasions, 'category' => $category]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, category $category)
    {
     
        try {
            $user = auth()->user();
            $data = $request->all();
            $data['created_by'] = $user->id;                       
                  
            $category->setIncrementing(true);
           
            $category->fill($data)->save();           
            $url = URL::to('/admin/subcategory');
            //$request->session()->flash('success', 'Product added successfully');
            return response()->json(array('status' => true, 'redirect'=>$url, 'msg' => 'Sub Category added successfully')); 
        } catch (\Exception $e) {
            //$request->session()->flash('error', $e->getMessage());
            return response()->json(array('status' => false, 'msg'=> $e->getMessage()));
        }
        return redirect(route('category.create'));
       // return redirect(route('vendorproduct.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        //
        return view('brand.show', ['brand' => $brand]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {       
        $occasions = Occasion::where('status','=','1')->where('type', $category->type)->get();
        return view('admin.subcategory.edit', ['data' => $category, 'occasions' => $occasions]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {      
        try {
            $data = $request->all();
            $user = auth()->user();
            $data['update_by'] = $user->id;                        
                
            $category->fill($data); 
            $category->save();
            $request->session()->flash('success', 'Sub Category updated successfully.');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
        }
        return redirect(route('Admin.subcategory.index'));
        //return redirect(route('vendorproduct.edit', [$product->id]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Occasion $occasion)
    {
        try {
            Category::where('id', $request->id)->delete();
            $request->session()->flash('danger', 'Sub Category deleted successfully.');

        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            throw new \Exception('Error while deleteing in the database.');
        }
        return redirect(route('category.index'));
    }

    

    
}


            