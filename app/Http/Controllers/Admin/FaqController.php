<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\DB;
use Session;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Models\Category;
use App\Helpers\Helper;
use App\Models\MailType;
use App\Models\Occasion;
use App\Models\Type;
use App\Models\SubCategory;
use App\Models\Unit;
use App\Models\Product;
use URL;
use App\Models\Faq;
use App\Models\Query;
use App\Models\Notification;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = Faq::query();
        $query->orderBy('id', 'DESC');
        $data = $query->get();

        return view('admin.faq.index', ['data'=>$data]);
    }

    public function query_list()
    {
        $now = \Carbon\Carbon::now()->toDateString();

        // Fetch new queries that haven't been seen by the admin
        $new_queries = Query::where('is_admin_seen', 0)
                             ->whereDate('created_at', $now)
                             ->get();

        // Update is_highlight field for the new queries
        foreach ($new_queries as $new_query) {
            $new_query->update([
                'is_admin_seen' => 1,
                'is_highlight' => 1
            ]);
        }

        // Fetch old queries that have been seen by the admin
        $old_queries = Query::where('is_admin_seen', 1)
                             ->whereDate('created_at', '<', $now)
                             ->get();

        // Update is_highlight field for the old queries
        foreach ($old_queries as $old_query) {
            $old_query->update([
                'is_highlight' => 0
            ]);
        }

        // Fetch the data with the latest queries sorted by their IDs in descending order
        $data = Query::orderBy('id', 'DESC')->get();

        return view('admin.faq.query', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$occasions = Occasion::where('status','=','1')->get();
        //$category = Category::where('status','=','1')->get();
        $occasions = [];
        $category = [];
        return view('admin.faq.create', ['occasions' => $occasions, 'category' => $category]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Faq $Faq)
    {

        try {
            $user = auth()->user();
            $data = $request->all();
            $data['created_by'] = $user->id;



            $Faq->setIncrementing(true);

            $Faq->fill($data)->save();
            $url = URL::to('/admin/faq');
            $request->session()->flash('success', 'Faq added successfully');
            return response()->json(array('status' => true, 'redirect'=>$url, 'msg' => 'Faq added successfully'));

        } catch (\Exception $e) {
            //$request->session()->flash('error', $e->getMessage());
            return response()->json(array('status' => false, 'msg'=> $e->getMessage()));
        }
        return redirect(route('faq.create'));
        // return redirect(route('vendorproduct.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        //
        return view('brand.show', ['brand' => $brand]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Faq $Faq)
    {

        return view('admin.faq.edit', ['data' => $Faq]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Faq $Faq)
    {
        try {
            $data = $request->all();
            $user = auth()->user();
            $data['update_by'] = $user->id;


            $Faq->fill($data);
            $Faq->save();
            $request->session()->flash('success', 'Faq updated successfully.');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
        }
        return redirect(route('Admin.faq.index'));
        //return redirect(route('vendorproduct.edit', [$product->id]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Faq $faq)
    {
        try {
            Faq::where('id', $request->id)->delete();
            $request->session()->flash('danger', 'Faq deleted successfully.');

        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            throw new \Exception('Error while deleteing in the database.');
        }
        return redirect(route('faq.index'));
    }

    public function delete_query(Request $request, Faq $faq)
    {
        try {
            Query::where('id', $request->id)->delete();
            $request->session()->flash('danger', 'Query deleted successfully.');

        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            throw new \Exception('Error while deleteing in the database.');
        }
        return redirect(route('query'));
    }

    public function notification_list()
    {
        $user = auth()->user();
        $data['created_by'] = $user->id;

        $query = Notification::query();
        $query->with('user');
        $query->where('created_by', $user->id);
        $query->orderBy('id', 'DESC');
        $data = $query->get();

        // $data = DB::table('notifications')->select('message', 'notifications.*')->where('created_by', $user->id)->groupBy('message')->get();
        $returnArray = [];
        foreach($data as $d) {
            $returnArray[$d->message] = $d;
        }
        // dd($returnArray);
        return view('admin.faq.notification', ['data'=>$returnArray]);
    }

    public function delete_notification(Request $request, Faq $faq)
    {
        try {
            Notification::where('id', $request->id)->delete();
            $request->session()->flash('danger', 'Notification deleted successfully.');

        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            throw new \Exception('Error while deleteing in the database.');
        }
        return redirect(route('notification'));
    }

    public function create_notification()
    {
        $occasions = [];
        $category = [];
        return view('admin.faq.notification_create', ['occasions' => $occasions, 'category' => $category]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_notification(Request $request, Notification $Notification)
    {
        try {
            $authUser = auth()->user();
            $data = $request->all();

            //send notification to all user.
            $allUser = User::where('user_type', 2)->get();
            foreach($allUser as $user) {
                //push notification start here.
                $message = $data['message'];

                $push_message = [
                    'title' => 'Beam',
                    'body' => $message,
                    'type' => '0',
                    'user_type' => '0'

                ];

                if (!empty($user->device_token)) {
                    if ($user->device_type == 1) {
                        $sendPush = Helper::sendNotification($user->device_token, $push_message);
                    } elseif ($user->device_type == 2) {
                        $sendPush = Helper::iosSendNotification($user->device_token, $push_message);
                    }
                }
                $notification = [
                    'user_id' => $user->id,
                    'message' => $message,
                    'created_by' => $authUser->id
                ];

                Notification::create($notification);
            }

            $url = URL::to('/admin/notification-list');
            //$request->session()->flash('success', 'Notification sent successfully');
            return response()->json(array('status' => true, 'redirect'=>$url, 'msg' => 'Notification sent successfully'));

        } catch (\Exception $e) {
            //$request->session()->flash('error', $e->getMessage());
            return response()->json(array('status' => false, 'msg'=> $e->getMessage()));
        }
        return redirect(route('faq.create'));
        // return redirect(route('vendorproduct.index'));
    }
}
