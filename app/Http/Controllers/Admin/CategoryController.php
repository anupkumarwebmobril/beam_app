<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\DB;
use Session;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Models\Category;
use App\Helpers\Helper;
use App\Models\Occasion;
use App\Models\Type;
use App\Models\SubCategory;
use App\Models\Unit;
use App\Models\Product;
use URL;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = Occasion::query();
        $query->orderBy('id', 'DESC');
        $data = $query->get();

        return view('admin.category.index', ['data'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$occasions = Occasion::where('status','=','1')->get();
        //$category = Category::where('status','=','1')->get();
        $occasions = [];
        $category = [];
        return view('admin.category.create', ['occasions' => $occasions, 'category' => $category]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Occasion $occasion)
    {

        try {

            $url = URL::to('admin/category/index');

            $validation = Validator::make($request->all(), [

                'image'=> 'image|mimes:png|max:2048',
            ]);
            if($validation->fails()){
                return response()->json(array('status' => false, 'msg'=> 'Image should be PNG formats only'));
            }

            $user = auth()->user();
            $data = $request->all();
            $data['created_by'] = $user->id;

            //first image
            if ($request->hasFile('image')) {
                $request['folder_name'] = 'occasion';
                $user = Helper::commonAddUpdateImageMethod($request, $user);
                if(!empty($user->image)){
                    $data['image'] = $user->image;
                }
            }


            $occasion->setIncrementing(true);

            $occasion->fill($data)->save();
            $url = URL::to('/admin/category');
            //$request->session()->flash('success', 'Product added successfully');
            return response()->json(array('status' => true, 'redirect'=>$url, 'msg' => 'Category added successfully'));
        } catch (\Exception $e) {
            //$request->session()->flash('error', $e->getMessage());
            return response()->json(array('status' => false, 'msg'=> $e->getMessage()));
        }
        return redirect(route('category.create'));
       // return redirect(route('vendorproduct.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        //
        return view('brand.show', ['brand' => $brand]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Occasion $occasion)
    {

        return view('admin.category.edit', ['data' => $occasion]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Occasion $occasion)
    {
        //try {

           $data = $request->all();
           $user = auth()->user();
           $data['update_by'] = $user->id;

            //first image
            if ($request->hasFile('image')) {
                //check if image is png only.
                $image = $request->file('image');
                $name = $image->getClientOriginalName();
                $extention = \File::extension($name);
               
                if($extention != "png"){
                    return redirect()->back()->withErrors('Image should be PNG formats only.');
                }
                $request['folder_name'] = 'occasion';
                $user = Helper::commonAddUpdateImageMethod($request, $user);
                if(!empty($user->image)){
                    $data['image'] = $user->image;
                }
            }
           
            $occasion->fill($data);
            $occasion->save();
            $request->session()->flash('success', 'Category updated successfully.');
        /* } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
        } */
        return redirect(route('Admin.category.index'));
        //return redirect(route('vendorproduct.edit', [$product->id]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Occasion $occasion)
    {
        try {
            Occasion::where('id', $request->id)->delete();
            Category::where('occasion_id', $request->id)->delete();
            $request->session()->flash('danger', 'Category deleted successfully.');

        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
            throw new \Exception('Error while deleteing in the database.');
        }
        return redirect(route('category.index'));
    }




}


