<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Exception;
use Validator;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Hash;
use Auth;
use Illuminate\Support\Facades\Storage;
use App\Models\ImportPostal;

class VendorController extends Controller
{
    public function profile()
    {

        try {

            $countUsers = User::where(['user_type' => 4, 'status' => 1])->count();


            return view('admin/vendor_profile')
                ->withCountUsers($countUsers);

        } catch (Exception $e) {

            return redirect()->back()->withErrors($e->getMessage());
        }
    }
    public function getProfile()
    {

        try{

            $user = auth()->user();
            $postalCodes = ImportPostal::get();
            return view('admin.vendor_update_profile', ['user' => $user, 'postalCodes' => $postalCodes]);

        }catch(Exception $e){

            return redirect()->back()->withErrors($e->getMessage());
        }
    }
     public function updateProfile(Request $request)
    {
        try{
            // dd($request->all());

            $rules = array(
                'name'    => 'required',
                'profile_image' => 'sometimes|mimes:jpg,jpeg,png,gif'
            );

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);

            } else {

                $user = auth()->user();

                $user->name = $request->input('name');

                $user->easy_parcel_api_key = $request->input('easy_parcel_api_key');

                $user->zip_code = $request->input('zip_code');

                $user->business_name = $request->input('business_name');
                $user->business_registration_no = $request->input('business_registration_no');
                $user->phone = $request->input('phone');
                $user->business_phone_no = $request->input('business_phone_no');
                $user->website = $request->input('website');
                $user->house_number = $request->input('house_number');
                $user->street_address = $request->input('street_address');
                $user->land_mark = $request->input('land_mark');
                $user->state = $request->input('state');
                $user->country = $request->input('country');
                $user->contact_person_1 = $request->input('contact_person_1');
                $user->contact_person_2 = $request->input('contact_person_2');
                $user->contact_number_2 = $request->input('contact_number_2');

                if ($request->hasFile('profile_image')) {

                    try {

                        $profile_image = $request->file('profile_image');

                        $name = $profile_image->getClientOriginalName();

                        // Delete file
                        if (file_exists($user->profile_image)) {
                            unlink($user->profile_image);
                        }

                        $extention = \File::extension($name);
                        $imageNameWithExtention = $user->id.'.'.$extention;

                        Storage::putFileAs('/profile_pictures/', $request->file('profile_image'), $imageNameWithExtention);

                        $user->profile_image = $imageNameWithExtention;


                    } catch (Exception $e) {

                        return redirect()->back()->withErrors($e->getMessage());
                    }
                }


                $user->update();

                return Redirect::to('vendor/profile');
            }

        }catch(Exception $e){

            return redirect()->back()->withErrors($e->getMessage());
        }
    }
     public function changePasswordView()
    {

        return view('admin/vendor_change_password');
    }

    public function changePassword(Request $request)
    {

        try {

            $rules = array(
                'old_password' => 'required|max:20|min:3',
               'password' => 'required|min:8|different:old_password',
               'confirm_password' => 'required|same:password',
            );

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);

            } else {

                $getdata = auth()->user();

                if (!Hash::check($request['old_password'], Auth::user()->password)) {

                    $message = 'Sorry! Your old password has incorrect.';
                    return redirect()->back()->withMessage($message);
                    // return Redirect::to('password_setting')
                    //     ->withMessage('Sorry! Your old password has incorrect.')
                    //     ->withInput($request->input());
                }

                $update = User::find($getdata->id);
                $update->update([
                    'password' => bcrypt($request->password),
                ]);
                $message = "Password updated successfully";
                    return redirect()->back()->withMessage($message);
            }

        } catch (Exception $e) {

            return redirect()->back()->withErrors($e->getMessage());
        }
    }
	public function index(){

        try{

            $vendors= User::where('user_type', 3)->get();

            return view('admin.vendor.vendor_list')->withVendors($vendors);

        }catch(Exception $e){

            return redirect()->back()
                ->withErrors($e->getMessage());
        }
    }

    public function addVendorForm(){

        return view('admin.vendor.add_vendor');

    }

    public function getVendor($id)
    {
        try {

            $vendor = User::query()->where('id', $id)->first();

            return view('admin.vendor.update_vendor')

                    ->withVendor($vendor);

        }catch(Exception $e){

            return redirect()->back()
                ->withErrors($e->getMessage());

        }
    }

    public function addVendor(Request $request){
        $rules = array(
            'name'    => 'required',
            'email' => 'required',
            'password'=>'required',
            'phone'=>'required|min:6|max:15'
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        if($request->isMethod('post')){

            try{

                $requestData = $request->all();

                $requestData['password'] = bcrypt($requestData['password']);

                $requestData['user_type'] = 2;

                User::create($requestData);
                // dd($man);

                return Redirect::to('admin/vendors-list')->withMessage("Vendor Created Successfully.");

            }catch(Exception $e){

                return redirect()->back()
                ->withErrors($e->getMessage());
            }
        }
    }

    public function updateVendor(Request $request){
        $rules=array(
           // 'name'=>'required',
           // 'email'=>'required',
            //'password'=>'required',
            'phone'=>'required|min:6|max:15'
        );
        $validator=Validator::make($request->all(), $rules);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator);
        }
        if($request->isMethod('post')){

            try{

                $vendorId = $request->input('id');

                $vendor = User::query()->where('id', $vendorId)->first();
                if($vendor->password != null)
                {
                    $vendor->password = bcrypt($request->password);
                }
                else
                {
                    $vendor->password = $vendor->password;
                }
                $vendor->name=$request->name;
                $vendor->email=$request->email;
                $vendor->phone=$request->phone;
                //$vendor->password=bcrypt($request->password);
                $vendor->status=$request->status;

                 $vendor->update();

                return Redirect::to('admin/vendors-list')->withMessage("Vendor Updated Successfully.");

            }catch(Exception $e){

                return redirect()->back()
                    ->withErrors($e->getMessage());
            }
        }

    }

    // public function approveVendor(Request $request){

    //     $vendorId = $request->vendorId;

    //     User::where('id', $vendorId)->update(['is_vendor_approved'=> 1]);

    // }

    // public function unapproveVendor(Request $request){

    //     $vendorId = $request->vendorId;

    //     User::where('id', $vendorId)->update(['is_vendor_approved'=> 0]);

    // }

    public function deleteVendor($id){

        try{

            $vendor = User::where('id', $id)->first();

            if(isset($vendor)){

                $vendor->delete();

                $message = "Vendor Deleted Successfully!";

                return redirect()->back()->withMessage($message);
            }

            else{

                return redirect()->back()->withErrors("Vendor could not be deleted!");
            }

        }catch(Exception $e){

            return redirect()->back()
                ->withErrors($e->getMessage());
        }
    }
}
