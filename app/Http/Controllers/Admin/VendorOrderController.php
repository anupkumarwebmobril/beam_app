<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\DB;
use Session;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Models\Category;
use App\Helpers\Helper;
use App\Models\Occasion;
use App\Models\Type;
use App\Models\SubCategory;
use App\Models\Unit;
use App\Models\Product;
use URL;
use App\Models\Order;
use App\Models\OrderStatus;
use App\Models\OrderStatusMaster;
use App\Models\OrderItem;
use App\Models\PaperTypes;
use App\Models\AddRecipient;
use App\Models\PostCard;
use Storage;
use App\Models\Contacts;
use App\Models\MailType;
use App\Models\Notification;
use App\Models\Rating;

class VendorOrderController extends Controller
{


     public function indexBackup()
    {
        $data = [];

        $user = OrderItem::where('vendor_id',auth()->user()->id)->get();
        foreach($user as $key => $value)
        {
            $query = Order::query();
            $query->orderBy('id', 'DESC');
            $query->where('id',$value->id);
            $data = $query->get();
            foreach($data as $val){
                $orderStatus = OrderStatus::with(['orderStatusMaster'])->where('order_id', $val->id)->orderBy('id', 'DESC')->first();
                if(!empty($orderStatus->orderStatusMaster['name'])){
                    $val->order_status_name = $orderStatus->orderStatusMaster['name'];
                } else {
                    $val->order_status_name = null;
                }

                //get order name (Product name).
                $orderItem = OrderItem::with('product')->where('order_id', $val->id)->first();
                if(!empty($orderItem['product']->name)){
                    $val->product_name = $orderItem['product']->name;

                } else {
                    $val->product_name = null;
                }

                //get sender list.
                if(!empty($val->send_to)){
                    $recepientUsers = explode(',', $val->send_to);
                    $recepient = AddRecipient::whereIn('id', $recepientUsers)->get();
                    /* $returnRecipient = null;
                    $i = 1;
                    foreach($recepient as $rep){
                        $returnRecipient = $returnRecipient.$rep->name;
                        if($i != sizeof($recepient)){
                            $returnRecipient = $returnRecipient."; ";
                        }

                        $i++;
                    } */
                    $val->recepients = $recepient;
                }
                // end get sender list.

            }

        }
        $paperTypes = paperTypes::where('status', 1)->get();


        return view('admin.vendororder.index', ['data'=>$data, 'paper_type' => $paperTypes]);
    }

    public function index()
    {
        $data = [];

        $orderItem = OrderItem::where('vendor_id',auth()->user()->id)
            ->where('section', 2)
            ->whereNotIn('status', [2,3])
            ->orderBy('id', 'DESC')
            ->get();
            // dd($user);
        $returnArray=[];
        foreach($orderItem as $key => $item)
        {
            $query = Order::query();
            $query->orderBy('id', 'DESC');
            $query->where('id',$item->order_id);
            $val = $query->first();

            $val->order_item_id = $item->id;

            //get order status from easy parcel.
            if(!empty($item->easy_parcel_order_no) && $item->easy_parcel_order_no != null){
                $domain = env('EASY_PARCEL_BASE_URL');

                $action = "MPParcelStatusBulk";
                $postparam = array(
                    'authentication'    => env('EASY_PARCEL_AUTHENTICATION_KEY'),
                    'api'    => 'EP-AoBYqujRY',
                    'bulk'    => array(
                        array(
                            'order_no'    => $item->easy_parcel_order_no,
                        ),

                    ),
                );

                $url = $domain . $action;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postparam));
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

                ob_start();
                $return = curl_exec($ch);
                ob_end_clean();
                curl_close($ch);

                $json = json_decode($return);
                if(!empty($json->result[0]->parcel[0]->ship_status)){
                    $val->order_status_name = $json->result[0]->parcel[0]->ship_status;
                } else {
                    $val->order_status_name = null;
                }

            } else {
                $orderStatus = OrderStatus::with(['orderStatusMaster'])->where('order_id', $val->id)->orderBy('id', 'DESC')->first();
                if(!empty($orderStatus->orderStatusMaster['name'])){
                    $val->order_status_name = $orderStatus->orderStatusMaster['name'];
                } else {
                    $val->order_status_name = null;
                }
            }

            //get paper type listing
            $paperTypeIds = explode(',', $val->paper_type_id);
            $paperTypes = PaperTypes::whereIn('id', $paperTypeIds)->get();
            $val['paperTypes'] = $paperTypes;

            //get order name (Product name).
            $product = Product::where('id', $item->product_id)->first();

            if(!empty($product->name)){
                $val->product_name = $product->name;
            } else {
                $val->product_name = null;
            }

            //get sender list.
            if(!empty($val->send_to)){
                $recepientUsers = explode(',', $val->send_to);
                $recepient = Contacts::whereIn('id', $recepientUsers)->get();

                $val->recepients = $recepient;
            }

            //get sender name.
            $sender = User::where('id', $item->created_by)->withTrashed()->first();
            if(!empty($sender->name)){
                $val->sender_name = $sender->name;
            } else {
                $val->sender_name = null;
            }
            // end get sender list.
            $val->to_text = $item->to_text;
            $val->write_your_text = $item->write_your_text;
            $val->quantity = $item->quantity;
            $val->order_item = $item;
            $returnArray[] = $val;

        }
        $paperTypes = paperTypes::where('status', 1)->get();


        return view('admin.vendororder.index', ['data'=>$returnArray, 'paper_type' => $paperTypes]);
    }

    public function zipBackup()
    {
            //get zip based orders
            $paperTypes = paperTypes::where('status', 1)->get();

            $data = [];
            $orderItemBasedZipCode = [];
            $authUser = Auth::user();

            if($authUser->user_type == 5){
                $orderItems = OrderItem::where('section', '!=', 2)
                    ->where('product_type', '!=', 3)
                    ->whereNotIn('status', [2,3])
                    ->get();

            }

            if($authUser->user_type == 6){
                $orderItems = OrderItem::where('product_type', '=', 3)
                    ->whereNotIn('status', [2,3])
                    ->get();
            }

            foreach($orderItems as $items){
                $recepientUsers = Helper::checkVendorAndUserZipcode($items);

                if(!empty($recepientUsers['recipientIds'][0])){
                    $items->recipientIds = $recepientUsers;
                    $orderItemBasedZipCode[] = $items;
                }
            }

            $returnArray=[];
            foreach($orderItemBasedZipCode as $key => $item)
            {

                // dd($item);
                $image_array = [];

                if($item->product_type == 2){
                    if($item->custom_order != 1){
                        $product = Product::where('id',$item->product_id)->withTrashed()->first();
                        // dd($product->image);

                        if(!empty($item->image)){
                            $image_array[] = Storage::url('/order_print/'.$item->image);
                        } else {
                            $image_array[] = Storage::url('/product/'.$product->image);
                        }

                        if(!empty($product->image_2)){
                            $image_array[] = Storage::url('/product/'.$product->image_2);
                        }
                        if(!empty($product->image_3)){
                            $image_array[] = Storage::url('/product/'.$product->image_3);
                        }
                        if(!empty($product->image_4)){
                            $image_array[] = Storage::url('/product/'.$product->image_4);
                        }
                    } else {

                        if(!empty($item->image)){
                            $image_array[] = Storage::url('/order_print/'.$item->image);
                        }

                        if(!empty($item->image_2)){
                            $image_array[] = Storage::url('/order_print/'.$item->image_2);
                        }

                        if(!empty($item->image_3)){
                            $image_array[] = Storage::url('/order_print/'.$item->image_3);
                        }

                        if(!empty($item->image_4)){
                            $image_array[] = Storage::url('/order_print/'.$item->image_4);
                        }


                    }
                }
                if($item->product_type == 1){
                    if($item->custom_order != 1){
                        $post_card = PostCard::where('id',$item->product_id)->first();

                        if($item->image == null){
                            $item->image = Storage::url('/postcard/'.$post_card->image);
                        }else{
                            $image_array[] = Storage::url('/order_print/'.$item->image);
                        }
                        if(!empty($post_card->image_2)){
                            $image_array[] =Storage::url('/postcard/'.$post_card->image_2);
                        }
                    } else {
                        if(!empty($item->image)){
                            $image_array[] = Storage::url('/order_print/'.$item->image);
                        }

                        if(!empty($item->image_2)){
                            $image_array[] = Storage::url('/order_print/'.$item->image_2);
                        }

                    }
                }

                if($item->product_type == 3){
                    if(!empty($item->image)){
                        $image_array[] = Storage::url('/order_print/'.$item->image);
                    } else {
                       // $image_array[] = Storage::url('/product/'.$item->image);
                    }
                }
                // dd($image_array);




                $query = Order::query();
                $query->orderBy('id', 'DESC');
                $query->where('id',$item->order_id);
                $val = $query->first();
                // dd($val);

                $val->order_item_id = $item->id;


                //get order status from easy parcel.
            if(!empty($item->easy_parcel_order_no) && $item->easy_parcel_order_no != null){

                $domain = env('EASY_PARCEL_BASE_URL');

                $action = "MPParcelStatusBulk";
                $postparam = array(
                    'authentication'    => env('EASY_PARCEL_AUTHENTICATION_KEY'),
                    'api'    => 'EP-AoBYqujRY',
                    'bulk'    => array(
                        array(
                            'order_no'    => $item->easy_parcel_order_no,
                        ),

                    ),
                );

                $url = $domain . $action;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postparam));
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

                ob_start();
                $return = curl_exec($ch);
                ob_end_clean();
                curl_close($ch);

                $json = json_decode($return);
                if(!empty($json->result[0]->parcel[0]->ship_status)){
                    $val->order_status_name = $json->result[0]->parcel[0]->ship_status;
                } else {
                    $val->order_status_name = null;
                }

            } else {
                $orderStatus = OrderStatus::with(['orderStatusMaster'])->where('order_id', $val->id)->orderBy('id', 'DESC')->first();
                if(!empty($orderStatus->orderStatusMaster['name'])){
                    $val->order_status_name = $orderStatus->orderStatusMaster['name'];
                } else {
                    $val->order_status_name = null;
                }
            }

                    //get paper type listing
                    $paperTypeIds = explode(',', $val->paper_type_id);
                    $paperTypes = PaperTypes::whereIn('id', $paperTypeIds)->get();
                    $val['paperTypes'] = $paperTypes;

                    //get order name (Product name).
                    if($item->product_type == 1)
                    {
                        $product = PostCard::where('id', $item->product_id)->first();
                    } else if($item->product_type == 3){
                        $product = MailType::where('id', $item->mail_type_id)
                        ->first();
                    }
                    else
                    {
                        $product = Product::where('id', $item->product_id)->first();
                    }

                    if(!empty($product->name)){
                        if($item->product_type == 3){
                            $val->product_name = $item->attachment_original_name." - ".$product->name;
                        } else {
                            $val->product_name = $product->name;
                        }
                    } else {
                        $val->product_name = null;
                    }

                    //get sender list.
                    if(!empty($val->send_to)){
                        /* $recepientUsers = explode(',', $val->send_to);
                        $recepient = AddRecipient::whereIn('id', $recepientUsers)->get();

                        $val->recepients = $recepient; */
                    }

                    /* if(!empty($recepientUsers['recipientIds'][0])){
                        $val->recepients = $recepientUsers['recipientIds'];
                    } */

                    if(!empty($item->recipientIds['recipientIds'])){
                        $val->recepients = $item->recipientIds['recipientIds'];
                    }

                    //get sender name.
                    $sender = User::where('id', $item->created_by)->withTrashed()->first();
                    if(!empty($sender->name)){
                        $val->sender_name = $sender->name;
                    } else {
                        $val->sender_name = null;
                    }

                    // dd($val);
                    // end get sender list.
                    $val->to_text = $item->to_text;
                    $val->write_your_text = $item->write_your_text;
                    $val->quantity = $item->quantity;
                    $val->images = $image_array;
                    $val->order_item = $item;
                    $returnArray[] = $val;

                    //}
                }


                // dd($orderItemBasedZipCode);

       return view('admin.order.vendororder', ['data' => $returnArray, 'paper_type' => $paperTypes]);
    }

    public function zip()
    {
            //get zip based orders
            $paperTypes = paperTypes::where('status', 1)->get();

            $data = [];
            $orderItemBasedZipCode = [];
            $authUser = Auth::user();

            if($authUser->user_type == 5){
               $orderItemBasedZipCode = OrderItem::where('section', '!=', 2)
                    ->where('product_type', '!=', 3)
                    ->whereNotIn('status', [2,3])
                    ->whereRaw('FIND_IN_SET(?, multi_vendor_id)', [Auth::user()->id])
                    ->get();
            }

            if($authUser->user_type == 6){
                $orderItemBasedZipCode = OrderItem::where('product_type', '=', 3)
                    ->whereNotIn('status', [2,3])
                    ->whereRaw('FIND_IN_SET(?, multi_vendor_id)', [Auth::user()->id])
                    ->get();
            }

            $returnArray=[];

            foreach($orderItemBasedZipCode as $key => $item)
            {

                // dd($item);
                $image_array = [];

                if($item->product_type == 2){
                    if($item->custom_order != 1){
                        $product = Product::where('id',$item->product_id)->withTrashed()->first();
                        // dd($product->image);

                        if(!empty($item->image)){
                            $image_array[] = Storage::url('/order_print/'.$item->image);
                        } else {
                            $image_array[] = Storage::url('/product/'.$product->image);
                        }

                        if(!empty($product->image_2)){
                            $image_array[] = Storage::url('/product/'.$product->image_2);
                        }
                        if(!empty($product->image_3)){
                            $image_array[] = Storage::url('/product/'.$product->image_3);
                        }
                        if(!empty($product->image_4)){
                            $image_array[] = Storage::url('/product/'.$product->image_4);
                        }
                    } else {

                        if(!empty($item->image)){
                            $image_array[] = Storage::url('/order_print/'.$item->image);
                        }

                        if(!empty($item->image_2)){
                            $image_array[] = Storage::url('/order_print/'.$item->image_2);
                        }

                        if(!empty($item->image_3)){
                            $image_array[] = Storage::url('/order_print/'.$item->image_3);
                        }

                        if(!empty($item->image_4)){
                            $image_array[] = Storage::url('/order_print/'.$item->image_4);
                        }


                    }
                }
                if($item->product_type == 1){
                    if($item->custom_order != 1){
                        $post_card = PostCard::where('id',$item->product_id)->first();

                        if($item->image == null){
                            $item->image = Storage::url('/postcard/'.$post_card->image);
                        }else{
                            $image_array[] = Storage::url('/order_print/'.$item->image);
                        }
                        if(!empty($post_card->image_2)){
                            $image_array[] =Storage::url('/postcard/'.$post_card->image_2);
                        }
                    } else {
                        if(!empty($item->image)){
                            $image_array[] = Storage::url('/order_print/'.$item->image);
                        }

                        if(!empty($item->image_2)){
                            $image_array[] = Storage::url('/order_print/'.$item->image_2);
                        }

                    }
                }

                if($item->product_type == 3){
                    if(!empty($item->image)){
                        $image_array[] = Storage::url('/order_print/'.$item->image);
                    } else {
                       // $image_array[] = Storage::url('/product/'.$item->image);
                    }
                }
                // dd($image_array);
                $query = Order::query();
                $query->orderBy('id', 'DESC');
                $query->where('id',$item->order_id);
                $val = $query->first();
                // dd($val);

                $val->order_item_id = $item->id;

                //get order status from easy parcel.
            if(!empty($item->easy_parcel_order_no) && $item->easy_parcel_order_no != null){

                $domain = env('EASY_PARCEL_BASE_URL');
                // dd($domain);

                $action = "EPTrackingBulk";
                $postparam = array(
                    'authentication'    => env('EASY_PARCEL_AUTHENTICATION_KEY'),
                    'api'    => 'EP-AoBYqujRY',
                    'bulk'    => array(
                        array(
                            'awb_no'    => $item->easy_parcel_order_no,
                        ),

                    ),
                );

                $url = $domain . $action;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postparam));
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                ob_start();
                $return = curl_exec($ch);
                ob_end_clean();
                curl_close($ch);

                $json = json_decode($return);
                // dd($json->result[0]->latest_status);
                if(!empty($json->result[0]->latest_status)){
                    $val->order_status_name = $json->result[0]->latest_status;
                } else {
                    $val->order_status_name = null;
                }

            } else {
                $orderStatus = OrderStatus::with(['orderStatusMaster'])->where('order_id', $val->id)->orderBy('id', 'DESC')->first();
                if(!empty($orderStatus->orderStatusMaster['name'])){
                    $val->order_status_name = $orderStatus->orderStatusMaster['name'];
                } else {
                    $val->order_status_name = null;
                }
            }

                    //get paper type listing
                    $paperTypeIds = explode(',', $val->paper_type_id);
                    $paperTypes = PaperTypes::whereIn('id', $paperTypeIds)->get();
                    $val['paperTypes'] = $paperTypes;

                    //get order name (Product name).
                    if($item->product_type == 1)
                    {
                        $product = PostCard::where('id', $item->product_id)->first();
                    } else if($item->product_type == 3){
                        $product = MailType::where('id', $item->mail_type_id)
                        ->first();
                    }
                    else
                    {
                        $product = Product::where('id', $item->product_id)->first();
                    }

                    if(!empty($product->name)){
                        if($item->product_type == 3){
                            $val->product_name = $item->attachment_original_name." - ".$product->name;
                        } else {
                            $val->product_name = $product->name;
                        }
                    } else {
                        $val->product_name = null;
                    }

                    //get sender list.
                    // dd($val->send_to);
                    // if(!empty($val->send_to)){
                    //     $recepientUsers = explode(',', $val->send_to);
                    //     $recepient = AddRecipient::whereIn('id', $recepientUsers)->get();
                    //     dd($recepient);

                    //     $val->recepients = $recepient;
                    // }
                      //get sender list.
            if(!empty($val->send_to)){
                $recepientUsers = explode(',', $val->send_to);
                $recepient = Contacts::whereIn('id', $recepientUsers)->get();

                $val->recepients = $recepient;
            }

                    // if(!empty($recepientUsers['recipientIds'][0])){
                    //     $val->recepients = $recepientUsers['recipientIds'];
                    // }

                    if(!empty($item->recipientIds['recipientIds'])){
                        $val->recepients = $item->recipientIds['recipientIds'];
                    }

                    //get sender name.
                    $sender = User::where('id', $item->created_by)->withTrashed()->first();
                    if(!empty($sender->name)){
                        $val->sender_name = $sender->name;
                    } else {
                        $val->sender_name = null;
                    }

                    if ($item->product_type == 1) {
                        $productItem = \App\Models\PostCard::where('id', $item->product_id)->withTrashed()->first();
                        $productType = !empty($productItem->name) ? $productItem->name : null;
                        $shipping_fee = $productItem->shipping_fee ?? "N/A";
                        $productType = "PostCard";
                    } elseif ($item->product_type == 3) {
                        $productItem = \App\Models\MailType::where('id', $item->product_id)->withTrashed()->first();
                        $productType = !empty($productItem->name) ? $productItem->name : null;
                        $shipping_fee = $productItem->shipping_fee ?? "N/A";

                    } else {
                        $productItem = \App\Models\Product::where('id', $item->product_id)->withTrashed()->first();
                        $productType = "Greeting";
                        $shipping_fee = $productItem->shipping_fee ?? "N/A";

                    }
                    // dd($shipping_fee);

                    // end get sender list.
                    $val->to_text = $item->to_text;
                    $val->write_your_text = $item->write_your_text;
                    $val->quantity = $item->quantity;
                    $val->images = $image_array;
                    $val->order_item = $item;
                    $val->productType = $productType;
                    $val->shipping_fee = $shipping_fee;
                    $returnArray[] = $val;

                    //}
                    // dd($returnArray);
                }
       return view('admin.order.vendororder', ['data' => $returnArray, 'paper_type' => $paperTypes]);
    }



    public function edit(Request $request, $id)
    {
        /* $orderStatus = OrderStatus::with(['orderStatusMaster'])->where('order_id', $order->id)->orderBy('id', 'DESC')->first();

        if(!empty($orderStatus['order_status_name'])){
            $order->order_status_name = $orderStatus['order_status_name'];
        }
        $orderStatusMaster = OrderStatusMaster::get(); */

        $page = !empty($request['page']) ? $request['page'] : null;

        $orderItem = OrderItem::where('id', $id)->first();

        return view('admin.vendororder.edit', ['order_item' => $orderItem, 'page' => $page]);
    }
    public function update(Request $request)
    {
        try {
           $data = $request->all();
           $user = auth()->user();

           $insert = ['id'=>$data['id']];

           $oldStatus = OrderItem::where($insert)->count();

           if($oldStatus == 1){
                // Order::where('id',$order->id)->update(['easy_parcel_order_no'=>$request->easy_parcel_order_no]);
               OrderItem::where('id',$data['id'])->update(['easy_parcel_order_no'=>$request->easy_parcel_order_no]);
               Session::flash('message','order updated successfully');

               /* if(auth()->user()->user_type == 4){
                return Redirect::to('/vendor/order');
               }else{
                return Redirect::to('/vendor/order/zip');
               } */

               return Redirect::to('/vendor/'.$data['page']);

           }

           // $request->session()->flash('success', 'Order Status updated successfully.');
        } catch (\Exception $e) {
            $request->session()->flash('error', $e->getMessage());
        }
        return redirect(route('Vendor.order.index'));
        //return redirect(route('vendorproduct.edit', [$product->id]));
    }

    public function cancel_order(Request $request, OrderItem $orderItem, $id)
    {

       // try {
           $data = $request->all();


        if(!$request->cancel_reason){
            if($data['customValue'] == null){

                $cancel_reason = $data['selectedOption'];

           }elseif($data['selectedOption'] == "custom"){
            // dd(3434);
            $cancel_reason = $data['customValue'];


           }else{
            $cancel_reason = "Vendor don't give a cancel reason!";

           }
        }

        //    dd($cancel_reason);

           //$user = auth()->user();
           //$data['id'] = $order->id;
           $orderItem = OrderItem::where('id',$id)->first();
           $order = Order::where('id', $orderItem->order_id)->first();


           if(!empty($orderItem->created_by)){
                if($orderItem->product_type == 1) {

                    if($orderItem->custom_order == 1){
                        $tempArray['image'] = Storage::URL('order_print/' . $orderItem->image);
                    } else {
                        $check = PostCard::where('id', $orderItem->product_id)->first();
                        $tempArray['name'] = $check->name;

                        if (!empty($orderItem->image)) {
                            $tempArray['image'] = Storage::URL('order_print/' . $orderItem->image);
                        } else {
                            $tempArray['image'] = Storage::URL('postcard/' . $check->image);
                        }
                    }
                } else if($orderItem->product_type == 2){
                    if($orderItem->custom_order == 1){
                        $tempArray['image'] = Storage::URL('order_print/' . $orderItem->image);
                    } else {
                        $check = Product::where('id', $orderItem->product_id)->first();
                        $tempArray['name'] = $check->name;

                        if (!empty($orderItem->image)) {
                            $tempArray['image'] = Storage::URL('order_print/' . $orderItem->image);
                        } else {
                            $tempArray['image'] = Storage::URL('product/' . $check->image);
                        }
                    }
                } else {
                    $check = MailType::where('id', $orderItem->mail_type_id)
                    ->first();
                    if(!empty($orderItem->attachment_original_name)){
                        $tempArray['name'] = $orderItem->attachment_original_name." - ".$check->name;
                    }

                    if (!empty($orderItem->image)) {
                        $tempArray['image'] = Storage::URL('order_print/' . $orderItem->image);
                    }
                }

               $user = User::where('id', $orderItem->created_by)->first();
               if(!empty($user->id)){
                    //send push notification.
                    //push notification start here.

                    $message = "Your order item";
                    if(!empty($tempArray['name'])){
                        $message = $message.' ('.$tempArray['name'].')';
                    }
                    $message = $message." for order number $order->order_number has being cancelled by Beam Vendor. Cancel reason:" . $orderItem->cancel_reason ?? $cancel_reason;
                    //dd($message);
                    $push_message = [
                        'title' => 'Order Item cancelled',
                        'body' => $message,
                        'type' => '0',
                        'user_type' => '1'

                    ];

                    if (!empty($user->device_token)) {
                        if ($user->device_type == 1) {
                            $sendPush = Helper::sendNotification($user->device_token, $push_message);
                        } else if ($user->device_type == 2) {
                            $sendPush = Helper::iosSendNotification($user->device_token, $push_message);
                        }
                    }

                    $notification = [
                        'user_id' => $user->id,
                        'message' => $message,
                    ];

                    Notification::create($notification);
               }
           }

            $debug = OrderItem::where('id',$orderItem->id)->update(['status'=>2,'cancel_reason'=>$request->cancel_reason ?? $cancel_reason]);
            // dd($debug );

            if(auth()->user()->user_type == 4){
                $url = URL::to('vendor/order');
            }else{
                $url = URL::to('vendor/order/zip');
            }
            return response()->json(array('status' => true, 'redirect'=>$url, 'msg' => 'Order cancelled successfully'));
       /*  } catch (\Exception $e) {
            return response()->json(array('status' => false, 'msg'=> $e->getMessage()));
        } */
        return redirect(route('Vendor.order.index'));
        //return redirect(route('vendorproduct.edit', [$product->id]));
    }

    public function cancelordervendor()
    {
        $data = [];

        $orderItem = OrderItem::where('vendor_id',auth()->user()->id)
            ->where('section', 2)
            ->whereIn('status', [2])
            ->get();
            // dd($user);
        $returnArray=[];
        foreach($orderItem as $key => $item)
        {
            $query = Order::query();
            $query->orderBy('id', 'DESC');
            $query->where('id',$item->order_id);
            $val = $query->first();

            $val->order_item_id = $item->id;

            //get order status from easy parcel.

            $val->order_status_name = "Cancelled";


            //get paper type listing
            $paperTypeIds = explode(',', $val->paper_type_id);
            $paperTypes = PaperTypes::whereIn('id', $paperTypeIds)->get();
            $val['paperTypes'] = $paperTypes;

            //get order name (Product name).
            $product = Product::where('id', $item->product_id)->first();

            if(!empty($product->name)){
                $val->product_name = $product->name;
            } else {
                $val->product_name = null;
            }

            //get sender list.
            if(!empty($val->send_to)){
                $recepientUsers = explode(',', $val->send_to);
                $recepient = Contacts::whereIn('id', $recepientUsers)->get();

                $val->recepients = $recepient;
            }

            //get sender name.
            $sender = User::where('id', $item->created_by)->withTrashed()->first();
            // dd($sender);
            if(!empty($sender->name)){
                $val->sender_name = $sender->name;
            } else {
                $val->sender_name = null;
            }
            // end get sender list.
            $val->to_text = $item->to_text;
            $val->write_your_text = $item->write_your_text;
            $val->quantity = $item->quantity;
            $val->order_item = $item;
            $returnArray[] = $val;

        }
        $paperTypes = paperTypes::where('status', 1)->get();


        return view('admin.vendororder.vendorcancelorder', ['data'=>$returnArray, 'paper_type' => $paperTypes]);
    }

    public function printercancelorderBackup()
    {
       //get zip based orders
        $paperTypes = paperTypes::where('status', 1)->get();

        $data = [];
        $orderItemBasedZipCode = [];

        $authUser = Auth::user();

        if($authUser->user_type == 5){
            $orderItems = OrderItem::where('section', '!=', 2)
                ->where('product_type', '!=', 3)
                ->whereIn('status', [2])
                ->get();

        }

        if($authUser->user_type == 6){
            $orderItems = OrderItem::where('product_type', '=', 3)
                ->whereIn('status', [2])
                ->get();
        }

            foreach($orderItems as $items){
                $recepientUsers = Helper::checkVendorAndUserZipcode($items);

                if(!empty($recepientUsers['recipientIds'][0])){
                    $items->recipientIds = $recepientUsers;
                    $orderItemBasedZipCode[] = $items;
                }
            }

            $returnArray=[];
            foreach($orderItemBasedZipCode as $key => $item)
            {

                // dd($item);
                $image_array = [];

                if($item->product_type == 2){
                    if($item->custom_order != 1){
                        $product = Product::where('id',$item->product_id)->withTrashed()->first();
                        // dd($product->image);

                        if(!empty($item->image)){
                            $image_array[] = Storage::url('/order_print/'.$item->image);
                        } else {
                            $image_array[] = Storage::url('/product/'.$product->image);
                        }

                        if(!empty($product->image_2)){
                            $image_array[] = Storage::url('/product/'.$product->image_2);
                        }
                        if(!empty($product->image_3)){
                            $image_array[] = Storage::url('/product/'.$product->image_3);
                        }
                        if(!empty($product->image_4)){
                            $image_array[] = Storage::url('/product/'.$product->image_4);
                        }
                    } else {

                        if(!empty($item->image)){
                            $image_array[] = Storage::url('/order_print/'.$item->image);
                        }

                        if(!empty($item->image_2)){
                            $image_array[] = Storage::url('/order_print/'.$item->image_2);
                        }

                        if(!empty($item->image_3)){
                            $image_array[] = Storage::url('/order_print/'.$item->image_3);
                        }

                        if(!empty($item->image_4)){
                            $image_array[] = Storage::url('/order_print/'.$item->image_4);
                        }


                    }
                }
                if($item->product_type == 1){
                    if($item->custom_order != 1){
                        $post_card = PostCard::where('id',$item->product_id)->first();

                        if($item->image == null){
                            $item->image = Storage::url('/postcard/'.$post_card->image);
                        }else{
                            $image_array[] = Storage::url('/order_print/'.$item->image);
                        }
                        if(!empty($post_card->image_2)){
                            $image_array[] =Storage::url('/postcard/'.$post_card->image_2);
                        }
                    } else {
                        if(!empty($item->image)){
                            $image_array[] = Storage::url('/order_print/'.$item->image);
                        }

                        if(!empty($item->image_2)){
                            $image_array[] = Storage::url('/order_print/'.$item->image_2);
                        }

                    }
                }

                if($item->product_type == 3){
                    if(!empty($item->image)){
                        $image_array[] = Storage::url('/order_print/'.$item->image);
                    } else {
                       // $image_array[] = Storage::url('/product/'.$item->image);
                    }
                }
                // dd($image_array);




                $query = Order::query();
                $query->orderBy('id', 'DESC');
                $query->where('id',$item->order_id);
                $val = $query->first();
                // dd($val);

                $val->order_item_id = $item->id;
                $val->order_status_name = "Cancelled";

                //get order status from easy parcel.


                    //get paper type listing
                    $paperTypeIds = explode(',', $val->paper_type_id);
                    $paperTypes = PaperTypes::whereIn('id', $paperTypeIds)->get();
                    $val['paperTypes'] = $paperTypes;

                    //get order name (Product name).
                    if($item->product_type == 1)
                    {
                        $product = PostCard::where('id', $item->product_id)->first();
                    } else if($item->product_type == 3){
                        $product = MailType::where('id', $item->mail_type_id)
                        ->first();
                    }
                    else
                    {
                        $product = Product::where('id', $item->product_id)->first();
                    }

                    if(!empty($product->name)){
                        if($item->product_type == 3){
                            $val->product_name = $item->attachment_original_name." - ".$product->name;
                        } else {
                            $val->product_name = $product->name;
                        }
                    } else {
                        $val->product_name = null;
                    }

                    //get sender list.
                    if(!empty($val->send_to)){
                        /* $recepientUsers = explode(',', $val->send_to);
                        $recepient = AddRecipient::whereIn('id', $recepientUsers)->get();

                        $val->recepients = $recepient; */
                    }

                    /* if(!empty($recepientUsers['recipientIds'][0])){
                        $val->recepients = $recepientUsers['recipientIds'];
                    } */

                    if(!empty($item->recipientIds['recipientIds'])){
                        $val->recepients = $item->recipientIds['recipientIds'];
                    }

                    //get sender name.
                    $sender = User::where('id', $item->created_by)->withTrashed()->first();
                    if(!empty($sender->name)){
                        $val->sender_name = $sender->name;
                    } else {
                        $val->sender_name = null;
                    }

                    // dd($val);
                    // end get sender list.
                    $val->to_text = $item->to_text;
                    $val->write_your_text = $item->write_your_text;
                    $val->quantity = $item->quantity;
                    $val->images = $image_array;
                    $val->order_item = $item;
                    $returnArray[] = $val;

                    //}
                }


                // dd($orderItemBasedZipCode);

       return view('admin.vendororder.vendorcancelorder', ['data' => $returnArray, 'paper_type' => $paperTypes]);
    }

    public function printercancelorder()
    {
       //get zip based orders
        $paperTypes = paperTypes::where('status', 1)->get();

        $data = [];
        $orderItemBasedZipCode = [];

        $authUser = Auth::user();

        if($authUser->user_type == 5){
            $orderItemBasedZipCode = OrderItem::where('section', '!=', 2)
                ->where('product_type', '!=', 3)
                ->whereIn('status', [2])
                ->whereRaw('FIND_IN_SET(?, multi_vendor_id)', [Auth::user()->id])
                ->get();

        }

        if($authUser->user_type == 6){
            $orderItemBasedZipCode = OrderItem::where('product_type', '=', 3)
                ->whereIn('status', [2])
                ->whereRaw('FIND_IN_SET(?, multi_vendor_id)', [Auth::user()->id])
                ->get();
        }

            $returnArray=[];
            foreach($orderItemBasedZipCode as $key => $item)
            {

                // dd($item);
                $image_array = [];

                if($item->product_type == 2){
                    if($item->custom_order != 1){
                        $product = Product::where('id',$item->product_id)->withTrashed()->first();
                        // dd($product->image);

                        if(!empty($item->image)){
                            $image_array[] = Storage::url('/order_print/'.$item->image);
                        } else {
                            $image_array[] = Storage::url('/product/'.$product->image);
                        }

                        if(!empty($product->image_2)){
                            $image_array[] = Storage::url('/product/'.$product->image_2);
                        }
                        if(!empty($product->image_3)){
                            $image_array[] = Storage::url('/product/'.$product->image_3);
                        }
                        if(!empty($product->image_4)){
                            $image_array[] = Storage::url('/product/'.$product->image_4);
                        }
                    } else {

                        if(!empty($item->image)){
                            $image_array[] = Storage::url('/order_print/'.$item->image);
                        }

                        if(!empty($item->image_2)){
                            $image_array[] = Storage::url('/order_print/'.$item->image_2);
                        }

                        if(!empty($item->image_3)){
                            $image_array[] = Storage::url('/order_print/'.$item->image_3);
                        }

                        if(!empty($item->image_4)){
                            $image_array[] = Storage::url('/order_print/'.$item->image_4);
                        }


                    }
                }
                if($item->product_type == 1){
                    if($item->custom_order != 1){
                        $post_card = PostCard::where('id',$item->product_id)->first();

                        if($item->image == null){
                            $item->image = Storage::url('/postcard/'.$post_card->image);
                        }else{
                            $image_array[] = Storage::url('/order_print/'.$item->image);
                        }
                        if(!empty($post_card->image_2)){
                            $image_array[] =Storage::url('/postcard/'.$post_card->image_2);
                        }
                    } else {
                        if(!empty($item->image)){
                            $image_array[] = Storage::url('/order_print/'.$item->image);
                        }

                        if(!empty($item->image_2)){
                            $image_array[] = Storage::url('/order_print/'.$item->image_2);
                        }

                    }
                }

                if($item->product_type == 3){
                    if(!empty($item->image)){
                        $image_array[] = Storage::url('/order_print/'.$item->image);
                    } else {
                       // $image_array[] = Storage::url('/product/'.$item->image);
                    }
                }
                // dd($image_array);




                $query = Order::query();
                $query->orderBy('id', 'DESC');
                $query->where('id',$item->order_id);
                $val = $query->first();
                // dd($val);

                $val->order_item_id = $item->id;
                $val->order_status_name = "Cancelled";

                //get order status from easy parcel.


                    //get paper type listing
                    $paperTypeIds = explode(',', $val->paper_type_id);
                    $paperTypes = PaperTypes::whereIn('id', $paperTypeIds)->get();
                    $val['paperTypes'] = $paperTypes;

                    //get order name (Product name).
                    if($item->product_type == 1)
                    {
                        $product = PostCard::where('id', $item->product_id)->first();
                    } else if($item->product_type == 3){
                        $product = MailType::where('id', $item->mail_type_id)
                        ->first();
                    }
                    else
                    {
                        $product = Product::where('id', $item->product_id)->first();
                    }

                    if(!empty($product->name)){
                        if($item->product_type == 3){
                            $val->product_name = $item->attachment_original_name." - ".$product->name;
                        } else {
                            $val->product_name = $product->name;
                        }
                    } else {
                        $val->product_name = null;
                    }

                    //get sender list.
                    if(!empty($val->send_to)){
                        /* $recepientUsers = explode(',', $val->send_to);
                        $recepient = AddRecipient::whereIn('id', $recepientUsers)->get();

                        $val->recepients = $recepient; */
                    }

                    /* if(!empty($recepientUsers['recipientIds'][0])){
                        $val->recepients = $recepientUsers['recipientIds'];
                    } */

                    if(!empty($item->recipientIds['recipientIds'])){
                        $val->recepients = $item->recipientIds['recipientIds'];
                    }

                    //get sender name.
                    $sender = User::where('id', $item->created_by)->withTrashed()->first();
                    if(!empty($sender->name)){
                        $val->sender_name = $sender->name;
                    } else {
                        $val->sender_name = null;
                    }

                    // dd($val);
                    // end get sender list.
                    $val->to_text = $item->to_text;
                    $val->write_your_text = $item->write_your_text;
                    $val->quantity = $item->quantity;
                    $val->images = $image_array;
                    $val->order_item = $item;
                    $returnArray[] = $val;

                    //}
                }


                // dd($orderItemBasedZipCode);

       return view('admin.vendororder.vendorcancelorder', ['data' => $returnArray, 'paper_type' => $paperTypes]);
    }

    public function giftPendingOrderVendor()
    {
        $data = [];

        $orderItem = OrderItem::where('vendor_id',auth()->user()->id)
            ->where('section', 2)
            ->whereNotIn('status', [2,3])
            ->whereNull('easy_parcel_order_no')
            ->get();
            // dd($user);
        $returnArray=[];
        foreach($orderItem as $key => $item)
        {
            $query = Order::query();
            $query->orderBy('id', 'DESC');
            $query->where('id',$item->order_id);
            $val = $query->first();

            $val->order_item_id = $item->id;


            $orderStatus = OrderStatus::with(['orderStatusMaster'])->where('order_id', $val->id)->orderBy('id', 'DESC')->first();
            if(!empty($orderStatus->orderStatusMaster['name'])){
                $val->order_status_name = $orderStatus->orderStatusMaster['name'];
            } else {
                $val->order_status_name = null;
            }

            //get paper type listing
            $paperTypeIds = explode(',', $val->paper_type_id);
            $paperTypes = PaperTypes::whereIn('id', $paperTypeIds)->get();
            $val['paperTypes'] = $paperTypes;

            //get order name (Product name).
            $product = Product::where('id', $item->product_id)->first();

            if(!empty($product->name)){
                $val->product_name = $product->name;
            } else {
                $val->product_name = null;
            }

            //get sender list.
            if(!empty($val->send_to)){
                $recepientUsers = explode(',', $val->send_to);
                $recepient = Contacts::whereIn('id', $recepientUsers)->get();

                $val->recepients = $recepient;
            }

            //get sender name.
            $sender = User::where('id', $item->created_by)->withTrashed()->first();
            if(!empty($sender->name)){
                $val->sender_name = $sender->name;
            } else {
                $val->sender_name = null;
            }
            // end get sender list.
            $val->to_text = $item->to_text;
            $val->write_your_text = $item->write_your_text;
            $val->quantity = $item->quantity;
            $val->order_item = $item;
            $returnArray[] = $val;

        }
        $paperTypes = paperTypes::where('status', 1)->get();


        return view('admin.vendororder.index', ['data'=>$returnArray, 'paper_type' => $paperTypes]);
    }

    public function ratingReviewGift()
    {
        $data = [];

        $ratings = Rating::with(['OrderItem'])->orderBy('id', 'DESC')->get();

        $returnArray=[];

        $authUser = Auth::user();

        foreach($ratings as $key => $rating)
        {

            if(!empty($rating['OrderItem']->section) && ($rating['OrderItem']->section == 2)){
                    if(!empty($rating['OrderItem']->vendor_id) && ($rating['OrderItem']->vendor_id == $authUser->id)){
                        $item =  $rating['OrderItem'];

                        $image_array=[];
                        $query = Order::query();
                        $query->orderBy('id', 'DESC');
                        $query->where('id',$item->order_id);
                        $val = $query->first();

                        $val->order_item_id = $item->id;

                        $product = Product::where('id',$item->product_id)->withTrashed()->first();

                        $image_array[] = Storage::url('/product/'.$product->image);

                        if(!empty($product->image_2)){
                            $image_array[] = Storage::url('/product/'.$product->image_2);
                        }

                        if(!empty($product->image_3)){
                            $image_array[] = Storage::url('/product/'.$product->image_3);
                        }
                        if(!empty($product->image_4)){
                            $image_array[] = Storage::url('/product/'.$product->image_4);
                        }

                        $val->images = $image_array;

                        //get paper type listing
                        $paperTypeIds = explode(',', $val->paper_type_id);
                        $paperTypes = PaperTypes::whereIn('id', $paperTypeIds)->get();
                        $val['paperTypes'] = $paperTypes;

                        //get order name (Product name).
                        $product = Product::where('id', $item->product_id)->first();

                        //get rating and reveiw
                        $rating = Rating::where('order_item_id', $item->id)->first();
                        $val->rating = $rating;

                        if(!empty($product->name)){
                            $val->product_name = $product->name;
                        } else {
                            $val->product_name = null;
                        }
                        //get sender list.
                        if(!empty($val->send_to)){
                            $recepientUsers = explode(',', $val->send_to);
                            $recepient = Contacts::whereIn('id', $recepientUsers)->get();

                            $val->recepients = $recepient;
                        }
                        // end get sender list.

                        //get vendor name.
                        $vendor = User::where('id', $item->vendor_id)->first();
                        if(!empty($vendor->name)){
                            $val->vendor_name = $vendor->name;
                        } else {
                            $val->vendor_name = null;
                        }

                        //get sender name.
                        $sender = User::where('id', $item->created_by)->withTrashed()->first();
                        if(!empty($sender->name)){
                            $val->sender_name = $sender->name;
                        } else {
                            $val->sender_name = null;
                        }

                        $val->to_text = $item->to_text;
                        $val->write_your_text = $item->write_your_text;
                        $val->quantity = $item->quantity;
                        $val->order_item = $item;
                        $returnArray[] = $val;
                    }
            }

        }

        $paperTypes = paperTypes::where('status', 1)->get();

        return view('admin.vendororder.index', ['data'=>$returnArray, 'paper_type' => $paperTypes]);

    }

    public function greetingPendingOrderVendor()
    {
            //get zip based orders
            $paperTypes = paperTypes::where('status', 1)->get();

            $data = [];
            $orderItemBasedZipCode = [];
            $authUser = Auth::user();

            $orderItemBasedZipCode = OrderItem::where('section', '!=', 2)
                ->where('product_type', '!=', 3)
                ->whereNotIn('status', [2,3])
                ->whereNull('easy_parcel_order_no')
                ->whereRaw('FIND_IN_SET(?, multi_vendor_id)', [Auth::user()->id])
                ->whereDate('created_at', '<=', now()->subDays(3))
                ->get();


            $returnArray=[];
            foreach($orderItemBasedZipCode as $key => $item)
            {

                // dd($item);
                $image_array = [];

                if($item->product_type == 2){
                    if($item->custom_order != 1){
                        $product = Product::where('id',$item->product_id)->withTrashed()->first();
                        // dd($product->image);

                        if(!empty($item->image)){
                            $image_array[] = Storage::url('/order_print/'.$item->image);
                        } else {
                            $image_array[] = Storage::url('/product/'.$product->image);
                        }

                        if(!empty($product->image_2)){
                            $image_array[] = Storage::url('/product/'.$product->image_2);
                        }
                        if(!empty($product->image_3)){
                            $image_array[] = Storage::url('/product/'.$product->image_3);
                        }
                        if(!empty($product->image_4)){
                            $image_array[] = Storage::url('/product/'.$product->image_4);
                        }
                    } else {

                        if(!empty($item->image)){
                            $image_array[] = Storage::url('/order_print/'.$item->image);
                        }

                        if(!empty($item->image_2)){
                            $image_array[] = Storage::url('/order_print/'.$item->image_2);
                        }

                        if(!empty($item->image_3)){
                            $image_array[] = Storage::url('/order_print/'.$item->image_3);
                        }

                        if(!empty($item->image_4)){
                            $image_array[] = Storage::url('/order_print/'.$item->image_4);
                        }


                    }
                }
                if($item->product_type == 1){
                    if($item->custom_order != 1){
                        $post_card = PostCard::where('id',$item->product_id)->first();

                        if($item->image == null){
                            $item->image = Storage::url('/postcard/'.$post_card->image);
                        }else{
                            $image_array[] = Storage::url('/order_print/'.$item->image);
                        }
                        if(!empty($post_card->image_2)){
                            $image_array[] =Storage::url('/postcard/'.$post_card->image_2);
                        }
                    } else {
                        if(!empty($item->image)){
                            $image_array[] = Storage::url('/order_print/'.$item->image);
                        }

                        if(!empty($item->image_2)){
                            $image_array[] = Storage::url('/order_print/'.$item->image_2);
                        }

                    }
                }

                if($item->product_type == 3){
                    if(!empty($item->image)){
                        $image_array[] = Storage::url('/order_print/'.$item->image);
                    } else {
                       // $image_array[] = Storage::url('/product/'.$item->image);
                    }
                }
                // dd($image_array);




                $query = Order::query();
                $query->orderBy('id', 'DESC');
                $query->where('id',$item->order_id);
                $val = $query->first();
                // dd($val);

                $val->order_item_id = $item->id;


                //get order status from easy parcel.
            if(!empty($item->easy_parcel_order_no) && $item->easy_parcel_order_no != null){

                $domain = env('EASY_PARCEL_BASE_URL');

                $action = "MPParcelStatusBulk";
                $postparam = array(
                    'authentication'    => env('EASY_PARCEL_AUTHENTICATION_KEY'),
                    'api'    => 'EP-AoBYqujRY',
                    'bulk'    => array(
                        array(
                            'order_no'    => $item->easy_parcel_order_no,
                        ),

                    ),
                );

                $url = $domain . $action;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postparam));
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

                ob_start();
                $return = curl_exec($ch);
                ob_end_clean();
                curl_close($ch);

                $json = json_decode($return);
                if(!empty($json->result[0]->parcel[0]->ship_status)){
                    $val->order_status_name = $json->result[0]->parcel[0]->ship_status;
                } else {
                    $val->order_status_name = null;
                }

            } else {
                $orderStatus = OrderStatus::with(['orderStatusMaster'])->where('order_id', $val->id)->orderBy('id', 'DESC')->first();
                // dd($orderStatus);
                if(!empty($orderStatus->orderStatusMaster['name'])){
                    $val->order_status_name = $orderStatus->orderStatusMaster['name'];
                } else {
                    $val->order_status_name = null;
                }
            }

                    //get paper type listing
                    $paperTypeIds = explode(',', $val->paper_type_id);
                    $paperTypes = PaperTypes::whereIn('id', $paperTypeIds)->get();
                    $val['paperTypes'] = $paperTypes;

                    //get order name (Product name).
                    if($item->product_type == 1)
                    {
                        $product = PostCard::where('id', $item->product_id)->first();
                    } else if($item->product_type == 3){
                        $product = MailType::where('id', $item->mail_type_id)
                        ->first();
                    }
                    else
                    {
                        $product = Product::where('id', $item->product_id)->first();
                    }

                    if(!empty($product->name)){
                        if($item->product_type == 3){
                            $val->product_name = $item->attachment_original_name." - ".$product->name;
                        } else {
                            $val->product_name = $product->name;
                        }
                    } else {
                        $val->product_name = null;
                    }

                    //get sender list.
                    if(!empty($val->send_to)){
                        /* $recepientUsers = explode(',', $val->send_to);
                        $recepient = AddRecipient::whereIn('id', $recepientUsers)->get();

                        $val->recepients = $recepient; */
                    }

                    /* if(!empty($recepientUsers['recipientIds'][0])){
                        $val->recepients = $recepientUsers['recipientIds'];
                    } */

                    if(!empty($item->recipientIds['recipientIds'])){
                        $val->recepients = $item->recipientIds['recipientIds'];
                    }

                    //get sender name.
                    $sender = User::where('id', $item->created_by)->withTrashed()->first();
                    if(!empty($sender->name)){
                        $val->sender_name = $sender->name;
                    } else {
                        $val->sender_name = null;
                    }

                    // dd($val);
                    // end get sender list.
                    $val->to_text = $item->to_text;
                    $val->write_your_text = $item->write_your_text;
                    $val->quantity = $item->quantity;
                    $val->images = $image_array;
                    $val->order_item = $item;
                    $returnArray[] = $val;

                    //}
                }


                // dd($orderItemBasedZipCode);

       return view('admin.order.vendororder', ['data' => $returnArray, 'paper_type' => $paperTypes]);
    }

    public function postalMailPendingOrderVendor()
    {
            //get zip based orders
            $paperTypes = paperTypes::where('status', 1)->get();

            $data = [];
            $orderItemBasedZipCode = [];
            $authUser = Auth::user();

            $orderItems = OrderItem::where('product_type', '=', 3)
                ->whereNotIn('status', [2,3])
                ->whereNull('easy_parcel_order_no')
                ->get();

            foreach($orderItems as $items){
                $recepientUsers = Helper::checkVendorAndUserZipcode($items);

                if(!empty($recepientUsers['recipientIds'][0])){
                    $items->recipientIds = $recepientUsers;
                    $orderItemBasedZipCode[] = $items;
                }
            }

            $returnArray=[];
            foreach($orderItemBasedZipCode as $key => $item)
            {

                // dd($item);
                $image_array = [];

                if($item->product_type == 2){
                    if($item->custom_order != 1){
                        $product = Product::where('id',$item->product_id)->withTrashed()->first();
                        // dd($product->image);

                        if(!empty($item->image)){
                            $image_array[] = Storage::url('/order_print/'.$item->image);
                        } else {
                            $image_array[] = Storage::url('/product/'.$product->image);
                        }

                        if(!empty($product->image_2)){
                            $image_array[] = Storage::url('/product/'.$product->image_2);
                        }
                        if(!empty($product->image_3)){
                            $image_array[] = Storage::url('/product/'.$product->image_3);
                        }
                        if(!empty($product->image_4)){
                            $image_array[] = Storage::url('/product/'.$product->image_4);
                        }
                    } else {

                        if(!empty($item->image)){
                            $image_array[] = Storage::url('/order_print/'.$item->image);
                        }

                        if(!empty($item->image_2)){
                            $image_array[] = Storage::url('/order_print/'.$item->image_2);
                        }

                        if(!empty($item->image_3)){
                            $image_array[] = Storage::url('/order_print/'.$item->image_3);
                        }

                        if(!empty($item->image_4)){
                            $image_array[] = Storage::url('/order_print/'.$item->image_4);
                        }


                    }
                }
                if($item->product_type == 1){
                    if($item->custom_order != 1){
                        $post_card = PostCard::where('id',$item->product_id)->first();

                        if($item->image == null){
                            $item->image = Storage::url('/postcard/'.$post_card->image);
                        }else{
                            $image_array[] = Storage::url('/order_print/'.$item->image);
                        }
                        if(!empty($post_card->image_2)){
                            $image_array[] =Storage::url('/postcard/'.$post_card->image_2);
                        }
                    } else {
                        if(!empty($item->image)){
                            $image_array[] = Storage::url('/order_print/'.$item->image);
                        }

                        if(!empty($item->image_2)){
                            $image_array[] = Storage::url('/order_print/'.$item->image_2);
                        }

                    }
                }

                if($item->product_type == 3){
                    if(!empty($item->image)){
                        $image_array[] = Storage::url('/order_print/'.$item->image);
                    } else {
                       // $image_array[] = Storage::url('/product/'.$item->image);
                    }
                }
                // dd($image_array);




                $query = Order::query();
                $query->orderBy('id', 'DESC');
                $query->where('id',$item->order_id);
                $val = $query->first();
                // dd($val);

                $val->order_item_id = $item->id;


                //get order status from easy parcel.
            if(!empty($item->easy_parcel_order_no) && $item->easy_parcel_order_no != null){

                $domain = env('EASY_PARCEL_BASE_URL');

                $action = "MPParcelStatusBulk";
                $postparam = array(
                    'authentication'    => env('EASY_PARCEL_AUTHENTICATION_KEY'),
                    'api'    => 'EP-AoBYqujRY',
                    'bulk'    => array(
                        array(
                            'order_no'    => $item->easy_parcel_order_no,
                        ),

                    ),
                );

                $url = $domain . $action;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postparam));
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

                ob_start();
                $return = curl_exec($ch);
                ob_end_clean();
                curl_close($ch);

                $json = json_decode($return);
                if(!empty($json->result[0]->parcel[0]->ship_status)){
                    $val->order_status_name = $json->result[0]->parcel[0]->ship_status;
                } else {
                    $val->order_status_name = null;
                }

            } else {
                $orderStatus = OrderStatus::with(['orderStatusMaster'])->where('order_id', $val->id)->orderBy('id', 'DESC')->first();
                if(!empty($orderStatus->orderStatusMaster['name'])){
                    $val->order_status_name = $orderStatus->orderStatusMaster['name'];
                } else {
                    $val->order_status_name = null;
                }
            }

                    //get paper type listing
                    $paperTypeIds = explode(',', $val->paper_type_id);
                    $paperTypes = PaperTypes::whereIn('id', $paperTypeIds)->get();
                    $val['paperTypes'] = $paperTypes;

                    //get order name (Product name).
                    if($item->product_type == 1)
                    {
                        $product = PostCard::where('id', $item->product_id)->first();
                    } else if($item->product_type == 3){
                        $product = MailType::where('id', $item->mail_type_id)
                        ->first();
                    }
                    else
                    {
                        $product = Product::where('id', $item->product_id)->first();
                    }

                    if(!empty($product->name)){
                        if($item->product_type == 3){
                            $val->product_name = $item->attachment_original_name." - ".$product->name;
                        } else {
                            $val->product_name = $product->name;
                        }
                    } else {
                        $val->product_name = null;
                    }

                    //get sender list.
                    if(!empty($val->send_to)){
                        /* $recepientUsers = explode(',', $val->send_to);
                        $recepient = AddRecipient::whereIn('id', $recepientUsers)->get();

                        $val->recepients = $recepient; */
                    }

                    /* if(!empty($recepientUsers['recipientIds'][0])){
                        $val->recepients = $recepientUsers['recipientIds'];
                    } */

                    if(!empty($item->recipientIds['recipientIds'])){
                        $val->recepients = $item->recipientIds['recipientIds'];
                    }

                    //get sender name.
                    $sender = User::where('id', $item->created_by)->withTrashed()->first();
                    if(!empty($sender->name)){
                        $val->sender_name = $sender->name;
                    } else {
                        $val->sender_name = null;
                    }

                    // dd($val);
                    // end get sender list.
                    $val->to_text = $item->to_text;
                    $val->write_your_text = $item->write_your_text;
                    $val->quantity = $item->quantity;
                    $val->images = $image_array;
                    $val->order_item = $item;
                    $returnArray[] = $val;

                    //}
                }


                // dd($orderItemBasedZipCode);

       return view('admin.order.vendororder', ['data' => $returnArray, 'paper_type' => $paperTypes]);
    }

    public function ratingReviewPostal()
    {
            $ratings = Rating::with(['OrderItem'])->orderBy('id', 'DESC')->get();

            //get zip based orders
            $paperTypes = paperTypes::where('status', 1)->get();

            $data = [];
            $orderItemBasedZipCode = [];
            $authUser = Auth::user();


            foreach($ratings as $items){
                //dd($items->OrderItem->multi_vendor_id);

                if(!empty($items->OrderItem->multi_vendor_id)){
                    $orderVendorIds = explode(',', $items->OrderItem->multi_vendor_id);

                    if(in_array(Auth::user()->id, $orderVendorIds)){
                        if(!empty($items['OrderItem']->id)){
                            $items = !empty($items['OrderItem']) ? $items['OrderItem'] : null;

                            //For Postal Email
                            if($authUser->user_type == 6 && $items->product_type == 3){
                                $recepientUsers = Helper::checkVendorAndUserZipcode($items);

                                if(!empty($recepientUsers['recipientIds'][0])){
                                    $items->recipientIds = $recepientUsers;
                                    $orderItemBasedZipCode[] = $items;
                                }
                            }

                            if($authUser->user_type == 5 && $items->product_type != 3 && $items->section != 2){
                                $recepientUsers = Helper::checkVendorAndUserZipcode($items);

                                if(!empty($recepientUsers['recipientIds'][0])){
                                    $items->recipientIds = $recepientUsers;
                                    $orderItemBasedZipCode[] = $items;
                                }
                            }
                        }
                    }
                }
            }

            $returnArray=[];
            foreach($orderItemBasedZipCode as $key => $item)
            {

                // dd($item);
                $image_array = [];

                if($item->product_type == 2){
                    if($item->custom_order != 1){
                        $product = Product::where('id',$item->product_id)->withTrashed()->first();
                        // dd($product->image);

                        if(!empty($item->image)){
                            $image_array[] = Storage::url('/order_print/'.$item->image);
                        } else {
                            $image_array[] = Storage::url('/product/'.$product->image);
                        }

                        if(!empty($product->image_2)){
                            $image_array[] = Storage::url('/product/'.$product->image_2);
                        }
                        if(!empty($product->image_3)){
                            $image_array[] = Storage::url('/product/'.$product->image_3);
                        }
                        if(!empty($product->image_4)){
                            $image_array[] = Storage::url('/product/'.$product->image_4);
                        }
                    } else {

                        if(!empty($item->image)){
                            $image_array[] = Storage::url('/order_print/'.$item->image);
                        }

                        if(!empty($item->image_2)){
                            $image_array[] = Storage::url('/order_print/'.$item->image_2);
                        }

                        if(!empty($item->image_3)){
                            $image_array[] = Storage::url('/order_print/'.$item->image_3);
                        }

                        if(!empty($item->image_4)){
                            $image_array[] = Storage::url('/order_print/'.$item->image_4);
                        }


                    }
                }
                if($item->product_type == 1){
                    if($item->custom_order != 1){
                        $post_card = PostCard::where('id',$item->product_id)->first();

                        if($item->image == null){
                            $item->image = Storage::url('/postcard/'.$post_card->image);
                        }else{
                            $image_array[] = Storage::url('/order_print/'.$item->image);
                        }
                        if(!empty($post_card->image_2)){
                            $image_array[] =Storage::url('/postcard/'.$post_card->image_2);
                        }
                    } else {
                        if(!empty($item->image)){
                            $image_array[] = Storage::url('/order_print/'.$item->image);
                        }

                        if(!empty($item->image_2)){
                            $image_array[] = Storage::url('/order_print/'.$item->image_2);
                        }

                    }
                }

                if($item->product_type == 3){
                    if(!empty($item->image)){
                        $image_array[] = Storage::url('/order_print/'.$item->image);
                    } else {
                       // $image_array[] = Storage::url('/product/'.$item->image);
                    }
                }
                // dd($image_array);




                $query = Order::query();
                $query->orderBy('id', 'DESC');
                $query->where('id',$item->order_id);
                $val = $query->first();
                // dd($val);

                $val->order_item_id = $item->id;


                //get order status from easy parcel.
            if(!empty($item->easy_parcel_order_no) && $item->easy_parcel_order_no != null){

                $domain = env('EASY_PARCEL_BASE_URL');

                $action = "MPParcelStatusBulk";
                $postparam = array(
                    'authentication'    => env('EASY_PARCEL_AUTHENTICATION_KEY'),
                    'api'    => 'EP-AoBYqujRY',
                    'bulk'    => array(
                        array(
                            'order_no'    => $item->easy_parcel_order_no,
                        ),

                    ),
                );

                $url = $domain . $action;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postparam));
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

                ob_start();
                $return = curl_exec($ch);
                ob_end_clean();
                curl_close($ch);

                $json = json_decode($return);
                if(!empty($json->result[0]->parcel[0]->ship_status)){
                    $val->order_status_name = $json->result[0]->parcel[0]->ship_status;
                } else {
                    $val->order_status_name = null;
                }

            } else {
                $orderStatus = OrderStatus::with(['orderStatusMaster'])->where('order_id', $val->id)->orderBy('id', 'DESC')->first();
                if(!empty($orderStatus->orderStatusMaster['name'])){
                    $val->order_status_name = $orderStatus->orderStatusMaster['name'];
                } else {
                    $val->order_status_name = null;
                }
            }

                    //get paper type listing
                    $paperTypeIds = explode(',', $val->paper_type_id);
                    $paperTypes = PaperTypes::whereIn('id', $paperTypeIds)->get();
                    $val['paperTypes'] = $paperTypes;

                    //get rating and reveiw
                    $rating = Rating::where('order_item_id', $item->id)->first();
                    $val->rating = $rating;

                    //get order name (Product name).
                    if($item->product_type == 1)
                    {
                        $product = PostCard::where('id', $item->product_id)->first();
                    } else if($item->product_type == 3){
                        $product = MailType::where('id', $item->mail_type_id)
                        ->first();
                    }
                    else
                    {
                        $product = Product::where('id', $item->product_id)->first();
                    }

                    if(!empty($product->name)){
                        if($item->product_type == 3){
                            $val->product_name = $item->attachment_original_name." - ".$product->name;
                        } else {
                            $val->product_name = $product->name;
                        }
                    } else {
                        $val->product_name = null;
                    }

                    //get sender list.
                    if(!empty($val->send_to)){
                        /* $recepientUsers = explode(',', $val->send_to);
                        $recepient = AddRecipient::whereIn('id', $recepientUsers)->get();

                        $val->recepients = $recepient; */
                    }

                    /* if(!empty($recepientUsers['recipientIds'][0])){
                        $val->recepients = $recepientUsers['recipientIds'];
                    } */

                    if(!empty($item->recipientIds['recipientIds'])){
                        $val->recepients = $item->recipientIds['recipientIds'];
                    }

                    //get sender name.
                    $sender = User::where('id', $item->created_by)->withTrashed()->first();
                    if(!empty($sender->name)){
                        $val->sender_name = $sender->name;
                    } else {
                        $val->sender_name = null;
                    }

                    // dd($val);
                    // end get sender list.
                    $val->to_text = $item->to_text;
                    $val->write_your_text = $item->write_your_text;
                    $val->quantity = $item->quantity;
                    $val->images = $image_array;
                    $val->order_item = $item;
                    $returnArray[] = $val;

                    //}
                }


                // dd($orderItemBasedZipCode);

       return view('admin.vendororder.index', ['data' => $returnArray, 'paper_type' => $paperTypes]);
    }
    public function refundGift()
    {
        try {
            $data = [];

            $orderItem = OrderItem::where('vendor_id',auth()->user()->id)
                ->where('section', 2)
                ->whereIn('status', [3])
                ->get();
                // dd($user);
            $returnArray=[];
            foreach($orderItem as $key => $item)
            {
                $query = Order::query();
                $query->orderBy('id', 'DESC');
                $query->where('id',$item->order_id);
                $val = $query->first();

                $val->order_item_id = $item->id;

                //get order status from easy parcel.

                $val->order_status_name = "Cancelled";


                //get paper type listing
                $paperTypeIds = explode(',', $val->paper_type_id);
                $paperTypes = PaperTypes::whereIn('id', $paperTypeIds)->get();
                $val['paperTypes'] = $paperTypes;

                //get order name (Product name).
                $product = Product::where('id', $item->product_id)->withTrashed()->first();

                if(!empty($product->name)){
                    $val->product_name = $product->name;

                } else {
                    $val->product_name = null;
                }

                //get sender list.
                if(!empty($val->send_to)){
                    $recepientUsers = explode(',', $val->send_to);
                    $recepient = Contacts::whereIn('id', $recepientUsers)->get();

                    $val->recepients = $recepient;
                }

                //get sender name.
                $sender = User::where('id', $item->created_by)->withTrashed()->first();
                if(!empty($sender->name)){
                    $val->sender_name = $sender->name;
                } else {
                    $val->sender_name = null;
                }
                // end get sender list.
                $val->to_text = $item->to_text;
                $val->write_your_text = $item->write_your_text;
                $val->quantity = $item->quantity;
                $val->order_item = $item;
                $returnArray[] = $val;

            }
            $paperTypes = paperTypes::where('status', 1)->get();


            return view('admin.order.refund_gift', ['data'=>$returnArray, 'paper_type' => $paperTypes]);

        } catch(Exception $e) {
            dd($e->getMessage());
        }
    }
}
