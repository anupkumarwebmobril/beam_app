<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Mail;
use App\Helpers\Helper;
use Validator;
use DB;
use Illuminate\Support\Facades\Storage;
use App\Models\Occasion;// 'cart_id' => 'required',
use App\Models\Category;
use App\Models\Product;
use App\Models\NeDesigns;
use App\Models\Contacts;
use App\Models\Banner;
use App\Models\OrderPrint;
use App\Models\MailType;
use App\Models\UserFavoriteContacts;
use App\Models\PostCard;
use App\User;
use App\Models\UserDesignerRequest;
use App\Models\CartItem;
use App\Models\Wishlist;
use App\Models\Notification;
use App\Models\Faq;
use App\Models\Query;
use App\Models\ProductDetailsImage;
use Auth;
use App\Models\Cart;
use App\Models\Reminder;
use Carbon\Carbon;
use App\Models\Payment;
use App\Models\OrderItem;
use App\Models\Order;
use App\Models\ContentManagement;
use App\Models\OrderStatus;
use App\Models\PrintTypes;
use App\Models\ImportPostal;
use App\Models\EmailTemplate;
use App\Models\Promocode;

class HomeController extends Controller
{
    public function getProfile(Request $request)
    {
        try {
            $user = auth()->user();

            if (isset($user) && $user != null) {
                if (!empty($user->profile_image)) {
                    $user->full_url = Storage::url('/profile_pictures/' . $user->profile_image);
                }
                $result['code'] = 200;
                $result['error'] = false;
                $result['result'] = $user;
                return response()->json($result);
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function homeSearch(Request $request)
    {
        try {

            $validation = Validator::make($request->all(), [
                'keyword' => 'required|min:2',
            ]);

            if ($validation->fails()) {
                $result['code']     = config('messages.http_codes.validation');
                $result['error']    = true;
                $result['message']  = $validation->messages()->first();
                return response()->json($result);
            }
            $keyWord = $request['keyword'];

            if (!empty($request['type']) && $request['type'] == 2) {
                //gifthub search
                $searchResult = Occasion::where('status', '=', 1)
                    ->WhereIn('type', [2, 3])
                    ->where('name', 'LIKE', "%$keyWord%")->get();
            } else {
                //postal search
                $searchResult = Occasion::where('status', '=', 1)->WhereIn('type', [1])->where('name', 'LIKE', "%$keyWord%")->get();
            }


            if (!empty($searchResult[0])) {
                //make image url
                foreach ($searchResult as $key => $val) {
                    if (!empty($val->image)) {
                        $val->image_full_url = Storage::url('/occasion/' . $val->image);
                        $searchResult[$key] = $val;
                    }
                }
                $result['code'] = 200;
                $result['error'] = false;
                $result['result'] = $searchResult;
                return response()->json($result);
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }
        } catch (\Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function occasions(Request $request)
    {
        try {

            if (!empty($request['type']) && $request['type'] == 2) {
                //gifthub occasions
                //$searchResult = Occasion::where('status', '=', 1)->where('type', 2)->get();
                //return the category if product exist in it.
                $searchResult = DB::table('occasion')
                    ->where('occasion.status', '=', 1)
                    ->where('occasion.type', 2)
                    ->where('occasion.deleted_at', '=', null)
                    ->whereExists(function ($query) {
                        $query->select(DB::raw(1))
                                ->from('product')
                                ->whereNull('product.deleted_at')
                                ->whereRaw('product.occasion_id = occasion.id');
                    })
                    ->orderBy('order_by', 'asc')
                    ->get();
            } elseif (!empty($request['type']) && $request['type'] == 4) {
                $searchResult = DB::table('occasion')
                    ->where('occasion.status', '=', 1)
                    ->where('occasion.deleted_at', '=', null)
                    ->whereIn('occasion.type', [2,3])
                    ->whereExists(function ($query) {
                        $query->select(DB::raw(1))
                                ->from('product')
                                ->whereNull('product.deleted_at')
                                ->whereRaw('product.occasion_id = occasion.id');
                    })
                    ->orderBy('order_by', 'asc')
                    ->get();

            } else {
                //postal occasions
                //$searchResult = Occasion::where('status', '=', 1)->where('type', 1)->get();
                //return the category if product exist in it.
                $searchResult = DB::table('occasion')
                    ->where('occasion.status', '=', 1)
                    ->where('occasion.type', 1)
                    ->where('occasion.deleted_at', '=', null)
                    ->whereExists(function ($query) {
                        $query->select(DB::raw(1))
                                ->from('product')
                                ->whereNull('product.deleted_at')
                                ->whereRaw('product.occasion_id = occasion.id');

                    })
                    ->orderBy('order_by', 'asc')
                    ->get();
            }

            if (!empty($searchResult[0])) {
                //make image url
                foreach ($searchResult as $key => $val) {
                    if (!empty($val->image)) {
                        $val->image_full_url = Storage::url('/occasion/' . $val->image);
                        $searchResult[$key] = $val;
                    }
                }
                $result['code'] = 200;
                $result['error'] = false;
                $result['result'] = $searchResult;
                return response()->json($result);
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function categoryBasedOccasionId(Request $request)
    {
        try {

            $validation = Validator::make($request->all(), [
                'occasion_id' => 'required',
            ]);

            if ($validation->fails()) {
                $result['code']     = config('messages.http_codes.validation');
                $result['error']    = true;
                $result['message']  = $validation->messages()->first();
                return response()->json($result);
            }

            $searchResult = Category::where('status', '=', 1)
                ->where('occasion_id', '=', $request['occasion_id'])
                ->get();

            if (!empty($searchResult[0])) {
                $result['code'] = 200;
                $result['error'] = false;
                $result['result'] = $searchResult;
                return response()->json($result);
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function productBasedOccasionId(Request $request)
    {
        try {

            $validation = Validator::make($request->all(), [
                'occasion_id' => 'required',
            ]);

            if ($validation->fails()) {
                $result['code']     = config('messages.http_codes.validation');
                $result['error']    = true;
                $result['message']  = $validation->messages()->first();
                return response()->json($result);
            }

            $categoryIds = Category::where('status', '=', 1)
                ->where('occasion_id', '=', $request['occasion_id'])
                ->pluck('id');

            if (!empty($categoryIds[0])) {
                if (!empty($request['price_order'])) {
                    if ($request['price_order'] == 'asc') {
                        $orderBy = "asc";
                    }

                    if ($request['price_order'] == 'desc') {
                        $orderBy = "desc";
                    }
                }

                $query = Product::query();
                $query->select('id', 'category_id', 'name', 'price', 'image', 'min_order_qty', 'product_code');
                $query->where('status', '=', 1);
                $query->whereIn('category_id', $categoryIds);

                if (!empty($orderBy)) {
                    $query->orderBy('price', $orderBy);
                }

                if (!empty($request->product_id)) {
                    //This will work if user click on favroute product then this product will come in top
                    $query->orderByRaw('FIELD(id, "'.$request->product_id.'") DESC');
                }


                $products = $query->get();

                if (!empty($request->user_id)) {
                    $wishListProduct = Wishlist::where([
                        'user_id' => $request->user_id,
                        'type' => 1,
                    ])->pluck('product_id');
                }

                if (!empty($request->user_id)) {
                    $checkCartItems = CartItem::where([
                        'created_by' => $request->user_id,
                        'product_type' => 2,
                    ])->pluck('product_id');
                }

                if (!empty($products[0])) {
                    //make image url
                    foreach ($products as $key => $val) {
                        //get count of post cart item if added in cart
                        if (!empty($request->user_id)) {
                            if ($checkCartItems->contains($val->id)) {
                                //get count of quantity if product is added in cart.
                                $checkCartItem = CartItem::where([
                                    'product_id' => $val->id,
                                    'created_by' => $request->user_id,
                                    'product_type' => 2,
                                ])->first();

                                if (!empty($checkCartItem->id)) {
                                    $val->cart_quantity = $checkCartItem->quantity;
                                } else {
                                    $val->cart_quantity = 0;
                                }
                            } else {
                                $val->cart_quantity = 0;
                            }

                            if ($wishListProduct->contains($val->id)) {
                                $val->is_in_wishlist = 1;
                            } else {
                                $val->is_in_wishlist = 0;
                            }
                        } else {
                            $val->cart_quantity = 0;
                            $val->is_in_wishlist = 0;
                        }

                        if (!empty($val->image)) {
                            $val->image_full_url = Storage::url('/product/' . $val->image);
                            $searchResult[$key] = $val;
                        }
                    }

                    $result['code'] = 200;
                    $result['error'] = false;
                    $result['result'] = $products;
                    return response()->json($result);
                } else {
                    $result['code'] = config('messages.http_codes.validation');
                    $result['error'] = false;
                    $result['message'] = "No record found";
                    return response()->json($result);
                }
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function getNewDesignsBackup(Request $request)
    {
        try {

            $searchResult = NeDesigns::select('id', 'image')->where('status', '=', 1)->get();

            if (!empty($searchResult[0])) {
                //make image url
                foreach ($searchResult as $key => $val) {
                    if (!empty($val->image)) {
                        $val->image_full_url = Storage::url('/new_designs/' . $val->image);
                        $searchResult[$key] = $val;
                    }
                }
                $result['code'] = 200;
                $result['error'] = false;
                $result['result'] = $searchResult;
                return response()->json($result);
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function getNewDesigns(Request $request)
    {
        try {
            //New design will come according latest added product.
            // $searchResult = Product::select('id', 'image', 'occasion_id')
            //     ->where('status', '=', 1)
            //     ->where('type', '=', 1)
            //     ->orderBy('created_at', 'DESC')
            //     ->limit(10)
            //     ->get();

            $searchResult = Product::select('id', 'image', 'occasion_id')
                ->where('status', '=', 1)
                ->where('type', '=', 1)
                ->orderBy('created_at', 'DESC')
                ->limit(10)
                ->with('occasion')
                ->whereHas('occasion', function($query) {
                    $query->where('status', '=', '1');
                })
                ->get();
           // dd($searchResult);
            if (!empty($searchResult[0])) {
                //make image url
                foreach ($searchResult as $key => $val) {
                    if (!empty($val->image)) {
                        $val->image_full_url = Storage::url('/product/' . $val->image);
                        $searchResult[$key] = $val;
                    }

                    //return category info.
                    $occasion = Occasion::where('id', $val->occasion_id)->first();

                    if (!empty($occasion->id)) {
                        $val->occatoin_name = $occasion->name;
                        $val->occatoin_id = $occasion->id;
                    }
                }
                $result['code'] = 200;
                $result['error'] = false;
                $result['result'] = $searchResult;
                return response()->json($result);
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function favoriteContacts(Request $request)
    {
        try {

            $user = auth()->user();
            $userFavCont = UserFavoriteContacts::where('user_id', '=', $user->id)
                ->groupBy('contact_id')
                ->pluck('contact_id');

            $searchResult = Contacts::where('status', '=', 1)
                ->whereIn('id', $userFavCont)->get();

            if (!empty($searchResult[0])) {
                //make image url
                foreach ($searchResult as $key => $val) {
                    if (!empty($val->image)) {
                        $val->image_full_url = Storage::url('/favorite_contacts/' . $val->image);
                        $searchResult[$key] = $val;
                    }
                }
                $result['code'] = 200;
                $result['error'] = false;
                $result['result'] = $searchResult;
                return response()->json($result);
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function favoriteContactsBackup(Request $request)
    {
        try {

            $user = auth()->user();

            $searchResult = Contacts::where('status', '=', 1)
                ->where('created_by', $user->id)->get();

            if (!empty($searchResult[0])) {
                //make image url
                foreach ($searchResult as $key => $val) {
                    if (!empty($val->image)) {
                        $val->image_full_url = Storage::url('/favorite_contacts/' . $val->image);
                        $searchResult[$key] = $val;
                    }
                }
                $result['code'] = 200;
                $result['error'] = false;
                $result['result'] = $searchResult;
                return response()->json($result);
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function productBasedCategoryId(Request $request)
    {
        try {

            $validation = Validator::make($request->all(), [
                'category_id' => 'required',
            ]);

            if ($validation->fails()) {
                $result['code']     = config('messages.http_codes.validation');
                $result['error']    = true;
                $result['message']  = $validation->messages()->first();
                return response()->json($result);
            }

            $categoryArry = explode(',', $request['category_id']);

            if (!empty(count($categoryArry) > 0)) {
                if (!empty($request['price_order'])) {
                    if ($request['price_order'] == 'asc') {
                        $orderBy = "asc";
                    }

                    if ($request['price_order'] == 'desc') {
                        $orderBy = "desc";
                    }
                }

                $query = Product::query();
                $query->select('id', 'category_id', 'name', 'price', 'image', 'min_order_qty', 'product_code');
                $query->where('status', '=', 1);
                $query->whereIn('category_id', $categoryArry);

                if (!empty($orderBy)) {
                    $query->orderBy('price', $orderBy);
                }

                if (!empty($request->product_id)) {
                    //This will work if user click on favroute product then this product will come in top
                    $query->orderByRaw('FIELD(id, "'.$request->product_id.'") DESC');
                }

                $products = $query->get();

                if (!empty($request->user_id)) {
                    $wishListProduct = Wishlist::where([
                        'user_id' => $request->user_id,
                        'type' => 1,
                    ])->pluck('product_id');
                }

                if (!empty($request->user_id)) {
                    $checkCartItems = CartItem::where([
                        'created_by' => $request->user_id,
                        'product_type' => 2,
                    ])->pluck('product_id');
                }

                if (!empty($products[0])) {
                    //make image url
                    foreach ($products as $key => $val) {
                        //get count of post cart item if added in cart
                        if (!empty($request->user_id)) {
                            if ($checkCartItems->contains($val->id)) {
                                //get count of quantity if product is added in cart.
                                $checkCartItem = CartItem::where([
                                    'product_id' => $val->id,
                                    'created_by' => $request->user_id,
                                    'product_type' => 2,
                                ])->first();

                                if (!empty($checkCartItem->id)) {
                                    $val->cart_quantity = $checkCartItem->quantity;
                                } else {
                                    $val->cart_quantity = 0;
                                }
                            } else {
                                $val->cart_quantity = 0;
                            }

                            if ($wishListProduct->contains($val->id)) {
                                $val->is_in_wishlist = 1;
                            } else {
                                $val->is_in_wishlist = 0;
                            }
                        } else {
                            $val->cart_quantity = 0;
                            $val->is_in_wishlist = 0;
                        }

                        if (!empty($val->image)) {
                            $val->image_full_url = Storage::url('/product/' . $val->image);
                            $searchResult[$key] = $val;
                        }
                    }

                    $result['code'] = 200;
                    $result['error'] = false;
                    $result['result'] = $products;
                    return response()->json($result);
                } else {
                    $result['code'] = config('messages.http_codes.validation');
                    $result['error'] = false;
                    $result['message'] = "No record found";
                    return response()->json($result);
                }
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function banners(Request $request)
    {
        try {

            $searchResult = Banner::where('status', '=', 1)->orderBy('order_by','asc')->get();

            if (!empty($searchResult[0])) {
                //make image url
                foreach ($searchResult as $key => $val) {
                    if (!empty($val->image)) {
                        $val->image_full_url = Storage::url('/banner/' . $val->image);
                        $searchResult[$key] = $val;
                    }
                }
                $result['code'] = 200;
                $result['error'] = false;
                $result['result'] = $searchResult;
                return response()->json($result);
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function orderPrint(Request $request)
    {
        try {

            $validation = Validator::make($request->all(), [
                'print_type' => 'required',
                'image' => 'required',
                'mail_type' => 'required',
            ]);

            if ($validation->fails()) {
                $result['code']     = config('messages.http_codes.validation');
                $result['error']    = true;
                $result['message']  = $validation->messages()->first();
                return response()->json($result);
            }

            $user = auth()->user();
            $requestData['print_type'] = $request->print_type;
            $requestData['created_by'] = $user->id;
            $requestData['mail_type'] = $request->mail_type;
            //update image.
            $user = Helper::addOrderPrint($request, $user);
            if (!empty($user->image)) {
                $requestData['image'] = $user->image;
            }

            $emailData = [
                'name' => 'Deepak',
                'url' => Storage::url('/order_print/' . $user->image),
                'print_type' => $request->print_type
            ];
            //Helper::pr($requestData);die;
            $searchResult = OrderPrint::insert($requestData);

            if ($searchResult) {
                $to_email = "beampro591@gmail.com";
                $to_name = "anjay";
                Mail::send('emails.order_print', $emailData, function ($message) use ($to_name, $to_email) {
                    $message->from(env('MAIL_USERNAME'), env('MAIL_FROM_NAME'));
                    $message->sender(env('MAIL_USERNAME'), $to_name);
                    $message->to($to_email, $to_name);
                    $message->subject('Print order ');
                    //$message->priority(3);
                });

                $result['code'] = 200;
                $result['error'] = false;
                $result['message'] = "Print request sent successfully.";
                return response()->json($result);
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function getMailType(Request $request)
    {
        try {

            $searchResult = MailType::select('id', 'name', 'price', 'colored_price', 'image')->where('status', '=', 1)->get();

            if (!empty($searchResult[0])) {
                //make image url
                foreach ($searchResult as $key => $val) {
                    if (!empty($val->image)) {
                        $val->image_full_url = Storage::url('/mail_type/' . $val->image);
                        $searchResult[$key] = $val;
                    }
                }

                $result['code'] = 200;
                $result['error'] = false;
                $result['result'] = $searchResult;
                return response()->json($result);
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function addFavoriteContactInListBackup(Request $request)
    {
        try {

            $validation = Validator::make($request->all(), [
                'contact_id' => 'required',
            ]);


            if ($validation->fails()) {
                $result['code']     = config('messages.http_codes.validation');
                $result['error']    = true;
                $result['message']  = $validation->messages()->first();
                return response()->json($result);
            }
            $user = Auth()->user();
            $insert = $request->all();
            $contactIds = explode(',', $request->contact_id);
            $insertData['user_id'] = $user->id;
            foreach ($contactIds as $contactId) {
                $insertData['contact_id'] = $contactId;
                UserFavoriteContacts::insert($insertData);
            }
            $result['code'] = 200;
            $result['error'] = false;
            $result['message'] = 'Contact added successfully';
            return response()->json($result);
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function addFavoriteContactInList(Request $request)
    {
        try {

            $validation = Validator::make($request->all(), [
                'name' => 'required',
                'phone_number' => 'required',
            ]);


            if ($validation->fails()) {
                $result['code']     = config('messages.http_codes.validation');
                $result['error']    = true;
                $result['message']  = $validation->messages()->first();
                return response()->json($result);
            }

            $user = Auth()->user();

            $insert = $request->all();
            $insert['created_by'] = $user->id;

            //insert pic
            if ($request->hasFile('image')) {
                $user = Helper::addFavoriteContactsPic($request, $user);
                if (!empty($user->favorite_contacts_image)) {
                    $insert['image'] = $user->favorite_contacts_image;
                }
            }

            //insert or update contact in contact table
            $checkDuplciate = Contacts::where('phone_number', $insert['phone_number'])->first();
            if (!empty($checkDuplciate->id)) {
                $insertOrUpdate = "update";
                DB::table('contacts')->where('id', $checkDuplciate->id)->update($insert);
                $last_id = $checkDuplciate->id;
            } else {
                $insertOrUpdate = "insert";
                DB::table('contacts')->insert($insert);
                $last_id = DB::getPDO()->lastInsertId();
            }

            if (!empty($last_id)) {
                //add contact id in favourite contact list

                $checkFevDuplicate = UserFavoriteContacts::where([
                    'user_id' => $user->id,
                    'contact_id' => $last_id
                ])->count();

                if ($checkFevDuplicate == 0) {
                    UserFavoriteContacts::insert([
                        'user_id' => $user->id,
                        'contact_id' => $last_id
                    ]);
                }

                $searchResult = Contacts::where('id', $last_id)->first();
                if (!empty($searchResult->image)) {
                    $searchResult->full_url = Storage::url('/favorite_contacts/' . $searchResult->image);
                }

                $result['code'] = 200;
                $result['result'] = $searchResult;
                $result['error'] = false;
                if($insertOrUpdate == "insert") {
                    $result['message'] = 'Detail added successfully';
                } else {
                    $result['message'] = 'Detail updated successfully';
                }

                return response()->json($result);
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "Something went wrong";
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function postCardList(Request $request)
    {
        try {

            $query = PostCard::query();
            $query->select('id', 'name', 'price', 'image', 'min_order_qty', 'product_code');
            $query->where('status', '=', 1);
            $query->limit(15);
            $postcard = $query->get();

            if (!empty($request->user_id)) {
                $wishListProduct = Wishlist::where([
                    'user_id' => $request->user_id,
                    'type' => 2,
                ])->pluck('product_id');
            }

            if (!empty($request->user_id)) {
                $checkCartItems = CartItem::where([
                    'created_by' => $request->user_id,
                    'product_type' => 1,
                ])->pluck('product_id');
            }

            if (!empty($postcard[0])) {
                //make image url
                foreach ($postcard as $key => $val) {

                    //get count of post cart item if added in cart
                    if (!empty($request->user_id)) {
                        if ($checkCartItems->contains($val->id)) {
                            //get count of quantity if product is added in cart.
                            $checkCartItem = CartItem::where([
                                'product_id' => $val->id,
                                'created_by' => $request->user_id,
                                'product_type' => 1,
                            ])->first();

                            if (!empty($checkCartItem->id)) {
                                $val->cart_quantity = $checkCartItem->quantity;
                            } else {
                                $val->cart_quantity = 0;
                            }
                        } else {
                            $val->cart_quantity = 0;
                        }

                        if ($wishListProduct->contains($val->id)) {
                            $val->is_in_wishlist = 1;
                        } else {
                            $val->is_in_wishlist = 0;
                        }
                    } else {
                        $val->cart_quantity = 0;
                        $val->is_in_wishlist = 0;
                    }

                    if (!empty($val->image)) {
                        $val->image_full_url = Storage::url('/postcard/' . $val->image);
                        $searchResult[$key] = $val;
                    }
                }

                $result['code'] = 200;
                $result['error'] = false;
                $result['result'] = $postcard;
                return response()->json($result);
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function getDesignerList(Request $request)
    {
        try {
            // $validation = Validator::make($request->all(), [
            //     'latitude' => 'required',
            //     'longitude' => 'required',
            // ]);

            // if ($validation->fails()) {
            //     $result['code']     = config('messages.http_codes.validation');
            //     $result['error']    = true;
            //     $result['message']  = $validation->messages()->first();
            //     return response()->json($result);
            // }

            $latitude = $request->latitude;
            $longitude = $request->longitude;

            /* $query = User::query();
            $query->select('id', 'phone', 'name', 'email', 'user_type', 'rank', 'designer_charge', 'details', 'sample_design_image', 'sample_design_image_2', 'sample_design_image_3');
            $query->where('status', '=', 1);
            $query->where('user_type', '=', 3);
            $designers = $query->get(); */

            // if($latitude && $longitude) {

            //     $designers = User::select(DB::raw('*, ( 6367 * acos( cos( radians(' . $latitude . ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) * sin( radians( latitude ) ) ) ) AS distance'))
            //     ->having('distance', '<', 30)
            //     ->orderBy('distance')
            //     ->where('user_type', 3)
            //     ->where('status', 1)
            //     ->limit(20)
            //     ->get();

            //     $designers = User::select(DB::raw('*, ( 6367 * acos( cos( radians(' . $latitude . ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) * sin( radians( latitude ) ) ) ) AS distance'))
            //         ->having('distance', '<', 30)
            //         ->orderBy('distance')
            //         ->where('user_type', 3)
            //         ->where('status', 1)
            //         ->limit(20)
            //         ->get();

            //     if (empty($designers[0])) {
            //         $designers = User::select(DB::raw('*, ( 6367 * acos( cos( radians(' . $latitude . ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) * sin( radians( latitude ) ) ) ) AS distance'))
            //             ->having('distance', '<', 100)
            //             ->orderBy('distance')
            //             ->where('user_type', 3)
            //             ->where('status', 1)
            //             ->limit(20)
            //             ->get();
            //     }
            // } else {

            // }

            $designers = User::select('users.*')
            ->where('user_type', 3)
            ->where('status', 1)
            ->limit(20)
            ->get();

            $designers = User::select('users.*')
                ->where('user_type', 3)
                ->where('status', 1)
                ->limit(20)
                ->get();

            if (empty($designers[0])) {
                $designers = User::select('users.*')
                    ->where('user_type', 3)
                    ->where('status', 1)
                    ->limit(20)
                    ->get();
            }
            //->pluck('id');
            if (!empty($designers[0])) {
                //make image url
                foreach ($designers as $key => $val) {
                    //add profile full url
                    if (!empty($val->profile_image)) {
                        $val->profile_image = Storage::url('/profile_pictures/' . $val->profile_image);
                    }

                    $imageArray = [];
                    if (!empty($val->sample_design_image)) {
                        $imageArray[]['path'] = Storage::url('/sample_design_image/' . $val->sample_design_image);
                    }

                    if (!empty($val->sample_design_image_2)) {
                        $imageArray[]['path'] = Storage::url('/sample_design_image_2/' . $val->sample_design_image_2);
                    }

                    if (!empty($val->sample_design_image_3)) {
                        $imageArray[]['path'] = Storage::url('/sample_design_image_3/' . $val->sample_design_image_3);
                    }
                    if (!empty($imageArray[0])) {
                        $val->designer_sample_images = $imageArray;
                    } else {
                        $val->designer_sample_images = null;
                    }

                    //get status if chat request accepted or not
                    if (!empty($request->user_id)) {
                        $chatUsers = UserDesignerRequest::where('from_user_id', $request->user_id)->where('to_user_id', $val->id)->first();
                    }

                    if (!empty($chatUsers->id)) {
                        $val->is_request_accepted = $chatUsers->is_request_accepted;
                        $val->is_request_accepted_ios = $chatUsers->is_request_accepted;
                    } else {
                        $val->is_request_accepted = null;
                        $val->is_request_accepted_ios = 9;
                    }
                }

                $result['code'] = 200;
                $result['error'] = false;
                $result['result'] = $designers;
                return response()->json($result);
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function sendRequestForHireDesigner(Request $request)
    {
        try {

            $validation = Validator::make($request->all(), [
                'to_user_id' => 'required',
            ]);


            if ($validation->fails()) {
                $result['code']     = config('messages.http_codes.validation');
                $result['error']    = true;
                $result['message']  = $validation->messages()->first();
                return response()->json($result);
            }

            $user = Auth()->user();

            $insertData = $request->all();

            if($user->id == $request->to_user_id){
                $result['code'] = 400;
                $result['error'] = true;
                $result['message'] = 'Sorry, but you cannot send a designer request to yourself.';
                return response()->json($result);
            }
            $insertData['from_user_id'] = $user->id;

            //check of request alreay sent
            $duplicateRequest = UserDesignerRequest::where([
                'from_user_id' => $user->id,
                'to_user_id' => $insertData['to_user_id'],
                'is_request_accepted' => 0
            ])->first();

            if (empty($duplicateRequest->id)) {
                UserDesignerRequest::insert($insertData);
            } else {
                UserDesignerRequest::where('id', $duplicateRequest->id)->update($insertData);
            }

            //push notification start here.
            $message = 'You got designing request from user';

            $push_message = [
                'title' => 'User Request',
                'body' => $message,
                'type' => '0',
                'user_type' => '1'

            ];

            $user = User::where('id', $insertData['to_user_id'])->first();

            if (!empty($user->device_token)) {
                if ($user->device_type == 1) {
                    $sendPush = Helper::sendNotification($user->device_token, $push_message);
                } elseif ($user->device_type == 2) {
                    $sendPush = Helper::iosSendNotification($user->device_token, $push_message);
                }
            }

            $result['code'] = 200;
            $result['error'] = false;
            $result['message'] = 'Request sent successfully';
            return response()->json($result);
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function getListOfUserDesignerRequest(Request $request)
    {
        try {
            $authUser = Auth()->user();
            if ($authUser->user_type == 2) {
                $searchResult = UserDesignerRequest::where('from_user_id', '=', $authUser->id)->get();
            } else {
                $searchResult = UserDesignerRequest::where('to_user_id', '=', $authUser->id)
                    ->where('is_request_accepted', '!=', 2)->get();
            }

            $finalArray = [];
            if (!empty($searchResult[0])) {
                foreach ($searchResult as $row) {
                    if ($authUser->user_type == 2) {
                        $user = User::where('id', $row->to_user_id)->first();
                    } else {
                        $user = User::where('id', $row->from_user_id)->first();
                    }

                    if(!empty($user->id)) {
                        if(!empty($user->email)) {
                            $row->email = $user->email;
                        } else {
                            $row->email = null;
                        }

                        if(!empty($user->phone)) {
                            $row->phone = $user->phone;
                        } else {
                            $row->phone = null;
                        }

                        if(!empty($user->name)) {
                            $row->name = $user->name;
                        } else {
                            $row->name = null;
                        }

                        if(!empty($user->name)) {
                            $row->designer_charge = $user->designer_charge;
                        } else {
                            $row->designer_charge = null;
                        }


                        $row->device_type = $user->device_type;
                        if (!empty($user->profile_image)) {
                            $row->full_url = Storage::url('/profile_pictures/' . $user->profile_image);
                        } else {
                            $row->full_url = null;
                        }

                        //Return device token for both user.
                        $designerDetails = User::select('device_token')->where('id', $row->to_user_id)->first();
                        $userDetails = User::select('device_token')->where('id', $row->from_user_id)->first();
                        $row->user_device_token = $userDetails->device_token;
                        $row->designer_device_token = $designerDetails->device_token;
                        $finalArray[] = $row;
                    }
                }



                $result['code'] = 200;
                $result['error'] = false;
                $result['result'] = $finalArray;
                return response()->json($result);
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function checkIfAddressAttached($phone_number)
    {

        try {
            $contact = Contacts::select('id', 'name', 'phone_number', 'house_number', 'land_mark', 'city', 'zip_code', 'state')->where('phone_number', $phone_number)->first();
            if (empty($contact->id)) {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            } else {

                if (!empty($contact->house_number) or !empty($contact->city) or !empty($contact->zip_code) or !empty($contact->state)) {

                    return response()->json(
                        [
                            'code' => config('httpcodes.HTTP_OK'),
                            'message' => "Card deleted successfully",
                            'is_adress_added' => 1,
                            'result' => $contact,
                        ],
                        config('httpcodes.HTTP_OK')
                    );
                } else {
                    return response()->json(
                        [
                            'code' => config('httpcodes.HTTP_OK'),
                            'message' => "Card deleted successfully",
                            'is_adress_added' => 0,
                            'result' => null
                        ],
                        config('httpcodes.HTTP_OK')
                    );
                }
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function corporateCharityCategory(Request $request)
    {
        try {

            //$searchResult = Occasion::where('status', '=', 1)->where('type', 3)->get();
            $searchResult = DB::table('occasion')
                    ->where('occasion.status', '=', 1)
                    ->where('occasion.type', 3)
                    ->where('occasion.deleted_at', '=', null)
                    ->whereExists(function ($query) {
                        $query->select(DB::raw(1))
                                ->from('product')
                                ->whereNull('product.deleted_at')
                                ->whereRaw('product.occasion_id = occasion.id');

                    })
                    ->get();

            if (!empty($searchResult[0])) {
                //make image url
                foreach ($searchResult as $key => $val) {
                    if (!empty($val->image)) {
                        $val->image_full_url = Storage::url('/occasion/' . $val->image);
                        $searchResult[$key] = $val;
                    }
                }
                $result['code'] = 200;
                $result['error'] = false;
                $result['result'] = $searchResult;
                return response()->json($result);
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function bestSellingProduct(Request $request)
    {
        //for gifthub
        try {
            $query = $query = OrderItem::query();
            $query->select('product_id');
            $query->count('product_id');
            $query->where('section', '=', 2);
            $query->groupBy('product_id');
            $query->orderBy('product_id', 'DESC');
            $query->limit(10);
            $besetSelling = $query->pluck('product_id');

            if (!empty($besetSelling)) {

                /* $query = Product::query();
                $query->select('id', 'category_id', 'name', 'price', 'image');
                $query->where('status', '=', 1);
                $query->where('type', '=', 2);
                $query->limit(5);
                $products = $query->get(); */

                $query = Product::query();
                $query->select('id', 'category_id', 'name', 'price', 'image', 'min_order_qty', 'product_code');
                $query->whereIn('id', $besetSelling);
                $products = $query->get();

                if (!empty($request->user_id)) {
                    $wishListProduct = Wishlist::where([
                        'user_id' => $request->user_id,
                        'type' => 1,
                    ])->pluck('product_id');
                }

                if (!empty($request->user_id)) {
                    $checkCartItems = CartItem::where([
                        'created_by' => $request->user_id,
                        'product_type' => 2,
                    ])->pluck('product_id');
                }

                if (!empty($products[0])) {
                    //make image url
                    foreach ($products as $key => $val) {
                        //get count of post cart item if added in cart
                        if (!empty($request->user_id)) {
                            if ($checkCartItems->contains($val->id)) {
                                //get count of quantity if product is added in cart.
                                $checkCartItem = CartItem::where([
                                    'product_id' => $val->id,
                                    'created_by' => $request->user_id,
                                    'product_type' => 2,
                                ])->first();

                                if (!empty($checkCartItem->id)) {
                                    $val->cart_quantity = $checkCartItem->quantity;
                                } else {
                                    $val->cart_quantity = 0;
                                }
                            } else {
                                $val->cart_quantity = 0;
                            }

                            if ($wishListProduct->contains($val->id)) {
                                $val->is_in_wishlist = 1;
                            } else {
                                $val->is_in_wishlist = 0;
                            }
                        } else {
                            $val->cart_quantity = 0;
                            $val->is_in_wishlist = 0;
                        }

                        if (!empty($val->image)) {
                            $val->image_full_url = Storage::url('/product/' . $val->image);
                            $searchResult[$key] = $val;
                        }
                    }

                    $result['code'] = 200;
                    $result['error'] = false;
                    $result['result'] = $products;
                    return response()->json($result);
                } else {
                    $result['code'] = config('messages.http_codes.validation');
                    $result['error'] = false;
                    $result['message'] = "No record found";
                    return response()->json($result);
                }
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function newGiftNearYou(Request $request)
    {
        try {
            //New design will come according latest added product.
            $searchResult = Product::select('id', 'category_id', 'name', 'price', 'image', 'min_order_qty', 'product_code')
                ->where('status', '=', 1)
                ->where('type', '=', 2)
                ->orderBy('created_at', 'DESC')
                ->limit(10)
                ->get();

            if (!empty($request->user_id)) {
                $wishListProduct = Wishlist::where([
                    'user_id' => $request->user_id,
                    'type' => 1,
                ])->pluck('product_id');
            }

            if (!empty($request->user_id)) {
                $checkCartItems = CartItem::where([
                    'created_by' => $request->user_id,
                    'product_type' => 2,
                ])->pluck('product_id');
            }

            if (!empty($searchResult[0])) {
                //make image url
                foreach ($searchResult as $key => $val) {
                    //get count of post cart item if added in cart
                    if (!empty($request->user_id)) {
                        if ($checkCartItems->contains($val->id)) {
                            //get count of quantity if product is added in cart.
                            $checkCartItem = CartItem::where([
                                'product_id' => $val->id,
                                'created_by' => $request->user_id,
                                'product_type' => 2,
                            ])->first();

                            if (!empty($checkCartItem->id)) {
                                $val->cart_quantity = $checkCartItem->quantity;
                            } else {
                                $val->cart_quantity = 0;
                            }
                        } else {
                            $val->cart_quantity = 0;
                        }

                        if ($wishListProduct->contains($val->id)) {
                            $val->is_in_wishlist = 1;
                        } else {
                            $val->is_in_wishlist = 0;
                        }
                    } else {
                        $val->cart_quantity = 0;
                        $val->is_in_wishlist = 0;
                    }

                    if (!empty($val->image)) {
                        $val->image_full_url = Storage::url('/product/' . $val->image);
                        $searchResult[$key] = $val;
                    }
                }
                $result['code'] = 200;
                $result['error'] = false;
                $result['result'] = $searchResult;
                return response()->json($result);
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function addToWishList(Request $request)
    {
        try {

            $validation = Validator::make($request->all(), [
                'product_id' => 'required',
            ]);

            if ($validation->fails()) {
                $result['code']     = config('messages.http_codes.validation');
                $result['error']    = true;
                $result['message']  = $validation->messages()->first();
                return response()->json($result);
            }

            $user = Auth()->user();
            $inputs = $request->all();
            $inputs['user_id'] = $user->id;

            if (!empty($inputs['delete']) && ($inputs['delete'] == 1)) {
                if (Wishlist::where(['product_id' => $request['product_id'], 'user_id' => $user->id])->delete()) {
                    $result['code'] = 200;
                    $result['error'] = false;
                    $result['message'] = "Successfully deleted from wishlist";
                    return response()->json($result);
                } else {
                    $result['code'] = config('messages.http_codes.validation');
                    $result['error'] = false;
                    $result['message'] = "Something went wrong";
                    return response()->json($result);
                }
            } else {
                unset($inputs['delete']);
                //check duplicate.
                $duplicate = Wishlist::where(['product_id' => $request['product_id'], 'user_id' => $user->id])->first();

                if (!empty($duplicate->id)) {
                    //product already added.
                    $status = true;
                } else {
                    //insert product
                    $searchResult = Wishlist::insert($inputs);
                    $status = true;
                }


                if (!empty($status)) {
                    $result['code'] = 200;
                    $result['error'] = false;
                    $result['message'] = "Successfully added to wishlist";
                    return response()->json($result);
                } else {
                    $result['code'] = config('messages.http_codes.validation');
                    $result['error'] = false;
                    $result['message'] = "Something went wrong";
                    return response()->json($result);
                }
            }
        } catch (\Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function getWishList(Request $request)
    {
        try {
            $user = auth()->user();
            $products = null;
            if (isset($user) && $user != null) {
                $userwishList = Wishlist::select('product_id')->where('user_id', $user->id)->pluck('product_id');

                if (!empty($userwishList[0])) {
                    $query = Product::query();
                    $query->select('id', 'category_id', 'name', 'price', 'image', 'min_order_qty', 'product_code');
                    $query->where('status', '=', 1);
                    $query->whereIn('id', $userwishList);

                    $products = $query->get();


                    //make image url
                    foreach ($products as $key => $val) {
                        //get count of post cart item if added in cart
                        if (!empty($request->user_id)) {
                            $checkCartItem = CartItem::where([
                                'product_id' => $val->id,
                                'created_by' => $request->user_id,
                                'product_type' => 2,
                            ])->first();

                            if (!empty($checkCartItem->id)) {
                                $val->cart_quantity = $checkCartItem->quantity;
                            } else {
                                $val->cart_quantity = 0;
                            }
                        } else {
                            $val->cart_quantity = 0;
                        }

                        if (!empty($val->image)) {
                            $val->image_full_url = Storage::url('/product/' . $val->image);
                            $searchResult[$key] = $val;
                        }
                    }
                }

                $result['code'] = 200;
                $result['error'] = false;
                $result['result'] = $products;
                return response()->json($result);
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function notifications(Request $request)
    {
        try {
            $user = auth()->user();
            
               if($user->device_type == 2){
                 if($user->version <= 1.5 || $user->version == null){
                    $user = Auth::user()->token();
                    User::where('id', Auth::user()->id)->update(['otp' => null, 'device_token' => null]);
                    Auth::user()->tokens->each(function($token, $key) {
                        $token->delete();
                    });
                    $user->revoke();
                    return response()->json(['error' => true, 'code'=>401,'message' => 'Unauthenticated.'], 401);
                    
                }
            }else{
                if($user->version <= 3.6 || $user->version == null){
                    $user = Auth::user()->token();
                    User::where('id', Auth::user()->id)->update(['otp' => null, 'device_token' => null]);

                    Auth::user()->tokens->each(function($token, $key) {
                        $token->delete();
                    });

                    $user->revoke();
                    return response()->json(['error' => true, 'code'=>401,'message' => 'Unauthenticated.'], 401);
                
                }
            } 
            
            if (isset($user) && $user != null) {
                $notifications = Notification::where('user_id', $user->id)->update([
                    'is_notification_seen' => 1
                ]);

                $notifications = Notification::select('id', 'user_id', 'message')
                ->where('user_id', $user->id)
                ->orderBy('id', "DESC")
                ->get();
                //$notifications = Notification::select('id', 'user_id', 'message')->get();

                $result['code'] = 200;
                $result['error'] = false;
                $result['result'] = $notifications;
                return response()->json($result);
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function deleteNotification($id)
    {
        try {
            $ifRecordExist = Notification::where('id', $id)->count();
            if ($ifRecordExist < 1) {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }

            Notification::where('id', $id)->delete();

            $result['code'] = 200;
            $result['error'] = false;
            $result['message'] = "Notification deleted";
            return response()->json($result);
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function delete_favoriteContacts($id)
    {
        try {
            $userId = Auth::user()->id;

            $ifRecordExist = UserFavoriteContacts::where('user_id', $userId)->where('contact_id', $id)->count();
            if ($ifRecordExist < 1) {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }

            UserFavoriteContacts::where('user_id', $userId)->where('contact_id', $id)->delete();

            $result['code'] = 200;
            $result['error'] = false;
            $result['message'] = "Favorite Contacts deleted";
            return response()->json($result);
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function faq()
    {
        try {
            $faq = Faq::get();

            $result['code'] = 200;
            $result['error'] = false;
            $result['result'] = $faq;
            return response()->json($result);
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function mailToSupport(Request $request)
    {
        try {

            $validation = Validator::make($request->all(), [
                'email' => 'required|email',
                'query' => 'required',
            ]);

            if ($validation->fails()) {
                $result['code']     = config('messages.http_codes.validation');
                $result['error']    = true;
                $result['message']  = $validation->messages()->first();
                return response()->json($result);
            }

            $inputs = $request->all();

            if (Query::insert($inputs)) {
                $result['code'] = 200;
                $result['error'] = false;
                $result['message'] = "Query added successfully.";
                return response()->json($result);
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "Something went wrong";
                return response()->json($result);
            }
        } catch (\Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    // *********************************** Manish code **********************************///
    public function getproductdetails(Request $request)
    {
        try {

            $validation = Validator::make($request->all(), [
                'product_id' => 'required',
                'product_type' => 'required',
            ]);
            if ($validation->fails()) {
                $result['code']     = config('messages.http_codes.validation');
                $result['error']    = true;
                $result['message']  = $validation->messages()->first();
                return response()->json($result);
            }
            $product_id = $request->product_id;
            // dd($product_id);
            $product_type = $request->product_type;
            // dd($product_type);
            if (isset($product_type) && $product_type == '2') {
                $product = Product::where('id', $product_id)->first();
                $temparray = [];

                if(!empty($product->image)) {
                    $temparray[] = array('image_full_url' => Storage::URL('product/' . $product->image));
                }

                if(!empty($product->image_2)) {
                    $temparray[] = array('image_full_url' => Storage::URL('product/' . $product->image_2));
                }

                if(!empty($product->image_3)) {
                    $temparray[] = array('image_full_url' => Storage::URL('product/' . $product->image_3));
                }

                if(!empty($product->image_4)) {
                    $temparray[] = array('image_full_url' => Storage::URL('product/' . $product->image_4));
                }

                if(!empty($product->image_5)) {
                    $temparray[] = array('image_full_url' => Storage::URL('product/' . $product->image_5));
                }

                if(!empty($product->image_6)) {
                    $temparray[] = array('image_full_url' => Storage::URL('product/' . $product->image_6));
                }

                if(!empty($product->image_7)) {
                    $temparray[] = array('image_full_url' => Storage::URL('product/' . $product->image_7));
                }

                if(!empty($product->image_8)) {
                    $temparray[] = array('image_full_url' => Storage::URL('product/' . $product->image_8));
                }

                if(!empty($product->image_9)) {
                    $temparray[] = array('image_full_url' => Storage::URL('product/' . $product->image_9));
                }

                if(!empty($product->image_10)) {
                    $temparray[] = array('image_full_url' => Storage::URL('product/' . $product->image_10));
                }

                unset($product->image, $product->image_2, $product->image_3, $product->image_4);

                $product->image = $temparray;

                $result['code'] = 200;
                $result['error'] = false;
                $result['result']  = $product;
                return response()->json($result);
            } elseif (isset($product_type) && $product_type == '1') {
                $postcard = PostCard::where('id', $product_id)->first();

                $temparray = [];
                $temparray[] = array('image_full_url' => Storage::URL('postcard/' . $postcard->image));
                $temparray[] = array('image_full_url' => Storage::URL('postcard/' . $postcard->image_2));

                unset($postcard->image, $postcard->image_2);

                $postcard->image = $temparray;

                $result['code'] = 200;
                $result['error'] = false;
                $result['result']  = $postcard;
                return response()->json($result);
            }
        } catch (\Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }
    // ***********************************************************************************************//
    public function getcartdetails(Request $request)
    {
        try {
            $user =  Auth::user();
            if($user->device_type == 2){
                 if($user->version <= 1.5 || $user->version == null){
                    $user = Auth::user()->token();
                    User::where('id', Auth::user()->id)->update(['otp' => null, 'device_token' => null]);
                    Auth::user()->tokens->each(function($token, $key) {
                        $token->delete();
                    });
                    $user->revoke();
                    return response()->json(['error' => true, 'code'=>401,'message' => 'Unauthenticated.'], 401);
                    
                }
            }else{
                if($user->version <= 3.6 || $user->version == null){
                    $user = Auth::user()->token();
                    User::where('id', Auth::user()->id)->update(['otp' => null, 'device_token' => null]);

                    Auth::user()->tokens->each(function($token, $key) {
                        $token->delete();
                    });

                    $user->revoke();
                    return response()->json(['error' => true, 'code'=>401,'message' => 'Unauthenticated.'], 401);
                
                }
            } 
            $validation = Validator::make($request->all(), [
                // 'cart_id' => 'required',
            ]);
            if ($validation->fails()) {
                $result['code']     = 400;
                $result['error']    = true;
                $result['message']  = $validation->messages()->first();
                return response()->json($result);
            }
            //   if(!$request->version){
            //     $result['code']     = 400;
            //     $result['error']    = true;
            //     $result['message']  = 'Please update the latest build from the store.';
            //     return response()->json($result);
            // }
            $user = Auth::user()->id;
            // dd($user);
            // $cart = Cart::where('created_by',$user)->get();
            // // dd($cart);
            // foreach($cart as $key => $value)
            // {
            $cart_data = CartItem::where('created_by', $user)->where('status', 1)->orderBy('id','desc')->get();
           //  dd($cart_data);
            $total = null;
            foreach ($cart_data as $key => $value) {
                // dd($value->quantity);
                if(!empty($value->print_charge)) {
                    $value->print_charge = floatVal($value->print_charge);
                }

                if(!empty($value->designer_charge)) {
                    $value->designer_charge = floatVal($value->designer_charge);
                }

                if ($value->product_type == 1) {

                    // dd($check);

                    if($value->custom_order == 1) {
                        //this data will come from directoy cart item table only

                        $man = $value->price;

                        $total += $man;
                        $value->price = floatVal($value->price);
                        $value->image = Storage::URL('order_print/' . $value->image);

                    } else {
                        $check = PostCard::where('id', $value->product_id)->withTrashed()->first();
                        $man = ($check->price) * ($value->quantity);
                        // dd($man);
                        $total += $man;
                        $value->name = $check->name;
                        $value->price = floatVal($check->price);
                        if (!empty($value->image)) {
                            $value->image = Storage::URL('order_print/' . $value->image);
                        } else {
                            $value->image = Storage::URL('postcard/' . $check->image);
                        }

                    }
                    if(!empty($check->min_order_qty)) {
                        $value->min_order_qty = $check->min_order_qty;
                    } else {
                        $value->min_order_qty = 0;
                    }

                    $value->shipping_fee = 0;

                    // dd($check->quantity);
                } elseif ($value->product_type == 2) {


                    if($value->custom_order == 1) {
                        //this data will come from directoy cart item table only

                        $man = $value->price;

                        $total += $man;
                        $value->price = floatVal($value->price);
                        $value->image = Storage::URL('order_print/' . $value->image);
                    } else {
                        $check = Product::where('id', $value->product_id)->first();
                        $quantity = 1;
                        $man = ($check->price) * ($value->quantity);
                        $total += $man;
                        $value->name = $check->name;
                        $value->price = floatVal($check->price);
                        if (!empty($value->image)) {
                            $value->image = Storage::URL('order_print/' . $value->image);
                        } else {
                            $value->image = Storage::URL('product/' . $check->image);
                        }

                    }

                    if(!empty($check->min_order_qty)) {
                        $value->min_order_qty = $check->min_order_qty;
                    } else {
                        $value->min_order_qty = 0;
                    }

                    $value->shipping_fee = 0;
                } else {

                    $check = MailType::select('mail_type.id', 'mail_type.name', 'mail_type.price', 'mail_type.shipping_fee', 'cart_item.pdf_page_count')
                        ->join('cart_item', 'cart_item.mail_type_id', '=', 'mail_type.id')
                        ->where('mail_type.id', $value->product_id)
                        ->where('created_by', auth()->user()->id)->withTrashed()
                        ->first();
                        //  dd($check);
                        $quantity = 1;
                        $ayu = ($check->price) * ($check->pdf_page_count);

                        // dd($ayu);
                        $man = ($ayu) * ($value->quantity);
                        // dd($man);
                        $total += $man;
                        $value->name = $value->name;
                        $value->price = round($value->price, 2);

                    // $value->shipping_fee = (int)$check->shipping_fee;
                                        $value->shipping_fee = (float)$check->shipping_fee;


                    if (!empty($value->image)) {
                        $value->image = Storage::URL('order_print/' . $value->image);
                    } else {
                        $value->image = Storage::URL('product/' . $check->image);
                    }
                    //There is no minimum order quantity for attachment.
                    $value->min_order_qty = 0;
                }


            }
            // dd(432323);
            // }
            // dd($cart_data);
           // dd('akl==');
            if ($cart_data) {
                $result['code'] = 200;
                $result['error'] = false;
                // $result['result']['top']  = $top;
                $result['result']['data'] = $cart_data;
                $result['result']['total'] = round($total, 2);

                return response()->json($result);
            } else {

                $result['code'] = 401;
                $result['success'] = false;
                $result['message']  = "No Data";
                // $result['result']['trending']  = $treand;
                return response()->json($result);
            }
        } catch (Exception $e) {
            dd($e->getMessage());
            return response()->json([
                'status_code' => 500,
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ]);
        }
    }

    public function get_custom_order_list(Request $request)
    {
        try {

            $user = Auth::user()->id;

            $cart_data = CartItem::select('id', 'product_type', 'price', 'image', 'designer_id')->where('created_by', $user)
                ->where('custom_order', 1)
                ->where('status', 0)
                ->get();

            foreach ($cart_data as $key => $value) {
                //this data will come from directoy cart item table only
                $value->price = floatVal($value->price);
                $value->image = Storage::URL('order_print/' . $value->image);

                //get designer information
                $designers = User::where('id', $value->designer_id)->first();
                if(!empty($designers->id)) {
                    $value->designer_profile_image = Storage::url('/profile_pictures/' . $designers->profile_image);
                    $value->designer_name = $designers->name;
                }

            }

            if ($cart_data) {
                $result['code'] = 200;
                $result['error'] = false;
                $result['result']['data'] = $cart_data;

                return response()->json($result);
            } else {
                $result['code'] = 401;
                $result['success'] = false;
                $result['message']  = "No Data";
                // $result['result']['trending']  = $treand;
                return response()->json($result);
            }
        } catch (Exception $e) {
            return response()->json([
                'status_code' => 500,
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ]);
        }
    }

    public function acceptRejectCustomOrder(Request $request)
    {
        try {
            $validation = Validator::make($request->all(), [
                'id' => 'required',
                // 'designer_id' => 'required',
                'status' => 'required',
            ]);
            if ($validation->fails()) {
                $result['code']     = 400;
                $result['error']    = true;
                $result['message']  = $validation->messages()->first();
                return response()->json($result);
            }

            $ifRecordExist = CartItem::where('id', $request['id'])->count();
            if ($ifRecordExist < 1) {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }

            $user = Auth::user()->id;
            $data = [];
            $data['status'] = $request->status;


            if (CartItem::where('id', $request['id'])->update($data)) {
                $cartItem = CartItem::where('id', $request['id'])->first();

                //push notification start here.
                if($request->status == 1) {
                    if($request->designer_id){
                        $user_designer_image = UserDesignerRequest::where('from_user_id', $user)->where('to_user_id', $request->designer_id)->update([
                            'image'  => $cartItem->image,
                            'image_2'=> $cartItem->image_2,
                            'image_3'=> $cartItem->image_3,
                            'image_4'=> $cartItem->image_4,
                            'attachment_original_name'=>$cartItem->attachment_original_name,
                        ]);
                    }

                    $message = 'Your designing request has been accepted';
                } else {
                    $message = 'Your designing request has been rejected';
                }


                $push_message = [
                    'title' => 'Designer Request',
                    'body' => $message,
                    'type' => '0',
                    'user_type' => '1'

                ];

                $user = User::where('id', $cartItem['designer_id'])->first();

                if (!empty($user->device_token)) {
                    if ($user->device_type == 1) {
                        $sendPush = Helper::sendNotification($user->device_token, $push_message);
                    } elseif ($user->device_type == 2) {
                        $sendPush = Helper::iosSendNotification($user->device_token, $push_message);
                    }
                }

                $notification = [
                    'user_id' => $user->id,
                    'message' => $message,
                ];

                Notification::create($notification);
                //push notification end here.

                $result['code'] = 200;
                $result['error'] = false;
                return response()->json($result);
            } else {
                $result['code'] = 401;
                $result['success'] = false;
                $result['message']  = "Something went wrong";
                // $result['result']['trending']  = $treand;
                return response()->json($result);
            }
        } catch (Exception $e) {
            return response()->json([
                'status_code' => 500,
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ]);
        }
    }
    /////////***************************Reminders**************************************************//

    public function postreminders(Request $request)
    {
        try {
            $validation = Validator::make($request->all(), [

                'title' => 'required',
                // 'image' => 'required',
                'date' =>  'required',
                'time'  => 'required',
                'remind_me' =>  'required',
            ]);
            if ($validation->fails()) {
                $result['code']     = 400;
                $result['error']    = true;
                $result['message']  = $validation->messages()->first();
                return response()->json($result);
            }
            $user = Auth::user()->id;
            $remind = new Reminder();
            $remind->title = $request->title;
            $remind->date = $request->date;
            $remind->time = $request->time;
            $remind->remind_me = $request->remind_me;
            $remind->user_id = $user;
            if ($request->hasFile('image')) {

                $image = $request->file('image');
                $name = $image->getClientOriginalName();
                $extention = \File::extension($name);
                $imageNameWithExtention = mt_rand(1000, 9999) . '_' . date('Y_m_d_h_i_s') . '.' . $extention;

                Storage::putFileAs('/reminder/', $request->file('image'), $imageNameWithExtention);

                $remind->image = $imageNameWithExtention;
            }
            $remind->save();
            if ($remind) {
                $result['code'] = 200;
                $result['error'] = false;
                $result['result']  = $remind;
                return response()->json($result);
            } else {
                $result['code'] = 401;
                $result['success'] = false;
                $result['message']  = "No Data";
                // $result['result']['trending']  = $treand;
                return response()->json($result);
            }
        } catch (Exception $e) {
            return response()->json([
                'status_code' => 500,
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ]);
        }
    }

    public function updatereminders(Request $request)
    {
        try {
            $validation = Validator::make($request->all(), [
                'id' => 'required',
                'title' => 'required',
                // 'image' => 'required',
                'date' =>  'required',
                'time'  => 'required',
                'remind_me' =>  'required',
            ]);
            if ($validation->fails()) {
                $result['code']     = 400;
                $result['error']    = true;
                $result['message']  = $validation->messages()->first();
                return response()->json($result);
            }

            $ifRecordExist = Reminder::where('id', $request['id'])->count();
            if ($ifRecordExist < 1) {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }

            $user = Auth::user()->id;
            $remind = [];
            $remind['title'] = $request->title;
            $remind['date'] = $request->date;
            $remind['time'] = $request->time;
            $remind['remind_me'] = $request->remind_me;
            $remind['user_id'] = $user;
            if ($request->hasFile('image')) {

                $image = $request->file('image');
                $name = $image->getClientOriginalName();
                $extention = \File::extension($name);
                $imageNameWithExtention = mt_rand(1000, 9999) . '_' . date('Y_m_d_h_i_s') . '.' . $extention;

                Storage::putFileAs('/reminder/', $request->file('image'), $imageNameWithExtention);

                $remind['image'] = $imageNameWithExtention;
            }

            if (Reminder::where('id', $request['id'])->update($remind)) {
                $result['code'] = 200;
                $result['error'] = false;
                $result['result']  = $remind;
                return response()->json($result);
            } else {
                $result['code'] = 401;
                $result['success'] = false;
                $result['message']  = "Something went wrong";
                // $result['result']['trending']  = $treand;
                return response()->json($result);
            }
        } catch (Exception $e) {
            return response()->json([
                'status_code' => 500,
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ]);
        }
    }

    public function getreminders(Request $request)
    {
        try {
            $validation = Validator::make($request->all(), []);
            if ($validation->fails()) {
                $result['code']     = 400;
                $result['error']    = true;
                $result['message']  = $validation->messages()->first();
                return response()->json($result);
            }



            $current_month = date('m', strtotime('now'));
            $record_data = array();
            for ($i = $current_month; $i < $current_month + 36; $i++) {
                $month = date('m', mktime(0, 0, 0, $i, 1));
                $year = date('Y', mktime(0, 0, 0, $i, 1));
                $month_name = date('F', mktime(0, 0, 0, $i, 1));
                $reminder_data = Reminder::whereMonth('date', $month)
                ->whereYear('date', $year)->where('user_id', Auth::user()->id)
                ->where('date', '>=', date('Y-m-d'))
                ->get();


                if (!$reminder_data->isEmpty()) {
                    $reminder_data = $reminder_data->map(function ($value) {
                        $reminder_day = date('D', strtotime($value->date));
                        $reminder_month = date('M', strtotime($value->date));
                        $reminder_date = date('d', strtotime($value->date));

                        $your_date = strtotime($value->date);
                        $now = time();
                        $datediff = $your_date - $now;

                        //echo round($datediff / (60 * 60 * 24));
                        //echo date('d', strtotime($value->date))."=====".date('d', strtotime(('now')));

                        $remaining_days = date('d', strtotime($value->date)) - date('d', strtotime(('now')));
                        $data['id'] = $value->id;
                        $data['title'] = $value->title;
                        $data['image'] = Storage::url('/reminder/' . $value->image);
                        $data['date'] = $value->date;
                        $data['reminder_date'] = $value->reminder_date;
                        $data['remind_me'] = $value->remind_me;
                        $data['time'] = $value->time;

                        $valuehoo = $datediff / (60 * 60 * 24);

                        if ($valuehoo < 0) {
                            $valuehoo = $valuehoo * -1;
                        }
                        // $data['reminder_text'] = $reminder_day . ', ' . $reminder_month . ' ' . $reminder_date . ' in ' . $remaining_days . ' days';
                        $data['reminder_text'] = $reminder_day . ', ' . $reminder_month . ' ' . $reminder_date . ' in ' . round($valuehoo) . ' days';
                        return $data;
                    });
                    $reminder_data->all();
                }
                if (!empty($reminder_data[0])) {
                    $record_data[] = [
                        'month' => $month_name,
                        'year' => $year,
                        'recordCount' => Reminder::whereMonth('date', $month)->whereYear('date', $year)->where('user_id', Auth::user()->id)->count(),
                        'data' => $reminder_data,
                    ];
                }
            }
            $result['code'] = 200;
            $result['error'] = false;
            $result['result']  = $record_data;
            return response()->json($result);
        } catch (Exception $e) {
            return response()->json([
                'status_code' => 500,
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ]);
        }
    }
//     public function getreminders(Request $request)
// {
//     try {
//         $validation = Validator::make($request->all(), []);
//         if ($validation->fails()) {
//             $result['code']     = 400;
//             $result['error']    = true;
//             $result['message']  = $validation->messages()->first();
//             return response()->json($result);
//         }

//         $record_data = array();
//         for ($i = 1; $i <= 12; $i++) { // Iterate over all months of the year
//             $month = date('m', mktime(0, 0, 0, $i, 1));
//             $year = date('Y', mktime(0, 0, 0, $i, 1));
//             $month_name = date('F', mktime(0, 0, 0, $i, 1));
//             $reminder_data = Reminder::whereMonth('date', $month)->whereYear('date', $year)->where('user_id', Auth::user()->id)
//                 ->where('date', '>=', date('Y-m-d'))
//                 ->get();

//                 if (!$reminder_data->isEmpty()) {
//                     $reminder_data = $reminder_data->map(function ($value) {
//                         $reminder_day = date('D', strtotime($value->date));
//                         $reminder_month = date('M', strtotime($value->date));
//                         $reminder_date = date('d', strtotime($value->date));

//                         $your_date = strtotime($value->date);
//                         $now = time();
//                         $datediff = $your_date - $now;

//                         //echo round($datediff / (60 * 60 * 24));
//                         //echo date('d', strtotime($value->date))."=====".date('d', strtotime(('now')));

//                         $remaining_days = date('d', strtotime($value->date)) - date('d', strtotime(('now')));
//                         $data['id'] = $value->id;
//                         $data['title'] = $value->title;
//                         $data['image'] = Storage::url('/reminder/' . $value->image);
//                         $data['date'] = $value->date;
//                         $data['reminder_date'] = $value->reminder_date;
//                         $data['remind_me'] = $value->remind_me;
//                         $data['time'] = $value->time;

//                         $valuehoo = $datediff / (60 * 60 * 24);

//                         if ($valuehoo < 0) {
//                             $valuehoo = $valuehoo * -1;
//                         }
//                         // $data['reminder_text'] = $reminder_day . ', ' . $reminder_month . ' ' . $reminder_date . ' in ' . $remaining_days . ' days';
//                         $data['reminder_text'] = $reminder_day . ', ' . $reminder_month . ' ' . $reminder_date . ' in ' . round($valuehoo) . ' days';
//                         return $data;
//                     });
//                     $reminder_data->all();
//                 }

//             if (!empty($reminder_data[0])) {
//                 $record_data[] = [
//                     'month' => $month_name,
//                     'year' => $year,
//                     'recordCount' => Reminder::whereMonth('date', $month)->whereYear('date', $year)->where('user_id', Auth::user()->id)->count(),
//                     'data' => $reminder_data,
//                 ];
//             }
//         }

//         $result['code'] = 200;
//         $result['error'] = false;
//         $result['result']  = $record_data;
//         return response()->json($result);
//     } catch (Exception $e) {
//         return response()->json([
//             'status_code' => 500,
//             'success' => false,
//             'message' => $e->getMessage(),
//             'data' => null
//         ]);
//     }
// }
// public function getreminders(Request $request)
// {
//     try {
//         $validation = Validator::make($request->all(), []);
//         if ($validation->fails()) {
//             $result['code']     = 400;
//             $result['error']    = true;
//             $result['message']  = $validation->messages()->first();
//             return response()->json($result);
//         }

//         $record_data = array();

//         // Query all reminders without a specific month or year filter
//         $reminder_data = Reminder::where('user_id', Auth::user()->id)
//             ->where('date', '>=', date('Y-m-d'))
//             ->get();

//         if (!$reminder_data->isEmpty()) {
//             $record_data = [
//                 'recordCount' => $reminder_data->count(),
//                 'data' => $reminder_data->map(function ($value) {
//                     $reminder_day = date('D', strtotime($value->date));
//                     $reminder_month = date('M', strtotime($value->date));
//                     $reminder_date = date('d', strtotime($value->date));

//                     $your_date = strtotime($value->date);
//                     $now = time();
//                     $datediff = $your_date - $now;

//                     $remaining_days = date('d', strtotime($value->date)) - date('d', strtotime(('now')));
//                     $data['id'] = $value->id;
//                     $data['title'] = $value->title;
//                     $data['image'] = Storage::url('/reminder/' . $value->image);
//                     $data['date'] = $value->date;
//                     $data['reminder_date'] = $value->reminder_date;
//                     $data['remind_me'] = $value->remind_me;
//                     $data['time'] = $value->time;

//                     $valuehoo = $datediff / (60 * 60 * 24);

//                     if ($valuehoo < 0) {
//                         $valuehoo = $valuehoo * -1;
//                     }

//                     $data['reminder_text'] = $reminder_day . ', ' . $reminder_month . ' ' . $reminder_date . ' in ' . round($valuehoo) . ' days';
//                     return $data;
//                 }),
//             ];
//         }

//         $result['code'] = 200;
//         $result['error'] = false;
//         $result['result'] = $record_data;
//         return response()->json($result);
//     } catch (Exception $e) {
//         return response()->json([
//             'status_code' => 500,
//             'success' => false,
//             'message' => $e->getMessage(),
//             'data' => null
//         ]);
//     }
// }



    public function deletereminders($id)
    {
        try {
            $ifRecordExist = Reminder::where('id', $id)->count();
            if ($ifRecordExist < 1) {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }

            Reminder::where('id', $id)->delete();

            $result['code'] = 200;
            $result['error'] = false;
            $result['message'] = "Reminder deleted";
            return response()->json($result);
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }
    ///////// **********Reminders End**************///////////////////////////

    ///////////// ************* Payment ********************/////////////////////////////////////////////
    public function payment(Request $request)
    {
        //  die('paymenttt');
        try {
            $validation = Validator::make($request->all(), [
                'order_number' => 'required',
                'txn_id' => 'required',
                'status' => 'required',
                'amount' =>  'required',
                'remark' =>  'required',
                'ref_no' =>  'required',
                'status' =>  'required',
            ]);
            if ($validation->fails()) {
                $result['code']     = 400;
                $result['error']    = true;
                $result['message']  = $validation->messages()->first();
                return response()->json($result);
            }
            
            $payment = new Payment();
            $payment->user_id = Auth()->user()->id;
            $payment->txn_id  = $request->txn_id;
            $payment->status  = $request->status;
            $payment->amount  = $request->amount;
            $payment->remark  = $request->remark;
            $payment->err_desc  = $request->err_desc;
            $payment->ref_no  = $request->ref_no;
            $payment->auth_code  = $request->auth_code;
            $payment->status  = $request->status;
            $payment->payment_response_complete_json  = $request->payment_response_complete_json;
            $payment->save();

            //update cart and cart item information in order and order item.
            if ($request->status == 0) {
                $message = 'Payment Transaction Failed';
            } else {
                $orderIdFromCart = $this->clearCart($payment->id, $request->all());
                // dd($orderIdFromCart);

                $payment->order_id = $orderIdFromCart;
                $message = 'Order placed successfully';

            }
            // dd($message);

            //push notification start here.

            $push_message = [
                'title' => 'Order Status',
                'body' => $message,
                'type' => '0',
                'user_type' => '0'
            ];
            $user = auth()->user();
            // dd($user);
            if ($user->device_type == 1) {
                $sendPush = Helper::sendNotification($user->device_token, $push_message);
                // dd($sendPush);
            } elseif ($user->device_type == 2) {
                $sendPush = Helper::iosSendNotification($user->device_token, $push_message);
            }

            $notification = [
                'user_id' => $user->id,
                'message' => $message,
            ];

            Notification::create($notification);
            //push notification end here.

            if ($payment) {
                $result['code'] = 200;
                $result['error'] = false;
                $result['result']  = $payment;
                return response()->json($result);
            } else {
                $result['code'] = 401;
                $result['success'] = false;
                $result['message']  = "No Data";
                // $result['result']['trending']  = $treand;
                return response()->json($result);
            }
        } catch (Exception $e) {
            return response()->json([
                'status_code' => 500,
                'success' => false,
                'message' => $e->getMessage(),
                'data' => null
            ]);
        }
    }

    public function clearCart($id, $request)
    {
        // dd();
        //update cart and cart item information in order and order item.
        $emailTemplateForGiftVendor = EmailTemplate::where('id', 5)->first();
        $emailTemplateForPrinterVendor = EmailTemplate::where('id', 6)->first();
        $emailTemplateForUser = EmailTemplate::where('id', 10)->first();


        $user = Auth::user()->id;
        $cart_data2 = Cart::where('created_by', Auth::user()->id)->first();
        // dd($cart_data2);

        //insert in order table
        $order =  new Order();
        $order->order_number  = $request['order_number'];

        $order->send_to = $cart_data2->send_to;

        $order->msg = $cart_data2->msg;
        $order->same_day_delivery = $cart_data2->same_day_delivery;
        $order->status = $cart_data2->status;
        $order->add_premium_service = $cart_data2->add_premium_service;
        $order->paper_type_id = $cart_data2->paper_type_id;
        $order->additional_interests = $cart_data2->additional_interests;
        $order->total = $cart_data2->total;
        $order->additional_interests_msg = $cart_data2->additional_interests_msg;
        $order->created_by = $cart_data2->created_by;
        $order->updated_by = $cart_data2->updated_by;
        $order->created_at = $cart_data2->created_at;
        $order->updated_at = $cart_data2->updated_at;
        $order->deleted_at = $cart_data2->deleted_at;
        $order->save();
        $orderId = $order->id;
        $orderNUmber = $request['order_number'];
        $orderTotal = $order->total;
        //add data in order status table.
        $status = [
            'order_id' => $order->id,
            'order_status_name' => "order_placed",
            'status' => 1,
        ];

        OrderStatus::insert($status);

        $cartqty = CartItem::where('created_by', $user)->where('final_checkout', 1)->sum('quantity');

        $total_amount = CartItem::where('created_by', Auth::user()->id)->where('final_checkout', 1)->sum('price');

        $cart_data = CartItem::where('created_by', Auth::user()->id)->where('final_checkout', 1)->get();

        if ($cartqty) {
            $cart_totalqty = $cartqty;
        }

        $vendorIds = [];
        foreach ($cart_data as $key => $value) {
            /*
             $value->product_id = 1;
             $product = Product::where('id',$value->product_id)->first();

             $vendor = User::where('id',$product->created_by)->first();

             $to_email = $vendor->email;

             $to_name = $vendor->name;
             $emailData['name'] = $vendor->name;
             $productName = "test product";
             //Mail to vendor for welcome

             $input_arr = array(
                 'name' => $vendor->name,
                // 'email' => $vendor->email,
                 'email' => "anjay.webmobril@gmail.com",
                 'body' => $emailTemplateForGiftVendor->message,
                 'subject' => !empty($emailTemplateForGiftVendor->subject) ? $emailTemplateForGiftVendor->subject : null,
                 'order_number' => $orderNUmber,
                 'product_name' => $productName,
                 'quantity' => $value->quantity,
                 'total_amount' => $orderTotal,
             );

             Mail::send(['html' => 'emails.email_to_vendor_of_order_placed'], $input_arr, function ($message) use ($input_arr) {
                 $message->to($input_arr['email'], 'admin')
                     ->subject($input_arr['subject']);
                 $message->from(config('mail.username'), 'Beam App');
             });

             die('ting tong');
             */

            $order_item =  new OrderItem();

            $order_item->product_type = $value->product_type;
            $order_item->section = $value->section;
            $order_item->custom_order = $value->custom_order;
            $order_item->product_id = $value->product_id;
            $order_item->order_id = $order->id;
            $order_item->item_grand_total_price = $order->total;
            $order_item->price = $value->price;
            $order_item->designer_charge = $value->designer_charge;
            $order_item->print_charge = $value->print_charge;
            $order_item->quantity = $value->quantity;
            $order_item->print_type = $value->print_type;
            $order_item->to_text = $value->to_text;
            $order_item->write_your_text = $value->write_your_text;
            $order_item->image = $value->image;
            $order_item->image_2 = $value->image_2;
            $order_item->image_3 = $value->image_3;
            $order_item->image_4 = $value->image_4;
            $order_item->attachment_original_name = $value->attachment_original_name;
            $order_item->pdf_page_count = $value->pdf_page_count;
            $order_item->mail_type_id = $value->mail_type_id;
            $order_item->created_by = $value->created_by;
            $order_item->designer_id = $value->designer_id;

            $order_item->wishesh = $value->wishesh;
            $order_item->host = $value->host;
            $order_item->mr_groom_person = $value->mr_groom_person;
            $order_item->mrs_bride = $value->mrs_bride;
            $order_item->date_time_of_wishes = $value->date_time_of_wishes;
            $order_item->venue1 = $value->venue1;
            $order_item->venue2 = $value->venue2;

            if(!empty($request['promocode'])){
                $order_item->promocode = $request['promocode'];

                //increase usage promode count
                $promocode = Promocode::where([
                    'promocode' => $request['promocode'],
                ])->first();

                if(!empty($promocode->id)){
                    $usageCount = ($promocode->usage_count+1);
                    Promocode::where('id', $promocode->id)->update(['usage_count' => $usageCount]);
                }

                if(!empty($request['discounted_amount'])){
                    $order_item->discounted_amount = $request['discounted_amount'];
                }
            }

            

            //for postal product
            if ($value->section != 2 && $value->product_type != 3) {

                $vendorIdsWithComa = $this->getVendorIds($value);
                $order_item->multi_vendor_id = $vendorIdsWithComa;
            }

            // for postcard and email product
            if ($value->product_type == 1 or $value->product_type == 3) {
                $vendorIdsWithComa = $this->getVendorIds($value);
                $order_item->multi_vendor_id = $vendorIdsWithComa;
            }


            if ($value->custom_order != 1) {
                if ($value->product_type == 1) {
                    $check = PostCard::select('created_by', 'price')->where('id', $value->product_id)->first();
                    $order_item->vendor_id = $check->created_by;
                    $order_item->price = $check->price;
                    $order_item->item_total_price = $check->price * $value->quantity;
                } elseif ($value->product_type == 2) {
                    $check = Product::where('id', $value->product_id)->first();
                    $order_item->vendor_id = $check->created_by;
                    $order_item->price = $check->price;
                    $order_item->item_total_price = $check->price * $value->quantity;
                } else {
                    $order_item->item_total_price = $value->price * $value->quantity;
                }
            } else {
                $order_item->item_total_price = $value->price * $value->quantity;
                if(!empty($value->designer_charge) && $value->designer_charge > 0) {
                    $order_item->item_total_price = round(($order_item->item_total_price + $value->designer_charge), 2);
                }
            }

            $order_item->save();

            //Send email to vendor that he received order.
            if($value->section == 2) {

                $product = Product::where('id', $value->product_id)->first();
                $productName = (!empty($product->name)) ? $product->name : null;
                //get vendor user info
                $vendor = User::where('id', $product->created_by)->first();

                $input_arr = array(
                    'name' => $vendor->name,
                    'email' => $vendor->email,
                   // 'email' => "anjay.webmobril@gmail.com",
                    'body' => !empty($emailTemplateForGiftVendor->message) ? $emailTemplateForGiftVendor->message : null,
                    'subject' => !empty($emailTemplateForGiftVendor->subject) ? $emailTemplateForGiftVendor->subject : null,
                    'order_number' => !empty($orderNUmber) ? $orderNUmber : null,
                    'product_name' => !empty($productName) ? $productName : null,
                    'quantity' => !empty($value->quantity) ? $value->quantity : null,
                    'total_amount' => !empty($orderTotal) ? $orderTotal : null,
                );

                Mail::send(['html' => 'emails.email_to_vendor_of_order_placed'], $input_arr, function ($message) use ($input_arr) {
                    $message->to($input_arr['email'], 'admin')
                        ->subject($input_arr['subject']);
                    $message->from(config('mail.username'), 'Beam App');
                });

            } else {
                //send email to printer vendor
                $sendUserIds = explode(',', $order_item->multi_vendor_id);

                $custCounter = 1;
                foreach($sendUserIds as $receiverId) {

                    //get vendor details
                    $vendor = User::where('id', $receiverId)->withTrashed()->first();
                    if ($value->custom_order == 1) {
                        $productName = null;
                    }else{
                      //get product name
                    if ($value->product_type == 1) {
                        $product = PostCard::where('id', $value->product_id)->first();


                         $productName = !empty($product->name) ? $product->name : null;
                    }

                    if ($value->product_type == 2) {
                        $product = Product::where('id', $value->product_id)->first();
                        $productName = $product->name ? $product->name : null;
                    }

                    if ($value->product_type == 3) {
                        $productName = $value->attachment_original_name;
                    }
                    }


                    $emailData = [
                        'product_name' => $productName,
                        'order_number' => $orderNUmber,
                        'from_user' => Auth::user()->name
                    ];

                    if($custCounter == 1) {
                        //send email to admin.
                        $admin = User::where('user_type', 1)->first();
                        $to_email = $admin->email;
                        $to_name = $admin->name;
                        $emailData['name'] = $admin->name;
                        Mail::send('emails.email_to_admin_of_order_placed', $emailData, function ($message) use ($to_name, $to_email) {
                            $message->from(env('MAIL_USERNAME'), env('MAIL_FROM_NAME'));
                            $message->sender(env('MAIL_USERNAME'), $to_name);
                            $message->to($to_email, $to_name);
                            $message->subject('Print order ');
                        });
                    }

                    //Send email to vendor
                    if(!empty($vendor->id)) {
                        //die('inside vendor');
                        $input_arr = array(
                            'name' => $vendor->name,
                            'email' => $vendor->email,
                           // 'email' => "anjay.webmobril@gmail.com",
                            'body' => !empty($emailTemplateForPrinterVendor->message) ? $emailTemplateForPrinterVendor->message : null,
                            'subject' => !empty($emailTemplateForPrinterVendor->subject) ? $emailTemplateForPrinterVendor->subject : null,
                            'order_number' => !empty($orderNUmber) ? $orderNUmber : null,
                            'product_name' => !empty($productName) ? $productName : null,
                            'quantity' => !empty($value->quantity) ? $value->quantity : null,
                            'total_amount' => !empty($orderTotal) ? $orderTotal : null,
                        );

                         Mail::send(['html' => 'emails.email_to_vendor_of_order_placed'], $input_arr, function ($message) use ($input_arr) {
                            $message->to($input_arr['email'], 'admin')
                                ->subject($input_arr['subject']);
                            $message->from(config('mail.username'), 'Beam App');
                        });

                        ///for user
                        $input_arr1 = array(
                            'name' => auth()->user()->name,
                            'merchant_name' =>$vendor->name,
                            'email' => auth()->user()->email,
                            'body' => !empty($emailTemplateForUser->message) ? $emailTemplateForUser->message : null,
                            'subject' => !empty($emailTemplateForUser->subject) ? $emailTemplateForUser->subject : null,
                            'order_number' => !empty($orderNUmber) ? $orderNUmber : null,
                            'product_name' => !empty($productName) ? $productName : null,
                            'quantity' => !empty($value->quantity) ? $value->quantity : null,
                            'total_amount' => !empty($orderTotal) ? $orderTotal : null,
                        );

                        //  Mail::send(['html' => 'emails.email_to_user_of_order_placed'], $input_arr1, function ($message) use ($input_arr1) {
                        //     $message->to($input_arr1['email'], 'admin')
                        //         ->subject($input_arr1['subject']);
                        //     $message->from(config('mail.username'), 'Beam App');
                        // });

                    }
                    $custCounter++;
                }

            }

        }
        //die('stop here');
        //send email to printer vendor
        /* $sendToUsers = Order::where('id', $orderId)->first();
        $sendUserIds = explode(',', $sendToUsers->send_to);
        $recipientIds = [];
        foreach($sendUserIds as $receiverId){
            //get receiver zip code
            $recipient = Contacts::where('id', $receiverId)->first();
            $vendor = User::where('zip_code', $recipient->zip_code)->first();
            if(!empty($vendor->id)){

                if(in_array($vendor->id, $vendorIds)){

                } else {
                    $vendorIds[] = $vendor->id;
                    $emailData = [
                        'product_name' => $orderId,
                        'name' => $vendor->name,
                        'from_user' => Auth::user()->name
                    ];

                    $to_email = $vendor->email;
                    $to_name = $vendor->name;
                    Mail::send('emails.email_to_vendor_of_order_placed', $emailData, function ($message) use ($to_name, $to_email) {
                        $message->from(env('MAIL_USERNAME'), env('MAIL_FROM_NAME'));
                        $message->sender(env('MAIL_USERNAME'), $to_name);
                        $message->to($to_email, $to_name);
                        $message->subject('Print order ');
                    });

                }

            }
        } */


        //delete of cart item.
        $cart_item_deleted =  DB::table('cart_item')->where('created_by', Auth::user()->id)->where('final_checkout', 1)->delete();
        if ($cart_item_deleted) {
            //delete of cart.
            $cart_deleted =  DB::table('cart')->where('created_by', Auth::user()->id)->delete();
            //get item id.
            $orderItem = OrderItem::where('order_id', $orderId)->first();

            /* $order_item->order_id = $order->id;
            $result['code'] = 200;
            $result['error'] = false;
            $result['result']  = $order->id;
            return response()->json($result); */
            return $orderItem->id;
        } else {
            $result['code'] = 401;
            $result['success'] = false;
            $result['message']  = "No Data";
            // $result['result']['trending']  = $treand;
            return response()->json($result);
        }
    }

    public function getVendorIds($value)
    {

        $cart_data2 = Cart::where('created_by', Auth::user()->id)->first();
        // dd($cart_data2);

        $sendUserIds = explode(',', $cart_data2->send_to);

        $vendorIdsWithComa = null;
        foreach($sendUserIds as $receiverId) {

            $return = 0;
            //get receiver zip code
            //dd($receiverId);
            $recipient = Contacts::where('id', $receiverId)->first();
            // dd($recipient);

            //get perticuler vendor.
            $orderItemType = $value->product_type;
            //get printer vendor.
            // dd($recipient->zip_code);
            if($orderItemType == 1) {
                $vendor = User::where('user_type', 5)->where('zip_code', $recipient->zip_code)->where('status', 1)->first();
            }

            if($orderItemType == 3) {
                $vendor = User::where('user_type', 6)->where('zip_code', $recipient->zip_code)->where('status', 1)->first();
            }

            //dd($vendor);

            if(!empty($vendor->id)) {
                $return = $vendor->id;
            } else {

                //get state name.
                // dd($recipient->zip_code);
                $vendorState = ImportPostal::where('postal_code', $recipient->zip_code)->first();

                if(!empty($vendorState->state_name)) {

                    $states = ImportPostal::where('state_name', $vendorState->state_name)->pluck('postal_code');


                    if($orderItemType == 1) {
                        $allVendorUsers = User::where('user_type', 5)->whereIn('zip_code', $states)->where('status', 1)->pluck('id');
                    }

                    if($orderItemType == 2 && $value->section == 1) {
                        $allVendorUsers = User::where('user_type', 5)->whereIn('zip_code', $states)->where('status', 1)->pluck('id');
                    }

                    if($orderItemType == 3) {
                        $allVendorUsers = User::where('user_type', 6)->whereIn('zip_code', $states)->where('status', 1)->pluck('id');
                    }

                    $allVendorCount = [];
                    foreach($allVendorUsers as $vendId) {

                        $orderItemCount = OrderItem::whereRaw('FIND_IN_SET(?, multi_vendor_id)', [$vendId])->groupBy('id')->get()->count();

                        if($orderItemCount > 0) {
                            $allVendorCount[$vendId] = $orderItemCount;
                        } else {
                            $allVendorCount[$vendId] = 0;
                        }
                    }
                    asort($allVendorCount);

                    if(count($allVendorCount) > 0) {
                        foreach($allVendorCount as $key => $val) {
                            $return = $key;
                        }
                    } else {
                        //if no vendor is found then assign Perak vendor in default.

                        // dd($vendorState->zone);
                        // $states = ImportPostal::where('zone', $vendorState->zone)->whereNotNull('zone')->pluck('postal_code');
                        $states = ImportPostal::where('state_name', 'Perak')->pluck('postal_code');

                        $allVendorUsers = User::whereIn('zip_code', $states)->pluck('id');

                        // if($orderItemType == 1){
                        //     $allVendorUsers = User::where('user_type', 5)->whereIn('zip_code', $states)->pluck('id');
                        // }

                        // if($orderItemType == 2 && $value->section == 1){
                        //     $allVendorUsers = User::where('user_type', 5)->whereIn('zip_code', $states)->pluck('id');
                        // }

                        // if($orderItemType == 3){
                        //     $allVendorUsers = User::where('user_type', 6)->whereIn('zip_code', $states)->pluck('id');
                        // }


                        $allVendorCount = [];
                        foreach($allVendorUsers as $vendId) {
                            // dd($vendId);
                            $orderItemCount = OrderItem::whereRaw('FIND_IN_SET(?, multi_vendor_id)', [$vendId])->groupBy('id')->get()->count();
                            //  dd($orderItemCount);
                            if($orderItemCount > 0) {
                                $allVendorCount[$vendId] = $orderItemCount;
                            } else {
                                $allVendorCount[$vendId] = 0;
                            }
                        }
                        asort($allVendorCount);
                        // dd($allVendorCount);
                        if(count($allVendorCount) > 0) {

                            foreach($allVendorCount as $key => $val) {
                                //dd($key);
                                // dd($val);
                                $return = $key;
                            }
                        } else {
                            //  die('else');
                            $return = 0;
                        }
                    }
                } else {
                    //$return = 0;
                    $states = ImportPostal::where('state_name', 'Perak')->pluck('postal_code');

                    $allVendorUsers = User::whereIn('zip_code', $states)->pluck('id');

                    $allVendorCount = [];
                    foreach($allVendorUsers as $vendId) {
                        // dd($vendId);
                        $orderItemCount = OrderItem::whereRaw('FIND_IN_SET(?, multi_vendor_id)', [$vendId])->groupBy('id')->get()->count();
                        //  dd($orderItemCount);
                        if($orderItemCount > 0) {
                            $allVendorCount[$vendId] = $orderItemCount;
                        } else {
                            $allVendorCount[$vendId] = 0;
                        }
                    }
                    asort($allVendorCount);
                    // dd($allVendorCount);
                    if(count($allVendorCount) > 0) {

                        foreach($allVendorCount as $key => $val) {
                            //dd($key);
                            // dd($val);
                            $return = $key;
                        }
                    } else {
                        //  die('else');
                        $return = 0;
                    }
                }

            }

            if($return > 0) {
                if(!empty($vendorIdsWithComa)) {
                    $vendorIdsWithComa = $vendorIdsWithComa.",".$return;
                } else {
                    $vendorIdsWithComa = $return;
                }
            }



        }
        return $vendorIdsWithComa;
    }

    private function generate_OTP()
    {
        return $randomOtp = 'Beam_App' . mt_rand(100000, 999999);
    }

    public function getContentManagement(Request $request)
    {
        try {
            $contentManagement = ContentManagement::get();

            if (!empty($contentManagement)) {
                $result['code'] = 200;
                $result['error'] = false;
                $result['result'] = $contentManagement;
                return response()->json($result);
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function getNotificationCountAndCartCount(Request $request)
    {
        try {
            $user = auth()->user();
            if($user->device_type == 2){
                 if($user->version <= 1.5 || $user->version == null){
                    $user = Auth::user()->token();
                    User::where('id', Auth::user()->id)->update(['otp' => null, 'device_token' => null]);
                    Auth::user()->tokens->each(function($token, $key) {
                        $token->delete();
                    });
                    $user->revoke();
                    return response()->json(['error' => true, 'code'=>401,'message' => 'Unauthenticated.'], 401);
                    
                }
            }else{
                if($user->version <= 3.6 || $user->version == null){
                    $user = Auth::user()->token();
                    User::where('id', Auth::user()->id)->update(['otp' => null, 'device_token' => null]);

                    Auth::user()->tokens->each(function($token, $key) {
                        $token->delete();
                    });

                    $user->revoke();
                    return response()->json(['error' => true, 'code'=>401,'message' => 'Unauthenticated.'], 401);
                
                }
            } 
            
            if (isset($user) && $user != null) {
                $notifications = Notification::select('id', 'user_id', 'message')
                    ->where('user_id', $user->id)
                    ->where('is_notification_seen', 0)
                    ->count();

                $cartItems = CartItem::select('id')->where('created_by', $user->id)->where('status', 1)->count();

                $return = [
                    'notification_count' => $notifications,
                    'cart_items_count' => $cartItems,
                ];

                $result['code'] = 200;
                $result['error'] = false;
                $result['result'] = $return;
                return response()->json($result);
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function getPrintTypes(Request $request)
    {
        //not using this method.
        try {
            $contentManagement = PrintTypes::get();

            if (!empty($contentManagement)) {
                $result['code'] = 200;
                $result['error'] = false;
                $result['result'] = $contentManagement;
                return response()->json($result);
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function sendEmailRnd()
    {

        $emailData = [
            'product_name' => "my Pro",
            'name' => "ak",
            'from_user' => "from user"
        ];
        $to_email = "anjay.webmobril@gmail.com";
        // $to_email = "support@letsbeam.it";

        $to_name = "anj";
        Mail::send('emails.testemail', $emailData, function ($message) use ($to_name, $to_email) {
            $message->from(env('MAIL_USERNAME'), env('MAIL_FROM_NAME'));
            $message->sender(env('MAIL_USERNAME'), $to_name);
            $message->to($to_email, $to_name);
            $message->subject('New Order ');
        });
        dd('ak');
        dd('ak');
    }

    ////////////*************************End****************////////////////////////////////////////////
};
