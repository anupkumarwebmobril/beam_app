<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\User;
use Auth;
use App\Helpers\Helper;

use Twilio\Rest\Client;
use Twilio\Jwt\ClientToken;
use Twilio\Exceptions\TwilioException;
use Twilio\Exceptions\RestException;
use App\Models\Reminder;
use App\Models\Notification;
use Token;
use App\Models\Setting;
use Mail;





class UsersController extends Controller
{
    public function loginBackup(Request $request)
    {
        try {
            $validation = Validator::make($request->all(), [
                'phone' => 'required|numeric',
            ]);

            $sixDigitRandomOtp = mt_rand(1000, 9999);

            if ($validation->fails()) {
                $result['code']     = config('messages.http_codes.validation');
                $result['error']    = true;
                $result['message']  = $validation->messages()->first();
                return response()->json($result);
            }

            if (User::where(['phone' => request('phone')])->first()) {
                User::where(['phone' => request('phone')])->update(['otp' => $sixDigitRandomOtp]);

                //send otp by sms on mobile
                //$msg = 'Your OTP is : ' . $sixDigitRandomOtp . ' Please do not share with anybody.';
                //$this->sendMessage(request('phone'),$msg);

                return response()->json([
                    'code' => config('httpcodes.HTTP_OK'),
                    'message' => 'Valid phone',
                    'otp' => $sixDigitRandomOtp,
                ]);
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = true;
                $result['message'] = 'Phone number does not exist. Please signup to proceed.';
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function login(Request $request)
    {
        try {
            $validation = Validator::make($request->all(), [
                // 'phone' => 'required|numeric',
                'login_type'    => 'required|numeric|min:1|max:4',         // 1:Mobile, 2:FB, 3:Google, 4:Instagram
                'phone'         => 'required_if:login_type,1',
                'social_id'         => 'required_if:login_type,2',
                'social_id'     => 'required_if:login_type,3',
                'social_id'  => 'required_if:login_type,4',
                'email'         => 'sometimes',
                'device_type' => 'required',
              //  'device_token' => 'required',
                'device_id' => 'required',
               // 'latitude' => 'required|min:4|',
            //    'longitude' => 'required|min:4|',
            ]);


            if (str_contains($request->phone, '9632580')) {
                //this for android testing for android play store for a perticuler number.
                $request->phone = "919632580";
                $sixDigitRandomOtp = 1234;
            } else {
                $sixDigitRandomOtp = mt_rand(1000, 9999); //when we go live please uncomment this code line
                // $sixDigitRandomOtp = 1234;
            }

            if ($validation->fails()) {
                $result['code']     = config('messages.http_codes.validation');
                $result['error']    = true;
                $result['message']  = $validation->messages()->first();
                return response()->json($result);
            }
            if($request->version == 0.0){
                $result['code']     = 400;
                $result['error']    = true;
                $result['message']  = 'Please update the latest build from the store.';
                return response()->json($result);
            }
            if($request->device_type == 2){
                if($request->version <= 1.5){
                    $result['code']     = 400;
                    $result['error']    = true;
                    $result['message']  = 'Please update the latest build from the store';
                    return response()->json($result);
                }
                
            }
            else{
                if($request->version <= 3.6){
                    $result['code']     = 400;
                    $result['error']    = true;
                    $result['message']  = 'Please update the latest build from the store.';
                    return response()->json($result);
                }
              
            }
            
            
            $payment_response = [
                'Merchant Registered Name'=> 'Lleapp Sdn Bhd',
                'Merchant Name: Beam'     => ' Brings You Closer',
                'MerchantID'              => 'LDL',
                'Merchant Password'       => 'CjFU8VXieni37ndKs9zwtPTjwaz9kTKG',
                'Payment URL'             => 'https://securepay.e-ghl.com/IPG/Payment.aspx'
            ];

            //get merchant key.
            $setting = Setting::where('id', 3)->first();

            if (isset($request->phone) && $request->login_type == 1) {

                $checkMobileUser = User::where(['phone' => $request->phone])->first();

                if (isset($checkMobileUser)) {
                    $oldDeviceId = $checkMobileUser['device_id'];

                    $checkMobileUser->update([
                        'login_type'    => $request->login_type,
                        'device_type'   => $request->device_type,
                        'device_token'  => $request->device_token,
                        'latitude'      => !empty($request->latitude) ? $request->latitude : null,
                        'longitude'     => !empty($request->longitude) ? $request->longitude : null,
                        'version'       => $request->version
                    ]);

                    $returnArry = [
                        'code' => config('httpcodes.HTTP_OK'),
                        'message' => 'Valid phone',
                    ];

                    $otpExpirtyTime = now()->addMinutes(5);

                    if ($checkMobileUser->status == 0) {
                        $result['code'] = config('messages.http_codes.validation');
                        $result['error'] = true;
                        $result['message'] = "Your access rights has been revoked by Admin";
                        return response()->json($result);
                    } else if (($checkMobileUser->is_otp_verified == 0)) {
                        User::where(['phone' => request('phone')])->update(['otp' => $sixDigitRandomOtp, 'otp_expire_on' => $otpExpirtyTime, 'device_token' => $request->device_token, 'latitude' => $request->latitude, 'longitude' => $request->longitude]);
                        $returnArry['otp'] = $sixDigitRandomOtp;
                        $returnArry['is_otp_verified'] = 0;
                        $returnArry['user_type'] = $checkMobileUser->user_type;
                        $returnArry['device_type'] = $checkMobileUser->device_type;

                        //send otp using klasiksms getway.


                        //when we go live uncomment blow line
                        $otpStatus = Helper::sendOtpOnMobile(request('phone'), $sixDigitRandomOtp);
                        //end live code


                        //below code is need to use when sms sending will work for indian client also.
                        /* $otpStatus = Helper::sendOtpOnMobile(request('phone'), $sixDigitRandomOtp);
                        if($otpStatus['statusCode'] == "X7021"){
                            return response()->json([
                                'code' => config('httpcodes.HTTP_OK'),
                                'message' => 'New otp',
                                'otp' => $sixDigitRandomOtp,
                            ]);
                        } else {
                            $result['code'] = config('messages.http_codes.validation');
                            $result['error'] = true;
                            $result['message'] = $otpStatus['statusMsg'];
                            return response()->json($result);
                        } */
                        //end of send otp.
                    } else {

                        if ((!empty($request['device_id'])) && ($oldDeviceId != $request['device_id'])) {
                            User::where(['phone' => request('phone')])->update(['is_otp_verified' => 0, 'otp' => $sixDigitRandomOtp, 'otp_expire_on' => $otpExpirtyTime, 'device_id' => $request->device_id, 'latitude' => $request->latitude, 'longitude' => $request->longitude]);
                            $returnArry['otp'] = $sixDigitRandomOtp;
                            $returnArry['is_otp_verified'] = 0;
                            $returnArry['user_type'] = $checkMobileUser->user_type;
                            $returnArry['device_type'] = $checkMobileUser->device_type;

                            //send otp using klasiksms getway.
                            $otpStatus = Helper::sendOtpOnMobile(request('phone'), $sixDigitRandomOtp);

                            //below code is need to use when sms sending will work for indian client also.
                            /* $otpStatus = Helper::sendOtpOnMobile(request('phone'), $sixDigitRandomOtp);
                            if($otpStatus['statusCode'] == "X7021"){
                                return response()->json([
                                    'code' => config('httpcodes.HTTP_OK'),
                                    'message' => 'New otp',
                                    'otp' => $sixDigitRandomOtp,
                                ]);
                            } else {
                                $result['code'] = config('messages.http_codes.validation');
                                $result['error'] = true;
                                $result['message'] = $otpStatus['statusMsg'];
                                return response()->json($result);
                            } */
                            //end of send otp.
                        } else {
                            //remove all tokens
                            $checkMobileUser = User::where(['phone' => $request->phone])->first();

                            $checkMobileUser->tokens()
                                ->delete();

                            $returnArry['user_type'] = $checkMobileUser->user_type;
                            $returnArry['device_type'] = $checkMobileUser->device_type;
                            $returnArry['is_otp_verified'] = 1;
                            $token = $checkMobileUser->createToken('BeamAppVerifyOtp')->accessToken;
                            if ($checkMobileUser->save()) {
                                $checkMobileUser->update([
                                    'login_type'    => $request->login_type,
                                    'device_type'   => $request->device_type,
                                    'device_token'  => $request->device_token,
                                    'latitude'      => !empty($request->latitude) ? $request->latitude : null,
                                    'longitude'     => !empty($request->longitude) ? $request->longitude : null,
                                    'version'       => $request->version
                                ]);



                                $returnArry['userInfo'] = $checkMobileUser;
                                $returnArry['userInfo']['token'] = $token;
                                $returnArry['otp'] = null;
                                $returnArry['device_type'] = $checkMobileUser->device_type;
                                $returnArry['payment_response'] = $payment_response;
                            } else {
                                $result['code'] = config('messages.http_codes.server');
                                $result['error'] = true;
                                $result['message'] = config('messages.error_messages.server_error');
                                return response()->json($result);
                            }
                        }
                    }

                    if(count($returnArry) > 0){
                        $returnArry['server_key'] = "AAAA9xJ_qKA:APA91bHyuLeIew8_jejBWxC7AfQSeD5gVrJvhBsINZXiAOFA0-Ysuvyt9DjatpK-bbrQWCkTnyTWrEkLkSB32IlO3AgFksHYwYc5ciPf4F0CHZ0TqIJrvbvOLpAw9xW7nXmGDhLZrO9B";

                    }

                    if(!empty($setting->payment_merchant_code)){
                        $returnArry['payment_merchant_code'] = $setting->payment_merchant_code;
                    }

                    return response()->json(
                        $returnArry
                    );

                    //send otp by sms on mobile
                    //$msg = 'Your OTP is : ' . $sixDigitRandomOtp . ' Please do not share with anybody.';
                    //$this->sendMessage(request('phone'),$msg);

                } else {
                    $result['code'] = config('messages.http_codes.validation');
                    $result['error'] = true;
                    $result['message'] = 'Phone number does not exist. Please signup to proceed.';
                    return response()->json($result);
                }
            } else if (isset($request->social_id) && $request->login_type == 2) {

                $checkGoogleUser = User::where(['social_id' => $request->social_id, 'login_type' => 2])->first();

                if (isset($checkGoogleUser)) {
                    $checkGoogleUser->tokens()->delete();

                    $checkGoogleUser->update([
                        'login_type'    => $request->login_type,
                        'device_type'   => $request->device_type,
                        'device_token'  => $request->device_token,
                        'latitude'      => !empty($request->latitude) ? $request->latitude : null,
                        'longitude'     => !empty($request->longitude) ? $request->longitude : null,
                        'version'       => $request->version

                    ]);

                    $token = $checkGoogleUser->createToken('AthleticateFacebookLoginToken')->accessToken;

                    if(!empty($setting->payment_merchant_code)){
                        $result['payment_merchant_code'] = $setting->payment_merchant_code;
                    }

                    $result['server_key'] = "AAAA9xJ_qKA:APA91bHyuLeIew8_jejBWxC7AfQSeD5gVrJvhBsINZXiAOFA0-Ysuvyt9DjatpK-bbrQWCkTnyTWrEkLkSB32IlO3AgFksHYwYc5ciPf4F0CHZ0TqIJrvbvOLpAw9xW7nXmGDhLZrO9B";
                    $result['status_code'] = config('httpcodes.HTTP_OK');
                    $result['error'] = false;
                    $result['message'] =  "USER LOGIN SUCCESSFUL.";
                    $result['loginResult'] = $checkGoogleUser;
                    $result['loginResult']['token'] = $token;
                    $result['payment_response'] = $payment_response;

                    return response()->json($result);
                } else {

                    $checkIfGoogleUserMobileExist = User::where('email', $request->email)->where('email', '!=', null)->first();

                    if (isset($checkIfGoogleUserMobileExist)) {
                        $checkIfGoogleUserMobileExist->update([
                            'social_id'         => $request->social_id,
                            'email'             => $request->email,
                            'name'              => $request->name,
                            'login_type'        => $request->login_type,
                            'device_type'       => $request->device_type,
                            'device_token'      => $request->device_token,
                            'latitude'      => !empty($request->latitude) ? $request->latitude : null,
                            'longitude'     => !empty($request->longitude) ? $request->longitude : null,
                            'version'       => $request->version
                        ]);

                        $token = $checkIfGoogleUserMobileExist->createToken('AthleticateFacebookLoginToken')->accessToken;


                        if(!empty($setting->payment_merchant_code)){
                            $result['payment_merchant_code'] = $setting->payment_merchant_code;
                        }
                        $result['server_key'] = "AAAA9xJ_qKA:APA91bHyuLeIew8_jejBWxC7AfQSeD5gVrJvhBsINZXiAOFA0-Ysuvyt9DjatpK-bbrQWCkTnyTWrEkLkSB32IlO3AgFksHYwYc5ciPf4F0CHZ0TqIJrvbvOLpAw9xW7nXmGDhLZrO9B";
                        $result['status_code'] = 200;
                        $result['error'] = false;
                        $result['message'] =  "USER LOGIN SUCCESSFUL.";
                        $result['loginResult'] = $checkIfGoogleUserMobileExist;
                        $result['loginResult']['token'] = $token;
                        $result['payment_response'] = $payment_response;

                        return response()->json($result);
                    }

                    $createGoogleUser = User::create([
                        'social_id'         => $request->social_id,
                        'email'             => $request->email,
                        'name'              => $request->name,
                        'login_type'        => $request->login_type,
                        'device_type'       => $request->device_type,
                        'device_token'      => $request->device_token,
                        'latitude'      => !empty($request->latitude) ? $request->latitude : null,
                        'longitude'     => !empty($request->longitude) ? $request->longitude : null,
                        'version'       => $request->version
                    ]);

                    $token = $createGoogleUser->createToken('AthleticateFacebookLoginToken')->accessToken;

                    if(!empty($setting->payment_merchant_code)){
                        $result['payment_merchant_code'] = $setting->payment_merchant_code;
                    }

                    $result['server_key'] = "AAAA9xJ_qKA:APA91bHyuLeIew8_jejBWxC7AfQSeD5gVrJvhBsINZXiAOFA0-Ysuvyt9DjatpK-bbrQWCkTnyTWrEkLkSB32IlO3AgFksHYwYc5ciPf4F0CHZ0TqIJrvbvOLpAw9xW7nXmGDhLZrO9B";
                    $result['status_code'] = 200;
                    $result['error'] = false;
                    $result['message'] =  "USER LOGIN SUCCESSFUL.";
                    $result['loginResult'] = $createGoogleUser;
                    $result['loginResult']['token'] = $token;
                    $result['payment_response'] = $payment_response;
                    return response()->json($result);
                }
            } else if (isset($request->social_id) && $request->login_type == 3) {

                $checkGoogleUser = User::where(['social_id' => $request->social_id, 'login_type' => 3])->first();

                if (isset($checkGoogleUser)) {
                    $checkGoogleUser->tokens()->delete();

                    $checkGoogleUser->update([
                        'login_type'    => $request->login_type,
                        'device_type'   => $request->device_type,
                        'device_token'  => $request->device_token,
                        'latitude'      => !empty($request->latitude) ? $request->latitude : null,
                        'longitude'     => !empty($request->longitude) ? $request->longitude : null,
                        'version'       => $request->version

                    ]);

                    $token = $checkGoogleUser->createToken('AthleticsGoogleLoginToken')->accessToken;

                    if(!empty($setting->payment_merchant_code)){
                        $result['payment_merchant_code'] = $setting->payment_merchant_code;
                    }
                    $result['server_key'] = "AAAA9xJ_qKA:APA91bHyuLeIew8_jejBWxC7AfQSeD5gVrJvhBsINZXiAOFA0-Ysuvyt9DjatpK-bbrQWCkTnyTWrEkLkSB32IlO3AgFksHYwYc5ciPf4F0CHZ0TqIJrvbvOLpAw9xW7nXmGDhLZrO9B";
                    $result['status_code'] = config('httpcodes.HTTP_OK');
                    $result['error'] = false;
                    $result['message'] =  "USER LOGIN SUCCESSFUL.";
                    $result['loginResult'] = $checkGoogleUser;
                    $result['loginResult']['token'] = $token;
                    $result['payment_response'] = $payment_response;
                    return response()->json($result);
                } else {

                    $checkIfGoogleUserMobileExist = User::where('email', $request->email)->where('email', '!=', null)->first();

                    if (isset($checkIfGoogleUserMobileExist)) {
                        $checkIfGoogleUserMobileExist->update([
                            'social_id'         => $request->social_id,
                            'email'             => $request->email,
                            'name'              => $request->name,
                            'login_type'        => $request->login_type,
                            'device_type'       => $request->device_type,
                            'device_token'      => $request->device_token,
                            'latitude'      => !empty($request->latitude) ? $request->latitude : null,
                            'longitude'     => !empty($request->longitude) ? $request->longitude : null,
                            'version'       => $request->version
                        ]);

                        $token = $checkIfGoogleUserMobileExist->createToken('AthleticsGoogleLoginToken')->accessToken;

                        if(!empty($setting->payment_merchant_code)){
                            $result['payment_merchant_code'] = $setting->payment_merchant_code;
                        }
                        $result['server_key'] = "AAAA9xJ_qKA:APA91bHyuLeIew8_jejBWxC7AfQSeD5gVrJvhBsINZXiAOFA0-Ysuvyt9DjatpK-bbrQWCkTnyTWrEkLkSB32IlO3AgFksHYwYc5ciPf4F0CHZ0TqIJrvbvOLpAw9xW7nXmGDhLZrO9B";
                        $result['status_code'] = 200;
                        $result['error'] = false;
                        $result['message'] =  "USER LOGIN SUCCESSFUL.";
                        $result['loginResult'] = $checkIfGoogleUserMobileExist;
                        $result['loginResult']['token'] = $token;
                        $result['payment_response'] = $payment_response;

                        return response()->json($result);
                    }

                    $createGoogleUser = User::create([
                        'social_id'         => $request->social_id,
                        'email'             => $request->email,
                        'name'              => $request->name,
                        'login_type'        => $request->login_type,
                        'device_type'       => $request->device_type,
                        'device_token'      => $request->device_token,
                        'latitude'      => !empty($request->latitude) ? $request->latitude : null,
                        'longitude'     => !empty($request->longitude) ? $request->longitude : null,
                        'version'       => $request->version
                    ]);

                    $token = $createGoogleUser->createToken('AthleticsGoogleLoginToken')->accessToken;

                    if(!empty($setting->payment_merchant_code)){
                        $result['payment_merchant_code'] = $setting->payment_merchant_code;
                    }
                    $result['server_key'] = "AAAA9xJ_qKA:APA91bHyuLeIew8_jejBWxC7AfQSeD5gVrJvhBsINZXiAOFA0-Ysuvyt9DjatpK-bbrQWCkTnyTWrEkLkSB32IlO3AgFksHYwYc5ciPf4F0CHZ0TqIJrvbvOLpAw9xW7nXmGDhLZrO9B";
                    $result['status_code'] = 200;
                    $result['error'] = false;
                    $result['message'] =  "USER LOGIN SUCCESSFUL.";
                    $result['loginResult'] = $createGoogleUser;
                    $result['loginResult']['token'] = $token;
                    $result['payment_response'] = $payment_response;

                    return response()->json($result);
                }
            } else if (isset($request->social_id) && $request->login_type == 4) {

                $checkGoogleUser = User::where(['social_id' => $request->social_id, 'login_type' => 4])->first();

                if (isset($checkGoogleUser)) {
                    $checkGoogleUser->tokens()->delete();

                    $checkGoogleUser->update([
                        'login_type'    => $request->login_type,
                        'device_type'   => $request->device_type,
                        'device_token'  => $request->device_token,
                        'latitude'      => !empty($request->latitude) ? $request->latitude : null,
                        'longitude'     => !empty($request->longitude) ? $request->longitude : null,
                        'version'       => $request->version

                    ]);

                    $token = $checkGoogleUser->createToken('AthleticsInstagramLoginToken')->accessToken;

                    if(!empty($setting->payment_merchant_code)){
                        $result['payment_merchant_code'] = $setting->payment_merchant_code;
                    }
                    $result['server_key'] = "AAAA9xJ_qKA:APA91bHyuLeIew8_jejBWxC7AfQSeD5gVrJvhBsINZXiAOFA0-Ysuvyt9DjatpK-bbrQWCkTnyTWrEkLkSB32IlO3AgFksHYwYc5ciPf4F0CHZ0TqIJrvbvOLpAw9xW7nXmGDhLZrO9B";
                    $result['status_code'] = config('httpcodes.HTTP_OK');
                    $result['error'] = false;
                    $result['message'] =  "USER LOGIN SUCCESSFUL.";
                    $result['loginResult'] = $checkGoogleUser;
                    $result['loginResult']['token'] = $token;
                    $result['payment_response'] = $payment_response;

                    return response()->json($result);
                } else {

                    $checkIfGoogleUserMobileExist = User::where('email', $request->email)->where('email', '!=', null)->first();

                    if (isset($checkIfGoogleUserMobileExist)) {
                        $checkIfGoogleUserMobileExist->update([
                            'social_id'         => $request->social_id,
                            'email'             => $request->email,
                            'name'              => $request->name,
                            'login_type'        => $request->login_type,
                            'device_type'       => $request->device_type,
                            'device_token'      => $request->device_token,
                            'latitude'      => !empty($request->latitude) ? $request->latitude : null,
                            'longitude'     => !empty($request->longitude) ? $request->longitude : null,
                            'version'       => $request->version
                        ]);

                        $token = $checkIfGoogleUserMobileExist->createToken('AthleticsInstagramLoginToken')->accessToken;

                        if(!empty($setting->payment_merchant_code)){
                            $result['payment_merchant_code'] = $setting->payment_merchant_code;
                        }
                        $result['server_key'] = "AAAA9xJ_qKA:APA91bHyuLeIew8_jejBWxC7AfQSeD5gVrJvhBsINZXiAOFA0-Ysuvyt9DjatpK-bbrQWCkTnyTWrEkLkSB32IlO3AgFksHYwYc5ciPf4F0CHZ0TqIJrvbvOLpAw9xW7nXmGDhLZrO9B";
                        $result['status_code'] = 200;
                        $result['error'] = false;
                        $result['message'] =  "USER LOGIN SUCCESSFUL.";
                        $result['loginResult'] = $checkIfGoogleUserMobileExist;
                        $result['loginResult']['token'] = $token;
                        $result['payment_response'] = $payment_response;

                        return response()->json($result);
                    }

                    $createGoogleUser = User::create([
                        'social_id'         => $request->social_id,
                        'email'             => $request->email,
                        'name'              => $request->name,
                        'login_type'        => $request->login_type,
                        'device_type'       => $request->device_type,
                        'device_token'      => $request->device_token,
                        'latitude'      => !empty($request->latitude) ? $request->latitude : null,
                        'longitude'     => !empty($request->longitude) ? $request->longitude : null,
                        'version'       => $request->version
                    ]);

                    $token = $createGoogleUser->createToken('AthleticsInstagramLoginToken')->accessToken;

                    if(!empty($setting->payment_merchant_code)){
                        $result['payment_merchant_code'] = $setting->payment_merchant_code;
                    }
                    $result['server_key'] = "AAAA9xJ_qKA:APA91bHyuLeIew8_jejBWxC7AfQSeD5gVrJvhBsINZXiAOFA0-Ysuvyt9DjatpK-bbrQWCkTnyTWrEkLkSB32IlO3AgFksHYwYc5ciPf4F0CHZ0TqIJrvbvOLpAw9xW7nXmGDhLZrO9B";
                    $result['status_code'] = 200;
                    $result['error'] = false;
                    $result['message'] =  "USER LOGIN SUCCESSFUL.";
                    $result['loginResult'] = $createGoogleUser;
                    $result['loginResult']['token'] = $token;
                    $result['payment_response'] = $payment_response;

                    return response()->json($result);
                }
            } else {
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }
    /**
     * Register api.
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        try {
            $validation = Validator::make($request->all(), [
                'phone' => 'required|numeric',
                'latitude' => 'required',
                'longitude' => 'required',
            ]);


            if ($validation->fails()) {
                $result['code']     = config('messages.http_codes.validation');
                $result['error']    = true;
                $result['message']  = $validation->messages()->first();
                return response()->json($result);
            }

            //check duplicate phone.
            $checkDuplicatePhone = User::where('phone', $request['phone'])->count();
            if ($checkDuplicatePhone > 0) {
                $exception['code'] = config('messages.http_codes.validation');
                $exception['error'] = true;
                $exception['message'] = "Phone number has already taken.";
                return response()->json($exception);
            }

            $input = $request->all();

            $sixDigitRandomOtp = mt_rand(1000, 9999);
            $otpExpirtyTime = now()->addMinutes(5);

            $input['otp'] = $sixDigitRandomOtp;
            $input['otp_expire_on'] = $otpExpirtyTime;
            //check version 
            if($request->version == 0.0){
                $result['code']     = 400;
                $result['error']    = true;
                $result['message']  = 'Please update the latest build from the store.';
                return response()->json($result);
            }
            if($request->device_type == 2){
                if($request->version <= 1.5){
                    $result['code']     = 400;
                    $result['error']    = true;
                    $result['message']  = 'Please update the latest build from the store';
                    return response()->json($result);
                }
                
            }
            else{
                if($request->version <= 3.6){
                    $result['code']     = 400;
                    $result['error']    = true;
                    $result['message']  = 'Please update the latest build from the store.';
                    return response()->json($result);
                }
              
            }

            if (User::create($input)) {
                //$success['token'] = $user->createToken('appToken')->accessToken;

                //send otp using klasiksms getway.
                $otpStatus = Helper::sendOtpOnMobile(request('phone'), $sixDigitRandomOtp);

                //below code is need to use when sms sending will work for indian client also.
                /* $otpStatus = Helper::sendOtpOnMobile(request('phone'), $sixDigitRandomOtp);
                if($otpStatus['statusCode'] == "X7021"){
                    return response()->json([
                        'code' => config('httpcodes.HTTP_OK'),
                        'message' => 'New otp',
                        'otp' => $sixDigitRandomOtp,
                    ]);
                } else {
                    $result['code'] = config('messages.http_codes.validation');
                    $result['error'] = true;
                    $result['message'] = $otpStatus['statusMsg'];
                    return response()->json($result);
                } */
                //end of send otp.

                return response()->json(
                    [
                        'code' => config('httpcodes.HTTP_OK'),
                        'message' => config('messages.error_messages.user_register_success'),
                        'otp' => $sixDigitRandomOtp,
                    ]
                );
            } else {
                $result['code'] = config('messages.http_codes.server');
                $result['error'] = true;
                $result['message'] = config('messages.error_messages.server_error');
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }


    public function logout(Request $res)
    {
        try {
            if (Auth::user()) {
                $user = Auth::user()->token();
                User::where('id', Auth::user()->id)->update(['otp' => null, 'device_token' => null]);

                Auth::user()->tokens->each(function($token, $key) {
                    $token->delete();
                });

                $user->revoke();

                return response()->json([
                    'code' => config('messages.http_codes.success'),
                    'message' => 'Logout Successfully'
                ]);
            } else {
                $result['code'] = config('messages.http_codes.server');
                $result['error'] = true;
                $result['message'] = config('messages.error_messages.server_error');
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function delete(Request $res)
    {
        try {
            if (Auth::user()) {
                $user = Auth::user()->token();
                User::where('id', Auth::user()->id)->update(['otp' => null, 'device_token' => null]);

                Auth::user()->tokens->each(function($token, $key) {
                    $token->delete();
                });

                $user->revoke();

                User::where('id', Auth::user()->id)->delete();

                return response()->json([
                    'code' => config('messages.http_codes.success'),
                    'message' => 'Account removed successfully'
                ]);
            } else {
                $result['code'] = config('messages.http_codes.server');
                $result['error'] = true;
                $result['message'] = config('messages.error_messages.server_error');
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }


    /**
     * User Verify OTP API
     */

    public function verifyOtp(Request $request)
    {

        try {

            $validation = Validator::make($request->all(), [
                'phone'      => 'required',
                'otp'        => 'required|min:4',
            ]);

            if ($validation->fails()) {

                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = true;
                $result['message'] = $validation->messages()->first();
                return response()->json($result);
            }

            $payment_response = [
                'Merchant Registered Name'=> 'Lleapp Sdn Bhd',
                'Merchant Name: Beam'     => ' Brings You Closer',
                'MerchantID'              => 'LDL',
                'Merchant Password'       => 'CjFU8VXieni37ndKs9zwtPTjwaz9kTKG',
                'Payment URL'             => 'https://securepay.e-ghl.com/IPG/Payment.aspx'
            ];
            $user = User::query()->where('phone', $request->phone)->first();

            if (isset($user)) {

                if ($user->otp == $request->otp) {
                    if(strtotime($user->otp_expire_on) < strtotime(now())){
                        $result['code'] = config('messages.http_codes.validation');
                        $result['error'] = true;
                        $result['message'] = 'Otp has expired';
                        return response()->json($result);

                    } else {
                        $result['server_key'] = "AAAA9xJ_qKA:APA91bHyuLeIew8_jejBWxC7AfQSeD5gVrJvhBsINZXiAOFA0-Ysuvyt9DjatpK-bbrQWCkTnyTWrEkLkSB32IlO3AgFksHYwYc5ciPf4F0CHZ0TqIJrvbvOLpAw9xW7nXmGDhLZrO9B";


                        $user->tokens()
                            ->delete();
                        $token = $user->createToken('BeamAppVerifyOtp')->accessToken;
                        $user->is_otp_verified = 1;

                        if ($user->save()) {
                            //get merchant key.
                            $setting = Setting::where('id', 3)->first();
                            if(!empty($setting->payment_merchant_code)){
                                $result['payment_merchant_code'] = $setting->payment_merchant_code;
                            }

                            $result['code'] = config('messages.http_codes.success');
                            $result['error'] = false;
                            $result['message'] = config('messages.error_messages.otp_verified');
                            $result['userInfo'] = $user;
                            $result['userInfo']['token'] = $token;
                            $result['$payment_response'] = $payment_response;
                            return response()->json($result);
                        } else {

                            $result['code'] = config('messages.http_codes.server');
                            $result['error'] = true;
                            $result['message'] = config('messages.error_messages.server_error');
                            return response()->json($result);
                        }

                    }
                } else {

                    $result['code'] = config('messages.http_codes.validation');
                    $result['error'] = true;
                    $result['message'] = config('messages.error_messages.invalid_otp');
                    return response()->json($result);
                }
            } else {

                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = true;
                $result['message'] = 'Phone number does not exist. Please signup to proceed.';
                return response()->json($result);
            }
        } catch (Exception $e) {

            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function resendOTP(Request $request)
    {
        try {
            $validation = Validator::make($request->all(), [
                'phone' => 'required|numeric',
            ]);

            $sixDigitRandomOtp = mt_rand(1000, 9999);

            if ($validation->fails()) {
                $result['code']     = config('messages.http_codes.validation');
                $result['error']    = true;
                $result['message']  = $validation->messages()->first();
                return response()->json($result);
            }

            if (User::where(['phone' => request('phone')])->first()) {
                $otpExpirtyTime = now()->addMinutes(5);
                User::where(['phone' => request('phone')])->update(['otp' => $sixDigitRandomOtp, 'otp_expire_on' => $otpExpirtyTime]);

                //send otp using klasiksms getway.
                $otpStatus = Helper::sendOtpOnMobile(request('phone'), $sixDigitRandomOtp);

                //below code is need to use when sms sending will work for indian client also.
                /* $otpStatus = Helper::sendOtpOnMobile(request('phone'), $sixDigitRandomOtp);
                if($otpStatus['statusCode'] == "X7021"){
                    return response()->json([
                        'code' => config('httpcodes.HTTP_OK'),
                        'message' => 'New otp',
                        'otp' => $sixDigitRandomOtp,
                    ]);
                } else {
                    $result['code'] = config('messages.http_codes.validation');
                    $result['error'] = true;
                    $result['message'] = $otpStatus['statusMsg'];
                    return response()->json($result);
                } */
                //end of send otp.

                return response()->json([
                    'code' => config('httpcodes.HTTP_OK'),
                    'message' => 'New otp',
                    'otp' => $sixDigitRandomOtp,
                ]);
            } else {
                //if authentication is unsuccessfull, notice how I return json parameters
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = true;
                $result['message'] = "Phone number does not exist. Please signup to proceed.";
                return response()->json($result);
            }
        } catch (Exception $e) {

            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }


    public function sendMessage($contact_one, $msg)
    {

        // $account_sid   = 'ACb259381580b24750b2f3d14b3114f398';
        // $auth_token    = 'd16b9460decab14a01317284681a0260';
        // $twilio_number = '+12064389166';
        $account_sid   = env('TWILIO_ACCOUNT_SID');
        $auth_token    = env('TWILIO_AUTH_TOKEN');
        $twilio_number = env('TWILIO_MOBILE');

        if ($contact_one) {
            $mobile = $contact_one;
            // $mobile = "8958960020";
            $client = new Client($account_sid, $auth_token);
            $client->messages->create(
                $mobile,
                ['from' => $twilio_number, 'body' => $msg]
            );
        }
    }

    public function update_profile(Request $request)
    {
        try {
            $validation = Validator::make($request->all(), [
                'name'               => 'required|max:255',
                "gender"             => 'required',
            ]);

            if ($validation->fails()) {

                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = true;
                $result['message'] = $validation->messages()->first();
                return response()->json($result);
            }

            $user = auth()->user();
            $requestData = $request->all();

            //update image.
            if ($request->hasFile('profile_image')) {
                $user = Helper::addProfilePic($request, $user);
                if (!empty($user->profile_image)) {
                    $requestData['profile_image'] = $user->profile_image;
                }
            }

            User::where('id', $user->id)->update($requestData);

            return response()->json(
                [
                    'code' => config('httpcodes.HTTP_OK'),
                    'message' => "Profile updated successfully",
                ],
                config('httpcodes.HTTP_OK')
            );
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function reminder()
    {

        date_default_timezone_set("Asia/Kuala_Lumpur");
        ///before date
        $current_date = date('Y-m-d');
        $day_before = date('Y-m-d', strtotime($current_date . ' +3 day'));

        $records = Reminder::where('date', '<=', $day_before)->where('date', '>=', $current_date)->where('reminder_2', 0)->get();

        foreach ($records as $key => $value) {
            $teampDays = ' -' . $value->remind_me . ' day';
            $day = date('Y-m-d', strtotime($value->date . $teampDays));
            $user = User::where('id', $value->user_id)->first();
            $current_date = date('Y-m-d');
            if ($day == $current_date) {
                $remind = Reminder::where('id', $value->id)->first();
                $remind->reminder_2 = 1;
                $remind->update();

                //push notification start here.
                $message = $value->title;
                $push_message = [
                    'title' => 'Reminder',
                    'body' => $message,
                    'type' => '0',
                    'user_type' => '0'
                ];

                if(!empty($user->device_type)){
                    if ($user->device_type == 1) {

                        $sendPush = Helper::sendNotification($user->device_token, $push_message);
                    } else {
                        $sendPush = Helper::iosSendNotification($user->device_token, $push_message);
                    }

                    //push notification end here.
                    $notification = [
                        'user_id' => $user->id,
                        'message' => $value->title,
                    ];

                    Notification::create($notification);
                }
            }
        }
       /* ///current date
        $current_date = date('Y-m-d');
        $reminders = Reminder::where('date', $current_date)->where('is_reminder', 0)->get();

        $time_now = mktime(date('h') + 5, date('i') + 30, date('s'));

        $current_time = date('H:i');
        foreach ($reminders as $key =>  $reminder) {

            $formate_time = date("H:i
", strtotime($reminder->time . ' -10 minutes'));
            $user = User::where('id', $reminder->user_id)->first();
        }*/



        $current_date = date('Y-m-d');
        $reminders = Reminder::where('date', $current_date)->where('is_reminder', 0)->get();

        $time_now = mktime(date('h') + 5, date('i') + 30, date('s'));

        $current_time = date('H:i');
        foreach ($reminders as $key =>  $reminder) {

            $formate_time = date("H:i
", strtotime($reminder->time . ' -10 minutes'));
            $user = User::where('id', $reminder->user_id)->first();
            if ($formate_time <= $current_time) {

                $check_reminder = Reminder::where('id', $reminder->id)->first();
                $check_reminder->is_reminder = 1;
                $check_reminder->update();

                //push notification start here.
                $message = $reminder->title;
                $push_message = [
                    'title' => 'Reminder',
                    'body' => $message,
                    'type' => '0',
                    'user_type' => '0'
                ];

                if(!empty($user->device_type)){
                    if ($user->device_type == 1) {

                        $sendPush = Helper::sendNotification($user->device_token, $push_message);
                    } else {
                        $sendPush = Helper::iosSendNotification($user->device_token, $push_message);
                    }

                    //push notification end here.
                    $notification = [
                        'user_id' => $user->id,
                        'message' => $reminder->title,
                    ];

                    Notification::create($notification);
                }
            }
        }
    }
    public function testing(){
        try{
            $input_arr = array(
                'name' => "sagar",
                'email' => "sagar741@yopmail.com",
                'contactNo' => "12345678",
                'message' => "sagar" ." Admin Add you.",
                'subject' => "testing"
            );
            $message = "Hii sagar this test mail";

            Mail::send('emails.demo', $input_arr, function ($message) use ($input_arr) {
                $message->to($input_arr['email'], 'admin')
                    ->subject($input_arr['subject']);
                $message->from('support@letsbeam.it', 'Beam App');
            });
        }catch(Exception $e){
            dd($e->getMessage());
        }
    }
}
