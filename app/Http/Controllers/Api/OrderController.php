<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Mail;
use App\Helpers\Helper;
use Validator;
use Illuminate\Support\Facades\Storage;
use App\Models\Occasion;
use App\Models\Category;
use App\Models\Product;
use App\Models\NeDesigns;
use App\Models\Contacts;
use App\Models\Banner;
use App\Models\OrderPrint;
use App\Models\CardDetails;
use DB;
use File;
use App\Models\Cart;
use App\Models\CartItem;
use App\Models\AddRecipient;
use App\Models\MailType;
use App\Models\PostCard;
use App\Models\Order;
use App\Models\OrderStatusMaster;
use App\Models\OrderStatus;
use App\Models\OrderItem;
use App\Models\EmailTemplate;

use App\Models\Rating;
use App\Models\EnvelopeColor;
use App\Models\PaperTypes;
use App\Models\UserDesignerRequest;
use App\User;
use App\Models\Notification;
use App\Models\Miscellaneous;
use App\Models\Promocode;

class OrderController extends Controller
{
    public function getSavedCard(Request $request)
    {
        try {

            $searchResult = CardDetails::select('id', 'card_holder_name', 'card_number', 'expires', 'cvv', 'user_id')->where('user_id', '=', Auth()->user()->id)->orderBy('id', 'DESC')->first();

            if (!empty($searchResult)) {

                $result['code'] = 200;
                $result['error'] = false;
                $result['result'] = $searchResult;
                return response()->json($result);
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function addCardDetails(Request $request)
    {
        try {

            $validation = Validator::make($request->all(), [
                'card_holder_name' => 'required',
                'card_number' => 'required',
                'expires' => 'required',

            ]);

            if ($validation->fails()) {

                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = true;
                $result['message'] = $validation->messages()->first();
                return response()->json($result);
            }

            $user = auth()->user();
            $requestData = $request->all();
            $requestData['user_id'] = auth()->user()->id;

            //check duplicate card.
            /* $cardDetail = CardDetails::where([
                'card_holder_name' => $requestData['card_holder_name'],
                'card_number' => $requestData['card_number'],
                'user_id' => $user->id
            ])->first();
            if(!empty($cardDetail->id)){
                CardDetails::where('id', $cardDetail->id)->update($requestData);
            } else {
                CardDetails::insert($requestData);
            } */

            CardDetails::insert($requestData);

            return response()->json(
                [
                    'code' => config('httpcodes.HTTP_OK'),
                    'message' => "Card details added successfully",
                ],
                config('httpcodes.HTTP_OK')
            );
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function addRecipientBackup(Request $request)
    {
        try {

            $validation = Validator::make($request->all(), [
                'name' => 'required',
                'phone_number' => 'required',
                'house_number' => 'required',
                'city' => 'required',
                'zip_code' => 'required',
                'state' => 'required',
            ]);


            if ($validation->fails()) {
                $result['code']     = config('messages.http_codes.validation');
                $result['error']    = true;
                $result['message']  = $validation->messages()->first();
                return response()->json($result);
            }

            $user = Auth()->user();

            $insert = $request->all();
            $insert['created_by'] = $user->id;

            //insert pic
            if ($request->hasFile('image')) {
                $user = Helper::addRecipientPic($request, $user);
                if (!empty($user->recipient_image)) {
                    $insert['image'] = $user->recipient_image;
                }
            }

            //$searchResult = FavoriteContacts::insert($insert);
            DB::table('add_recipient')->insert($insert);
            $last_id = DB::getPDO()->lastInsertId();

            if (!empty($last_id)) {
                $searchResult = AddRecipient::where('id', $last_id)->first();
                if (!empty($searchResult->image)) {
                    $searchResult->full_url = Storage::url('/recipient/' . $searchResult->image);
                }

                $result['code'] = 200;
                $result['result'] = $searchResult;
                $result['error'] = false;
                $result['message'] = 'Detail added successfully';
                return response()->json($result);
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "Something went wrong";
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function addRecipient(Request $request)
    {
        try {

            $validation = Validator::make($request->all(), [
                'name' => 'required',
                'phone_number' => 'required',
                'house_number' => 'required',
                'city' => 'required',
                'zip_code' => 'required',
                'state' => 'required',
                'full_address' => 'required',
            ]);


            if ($validation->fails()) {
                $result['code']     = config('messages.http_codes.validation');
                $result['error']    = true;
                $result['message']  = $validation->messages()->first();
                return response()->json($result);
            }

            $user = Auth()->user();

            $insert = $request->all();
            $insert['created_by'] = $user->id;

            //insert or update contact in contact table
            $checkDuplciate = Contacts::where('phone_number', $insert['phone_number'])->first();
            if (!empty($checkDuplciate->id)) {
                DB::table('contacts')->where('id', $checkDuplciate->id)->update($insert);
                $last_id = $checkDuplciate->id;
            } else {
                DB::table('contacts')->insert($insert);
                $last_id = DB::getPDO()->lastInsertId();
            }

            if (!empty($last_id)) {
                $searchResult = Contacts::where('id', $last_id)->first();
                if (!empty($searchResult->image)) {
                    $searchResult->full_url = Storage::url('/recipient/' . $searchResult->image);
                }

                $result['code'] = 200;
                $result['result'] = $searchResult;
                $result['error'] = false;
                $result['message'] = 'Detail added successfully';
                return response()->json($result);
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "Something went wrong";
                return response()->json($result);
            }
        } catch (\Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function getAllDirectoryMembers(Request $request)
    {
        try {

            $searchResult = FavoriteContacts::where('status', '=', 1)->get();

            if (!empty($searchResult[0])) {
                //make image url
                foreach ($searchResult as $key => $val) {
                    if (!empty($val->image)) {
                        $val->image_full_url = Storage::url('/favorite_contacts/' . $val->image);
                        $searchResult[$key] = $val;
                    }
                }
                $result['code'] = 200;
                $result['error'] = false;
                $result['result'] = $searchResult;
                return response()->json($result);
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function addCart(Request $request)
    {

        try {
            $validation = Validator::make($request->all(), [

                'send_to' => 'required',
                'total' => 'required',
            ]);

            if ($validation->fails()) {

                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = true;
                $result['message'] = $validation->messages()->first();
                return response()->json($result);
            }

            $user = auth()->user();
            $requestData = $request->all();
            

            //Code for IOS users.
                //dd($requestData['send_to']);
                $sendToArr = explode(',', $requestData['send_to']);
                //dd($user->id);
                if(count($sendToArr) > 1){
                    $iosUsers = [693,698,699,700,710,712,715,716,719,721,722,723,724,725,730,736,737,744,745,746,686,747];
                    if(in_array($user->id, $iosUsers)){
                        $result['code'] = config('messages.http_codes.validation');
                        $result['error'] = true;
                        $result['message'] = "Update Beam app now for enhanced features. Seamless order processing awaits. Thank you for choosing Beam!";
                        return response()->json($result);
                    } 
                }
                
               

            //End code for IOS users.

            $requestData['created_by'] = auth()->user()->id;

            //check if cart already added.
            $count = Cart::where('created_by', $user->id)->count();
            if ($count > 0) {
                Cart::where('created_by', $user->id)->update($requestData);
            } else {
                Cart::insert($requestData);
                //Also update card id in card item table because card item can be added without cart.
                $cart = Cart::where('created_by', $user->id)->first();
                CartItem::where('created_by', $user->id)->update([
                    'cart_id' => $cart->id,

                ]);
            }

            //add cart and cart item in order and order item table for temporary basis.
            //$this->addOrderAndOrderItem();

            //push notification start here.
            /* $message = 'Order placed successfully';
            $push_message = [
                'title' => 'Beam App',
                'body' => $message,
                'type' =>'0'
            ];
            $user = auth()->user();
            if($user->device_type==1){
                $sendPush = Helper::sendNotification($user->device_token,$push_message);
            }else if($user->device_type==2){
                $sendPush = Helper::iosSendNotification($user->device_token,$push_message);
            } */
            //push notification end here.

            return response()->json(
                [
                    'code' => config('httpcodes.HTTP_OK'),
                    'message' => "Cart details added successfully",
                ],
                config('httpcodes.HTTP_OK')
            );
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function getCartDetail(Request $request)
    {
        try {

            $validation = Validator::make($request->all(), [
                'cart_item_ids' => 'required',
            ]);

            if ($validation->fails()) {

                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = true;
                $result['message'] = $validation->messages()->first();
                return response()->json($result);
            }

            $user = Auth()->user();

            //final checkout of cart items. only these items will be display in wrap your order screen.
            if (!empty($request['cart_item_ids'])) {
                //make fincal checkout as unchecked then checked again.
                $cartItems = CartItem::select('id', 'product_type', 'product_id', 'cart_id', 'price', 'quantity', 'section', 'print_type', 'designer_charge')->where([
                    'created_by' => $user->id,
                ])->get();
                foreach ($cartItems as $cartItems) {
                    if (!empty($cartItems->id)) {
                        CartItem::where('id', $cartItems->id)->update(['final_checkout' => 0]);
                    }
                }

                $cartItemIds = explode(',', $request['cart_item_ids']);
                foreach ($cartItemIds as $cartIds) {
                    CartItem::where('id', $cartIds)->update(['final_checkout' => 1]);
                }
            }

            $cartItem = CartItem::select('id', 'product_type', 'product_id', 'cart_id', 'price', 'quantity', 'section', 'custom_order', 'attachment_original_name', 'pdf_page_count', 'print_type', 'designer_charge')->where([
                'created_by' => $user->id,
                'final_checkout' => 1
            ])->get();


            $cartItemCount = 0;

            $order = $user->id . date('Ymdhis');
            // dd($ordernumber);
            $returnArray = [];
            $grandTotal = null;
            if (!empty($cartItem[0])) {
                foreach ($cartItem as $item) {
                    $shippingFeeForMailType = 0;
                    $prod = null;
                    if ($item->custom_order != 1) {

                        if ($item->product_type == 1) {
                            $prod = PostCard::where('id', $item->product_id)->first();
                            $item->name = $prod->name;
                            $item->price = $prod->price;
                            $item->total_price = round(($prod->price * $item->quantity), 2);

                            //if designer charge is there then add it.
                            if(!empty($item->designer_charge) && $item->designer_charge > 0) {
                                $item->total_price = round(($item->total_price + $item->designer_charge), 2);
                            }
                        }

                        if ($item->product_type == 2) {
                            // dd($item);
                            $prod = Product::where('id', $item->product_id)->first();
                            $item->name = $prod->name;
                            $item->price = $prod->price;
                            $item->total_price = round(($prod->price * $item->quantity), 2);

                            //if designer charge is there then add it.
                            if(!empty($item->designer_charge) && $item->designer_charge > 0) {
                                $item->total_price = round(($item->total_price + $item->designer_charge), 2);
                            }
                        }

                        if ($item->product_type == 3) {
                            $prod = MailType::where('id', $item->product_id)->first();
                            $item->name = $prod->name;
                            if($item->print_type == 1) {
                                $item->price = $prod->price;
                                // $priceAndPdfCountSum = $prod->price * $item->pdf_page_count;
                            } else {
                                $item->price = $prod->colored_price;
                                //  $priceAndPdfCountSum = $prod->colored_price * $item->pdf_page_count;
                            }

                            if(!empty($item->pdf_page_count)) {
                                $item->price = $item->price * $item->pdf_page_count;
                            }

                            $item->price = round($item->price, 2);

                            $item->total_price = round(($item->price * $item->quantity), 2);

                            //if designer charge is there then add it.
                            if(!empty($item->designer_charge) && $item->designer_charge > 0) {
                                $item->total_price = round(($item->total_price + $item->designer_charge), 2);
                            }

                            if(!empty($prod->shipping_fee)) {
                                $shippingFeeForMailType = ($prod->shipping_fee) * ($item->quantity);
                            }

                        }
                    } else {
                        $item->name = null;
                        //  $item->price = $prod->price;
                        $item->total_price = round(($item->price * $item->quantity), 2);
                        if(!empty($item->designer_charge) && $item->designer_charge > 0) {
                            $item->total_price = round(($item->total_price + $item->designer_charge), 2);
                        }
                    }
                    $grandTotal = $grandTotal + $item->total_price + $shippingFeeForMailType;
                    $cartItemCount = $cartItemCount + $item->quantity;
                    //  Helper::pr($item);die;
                }
            }

            $returnArray['cart_item_count'] = $cartItemCount;
            $returnArray['order_number'] = $order;

            $returnArray['cart_items'] = $cartItem;

            $cart = Cart::where('created_by', '=', Auth()->user()->id)->orderBy('id', 'DESC')->first();
            $returnArray['grand_total'] = round($grandTotal, 2);
            $returnArray['cart'] = $cart;
            $envelope = EnvelopeColor::select('id', 'name', 'price')->get();
            $returnArray['envelope_colors'] = $envelope;
            $papertypes = PaperTypes::select('id', 'name', 'price')->where('status', 1)->orderBy('order_by', 'ASC')->get();
            $returnArray['paper_types'] = $papertypes;
            $miscellaneous = Miscellaneous::select('id', 'name', 'price')->where('id', 7)->first();
            $returnArray['same_day_delivery'] = $miscellaneous;

            $miscellaneous = Miscellaneous::select('id', 'name', 'price')->where('id', 8)->first();
            $returnArray['add_gift_note'] = $miscellaneous;


            if (!empty($returnArray)) {
                $result['code'] = 200;
                $result['error'] = false;
                $result['result'] = $returnArray;
                return response()->json($result);
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function addCartItem(Request $request)
    {
        try {
            $validation = Validator::make($request->all(), [
                'product_type' => 'required',
                'quantity' => 'required',
                'section' => 'required',
                // 'product_id' => 'required',
                // 'mail_type_id' => 'required',
                // 'pdf_page_count' => 'required',
                // 'name'       =>   'required',

            ]);

            if ($validation->fails()) {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = true;
                $result['message'] = $validation->messages()->first();
                return response()->json($result);
            }

            $user = auth()->user();
            $requestData = $request->all();

            //on update we will get cart item id.
            if(!empty($requestData['id'])) {
                $cartItemOnUpdate = CartItem::where('id', $requestData['id'])->first();
            }

            // dd($requestData);
            $requestData['created_by'] = auth()->user()->id;
            //get price and save
            if (!empty($requestData['product_type']) && ($requestData['product_type'] == 1)) {
                if(!empty($cartItemOnUpdate->id) && $cartItemOnUpdate->custom_order == 1) {
                    //No need to update the price
                } else {
                    $product = PostCard::where('id', $requestData['product_id'])->first();
                    $requestData['price'] = $product->price;

                }
                //insert print image
                if ($request->hasFile('image')) {
                    $user = Helper::addOrderPrint($request, $user);
                    if (!empty($user->image)) {
                        $requestData['image'] = $user->image;
                    }
                } else {
                    if(!empty($cartItemOnUpdate->id) && $cartItemOnUpdate->custom_order == 1) {
                        //No need to update the image in custom order
                    } else {
                        $product_old_image = PostCard::where('id', $request->product_id)->first()->image;
                        $extention = \File::extension($product_old_image);
                        $imageNameWithExtention = $user->id . date('Y-m-d-h-i-s') . '.' . $extention;
                        $check = Storage::copy('postcard/' . $product_old_image, 'order_print/' . $imageNameWithExtention);
                        $requestData['image'] = $imageNameWithExtention;
                    }
                    //echo $requestData['image']; die;

                }
            }

            if (!empty($requestData['product_type']) && ($requestData['product_type'] == 2)) {
                if(!empty($cartItemOnUpdate->id) && $cartItemOnUpdate->custom_order == 1) {
                    //No need to update the price
                } else {
                    $product = Product::where('id', $requestData['product_id'])->first();
                    $requestData['price'] = $product->price;
                }
                if ($request->hasFile('image')) {
                    $user = Helper::addOrderPrint($request, $user);
                    if (!empty($user->image)) {
                        $requestData['image'] = $user->image;
                    }
                }
            }

            if (!empty($requestData['product_type']) && ($requestData['product_type'] == 3)) {
                $product = MailType::where('id', $requestData['mail_type_id'])->first();
                // dd($product);
                $requestData['mail_type_id'] = $product->id;
                if ($request->hasFile('image')) {
                    $profile_image = $request->file('image');

                    $clientOriginalName = $profile_image->getClientOriginalName();
                    $requestData['attachment_original_name'] = pathinfo($clientOriginalName, PATHINFO_FILENAME);

                    $user = Helper::addOrderPrint($request, $user);
                    if (!empty($user->image)) {
                        $requestData['image'] = $user->image;
                    }


                }

            }


            //check if cart already added then get card id.
            $cart = Cart::where('created_by', $user->id)->first();
            if (!empty($cart->id)) {
                $requestData['cart_id'] = $cart->id;
            }

            //check if product already added
            if (empty($requestData['id'])) {
                //we also add multiple standard and registered email.
                if($requestData['product_type'] != 3) {
                    $where = array(
                        'product_id' => $requestData['product_id'],
                        'created_by' => $user->id,
                        'product_type' => $requestData['product_type'],

                    );

                    if (!empty($requestData['pdf_page_count'])) {
                        $where['pdf_page_count'] = $requestData['pdf_page_count'];
                    }

                    if (!empty($requestData['name'])) {
                        $where['name'] = $requestData['name'];
                    }

                    if (!empty($requestData['mail_type_id'])) {
                        $where['mail_type_id'] = $requestData['mail_type_id'];
                    }
                } else {
                    $where = null;
                }
            } else {
                $where = array('id' => $requestData['id']);
            }
            unset($requestData['id']);

            if(!empty($where)) {
                $checkCartItem = CartItem::where($where)->first();
            } else {
                $checkCartItem = null;
            }

            if (!empty($checkCartItem->id)) {
                if ($requestData['quantity'] == 0) {
                    CartItem::where('id', $checkCartItem->id)->delete();
                    $msg = "Item deleted";
                } else {
                    CartItem::where('id', $checkCartItem->id)->update($requestData);
                    $msg = "Item added to cart";
                }
            } else {
                if ($requestData['quantity'] == 0) {
                    $result['code'] = config('messages.http_codes.validation');
                    $result['error'] = false;
                    $result['message'] = "Item does not exist";
                    return response()->json($result);
                }

                //More than one product can not be added for postal. So delete all products of a user for postal.
                /* if (!empty($requestData['section']) && ($requestData['section'] == 1)) {
                    CartItem::where(['created_by' => $user->id, 'section' => "1"])->delete();
                } */

                CartItem::insert($requestData);
                $msg = "Item added to cart";
            }

            return response()->json(
                [
                    'code' => config('httpcodes.HTTP_OK'),
                    'message' => $msg,
                ],
                config('httpcodes.HTTP_OK')
            );
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function addCustomCartItem(Request $request)
    {
        try {
            $validation = Validator::make($request->all(), [
                'product_type' => 'required',
                'price' => 'required', //this designer charge
                'image' => 'required',
                'image_2' => 'required',
                'user_id' => 'required', //will receive from front end. For whom this order is
                'designer_id' => 'required',
            ]);

            if ($validation->fails()) {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = true;
                $result['message'] = $validation->messages()->first();
                return response()->json($result);
            }


            $user = auth()->user();
            $requestData = $request->all();

            $requestData['created_by'] = $requestData['user_id'];
            unset($requestData['user_id']);

            if($request->product_type == 0 && $request->product_type == null){
                return response()->json(
                    [
                        'code' => config('httpcodes.HTTP_OK'),
                        'message' => "Please add valid product type",
                    ],
                    config('httpcodes.HTTP_OK')
                );
            }

            //update designer and printing charge.
            if($requestData['product_type'] == 1) {
                $miscellaneous = Miscellaneous::where('id', 10)->first();
                $requestData['print_charge'] = !empty($miscellaneous->price) ? $miscellaneous->price : 0;
            } else {
                $miscellaneous = Miscellaneous::where('id', 9)->first();
                $requestData['print_charge'] = !empty($miscellaneous->price) ? $miscellaneous->price : 0;
            }
            $requestData['designer_charge'] = $requestData['price'];

            $requestData['price'] = $requestData['print_charge'];

            //insert print image
            if ($request->hasFile('image')) {
                $imageDynamicParameter = [
                    'folder_path' => "order_print",
                    'input_name' => "image",
                ];
                $imageName = Helper::addDynamicUploadImage($request, $imageDynamicParameter);
                if (!empty($imageName)) {
                    $requestData['image'] = $imageName;
                }
            }

            if ($request->hasFile('image_2')) {
                $imageDynamicParameter = [
                    'folder_path' => "order_print",
                    'input_name' => "image_2",
                ];
                $imageName = Helper::addDynamicUploadImage($request, $imageDynamicParameter);
                if (!empty($imageName)) {
                    $requestData['image_2'] = $imageName;
                }
            }

            if ($request->hasFile('image_3')) {
                $imageDynamicParameter = [
                    'folder_path' => "order_print",
                    'input_name' => "image_3",
                ];
                $imageName = Helper::addDynamicUploadImage($request, $imageDynamicParameter);
                if (!empty($imageName)) {
                    $requestData['image_3'] = $imageName;
                }
            }

            if ($request->hasFile('image_4')) {
                $imageDynamicParameter = [
                    'folder_path' => "order_print",
                    'input_name' => "image_4",
                ];
                $imageName = Helper::addDynamicUploadImage($request, $imageDynamicParameter);
                if (!empty($imageName)) {
                    $requestData['image_4'] = $imageName;
                }
            }


            $requestData['section'] = 1;
            $requestData['quantity'] = 1;
            $requestData['custom_order'] = 1;
            $requestData['status'] = 0;
            $requestData['product_type']= $request->product_type;

            CartItem::insert($requestData);
            $msg = "New Designs send to user successfully";

            //send push notification to uesr.
            //push notification start here.
            $message = 'Your custom design request is completed.';

            $push_message = [
                'title' => 'New Design',
                'body' => $message,
                'type' => '0',
                'user_type' => '0'
            ];

            $user = User::where('id', $requestData['created_by'])->first();

            if (!empty($user->device_token)) {
                if ($user->device_type == 1) {
                    $sendPush = Helper::sendNotification($user->device_token, $push_message);
                } elseif ($user->device_type == 2) {
                    $sendPush = Helper::iosSendNotification($user->device_token, $push_message);
                }
            }

            $notification = [
                'user_id' => $user->id,
                'message' => $message,
            ];

            Notification::create($notification);
            //push notification end here.

            return response()->json(
                [
                    'code' => config('httpcodes.HTTP_OK'),
                    'message' => $msg,
                ],
                config('httpcodes.HTTP_OK')
            );
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function deleteCard($id)
    {

        try {
            $ifRecordExist = CardDetails::where('id', $id)->count();
            if ($ifRecordExist < 1) {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }

            $user = auth()->user();

            if (CardDetails::where('id', $id)->delete()) {
                return response()->json(
                    [
                        'code' => config('httpcodes.HTTP_OK'),
                        'message' => "Card deleted successfully",
                    ],
                    config('httpcodes.HTTP_OK')
                );
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "Something went wrong";
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function getOrderListBackup(Request $request)
    {
        try {
            $user = Auth()->user();

            $orders = Order::where('created_by', $user->id)->orderBy('id', 'DESC')->get();

            $returnArray = [];
            if (!empty($orders[0])) {
                foreach ($orders as $order) {
                    $tempArray = [];

                    $orderStatus = OrderStatus::with(['orderStatusMaster'])->where('order_id', $order->id)->orderBy('id', 'DESC')->first();
                    if (!empty($orderStatus->orderStatusMaster->name)) {
                        $tempArray['order_status'] = $orderStatus->orderStatusMaster->name;
                    }

                    /* $orderItem = OrderItem::with(['product'])->where([
                        'order_id' => $order->id,
                    ])->first(); */

                    $orderItem = OrderItem::where([
                        'order_id' => $order->id,
                    ])->first();

                    if(!empty($orderItem->id)) {
                        if($orderItem->product_type == 1) {

                            if($orderItem->custom_order == 1) {
                                $tempArray['image'] = Storage::URL('order_print/' . $orderItem->image);
                            } else {
                                $check = PostCard::where('id', $orderItem->product_id)->first();
                                $tempArray['name'] = $check->name;

                                if (!empty($orderItem->image)) {
                                    $tempArray['image'] = Storage::URL('order_print/' . $orderItem->image);
                                } else {
                                    $tempArray['image'] = Storage::URL('postcard/' . $check->image);
                                }
                            }
                        } elseif($orderItem->product_type == 2) {
                            if($orderItem->custom_order == 1) {
                                $tempArray['image'] = Storage::URL('order_print/' . $orderItem->image);
                            } else {
                                $check = Product::where('id', $orderItem->product_id)->first();
                                $tempArray['name'] = $check->name;

                                if (!empty($orderItem->image)) {
                                    $tempArray['image'] = Storage::URL('order_print/' . $orderItem->image);
                                } else {
                                    $tempArray['image'] = Storage::URL('product/' . $check->image);
                                }
                            }
                        } else {
                            $check = MailType::where('id', $orderItem->mail_type_id)
                            ->first();
                            $tempArray['name'] = $orderItem->attachment_original_name." - ".$check->name;

                            if (!empty($orderItem->image)) {
                                $tempArray['image'] = Storage::URL('order_print/' . $orderItem->image);
                            }
                        }
                    }

                    /* if (!empty($orderItem->product->name)) {
                        $tempArray['name'] = $orderItem->product->name;
                        $tempArray['image'] = Storage::url('/product/' . $orderItem->product->image);
                    } */

                    $tempArray['created_at'] = $order->created_at;
                    $tempArray['id'] = $order->id;
                    $tempArray['total'] = $order->total;

                    //return status if review already given.
                    $rating = Rating::where('order_id', $order->id)
                        ->where('user_id', $user->id)
                        ->count();
                    //dd($rating);
                    if ($rating > 0) {
                        $tempArray['is_rating_given'] = 1;
                    } else {
                        $tempArray['is_rating_given'] = 0;
                    }
                    $returnArray[] = $tempArray;
                }
            }

            if (!empty($returnArray)) {
                $result['code'] = 200;
                $result['error'] = false;
                $result['result'] = $returnArray;
                return response()->json($result);
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function getOrderList(Request $request)
    {
        try {
            $user = Auth()->user();
            // $user = User::find(483);

            $orders = Order::where('created_by', $user->id)->orderBy('id', 'DESC')->get();
            // dd($orders);

            $returnArray = [];
            if (!empty($orders[0])) {
                foreach ($orders as $order) {
                    $orderItems = OrderItem::where([
                        'order_id' => $order->id,
                    ])->get();
                    foreach($orderItems as $orderItem) {

                        $tempArray = [];

                        /* $orderStatus = OrderStatus::with(['orderStatusMaster'])->where('order_id', $order->id)->orderBy('id', 'DESC')->first();
                        if (!empty($orderStatus->orderStatusMaster->name)) {
                            $tempArray['order_status'] = $orderStatus->orderStatusMaster->name;
                        } */

                        $statusArray = [2,3];
                        if(!empty($orderItem->status) && (in_array($orderItem->status, $statusArray))) {
                            $tempArray = [];

                            if($orderItem->status == 2) {
                                $tempArray['order_status'] = "Cancelled";
                            }

                            if($orderItem->status == 3) {
                                $tempArray['order_status'] = "Refunded";
                            }

                        } elseif(!empty($orderItem->easy_parcel_order_no) && $orderItem->easy_parcel_order_no != null) {
                            //get data from easy parcel.
                            $domain = env('EASY_PARCEL_BASE_URL');
                            //$domain = "https://connect.easyparcel.my/?ac=";
                            //$domain = "https://demo.connect.easyparcel.my/?ac=";

                            $action = "MPParcelStatusBulk";
                            $postparam = array(
                                //'authentication'    => 'XozEcfeQVu',
                                'authentication'    => env('EASY_PARCEL_AUTHENTICATION_KEY'),
                                'api'    => 'EP-AoBYqujRY',
                                'bulk'    => array(
                                    array(
                                        'order_no'    => $orderItem->easy_parcel_order_no,
                                    ),

                                ),
                            );

                            $url = $domain . $action;
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, $url);
                            curl_setopt($ch, CURLOPT_POST, 1);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postparam));
                            curl_setopt($ch, CURLOPT_HEADER, 0);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                            ob_start();
                            $return = curl_exec($ch);
                            ob_end_clean();
                            curl_close($ch);

                            $json = json_decode($return);
                            //dd($json->result[0]->parcel[0]->ship_status);
                            if(!empty($json->result[0]->parcel[0]->ship_status)) {

                                $tempArray['order_status'] = $json->result[0]->parcel[0]->ship_status;
                            } else {
                                $tempArray['order_status'] = null;
                            }
                            //dd($json->result[0]->parcel);

                        } else {
                            $orderStatus = OrderStatus::with(['orderStatusMaster'])->where('order_id', $order->id)->orderBy('id', 'DESC')->first();
                            if (!empty($orderStatus->orderStatusMaster->name)) {
                                $tempArray['order_status'] = $orderStatus->orderStatusMaster->name;
                            }
                        }
                        /* $orderItem = OrderItem::with(['product'])->where([
                            'order_id' => $order->id,
                        ])->first(); */


                        if(!empty($orderItem->id)) {
                            if($orderItem->product_type == 1) {

                                if($orderItem->custom_order == 1) {
                                    $tempArray['image'] = Storage::URL('order_print/' . $orderItem->image);
                                    $tempArray['name'] = "Custom Order";
                                } else {
                                    $check = PostCard::where('id', $orderItem->product_id)->withTrashed()->first();
                                    $tempArray['name'] = $check->name;

                                    if (!empty($orderItem->image)) {
                                        $tempArray['image'] = Storage::URL('order_print/' . $orderItem->image);
                                    } else {
                                        $tempArray['image'] = Storage::URL('postcard/' . $check->image);
                                    }
                                }
                            } elseif($orderItem->product_type == 2) {
                                if($orderItem->custom_order == 1) {
                                    $tempArray['image'] = Storage::URL('order_print/' . $orderItem->image);
                                    $tempArray['name'] = "Custom Order";
                                } else {
                                    $check = Product::where('id', $orderItem->product_id)->withTrashed()->first();
                                    $tempArray['name'] = $check->name;

                                    if (!empty($orderItem->image)) {
                                        $tempArray['image'] = Storage::URL('order_print/' . $orderItem->image);
                                    } else {
                                        $tempArray['image'] = Storage::URL('product/' . $check->image);
                                    }
                                }
                            } else {
                                $check = MailType::where('id', $orderItem->mail_type_id)
                                ->first();
                                if(!empty($orderItem->attachment_original_name)) {
                                    $tempArray['name'] = $orderItem->attachment_original_name." - ".$check->name;
                                }

                                if (!empty($orderItem->image)) {
                                    $tempArray['image'] = Storage::URL('order_print/' . $orderItem->image);
                                }
                            }
                        }


                        /* if (!empty($orderItem->product->name)) {
                            $tempArray['name'] = $orderItem->product->name;
                            $tempArray['image'] = Storage::url('/product/' . $orderItem->product->image);
                        } */

                        $tempArray['created_at'] = $order->created_at;
                        $tempArray['id'] = $orderItem->id;
                        //$tempArray['total'] = $order->total;
                        // $tempArray['total'] = $orderItem->price;
                        $tempArray['total'] = $orderItem->item_grand_total_price;

                        $tempArray['product_type'] = $orderItem->product_type;
                        $tempArray['section'] = $orderItem->section;

                        // dd(43434);
                        //return status if review already given.
                        $rating = Rating::where('order_item_id', $orderItem->id)
                            ->where('user_id', $user->id)
                            ->count();
                        // dd($rating);
                        if ($rating > 0) {
                            $tempArray['is_rating_given'] = 1;
                        } else {
                            $tempArray['is_rating_given'] = 0;
                        }
                        $returnArray[] = $tempArray;
                        // dd($returnArray);
                    }
                }
            }


            if (!empty($returnArray)) {
                $result['code'] = 200;
                $result['error'] = false;
                $result['result'] = $returnArray;
                return response()->json($result);
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }
        } catch (Exception $e) {
            dd($e->getMessage());
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function trackItemBackup(Request $request)
    {
        try {
            $validation = Validator::make($request->all(), [
                'order_id' => 'required',
            ]);


            if ($validation->fails()) {
                $result['code']     = config('messages.http_codes.validation');
                $result['error']    = true;
                $result['message']  = $validation->messages()->first();
                return response()->json($result);
            }

            $user = Auth()->user();

            $returnArray = [];

            $orderItem = OrderItem::with(['product'])->where([
                'order_id' => $request->order_id,
            ])->first();

            if (!empty($orderItem->product->name)) {
                $returnArray['name'] = $orderItem->product->name;
                $returnArray['image'] = Storage::url('/product/' . $orderItem->product->image);
                $returnArray['placed_at'] = $orderItem['create_at'];
            }

            $orderStatus = OrderStatus::with(['orderStatusMaster'])->where('order_id', $request->order_id)->orderBy('id', 'DESC')->orderBy('id', 'DESC')->get();

            if (!empty($orderStatus[0])) {
                foreach ($orderStatus as $status) {
                    $tempArray = [];
                    if (!empty($status->orderStatusMaster->name)) {
                        $tempArray['name'] = $status->orderStatusMaster->name;
                        $tempArray['date'] = $status->created_at;
                        $returnArray['status'][] = $tempArray;
                    }
                }
            }

            if (!empty($returnArray)) {
                $result['code'] = 200;
                $result['error'] = false;
                $result['result'] = $returnArray;
                return response()->json($result);
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function trackItem(Request $request)
    {
        try {
            $validation = Validator::make($request->all(), [
                'order_id' => 'required',
            ]);


            if ($validation->fails()) {
                $result['code']     = config('messages.http_codes.validation');
                $result['error']    = true;
                $result['message']  = $validation->messages()->first();
                return response()->json($result);
            }

            $user = Auth()->user();

            $returnArray = [];

            // $order = Order::where([
            //     'id' => $request->order_id,
            // ])->first();

            /* $orderItem = OrderItem::with(['product'])->where([
                'order_id' => $request->order_id,
            ])->first();

            if (!empty($orderItem->product->name)) {
                $returnArray['name'] = $orderItem->product->name;
                $returnArray['image'] = Storage::url('/product/' . $orderItem->product->image);
                $returnArray['placed_at'] = $orderItem['create_at'];
            } */

            $orderItem = OrderItem::where([
                'id' => $request->order_id,
            ])->first();

            if(!empty($orderItem->id)) {
                if($orderItem->product_type == 1) {

                    if($orderItem->custom_order == 1) {
                        $returnArray['image'] = Storage::URL('order_print/' . $orderItem->image);
                    } else {
                        $check = PostCard::where('id', $orderItem->product_id)->first();
                        $returnArray['name'] = $check->name;

                        if (!empty($orderItem->image)) {
                            $returnArray['image'] = Storage::URL('order_print/' . $orderItem->image);
                        } else {
                            $returnArray['image'] = Storage::URL('postcard/' . $check->image);
                        }
                    }
                } elseif($orderItem->product_type == 2) {
                    if($orderItem->custom_order == 1) {
                        $returnArray['image'] = Storage::URL('order_print/' . $orderItem->image);
                    } else {
                        $check = Product::where('id', $orderItem->product_id)->first();
                        $returnArray['name'] = $check->name;

                        if (!empty($orderItem->image)) {
                            $returnArray['image'] = Storage::URL('order_print/' . $orderItem->image);
                        } else {
                            $returnArray['image'] = Storage::URL('product/' . $check->image);
                        }
                    }
                } else {
                    $check = MailType::where('id', $orderItem->mail_type_id)
                    ->first();
                    $returnArray['name'] = $orderItem->attachment_original_name." - ".$check->name;

                    if (!empty($orderItem->image)) {
                        $returnArray['image'] = Storage::URL('order_print/' . $orderItem->image);
                    }
                }
            }

            $statusArray = [2,3];
            if(!empty($orderItem->status) && (in_array($orderItem->status, $statusArray))) {
                $tempArray = [];

                if($orderItem->status == 2) {
                    $tempArray['event_date'] = date('Y-m-d', strtotime($orderItem->updated_at));
                    $tempArray['event_time'] = date('H:i A', strtotime($orderItem->updated_at));
                    $tempArray['status'] = "Cancelled";
                    $tempArray['ep_status'] = null;
                    $tempArray['ep_status_code'] = null;
                    $tempArray['location'] = null;
                    $returnArray['status'][] = $tempArray;
                }

                if($orderItem->status == 3) {
                    $tempArray['event_date'] = date('Y-m-d', strtotime($orderItem->updated_at));
                    $tempArray['event_time'] = date('H:i A', strtotime($orderItem->updated_at));
                    $tempArray['status'] = "Refunded";
                    $tempArray['ep_status'] = null;
                    $tempArray['ep_status_code'] = null;
                    $tempArray['location'] = null;
                    $returnArray['status'][] = $tempArray;

                }

            } elseif(!empty($orderItem->easy_parcel_order_no) && $orderItem->easy_parcel_order_no != null) {

                //get data from easy parcel.
                $domain = env('EASY_PARCEL_BASE_URL');
                //$domain = "https://connect.easyparcel.my/?ac=";
                //$domain = "https://demo.connect.easyparcel.my/?ac=";

                /* $action = "MPParcelStatusBulk";
                $postparam = array(
                    //'authentication'    => 'XozEcfeQVu',
                    'authentication'    => env('EASY_PARCEL_AUTHENTICATION_KEY'),
                    'api'    => 'EP-AoBYqujRY',
                    'bulk'    => array(
                        array(
                            'order_no'    => $orderItem->easy_parcel_order_no,
                        ),

                    ),
                ); */

                $action = "EPTrackingBulk";
                $postparam = array(
                    'authentication'    => env('EASY_PARCEL_AUTHENTICATION_KEY'),
                    'api'    => 'EP-AoBYqujRY',
                    'bulk'    => array(
                        array(
                            'awb_no'    => $orderItem->easy_parcel_order_no,
                        ),
                    ),
                );

                $url = $domain . $action;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postparam));
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                ob_start();
                $return = curl_exec($ch);
                ob_end_clean();
                curl_close($ch);

                $json = json_decode($return);
                // dd($json->result[0]->status_list);
                if(!empty($json->result[0]->status_list)) {
                    foreach($json->result[0]->status_list as $key => $v) {
                        if(!empty($v->status)) {
                            $returnArray['status'][] = $v;
                        }
                    }
                    //  $returnArray['status'][] = $json->result[0]->status_list;
                } else {
                    $returnArray['status'][] = null;
                }
                //dd($json->result[0]->parcel);

            } else {

                $orderStatus = OrderStatus::with(['orderStatusMaster'])->where('order_id', $orderItem->order_id)->orderBy('id', 'DESC')->orderBy('id', 'DESC')->get();

                if (!empty($orderStatus[0])) {
                    foreach ($orderStatus as $status) {
                        $tempArray = [];
                        if (!empty($status->orderStatusMaster->name)) {
                            $tempArray['event_date'] = date('Y-m-d', strtotime($status->created_at));
                            $tempArray['event_time'] = date('H:i A', strtotime($status->created_at));
                            $tempArray['status'] = $status->orderStatusMaster->name;
                            $tempArray['ep_status'] = null;
                            $tempArray['ep_status_code'] = null;
                            $tempArray['location'] = null;
                            $returnArray['status'][] = $tempArray;
                        }
                    }
                }
            }


            if (!empty($returnArray)) {
                $returnArray['item'] = $orderItem;
                $result['code'] = 200;
                $result['error'] = false;
                $result['result'] = $returnArray;
                return response()->json($result);
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function addOrderAndOrderItem()
    {
        $user = Auth()->user();
        $cart = CartItem::where('created_by', $user->id)->first()->toArray();
        // dd($cart);
        unset($cart['id'], $cart['create_at']);
        DB::table('order')->insert($cart);
        $id = DB::getPdo()->lastInsertId();

        //generate Order number
        $randomOrderId = Helper::generateOrderNumber($id);
        Order::where('id', $id)->update([
            'order_number' => $randomOrderId
        ]);

        $cartItems = CartItem::where('created_by', $user->id)->get()->toArray();
        foreach ($cartItems as $cartItem) {
            unset($cartItem['id'], $cart['create_at'], $cartItem['cart_id']);
            $cartItem['order_id'] = $id;
            OrderItem::insert($cartItem);
        }

        //add order placed.
        $orderStatus = [
            'order_id' => $id,
            'order_status_name' => 'order_placed',
            'description' => 'Order place',
            'status' => 1,
        ];
        OrderStatus::insert($orderStatus);
    }

    public function addRating(Request $request)
    {
        try {
            $validation = Validator::make($request->all(), [
                'order_id' => 'required', //this is order item id.
                'rate' => 'required',
            ]);

            if ($validation->fails()) {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = true;
                $result['message'] = $validation->messages()->first();
                return response()->json($result);
            }

            $user = auth()->user();
            $requestData = $request->all();
            $requestData['user_id'] = auth()->user()->id;

            $orderItem = OrderItem::where('id', $requestData['order_id'])->first();
            $requestData['order_id'] = $orderItem->order_id;
            $requestData['order_item_id'] = $orderItem->id;

            //get product id and save
            if (!empty($orderItem->product_type) && ($orderItem->product_type == 1)) {
                if (!empty($requestData['product_id'])) {
                    $product = PostCard::where('id', $requestData['product_id'])->first();
                    $requestData['product_id'] = $product->id;
                }
            }

            if (!empty($orderItem->product_type) && ($orderItem->product_type == 2)) {
                if (!empty($requestData['product_id'])) {
                    $product = Product::where('id', $requestData['product_id'])->first();
                    $requestData['product_id'] = $product->id;
                }
            }

            //dd($requestData);
            //check if rating already added.
            $where = array(
                'order_item_id' => $requestData['order_item_id'],
                'user_id' => $user->id,
            );

            $checkRating = Rating::where($where)->first();
            if (!empty($checkRating->id)) {
                Rating::where('id', $checkRating->id)->update($requestData);
            } else {
                Rating::insert($requestData);
            }

            return response()->json(
                [
                    'code' => config('httpcodes.HTTP_OK'),
                    'message' => 'Rating added successfully',
                ],
                config('httpcodes.HTTP_OK')
            );
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function requestUrlToIPay88(Request $request)
    {
        try {
            $requestData = $request->all();

            return response()->json(
                [
                    'code' => config('httpcodes.HTTP_OK'),
                    'message' => 'Response URL',
                    'data' => $requestData
                ],
                config('httpcodes.HTTP_OK')
            );
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function backEndUrlToPay88(Request $request)
    {
        try {

            return response()->json(
                [
                    'code' => config('httpcodes.HTTP_OK'),
                    'message' => 'Back end URL',
                ],
                config('httpcodes.HTTP_OK')
            );
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function curlTestApi(Request $request)
    {
        /* $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://payment.ipay88.com.my/epayment/entry.asp',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => array('MerchantCode' => 'M33717','PaymentId' => '2','RefNo' => 'A00000001','Amount' => '1.00','Currency' => 'MYR','ProdDesc' => 'My Desc','UserName' => 'ak','UserEmail' => 'anjaylakh@gmail.com','UserContact' => '0126500100','Remark' => 'My remark','Lang' => 'UTF-8','SignatureType' => 'SHA256','Signature' => '3cf91c2628325affa36f3d9966674f6a6f07d3db52013670edab731504905a03','ResponseURL' => 'https://webmobril.org/dev/beam_app/api/requestUrlToIPay88','BackendURL' => 'https://webmobril.org/dev/beam_app/api/backEndUrlToPay88'),
        CURLOPT_HTTPHEADER => array(
            'Cookie: payment%2Eipay88%2Ecom%2Emy=paymentid=0; ASPSESSIONIDAEBACRQR=NMMPEEDCNCKFDJNMJFHEEIKE; ASPSESSIONIDCWRBRSRQ=CPENGEDCNNNBEOBALNPILFNK; ASPSESSIONIDAWQTTDSS=DBBNHEDCDFFGMLLKLCLNNHBH; ASPSESSIONIDAUQCSSRR=CEGJJFDCHAGPNJNPKHNMCKIN; ASPSESSIONIDQQXQQART=EFHBJFDCNKNFGBPMCGCAMEJC; ASPSESSIONIDQUTQSCQQ=FGENAIDCMEIDGIGGMKMMLIKG; ASPSESSIONIDAEDCBTTR=IOEHLMDCKKIOFPCGFNFJEPPL; ASPSESSIONIDAGSCQSBR=JHCLHODCEGFBGOPBJEBLMEDA; ASPSESSIONIDSWSTSCQQ=AMEDLODCLJAHHENPGGNLAOLP; ASPSESSIONIDQWQQSATR=MEBFOODCKLLAADIFNDPBPNFM; ASPSESSIONIDCGTDRRAQ=HOIPOODCBNJDBACEANIMKEBF; ASPSESSIONIDQEQAADAS=HLDNOODCNOBOKCAKECKAIKDB; ASPSESSIONIDSEQBDCBT=FCPDNGECAJPDCOBCFBEDNGDI; ASPSESSIONIDQUQQRCSR=GPJHPMECODBJGDOJCKEGMFCH; ASPSESSIONIDAURQQCTQ=KLFLJAFCGOMCKFIBMFFPJLAA; ASPSESSIONIDCURSSCRR=OJOJKAFCCJEEJCOCBOMLEMFO; ASPSESSIONIDQGSDCABS=DHFPLAFCPBLKBIKEHGKOJFBC; ASPSESSIONIDQWQRQBRS=KOIFOAFCPDIHIJKDJKGCKPKH; ASPSESSIONIDSEQABBDT=AFMDMAFCCDBHJHJNGPNMMFDJ; ASPSESSIONIDSETASRTT=KJNNBBFCFDJOCOABONAGHOMP'
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response; */

        $signature = hash('sha256', 'R1uHTYlzjkM33717A00000001100MYR');

        $url = 'https://payment.ipay88.com.my/epayment/entry.asp';
        $data = array('MerchantCode' => 'M33717', 'PaymentId' => '2', 'RefNo' => 'A00000001', 'Amount' => '1.00', 'Currency' => 'MYR', 'ProdDesc' => 'My Desc', 'UserName' => 'ak', 'UserEmail' => 'anjaylakh@gmail.com', 'UserContact' => '0126500100', 'Remark' => 'My remark', 'Lang' => 'UTF-8', 'SignatureType' => 'SHA256', 'Signature' => '3cf91c2628325affa36f3d9966674f6a6f07d3db52013670edab731504905a03', 'ResponseURL' => 'https://webmobril.org/dev/beam_app/api/requestUrlToIPay88', 'BackendURL' => 'https://webmobril.org/dev/beam_app/api/backEndUrlToPay88');
        $data = http_build_query($data);
        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n"
                    . "Content-Length: " . strlen($data) . "\r\n",
                'method'  => 'POST',
                'content' => $data
            )
        );

        $context = stream_context_create($options);
        $fp = fopen('https://payment.ipay88.com.my/epayment/entry.asp', 'r', false, $context);
        fpassthru($fp);
        fclose($fp);

        // use key 'http' even if you send the request to https://...
        /* $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data)
            )
        );
        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        if ($result === FALSE) {  } else {

        } */
    }

    public function addChatRequestStaus(Request $request)
    {
        try {
            $validation = Validator::make($request->all(), [
                'id' => 'required',
                'is_request_accepted' => 'required',
                'device_type' => 'required',
            ]);

            if ($validation->fails()) {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = true;
                $result['message'] = $validation->messages()->first();
                return response()->json($result);
            }

            $requestData = $request->all();
            $requestData['user_id'] = auth()->user()->id;

            UserDesignerRequest::where('id', $requestData['id'])->update(['is_request_accepted' => $requestData['is_request_accepted']]);

            //push notification start here.
            if ($requestData['is_request_accepted'] == 1) {
                $message = 'Designer accepted the user request';
            } else {
                $message = 'Designer rejected the user request';
            }

            $push_message = [
                'title' => 'User request',
                'body' => $message,
                'type' => '0',
                'user_type' => '0'
            ];

            $chatUsers = UserDesignerRequest::where('id', $requestData['id'])->first();
            $user = User::where('id', $chatUsers->from_user_id)->first();

            if (!empty($user->device_token)) {
                if ($user->device_type == 1) {
                    $sendPush = Helper::sendNotification($user->device_token, $push_message);
                } elseif ($user->device_type == 2) {
                    $sendPush = Helper::iosSendNotification($user->device_token, $push_message);
                }
            }

            $notification = [
                'user_id' => $user->id,
                'message' => $message,
            ];

            Notification::create($notification);
            //push notification end here.


            return response()->json(
                [
                    'code' => config('httpcodes.HTTP_OK'),
                    'message' => 'Chat request status updated successfully',
                    'result' => $user
                ],
                config('httpcodes.HTTP_OK')
            );
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function checkChatRequestStatusOfPerticularDesigner(Request $request)
    {
        try {
            $validation = Validator::make($request->all(), [
                'designer_user_id' => 'required',
            ]);

            if ($validation->fails()) {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = true;
                $result['message'] = $validation->messages()->first();
                return response()->json($result);
            }

            $requestData = $request->all();
            $chatUsers = UserDesignerRequest::where('from_user_id', $requestData['designer_user_id'])->where('to_user_id', auth()->user()->id)->first();

            return response()->json(
                [
                    'code' => config('httpcodes.HTTP_OK'),
                    'message' => 'Chat request status updated successfully',
                    'result' => $chatUsers
                ],
                config('httpcodes.HTTP_OK')
            );
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }
    public function getRating(Request $request)
    {
        try {

            $searchResult = Rating::where('user_id', $request->user_id)->where('order_item_id', $request->order_id)->first();

            if ($searchResult) {

                $result['code'] = 200;
                $result['error'] = false;
                $result['result'] = $searchResult;
                return response()->json($result);
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }
    public function refunds(Request $request)
    {
        try {
            $validation = Validator::make($request->all(), [
                'order_item_id' => 'required',
                'refund_reason' => 'required|max:200',
                'refund_email' => 'required|max:100'
            ]);

            if ($validation->fails()) {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = true;
                $result['message'] = $validation->messages()->first();
                return response()->json($result);
            }
            $id = $request->order_item_id;
            $orderItem = OrderItem::find($id);
            if(empty($orderItem)) {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            } else {
                $orderItem->update([
                    'status'=> 3,
                    'refund_reason'=>$request->refund_reason,
                    'refund_email'=> $request->refund_email,
                ]);

                $order = Order::where('id', $orderItem->order_id)->first();
                $orderNUmber  = $order->order_number;
                $orderTotal = $orderItem->item_grand_total_price;

                $emailTemplateForGiftVendor = EmailTemplate::where('id', 9)->first();
                $emailTemplateForPrinterVendor = EmailTemplate::where('id', 9)->first();

                if($orderItem->section == 2) {
                    $product = Product::where('id', $orderItem->product_id)->first();
                    $productName = (!empty($product->name)) ? $product->name : null;
                    //get vendor user info
                    $vendor = User::where('id', $product->created_by)->first();

                    $input_arr = array(
                        'name' => $vendor->name,
                        'email' => $vendor->email,
                    //    'email' => "sagar741@yopmail.com",
                        'body' => !empty($emailTemplateForGiftVendor->message) ? $emailTemplateForGiftVendor->message : null,
                        'subject' => !empty($emailTemplateForGiftVendor->subject) ? $emailTemplateForGiftVendor->subject : null,
                        'order_number' => !empty($orderNUmber) ? $orderNUmber : null,
                        'product_name' => !empty($productName) ? $productName : null,
                        'quantity' => !empty($orderItem->quantity) ? $orderItem->quantity : null,
                        'total_amount' => !empty($orderTotal) ? $orderTotal : null,
                    );

                    Mail::send(['html' => 'emails.refund'], $input_arr, function ($message) use ($input_arr) {
                        $message->to($input_arr['email'], 'admin')
                            ->subject($input_arr['subject']);
                        $message->from(config('mail.username'), 'Beam App');
                    });
                } else {
                    $product = PostCard::where('id', $orderItem->product_id)->withTrashed()->first();
                    $productName = (!empty($product->name)) ? $product->name : null;
                    //get vendor user info
                    $vendor = User::where('id', $product->created_by)->first();

                    $input_arr = array(
                        'name' => $vendor->name,
                        // 'email' => $vendor->email,
                       'email' => "sagar741@yopmail.com",
                        'body' => !empty($emailTemplateForGiftVendor->message) ? $emailTemplateForGiftVendor->message : null,
                        'subject' => !empty($emailTemplateForGiftVendor->subject) ? $emailTemplateForGiftVendor->subject : null,
                        'order_number' => !empty($orderNUmber) ? $orderNUmber : null,
                        'product_name' => !empty($productName) ? $productName : null,
                        'quantity' => !empty($orderItem->quantity) ? $orderItem->quantity : null,
                        'total_amount' => !empty($orderTotal) ? $orderTotal : null,
                    );

                    Mail::send(['html' => 'emails.refund'], $input_arr, function ($message) use ($input_arr) {
                        $message->to($input_arr['email'], 'admin')
                            ->subject($input_arr['subject']);
                        $message->from(config('mail.username'), 'Beam App');
                    });
                }
                $msg = "Your Refund request has been sent to the Merchant for further action. For further assistance , you can send your query in support section";

                //send push notification to uesr.
                //push notification start here.
                $message = 'Your Refund request has been sent to the Merchant for further action. For further assistance , you can send your query in support section';

                $push_message = [
                    'title' => 'Refund',
                    'body' => $message,
                    'type' => '0',
                    'user_type' => '0'
                ];

                $user = User::where('id', $orderItem['created_by'])->first();
                // dd($user);

                if (!empty($user->device_token)) {
                    if ($user->device_type == 1) {
                        $sendPush = Helper::sendNotification($user->device_token, $push_message);
                    } elseif ($user->device_type == 2) {
                        $sendPush = Helper::iosSendNotification($user->device_token, $push_message);
                    }
                }

                $notification = [
                    'user_id' => $user->id,
                    'message' => $message,
                ];

                Notification::create($notification);

                $result['code'] = 200;
                $result['error'] = false;
                $result['message'] = "Your Refund request has been sent to the Merchant for further action. For further assistance , you can send your query in support section.";
                $result['result'] = $orderItem;
                return response()->json($result);
            }
        } catch(Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function validatePromocodeBackup(Request $request)
    {
        try {

            $validation = Validator::make($request->all(), [
                'promocode' => 'required',
                'product_id' => 'required',
                'product_type' => 'required',
            ]);

            if ($validation->fails()) {

                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = true;
                $result['message'] = $validation->messages()->first();
                return response()->json($result);
            }

            $user = Auth()->user();

            if($request->product_type != '2'){
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "Promo code is not valid for the selected product.";
                return response()->json($result);
            }
           
            $promocode = Promocode::where([
                'promocode' => $request['promocode'],
                'status' => 1,
            ])->first();

            //check if user has already used this code
            $orderItem = OrderItem::where([
                'promocode' => $request['promocode'],
                'created_by' => $user->id
            ])->first();

            if(!empty($orderItem->id)){
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "Promo code already applied.";
                return response()->json($result);
            }

            if(empty($promocode->id)){
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "Promo code is not valid.";
                return response()->json($result);
            }

            $query = Product::query();
            $query->where('id', $request['product_id']);
            $productdata = $query->first();
            if(!empty($productdata->id)){
                $subcatArr = explode(',', $promocode->associated_products_categories);
                if (! in_array($productdata->category_id, $subcatArr)){
                    $result['code'] = config('messages.http_codes.validation');
                    $result['error'] = false;
                    $result['message'] = "Promo code is not valid for the selected product.";
                    return response()->json($result);
                }
            }
            
            if(date('Y-m-d') > $promocode->expiration_date){
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "Promo code has expired.";
                return response()->json($result);                
            }

            if($promocode->user_restrictions == "for_new_user"){
                $procodeDate = date($promocode->created_at, strtotime('Y-m-d'));
                $userDate = date($user->created_at, strtotime('Y-m-d'));                

                if($procodeDate > $userDate){
                    $result['code'] = config('messages.http_codes.validation');
                    $result['error'] = false;
                    $result['message'] = "This promo code is exclusive for new users.";
                    return response()->json($result); 
                }                               
            }

            if($promocode->user_restrictions == "for_old_user"){
                $procodeDate = date($promocode->created_at, strtotime('Y-m-d'));
                $userDate = date($user->created_at, strtotime('Y-m-d'));                

                if($procodeDate < $userDate){
                    $result['code'] = config('messages.http_codes.validation');
                    $result['error'] = false;
                    $result['message'] = "This promo code is exclusive for existing users.";
                    return response()->json($result); 
                }                               
            }


            if($promocode->usage_limits > 0){
                if($promocode->usage_limits < $promocode->usage_count+1){
                    $result['code'] = config('messages.http_codes.validation');
                    $result['error'] = false;
                    $result['message'] = "Promo code usage limit has been reached.";
                    return response()->json($result);                     
                }                
            }
            
            //dd($promocode);

            $returnArray = array(
                'id'=> $promocode->id,
                'promocode' => $promocode->promocode,
                'discount_type' => $promocode->discount_type,
                'discount_value' => $promocode->discount_value
            );
                


            $result['code'] = 200;
            $result['error'] = false;
            $result['result'] = $returnArray;
            $result['message'] = "Ok";
            return response()->json($result);
            
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function validatePromocode(Request $request)
    {
        try {

            $validation = Validator::make($request->all(), [
                'promocode' => 'required',
                'product_id' => 'required',
                'product_type' => 'required',
            ]);

            if ($validation->fails()) {

                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = true;
                $result['message'] = $validation->messages()->first();
                return response()->json($result);
            }

            $user = Auth()->user();

            
            // if($request->product_type != '2'){
            //     $result['code'] = config('messages.http_codes.validation');
            //     $result['error'] = false;
            //     $result['message'] = "Promo code is not valid for the selected product.";
            //     return response()->json($result);
            // }
           
            $promocode = Promocode::where([
                'promocode' => $request['promocode'],
                'status' => 1,
                'product_type' => $request['product_type'], 
            ])->first();

            //check if user has already used this code
            // $orderItem = OrderItem::where([
            //     'promocode' => $request['promocode'],
            //     'created_by' => $user->id
            // ])->first();

            // if(!empty($orderItem->id)){
            //     $result['code'] = config('messages.http_codes.validation');
            //     $result['error'] = false;
            //     $result['message'] = "Promo code already applied.";
            //     return response()->json($result);
            // }

            if(empty($promocode->id)){
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "Promo code is not valid.";
                return response()->json($result);
            }

            //check if promocode is valid based on type and product.
            if($request->product_type == '3'){
                $promocode = Promocode::where([
                    'promocode' => $request['promocode'],
                    'type' => $request['product_id'],                    
                ])->first();

                if(empty ($promocode->id)){
                    $result['code'] = config('messages.http_codes.validation');
                    $result['error'] = false;
                    $result['message'] = "Promo code is not valid for the selected product.";
                    return response()->json($result);
                }
            } else if($request->product_type == '1'){
                $query = Postcard::query();
                $query->where('id', $request['product_id']);
                $productdata = $query->first();
                
                if($productdata != null){           
                    if($promocode->product_id != null){
                        
                        $subcatArr = explode(',', $promocode->product_id);
                        if (! in_array($productdata->id, $subcatArr)){
                           
                            $result['code'] = config('messages.http_codes.validation');
                            $result['error'] = false;
                            $result['message'] = "Promo code is not valid for the selected product.";
                            return response()->json($result);
                        }
                    }
                } else {
                    $result['code'] = config('messages.http_codes.validation');
                    $result['error'] = false;
                    $result['message'] = "Promo code is not valid for the selected product.";
                    return response()->json($result);
                }
            } else {
                $query = Product::query();
                $query->where('id', $request['product_id']);
                $productdata = $query->first();
                if(!empty($productdata->id)){
                    $subcatArr = explode(',', $promocode->associated_products_categories);
                    if (! in_array($productdata->category_id, $subcatArr)){
                        $result['code'] = config('messages.http_codes.validation');
                        $result['error'] = false;
                        $result['message'] = "Promo code is not valid for the selected product.";
                        return response()->json($result);
                    }
                }
            }
            
            if(date('Y-m-d') > $promocode->expiration_date){
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "Promo code has expired.";
                return response()->json($result);                
            }

            if($promocode->user_restrictions == "for_new_user"){
                $procodeDate = date($promocode->created_at, strtotime('Y-m-d'));
                $userDate = date($user->created_at, strtotime('Y-m-d'));                

                if($procodeDate > $userDate){
                    $result['code'] = config('messages.http_codes.validation');
                    $result['error'] = false;
                    $result['message'] = "This promo code is exclusive for new users.";
                    return response()->json($result); 
                }                               
            }

            if($promocode->user_restrictions == "for_old_user"){
                $procodeDate = date($promocode->created_at, strtotime('Y-m-d'));
                $userDate = date($user->created_at, strtotime('Y-m-d'));                

                if($procodeDate < $userDate){
                    $result['code'] = config('messages.http_codes.validation');
                    $result['error'] = false;
                    $result['message'] = "This promo code is exclusive for existing users.";
                    return response()->json($result); 
                }                               
            }


            if($promocode->usage_limits > 0){
                if($promocode->usage_limits < $promocode->usage_count+1){
                    $result['code'] = config('messages.http_codes.validation');
                    $result['error'] = false;
                    $result['message'] = "Promo code usage limit has been reached.";
                    return response()->json($result);                     
                }                
            }
            
            //dd($promocode);

            $returnArray = array(
                'id'=> $promocode->id,
                'promocode' => $promocode->promocode,
                'discount_type' => $promocode->discount_type,
                'discount_value' => $promocode->discount_value
            );
                


            $result['code'] = 200;
            $result['error'] = false;
            $result['result'] = $returnArray;
            $result['message'] = "Ok";
            return response()->json($result);
            
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }
}
