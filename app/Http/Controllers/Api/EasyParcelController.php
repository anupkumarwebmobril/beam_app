<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Mail;
use App\Helpers\Helper;
use Validator;
use DB;
use Illuminate\Support\Facades\Storage;
use App\Models\Occasion;
use App\Models\Category;
use App\Models\Product;
use App\Models\NeDesigns;
use App\Models\Contacts;
use App\Models\Banner;
use App\Models\OrderPrint;
use App\Models\MailType;
use App\Models\UserFavoriteContacts;
use App\Models\PostCard;
use App\User;
use App\Models\UserDesignerRequest;
use App\Models\CartItem;
use App\Models\Wishlist;
use App\Models\Notification;
use App\Models\Faq;
use App\Models\Query;
use App\Models\ProductDetailsImage;
use Auth;
use App\Models\Cart;
use App\Models\Reminder;
use Carbon\Carbon;
use App\Models\Payment;
use App\Models\OrderItem;
use App\Models\Order;

class EasyParcelController extends Controller
{

    public function checking_order_status_backup(Request $request)
    {
        try {

            $user = auth()->user();

            $domain = "https://connect.easyparcel.my/?ac=";
            //$domain = "https://demo.connect.easyparcel.my/?ac=";

            $action = "MPOrderStatusBulk";
            $postparam = array(
                //'authentication'    => 'aGff5XBOsT',
                'authentication'    => 'XozEcfeQVu',                
                'api'    => 'EP-AoBYqujRY',
                'bulk'    => array(
                    array(
                        'order_no'    => 'EI-CIH94',
                    ),
                    
                ),
            );

            $url = $domain . $action;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postparam));
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

            ob_start();
            $return = curl_exec($ch);
            ob_end_clean();
            curl_close($ch);

            $json = json_decode($return);
            echo "<pre>";
            print_r($json);
            echo "</pre>";
            die('ak');

            if (isset($user) && $user != null) {
                if (!empty($user->profile_image)) {
                    $user->full_url = Storage::url('/profile_pictures/' . $user->profile_image);
                }
                $result['code'] = 200;
                $result['error'] = false;
                $result['result'] = $user;
                return response()->json($result);
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function checking_order_status(Request $request)
    {
        try {

            $user = auth()->user();

            $domain = "https://connect.easyparcel.my/?ac=";
            //$domain = "https://demo.connect.easyparcel.my/?ac=";

            $action = "MPOrderStatusBulk";
            $postparam = array(
                //'authentication'    => 'aGff5XBOsT',
                'authentication'    => 'XozEcfeQVu',                
                'api'    => 'EP-AoBYqujRY',
                'bulk'    => array(
                    array(
                        'order_no'    => 'EI-CIH94',
                    ),
                    
                ),
            );

            $url = $domain . $action;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postparam));
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

            ob_start();
            $return = curl_exec($ch);
            ob_end_clean();
            curl_close($ch);

            $json = json_decode($return);
            echo "<pre>";
            print_r($json);
            echo "</pre>";
            die('ak');

            if (isset($user) && $user != null) {
                if (!empty($user->profile_image)) {
                    $user->full_url = Storage::url('/profile_pictures/' . $user->profile_image);
                }
                $result['code'] = 200;
                $result['error'] = false;
                $result['result'] = $user;
                return response()->json($result);
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function checking_parcel_status(Request $request)
    {
        try {

            $user = auth()->user();

            $domain = "https://connect.easyparcel.my/?ac=";
            //$domain = "https://demo.connect.easyparcel.my/?ac=";

            $action = "MPParcelStatusBulk";
            $postparam = array(
                //'authentication'    => 'XozEcfeQVu',
                'authentication'    => env('EASY_PARCEL_AUTHENTICATION_KEY'),                
                'api'    => 'EP-AoBYqujRY',
                'bulk'    => array(
                    array(
                        'order_no'    => 'EI-CIH94',
                    ),
                    
                ),
            );

            $url = $domain . $action;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postparam));
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

            ob_start();
            $return = curl_exec($ch);
            ob_end_clean();
            curl_close($ch);

            $json = json_decode($return);
            
            /* echo "<pre>";
            print_r($json);
            echo "</pre>";
            die('ak'); */

            if (isset($json) && $json != null) {
                if (!empty($user->profile_image)) {
                    $user->full_url = Storage::url('/profile_pictures/' . $user->profile_image);
                }
                $result['code'] = 200;
                $result['error'] = false;
                $result['result'] = $json;
                return response()->json($result);
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function tracking_parcel_details(Request $request)
    {
        try {

            $user = auth()->user();

            $domain = "https://connect.easyparcel.my/?ac=";
            //$domain = "https://demo.connect.easyparcel.my/?ac=";

            $action = "MPTrackingBulk";
            $postparam = array(
                //'authentication'    => 'aGff5XBOsT',
                'authentication'    => env('EASY_PARCEL_AUTHENTICATION_KEY'),                
                'api'    => 'EP-AoBYqujRY',
                'bulk'    => array(
                    array(
                        'order_no'    => 'EI-CIH94',
                    ),
                    
                ),
            );

            $url = $domain . $action;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postparam));
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

            ob_start();
            $return = curl_exec($ch);
            ob_end_clean();
            curl_close($ch);

            $json = json_decode($return);
            echo "<pre>";
            print_r($json);
            echo "</pre>";
            die('ak');

            if (isset($json) && $json != null) {
                if (!empty($user->profile_image)) {
                    $user->full_url = Storage::url('/profile_pictures/' . $user->profile_image);
                }
                $result['code'] = 200;
                $result['error'] = false;
                $result['result'] = $user;
                return response()->json($result);
            } else {
                $result['code'] = config('messages.http_codes.validation');
                $result['error'] = false;
                $result['message'] = "No record found";
                return response()->json($result);
            }
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }

    public function klasiksms(Request $request)
    {
        try {
                    
            $curl = curl_init();
            
            //$key = "71dc1965276e33718db087d9d35227c3";
            $key = env('KLASIKSMS_KEY');
            $recipient = "9667269116";
            $message = "test mesg";
            curl_setopt_array($curl, array(
            CURLOPT_URL => "https://www.klasiksms.com/api/sendsms.php?key=$key&recipient=$recipient&message=$message",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiZTZmMGE1MTUyMmEzYzg0OTNmOWQxN2U5ZDI1MTc3YjdhMjIyMjJlZWNlYjgxNTk3Y2I1YjRhZmM3NjlkN2RhMmJmMzk0NTYxNzFiN2UwNDEiLCJpYXQiOjE2NTUyNzUwMDksIm5iZiI6MTY1NTI3NTAwOSwiZXhwIjoxNjg2ODExMDA5LCJzdWIiOiIxMCIsInNjb3BlcyI6W119.MsdbxUjzNnryo46g1B5vDQXACHZHpxdk0ZVWPV566hpQJKXOlz_fUZWSTTyepM3fG1dEBst85VM9Ifv0ui3vT8AhE1DdLk4kxY0IbdrEty_uY_W4oTAPkwYbk2HUU4wKqgXbBV6w_oE_YC0uag4Yga-p1aZnaECCjf1rhKjLezOXDjIjHVHTN07g3_C8-ZEYCTSP3h-3Ctpze0K1XwI-zBFBeSpOH7U_es3o_EFLFJRDzmqSkBKOlItDqOcLz7A7iaKZ4znt4NlHmqPEVcP_hIUjY1XKlJgCS7i6ts8ZPF2iM-yIMK2a-INk2HvMqUm60KNV7EDVVnYzDDwF-3UGwf0MfJK867uu3KtIxikPUZu9WTmZyJEeeqwNMgEMvey8eVGxfR9Gdhgu4oJWuupKIKSyV9zdnsoc0cLnY2JeckOp1smyMWJiT9XLFO78JgoE-jq01gMNt6uYo5giEt7zMnJW6N0uiNv_HZ6IbL0Yu2Jd-6O-0nK8MrV72nxFAh4hqkqJakwbj4upsz_pf5L7fVjZmMnWX5-HgpsXSMYD_2docv4KiFhf6AAJYRw4EsWIA6OT8oRhI1sX0khHIbQlZrpIK-6taU8xfBZuvwNR9fIVB-aEmw4M57AdfiyKLFzKsDW4xpApOopeMqLrQThB-8h5QtekPntgIWKJnHhfXqI',
                'Cookie: language=english; PHPSESSID=6bisl8cchebgadqpjksmk5v8h1'
            ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            
            $xml = simplexml_load_string($response);
            $json = json_encode($xml);
            $array = json_decode($json,TRUE);
            echo '<pre>';
            print_r($array);exit;

            //$json = json_decode($response, TRUE);
            //$a = json_decode($json);
            echo "<pre>";
            print_r($data);
            echo "</pre>";

           

            die('ak');

            
        } catch (Exception $e) {
            $exception['code'] = config('messages.http_codes.server');
            $exception['error'] = true;
            $exception['message'] = $e->getMessage(); //config('messages.error_messages.server_error');
            return response()->json($exception);
        }
    }
}
