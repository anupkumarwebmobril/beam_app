<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Auth::user()) {
            if(Auth::user()->user_type == 1){
                return Redirect::to('admin/dashboard');
            } else {
                return Redirect::to('vendor/dashboard');
            }
           
            //echo Auth::user()->user_type;die;
            //Auth::logout();
            //return Redirect::to('/');
        }
        return view('home');
    }
}
