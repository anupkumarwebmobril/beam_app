<?php

namespace App\Helpers;
use Illuminate\Support\Facades\Storage;
use App\Models\PostCard;
use Auth;
use App\User;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\AddRecipient;
use App\Models\Contacts;
use App\Models\EmailTemplate;
class Helper
{
    public static function pr($obj) {
        echo '<pre>';
        print_r($obj);
        echo '</pre>';
    }

    public static function addProfilePic($request, $user){
        try {

            $profile_image = $request->file('profile_image');

            $name = $profile_image->getClientOriginalName();

            // Delete file
            if (file_exists($user->profile_image)) {
                unlink($user->profile_image);
            }

            $extention = \File::extension($name);
            $imageNameWithExtention = $user->id.'.'.$extention;

            Storage::putFileAs('/profile_pictures/', $request->file('profile_image'), $imageNameWithExtention);
            $user->profile_image = $imageNameWithExtention;
            return $user;
        } catch (Exception $e) {

            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public static function addOrderPrint($request, $user){
        try {

            $profile_image = $request->file('image');

            $name = $profile_image->getClientOriginalName();

            // Delete file
            if (file_exists($user->image)) {
                unlink($user->image);
            }

            $extention = \File::extension($name);
            $imageNameWithExtention = $user->id.date('Y-m-d-h-i-s').'.'.$extention;

            Storage::putFileAs('/order_print/', $request->file('image'), $imageNameWithExtention);
            $user->image = $imageNameWithExtention;
            return $user;
        } catch (Exception $e) {

            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public static function addDynamicUploadImage($request, $imageDynamicParameter){
        try {

            $profile_image = $request->file($imageDynamicParameter['input_name']);

            $name = $profile_image->getClientOriginalName();

            $extention = \File::extension($name);
            $imageNameWithExtention = mt_rand(1000, 9999).'_'.date('Y-m-d-h-i-s').'.'.$extention;

            Storage::putFileAs('/'.$imageDynamicParameter['folder_path'].'/', $request->file($imageDynamicParameter['input_name']), $imageNameWithExtention);
            return $imageNameWithExtention;
        } catch (Exception $e) {

            return redirect()->back()->withErrors($e->getMessage());
        }
    }
    public static function addOrderEmptyPrint($request, $user){
        try {
           //  $profile_image = $request->file('image');
           // dd($profile_image);
            $profile_image =  PostCard::select('image')->where('id', $request->product_id)->first();
            // dd($profile_image);
            $profile = $profile_image->image;
            // dd($profile);


            $name = $profile->getClientOriginalName();

            // Delete file
            if (file_exists($user->image)) {
                unlink($user->image);
            }

            $extention = \File::extension($name);
            $imageNameWithExtention = $user->id.date('Y-m-d-h-i-s').'.'.$extention;

            Storage::putFileAs('/order_print/', $request->file('image'), $imageNameWithExtention);
            $user->image = $imageNameWithExtention;
            return $user;
        } catch (Exception $e) {

            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public static function addFavoriteContactsPic($request, $user){
        try {

            $image = $request->file('image');

            $name = $image->getClientOriginalName();


            $extention = \File::extension($name);
            $imageNameWithExtention = mt_rand(1000, 9999).'_'.date('Y_m_d_h_i_s').'.'.$extention;

            Storage::putFileAs('/favorite_contacts/', $request->file('image'), $imageNameWithExtention);
            $user->favorite_contacts_image = $imageNameWithExtention;
            return $user;
        } catch (Exception $e) {

            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public static function addRecipientPic($request, $user){
        try {

            $image = $request->file('image');

            $name = $image->getClientOriginalName();


            $extention = \File::extension($name);
            $imageNameWithExtention = mt_rand(1000, 9999).'_'.date('Y_m_d_h_i_s').'.'.$extention;

            Storage::putFileAs('/recipient/', $request->file('image'), $imageNameWithExtention);
            $user->recipient_image = $imageNameWithExtention;
            return $user;
        } catch (Exception $e) {

            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public static function addProductImageOne($request, $user){
        try {
            $image = $request->file('image');


            $name = $image->getClientOriginalName();

            $extention = \File::extension($name);
            $imageNameWithExtention = mt_rand(1000, 9999).'_'.date('Y_m_d_h_i_s').'.'.$extention;

            Storage::putFileAs('/product/', $request->file('image'), $imageNameWithExtention);
            $user->image = $imageNameWithExtention;
            return $user;
        } catch (\Exception $e) {

            return redirect()->back()->withErrors($e->getMessage());
        }
    }
     public static function addProductImageTwo($request, $user){
        try {
            $image = $request->file('image_2');


            $name = $image->getClientOriginalName();

            $extention = \File::extension($name);
            $imageNameWithExtention = mt_rand(1000, 9999).'_'.date('Y_m_d_h_i_s').'.'.$extention;

            Storage::putFileAs('/product/', $request->file('image_2'), $imageNameWithExtention);
            $user->image = $imageNameWithExtention;
            return $user;
        } catch (\Exception $e) {

            return redirect()->back()->withErrors($e->getMessage());
        }
    }
     public static function addProductImageThree($request, $user){
        try {
            $image = $request->file('image_3');


            $name = $image->getClientOriginalName();

            $extention = \File::extension($name);
            $imageNameWithExtention = mt_rand(1000, 9999).'_'.date('Y_m_d_h_i_s').'.'.$extention;

            Storage::putFileAs('/product/', $request->file('image_3'), $imageNameWithExtention);
            $user->image = $imageNameWithExtention;
            return $user;
        } catch (\Exception $e) {

            return redirect()->back()->withErrors($e->getMessage());
        }
    }
     public static function addProductImageFour($request, $user){
        try {
            $image = $request->file('image_4');


            $name = $image->getClientOriginalName();

            $extention = \File::extension($name);
            $imageNameWithExtention = mt_rand(1000, 9999).'_'.date('Y_m_d_h_i_s').'.'.$extention;

            Storage::putFileAs('/product/', $request->file('image_4'), $imageNameWithExtention);
            $user->image = $imageNameWithExtention;
            return $user;
        } catch (\Exception $e) {

            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    //for postcard

    public static function addPostcardImageOne($request, $user){
        try {
            $image = $request->file('image');


            $name = $image->getClientOriginalName();

            $extention = \File::extension($name);
            $imageNameWithExtention = mt_rand(1000, 9999).'_'.date('Y_m_d_h_i_s').'.'.$extention;

            Storage::putFileAs('/postcard/', $request->file('image'), $imageNameWithExtention);
            $user->image = $imageNameWithExtention;
            return $user;
        } catch (\Exception $e) {

            return redirect()->back()->withErrors($e->getMessage());
        }
    }
     public static function addPostcardImageTwo($request, $user){
        try {
            $image = $request->file('image_2');


            $name = $image->getClientOriginalName();

            $extention = \File::extension($name);
            $imageNameWithExtention = mt_rand(1000, 9999).'_'.date('Y_m_d_h_i_s').'.'.$extention;

            Storage::putFileAs('/postcard/', $request->file('image_2'), $imageNameWithExtention);
            $user->image = $imageNameWithExtention;
            return $user;
        } catch (\Exception $e) {

            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    // designer image

    public static function addDesignerImage($request, $data){
        try {
            // dd($data);
            $image = $request->file('profile_image');

            $name = $image->getClientOriginalName();

            $extention = \File::extension($name);
            $imageNameWithExtention = mt_rand(1000, 9999).'_'.date('Y_m_d_h_i_s').'.'.$extention;

            Storage::putFileAs('/profile_pictures/', $request->file('profile_image'), $imageNameWithExtention);

            // dd($imageNameWithExtention);

            return $imageNameWithExtention;
        } catch (\Exception $e) {

            return redirect()->back()->withErrors($e->getMessage());
        }
    }
    public static function addsampleImage($request, $data){
        try {
            // dd($data);
            $image = $request->file('sample_design_image');

            $name = $image->getClientOriginalName();

            $extention = \File::extension($name);
            $imageNameWithExtention = mt_rand(1000, 9999).'_'.date('Y_m_d_h_i_s').'.'.$extention;

            Storage::putFileAs('/sample_design_image/', $request->file('sample_design_image'), $imageNameWithExtention);


            return $imageNameWithExtention;
        } catch (\Exception $e) {

            return redirect()->back()->withErrors($e->getMessage());
        }
    }
    public static function addsampleImage_2($request, $data){
        try {
            // dd($data);
            $image = $request->file('sample_design_image_2');

            $name = $image->getClientOriginalName();

            $extention = \File::extension($name);
            $imageNameWithExtention = mt_rand(1000, 9999).'_'.date('Y_m_d_h_i_s').'.'.$extention;

            Storage::putFileAs('/sample_design_image_2/', $request->file('sample_design_image_2'), $imageNameWithExtention);


            return $imageNameWithExtention;
        } catch (\Exception $e) {

            return redirect()->back()->withErrors($e->getMessage());
        }
    }
    public static function addsampleImage_3($request, $data){
        try {
            // dd($data);
            $image = $request->file('sample_design_image_3');

            $name = $image->getClientOriginalName();

            $extention = \File::extension($name);
            $imageNameWithExtention = mt_rand(1000, 9999).'_'.date('Y_m_d_h_i_s').'.'.$extention;

            Storage::putFileAs('/sample_design_image_3/', $request->file('sample_design_image_3'), $imageNameWithExtention);


            return $imageNameWithExtention;
        } catch (\Exception $e) {

            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public static function commonAddUpdateImageMethod($request, $user){
        try {
            $image = $request->file('image');

            $name = $image->getClientOriginalName();

            $extention = \File::extension($name);
            $imageNameWithExtention = mt_rand(1000, 9999).'_'.date('Y_m_d_h_i_s').'.'.$extention;

            Storage::putFileAs('/'.$request['folder_name'].'/', $request->file('image'), $imageNameWithExtention);
            $user->image = $imageNameWithExtention;
            return $user;
        } catch (\Exception $e) {

            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public static function sendNotification($token,$message)
    {
        //$token = "ckzx1lg3SZmM7zcjQCex2H:APA91bGhAnf46AZg9SieQEEkiqnLJ5f3de0PGibyUcu95560jUyRVLk61NZJjkK_Bq1e2ZwzvExnUuG1-Q-R0fk1V1Cq4rVDn17PnynmEagO_n7YSKXAvGUSgpmBM6M5m8qdb6H7wzj8";
        $data = array(
            'title'             =>  $message['title'],
            'body'              =>  $message['body'],
            //'type'              =>  $message['type'],
            'type'         =>  $message['user_type'],
        );
        //$data =  Response::json(array('data'=>$data));
        //$data = json_encode(array('data'=>$data),true);
        $url = 'https://fcm.googleapis.com/fcm/send';
        $msg = array(
            'to'            =>  $token,
            // 'notification'  =>  $message,
            'data'          =>  $data,
        );
        // echo "<pre>";
        // print_r($msg);
        // die;
        $headers = array(
            'Authorization: key=AAAA9xJ_qKA:APA91bHyuLeIew8_jejBWxC7AfQSeD5gVrJvhBsINZXiAOFA0-Ysuvyt9DjatpK-bbrQWCkTnyTWrEkLkSB32IlO3AgFksHYwYc5ciPf4F0CHZ0TqIJrvbvOLpAw9xW7nXmGDhLZrO9B',
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($msg));

        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        // Close connection
        curl_close($ch);
        //echo '<pre>'; print_r($result);die;
        return  $result;
    }

    public static function iosSendNotification($token,$message){
        //$token = 'dBxi2IibkUgWhUAevXhGF2:APA91bHDYxG6cZJLHaycSZpOn9JePzM_gnG2dWmOmX_m52rzlwFDRWdSHOROXJ6lAqZknGsLDgF9wCXVikUhpdCRklCK_97HzpReEtPS7otGM7pLVcHREJtuTTsB4dTpQOMDKkKxIN9g';

        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $jsonArray['title'] = $message['title'];
        $jsonArray['description'] = $message['body'];
        $jsonArray['body'] = $message['body'];
        $jsonArray['user_type'] = $message['user_type'];

        $notification = array('title' => $message['title'], 'body' => $message['body'], 'sound' => 'default', 'badge' => '1', 'notification_type' => $message['type']
        );

        $fcmNotification = [
            // 'registration_ids' => $tokenList, //multple token array
            'registration_ids' => [$token], //single token
            'notification' => $notification,
            'priority' => 'high',
            'data' => $jsonArray,
        ];

        $json = json_encode($fcmNotification);

        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: key=AAAA9xJ_qKA:APA91bHyuLeIew8_jejBWxC7AfQSeD5gVrJvhBsINZXiAOFA0-Ysuvyt9DjatpK-bbrQWCkTnyTWrEkLkSB32IlO3AgFksHYwYc5ciPf4F0CHZ0TqIJrvbvOLpAw9xW7nXmGDhLZrO9B';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);

        if ($result === false) {
            die('FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }

    public static function generateOrderNumber($id){
        $orderNumber = date('Ymd').$id;
        return $orderNumber;
    }

    public static function checkVendorAndUserZipcode($items){
        $authUser = Auth::user();
        $returnArray = [];
        //get send to id
        $sendToUsers = Order::where('id', $items->order_id)->first();
        $sendUserIds = explode(',', $sendToUsers->send_to);
        $recipientIds = [];
        foreach($sendUserIds as $receiverId){
            //get receiver zip code
            $recipient = Contacts::where('id', $receiverId)->first();
            if($recipient->zip_code == $authUser->zip_code){
                $recipientIds[] = $recipient;
            }
        }
        $returnArray['recipientIds'] = $recipientIds;
        // dd($returnArray);
        return $returnArray;
    }


    public static function sendOtpOnMobile($phone,$otp)
    {
        $curl = curl_init();
        $otp1 = 'RM0.00 | letsbeam.it | Please confirm your Beam account using this One Time Password '.$otp. '. This OTP is valid for the next five minutes. Happy Beaming..';
        $otp2 = urlencode($otp1);
        //$key = "71dc1965276e33718db087d9d35227c3";
        $key = env('KLASIKSMS_KEY');
        //$phone = "+919667269116";
        //$otp = "test mesg";
        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://www.klasiksms.com/api/sendsms.php?key=$key&recipient=$phone&message=".$otp2,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiZTZmMGE1MTUyMmEzYzg0OTNmOWQxN2U5ZDI1MTc3YjdhMjIyMjJlZWNlYjgxNTk3Y2I1YjRhZmM3NjlkN2RhMmJmMzk0NTYxNzFiN2UwNDEiLCJpYXQiOjE2NTUyNzUwMDksIm5iZiI6MTY1NTI3NTAwOSwiZXhwIjoxNjg2ODExMDA5LCJzdWIiOiIxMCIsInNjb3BlcyI6W119.MsdbxUjzNnryo46g1B5vDQXACHZHpxdk0ZVWPV566hpQJKXOlz_fUZWSTTyepM3fG1dEBst85VM9Ifv0ui3vT8AhE1DdLk4kxY0IbdrEty_uY_W4oTAPkwYbk2HUU4wKqgXbBV6w_oE_YC0uag4Yga-p1aZnaECCjf1rhKjLezOXDjIjHVHTN07g3_C8-ZEYCTSP3h-3Ctpze0K1XwI-zBFBeSpOH7U_es3o_EFLFJRDzmqSkBKOlItDqOcLz7A7iaKZ4znt4NlHmqPEVcP_hIUjY1XKlJgCS7i6ts8ZPF2iM-yIMK2a-INk2HvMqUm60KNV7EDVVnYzDDwF-3UGwf0MfJK867uu3KtIxikPUZu9WTmZyJEeeqwNMgEMvey8eVGxfR9Gdhgu4oJWuupKIKSyV9zdnsoc0cLnY2JeckOp1smyMWJiT9XLFO78JgoE-jq01gMNt6uYo5giEt7zMnJW6N0uiNv_HZ6IbL0Yu2Jd-6O-0nK8MrV72nxFAh4hqkqJakwbj4upsz_pf5L7fVjZmMnWX5-HgpsXSMYD_2docv4KiFhf6AAJYRw4EsWIA6OT8oRhI1sX0khHIbQlZrpIK-6taU8xfBZuvwNR9fIVB-aEmw4M57AdfiyKLFzKsDW4xpApOopeMqLrQThB-8h5QtekPntgIWKJnHhfXqI',
            'Cookie: language=english; PHPSESSID=6bisl8cchebgadqpjksmk5v8h1'
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $xml = simplexml_load_string($response);
        $json = json_encode($xml);
        $array = json_decode($json,TRUE);
       // dd($array);
        return $array;
    }

    public static function getEasyParcelOrderStatus($item){
        // $domain = env('EASY_PARCEL_BASE_URL');
        $domain = "https://connect.easyparcel.my/?ac=";
            // dd($domain);

            $action = "EPTrackingBulk";
            $postparam = array(
                'authentication'    => "71dc1965276e33718db087d9d35227c3",
                'api'    => 'EP-AoBYqujRY',
                'bulk'    => array(
                    array(
                        'awb_no'    => $item->easy_parcel_order_no,
                    ),

                ),
            );

            $url = $domain . $action;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postparam));
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            ob_start();
            $return = curl_exec($ch);
            ob_end_clean();
            curl_close($ch);

            $json = json_decode($return);
            return $json;

    }


    public static function getEmailTemplate(){
        try {
            $emailTemplate = EmailTemplate::get();
            return $emailTemplate;
        } catch (\Exception $e) {

            return redirect()->back()->withErrors($e->getMessage());
        }
    }
}

