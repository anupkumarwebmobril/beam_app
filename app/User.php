<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'login_type', 'social_id', 'phone', 'otp', 'otp_expire_on', 'is_otp_verified', 'name', 'email', 'password', 'status', 'user_type', 'gender', 'latitude', 'longitude', 'rank', 'designer_charge', 'details', 'experience', 'sample_design_image', 'sample_design_image_2', 'sample_design_image_3', 'device_type', 'device_token', 'device_id', 'house_number', 'land_mark', 'city', 'zip_code', 'state','profile_image','easy_parcel_api_key', 'business_name', 'business_registration_no', 'business_phone_no', 'contact_person_1', 'contact_number_1', 'contact_person_2', 'contact_number_2', 'website', 'street_address', 'country',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function tokens(){

        return $this->hasMany('App\Models\PassportToken');
    }

    use SoftDeletes;
}
