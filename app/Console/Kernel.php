<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Http\Request;
use Validator;
use App\User;
use Auth;
use App\Helpers\Helper;

use Twilio\Rest\Client;
use Twilio\Jwt\ClientToken;
use Twilio\Exceptions\TwilioException;
use Twilio\Exceptions\RestException;
use App\Models\Reminder;
use App\Models\Notification;
use Token;
use DB;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(
            function () {
                // date_default_timezone_set("Asia/Kuala_Lumpur");
                date_default_timezone_set("Asia/Kolkata");

                ///before date
                $current_date = date('Y-m-d');
                $day_before = date('Y-m-d', strtotime($current_date . ' +3 day'));

                $records = Reminder::where('date', '<=', $day_before)->where('date', '>=', $current_date)->where('reminder_2', 0)->get();

                foreach ($records as $key => $value) {
                    $teampDays = ' -' . $value->remind_me . ' day';
                    $day = date('Y-m-d', strtotime($value->date . $teampDays));
                    $user = User::where('id', $value->user_id)->first();
                    $current_date = date('Y-m-d');
                    if ($day == $current_date) {
                        $remind = Reminder::where('id', $value->id)->first();
                        $remind->reminder_2 = 1;
                        $remind->update();

                        //push notification start here.
                        $message = $value->title;
                        $push_message = [
                            'title' => 'Reminder',
                            'body' => $message,
                            'type' => '0',
                            'user_type' => '0'
                        ];

                        if(!empty($user->device_type)) {
                            if ($user->device_type == 1) {

                                $sendPush = Helper::sendNotification($user->device_token, $push_message);
                            } else {
                                $sendPush = Helper::iosSendNotification($user->device_token, $push_message);
                            }

                            //push notification end here.
                            $notification = [
                                'user_id' => $user->id,
                                'message' => $value->title,
                            ];

                            Notification::create($notification);
                        }
                    }
                }
                /* ///current date
                 $current_date = date('Y-m-d');
                 $reminders = Reminder::where('date', $current_date)->where('is_reminder', 0)->get();

                 $time_now = mktime(date('h') + 5, date('i') + 30, date('s'));

                 $current_time = date('H:i');
                 foreach ($reminders as $key =>  $reminder) {

                     $formate_time = date("H:i
", strtotime($reminder->time . ' -10 minutes'));
                     $user = User::where('id', $reminder->user_id)->first();
                 }*/



                $current_date = date('Y-m-d');
                $reminders = Reminder::where('date', $current_date)->where('is_reminder', 0)->get();

                $time_now = mktime(date('h') + 5, date('i') + 30, date('s'));

                $current_time = date('H:i');
                foreach ($reminders as $key =>  $reminder) {

                    $formate_time = date("H:i
", strtotime($reminder->time . ' -10 minutes'));
                    $user = User::where('id', $reminder->user_id)->first();
                    if ($formate_time <= $current_time) {

                        $check_reminder = Reminder::where('id', $reminder->id)->first();
                        $check_reminder->is_reminder = 1;
                        $check_reminder->update();

                        //push notification start here.
                        $message = $reminder->title;
                        $push_message = [
                            'title' => 'Reminder',
                            'body' => $message,
                            'type' => '0',
                            'user_type' => '0'
                        ];

                        if(!empty($user->device_type)) {
                            if ($user->device_type == 1) {

                                $sendPush = Helper::sendNotification($user->device_token, $push_message);
                            } else {
                                $sendPush = Helper::iosSendNotification($user->device_token, $push_message);
                            }

                            //push notification end here.
                            $notification = [
                                'user_id' => $user->id,
                                'message' => $reminder->title,
                            ];

                            Notification::create($notification);
                        }
                    }
                }
                \Log::info('Cron job executed with status code: ');

            }

        )->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
