
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">	
</head>
<body>
    <div>Hi,{{ $name }}</div>
    <br>
    <div>Please print the order in {{($print_type == 2) ? "Coloured" : "Black & white"}}. Click <a href="{{$url}}" download>here</a>  to download the file.</div>

</body>
</html>