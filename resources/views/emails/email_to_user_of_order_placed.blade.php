<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
</head>
<body>
    <div>Hi {{ $name }},</div>
    <br>
    <div>Your order placed by product name {{(!empty($product_name)) ? "$product_name" : null}} (order number : {{$order_number}}) (Qty : {{ $qty }}) (Total Amount : {{ $amount }}) (Merchant : {{ $merchant_name }}).</div>
    <br>
    <div>Regards</div>
    <div>Beam Team</div>
</body>
</html>
