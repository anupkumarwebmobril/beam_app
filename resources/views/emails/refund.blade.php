
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
</head>
<body>
    <div>Dear {{ $name }},</div>
    <br>
    <br>
    <div>Please find the details of the order below:</div>
    <br>
    <div>Order Number: {{ $order_number }}</div>
    <br>
    <div>Product Name(s): {{ $product_name }}</div>
    <br>
    <div>Quantity: {{ $quantity }}</div>
    <br>
    <div>Total Amount: {{ $total_amount }}</div>
    <br>
    <br>
    <p>{!!$body!!}</p>
</body>
</html>
