
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">	
</head>
<body>
    <div>Hi {{ $name }},</div>
    <br>
    <div>New order placed by product name {{(!empty($product_name)) ? "$product_name" : null}} (order number : {{$order_number}}) from {{$from_user}}.</div>
    <br>
    <div>Regards</div>
    <div>Beam Team</div>
</body>
</html>