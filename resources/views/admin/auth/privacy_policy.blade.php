<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{env('APP_NAME', 'beam_app')}}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('public/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{asset('public/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('public/dist/css/adminlte.min.css')}}">
    <link rel="icon" href="{{asset('public/admin.png')}}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition login-page" >
<!-- style="background-image: url('public/images/banner-image.jpg');background-repeat: no-repeat;background-size:cover;"> -->
<div class="login-box" style="width:80%">
    <!-- <div class="logo">
        <img src="{{asset('public/images/app_logo.png')}}" style="width:120px;">
    </div> -->
    <div class="login-logo">
        <a href="#">{{env('APP_NAME', 'BeamApp')}}</a>
    </div>
    <!-- /.login-logo -->

    <div>
        {!! $privacy_policy->privacy_policy !!}
        
    </div>
    
</div>
<!-- /.login-box -->
@include('admin.partials.footer_js')

</body>
</html>
