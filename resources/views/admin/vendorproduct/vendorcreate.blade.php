@extends('admin.index')
<meta name="csrf-token" content="{{ csrf_token() }}" />
@section('content')

<!-- <div class="wrapper"> -->

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('Admin.Dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Add Product</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
				<h6 id="ajax_response" style="color:red; display:none;"><span id="msg_span" class="success alert-success" style="color: green;"></span> </h2>
                <div class="col-1"></div>
                <div class="col-10">

                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Add Product</strong></h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        @include('admin.partials.messages')
                       <form id="form" class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
                            @csrf
                            <div class="card-body">


								<div class="form-group row">
									<label for="Status" class="col-sm-2 col-form-label">Product Type</label>
									<div class="col-sm-10">
										<select class="form-control select2" style="width: 100%;" name="type"
											id="type" required="true">
											{{-- <option value="">Select product type</option> --}}
											{{-- <option value="1" >For Postal
											</option> --}}
											<option value="2" >For Gift Hub
											</option>

										</select>
									</div>
								</div>


								<div class="form-group row">
									<label for="Status" class="col-sm-2 col-form-label">Category</label>
									<div class="col-sm-10">
										<select class="form-control select2" style="width: 100%;" id="occasion_id"
											name="occasion_id" required="true">
											<option value="">Select category</option>
											@foreach ($occasions as $occasion)
												<option value="{{ $occasion->id }}">
													{{ $occasion->name }}</option>
											@endforeach
										</select>
									</div>
								</div>


								<div class="form-group row">
									<label for="Status" class="col-sm-2 col-form-label">Sub Category</label>
									<div class="col-sm-10">
										<select class="form-control select2" style="width: 100%;" id="category_id"
											name="category_id" required="true">
											<option value="">Select Sub Category</option>
											@foreach ($category as $cat)
												<option value="{{ $cat->id }}"
													>
													{{ $cat->name }}</option>
											@endforeach
										</select>
									</div>
								</div>

								<div class="form-group row">
									<label for="Name" class="col-sm-2 col-form-label">Product Name</label>
									<div class="col-sm-10">
										<input type="text" class="form-control name" id="name" name="name"
											value="" placeholder="Name" maxlength="40" required="true"/>
									</div>
								</div>

								<div class="form-group row">
									<label for="Email" class="col-sm-2 col-form-label">Price</label>
									<div class="col-sm-10">
										<input id="price" name="price" type="number" class="form-control" required="true" value="{{ !empty($product->price) ? $product->price : ''}}" step=".01">
									</div>
								</div>

								<div class="form-group row">
									<label for="Mobile" class="col-sm-2 col-form-label">Product Images1</label>
									<div class="col-sm-10">
										<input type="file" class="form-control customFileUplIpt" id="image" name="image" accept="image/png, image/jpg, image/jpeg">
											<img src="<?php echo !empty($product->image) ? asset("storage/product/".$product->image) : '' ?>"  style="max-height:300px; max-width:300px;"/>
									</div>
								</div>
								<div class="form-group row">
									<label for="Mobile" class="col-sm-2 col-form-label">Product Images2</label>
									<div class="col-sm-10">
										<input type="file" class="form-control customFileUplIpt" id="image" name="image_2" accept="image/png, image/jpg, image/jpeg">
											<img src="<?php echo !empty($product->image) ? asset("storage/product/".$product->image) : '' ?>"  style="max-height:300px; max-width:300px;"/>
									</div>
								</div><div class="form-group row">
									<label for="Mobile" class="col-sm-2 col-form-label">Product Images3</label>
									<div class="col-sm-10">
										<input type="file" class="form-control customFileUplIpt" id="image" name="image_3" accept="image/png, image/jpg, image/jpeg">
											<img src="<?php echo !empty($product->image) ? asset("storage/product/".$product->image) : '' ?>"  style="max-height:300px; max-width:300px;"/>
									</div>
								</div><div class="form-group row">
									<label for="Mobile" class="col-sm-2 col-form-label">Product Images4</label>
									<div class="col-sm-10">
										<input type="file" class="form-control customFileUplIpt" id="image" name="image_4" accept="image/png, image/jpg, image/jpeg">
											<img src="<?php echo !empty($product->image) ? asset("storage/product/".$product->image) : '' ?>"  style="max-height:300px; max-width:300px;"/>
									</div>
								</div>


								<div class="form-group row">
									<label for="Mobile" class="col-sm-2 col-form-label">Product Description</label>
									<div class="col-sm-10">
										<textarea id="description" name="description" style="height: 125px; width: 100%; padding: 10px"></textarea>
									</div>
								</div>
                                <input type="hidden" name="status" value="0">

								{{-- <div class="form-group row">
									<label for="Status" class="col-sm-2 col-form-label">Status</label>
									<div class="col-sm-10">
										<select class="form-control select2" style="width: 100%;" name="status"
											id="status" required>
											<option value="1" >Active
											</option>
											<option value="0" >Inactive
											</option>

										</select>
									</div>
								</div> --}}

                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-info">Add Product</button>
                                <a href="{{url('vendor/product')}}" class="btn btn-default float-right">Cancel</a>
                            </div>
                            <!-- /.card-footer -->
                        </form>
                    </div>

                </div>
                <div class="col-1"></div>
            </div>
        </div>
    </section>

</div>

<script>
	$(document).ready(function() {
		$('body').on('change', '#occasion_id', function() {
			var id = $(this).val();
			idBasedData(id);
		});

		$('body').on('change', '#type', function() {
			var id = $(this).val();
			typeBasedOccasion(id);
		});

		$("#form").submit(function(e) {
			e.preventDefault(); // avoid to execute the actual submit of the form.

			var formData = new FormData(this);

			$.ajax({

				type: 'POST',
				url: "{{ url('vendor/productadd') }}",
				data: formData,
				cache: false,
				contentType: false,
				processData: false,
				success: (data) => {
					if(data.status === false){
						$('#ajax_response').css('display', 'block');
						$('#msg_span').html(data.msg);
					} else {
						$('#ajax_response').css('color', 'green');
						$('#ajax_response').css('display', 'block');
						$('#msg_span').text(data.msg);
						$('#app').scrollTop()
						$('#ajax_response').fadeIn().delay(6000).fadeOut();
						$("html, body").animate({ scrollTop: 0 }, "slow");
						window.location.href = data.redirect;
					}
				},
				error: function (data) {
					// console.log(data);
				}
			});
		});
	});


	function idBasedData(id){
		var APP_URL = {!! json_encode(url('/')) !!};

		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: APP_URL+'/vendor/product/CategoryList/'+id,
			//url: APP_URL+product.categoryList+id,

			type: "POST",
			data: {
				"id": id,
				"_method":'POST',
			},
			success: function (response) {
				orderData = JSON.parse(response);
				//Render order listing
				renderOrderOption(orderData);
			},
			error: function(data) {
				//console.log("IN ERRROR "+data);
			}
		});
	}

	function renderOrderOption(data) {
		var html = '<option value="">Select category</option>';

		$(data).each(function(key, val){

			html += '<option value="'+val.id+'">'+val.name+'</option>';

		});
		$('#category_id').html(html);
	}

	function typeBasedOccasion(id){
		var APP_URL = {!! json_encode(url('/')) !!};

		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: APP_URL+'/vendor/product/occasionListBasedTypeId/'+id,
			//url: APP_URL+product.categoryList+id,

			type: "POST",
			data: {
				"id": id,
				"_method":'POST',
			},
			success: function (response) {
				orderData = JSON.parse(response);
				//Render order listing
				renderOccasion(orderData);
			},
			error: function(data) {
				//console.log("IN ERRROR "+data);
			}
		});
	}

	function renderOccasion(data) {
		var html = '<option value="">Select Occasion</option>';

		$(data).each(function(key, val){

			html += '<option value="'+val.id+'">'+val.name+'</option>';

		});
		$('#occasion_id').html(html);
	}

</script>

<!-- </div> -->
@endsection
