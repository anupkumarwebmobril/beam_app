@extends('admin.index')

@section('content')

<div class="wrapper">

    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        {{--<h1>DataTables</h1>--}}
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('Admin.Dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item active">Profile</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        {{-- @dd(3534) --}}
        <section class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">

                        <div class="card card-widget widget-user">
                            <div class="widget-user-header bg-info">
                                <h3 class="widget-user-username">{{ ucfirst(auth()->user()->name)}}</h3>
                                <h5 class="widget-user-desc">{{auth()->user()->type == 1 ? 'Admin' : ""}}</h5>
                            </div>
                            <div class="widget-user-image">
                                <img class="img-circle elevation-2" style="height: 88px;" src="<?php echo !empty(auth()->user()->profile_image) ? asset("storage/profile_pictures/".auth()->user()->profile_image) :  asset("storage/profile_pictures/user.png") ?>" alt="User Avatar">
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-sm-6 border-right">
                                        <div class="description-block">
                                            <h5 class="description-header">Email</h5>
                                            <span class="description">{{auth()->user()->email}}</span>
                                        </div>
                                        <!-- /.description-block -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-sm-6 border-right">
                                        <div class="description-block">
                                            <h5 class="description-header">Status</h5>
                                            <span class="description">{{(auth()->user()->status == 1) ? 'Active' : 'Inactive'}}</span>
                                        </div>
                                        <!-- /.description-block -->
                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>
                        </div>
                        <div class="edit-profile" style="float:right;position: absolute;top: 0;right: 1%;">
                            <a href="{{route('Admin.Get.Profile')}}"><i class="fa fa-pencil"></i></a>
                        </div>
                        <!-- /.widget-user -->
                    </div>
                    <div class="col-md-2"></div>
                </div>

            </div>
        </section>
    </div>
</div>

<style>
    .fa-pencil{

        display: block;
        padding: 5px;
        color:white;

    }
    .fa-pencil:hover{
        color: #FFB6C1;
        transition: color 2000ms linear;
    }
</style>
@endsection

