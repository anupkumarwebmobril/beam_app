@extends('admin.index')
<meta name="csrf-token" content="{{ csrf_token() }}" />

<link href="https://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css" rel="stylesheet">

<style>
    .multiselect-container {
    height: 260px;
    overflow: scroll;
}
</style>
@section('content')
    <!-- <div class="wrapper"> -->

    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <!--  -->
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('Admin.Dashboard') }}">Home</a></li>
                            <li class="breadcrumb-item active">Update Promo Code (Standard, Registered, Postcard)</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-1"></div>
                    <div class="col-10">

                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">Update <strong>{{ $data->name }}</strong></h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            @include('admin.partials.messages')
                           	<form class="form-horizontal" method="post" action="{{route('Admin.promocodestandregis.update',['promocode'=>$data->id])}}" enctype="multipart/form-data" autocomplete="off">
                                @csrf
								@method('PUT')
                                <input type="hidden" name="id" value="{{ $data->id }}" />
                                <div class="card-body">

                                    <div class="form-group row">
                                        <label for="Name" class="col-sm-2 col-form-label">Promo Code</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control name" id="promocode" name="promocode"
                                                value="{{ $data->promocode }}" placeholder="Promo code" maxlength="40" required="true" required/>
                                        </div>
                                    </div>
    
                                    <div class="form-group row">
                                        <label for="Status" class="col-sm-2 col-form-label">Discount Type</label>
                                        <div class="col-sm-10">
                                            <select class="form-control select2" style="width: 100%;" name="discount_type"
                                                id="discount_type" required="true">
                                                <option value="">Select Discount Type</option>
                                                <option value="fixed_amount" {{ $data->discount_type == "fixed_amount" ? 'selected' : '' }}>Fixed Amount
                                                </option>
                                                <option value="percentage" {{ $data->discount_type == "percentage" ? 'selected' : '' }}>Percentage
                                                </option>
                                                
                                            </select>
                                        </div>
                                    </div>
    
                                    <div class="form-group row">
                                        <label for="Name" class="col-sm-2 col-form-label">Discount Value</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control name" id="discount_value" name="discount_value"
                                                value="{{ $data->discount_value }}" placeholder="Discount Value" maxlength="40" required="true" required/>
                                        </div>
                                    </div>
    
                                    <div class="form-group row">
                                        <label for="Name" class="col-sm-2 col-form-label">Expiration Date</label>
                                        <div class="col-sm-10">
                                            <input type="date" class="form-control" name="expiration_date" value="{{ $data->expiration_date }}" min="<?=date('Y-m-d')?>" pattern="yyyy-mm-dd">
    
                                        </div>
                                    </div>
    
                                    <div class="form-group row">
                                        <label for="Name" class="col-sm-2 col-form-label">Usage Limits</label>
                                        <div class="col-sm-10">
                                            <input type="number" class="form-control name" id="usage_limits" name="usage_limits"
                                                value="{{ $data->usage_limits }}" placeholder="Usage Limits" maxlength="40" />
                                        </div>
                                    </div>

									<div class="form-group row">
										<label for="Status" class="col-sm-2 col-form-label">Category Type</label>
										<div class="col-sm-10">
											<select class="form-control select2" style="width: 100%;" name="type"
												id="type" required="true">
												<option value="">Select Category type</option>
												<option value="1" {{ !empty($data->type) && $data->type == 1 ? 'selected="selected"' : '' }}>Standard Mail
												</option>
												<option value="2" {{ !empty($data->type) && $data->type == 2 ? 'selected="selected"' : '' }}>Registered Mail
												</option>
												<option value="3" {{ !empty($data->type) && $data->type == 3 ? 'selected="selected"' : '' }}>Postcard
												</option>
											</select>
										</div>
									</div>

                                    <div class="form-group row" style="display:<?php echo $data->type != 3 ? 'none' : ''?>">
                                        <label for="Status" class="col-sm-2 col-form-label">Postcard</label>
                                        <div class="col-sm-10">
                                            <select class="occasion form-control" style="width: 100%;" id="postcard_id"
                                                name="postcard_id[]" multiple="multiple">
                                                <?php 
                                                    $catIds = explode(",", $data->product_id);
                                                ?>
                                                {{-- <option value="">Select Category</option> --}}
                                                @foreach ($postcards as $postcard)
                                                    <option value="{{ $postcard->id }}"
                                                        {{ !empty($data->product_id) && in_array($postcard->id, $catIds)  ? 'selected="selected"' : '' }}>
                                                        {{ $postcard->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    

                                    <div class="form-group row">
                                        <label for="Status" class="col-sm-2 col-form-label">User Restrictions</label>
                                        <div class="col-sm-10">
                                            <select class="form-control select2" style="width: 100%;" name="user_restrictions"
                                                id="user_restrictions" >
                                                <option value="">Select User Restrictions</option>
                                                <option value="for_old_user" {{ $data->user_restrictions == "for_old_user" ? 'selected' : '' }}>For Old User
                                                </option>
                                                <option value="for_new_user" {{ $data->user_restrictions == "for_new_user" ? 'selected' : '' }}>For New User
                                                </option>
                                                
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="Status" class="col-sm-2 col-form-label">Status</label>
                                        <div class="col-sm-10">
                                            <select class="form-control select2" style="width: 100%;" name="status"
                                                id="status" required>
                                                <option value="0" {{ $data->status == 0 ? 'selected' : '' }}>Inactive
                                                </option>
                                                <option value="1" {{ $data->status == 1 ? 'selected' : '' }}>Active
                                                </option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-info">Update Promo Code</button>
                                    <a href="{{ route('Admin.promocode.index') }}"
                                        class="btn btn-default float-right">Cancel</a>
                                </div>
                                <!-- /.card-footer -->
                            </form>
                        </div>

                    </div>
                    <div class="col-1"></div>
                </div>
            </div>
        </section>

    </div>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

    <script>
        $(document).ready(function() {
            $('.product').multiselect({
            	includeSelectAllOption: true,
            	enableFiltering: true, // Enable the search feature
            });

            $('.occasion').multiselect({
            	includeSelectAllOption: true,
            	enableFiltering: true, // Enable the search feature
            });

            $('body').on('change', '#occasion_id', function() {
                var id = $(this).val();
                var selectedLeng = $("select[name='occasion_id[]'] option:selected").length;
                if(selectedLeng == 0){
                    id = 0;
                }
                idBasedData(id);
            });

            $('body').on('change', '#type', function() {
                var id = $(this).val();
                typeBasedOccasion(id);
            });


        });


        function idBasedData(id){
            var APP_URL = {!! json_encode(url('/')) !!};

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: APP_URL+'/admin/product/multiCategoryList/'+id,
                //url: APP_URL+product.categoryList+id,

                type: "POST",
                data: {
                    "id": id,
                    "_method":'POST',
                },
                success: function (response) {
                    orderData = JSON.parse(response);
                    //Render order listing
                    renderOrderOption(orderData);
                },
                error: function(data) {
                    //console.log("IN ERRROR "+data);
                }
            });
        }

        function renderOrderOption(data) {
            //var html = '<option value="">Select category</option>';
            var html = null;
            $('.product').multiselect('destroy');

            $(data).each(function(key, val){

                html += '<option value="'+val.id+'">'+val.name+'</option>';

            });
            $('#category_id').html(html);
            //$('#category_id').append(html);

            setTimeout(() => {
                $('.product').multiselect({
					includeSelectAllOption: true,
					enableFiltering: true, // Enable the search feature
				});
                //$('#category_id').multiselect('refresh');
            }, 1000);

        }

        function typeBasedOccasion(id){
            var APP_URL = {!! json_encode(url('/')) !!};

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: APP_URL+'/admin/product/occasionListBasedTypeId/'+id,
                //url: APP_URL+product.categoryList+id,

                type: "POST",
                data: {
                    "id": id,
                    "_method":'POST',
                },
                success: function (response) {
                    orderData = JSON.parse(response);
                    //Render order listing
                    renderOccasion(orderData);
                },
                error: function(data) {
                    //console.log("IN ERRROR "+data);
                }
            });
        }

        function renderOccasion(data) {
            //var html = '<option value="">Select Occasion</option>';
            var html = null;

            $(data).each(function(key, val){

                html += '<option value="'+val.id+'">'+val.name+'</option>';

            });
            $('#occasion_id').html(html);
            setTimeout(() => {
				$('.occasion').multiselect({
					includeSelectAllOption: true,
					enableFiltering: true, // Enable the search feature
				});
			}, 1000);
        }

    </script>

    <!-- </div> -->
@endsection
