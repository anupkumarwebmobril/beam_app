@extends('admin.index')
<meta name="csrf-token" content="{{ csrf_token() }}" />
@section('content')

<!-- <div class="wrapper"> -->
<link href="https://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css" rel="stylesheet">

<style>
    .multiselect-container {
    height: 260px;
    overflow: scroll;
}
</style>

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('Admin.Dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Promo Code</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
				<h6 id="ajax_response" style="color:red; display:none; margin-left:10px; font-size:20px;">
					<span id="msg_span" class="success alert-success" style="color: green;"></span>
				 </h2>
                <div class="col-1"></div>
                <div class="col-10">

                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Promo Code (Standard, Registered, Postcard)</strong></h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        @include('admin.partials.messages')
                       <form id="form" class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
                            @csrf
                            <div class="card-body">

								<div class="form-group row">
									<label for="Name" class="col-sm-2 col-form-label">Promo Code</label>
									<div class="col-sm-10">
										<input type="text" class="form-control name" id="promocode" name="promocode"
											value="" placeholder="Promo code" maxlength="40" required="true" required/>
									</div>
								</div>

								<div class="form-group row">
									<label for="Status" class="col-sm-2 col-form-label">Discount Type</label>
									<div class="col-sm-10">
										<select class="form-control select2" style="width: 100%;" name="discount_type"
											id="discount_type" required="true">
											<option value="">Select Discount Type</option>
											<option value="fixed_amount" >Fixed Amount
											</option>
											<option value="percentage" >Percentage
											</option>
											
										</select>
									</div>
								</div>

								<div class="form-group row">
									<label for="Name" class="col-sm-2 col-form-label">Discount Value</label>
									<div class="col-sm-10">
										<input type="text" class="form-control name" id="discount_value" name="discount_value"
											value="" placeholder="Discount Value" maxlength="40" required="true" required/>
									</div>
								</div>

								<div class="form-group row">
									<label for="Name" class="col-sm-2 col-form-label">Expiration Date</label>
									<div class="col-sm-10">
										<input type="date" class="form-control" name="expiration_date" value="{{old('event_date') }}" min="<?=date('Y-m-d')?>" pattern="yyyy-mm-dd">

									</div>
								</div>

								<div class="form-group row">
									<label for="Name" class="col-sm-2 col-form-label">Usage Limits</label>
									<div class="col-sm-10">
										<input type="number" class="form-control name" id="usage_limits" name="usage_limits"
											value="" placeholder="Usage Limits" maxlength="40" />
									</div>
								</div>	 
				 

								<div class="form-group row">
									<label for="Status" class="col-sm-2 col-form-label">Category Type</label>
									<div class="col-sm-10">
										
										<select class="form-control select2" style="width: 100%;" name="type"
											id="type" required="true">
										{{-- <select class="form-control" multiple="multiple" name="territory_name[]" > --}}

											<option value="">Select Category type</option>
											<option value="1" >Standard Mail
											</option>
											<option value="2" >Registered Mail
											</option>
											<option value="3" >Postcard
											</option>
										</select>
									</div>
								</div>

								<div class="form-group row" id="postcard_row" style="display:none">
									<label for="Status" class="col-sm-2 col-form-label">Postcard</label>
									<div class="col-sm-10">
										<select class="occasion form-control postcard_id" style="width: 100%;" id="postcard_id"
											name="postcard_id[]" multiple="multiple">
											@foreach ($postcards as $postcard)
												<option value="{{ $postcard->id }}"
													{{ !empty($data->product_id) && $data->product_id === $postcard->id ? 'selected="selected"' : '' }}>
													{{ $postcard->name }}</option>
											@endforeach
										</select>
									</div>
								</div>

								

								<div class="form-group row">
									<label for="Status" class="col-sm-2 col-form-label">User Restrictions</label>
									<div class="col-sm-10">
										<select class="form-control select2" style="width: 100%;" name="user_restrictions"
											id="user_restrictions" >
											<option value="">Select User Restrictions</option>
											<option value="for_old_user" >For Old User
											</option>
											<option value="for_new_user" >For New User
											</option>
											
										</select>
									</div>
								</div>

								<div class="form-group row">
									<label for="Status" class="col-sm-2 col-form-label">Status</label>
									<div class="col-sm-10">
										<select class="form-control select2" style="width: 100%;" name="status"
											id="status" required>
											<option value="1" >Active
											</option>
											<option value="0" >Inactive
											</option>

										</select>
									</div>
								</div>

                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-info">Add Promo Code</button>
                                <a href="{{route('Admin.subcategory.index')}}" class="btn btn-default float-right">Cancel</a>
                            </div>
                            <!-- /.card-footer -->
                        </form>
                    </div>

                </div>
                <div class="col-1"></div>
            </div>
        </div>
    </section>

</div>

<script>
	$(document).ready(function() {
		setTimeout(() => {
			$('.postcard_id').multiselect({
				includeSelectAllOption: true,
				enableFiltering: true, // Enable the search feature
			});
		}, 1000);

		$('body').on('change', '#occasion_id', function() {
			 var id = $(this).val();			 
			// var selectedLeng = $("select[name='occasion_id[]'] option:selected").length;
			// if(selectedLeng == 0){
			// 	id = 0;
			// }
			// idBasedData(id);
			
		});

		$('body').on('change', '#type', function() {
			var id = $(this).val();
			 if(id == 3){
				$('#postcard_row').css('display', '');
			 } else {
				$('#postcard_row').css('display', 'none');
			 }
			//typeBasedOccasion(id);
		});

		$("#form").submit(function(e) {
			e.preventDefault(); // avoid to execute the actual submit of the form.

			var formData = new FormData(this);

			$.ajax({
				type: 'POST',
				url: "{{ url('admin/promocodestandregis') }}",
				data: formData,
				cache: false,
				contentType: false,
				processData: false,
				success: (data) => {
					if(data.status === false){
						$('#ajax_response').css('display', 'block');
						$('#msg_span').css('display', 'block');						
						$('#msg_span').html(data.msg);
					} else {
						$('#ajax_response').css('color', 'green');
						$('#ajax_response').css('display', 'block');
						$('#msg_span').text(data.msg);
						$('#app').scrollTop()
						$('#ajax_response').fadeIn().delay(6000).fadeOut();
						$("html, body").animate({ scrollTop: 0 }, "slow");
						window.location.href = data.redirect;
					}
				},
				error: function (data) {
					// console.log(data);
				}
			});
		});
	});

	function idBasedData(id){
            var APP_URL = {!! json_encode(url('/')) !!};

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: APP_URL+'/admin/product/multiCategoryList/'+id,
                //url: APP_URL+product.categoryList+id,

                type: "POST",
                data: {
                    "id": id,
                    "_method":'POST',
                },
                success: function (response) {
                    orderData = JSON.parse(response);
                    //Render order listing
                    renderOrderOption(orderData);
                },
                error: function(data) {
                    //console.log("IN ERRROR "+data);
                }
            });
        }

        function renderOrderOption(data) {
			$('.product').multiselect('destroy');
			//$('#category_id').html("");
			//$(".product").multiselect("destroy").multiselect();

            //var html = '<option value="" id="">Select category</option>';
			var html = null;
            $(data).each(function(key, val){

                html += '<option value="'+val.id+'">'+val.name+'</option>';

            });
            $('#category_id').html(html);
			// $('#category_id').append(html);
			//console.log(html);
			setTimeout(() => {
				$('.product').multiselect({
					includeSelectAllOption: true,
					enableFiltering: true, // Enable the search feature
				});
				//$('#category_id').multiselect('refresh');
			}, 1000);
			
        }

        function typeBasedOccasion(id){
            var APP_URL = {!! json_encode(url('/')) !!};

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: APP_URL+'/admin/product/occasionListBasedTypeId/'+id,
                //url: APP_URL+product.categoryList+id,

                type: "POST",
                data: {
                    "id": id,
                    "_method":'POST',
                },
                success: function (response) {
                    orderData = JSON.parse(response);
                    //Render order listing
                    renderOccasion(orderData);
                },
                error: function(data) {
                    //console.log("IN ERRROR "+data);
                }
            });
        }

        function renderOccasion(data) {
            //var html = '<option value="">Select Occasion</option>';
			var html = null;
            $(data).each(function(key, val){

                html += '<option value="'+val.id+'">'+val.name+'</option>';

            });
            $('#occasion_id').html(html);
			setTimeout(() => {
				$('.occasion').multiselect({
					includeSelectAllOption: true,
					enableFiltering: true, // Enable the search feature
				});
			}, 1000);
        }


</script>

 
 {{--<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script> --}}
 {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> --}}
 

	{{-- <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {
			$('#example-getting-started').multiselect();
		});
	</script> --}}

<!-- </div> -->
@endsection
