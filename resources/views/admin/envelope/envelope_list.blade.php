@extends('admin.index')
@section('after-style')
<link rel="stylesheet" href="{{asset('public/admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('public/admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
@endsection
@section('content')
<div class="content-wrapper">
   <section class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="{{route('Admin.Dashboard')}}">Home</a></li>
                  <li class="breadcrumb-item active">Manage Envelope Color</li>
               </ol>
            </div>
         </div>
      </div>
   </section>
   <section class="content">
      <div class="container-fluid">
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-header">
                     <h3 class="card-title">Envelope Color List</h3>
                     <a href="{{route('Admin.AddEnvelopeForm')}}" style="float: right;"><i class="fas fa-plus-square" style="font-size:25px;"></i></a>
                  </div>
                  @if(session('message'))
                  <div class="alert alert-success" style="padding: 5px 20px;margin-bottom: 5px;">{{ session('message') }}</div>
                  @endif
                  <!-- /.card-header -->
                  <div class="card-body">
                     <table id="category_list" class="table table-bordered table-striped" data-order='[[ 0, "asc" ]]'>
                        <thead>
                           <tr>
                              <th>Id</th>
                              <th>Name</th>
                              <th>Price</th>
                              <th>Status</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                           @if(isset($envelope) && $envelope != null)
                           @foreach($envelope as $envelope)
                           <tr>
                              <td>{{$envelope->id}}</td>
                              <td>{{$envelope->name}}</td>
                              <td><p class="text-success">RM:{{$envelope->price}}</p></td>
                           <td style="text-aliexcercisesgn:center">
                           @if($envelope->status == 1)
                           <p style="color:green">Active</p>
                           @elseif($envelope->status == 0)
                           <p style="color:red">In-active</p>
                           @endif
                           </td>
                              <td style="text-align: center;">
                                 <a href="{{route('Admin.EditEnvelope',[$envelope->id])}}"><i class="fas fa-edit"></i>&nbsp;&nbsp;</a>
                                 <a href="{{route('Admin.DeleteEnvelope',[$envelope->id])}}"onclick="return confirm('Are you sure?')"><i class="fas fa-trash"></i>&nbsp;&nbsp;</a>
                              </td>
                           </tr>
                           @endforeach
                           @endif
                        </tbody>
                     </table>
                  </div>
                  <!-- /.card-body -->
               </div>
               <!-- /.card -->
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
@endsection
@section('after-scripts')
<script src="{{asset('public/admin/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/admin/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('public/admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script>
   $(function() {
       $("#category_list").DataTable({
           "responsive": true,
           "autoWidth": false,
       });
   });
</script>
@endsection
