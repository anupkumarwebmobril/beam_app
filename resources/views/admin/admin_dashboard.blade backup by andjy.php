@extends('admin.index')
<meta name="csrf-token" content="{{ csrf_token() }}" />
@section('after-style')
<style>

    a{

        color:inherit;
    }

    table, th, td {
        border: 1px solid;
    }
</style>

@endsection

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">{{(auth()->user()->user_type == 1) ? "Admin" : "Vendor" }} Dashboard</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">{{(auth()->user()->user_type == 1) ? "Admin" : "Vendor" }} Dashboard</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Info boxes -->
            <div class="row">
                @if (Auth::user()->user_type == 1)
                    <div class="col-12 col-sm-6 col-md-3">
                        {{-- <a href="{{route('Admin.Users.List')}}"> --}}
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Postal Mails</span>
                                    <span class="info-box-number">Today's Orders : {{$postal_mails ?? 00}}</span>
                                    <span class="info-box-number" id="postal_mails_amount_today"></span>

                                </div>
                                <!-- /.info-box-content -->
                            </div>
                        {{-- </a> --}}
                        <!-- /.info-box -->
                    </div>

                    <div class="col-12 col-sm-6 col-md-3">
                        {{-- <a href="{{route('Admin.Users.List')}}"> --}}
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Greetings</span>
                                    <span class="info-box-number">Today's Orders : {{$greetings ?? 00}}</span>
                                    <span class="info-box-number" id="greetings_amount_today"></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                        {{-- </a> --}}
                        <!-- /.info-box -->
                    </div>

                    <div class="col-12 col-sm-6 col-md-3">
                        {{-- <a href="{{route('Admin.Users.List')}}"> --}}
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Post Card</span>
                                    <span class="info-box-number">Today's Orders : {{$postcard ?? 00}}</span>
                                    <span class="info-box-number" id="postcard_amount_today"></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                        {{-- </a> --}}
                        <!-- /.info-box -->
                    </div>

                    <div class="col-12 col-sm-6 col-md-3">
                        {{-- <a href="{{route('Admin.Users.List')}}"> --}}
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Gift</span>
                                    <span class="info-box-number">Today's Orders : {{$gifthub ?? 00}}</span>
                                    <span class="info-box-number" id="gifthub_amount_today"></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                        {{-- </a> --}}
                        <!-- /.info-box -->
                    </div>

                    <!--Weekly-->
                    <div class="col-12 col-sm-6 col-md-3">
                        {{-- <a href="{{route('Admin.Users.List')}}"> --}}
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Postal Mails</span>
                                    <span class="info-box-number" id="postal_mails_weekly"></span>
                                    <span class="info-box-number" id="postal_mails_weekly_amount"></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                        {{-- </a> --}}
                        <!-- /.info-box -->
                    </div>

                    <div class="col-12 col-sm-6 col-md-3">
                        {{-- <a href="{{route('Admin.Users.List')}}"> --}}
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Greetings</span>
                                    <span class="info-box-number" id="greetings_weekly"></span>
                                    <span class="info-box-number" id="greetings_weekly_amount"></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                        {{-- </a> --}}
                        <!-- /.info-box -->
                    </div>

                    <div class="col-12 col-sm-6 col-md-3">
                        {{-- <a href="{{route('Admin.Users.List')}}"> --}}
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Post Card</span>
                                    <span class="info-box-number" id="postcard_weekly"></span>
                                    <span class="info-box-number" id="postcard_weekly_amount"></span>


                                </div>
                                <!-- /.info-box-content -->
                            </div>
                        {{-- </a> --}}
                        <!-- /.info-box -->
                    </div>

                    <div class="col-12 col-sm-6 col-md-3">
                        {{-- <a href="{{route('Admin.Users.List')}}"> --}}
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Gift</span>
                                    <span class="info-box-number" id="gifthub_weekly"></span>
                                    <span class="info-box-number" id="gifthub_weekly_amount"></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                        {{-- </a> --}}
                        <!-- /.info-box -->
                    </div>

                    <!--monthly-->
                    <div class="col-12 col-sm-6 col-md-3">
                        {{-- <a href="{{route('Admin.Users.List')}}"> --}}
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Postal Mails</span>
                                    <span class="info-box-number" id="postal_mails_monthly"></span>
                                    <span class="info-box-number" id="postal_mails_monthly_amount"></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                        {{-- </a> --}}
                        <!-- /.info-box -->
                    </div>

                    <div class="col-12 col-sm-6 col-md-3">
                        {{-- <a href="{{route('Admin.Users.List')}}"> --}}
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Greetings</span>
                                    <span class="info-box-number" id="greetings_monthly"></span>
                                    <span class="info-box-number" id="greetings_monthly_amount"></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                        {{-- </a> --}}
                        <!-- /.info-box -->
                    </div>

                    <div class="col-12 col-sm-6 col-md-3">
                        {{-- <a href="{{route('Admin.Users.List')}}"> --}}
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Post Card</span>
                                    <span class="info-box-number" id="postcard_monthly"></span>
                                    <span class="info-box-number" id="postcard_monthly_amount"></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                        {{-- </a> --}}
                        <!-- /.info-box -->
                    </div>

                    <div class="col-12 col-sm-6 col-md-3">
                        {{-- <a href="{{route('Admin.Users.List')}}"> --}}
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Gift</span>
                                    <span class="info-box-number" id="gifthub_monthly"></span>
                                    <span class="info-box-number" id="gifthub_monthly_amount"></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                        {{-- </a> --}}
                        <!-- /.info-box -->
                    </div>

                    <div class="col-12 col-sm-6 col-md-3">
                        {{-- <a href="{{route('Admin.Users.List')}}"> --}}
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Monthly Total Sales</span>
                                    <span class="info-box-number" id="monthly_total_amount"></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                        {{-- </a> --}}
                        <!-- /.info-box -->
                    </div>

                    <br>

                    <div class="col-12 col-sm-6 col-md-3">
                        {{-- <a href="{{route('Admin.Users.List')}}"> --}}
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Users Total</span>
                                    <span class="info-box-number">{{$countUsers ?? 00}}</span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                        {{-- </a> --}}
                        <!-- /.info-box -->
                    </div>

                    <div class="col-12 col-sm-6 col-md-3">
                        {{-- <a href="{{route('Admin.Vendors.List')}}"> --}}
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Vendors Total</span>
                                    <span class="info-box-number">{{$countVendors ?? 0}}</span>
                                </div>

                            </div>
                        {{-- </a> --}}

                    </div>

                    <div class="col-12 col-sm-6 col-md-3">
                        {{-- <a href="{{route('Admin.Vendors.List')}}"> --}}
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Designers Total</span>
                                    <span class="info-box-number">{{$designer ?? 00}}</span>
                                </div>

                            </div>
                        {{-- </a> --}}

                    </div>

                    {{-- <div class="col-12 col-sm-6 col-md-3">
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Categories Total</span>
                                    <span class="info-box-number">{{$category ?? 00}}</span>
                                </div>

                            </div>

                    </div> --}}

                    <div class="col-12 col-sm-6 col-md-3">
                        {{-- <a href="{{route('Admin.category.index')}}"> --}}
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Design Request</span>
                                    <span class="info-box-number" id="design_request">0</span>
                                </div>

                            </div>
                        {{-- </a> --}}

                    </div>

                    <div class="col-12 col-sm-6 col-md-3">
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user"></i></span>

                                <div class="info-box-content" style="max-width:100%">
                                    <span class="info-box-text">Total Orders Shipped</span>
                                    <span class="info-box-text">and Awaiting Delivery</span>
                                    <span class="info-box-number" id="totalORderShippedAndAwaitingForDelivery">0</span>
                                </div>

                            </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box">
                            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Total Orders Delivered</span>
                                <span class="info-box-number" id="totalORderDelivered">0</span>
                            </div>

                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box">
                            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Total Pending Orders</span>
                                <span class="info-box-number" id="totalPedningOrder">0</span>
                            </div>

                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box">
                            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Return / Refund request</span>
                                <span class="info-box-number" id="totalReturnRefund">0</span>
                            </div>

                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box">
                            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Total Cancel Orders</span>
                                <span class="info-box-number" id="totalCancelOrder">0</span>
                            </div>

                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box">
                            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Total Available</span>
                                <span class="info-box-text">Products Postal</span>
                                <span class="info-box-number" id="postalProductCount">0</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box">
                            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Total Available </span>
                                <span class="info-box-text">Products Postcard</span>
                                <span class="info-box-number" id="postcardProductCount">0</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box">
                            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Total Available</span>
                                <span class="info-box-text">Products Gifts</span>
                                <span class="info-box-number" id="gifthubProductCount">0</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box">
                            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Active Products</span>
                                <span class="info-box-text">Postal</span>
                                <span class="info-box-number" id="postalProductActiveCount">0</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box">
                            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">In-Active Products Postal</span>
                                <span class="info-box-number" id="postalProductInActiveCount">0</span>
                            </div>
                        </div>
                    </div>



                @endif

            </div>
            {{--
             <div class="row">
                <div class="col-12"><b>Top 10 selling products Greeting</b> </div>
                <div class="col-12">
                    <table>
                        <?php foreach($postalTopTenSellingProducts as $postalPro){ ?>
                            <tr>
                                <td><?php echo $postalPro[0] ? $postalPro[0] : null ?></td>
                            </tr>
                        <?php } ?>

                    </table>
                </div>
            </div>
            
            <br>
            <div class="row">
                <div class="col-12"><b>Top 10 Selling Products Postcard</b> </div>
                <div class="col-12">
                    <table>
                        <?php //foreach($postcardTopTenSellingProducts as $postcardPro){ ?>
                            <tr>
                                <td><?php // echo $postcardPro[0] ? $postcardPro[0] : null ?></td>
                            </tr>
                        <?php //} ?>

                    </table>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-12"><b>Top 10 Selling Products Gifthub</b> </div>
                <div class="col-12">
                    <table>
                        <?php //foreach($gifthubTopTenSellingProducts as $giftPro){ ?>
                            <tr>
                                <td><?php // echo $giftPro[0] ? $giftPro[0] : null ?></td>
                            </tr>
                        <?php //} ?>

                    </table>
                </div>
            </div> --}}
            <div class="row">
                <div class="col-sm">
                    <div id="piechart" style="width: 100%; height: 500px;"></div>
                </div>
                <div class="col-sm">
                    <div id="piechart1" style="width: 100%; height: 500px;"></div>
                </div>

                    <div class="col-sm">
                        <div id="gifthubTopTenSellingProducts" style="width: 100%; height: 500px;"></div>
                    </div>

            </div>
            <div class="row">
                <div class="col-sm">
                    {{-- <button id="change-chart">Change to Classic</button> --}}
                    <br><br>
                    <div id="chart_div" style="width: 1000px; height: 100%;"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm">
                    {{-- <button id="change-chart">Change to Classic</button> --}}
                    <br><br>
                    <div id="chart_div1" style="width: 1000px; height: 100%;"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm">
                    {{-- <button id="change-chart">Change to Classic</button> --}}
                    <br><br>
                    <div id="chart_div2" style="width: 1000px; height: 100%;"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm">
                    {{-- <button id="change-chart">Change to Classic</button> --}}
                    <br><br>
                    <div id="chart_div3" style="width: 1000px; height: 100%;"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm">
                    {{-- <button id="change-chart">Change to Classic</button> --}}
                    <br><br>
                    <div id="chart_div4" style="width: 1000px; height: 100%;"></div>
                </div>
            </div>



            <!-- /.row -->
        </div><!--/. container-fluid -->
    </section>

    <!-- /.content -->
</div>
@endsection

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script>
    $(document).ready(function() {
        weeklyOrderTotal();
        orderData();
        productDataForDashboard();
        monthyreporter();
        monthyreportergreeting();
        monthyreporterpostmail();
        monthyreporterpostcard();
        monthyreportdesigner();

    });
    // sagar code
    function monthyreporter() {
        var APP_URL = {!! json_encode(url('/')) !!};

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: APP_URL + '/admin/monthlyreport/',
            //url: APP_URL+product.categoryList+id,

            type: "POST",
            data: {
                "id": null,
                "_method": 'POST',
            },
            success: function(response) {
                if (response.status == true) {
                    // console.log(response.data.monthlyreport);
                    $('#chart_div').html(response.data.monthlyreport);


                    // sagar code for chart
                    google.charts.load('current', {
                        'packages': ['bar']
                    });
                    google.charts.setOnLoadCallback(drawChart);

                    function drawChart() {
                        // var data = google.visualization.arrayToDataTable([
                        //     ['Date', 'qty', 'sell'],
                        //     ['2022-12-1', 120, 400],
                        //     ['2022-12-2', 343, 460],
                        //     ['2022-12-3', 41, 1120],
                        //     ['2022-12-4', 67, 540]
                        // ]);
                        var data = google.visualization.arrayToDataTable(response.data.monthlyreport);


                        var options = {
                            chart: {
                                title: 'Monthly  Gifthub Report',
                                subtitle: '(Date, Qty, Sales)',
                            },
                            bars: 'vertical',
                            vAxis: {
                                format: 'decimal'
                            },

                            height: 400,
                            colors: ['#1b9e77', '#d95f02', '#7570b3']
                        };

                        var chart = new google.charts.Bar(document.getElementById('chart_div'));

                        chart.draw(data, google.charts.Bar.convertOptions(options));

                        var btns = document.getElementById('btn-group');

                        btns.onclick = function(e) {

                            if (e.target.tagName === 'BUTTON') {
                                options.vAxis.format = e.target.id === 'none' ? '' : e.target.id;
                                chart.draw(data, google.charts.Bar.convertOptions(options));
                            }
                        }
                    }
                }
            },
            error: function(data) {
                //console.log("IN ERRROR "+data);
            }
        });
    }
    function monthyreportergreeting() {
        var APP_URL = {!! json_encode(url('/')) !!};

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: APP_URL + '/admin/monthlyreport-greeting/',
            //url: APP_URL+product.categoryList+id,

            type: "POST",
            data: {
                "id": null,
                "_method": 'POST',
            },
            success: function(response) {
                if (response.status == true) {
                    // console.log(response.data.greetingreport);
                    $('#chart_div1').html(response.data.greetingreport);


                    // sagar code for chart
                    google.charts.load('current', {
                        'packages': ['bar']
                    });
                    google.charts.setOnLoadCallback(drawChart);

                    function drawChart() {
                        // var data = google.visualization.arrayToDataTable([
                        //     ['Date', 'qty', 'sell'],
                        //     ['2022-12-1', 120, 400],
                        //     ['2022-12-2', 343, 460],
                        //     ['2022-12-3', 41, 1120],
                        //     ['2022-12-4', 67, 540]
                        // ]);
                        var data = google.visualization.arrayToDataTable(response.data.greetingreport);


                        var options = {
                            chart: {
                                title: 'Monthly Greeting Report',
                                subtitle: '(Date, Qty, Sales)',
                            },
                            bars: 'vertical',
                            vAxis: {
                                format: 'decimal'
                            },

                            height: 400,
                            colors: ['#1b9e77', '#d95f02', '#7570b3']
                        };

                        var chart = new google.charts.Bar(document.getElementById('chart_div1'));

                        chart.draw(data, google.charts.Bar.convertOptions(options));

                        var btns = document.getElementById('btn-group');

                        btns.onclick = function(e) {

                            if (e.target.tagName === 'BUTTON') {
                                options.vAxis.format = e.target.id === 'none' ? '' : e.target.id;
                                chart.draw(data, google.charts.Bar.convertOptions(options));
                            }
                        }
                    }
                }
            },
            error: function(data) {
                //console.log("IN ERRROR "+data);
            }
        });
    }

    function monthyreporterpostmail() {
        var APP_URL = {!! json_encode(url('/')) !!};

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: APP_URL + '/admin/monthlyreport-postmail/',
            //url: APP_URL+product.categoryList+id,

            type: "POST",
            data: {
                "id": null,
                "_method": 'POST',
            },
            success: function(response) {
                if (response.status == true) {
                    // alert(3434);
                    console.log(response.data.greetingreport);
                    $('#chart_div2').html(response.data.postmailreport);


                    // sagar code for chart
                    google.charts.load('current', {
                        'packages': ['bar']
                    });
                    google.charts.setOnLoadCallback(drawChart);

                    function drawChart() {
                        // var data = google.visualization.arrayToDataTable([
                        //     ['Date', 'qty', 'sell'],
                        //     ['2022-12-1', 120, 400],
                        //     ['2022-12-2', 343, 460],
                        //     ['2022-12-3', 41, 1120],
                        //     ['2022-12-4', 67, 540]
                        // ]);
                        var data = google.visualization.arrayToDataTable(response.data.postmailreport);


                        var options = {
                            chart: {
                                title: 'Monthly Postmail Report',
                                subtitle: '(Date, Qty, Sales)',
                            },
                            bars: 'vertical',
                            vAxis: {
                                format: 'decimal'
                            },

                            height: 400,
                            colors: ['#1b9e77', '#d95f02', '#7570b3']
                        };

                        var chart = new google.charts.Bar(document.getElementById('chart_div2'));

                        chart.draw(data, google.charts.Bar.convertOptions(options));

                        var btns = document.getElementById('btn-group');

                        btns.onclick = function(e) {

                            if (e.target.tagName === 'BUTTON') {
                                options.vAxis.format = e.target.id === 'none' ? '' : e.target.id;
                                chart.draw(data, google.charts.Bar.convertOptions(options));
                            }
                        }
                    }
                }
            },
            error: function(data) {
                //console.log("IN ERRROR "+data);
            }
        });
    }
    function monthyreporterpostcard() {
        var APP_URL = {!! json_encode(url('/')) !!};

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: APP_URL + '/admin/monthlyreport-postcard/',
            //url: APP_URL+product.categoryList+id,

            type: "POST",
            data: {
                "id": null,
                "_method": 'POST',
            },
            success: function(response) {
                if (response.status == true) {
                    // alert(3434);
                    // console.log(response.data.greetingreport);
                    $('#chart_div3').html(response.data.postcardreport);


                    // sagar code for chart
                    google.charts.load('current', {
                        'packages': ['bar']
                    });
                    google.charts.setOnLoadCallback(drawChart);

                    function drawChart() {
                        // var data = google.visualization.arrayToDataTable([
                        //     ['Date', 'qty', 'sell'],
                        //     ['2022-12-1', 120, 400],
                        //     ['2022-12-2', 343, 460],
                        //     ['2022-12-3', 41, 1120],
                        //     ['2022-12-4', 67, 540]
                        // ]);
                        var data = google.visualization.arrayToDataTable(response.data.postcardreport);


                        var options = {
                            chart: {
                                title: 'Monthly Postcard Report',
                                subtitle: '(Date, Qty, Sales)',
                            },
                            bars: 'vertical',
                            vAxis: {
                                format: 'decimal'
                            },

                            height: 400,
                            colors: ['#1b9e77', '#d95f02', '#7570b3']
                        };

                        var chart = new google.charts.Bar(document.getElementById('chart_div3'));

                        chart.draw(data, google.charts.Bar.convertOptions(options));

                        var btns = document.getElementById('btn-group');

                        btns.onclick = function(e) {

                            if (e.target.tagName === 'BUTTON') {
                                options.vAxis.format = e.target.id === 'none' ? '' : e.target.id;
                                chart.draw(data, google.charts.Bar.convertOptions(options));
                            }
                        }
                    }
                }
            },
            error: function(data) {
                //console.log("IN ERRROR "+data);
            }
        });
    }

    function monthyreportdesigner() {
        var APP_URL = {!! json_encode(url('/')) !!};

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: APP_URL + '/admin/designer-request-report/',
            //url: APP_URL+product.categoryList+id,

            type: "POST",
            data: {
                "id": null,
                "_method": 'POST',
            },
            success: function(response) {
                if (response.status == true) {
                    // alert(3434);
                    // console.log(response.data.greetingreport);
                    $('#chart_div4').html(response.data.designer_request);


                    // sagar code for chart
                    google.charts.load('current', {
                        'packages': ['bar']
                    });
                    google.charts.setOnLoadCallback(drawChart);

                    function drawChart() {
                        // var data = google.visualization.arrayToDataTable([
                        //     ['Date', 'qty', 'sell'],
                        //     ['2022-12-1', 120, 400],
                        //     ['2022-12-2', 343, 460],
                        //     ['2022-12-3', 41, 1120],
                        //     ['2022-12-4', 67, 540]
                        // ]);
                        var data = google.visualization.arrayToDataTable(response.data.designer_request);


                        var options = {
                            chart: {
                                title: 'Monthly Designer Report',
                                subtitle: 'Date, request',
                            },
                            bars: 'vertical',
                            vAxis: {
                                format: 'decimal'
                            },

                            height: 400,
                            colors: ['#1b9e77', '#d95f02', '#7570b3']
                        };

                        var chart = new google.charts.Bar(document.getElementById('chart_div4'));

                        chart.draw(data, google.charts.Bar.convertOptions(options));

                        var btns = document.getElementById('btn-group');

                        btns.onclick = function(e) {

                            if (e.target.tagName === 'BUTTON') {
                                options.vAxis.format = e.target.id === 'none' ? '' : e.target.id;
                                chart.draw(data, google.charts.Bar.convertOptions(options));
                            }
                        }
                    }
                }
            },
            error: function(data) {
                //console.log("IN ERRROR "+data);
            }
        });
    }


    function weeklyOrderTotal(){
		var APP_URL = {!! json_encode(url('/')) !!};

		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: APP_URL+'/admin/weeklyOrderTotal/',
			//url: APP_URL+product.categoryList+id,

			type: "POST",
			data: {
				"id": null,
				"_method":'POST',
			},
			success: function (response) {
                if(response.status == true){
                    $('#postal_mails_weekly').html("Weekly Orders : " + response.data.postal_mails_weekly );
                    $('#postal_mails_weekly_amount').html("Weekly Total : " + response.data.postal_mails_weekly_amount);

                    $('#postcard_weekly').html("Weekly Orders : " + response.data.postcard_weekly );
                    $('#postcard_weekly_amount').html("Weekly Total : " + response.data.postcard_weekly_amount);

                    $('#gifthub_weekly').html("Weekly Orders : " + response.data.gifthub_weekly );
                    $('#gifthub_weekly_amount').html("Weekly Total : " + response.data.gifthub_weekly_amount);

                    $('#greetings_weekly').html("Weekly Orders : " + response.data.greetings_weekly );
                    $('#greetings_weekly_amount').html("Weekly Total : " + response.data.greetings_weekly_amount);


                    //today toal amount
                    $('#postal_mails_amount_today').html("Today's Total : " + response.data.postal_mails_amount_today );
                    $('#postcard_amount_today').html("Today's Total : " + response.data.postcard_amount_today );
                    $('#gifthub_amount_today').html("Today's Total : " + response.data.gifthub_amount_today );
                    $('#greetings_amount_today').html("Today's Total : " + response.data.greetings_amount_today );

                    //monthly
                    $('#postal_mails_monthly').html("Monthly Order : " + response.data.postal_mails_monthly );
                    $('#postal_mails_monthly_amount').html("Monthly Total : " + response.data.postal_mails_monthly_amount);

                    $('#postcard_monthly').html("Monthly Order : " + response.data.postcard_monthly );
                    $('#postcard_monthly_amount').html("Monthly Total : " + response.data.postcard_monthly_amount);

                    $('#gifthub_monthly').html("Monthly Order : " + response.data.gifthub_monthly );
                    $('#gifthub_monthly_amount').html("Monthly Total : " + response.data.gifthub_monthly_amount);

                    $('#greetings_monthly').html("Monthly Order : " + response.data.greetings_monthly );
                    $('#greetings_monthly_amount').html("Monthly Total : " + response.data.greetings_monthly_amount);

                    $('#monthly_total_amount').html(response.data.monthly_total_amount );

                    $('#design_request').html(response.data.design_request );
                }
			},
			error: function(data) {
				//console.log("IN ERRROR "+data);
			}
		});
	}

    function orderData(){
		var APP_URL = {!! json_encode(url('/')) !!};

		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: APP_URL+'/admin/orderData/',
			//url: APP_URL+product.categoryList+id,

			type: "POST",
			data: {
				"id": null,
				"_method":'POST',
			},
			success: function (response) {
                if(response.status == true){
                    $('#totalORderShippedAndAwaitingForDelivery').html(response.data.totalORderShippedAndAwaitingForDelivery );
                    $('#totalORderDelivered').html(response.data.totalORderDelivered);
                    $('#totalPedningOrder').html(response.data.totalPedningOrder);
                    $('#totalReturnRefund').html(response.data.totalReturnRefund);
                    $('#totalCancelOrder').html(response.data.totalCancelOrder);
                }
			},
			error: function(data) {
				//console.log("IN ERRROR "+data);
			}
		});
	}

    function productDataForDashboard(){
		var APP_URL = {!! json_encode(url('/')) !!};

		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: APP_URL+'/admin/productDataForDashboard/',
			//url: APP_URL+product.categoryList+id,

			type: "POST",
			data: {
				"id": null,
				"_method":'POST',
			},
			success: function (response) {
                if(response.status == true){
                    $('#postalProductCount').html(response.data.postalProductCount );
                    $('#gifthubProductCount').html(response.data.gifthubProductCount );
                    $('#postcardProductCount').html(response.data.postcardProductCount );
                    $('#postalProductActiveCount').html(response.data.postalProductActiveCount );
                    $('#postalProductInActiveCount').html(response.data.postalProductInActiveCount );

                    //Google chart start here.
                    google.charts.load('current', {'packages':['corechart']});
                    google.charts.setOnLoadCallback(drawChart);
                    google.charts.setOnLoadCallback(drawChart1);
                    google.charts.setOnLoadCallback(gifthubTopTenSellingProducts);

                    function drawChart() {
                        /* var data = google.visualization.arrayToDataTable([
                        ['Task', 'Hours per Day'],
                        ['Work',     11],
                        ['Eat',      2],
                        ['Commute',  2],
                        ['Watch TV', 2],
                        ['Sleep',    7]
                        ]); */

                        var data = google.visualization.arrayToDataTable(response.data.postalTopTenSellingProducts);

                        var options = {
                        title: 'Greetings Top Ten Selling Products'
                        };

                        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

                        chart.draw(data, options);
                    }

                    function drawChart1() {
                        var data = google.visualization.arrayToDataTable(response.data.postcardTopTenSellingProducts);

                        var options = {
                            title: 'Postcard Top Ten Selling Products'
                        };

                        var chart = new google.visualization.PieChart(document.getElementById('piechart1'));

                        chart.draw(data, options);
                    }

                    function gifthubTopTenSellingProducts() {
                        var data = google.visualization.arrayToDataTable(response.data.gifthubTopTenSellingProducts);

                        var options = {
                            title: 'Gift Top Ten Selling Products'
                        };

                        var chart = new google.visualization.PieChart(document.getElementById('gifthubTopTenSellingProducts'));

                        chart.draw(data, options);
                    }
                }
			},
			error: function(data) {
				//console.log("IN ERRROR "+data);
			}
		});
	}
</script>










