<meta name="csrf-token" content="{{ csrf_token() }}" />

@extends('admin.index')

@section('after-style')

<link rel="stylesheet" href="{{asset('public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

@endsection

@section('content')

<?php $uriSegment = Request::segment(2); ?>

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('Admin.Dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Manage Order</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                @if(!empty($uriSegment) && ($uriSegment == "giftPendingOrderVendor"))

                                    Pending Order List
                                @elseif(!empty($uriSegment) && ($uriSegment == "postalCompleted"))


                                    Postal Completed Order List
                                @elseif(!empty($uriSegment) && ($uriSegment == "ratingReviewGift"))
                                    Product Review/Rating
                                @else
                                    Order List

                                @endif
                            </h3>

                            </h3>

                        </div>

                        <h5 id="ajax_response" style="color:red; display:none;"><span id="msg_span" class="" style="color: green; margin:10px;"></span> </h5>

                        @if(session('message'))
                            <div class="alert alert-success" style="padding: 5px 20px;margin-bottom: 5px;">{{ session('message') }}</div>
                        @endif
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="user-list" class="table table-bordered table-striped" data-order='[[ 0, "asc" ]]'>
                                <thead>
                                    <tr>
                                        <th>Order Number</th>
										<th>Order Item</th>

                                        <th>Client Name</th>
                                        <th>Created At</th>
                                        @if(!empty($uriSegment) && ($uriSegment == "ratingReviewGift") OR ($uriSegment == "ratingReviewPostal"))
                                            <th>Rating</th>
                                            <th>Review</th>
                                        @else
                                        @if(!empty($uriSegment) && ($uriSegment == "giftPendingOrderVendor"))
                                        <th>Pending Days</th>
                                        @endif

                                            <th>Order Status</th>
                                            <th width="20%">View</th>
                                        @endif


                                    </tr>
                                </thead>
                                <tbody>
                                @php $i=1; @endphp
                                    {{-- @dd(3434); --}}
                                    @if(isset($data) && $data != null)
                                        @foreach($data as $val)

                                            <tr>
                                                <td>{{$val->order_number}}</td>
                                                <td>{{$val->product_name}}</td>

                                                <td>{{$val->sender_name}}</td>
                                                <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$val->created_at)->tz("Asia/Kuala_Lumpur") }}

                                                @if(!empty($uriSegment) && ($uriSegment == "ratingReviewGift") OR ($uriSegment == "ratingReviewPostal"))
                                                    <td>
                                                        @if(!empty($val['rating']->rate))
                                                            @for ($i = 0; $i < 5; $i++)
                                                                @if (floor($val['rating']->rate) - $i >= 1)
                                                                    {{-- Full Start --}}
                                                                    <i class="fas fa-star text-warning"> </i>
                                                                @elseif ($val['rating']->rate - $i > 0)
                                                                    {{-- Half Start --}}
                                                                    <i class="fas fa-star-half-alt text-warning"> </i>
                                                                @else
                                                                    {{-- Empty Start --}}
                                                                    <i class="far fa-star text-warning"> </i>
                                                                @endif
                                                            @endfor
                                                        @endif
                                                    </td>
                                                    <td>{{$val->rating->review}}</td>
                                                @else
                                                @if(!empty($uriSegment) && ($uriSegment == "giftPendingOrderVendor"))

                                                <td>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($val->created_at))->diffForHumans()  }}</td>

                                                @endif
                                                @if(!empty($uriSegment) && ($uriSegment == "giftPendingOrderVendor"))
                                                    <td>Pending Order</td>
                                                @else

                                                <td>{{$val->order_status_name  }}</td>
                                                @endif

                                                    <td style="text-align: center;">
                                                        <a href="javascript:void();" data-toggle="modal" data-target="#user-info{{$val->order_item_id}}"><i class="fas fa-eye"></i>&nbsp;&nbsp;</a>
                                                        <a href="{{route('Vendor.order.edit',[$val->order_item->id])}}?page={{(!empty($uriSegment)) ? $uriSegment :null}}"><img style="width:20px;" src="{{asset('public/ic_track.png')}}" />{{-- <i class="fas fa-edit"></i> --}}&nbsp;&nbsp;</a>
                                                        {{-- <a href="#" class="cancel_order" data-id="{{$val->order_item->id}}">Cancel Order </a> --}}
                                                         <a href="#" data-toggle="modal"
                                                                data-target="#cancel{{ $val->order_item_id }}">Cancel Order
                                                            </a>

                                                    </td>
                                                @endif

                                                {{-- -------sagar order cencel ----------------- --}}
                                                 <!-- Modal -->
                                                 <div class="modal fade" id="cancel{{ $val->order_item_id }}"
                                                    tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                    aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">Cancel
                                                                    Order </h5>
                                                                <button type="button" class="close"
                                                                    data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="form-group">
                                                                    <label for="customSelect">Select an Option:</label>
                                                                    <select class="form-control customSelect">
                                                                        <option class="form-control"
                                                                            value="Out of Stock">Out of Stock</option>
                                                                        <option class="form-control"
                                                                            value="Unable to Reach User Location">Unable
                                                                            to Reach User Location</option>
                                                                        <option class="form-control" value="custom">
                                                                            Custom</option>
                                                                    </select>

                                                                    <div class="form-group customInput"
                                                                        style="display: none;">
                                                                        <label for="customValue">Enter Custom
                                                                            Comment:</label>
                                                                        <input type="text"
                                                                            class="form-control customValue">
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="modal-footer">

                                                                <button type="button" class="btn btn-secondary"
                                                                    data-dismiss="modal">Close</button>


                                                                <button type="button"
                                                                    data-id="{{ $val->order_item->id }}"
                                                                    class="btn btn-primary cancel_order">Confirm</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    @include('admin.order.view')
</div>

@endsection

@section('after-scripts')
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script>
    const selectElements = document.querySelectorAll(".customSelect");
    const customInputs = document.querySelectorAll(".customInput");
    const customValueInputs = document.querySelectorAll(".customValue");

    // Add event listeners to all select elements
    selectElements.forEach((selectElement, index) => {
        selectElement.addEventListener("change", function() {
            if (selectElement.value === "custom") {
                customInputs[index].style.display = "block";
            } else {
                customInputs[index].style.display = "none";
            }
        });
    });
</script>
<script>
    $(function() {
        $("#user-list").DataTable({
            "responsive": true,
            "autoWidth": false,
        });
    });

    $('body').on('click', '.rowDelete', function() {
      	var APP_URL = {!! json_encode(url('/')) !!};
        var id = $(this).data("id");
        var token = $(this).data("token");

        $(this).closest('tr').remove();
	     $.ajax({
           // url: APP_URL+'/vendorproduct/delete/'+id,
            url: APP_URL+'/admin/order/delete/'+id,
            type: "POST",
            data: {
                "id": id,
                "_method":'DELETE',
                "_token": token
            },
            success: function (response) {
                console.log('data deleted successfully');
                //window.location.href= APP_URL+'/vendorproduct';
                location.reload();
            },
            error: function(response) {
                console.log("IN ERRROR "+response);
                // window.location.href= window.location.pathname;
            }
        });

    });

    $('body').on('click', '.cancel_order', function() {
            if (confirm("Are you sure?") == true) {
                var APP_URL = {!! json_encode(url('/')) !!};
                var id = $(this).data("id");
                var token = $(this).data("token");
                var selectedOption = $(".customSelect")
            .val(); // Assuming you have a select element with an id of "customSelect"
                var customValue = $(".customValue")
            .val(); // Assuming you have an input element with an id of "customValue"

                $(this).closest('tr').remove();
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: APP_URL + '/vendor/order/cancel-order/' + id,
                    type: "POST",
                    data: {
                        "id": id,
                        "selectedOption": selectedOption, // Add the selected option value to the data
                        "customValue": customValue, // Add the custom value to the data
                        "_method": 'POST',
                        "_token": token
                    },
                    success: function(data) {
                        if (data.status === false) {
                            $('#ajax_response').css('color', 'green');
                            $('#ajax_response').css('display', 'block');
                            $('#msg_span').html(data.msg);
                        } else {
                            $('#ajax_response').css('color', 'green');
                            $('#ajax_response').css('display', 'block');
                            $('#msg_span').text(data.msg);
                            $('#app').scrollTop()
                            $('#ajax_response').fadeIn().delay(6000).fadeOut();
                            $("html, body").animate({
                                scrollTop: 0
                            }, "slow");
                            //  window.location.href = data.redirect;
                        }
                    },
                    error: function(response) {
                        console.log("IN ERRROR " + response);
                        // window.location.href= window.location.pathname;
                    }
                });
            }
        });
</script>
@endsection
