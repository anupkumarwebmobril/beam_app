@extends('admin.index')
<meta name="csrf-token" content="{{ csrf_token() }}" />
@section('content')
    <!-- <div class="wrapper"> -->

    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <!--  -->
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('Admin.Dashboard') }}">Home</a></li>
                            <li class="breadcrumb-item active">Update Order Status</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-1"></div>
                    <div class="col-10">

                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">Update Easy parcel tracking number</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            
                            @include('admin.partials.messages')
                           	<form class="form-horizontal" method="post" action="{{route('Vendor.order.update')}}" enctype="multipart/form-data" autocomplete="off">
                                
								@method('POST')
                                @csrf
                                <input type="hidden" name="id" value="{{ $order_item->id }}" />
                                <input type="hidden" name="page" value="{{ $page }}" />
                                <input type="hidden" name="old_order_status" value="{{$order_item->order_status_name}}" />
                                <div class="card-body">

									<div class="form-group row">
										<label for="Status" class="col-sm-2 col-form-label">Easy parcel tracking number</label>

										<div class="col-sm-10">
                                        <input type="text" class="form-control" placeholder=""name="easy_parcel_order_no" value="{{$order_item->easy_parcel_order_no}}" required>

											{{-- <select class="form-control select2" style="width: 100%;" name="order_status_name"
												id="order_status_name" required="true">
												<option value="">Select order status</option>
												@foreach($order_status_master as $order_satus_master)
                                                    <option value="{{ $order_satus_master->order_status_name }}"
                                                        {{ !empty($data->order_status_name) && $data->order_status_name === $order_satus_master->order_status_name ? 'selected="selected"' : '' }}>
                                                        {{ $order_satus_master->name }}
                                                    </option>
                                                @endforeach

                                            </select> --}}
										</div>
									</div>

                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-info">Update</button>
                                    {{-- <a href="{{ route('Vendor.order.zip') }}"
                                        class="btn btn-default float-right">Cancel</a> --}}
                                </div>
                                <!-- /.card-footer -->
                            </form>
                        </div>

                    </div>
                    <div class="col-1"></div>
                </div>
            </div>
        </section>

    </div>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>


    <!-- </div> -->
@endsection
