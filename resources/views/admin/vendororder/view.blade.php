<style>
    .custom-class-grid {
        display: grid;
        grid-template-columns: repeat(3,1fr);
        grid-column-gap: 30px;
        grid-row-gap: 0px;
        border: 1px solid #c2c2c2;
        border-radius: 5px;
        padding: 20px;
    }
</style>

<?php 
    $mislanious7 = \App\Models\Miscellaneous::where('id', 7)->first();
    $mislanious8 = \App\Models\Miscellaneous::where('id', 8)->first();
    
?>

@foreach($data as $view)
<div class="modal fade" id="user-info{{$view->id}}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Viewing order number <strong>{{$view->order_number}}</strong></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Name" class="col-form-label">Order Item</label>
                            <div class="col-sm-10">
                                <p>{{$view->product_name}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Email" class="col-form-label">Order Status</label>
                            <div class="col-sm-10">
                                <p>{{$val->order_status_name}}</p>
                            </div>
                        </div>
                    </div>
                </div>

                <hr>

                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Mobile" class="col-form-label">Message</label>
                            <div class="col-sm-10">
                                <p>{{$view->msg}}</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label for="Status" class="col-form-label">{{ (!empty($mislanious7->name)) ? $mislanious7->name : "" }}</label>
                            <div class="col-sm-10">
                               <p>{{($view->same_day_delivery == 1) ? "Yes" : "No"}}</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label for="Status" class="col-form-label">Add premium service</label>
                            <div class="col-sm-10">
                                <p>{{($view->add_premium_service == 1) ? "Yes" : "No"}}</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Mobile" class="col-form-label">Paper type</label>
                            <div class="col-sm-10">

                                <select disabled="true" class="form-control select2">
                                    <option></option>
                                    @foreach($paper_type as $paper)
                                        <option value="{{$paper->id}}" {{($paper->id == $view->paper_type_id) ? "selected='selected'" : null }}>{{$paper->name}}</option>
                                    @endforeach
                                </select>
                                <p>{{$view->medical_card_number}}</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label for="Status" class="col-form-label">{{ (!empty($mislanious8->name)) ? $mislanious8->name : "" }}</label>
                            <div class="col-sm-10">
                                 <p>{{$view->additional_interests}}</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label for="Mobile" class="col-form-label">Total</label>
                            <div class="col-sm-10">
                                <p>RM:{{$view->total}}</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Status" class="col-form-label">{{ (!empty($mislanious8->name)) ? $mislanious8->name : "" }} Msg</label>
                            <div class="col-sm-10">
                                <p>{{$view->additional_interests_msg}}</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label for="Status" class="col-form-label">Recepient</label>
                            <div class="col-sm-10">
                                <p>{{$view->recepient}}</p>
                            </div>
                        </div>
                    </div>

                </div>
                
                <hr>
                
                <div class="row">
                    <div class="col-4">

                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label for="Status" class="col-form-label">Recepient Information</label>
                            <div class="col-sm-10">
                                <p></p>
                            </div>
                        </div>
                    </div>

                    <div class="col-4">

                    </div>
                </div>

                <div class="row">

                    <div class="col-4">
                        <div >
                            <label for="Mobile" class="col-form-label">Name</label>                                
                        </div>
                    </div>

                    <div class="col-4">
                        <div >
                            <label for="Mobile" class="col-form-label">Phone Number</label>                                
                        </div>
                    </div>

                    <div class="col-4">
                        <div >
                            <label for="Mobile" class="col-form-label">House Number</label>                                
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-4">
                        <div >
                            <label for="Mobile" class="col-form-label">Landmark</label>
                        </div>
                    </div>

                    <div class="col-4">
                        <div >
                            <label for="Mobile" class="col-form-label">City</label>
                        </div>
                    </div>

                    <div class="col-4">
                        <div >
                            <label for="Mobile" class="col-form-label">State</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-4">
                        <div>
                            <label for="Mobile" class="col-form-label">Zip Code</label>                                
                        </div>
                    </div>
                    <div class="col-8">
                        <div>
                            <label for="Mobile" class="col-form-label">Full Address</label>                                
                        </div>
                    </div>
                </div>

                @if(!empty($view->recepients))
                    @foreach ($view->recepients as $recepient)
                        <div class="row custom-class-grid">
                            <div>
                                <p>{{ $recepient->name }}</p>
                            </div>

                            <div>
                                <p>{{ $recepient->phone_number }}</p>
                            </div>

                            <div>
                                <p>{{ $recepient->house_number }}</p>
                            </div>

                            <div>
                                <p>{{ $recepient->land_mark }}</p>
                            </div>

                            <div>
                                <p>{{ $recepient->city }}</p>
                            </div>

                            <div>
                                <p>{{ $recepient->state }}</p>
                            </div>

                            <div>
                                <p>{{ $recepient->zip_code }}</p>
                            </div>
                            <div>
                                <p>{{ $recepient->full_address }}</p>
                            </div>
                        </div>
                    @endforeach            
                @endif





            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endforeach
