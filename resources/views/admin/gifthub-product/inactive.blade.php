@extends('admin.index')

@section('after-style')
    <link rel="stylesheet" href="{{ asset('public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endsection

@section('content')

    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">

                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('Admin.Dashboard') }}">Home</a></li>
                            <li class="breadcrumb-item active">Gifthub Product</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Gifthub Inactive Product List</h3>

                                <a href="{{ url('admin/gifthub/product/active-all') }}" style="float: right;">Active All<i
                                        class="fas fa-check" style="font-size:25px;"></i></a>
                            </div>

                            @if (session('error'))
                                <div class="alert alert-danger" style="padding: 5px 20px;margin-bottom: 5px;">
                                    {{ session('error') }}</div>
                            @endif

                            @if (session('message'))
                                <div class="alert alert-success" style="padding: 5px 20px;margin-bottom: 5px;">
                                    {{ session('message') }}</div>
                            @endif
                            <!-- /.card-header -->
                            <div class="card-body tablescroll">
                                <table id="user-list" class="table table-bordered table-striped"
                                    data-order='[[ 0, "asc" ]]'>
                                    <thead>
                                        <tr>
                                            <th>Sr No.</th>
                                            <th>Product Name</th>
                                            <th>Shipping Fee</th>
                                            <th>Product Price</th>
                                            <th>Total Price</th>
                                            <th>Product Code</th>
                                            <th>Min order quantity</th>
                                            <th>Category</th>
                                            <th>Sub Category</th>
                                            <th>Vendor</th>
                                            <th>Product Images1</th>
                                            {{-- <th>Product Images2</th>
                                        <th>Product Images3</th>
                                        <th>Product Images4</th> --}}
                                            <th>Description</th>
                                            <th>Specifications</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $i=1; @endphp

                                        @if (isset($data) && $data != null)
                                            @foreach ($data as $val)
                                                <tr>
                                                    <td>{{ $i++ }}</td>
                                                    <td>{{ $val->name }}</td>
                                                    <td>
                                                        <p class="text-success">RM:{{ $val->shipping_fee }}</p>
                                                    </td>
                                                    <td>
                                                        <p class="text-success">RM:{{ $val->price_without_shipping }}</p>
                                                    </td>
                                                    <td>
                                                        <p class="text-success">RM:{{ $val->price }}</p>
                                                    </td>
                                                    <td>{{$val->product_code}}</td>
                                                    <td>{{$val->min_order_qty}}</td>

                                                    <td> {{ isset($val->occasion['name']) ? $val->occasion['name'] : '' }}
                                                    </td>
                                                    <td> {{ isset($val->category['name']) ? $val->category['name'] : '' }}
                                                    </td>
                                                    <td> {{ isset($val->user['name']) ? $val->user['name'] : '' }}</td>
                                                    <td><img src={{ asset('storage/product/' . $val->image) }}
                                                            class="img-fluid ${3|rounded-top,rounded-right,rounded-left,rounded-circle,|}"
                                                            width="60%" style="border:3px solid white" alt="">
                                                    </td>
                                                    {{-- <td><img src={{asset("storage/product/".$val->image_2)}} class="img-fluid ${3|rounded-top,rounded-right,rounded-left,rounded-circle,|}" width="60%" style="border:3px solid white" alt=""></td> --}}
                                                    {{-- <td><img src={{asset("storage/product/".$val->image_3)}} class="img-fluid ${3|rounded-top,rounded-right,rounded-left,rounded-circle,|}" width="60%" style="border:3px solid white" alt=""></td> --}}
                                                    {{-- <td><img src={{asset("storage/product/".$val->image_4)}} class="img-fluid ${3|rounded-top,rounded-right,rounded-left,rounded-circle,|}" width="60%" style="border:3px solid white" alt=""></td> --}}
                                                    <td>{!! $val->product_description !!}</td>
                                                    <td>{!! $val->product_specification !!}</td>
                                                    <td>
                                                        @if ($val->status == 0)
                                                            <button id="{{ $val->id }}"
                                                                class="btn btn-outline-success btn-sm"
                                                                onclick="activate(this.id)">Activate</button>
                                                        @elseif($val->status == 1)
                                                            <button id="{{ $val->id }}"
                                                                class="btn btn-outline-danger btn-sm"
                                                                onclick="deactivate(this.id)">Deactivate</button>
                                                        @endif
                                                    </td>







                                                    <td style="text-align: center;">
                                                        {{-- <a href="javascript:void();" data-toggle="modal" data-target="#user-info{{$val->id}}"><i class="fas fa-eye"></i>&nbsp;&nbsp;</a> --}}
                                                        <a
                                                            href="{{ route('Admin.gifthub.edit', [$val->id]) }}?page=inactive"><i
                                                                class="fas fa-edit"></i>&nbsp;&nbsp;</a>
                                                        <a href="javascript:void(0);" class="rowDelete"
                                                            data-id="{{ $val->id }}"
                                                            data-token="{{ csrf_token() }}"><i
                                                                class="fas fa-trash"></i></a>

                                                        {{-- <a href="vendorproduct/edit/{{$val->id}}" class="me-3 text-primary" data-bs-toggle="tooltip" data-placement="top" title="Edit" ><i class="material-icons">mode_edit</i></a>
													<a href="javascript:void(0);" class="text-danger rowDelete" data-bs-toggle="tooltip" data-placement="top" title="Delete" data-id="{{$val->id}}" data-token="{{ csrf_token() }}"><i class="material-icons">delete</i></a> --}}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>

                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
        @include('admin.product.view_product')
    </div>

@endsection

@section('after-scripts')
    <script src="{{ asset('public/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('public/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script>
        function activate(id) {
            $.ajax({
                type: 'POST',
                url: "{{ url('admin/activate-product') }}",
                data: {
                    _token: "{{ csrf_token() }}",
                    userId: id,
                },
                success: function(data) {
                    location.reload();
                }
            });
        }
    </script>
    <script>
        function deactivate(id) {
            $.ajax({
                type: 'POST',
                url: "{{ url('admin/deactivate-product') }}",
                data: {
                    _token: "{{ csrf_token() }}",
                    userId: id,
                },
                success: function(data) {
                    location.reload();
                }
            });
        }
    </script>
    <script>
        $(function() {
            $("#user-list").DataTable({
                "responsive": true,
                "autoWidth": false,
            });
        });

        $('body').on('click', '.rowDelete', function() {
            var APP_URL = {!! json_encode(url('/')) !!};
            var id = $(this).data("id");
            var token = $(this).data("token");

            $(this).closest('tr').remove();
            $.ajax({
                // url: APP_URL+'/vendorproduct/delete/'+id,
                url: APP_URL + '/admin/product/delete/' + id,
                type: "POST",
                data: {
                    "id": id,
                    "_method": 'DELETE',
                    "_token": token
                },
                success: function(response) {
                    console.log('data deleted successfully');
                    //window.location.href= APP_URL+'/vendorproduct';
                    location.reload();
                },
                error: function(response) {
                    console.log("IN ERRROR " + response);
                    // window.location.href= window.location.pathname;
                }
            });

        });
    </script>
@endsection
