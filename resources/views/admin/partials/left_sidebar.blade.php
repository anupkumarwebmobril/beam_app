<style>
    .main-sidebar,
    .main-sidebar::before {
        transition: margin-left .3s ease-in-out, width .3s ease-in-out;
        width: 260px;
    }

    .cust-ul {
        padding-left: 16px;
        list-style-type: none;
    }
</style>

<aside class="main-sidebar sidebar-dark-primary elevation-4">
    @if (auth()->user()->user_type == 1)
        <!-- Brand Logo -->
        <a href="{{ route('Admin.Dashboard') }}" class="brand-link">
            <img src="{{ asset('public/admin.png') }}" alt="Beam App" class="brand-image img-circle elevation-3"
                style="opacity: .8;">
            <h5>Beam App</h5>
            <span class="brand-text font-weight-light"></span>
        </a>
        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <img src="{{ asset('storage/profile_pictures/' . auth()->user()->profile_image) }}"
                        class="img-circle elevation-2" alt="">
                </div>
                <div class="info">
                    <a href="{{ route('Admin.Dashboard') }}" class="d-block">{{ auth()->user()->name ?? '' }}</a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                    data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                    <li
                        class="nav-item {{ request()->is('admin/profile') || request()->is('admin/change-password') || request()->is('admin/get-profile') ? 'menu-open' : '' }}">
                        <a href="#" class="nav-link">
                            <i class="fas fa-info"></i>
                            <p>Manage Profile
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route('Admin.Profile') }}"
                                    class="nav-link {{ request()->is('admin/profile') ? 'active-child' : '' }}">
                                    <i class="fas fa-address-card nav-icon"></i>
                                    <p>Profile</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('Admin.Change.Password.Form') }}"
                                    class="nav-link {{ request()->is('admin/change-password') ? 'active-child' : '' }}">
                                    <i class="fas fa-key    "></i>
                                    <p>Change Password</p>
                                </a>
                            </li>

                        </ul>
                    </li>




                    <ul class="cust-ul">
                        <a>BEAM COMMUNITY</a>

                        <li class="nav-item">
                            <a href="{{ route('Admin.Users.List') }}"
                                class="nav-link {{ request()->is('admin/users-list') ? 'active' : '' }}">
                                <i class="fas fa-users-cog"></i>
                                <p>
                                    User List
                                </p>
                            </a>
                        </li>

                        <li class="nav-item @if (request()->is('admin/vendor-list') or request()->is('admin/printer-vendor-list')) {{ 'menu-open' }} @endif">
                            <a href="#"
                                class="nav-link @if (request()->is('admin/vendor-list') or request()->is('admin/printer-vendor-list')) {{ 'active' }} @endif">
                                <i class=" fa fa-product-hunt"></i>
                                <p>
                                    Vendor List
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview"
                                style="@if (request()->is('admin/vendor-list') or request()->is('admin/printer-vendor-list')) OR (request()->is('admin/securePrinterList'))) {{ 'display:block' }} @endif">
                                <li class="nav-item">
                                    <a href="{{ route('Admin.Vendor.List') }}" class="nav-link">
                                        <i class="nav-icon far fa-circle text-info"></i>
                                        <p>
                                            Gifting Vendor
                                        </p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('Admin.Vendor.PrinterList') }}" class="nav-link">
                                        <i class="nav-icon far fa-circle text-info"></i>
                                        <p>
                                            Printing Service Provider
                                        </p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('Admin.Vendor.securePrinterList') }}" class="nav-link"
                                        style="white-space: normal;display: flex;align-items: baseline;">
                                        <i class="nav-icon far fa-circle text-info"></i>
                                        <p>
                                            Secure Printing Service Provider
                                        </p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route('Admin.Designer.List') }}"
                                class="nav-link {{ request()->is('admin/designer-list') || request()->is('admin/add-Designer') || request()->is('admin/edit-designer/*') ? 'active' : '' }}">
                                <i class="fas fa-users-cog"></i>
                                <p>
                                    Designer List
                                </p>
                            </a>
                        </li>
                    </ul>

                    {{-- <li class="nav-item">
                        <a href="{{route('Admin.Vendor.List')}}" class="nav-link {{ (request()->is('admin/vendor-list')) ? 'active' : '' }}">
                            <i class="fas fa-users-cog"></i>
                            <p>
                                Gifting Vendor List
                            </p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{route('Admin.Vendor.PrinterList')}}" class="nav-link {{ (request()->is('admin/printer-vendor-list')) ? 'active' : '' }}">
                            <i class="fas fa-users-cog"></i>
                            <p>
                                Printing Service Provider
                            </p>
                        </a>
                    </li> --}}




                    {{-- <li class="nav-item">
                        <a href="{{route('Admin.Vendors.List')}}" class="nav-link {{ (request()->is('admin/vendors-list')) || (request()->is('admin/add-vendor')) || (request()->is('admin/edit-vendor/*'))? 'active' : '' }}">
                            <i class="nav-icon fas fa-user-tie"></i>
                            <p>
                                Vendor List
                            </p>
                        </a>
                    </li> --}}
                    <br>
                    <ul class="cust-ul">
                        <a>PRODUCT CATEGORIES</a>
                        <li class="nav-item">
                            <a href="{{ route('Admin.category.index') }}"
                                class="nav-link {{ request()->is('admin/category') || request()->is('admin/category') || request()->is('admin/category/*') ? 'active' : '' }}">
                                <i class="fa fa-list"></i>
                                <p>
                                    Category List
                                </p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route('Admin.subcategory.index') }}"
                                class="nav-link {{ request()->is('admin/subcategory') || request()->is('admin/subcategory') || request()->is('admin/subcategory/*') ? 'active' : '' }}">
                                <i class="fa fa-list-alt"></i>
                                <p>
                                    Subcategory List
                                </p>
                            </a>
                        </li>
                    </ul>
                    <hr>
                    <ul class="cust-ul">
                        <a>POSTAL PRODUCT</a>
                        <li
                            class="nav-item  {{ request()->is('admin/product') || request()->is('admin/product/inactive') ? 'menu-open' : '' }} ">
                            <a href="#"
                                class="nav-link {{ request()->is('admin/product') || request()->is('admin/product/inactive') ? 'active' : '' }}">
                                <i class=" fa fa-product-hunt"></i>
                                <p>
                                    Greetings List
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{ route('Admin.product.index') }}"
                                        class="nav-link {{ request()->is('admin/product') ? 'active-child' : '' }}">
                                        <i class="nav-icon far fa-circle text-info"></i>
                                        <p>Active Products</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ url('admin/product/inactive') }}"
                                        class="nav-link {{ request()->is('admin/product/inactive') ? 'active-child' : '' }}">
                                        <i class="nav-icon far fa-circle text-info"></i>
                                        <p>In-Active Products</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route('Admin.postcard.index') }}"
                                class="nav-link {{ request()->is('admin/postcard') || request()->is('admin/postcard') || request()->is('admin/postcard/*') ? 'active' : '' }}">
                                <i class="fa fa-product-hunt"></i>
                                <p>
                                    Postcard List
                                </p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route('Admin.product.recommended') }}"
                                class="nav-link {{ request()->is('admin/recommended') ? 'active' : '' }}">
                                <i class="fa fa-product-hunt"></i>
                                <p>
                                    Recommended For You
                                </p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route('Admin.formal.index') }}"
                                class="nav-link {{ request()->is('admin/formal') || request()->is('admin/formal') || request()->is('admin/formal/*') ? 'active' : '' }}">
                                <i class="fa fa-envelope-o"></i>
                                <p>
                                    Postal Services
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('Admin.Designer.Request') }}"
                                class="nav-link {{ request()->is('admin/designer-request') || request()->is('admin/designer-request') || request()->is('admin/designer-request/*') ? 'active' : '' }}">
                                <i class="fa fa-envelope-o"></i>
                                <p>
                                    Design Request
                                </p>
                            </a>
                        </li>
                    </ul>
                    <br>
                    <ul class="cust-ul">
                        <a>GIFTHUB PRODUCT</a>
                        <li class="nav-item  @if (request()->is('admin/gifthub/product') or request()->is('admin/gifthub/product/inactive')) {{ 'menu-open' }} @endif">
                            <a href="#" class="nav-link"
                                @if (request()->is('admin/gifthub/product') or request()->is('admin/gifthub/product/inactive')) {{ 'active' }} @endif">
                                <i class="fas fa-gift"></i>
                                <p>
                                    Gifthub Product
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview"
                                style=" @if (request()->is('admin/gifthub/product') or request()->is('admin/gifthub/product/inactive')) {{ 'display:block' }} @endif">
                                <li class="nav-item">
                                    <a href="{{ route('Admin.gifthub.index') }}" class="nav-link">
                                        <i class=" far fa-circle text-info"></i>
                                        <p>Active Products</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ url('admin/gifthub/product/inactive') }}" class="nav-link">
                                        <i class="far fa-circle text-info"></i>
                                        <p>In-Active Products</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route('Admin.product.bestseller') }}"
                                class="nav-link {{ request()->is('admin/bestseller') ? 'active' : '' }}">
                                <i class="fa fa-product-hunt"></i>
                                <p>
                                    Best Seller
                                </p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route('Admin.product.newgiftnearyou') }}"
                                class="nav-link {{ request()->is('admin/newgiftnearyou') ? 'active' : '' }}">
                                <i class="fa fa-product-hunt"></i>
                                <p>
                                    New Gift Near You
                                </p>
                            </a>
                        </li>
                    </ul>
                    <br>
                    <ul class="cust-ul">
                        <a>ORDER SUMMARY</a>
                        <li class="nav-item  @if (request()->is('admin/order') or request()->is('admin/order-printer')) {{ 'menu-open' }} @endif">
                            <a href="#"
                                class="nav-link @if (request()->is('admin/order') or request()->is('admin/order-printer')) {{ 'active' }} @endif">
                                <i class="fa fa-shopping-cart"></i>
                                <p>
                                    Order List
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview"
                                style="@if (request()->is('admin/order') or request()->is('admin/order-printer')) {{ 'display:block' }} @endif">
                                <li class="nav-item">
                                    <a href="{{ route('Admin.order.index') }}" class="nav-link">
                                        <i class="nav-icon far fa-circle text-info"></i>
                                        <p>Gift Order List</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('Admin.order-printer.zip') }}" class="nav-link">
                                        <i class="nav-icon far fa-circle text-info"></i>
                                        <p>Postal Order List</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item  @if (request()->is('admin/cancelordervendoradmin') or request()->is('admin/printercancelorderadmin')) {{ 'menu-open' }} @endif">
                            <a href="#"
                                class="nav-link @if (request()->is('admin/cancelordervendoradmin') or request()->is('admin/printercancelorderadmin')) {{ 'active' }} @endif">
                                <i class="fa fa-shopping-cart"></i>
                                <p>
                                    Cancelled Order List
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview"
                                style="@if (request()->is('admin/cancelordervendoradmin') or request()->is('admin/printercancelorderadmin')) {{ 'display:block' }} @endif">

                                <li class="nav-item">
                                    <a href="{{ route('Admin.cancelordervendoradmin') }}"
                                        class="nav-link {{ request()->is('cancelordervendoradmin') ? 'active' : '' }}">
                                        <i class="nav-icon far fa-circle text-info"></i>
                                        <p>
                                            Cancelled Gift Order List
                                        </p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('Admin.printercancelorderadmin') }}"
                                        class="nav-link {{ request()->is('printercancelorderadmin') ? 'active' : '' }}">
                                        <i class="nav-icon far fa-circle text-info"></i>
                                        <p>
                                            Cancelled Postal Order List
                                        </p>
                                    </a>
                                </li>

                            </ul>
                        </li>
                        <li class="nav-item  @if (request()->is('admin/refundordervendoradmin') or request()->is('admin/refundordervendoradmin')) {{ 'menu-open' }} @endif">
                            <a href="#"
                                class="nav-link @if (request()->is('admin/refundordervendoradmin') or request()->is('admin/refundordervendoradmin')) {{ 'active' }} @endif">
                                <i class="fa fa-shopping-cart"></i>
                                <p>
                                    Refunds Order List
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview"
                                style="@if (request()->is('admin/refundordervendoradmin') or request()->is('admin/printerrefundorderadmin')) {{ 'display:block' }} @endif">

                                <li class="nav-item">
                                    <a href="{{ route('Admin.refundordervendoradmin') }}"
                                        class="nav-link {{ request()->is('refundordervendoradmin') ? 'active' : '' }}">
                                        <i class="nav-icon far fa-circle text-info"></i>
                                        <p>
                                            Refunded Gift Order List
                                        </p>
                                    </a>
                                </li>
                                {{-- <li class="nav-item">
                                    <a href="{{ route('Admin.printerrefundorderadmin') }}"
                                        class="nav-link {{ request()->is('printerrefundorderadmin') ? 'active' : '' }}">
                                        <i class="nav-icon far fa-circle text-info"></i>
                                        <p>
                                            Refunded Postal Order List
                                        </p>
                                    </a>
                                </li> --}}

                            </ul>
                        </li>

                        <li class="nav-item  @if (request()->is('admin/giftCompleted') or request()->is('admin/postalCompleted')) {{ 'menu-open' }} @endif">
                            <a href="#"
                                class="nav-link @if (request()->is('admin/giftCompleted') or request()->is('admin/postalCompleted')) {{ 'active' }} @endif">
                                <i class="fa fa-shopping-cart"></i>
                                <p>
                                    Completed Order List
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview"
                                style="@if (request()->is('admin/giftCompleted') or request()->is('admin/postalCompleted')) {{ 'display:block' }} @endif">
                                <li class="nav-item">
                                    <a href="{{ route('Admin.order.giftCompleted') }}" class="nav-link">
                                        <i class="nav-icon far fa-circle text-info"></i>
                                        <p>Gift Completed</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('Admin.order.postalCompleted') }}" class="nav-link">
                                        <i class="nav-icon far fa-circle text-info"></i>
                                        <p>Postal Completed</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item  @if (request()->is('admin/giftPending') or request()->is('admin/postalPending')) {{ 'menu-open' }} @endif">
                            <a href="#"
                                class="nav-link @if (request()->is('admin/giftPending') or request()->is('admin/postalPending')) {{ 'active' }} @endif">
                                <i class="fa fa-shopping-cart"></i>
                                <p>
                                    Pending Order List
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview"
                                style="@if (request()->is('admin/giftPending') or request()->is('admin/postalPending')) {{ 'display:block' }} @endif">
                                <li class="nav-item">
                                    <a href="{{ route('Admin.order.giftPending') }}" class="nav-link">
                                        <i class="nav-icon far fa-circle text-info"></i>
                                        <p>Gift Pending</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('Admin.order.postalPending') }}" class="nav-link">
                                        <i class="nav-icon far fa-circle text-info"></i>
                                        <p>Postal Pending</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item  @if (request()->is('admin/giftReview') or request()->is('admin/postalReview')) {{ 'menu-open' }} @endif">
                            <a href="#"
                                class="nav-link @if (request()->is('admin/giftReview') or request()->is('admin/postalReview')) {{ 'active' }} @endif">
                                <i class="fa fa-shopping-cart"></i>
                                <p>
                                    Rating/Review List
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview"
                                style="@if (request()->is('admin/giftReview') or request()->is('admin/postalReview')) {{ 'display:block' }} @endif">
                                <li class="nav-item">
                                    <a href="{{ route('Admin.order.giftReview') }}" class="nav-link">
                                        <i class="nav-icon far fa-circle text-info"></i>
                                        <p>Gift</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('Admin.order.postalReview') }}" class="nav-link">
                                        <i class="nav-icon far fa-circle text-info"></i>
                                        <p>Postal</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>

                    <br>

                    <ul class="cust-ul">
                        <a>BEAM SETTINGS</a>
                        <li class="nav-item">
                            <a href="{{ route('Admin.banner.index') }}"
                                class="nav-link {{ request()->is('admin/banner') || request()->is('admin/banner') || request()->is('admin/banner/*') ? 'active' : '' }}">
                                <i class="fa fa-picture-o"></i>
                                <p>
                                    Banner List
                                </p>
                            </a>
                        </li>



                        <li class="nav-item">
                            <a href="{{ route('Admin.faq.index') }}"
                                class="nav-link {{ request()->is('admin/faq') || request()->is('admin/faq') || request()->is('admin/faq/*') ? 'active' : '' }}">
                                <i class="fa fa-question-circle"></i>
                                <p>
                                    Faq List
                                </p>
                            </a>
                        </li>
                        @php
                            $now = \Carbon\Carbon::now();
                            // dd($now->toDateString());
                            $new_query = \App\Models\Query::where('is_admin_seen', 0)
                                ->whereDate('created_at', $now->toDateString())
                                ->count();

                        @endphp
                        <li class="nav-item">
                            <a href="{{ route('Admin.query') }}"
                                class="nav-link {{ request()->is('admin/query-list') || request()->is('admin/query') || request()->is('admin/query/*') ? 'active' : '' }}">
                                <i class="fa fa-question-circle-o"></i>
                                <p>FAQ Query List</p>

                                <span class="badge badge-warning"> {{ $new_query }}</span>


                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route('Admin.notification') }}"
                                class="nav-link {{ request()->is('admin/notification-list') || request()->is('admin/notification') || request()->is('admin/notification/*') ? 'active' : '' }}">
                                <i class="fa fa-bell"></i>
                                <p> Push Notification </p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route('Admin.setting') }}"
                                class="nav-link {{ request()->is('admin/setting') ? 'active' : '' }}">
                                <i class="fa fa-list"></i>
                                <p>
                                    Order reminder settings
                                </p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route('Admin.import') }}"
                                class="nav-link {{ request()->is('admin/import') || request()->is('admin/import') || request()->is('admin/import/*') ? 'active' : '' }}">
                                <i class="fa fa-picture-o"></i>
                                <p>
                                    Import
                                </p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{ url('admin/privacy-policy/list') }}"
                                class="nav-link @if (URL::current() == url('admin/privacy-policy/list')) {{ 'active' }} @endif">
                                <i class="fas fa-file-contract"></i>
                                <p>Privacy Policy</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route('Admin.promocode.index') }}"
                                class="nav-link {{ request()->is('admin/promocode') || request()->is('admin/promocode') || request()->is('admin/promocode/*') ? 'active' : '' }}">
                                <i class="fa fa-question-circle"></i>
                                <p>
                                    Promo Code
                                </p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route('Admin.promocodestandregis.index') }}"
                                class="nav-link {{ request()->is('admin/promocodestandregis') || request()->is('admin/promocodestandregis') || request()->is('admin/promocodestandregis/*') ? 'active' : '' }}">
                                <i class="fa fa-question-circle"></i>
                                <p style="text-wrap:initial">
                                    Promo Code Standard/Registered/Postcard
                                </p>
                            </a>
                        </li>
                        {{-- <li class="nav-item">
                            <a href="#" class="nav-link  @if (URL::current() == url('admin/term_and_condition/list'))
                            {{'side-active'}}

                            @elseif(URL::current()==url('admin/privacy-policy/list'))
                            {{'side-active'}}

                            @elseif(URL::current()==url('admin/refund-policy/list'))
                            {{'side-active'}}

                            @endif">
                                <i class="fas fa-file-contract"></i>
                                <p>
                                    Terms and Conditions
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">

                                {{-- <li class="nav-item">
                                    <a href="{{url('admin/term_and_condition/list')}}" class="nav-link @if (URL::current() == url('admin/term_and_condition/list')) {{'side-active'}} @endif">
                                        <i class="nav-icon far fa-circle text-info"></i>
                                        <p>Terms and Conditions </p>
                                    </a>
                                </li>

                                 <li class="nav-item">
                                    <a href="{{url('admin/privacy-policy/list')}}" class="nav-link @if (URL::current() == url('admin/privacy-policy/list')) {{'side-active'}} @endif">
                                        <i class="nav-icon far fa-circle text-info"></i>
                                        <p>Privacy Policy</p>
                                    </a>
                                </li>
                                {{-- <li class="nav-item">
                                    <a href="{{url('admin/refund-policy/list')}}" class="nav-link @if (URL::current() == url('admin/refund-policy/list')) {{'side-active'}} @endif">
                                        <i class="nav-icon far fa-circle text-info"></i>
                                        <p>Refund Policy</p>
                                    </a>
                                </li>

                            </ul>
                        </li> --}}
                    </ul>

                    <br>

                    <ul class="cust-ul">
                        <a>CHECKOUT SETTINGS</a>
                        <li class="nav-item">
                            <a href="{{ route('Admin.Paper.List') }}"
                                class="nav-link {{ request()->is('admin/paper-list') || request()->is('admin/paper-list') || request()->is('admin/paper-list/*') ? 'active' : '' }}">
                                <i class="fa fa-file-alt"></i>
                                <p>
                                    Other Services
                                </p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route('Admin.miscellaneous.index') }}"
                                class="nav-link {{ request()->is('admin/miscellaneous') || request()->is('admin/miscellaneous') || request()->is('admin/miscellaneous/*') ? 'active' : '' }}">
                                <i class="fa fa-list"></i>
                                <p>
                                    Miscellaneous
                                </p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route('Admin.emailTemplateIndex') }}"
                                class="nav-link {{ request()->is('admin/emailTemplateIndex') || request()->is('admin/emailTemplateIndex/*') ? 'active' : '' }}">
                                <i class="fa fa-list"></i>
                                <p>
                                    Email Templates
                                </p>
                            </a>
                        </li>

                    </ul>






                    {{-- <li class="nav-item">
                    <a href="{{route('Admin.order.index')}}" class="nav-link {{ (request()->is('admin/order')) || (request()->is('admin/order')) || (request()->is('admin/order/*'))? 'active' : '' }}">
                        <i class="fa fa-list"></i>
                        <p>
                            Order List
                        </p>
                    </a>
                </li> --}}

                    {{-- <li class="nav-item">
                    <a href="{{route('Admin.Envelope.List')}}" class="nav-link {{ (request()->is('admin/envelope-list')) || (request()->is('admin/envelope-list')) || (request()->is('admin/envelope-list/*'))? 'active' : '' }}">
                        <i class="fa fa-envelope"></i>
                        <p>
                            Envelope Color
                        </p>
                    </a>
                </li> --}}

                    {{-- <li class="nav-item">
                    <a href="{{route('Admin.Printtype.List')}}" class="nav-link {{ (request()->is('admin/printtype-list')) || (request()->is('admin/printtype-list')) || (request()->is('admin/printtype-list/*'))? 'active' : '' }}">
                        <i class="fa fa-file-alt"></i>
                        <p>
                           Print Type
                        </p>
                    </a>
                </li> --}}



                    <li class="nav-item" style="margin-bottom:30px;">
                        <a href="{{ route('Admin.Logout') }}" class="nav-link">
                            <i class="fas fa-sign-out-alt nav-icon"></i>
                            <p>Logout</p>
                        </a>
                    </li>

                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
    @endif
    <!-- /.sidebar -->
    @if (auth()->user()->user_type == 4 || auth()->user()->user_type == 5 || auth()->user()->user_type == 6)

        <!-- Brand Logo -->
        <a href="{{ route('Vendor.Dashboard') }}" class="brand-link">
            <img src="{{ asset('public/admin.png') }}" alt="Beam App" class="brand-image img-circle elevation-3"
                style="opacity: .8;">
            <h5>Beam App</h5>
            <span class="brand-text font-weight-light"></span>
        </a>
        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <img src="{{ asset('storage/profile_pictures/' . auth()->user()->profile_image) }}"
                        class="img-circle elevation-2" alt="">
                </div>
                <div class="info">
                    <a href="{{ route('Vendor.Dashboard') }}" class="d-block">{{ auth()->user()->name ?? '' }}</a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                    data-accordion="false">
                    <!-- Add icons to the links using the .nav-icon class
                        with font-awesome or any other icon font library -->
                    <li
                        class="nav-item {{ request()->is('vendor/profile') || request()->is('vendor/change-password') || request()->is('vendor/get-profile') ? 'menu-open' : '' }}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-info"></i>
                            <p>Manage Profile
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route('Vendor.Profile') }}"
                                    class="nav-link {{ request()->is('vendor/profile') ? 'active-child' : '' }}">
                                    <i class="fas fa-address-card nav-icon"></i>
                                    <p>Profile</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('Vendor.Change.Password.Form') }}"
                                    class="nav-link {{ request()->is('admin/vendor-password') ? 'active-child' : '' }}">
                                    <i class="fas fa-key nav-icon"></i>
                                    <p>Change Password</p>
                                </a>
                            </li>

                        </ul>
                    </li>

                    {{-- <li class="nav-item">
                        <a href="#" class="nav-link @if (URL::current() == url('/vendor/product'))
                        {{'side-active'}}

                        @elseif(URL::current()==url('vendor/product/inactive'))
                        {{'side-active'}}

                        @endif">
                        <i class="nav-icon fa fa-product-hunt"></i>
                            <p>
                                Postal Product
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">

                            <li class="nav-item">
                                <a href="{{route('Vendor.product.index')}}" class="nav-link">
                                    <i class="nav-icon far fa-circle text-info"></i>
                                    <p>Active Products</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('vendor/product/inactive')}}" class="nav-link">
                                    <i class="nav-icon far fa-circle text-info"></i>
                                    <p>In-Active Products</p>
                                </a>
                            </li>

                        </ul>
                    </li> --}}
                    @if (auth()->user()->user_type == 4)
                        <li class="nav-item  @if (request()->is('vendor/gifthub/product') or request()->is('vendor/gifthub/product/inactive')) {{ 'menu-open' }} @endif">
                            <a href="#"
                                class="nav-link @if (request()->is('vendor/gifthub/product') or request()->is('vendor/gifthub/product/inactive')) {{ 'active' }} @endif"">
                                <i class="nav-icon fas fa-gift"></i>
                                <p>
                                    Gifthub Product
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview"
                                style="@if (request()->is('vendor/gifthub/product') or request()->is('vendor/gifthub/product/inactive')) {{ 'display:block' }} @endif">
                                <li class="nav-item">
                                    <a href="{{ route('Vendor.gifthub.product.index') }}" class="nav-link">
                                        <i class="nav-icon far fa-circle text-info"></i>
                                        <p>Active Products</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ url('vendor/gifthub/product/inactive') }}" class="nav-link">
                                        <i class="nav-icon far fa-circle text-info"></i>
                                        <p>In-Active Products</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    @endif



                    {{-- <li class="nav-item">
                        <a href="{{route('Vendor.product.index')}}" class="nav-link {{ (request()->is('vendor/product')) || (request()->is('vendor/product')) || (request()->is('vendor/product/*'))? 'active' : '' }}">
                            <i class="fa fa-product-hunt"></i>
                            <p>
                                Product List
                            </p>
                        </a>
                    </li> --}}
                    {{-- <li class="nav-item">
                        <a href="{{route('Vendor.postcard.index')}}" class="nav-link {{ (request()->is('vendor/postcard')) || (request()->is('vendor/postcard')) || (request()->is('vendor/postcard/*'))? 'active' : '' }}">
                            <i class="fa fa-product-hunt"></i>
                            <p>
                                Postcard List
                            </p>
                        </a>
                    </li> --}}
                    <br>
                    <ul class="cust-ul">
                        <a>ORDER SUMMARY</a>


                        @if (auth()->user()->user_type == 4 || auth()->user()->user_type == 1)
                            <li class="nav-item">
                                <a href="{{ route('Vendor.order.index') }}"
                                    class="nav-link {{ request()->is('vendor/order') || request()->is('vendor/order') || request()->is('vendor/order/*') ? 'active' : '' }}">
                                    <i class="fa fa-list"></i>
                                    <p>
                                        Order List
                                    </p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{ route('Vendor.cancelordervendor') }}"
                                    class="nav-link {{ request()->is('vendor/cancelordervendor') ? 'active' : '' }}">
                                    <i class="fa fa-list"></i>
                                    <p>
                                        Cancelled Order List
                                    </p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{ route('Vendor.giftPendingOrderVendor') }}"
                                    class="nav-link {{ request()->is('vendor/giftPendingOrderVendor') ? 'active' : '' }}">
                                    <i class="fa fa-list"></i>
                                    <p>
                                        Pending Order List
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('Vendor.refundordervendor') }}"
                                    class="nav-link {{ request()->is('vendor/refundordervendor') ? 'active' : '' }}">
                                    <i class="fa fa-list"></i>
                                    <p>
                                        Refund Order list
                                    </p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{ route('Vendor.ratingReviewGift') }}"
                                    class="nav-link {{ request()->is('vendor/ratingReviewGift') ? 'active' : '' }}">
                                    <i class="fa fa-list"></i>
                                    <p>
                                        Product Review/Rating
                                    </p>
                                </a>
                            </li>

                        @endif

                        @if (auth()->user()->user_type == 5 || auth()->user()->user_type == 6)
                            <li class="nav-item">
                                <a href="{{ route('Vendor.order.zip') }}"
                                    class="nav-link {{ request()->is('vendor/order/zip') || request()->is('vendor/order/zip') || request()->is('vendor/order/zip/*') ? 'active' : '' }}">
                                    <i class="fa fa-list"></i>
                                    <p>
                                        Order List
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('Vendor.printercancelorder') }}"
                                    class="nav-link {{ request()->is('vendor/printercancelorder') ? 'active' : '' }}">
                                    <i class="fa fa-list"></i>
                                    <p>
                                        Cancelled Order List
                                    </p>
                                </a>
                            </li>
                        @endif

                        @if (auth()->user()->user_type == 5)
                            <li class="nav-item">
                                <a href="{{ route('Vendor.greetingPendingOrderVendor') }}"
                                    class="nav-link {{ request()->is('vendor/greetingPendingOrderVendor') ? 'active' : '' }}">
                                    <i class="fa fa-list"></i>
                                    <p>
                                        Pending Order List
                                    </p>
                                </a>
                            </li>
                        @endif


                        @if (auth()->user()->user_type == 6)
                            <li class="nav-item">
                                <a href="{{ route('Vendor.postalMailPendingOrderVendor') }}"
                                    class="nav-link {{ request()->is('vendor/postalMailPendingOrderVendor') ? 'active' : '' }}">
                                    <i class="fa fa-list"></i>
                                    <p>
                                        Pending Order List
                                    </p>
                                </a>
                            </li>
                        @endif

                        @if (auth()->user()->user_type == 6 or auth()->user()->user_type == 5)
                            <li class="nav-item">
                                <a href="{{ route('Vendor.ratingReviewPostal') }}"
                                    class="nav-link {{ request()->is('vendor/ratingReviewPostal') ? 'active' : '' }}">
                                    <i class="fa fa-list"></i>
                                    <p>
                                        Product Review/Rating
                                    </p>
                                </a>
                            </li>
                        @endif

                        <!--   <li class="nav-item">
                            <a href="{{ route('Admin.Envelope.List') }}" class="nav-link {{ request()->is('admin/envelope-list') || request()->is('admin/envelope-list') || request()->is('admin/envelope-list/*') ? 'active' : '' }}">
                                <i class="fa fa-list"></i>
                                <p>
                                    Envelope Color
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('Admin.Paper.List') }}" class="nav-link {{ request()->is('admin/paper-list') || request()->is('admin/paper-list') || request()->is('admin/paper-list/*') ? 'active' : '' }}">
                                <i class="fa fa-list"></i>
                                <p>
                                Paper Types
                                </p>
                            </a>
                        </li> -->
                    </ul>


                    <li class="nav-item">
                        <a href="{{ route('vendor.logout') }}" class="nav-link">
                            <i class="fas fa-sign-out-alt nav-icon"></i>
                            <p>Logout</p>
                        </a>
                    </li>



                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
    @endif
    <!-- /.sidebar -->
</aside>
