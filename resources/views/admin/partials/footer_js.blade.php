<div class="row">
    <div class="col-md-6">

    </div>
    <div class="col-md-6" >
        <div class="footer">
            <footer>
                Copyright ©️2023 All rights reserved | LetsBeam.it
            </footer>
        </div>
    </div>
</div>
<script src="{{asset('public/plugins/jquery/jquery.min.js')}}"></script>
<script src="{{asset('public/js/lazyload.js')}}"></script>
<script src="{{asset('public/js/moment.min.js')}}"></script>
<script src="{{asset('public/js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.js"></script>
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<!-- Bootstrap -->
<script src="{{asset('public/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{asset('public/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('public/dist/js/adminlte.js')}}"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="{{asset('public/dist/js/demo.js')}}"></script>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="{{asset('public/plugins/jquery-mousewheel/jquery.mousewheel.js')}}"></script>
<script src="{{asset('public/plugins/raphael/raphael.min.js')}}"></script>
<script src="{{asset('public/plugins/jquery-mapael/jquery.mapael.min.js')}}"></script>
<script src="{{asset('public/plugins/jquery-mapael/maps/usa_states.min.js')}}"></script>
<!-- ChartJS -->
<script src="{{asset('public/plugins/chart.js/Chart.min.js')}}"></script>

<script src="{{asset('public/js/message.js')}}"></script>
<script src="{{asset('public/plugins/summernote/summernote-bs4.min.js')}}"></script>
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">


<!-- PAGE SCRIPTS -->
<!-- <script src="{{asset('public/dist/js/pages/dashboard2.js')}}"></script> -->
<script src="{{asset('public/js/message.js')}}"></script>
<script>
    $(function () {
      // Summernote
      //$('#summernote').summernote()
        $('#summernote').summernote({
            height: 300

        });
        $('#summernote').summernote('fontName', 'Roboto','Poppins');

      // CodeMirror
      CodeMirror.fromTextArea(document.getElementById("codeMirrorDemo"), {
        mode: "htmlmixed",
        theme: "monokai"
      });
    })
  </script>
  <script>
    $('#add_recipe').click(function(){
        var image = $('#image').val();
        var recipe_name = $('#recipe_name').val();
        var ingredient_id = $('#ingredient_id').val();

        if($('#image').val() ==""){
           $('#image').css('border','2px solid red');
        }
        else if($('#recipe_name').val() ==""){
           $('#recipe_name').css('border','2px solid red');
            $('#image').css('border','');
        }
        else if($('#ingredient_id').val() ==""){
           $('.fs-label').css('border','2px solid red');
           $('#recipe_name').css('border','');
            $('#image').css('border','');
        }
        else if($('#summernote').val() ==""){
           $('.fs-label').css('border','2px solid red');
           $('#recipe_name').css('border','');
            $('#image').css('border','');
        }else{
            $('#insert_recipe').submit();
        }
    })
</script>

<script src="https://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		// $('.product').multiselect({
		// 	includeSelectAllOption: true,
		// 	enableFiltering: true, // Enable the search feature
		// });
	});
</script> 


<script>
    $('#edit_recipe').click(function(){
        if($('#recipe_name').val() ==""){
            $('#recipe_name').css('border','2px solid red');
        }
        else if($('#ingredient_id').val() ==""){
            $('.fs-label').css('border','2px solid red');
            $('#recipe_name').css('border','');
        }
        else if($('#summernote').val() ==""){
            $('.fs-label').css('border','2px solid red');
            $('#recipe_name').css('border','');
        }else{
            $('#update_recipe').submit();
        }
    })
</script>

@yield('after-scripts')
