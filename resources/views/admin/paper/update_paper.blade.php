@extends('admin.index')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <!--  -->
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('Admin.Dashboard') }}">Home</a></li>
                            <li class="breadcrumb-item active">Update Other Services</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-1"></div>
                    <div class="col-10">
                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">Update Other Services<strong></strong></h3>
                            </div>
                            @include('admin.partials.messages')
                            <form class="form-horizontal" method="post" action="{{ route('Admin.UpdatePaper') }} "
                                enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" value="{{ $paper->id }}" />
                                <div class="card-body">
                                    
                                    
                                    
                                    <!-- ...<div class="form-group row">
                                        <label for="Name" class="col-sm-2 col-form-label">Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="name" name="name" value="{{ $paper->name }}" placeholder="Name" />
                                        </div>
                                    </div> ... -->
                                    
                                    

<div class="form-group row">
    <label for="Name" class="col-sm-2 col-form-label">Name</label>
    <div class="col-sm-10">
        <textarea class="form-control" id="name" name="name" placeholder="Name">{{ $paper->name }}</textarea>
    </div>
</div>




                                    
                                    
                                    
                                    
                                    <div class="form-group row">
                                        <label for="Name" class="col-sm-2 col-form-label">Price</label>
                                        <div class="col-sm-10">
                                            <input type="number" class="form-control" id="price" name="price"
                                                step="any" value="{{ $paper->price }}" placeholder="Price" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="Name" class="col-sm-2 col-form-label">Order By</label>
                                        <div class="col-sm-10">
                                            <input type="number" class="form-control" id="order_by" name="order_by"
                                                step="any" value="{{ $paper->order_by }}" placeholder="order_by" />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="Status" class="col-sm-2 col-form-label">Status</label>
                                        <div class="col-sm-10">
                                            <select class="form-control select2" style="width: 100%;" name="status"
                                                required>
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-info">Update Other Services</button>
                                    <a href="{{ route('Admin.Paper.List') }}" class="btn btn-default float-right">Cancel</a>
                                </div>
                                <div class="col-1">

                                </div>
                            </form>
                        </div>



                    </div>


                </div>
            </div>
        </section>

    </div>

    <!-- </div> -->
@endsection


@section('after-scripts')
    <script src="{{ asset('public/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script>
        $(function() {
            // Summernote
            $('.textarea').summernote()
        })
    </script>
@endsection
@section('after-style')
    <link rel="stylesheet" href="{{ asset('public/plugins/summernote/summernote-bs4.css') }}">
@endsection
