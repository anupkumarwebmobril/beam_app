@foreach($usersubscriptions as $usersubscription)
<div class="modal fade" id="usersubscription-info{{$usersubscription->id}}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Viewing <strong>{{$usersubscription->user_name}}</strong></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Name" class="col-form-label">User Name</label>
                            <div class="col-sm-10">
                                <p>{{$usersubscription->user_name}}</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label for="Email" class="col-form-label">Plan Price</label>
                            <div class="col-sm-10">
                                <p>{{$usersubscription->plan_price}}</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label for="Gender" class="col-form-label">Plan Name</label>
                            <div class="col-sm-10">
                                <p>{{$usersubscription->plan_name}}</p>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Gender" class="col-form-label">Start Date</label>
                            <div class="col-sm-10">
                                <p>{{$usersubscription->start_date}}</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label for="Gender" class="col-form-label">End Date</label>
                            <div class="col-sm-10">
                                <p>{{$usersubscription->end_date}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endforeach