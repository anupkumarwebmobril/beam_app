@extends('admin.index')

@section('after-style')

<link rel="stylesheet" href="{{asset('public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

@endsection

@section('content')

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('Admin.Dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Manage User Subscriptions</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">User Subscriptions List</h3>

                            <!-- <a href="{{route('Admin.Add.UserSubscription')}}" style="float: right;"><i class="fas fa-plus-square" style="font-size:25px;"></i></a> -->
                        </div>

                        @if(session('message'))
                            <div class="alert alert-success" style="padding: 5px 20px;margin-bottom: 5px;">{{ session('message') }}</div>
                        @endif
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="usersubscription-list" class="table table-bordered table-striped" data-order='[[ 0, "asc" ]]'>
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>User Name</th>
                                        <th>Plan Price</th>
                                        <th>Plan Name</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $i=1; @endphp
                                    @if(isset($usersubscriptions) && $usersubscriptions != null)
                                        @foreach($usersubscriptions as $usersubscription)

                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>{{$usersubscription->user_name}}</td>
                                                <td>{{$usersubscription->plan_price}}</td>
                                                <td>{{$usersubscription->plan_name}}</td>
                                                <td>{{$usersubscription->start_date}}</td>
                                                <td>{{$usersubscription->end_date}}</td>

                                                <td style="text-align: center;">
                                                    <a href="javascript:void();" data-toggle="modal" data-target="#usersubscription-info{{$usersubscription->id}}"><i class="fas fa-eye"></i>&nbsp;&nbsp;</a>
                                                    <!-- <a href="{{route('Admin.Get.UserSubscription',[$usersubscription->id])}}"><i class="fas fa-edit"></i>&nbsp;&nbsp;</a>
                                                    <a href="{{route('Admin.Delete.UserSubscription',[$usersubscription->id])}}" onclick="return confirm('Are you sure?')"><i class="fas fa-trash"></i></a> -->
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    @include('admin.usersubscription.view_user_subscription')
</div>

@endsection

@section('after-scripts')
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>

<script>
    $(function() {
        $("#usersubscription-list").DataTable({
            "responsive": true,
            "autoWidth": false,
        });
    });
</script>
<script>
    function approve(id) {
        $.ajax({
            type: 'POST',
            url: "{{url('admin/approve-nurse')}}",
            data: {
                _token: "{{ csrf_token() }}",
                driverId: id,
            },
            success: function(data) {
                location.reload();
            }
        });
    }
    function unapprove(id) {
        $.ajax({
            type: 'POST',
            url: "{{url('admin/unapprove-nurse')}}",
            data: {
                _token: "{{ csrf_token() }}",
                nurseId: id,
            },
            success: function(data) {
                location.reload();
            }
        });
    }
</script>
@endsection
