@extends('admin.index')
<meta name="csrf-token" content="{{ csrf_token() }}" />
@section('content')
    <!-- <div class="wrapper"> -->

    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <!--  -->
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('Admin.Dashboard') }}">Home</a></li>
                            <li class="breadcrumb-item active">Update Category</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-1"></div>
                    <div class="col-10">

                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">Update <strong>{{ $data->name }}</strong></h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            @include('admin.partials.messages')
                           	<form class="form-horizontal" method="post" action="{{route('Admin.category.update',['occasion'=>$data->id])}}" enctype="multipart/form-data" autocomplete="off">
                                @csrf
								@method('PUT')
                                <input type="hidden" name="id" value="{{ $data->id }}" />
                                <div class="card-body">

									<div class="form-group row">
										<label for="Status" class="col-sm-2 col-form-label">Category Type</label>
										<div class="col-sm-10">
											<select class="form-control select2" style="width: 100%;" name="type"
												id="type" required="true">
												<option value="">Select Category type</option>
												<option value="1" {{ !empty($data->type) && $data->type == 1 ? 'selected="selected"' : '' }}>For Postal
												</option>
												<option value="2" {{ !empty($data->type) && $data->type == 2 ? 'selected="selected"' : '' }}>For Gift Hub
												</option>
                                                <option value="3" {{ !empty($data->type) && $data->type == 3 ? 'selected="selected"' : '' }}>Corporate & Charity
												</option>

											</select>
										</div>
									</div>

                                    <div class="form-group row">
                                        <label for="Name" class="col-sm-2 col-form-label">Category Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control name" id="name" name="name"
                                                value="{{ $data->name }}" placeholder="Name" maxlength="40" required />
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label for="Mobile" class="col-sm-2 col-form-label">Category Images</label>
                                        <div class="col-sm-10">
                                            {{-- <input type="file" class="form-control customFileUplIpt" id="image" name="image" accept="image/png"> --}}
                                            <input type="file" class="form-control customFileUplIpt" id="image" name="image" >
												<img src="<?php echo !empty($data->image) ? asset("storage/occasion/".$data->image) : '' ?>"  style="max-height:300px; max-width:300px;"/>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="Status" class="col-sm-2 col-form-label">Status</label>
                                        <div class="col-sm-10">
                                            <select class="form-control select2" style="width: 100%;" name="status"
                                                id="status" required>
                                                <option value="0" {{ $data->status == 0 ? 'selected' : '' }}>Inactive
                                                </option>
                                                <option value="1" {{ $data->status == 1 ? 'selected' : '' }}>Active
                                                </option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-info">Update Category</button>
                                    <a href="{{ route('Admin.category.index') }}"
                                        class="btn btn-default float-right">Cancel</a>
                                </div>
                                <!-- /.card-footer -->
                            </form>
                        </div>

                    </div>
                    <div class="col-1"></div>
                </div>
            </div>
        </section>

    </div>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>


    <!-- </div> -->
@endsection
