@extends('admin.index')
<meta name="csrf-token" content="{{ csrf_token() }}" />
@section('content')
    <!-- <div class="wrapper"> -->

    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <!--  -->
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('Admin.Dashboard') }}">Home</a></li>
                            <li class="breadcrumb-item active">Update Formal</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-1"></div>
                    <div class="col-10">

                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">Update <strong>{{ $data->name }}</strong></h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            @include('admin.partials.messages')
                           	<form class="form-horizontal" method="post" action="{{route('Admin.formal.update',['MailType'=>$data->id])}}" enctype="multipart/form-data" autocomplete="off">
                                @csrf
								@method('PUT')
                                <input type="hidden" name="id" value="{{ $data->id }}" />
                                <div class="card-body">



                                    <div class="form-group row">
                                        <label for="Name" class="col-sm-2 col-form-label">Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control name" id="name" name="name"
                                                value="{{ $data->name }}" placeholder="Name" maxlength="40" />
                                        </div>
                                    </div>
                                    
                                    @if ($data->id == 3)                                                                            
                                        {{-- <div class="form-group row">
                                            <label for="Name" class="col-sm-2 col-form-label">Black & White Price</label>
                                            <div class="col-sm-10">
                                                <input type="number" class="form-control name" id="price" name="price"
                                                value="{{ $data->price }}" placeholder="Name" maxlength="40" step="any" required="true"/>
                                            </div>
                                        </div> --}}
                                    @else

                                        <div class="form-group row">
                                            <label for="Name" class="col-sm-2 col-form-label">Black & White Price</label>
                                            <div class="col-sm-10">
                                                <input type="number" class="form-control name" id="price" name="price"
                                                value="{{ $data->price }}" placeholder="Name" maxlength="40" step="any" required="true"/>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="Name" class="col-sm-2 col-form-label">Coloured Price</label>
                                            <div class="col-sm-10">
                                                <input type="number" class="form-control name" id="colored_price" name="colored_price"
                                                value="{{ $data->colored_price }}" placeholder="Colored Price" maxlength="40" step="any" required="true"/>
                                            </div>
                                        </div>
                                    @endif

                                    @if ($data->id == 1 OR $data->id == 2)
                                        <div class="form-group row">
                                            <label for="Email" class="col-sm-2 col-form-label">Shipping Fee</label>
                                            <div class="col-sm-10">
                                                <input id="shipping_fee" name="shipping_fee" type="number" class="form-control" required="true" value="{{ !empty($data->shipping_fee) ? $data->shipping_fee : ''}}" step=".01">
                                            </div>
                                        </div>
                                    @endif


                                    <div class="form-group row">
                                        <label for="Mobile" class="col-sm-2 col-form-label">Images</label>
                                        <div class="col-sm-10">
                                            <input type="file" class="form-control customFileUplIpt" id="image" name="image" accept="image/png">
												<img src="<?php echo !empty($data->image) ? asset("storage/mail_type/".$data->image) : '' ?>"  style="width:150px; height:100px; max-height:200px; max-width:200px;"/>
                                        </div>
                                    </div>

                                    {{-- <div class="form-group row">
                                        <label for="Status" class="col-sm-2 col-form-label">Status</label>
                                        <div class="col-sm-10">
                                            <select class="form-control select2" style="width: 100%;" name="status"
                                                id="status" required>
                                                <option value="0" {{ $data->status == 0 ? 'selected' : '' }}>Inactive
                                                </option>
                                                <option value="1" {{ $data->status == 1 ? 'selected' : '' }}>Active
                                                </option>
                                            </select>
                                        </div>
                                    </div> --}}

                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-info">Update Formal</button>
                                    <a href="{{ route('Admin.formal.index') }}"
                                        class="btn btn-default float-right">Cancel</a>
                                </div>
                                <!-- /.card-footer -->
                            </form>
                        </div>

                    </div>
                    <div class="col-1"></div>
                </div>
            </div>
        </section>

    </div>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>


    <!-- </div> -->
@endsection
