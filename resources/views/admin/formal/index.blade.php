@extends('admin.index')

@section('after-style')

<link rel="stylesheet" href="{{asset('public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

@endsection

@section('content')

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('Admin.Dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Manage Formal</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Formal List</h3>

                             {{-- <a href="{{route('Admin.formal.create')}}" style="float: right;"><i class="fas fa-plus-square" style="font-size:25px;"></i></a> --}}
                        </div>


                        @if(session('message'))
                            <div class="alert alert-success" style="padding: 5px 20px;margin-bottom: 5px;">{{ session('message') }}</div>
                        @endif
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="user-list" class="table table-bordered table-striped" data-order='[[ 0, "asc" ]]'>
                                <thead>
                                    <tr>
                                        <th>Sr No.</th>
										<th>Name</th>
										<th>Black & White Price</th>
                                        <th>Coloured Price</th>
                                        <th>Icon</th>
                                        <th>Shipping Fee</th>
										<th>Status</th>
										<th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php $i=1; @endphp

                                    @if(isset($data) && $data != null)
                                        @foreach($data as $val)

                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>{{$val->name}}</td>
                                                <td><p class="text-success">RM:{{$val->price}}</p></td>
                                                <td><p class="text-success">RM:{{$val->colored_price}}</p></td>
                                                <td><img src="<?php echo !empty($val->image) ? asset("storage/mail_type/".$val->image) : '' ?>"  style="width:150px; height:100px; max-height:200px; max-width:200px;"/></td>
                                                <td>{{$val->shipping_fee}}</td>
                                                <td>{{($val->status == 1) ? "Active" : "In-Active"}}</td>


                                                <td style="text-align: center;">
                                                    {{-- <a href="javascript:void();" data-toggle="modal" data-target="#user-info{{$val->id}}"><i class="fas fa-eye"></i>&nbsp;&nbsp;</a> --}}
                                                    <a href="{{route('Admin.formal.edit',[$val->id])}}"><i class="fas fa-edit"></i>&nbsp;&nbsp;</a>
                                                    {{-- <a href="javascript:void(0);" class="rowDelete" data-id="{{$val->id}}" data-token="{{ csrf_token() }}"><i class="fas fa-trash" ></i></a> --}}

													{{-- <a href="vendorproduct/edit/{{$val->id}}" class="me-3 text-primary" data-bs-toggle="tooltip" data-placement="top" title="Edit" ><i class="material-icons">mode_edit</i></a>
													<a href="javascript:void(0);" class="text-danger rowDelete" data-bs-toggle="tooltip" data-placement="top" title="Delete" data-id="{{$val->id}}" data-token="{{ csrf_token() }}"><i class="material-icons">delete</i></a> --}}
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    @include('admin.product.view_product')
</div>

@endsection

@section('after-scripts')
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>

<script>
    $(function() {
        $("#user-list").DataTable({
            "responsive": true,
            "autoWidth": false,
        });
    });

    $('body').on('click', '.rowDelete', function() {
      	var APP_URL = {!! json_encode(url('/')) !!};
        var id = $(this).data("id");
        var token = $(this).data("token");

        $(this).closest('tr').remove();
	     $.ajax({
           // url: APP_URL+'/vendorproduct/delete/'+id,
            url: APP_URL+'/admin/formal/delete/'+id,
            type: "POST",
            data: {
                "id": id,
                "_method":'DELETE',
                "_token": token
            },
            success: function (response) {
                console.log('data deleted successfully');
                //window.location.href= APP_URL+'/vendorproduct';
                location.reload();
            },
            error: function(response) {
                console.log("IN ERRROR "+response);
                // window.location.href= window.location.pathname;
            }
        });

    });
</script>
@endsection
