@foreach($doctors as $doctor)
<div class="modal fade" id="doctor-info{{$doctor->id}}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Viewing <strong>{{$doctor->name}}</strong></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Name" class="col-form-label">Name</label>
                            <div class="col-sm-10">
                                <p>{{$doctor->name}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Email" class="col-form-label">Email</label>
                            <div class="col-sm-10">
                                <p>{{$doctor->email}}</p>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
                <div class="row">

                    <div class="col-4">
                        <div class="form-group">
                            <label for="Gender" class="col-form-label">Mobile</label>
                            <div class="col-sm-10">
                                <p>{{$doctor->mobile_number}}</p>
                            </div>
                        </div>
                    </div>

                    

                    <div class="col-4">
                        <div class="form-group">
                            <label for="Status" class="col-form-label">Status</label>
                            <div class="col-sm-10">
                                @if($doctor->status == 1)
                                    <p style="color:green">Active</p>
                                @elseif($doctor->status == 0)
                                    <p style="color:red">In-active</p>
                                @endif                                
                            </div>
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label for="Approved" class="col-form-label">Approved/Unapproved</label>
                            <div class="col-sm-10">
                                @if($doctor->is_approved == 1)
                                    <p style="color:green">Approved</p>
                                @elseif($doctor->is_approved == 0)
                                    <p style="color:red">Unapproved</p>
                                @endif                                
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endforeach