@extends('admin.index')
@section('content')

 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{$title}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Home</a></li>
              <li class="breadcrumb-item active">{{$title}}</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-outline card-info">
            <div class="card-header">
              <h3 class="card-title">
                {{$title}} Manage
              </h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
               @if(Session::has('message'))
                    <div class="alert alert-danger" role="alert">{{Session::get('message')}}</div>

                  @elseif(Session::has('success'))
                   <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
                @endif
              <form method="post" action="{{url('admin/update_refund')}}">
                @csrf
                <textarea id="summernote" name="refund_policy"><?= $details->refund_policy?></textarea>
                <br>
                  <center><button class="btn btn-success">Update</button></center>
              </form>
            </div>
          </div>
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->

    </section>
    <!-- /.content -->
  </div>
  @endsection
<!-- /.content-wrapper -->
