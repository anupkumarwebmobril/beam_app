@extends('admin.index')
<meta name="csrf-token" content="{{ csrf_token() }}" />
@section('after-style')
    <style>
        a {

            color: inherit;
        }
    </style>
@endsection
@php
    $userType = auth()->user()->user_type;
@endphp
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header" style="margin-left:10px;">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">

                        @if (auth()->user()->user_type == 1)
                            <h1 class="m-0 text-dark">Admin Dashboard</h1>
                        @elseif (auth()->user()->user_type == 4)
                            <h1 class="m-0 text-dark">Merchant Provider Dashboard</h1>
                        @elseif (auth()->user()->user_type == 5)
                            <h1 class="m-0 text-dark">Printer Vendor Dashboard</h1>
                        @elseif (auth()->user()->user_type == 6)
                            <h1 class="m-0 text-dark">Secure Vendor Dashboard</h1>
                        @endif

                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">{{ auth()->user()->user_type == 1 ? 'Admin' : 'Vendor' }}
                                Dashboard</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content" style="margin-left:10px;">
            <div class="container-fluid">
                <!-- Info boxes -->
                <div class="row">
                    @if (Auth::user()->user_type == 4)
                        <div class="col-12 col-sm-6 col-md-4">
                            {{-- <a href="{{route('Admin.Users.List')}}"> --}}
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fa fa-clock-o"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">TODAY'S ORDERS</span>
                                    <span class="info-box-number">Order Quantity : {{ $gifthub ?? 00 }}</span>
                                    <span class="info-box-number" id="gifthub_amount_today"></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            {{-- </a> --}}
                            <!-- /.info-box -->
                        </div>
                    @endif

                   
                </div>

                <div class="row">
                    @if (Auth::user()->user_type == 4)
                        

                        <div class="col-12 col-sm-6 col-md-3">
                            {{-- <a href="{{route('Admin.Users.List')}}"> --}}
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fa fa-calendar-o"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">WEEKLY ORDER SUMMARY</span>
                                    <span class="info-box-number" id="gifthub_weekly"></span>
                                    <span class="info-box-number" id="gifthub_weekly_amount"></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            {{-- </a> --}}
                            <!-- /.info-box -->
                        </div>

                        <div class="col-12 col-sm-6 col-md-3">
                            {{-- <a href="{{route('Admin.Users.List')}}"> --}}
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fa fa-calendar"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">MONTHLY ORDER SUMMARY</span>
                                    <span class="info-box-number" id="gifthub_monthly"></span>
                                    <span class="info-box-number" id="gifthub_monthly_amount"></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            {{-- </a> --}}
                            <!-- /.info-box -->
                        </div>

                        <div class="col-12 col-sm-6 col-md-3">
                            {{-- <a href="{{route('Admin.Users.List')}}"> --}}
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">TOTAL ORDER TO-DATE</span>
                                    <span class="info-box-number" id="total_order_recieved_till_date"></span>

                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            {{-- </a> --}}
                            <!-- /.info-box -->
                        </div>
                    @endif
                </div>


                @if (Auth::user()->user_type == 5)
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-3">
                            {{-- <a href="{{route('Admin.Users.List')}}"> --}}
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fa fa-clock-o"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">TODAY'S ORDERS</span>
                                    <span class="info-box-text">POSTCARD</span>
                                    <span class="info-box-number">Order Quantity : {{ $postcard ?? 00 }}</span>
                                    <span class="info-box-number" id="postcard_amount_today"></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            {{-- </a> --}}
                            <!-- /.info-box -->
                        </div>

                        <div class="col-12 col-sm-6 col-md-3">
                            {{-- <a href="{{route('Admin.Users.List')}}"> --}}
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fa fa-clock-o"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">TODAY'S ORDERS</span>
                                    <span class="info-box-text">GREETINGS</span>
                                    <span class="info-box-number">Order Quantity : {{ $greetings ?? 00 }}</span>
                                    <span class="info-box-number" id="greetings_amount_today"></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            {{-- </a> --}}
                            <!-- /.info-box -->
                        </div>

                        <div class="col-12 col-sm-6 col-md-3">
                            {{-- <a href="{{route('Admin.Users.List')}}"> --}}
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fa fa-calendar-o"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">WEEKLY ORDER SUMMARY</span>
                                    <span class="info-box-text">POSTCARD</span>
                                    <span class="info-box-number" id="postcard_weekly"></span>
                                    <span class="info-box-number" id="postcard_weekly_amount"></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            {{-- </a> --}}
                            <!-- /.info-box -->
                        </div>

                        <div class="col-12 col-sm-6 col-md-3">
                            {{-- <a href="{{route('Admin.Users.List')}}"> --}}
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fa fa-calendar-o"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">WEEKLY ORDER SUMMARY</span>
                                    <span class="info-box-text">GREETINGS</span>
                                    <span class="info-box-number" id="greetings_weekly"></span>
                                    <span class="info-box-number" id="greetings_weekly_amount"></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            {{-- </a> --}}
                            <!-- /.info-box -->
                        </div>



                        <div class="col-12 col-sm-6 col-md-3">
                            {{-- <a href="{{route('Admin.Users.List')}}"> --}}
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fa fa-calendar"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">MONTHLY ORDER SUMMARY</span>
                                    <span class="info-box-text">POSTCARD</span>
                                    <span class="info-box-number" id="postcard_monthly"></span>
                                    <span class="info-box-number" id="postcard_monthly_amount"></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            {{-- </a> --}}
                            <!-- /.info-box -->
                        </div>



                        <div class="col-12 col-sm-6 col-md-3">
                            {{-- <a href="{{route('Admin.Users.List')}}"> --}}
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fa fa-calendar"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">MONTHLY ORDER SUMMARY</span>
                                    <span class="info-box-text">GREETINGS</span>
                                    <span class="info-box-number" id="greetings_monthly"></span>
                                    <span class="info-box-number" id="greetings_monthly_amount"></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            {{-- </a> --}}
                            <!-- /.info-box -->
                        </div>

                        <div class="col-12 col-sm-6 col-md-3">
                            {{-- <a href="{{route('Admin.Users.List')}}"> --}}
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fa fa-line-chart"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Monthly Total Sales</span>
                                    <span class="info-box-number" id="monthly_total_amount"></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            {{-- </a> --}}
                            <!-- /.info-box -->
                        </div>

                    </div>

                @endif

                <div class="row">                    

                    @if (Auth::user()->user_type == 6)

                        <div class="col-12 col-sm-6 col-md-3">
                            {{-- <a href="{{route('Admin.Users.List')}}"> --}}
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fa fa-clock-o"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">TODAY'S ORDERS</span>
                                    <span class="info-box-number">Today's Orders : {{ $postal_mails ?? 00 }}</span>
                                    <span class="info-box-number" id="postal_mails_amount_today"></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            {{-- </a> --}}
                            <!-- /.info-box -->
                        </div>
                        
                        <!--Weekly-->
                        <div class="col-12 col-sm-6 col-md-3">
                            {{-- <a href="{{route('Admin.Users.List')}}"> --}}
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fa fa-calendar-o"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">WEEKLY ORDER SUMMARY</span>
                                    <span class="info-box-number" id="postal_mails_weekly"></span>
                                    <span class="info-box-number" id="postal_mails_weekly_amount"></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            {{-- </a> --}}
                            <!-- /.info-box -->
                        </div>

                        <!--monthly-->
                        <div class="col-12 col-sm-6 col-md-3">
                            {{-- <a href="{{route('Admin.Users.List')}}"> --}}
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fa fa-calendar"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">MONTHLY ORDER SUMMARY</span>
                                    <span class="info-box-number" id="postal_mails_monthly"></span>
                                    <span class="info-box-number" id="postal_mails_monthly_amount"></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            {{-- </a> --}}
                            <!-- /.info-box -->
                        </div>
                    @endif
                </div>
                
                <div class="row">
                    <div class="col-12 col-sm-6 col-md-4" >
                        <div class="info-box" style="background-color:#f37171">
                            <span class="info-box-icon bg-info elevation-1"><i class="fa fa-exclamation-triangle"></i></span>

                            <div class="info-box-content" style="background-color:#f37171">
                                <span class="info-box-text">Total Pending Orders</span>
                                <span class="info-box-number" id="totalPedningOrder">0</span>
                            </div>

                        </div>
                    </div>                    
                </div>

                <div class="row">                   

                    @if (Auth::user()->user_type == 5)
                        

                        <br>


                        <div class="col-12 col-sm-6 col-md-3">
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-times"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Total Cancel Orders</span>
                                    <span class="info-box-number" id="totalCancelOrder">0</span>
                                </div>

                            </div>
                        </div>

                        {{-- <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box">
                            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Total Available Products Postal</span>
                                <span class="info-box-number" id="postalProductCount">0</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box">
                            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Total Available Products Postcard</span>
                                <span class="info-box-number" id="postcardProductCount">0</span>
                            </div>
                        </div>
                    </div>



                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box">
                            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Active Products Postal</span>
                                <span class="info-box-number" id="postalProductActiveCount">0</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box">
                            <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">In-Active Products Postal</span>
                                <span class="info-box-number" id="postalProductInActiveCount">0</span>
                            </div>
                        </div>
                    </div> --}}
                    @endif

                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box">
                            <span class="info-box-icon bg-info elevation-1"><i class="fa fa-truck"></i></span>

                            <div class="info-box-content" style="max-width:100%">
                                <span class="info-box-text">Total Orders Shipped</span>
                                <span class="info-box-text">and Awaiting Delivery</span>
                                <span class="info-box-number" id="totalORderShippedAndAwaitingForDelivery">0</span>
                            </div>

                        </div>
                    </div>

                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="info-box">
                            <span class="info-box-icon bg-info elevation-1"><i class="fa fa-thumbs-up"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Total Orders Delivered</span>
                                <span class="info-box-number" id="totalORderDelivered">0</span>
                            </div>

                        </div>
                    </div>

                    
                    @if (Auth::user()->user_type == 4)
                        <div class="col-12 col-sm-6 col-md-3">
                            <div class="info-box" style="background-color:#191B1C">
                                <span class="info-box-icon bg-info elevation-1"><i class="fa fa-thumbs-down"></i></span>

                                <div class="info-box-content" style="background-color:#191B1C; color:white">
                                    <span class="info-box-text">Return / Refund request</span>
                                    <span class="info-box-number" id="totalReturnRefund">0</span>
                                </div>

                            </div>
                        </div>
                    @endif
                </div>

                
                <div class="row">
                    @if (Auth::user()->user_type == 4)
                        <div class="col-12 col-sm-6 col-md-3">
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fa fa-shopping-basket"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Total Product In Store</span>
                                    <span class="info-box-number" id="gifthubProductCount">0</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-6 col-md-3">
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fa fa-battery-full"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Active Products</span>
                                    <span class="info-box-number" id="gifthubActiveProductCount">0</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-sm-6 col-md-3">
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fa fa-battery-empty"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">In-Active Products</span>
                                    <span class="info-box-number" id="gifthubInActiveProductCount">0</span>
                                </div>
                            </div>
                        </div>
                    @endif



                </div>

                @if (Auth::user()->user_type == 4)
                    {{-- <div class="row">
                        <div class="col-12"><b>Top 10 Selling Products Postal</b> </div>
                        <div class="col-12">
                            <table>
                                 //foreach($gifthubTopTenSellingProducts as $giftPro){ 
                                <tr>
                                    <td>{{// $giftPro[0] }}</td>
                                </tr>
                                 //} 

                            </table>
                        </div>
                    </div> --}}
                @endif
                {{-- <div class="row">
                    <div class="col-sm">
                        <div id="piechart" style="width: 100%; height: 500px;"></div>
                    </div>
                    <div class="col-sm">
                        <div id="piechart1" style="width: 100%; height: 500px;"></div>
                    </div>
                </div> --}}

                @if (Auth::user()->user_type == 4)
                    <div class="row">
                        <div class="col-sm">
                            {{-- <button id="change-chart">Change to Classic</button> --}}
                            <br><br>
                            <div style="width: 1000px; height: 100%; padding:20px;"><div id="chart_div" style="background:white; width: 108%;padding:44px;"></div></div>
                        </div>
                    </div>
                @endif

                @if (Auth::user()->user_type == 5)
                    <div class="row">
                        <div class="col-sm">
                            {{-- <button id="change-chart">Change to Classic</button> --}}
                            <br><br>
                            <div style="width: 1000px; height: 100%; padding:20px;"><div id="postcard" style="background:white; width: 108%;padding:44px;"></div></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm">
                            {{-- <button id="change-chart">Change to Classic</button> --}}
                            <br><br>
                            <div style="width: 1000px; height: 100%; padding:20px;"><div id="greeting_graph_cust" style="background:white; width: 108%;padding:44px;"></div></div>
                        </div>
                    </div>
                @endif
                
                @if (Auth::user()->user_type == 6)
                    <div class="row">
                        <div class="col-sm">
                            {{-- <button id="change-chart">Change to Classic</button> --}}
                            <br><br>
                            <div style="width: 1000px; height: 100%; padding:20px;"><div id="secure_printer" style="background:white; width: 108%;padding:44px;"></div></div>
                        </div>
                    </div>
                @endif
                

                @if (Auth::user()->user_type == 4)
                    <div class="row">
                        <div class="col-sm">
                            <div id="piechart" style="width: 900px; height: 500px; padding:20px;"></div>
                        </div>
                    </div>
                @endif
                <!-- /.row -->
            </div>
            <!--/. container-fluid -->
        </section>

        <!-- /.content -->
    </div>
@endsection

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script>
    $(document).ready(function() {
        weeklyOrderTotal();
        orderData();
        productDataForDashboard();
        monthyreporter();
        all_product_count();
        monthyreportergreeting();
        postcard();


    });

    function weeklyOrderTotal() {
        var APP_URL = {!! json_encode(url('/')) !!};

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: APP_URL + '/vendor/weeklyOrderTotal/',
            //url: APP_URL+product.categoryList+id,

            type: "POST",
            data: {
                "id": null,
                "_method": 'POST',
            },
            success: function(response) {
                if (response.status == true) {
                    <?php $authUser = Auth::user(); if($authUser->user_type == 4){ ?>
                    $('#gifthub_amount_today').html("Total Sales : RM " + response.data
                        .gifthub_amount_today);

                    $('#gifthub_weekly').html("Order Quantity : " + response.data.gifthub_weekly);
                    $('#gifthub_weekly_amount').html("Total Sales : RM " + response.data
                        .gifthub_weekly_amount);

                    $('#gifthub_monthly').html("Order Quantity : " + response.data.gifthub_monthly);
                    $('#gifthub_monthly_amount').html("Total Sales : RM " + response.data
                        .gifthub_monthly_amount);

                    $('#total_order_recieved_till_date').html("Order Quantity : " + response
                        .data.total_order_recieved_till_date);

                    <?php } ?>

                    <?php if($authUser->user_type == 5){ ?>

                    $('#postcard_weekly').html("Order Quantity : " + response.data.postcard_weekly);
                    $('#postcard_weekly_amount').html("Total Sales : RM " + response.data
                        .postcard_weekly_amount);


                    $('#greetings_weekly').html("Order Quantity : " + response.data.greetings_weekly);
                    $('#greetings_weekly_amount').html("Total Sales : RM " + response.data
                        .greetings_weekly_amount);


                    //today toal amount
                    $('#postcard_amount_today').html("Total Sales : RM " + response.data
                        .postcard_amount_today);

                    $('#greetings_amount_today').html("Total Sales : RM " + response.data
                        .greetings_amount_today);

                    //monthly

                    $('#postcard_monthly').html("Order Quantity : " + response.data.postcard_monthly);
                    $('#postcard_monthly_amount').html("Total Sales : RM " + response.data
                        .postcard_monthly_amount);


                    $('#greetings_monthly').html("Order Quantity : " + response.data.greetings_monthly);
                    $('#greetings_monthly_amount').html("Total Sales : RM " + response.data
                        .greetings_monthly_amount);

                    $('#monthly_total_amount').html(response.data.monthly_total_amount);

                    $('#design_request').html(response.data.design_request);
                    <?php } ?>

                    <?php if($authUser->user_type == 6){ ?>
                    $('#postal_mails_weekly').html("Order Quantity : " + response.data.postal_mails_weekly);

                    $('#postal_mails_weekly_amount').html("Total Sales : RM " + response.data
                        .postal_mails_weekly_amount);
                    $('#postal_mails_amount_today').html("Total Sales : RM " + response.data
                        .postal_mails_amount_today);

                    $('#postal_mails_monthly').html("Order Quantity : " + response.data
                        .postal_mails_monthly);
                    $('#postal_mails_monthly_amount').html("Total Sales : RM " + response.data
                        .postal_mails_monthly_amount);

                    <?php } ?>
                }
            },
            error: function(data) {
                //console.log("IN ERRROR "+data);
            }
        });
    }

    function monthyreporter() {
        var APP_URL = {!! json_encode(url('/')) !!};

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: APP_URL + '/vendor/monthlyreport/',
            //url: APP_URL+product.categoryList+id,

            type: "POST",
            data: {
                "id": null,
                "_method": 'POST',
            },
            success: function(response) {
                if (response.status == true) {


                    <?php $authUser = Auth::user(); if($authUser->user_type == 4){ ?>


                    $('#chart_div').html(response.data.monthlyreport);


                    // sagar code for chart
                    google.charts.load('current', {
                        'packages': ['bar']
                    });
                    google.charts.setOnLoadCallback(drawChart);

                    function drawChart() {
                        // var data = google.visualization.arrayToDataTable([
                        //     ['Date', 'qty', 'sell'],
                        //     ['2022-12-1', 120, 400],
                        //     ['2022-12-2', 343, 460],
                        //     ['2022-12-3', 41, 1120],
                        //     ['2022-12-4', 67, 540]
                        // ]);
                        var data = google.visualization.arrayToDataTable(response.data.monthlyreport);


                        var options = {
                            chart: {
                                title: 'Monthly Product Sales Overview',
                                subtitle: '(Date, Qty, Sales)',
                            },
                            bars: 'vertical',
                            vAxis: {
                                format: 'decimal'
                            },

                            height: 400,
                            colors: ['#1b9e77', '#d95f02', '#7570b3']
                        };

                        var chart = new google.charts.Bar(document.getElementById('chart_div'));

                        chart.draw(data, google.charts.Bar.convertOptions(options));

                        var btns = document.getElementById('btn-group');

                        btns.onclick = function(e) {

                            if (e.target.tagName === 'BUTTON') {
                                options.vAxis.format = e.target.id === 'none' ? '' : e.target.id;
                                chart.draw(data, google.charts.Bar.convertOptions(options));
                            }
                        }
                    }
                    <?php } ?>



                }
            },
            error: function(data) {
                //console.log("IN ERRROR "+data);
            }
        });
    }

    function productDataForDashboard() {
        var APP_URL = {!! json_encode(url('/')) !!};

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: APP_URL + '/vendor/productDataForDashboard/',
            //url: APP_URL+product.categoryList+id,

            type: "POST",
            data: {
                "id": null,
                "_method": 'POST',
            },
            success: function(response) {
                if (response.status == true) {

                    if (response.status == true) {
                        <?php if($authUser->user_type == 4){ ?>
                        $('#gifthubProductCount').html(response.data.gifthubProductCount);
                        $('#gifthubActiveProductCount').html(response.data.gifthubActiveProductCount);
                        $('#gifthubInActiveProductCount').html(response.data.gifthubInActiveProductCount);
                        <?php } ?>
                        <?php if($authUser->user_type == 5){ ?>
                        $('#postalProductCount').html(response.data.postalProductCount);

                        $('#postcardProductCount').html(response.data.postcardProductCount);
                        $('#postalProductActiveCount').html(response.data.postalProductActiveCount);
                        $('#postalProductInActiveCount').html(response.data.postalProductInActiveCount);
                        <?php } ?>
                        google.charts.load('current', {
                            'packages': ['corechart']
                        });
                        google.charts.setOnLoadCallback(drawChart);

                        function drawChart() {
                            var data = google.visualization.arrayToDataTable(response.data
                                .gifthubTopTenSellingProducts);
                            // var data = google.visualization.arrayToDataTable([
                            //     ['Task', 'Hours per Day'],
                            //     ['Work', 11],
                            //     ['Eat', 2],
                            //     ['Commute', 2],
                            //     ['Watch TV', 2],
                            //     ['Sleep', 7]
                            // ]);

                            var options = {
                                title: 'Top 10 Seller'
                            };

                            var chart = new google.visualization.PieChart(document.getElementById(
                                'piechart'));

                            chart.draw(data, options);
                        }


                    }
                }
            },
            error: function(data) {
                //console.log("IN ERRROR "+data);
            }
        });
    }
    //sagar code
    function monthyreportergreeting() {
        var APP_URL = {!! json_encode(url('/')) !!};

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: APP_URL + '/vendor/monthlyreport-greeting/',
            //url: APP_URL+product.categoryList+id,

            type: "POST",
            data: {
                "id": null,
                "_method": 'POST',
            },
            success: function(response) {
                if (response.status == true) {
                    // console.log(response.data.greetingreport);
                    $('#chart_div1').html(response.data.greetingreport);


                    // sagar code for chart
                    google.charts.load('current', {
                        'packages': ['bar']
                    });
                    google.charts.setOnLoadCallback(drawChart);

                    function drawChart() {
                        // var data = google.visualization.arrayToDataTable([
                        //     ['Date', 'qty', 'sell'],
                        //     ['2022-12-1', 120, 400],
                        //     ['2022-12-2', 343, 460],
                        //     ['2022-12-3', 41, 1120],
                        //     ['2022-12-4', 67, 540]
                        // ]);
                        var data = google.visualization.arrayToDataTable(response.data.greetingreport);


                        var options = {
                            chart: {
                                title: 'Monthly Greeting Report',
                                subtitle: '(Date, Qty, Sales)',
                            },
                            bars: 'vertical',
                            vAxis: {
                                format: 'decimal'
                            },

                            height: 400,
                            colors: ['#1b9e77', '#d95f02', '#7570b3']
                        };

                        var chart = new google.charts.Bar(document.getElementById('chart_div1'));

                        chart.draw(data, google.charts.Bar.convertOptions(options));

                        var btns = document.getElementById('btn-group');

                        btns.onclick = function(e) {

                            if (e.target.tagName === 'BUTTON') {
                                options.vAxis.format = e.target.id === 'none' ? '' : e.target.id;
                                chart.draw(data, google.charts.Bar.convertOptions(options));
                            }
                        }
                    }
                }
            },
            error: function(data) {
                //console.log("IN ERRROR "+data);
            }
        });
    }


    function orderData() {
        var APP_URL = {!! json_encode(url('/')) !!};

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: APP_URL + '/vendor/orderData/',
            //url: APP_URL+product.categoryList+id,

            type: "POST",
            data: {
                "id": null,
                "_method": 'POST',
            },
            success: function(response) {
                if (response.status == true) {
                    $('#totalORderShippedAndAwaitingForDelivery').html(response.data
                        .totalORderShippedAndAwaitingForDelivery);
                    $('#totalORderDelivered').html(response.data.totalORderDelivered);
                    $('#totalPedningOrder').html(response.data.totalPedningOrder);
                    $('#totalReturnRefund').html(response.data.totalReturnRefund);
                    $('#totalCancelOrder').html(response.data.totalCancelOrder);
                }
            },
            error: function(data) {
                //console.log("IN ERRROR "+data);
            }
        });
    }

    function all_product_count() {
        var APP_URL = {!! json_encode(url('/')) !!};

        $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: APP_URL + '/vendor/all-product-count/',

                type: "POST",
                data: {
                    "id": null,
                    "_method": 'POST',
                },
                success: function(response) {
                    if (response.status == true) {
                        $('#chart_div').html(response.data.printer);

                        // sagar code for chart
                        google.charts.load('current', {
                            'packages': ['bar']
                        });
                        google.charts.setOnLoadCallback(drawChart);

                        function drawChart() {
                        //     var data = google.visualization.arrayToDataTable([
                        //         ['Date', 'qty', 'sell'],
                        //         ['2022-12-1', 120, 400],
                        //         ['2022-12-2', 343, 460],
                        //         ['2022-12-3', 41, 1120],
                        //         ['2022-12-4', 67, 540]
                        //     ]);
                        var data = google.visualization.arrayToDataTable(response.data.printer);

                        var user_type = '<?php echo $userType; ?>';
                        if(user_type == 5){
                            var msg = "Monthly Greeting Report";
                        }
                        if(user_type == 6){
                            var msg = "Weekly Postmail Report";
                        }if(user_type){
                            var msg = "Monthly Greeting Report";
                        }
                        var options = {
                            chart: {
                                title: msg,
                                subtitle: 'Date, Qty,Sell(RM),Monthly data',
                            },
                            bars: 'vertical',
                            vAxis: {
                                format: 'decimal'
                            },

                            height: 400,
                            colors: ['#1b9e77', '#d95f02', '#7570b3']
                        };

                        var chart = new google.charts.Bar(document.getElementById('chart_div'));

                        chart.draw(data, google.charts.Bar.convertOptions(options));

                        var btns = document.getElementById('btn-group');

                        btns.onclick = function(e) {

                            if (e.target.tagName === 'BUTTON') {
                                options.vAxis.format = e.target.id === 'none' ? '' : e.target.id;
                                chart.draw(data, google.charts.Bar.convertOptions(options));
                            }
                        }

                    }
                    }
                },
                error: function(data) {
                //console.log("IN ERRROR "+data);
            }
            });


    }
    function postcard() {
        var APP_URL = {!! json_encode(url('/')) !!};

        $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: APP_URL + '/vendor/postcard-report/',

                type: "POST",
                data: {
                    "id": null,
                    "_method": 'POST',
                },
                success: function(response) {
                    if (response.status == true) {
                        if(response.data.postcard){
                           
                            $('#postcard').html(response.data.postcard.printer);

                            // sagar code for chart
                            google.charts.load('current', {
                                'packages': ['bar']
                            });
                            google.charts.setOnLoadCallback(drawChart);

                            function drawChart() {
                                //     var data = google.visualization.arrayToDataTable([
                                //         ['Date', 'qty', 'sell'],
                                //         ['2022-12-1', 120, 400],
                                //         ['2022-12-2', 343, 460],
                                //         ['2022-12-3', 41, 1120],
                                //         ['2022-12-4', 67, 540]
                                //     ]);
                                var data = google.visualization.arrayToDataTable(response.data.postcard.printer);


                                var options = {
                                    chart: {
                                        title: 'Monthly Postcard Report',
                                        subtitle: 'Date, Qty,Sell(RM),Monthly data',
                                    },
                                    bars: 'vertical',
                                    vAxis: {
                                        format: 'decimal'
                                    },

                                    height: 400,
                                    colors: ['#1b9e77', '#d95f02', '#7570b3']
                                };

                                var chart = new google.charts.Bar(document.getElementById('postcard'));

                                chart.draw(data, google.charts.Bar.convertOptions(options));

                                var btns = document.getElementById('btn-group');

                                btns.onclick = function(e) {

                                    if (e.target.tagName === 'BUTTON') {
                                        options.vAxis.format = e.target.id === 'none' ? '' : e.target.id;
                                        chart.draw(data, google.charts.Bar.convertOptions(options));
                                    }
                                }

                            }

                        }
                    }

                    if (response.status == true) {
                        if(response.data.greeting){
                            $('#greeting_graph_cust').html(response.data.greeting.printer);

                            // sagar code for chart
                            google.charts.load('current', {
                                'packages': ['bar']
                            });
                            google.charts.setOnLoadCallback(drawChart);

                            function drawChart() {
                                //     var data = google.visualization.arrayToDataTable([
                                //         ['Date', 'qty', 'sell'],
                                //         ['2022-12-1', 120, 400],
                                //         ['2022-12-2', 343, 460],
                                //         ['2022-12-3', 41, 1120],
                                //         ['2022-12-4', 67, 540]
                                //     ]);
                                var data = google.visualization.arrayToDataTable(response.data.greeting.printer);


                                var options = {
                                    chart: {
                                        title: 'Monthly Greeting Report',
                                        subtitle: 'Date, Qty,Sell(RM),Monthly data',
                                    },
                                    bars: 'vertical',
                                    vAxis: {
                                        format: 'decimal'
                                    },

                                    height: 400,
                                    colors: ['#1b9e77', '#d95f02', '#7570b3']
                                };

                                var chart = new google.charts.Bar(document.getElementById('greeting_graph_cust'));

                                chart.draw(data, google.charts.Bar.convertOptions(options));

                                var btns = document.getElementById('btn-group');

                                btns.onclick = function(e) {

                                    if (e.target.tagName === 'BUTTON') {
                                        options.vAxis.format = e.target.id === 'none' ? '' : e.target.id;
                                        chart.draw(data, google.charts.Bar.convertOptions(options));
                                    }
                                }

                            }
                        }
                        
                    }

                    if (response.status == true) {
                        if(response.data.securePrinterMonthlyReport){
                            $('#secure_printer').html(response.data.securePrinterMonthlyReport.printer);

                            // sagar code for chart
                            google.charts.load('current', {
                                'packages': ['bar']
                            });
                            google.charts.setOnLoadCallback(drawChart);

                            function drawChart() {
                                //     var data = google.visualization.arrayToDataTable([
                                //         ['Date', 'qty', 'sell'],
                                //         ['2022-12-1', 120, 400],
                                //         ['2022-12-2', 343, 460],
                                //         ['2022-12-3', 41, 1120],
                                //         ['2022-12-4', 67, 540]
                                //     ]);
                                var data = google.visualization.arrayToDataTable(response.data.securePrinterMonthlyReport.printer);


                                var options = {
                                    chart: {
                                        title: 'Monthly Report',
                                        subtitle: 'Date, Qty,Sell(RM),Monthly data',
                                    },
                                    bars: 'vertical',
                                    vAxis: {
                                        format: 'decimal'
                                    },

                                    height: 400,
                                    colors: ['#1b9e77', '#d95f02', '#7570b3']
                                };

                                var chart = new google.charts.Bar(document.getElementById('secure_printer'));

                                chart.draw(data, google.charts.Bar.convertOptions(options));

                                var btns = document.getElementById('btn-group');

                                btns.onclick = function(e) {

                                    if (e.target.tagName === 'BUTTON') {
                                        options.vAxis.format = e.target.id === 'none' ? '' : e.target.id;
                                        chart.draw(data, google.charts.Bar.convertOptions(options));
                                    }
                                }

                            }
                        }
                        
                    }
                },
                error: function(data) {
                //console.log("IN ERRROR "+data);
            }
            });


    }

</script>
