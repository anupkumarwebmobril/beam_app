<?php
    $mislanious7 = \App\Models\Miscellaneous::where('id', 7)->first();
    $mislanious8 = \App\Models\Miscellaneous::where('id', 8)->first();

?>

<style>
    .custom-class-grid {
        display: grid;
        grid-template-columns: repeat(3,1fr);
        grid-column-gap: 30px;
        grid-row-gap: 0px;
        border: 1px solid #c2c2c2;
        border-radius: 5px;
        padding: 20px;
    }
</style>

@foreach ($data as $view)


    <div class="modal fade" id="user-info{{ $view->order_item_id }}" style="width:100%">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Viewing order number <strong>{{ $view->order_number }}</strong></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">

                        <div class="col-4">
                            <div >
                                <label for="Name" class="col-form-label">Order Item</label>
                                <div class="col-sm-10">
                                    <p>{{ $view->product_name }}
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-4">
                            <div >
                                <label for="Email" class="col-form-label">Order Status</label>
                                <div class="col-sm-10">
                                    <p>{{ $view->order_status_name }}
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-4">
                            <div >
                                <label for="Email" class="col-form-label">Order Placed Date</label>
                                <div class="col-sm-10">
                                    <p>{{ $view->created_at }}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        {{-- <div class="col-4">
                            <div class="form-group">
                                <label for="Mobile" class="col-form-label">Best wishes for you</label>
                                <div class="col-sm-10">
                                    <p>{{ $view->additional_interests_msg }}</p>
                                </div>
                            </div>
                        </div> --}}

                        <div class="col-4">
                            <div class="form-group">
                                <label for="Status" class="col-form-label">{{ (!empty($mislanious7->name)) ? $mislanious7->name : "" }}</label>
                                <div class="col-sm-10">
                                    <p>{{ $view->same_day_delivery == 1 ? 'Yes' : 'No' }}</p>
                                </div>
                            </div>
                        </div>


                        {{-- <div class="col-4">
                            <div class="form-group">
                                <label for="Status" class="col-form-label">Add premium service</label>
                                <div class="col-sm-10">
                                    <p>{{ $view->add_premium_service == 1 ? 'Yes' : 'No' }}</p>
                                </div>
                            </div>
                        </div> --}}
                    </div>

                    <hr>
                    <div class="row">
                        <div class="col-4">
                            <div class="form-group">
                                <label for="Mobile" class="col-form-label">Other Add-Ons</label>
                                <div class="col-sm-10">


                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        @foreach ($view->paperTypes as $paperType)
                            <div class="col-3">
                                <div class="form-group">
                                    <div class="col-sm-10">
                                        <p>{{$paperType->name}}</p>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="Mobile" class="col-form-label">Promo Code</label>
                                <div class="col-sm-28">
                                    <p>{{ !empty($view['order_item']->promocode) ? $view['order_item']->promocode : 'N/A' }}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col">
                            <div class="form-group">
                                <label for="Mobile" class="col-form-label">Discount Amount</label>
                                <div class="col-sm-28">
                                    <p>{{ !empty($view['order_item']->discounted_amount) ? $view['order_item']->discounted_amount : 'N/A' }}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col-3">
                            <div class="form-group">
                                <label for="Mobile" class="col-form-label">Order Total</label>
                                <div class="col-sm-10">
                                    <p>RM:{{ Round($view->total, 2) }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                <label for="Mobile" class="col-form-label">Quantity</label>
                                <div class="col-sm-10">
                                    <p>{{ !empty($view['order_item']->quantity) ? $view['order_item']->quantity : null }}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="col-3">
                            <div class="form-group">
                                <label for="Mobile" class="col-form-label">Price</label>
                                <div class="col-sm-10" style="padding-left:0px;">
                                    <p>{{ !empty($view['order_item']->price) ? "RM:".$view['order_item']->price : null }}
                                    </p>
                                </div>
                            </div>
                        </div>

                        @if($view['order_item']->custom_order == 1)
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="Mobile" class="col-form-label">Designer Charge</label>
                                    <div class="col-sm-10" style="padding-left:0px;">
                                        <p>{{ !empty($view['order_item']->designer_charge) ? "RM:".$view['order_item']->designer_charge : null }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="col-3">
                            <div class="form-group">
                                <label for="Mobile" class="col-form-label">Item Total</label>
                                <div class="col-sm-10">
                                    {{-- @if($view['order_item']->custom_order == 1)
                                        <p>{{ Round($view['order_item']->item_total_price, 2) }}</p>
                                    @else
                                        <p>{{ !empty($view['order_item']->quantity) ? "RM:".Round($view['order_item']->quantity * $view['order_item']->price, 2) : null }}</p>
                                    @endif --}}
                                        <p>RM:{{ $view->recepients ? Round($view->total, 2) / $view->recepients->count() : 0.0  }}</p>

                                </div>
                            </div>
                        </div>


                    </div>
                    @if(!empty($view['order_item']->product_type) && ($view['order_item']->section == 1))
                        <div class="row">
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="Mobile" class="col-form-label">Print Type</label>
                                    <div class="col-sm-10">
                                        <p>{{ !empty($view['order_item']->print_type) && $view['order_item']->print_type == 1 ? 'Black & white' : 'Coloured' }}
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-4">
                                <div class="form-group">
                                    <label for="Mobile" class="col-form-label">To Text</label>
                                    <div class="col-sm-10">
                                        <p>{{ $view->to_text }}</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-4">
                                <div class="form-group">
                                    <label for="Mobile" class="col-form-label">Write your text</label>
                                    <div class="col-sm-10">
                                        <p>{{ $view->write_your_text }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="Mobile" class="col-form-label">Wishesh</label>
                                    <div class="col-sm-10">
                                        <p>{{ !empty($view['order_item']->wishesh) ? $view['order_item']->wishesh : null }}
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-4">
                                <div class="form-group">
                                    <label for="Mobile" class="col-form-label">Host</label>
                                    <div class="col-sm-10">
                                        <p>{{ !empty($view['order_item']->host) ? $view['order_item']->host : null }}
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-4">
                                <div class="form-group">
                                    <label for="Mobile" class="col-form-label">Mr/Groom/Person</label>
                                    <div class="col-sm-10">
                                        <p>{{ !empty($view['order_item']->mr_groom_person) ? $view['order_item']->mr_groom_person : null }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="Mobile" class="col-form-label">Mrs/Bride</label>
                                    <div class="col-sm-10">
                                        <p>{{ !empty($view['order_item']->mrs_bride) ? $view['order_item']->mrs_bride : null }}
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-4">
                                <div class="form-group">
                                    <label for="Mobile" class="col-form-label">Date Time</label>
                                    <div class="col-sm-10">
                                        <p>{{ !empty($view['order_item']->date_time_of_wishes) ? $view['order_item']->date_time_of_wishes : null }}
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-4">
                                <div class="form-group">
                                    <label for="Mobile" class="col-form-label">Venue 1</label>
                                    <div class="col-sm-10">
                                        <p>{{ !empty($view['order_item']->venue1) ? $view['order_item']->venue1 : null }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="Mobile" class="col-form-label">Venue 2</label>
                                    <div class="col-sm-10">
                                        <p>{{ !empty($view['order_item']->venue2) ? $view['order_item']->venue2 : null }}
                                        </p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    @endif

                    <div class="row">
                        <div class="col-4">
                            <div class="form-group">
                                <label for="Status" class="col-form-label">{{ (!empty($mislanious8->name)) ? $mislanious8->name : "" }}</label>
                                <div class="col-sm-10">
                                    <p>{{ ($view->additional_interests == 0) ? "No" : "Yes" }}</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-4">
                            <div class="form-group">
                                <label for="Status" class="col-form-label">{{ (!empty($mislanious8->name)) ? $mislanious8->name : "" }} Msg</label>
                                <div class="col-sm-10">
                                    <p>{{ $view->additional_interests_msg }}</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-4">
                            {{-- <div class="form-group">
                            <label for="Status" class="col-form-label">Recepient</label>
                            <div class="col-sm-10">
                                <p>{{$view->recepient}}</p>
                            </div>
                        </div> --}}
                        </div>

                    </div>

                    <hr>
                    <div class="row">
                        <div class="col-4">
                            <div class="form-group">
                                <label for="Status" class="col-form-label">Rate</label>
                                <div class="col-sm-10">
                                    @if(!empty($view['rating']->rate))
                                        @for ($i = 0; $i < 5; $i++)
                                            @if (floor($view['rating']->rate) - $i >= 1)
                                                {{-- Full Start --}}
                                                <i class="fas fa-star text-warning"> </i>
                                            @elseif ($view['rating']->rate - $i > 0)
                                                {{-- Half Start --}}
                                                <i class="fas fa-star-half-alt text-warning"> </i>
                                            @else
                                                {{-- Empty Start --}}
                                                <i class="far fa-star text-warning"> </i>
                                            @endif
                                        @endfor
                                    @endif


                                </div>
                            </div>
                        </div>

                        <div class="col-4">
                            <div class="form-group">
                                <label for="Status" class="col-form-label">Review</label>
                                <div class="col-sm-10">
                                    <p>{{ !empty($view['rating']->review) ? $view['rating']->review : null }}</p>
                                </div>
                            </div>
                        </div>

                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-4">
                            <div class="form-group">
                                <label for="Status" class="col-form-label">Recepient Information</label>
                                <div class="col-sm-28">
                                    <p></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    @if (!empty($view->recepients))
                        @foreach ($view->recepients as $recepient)
                            <div class="row custom-class-grid">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="Mobile" class="col-form-label">Name</label>
                                        <div class="col-sm-28">
                                            {{ $recepient->name }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="Mobile" class="col-form-label">Phone Number</label>
                                        <div class="col-sm-28">
                                            {{ $recepient->phone_number }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="Mobile" class="col-form-label">House Number</label>
                                        <div class="col-sm-28">
                                            {{ $recepient->house_number }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="Mobile" class="col-form-label">Landmark</label>
                                        <div class="col-sm-28">
                                            {{ $recepient->land_mark }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="Mobile" class="col-form-label">City</label>
                                        <div class="col-sm-28">
                                            {{ $recepient->city }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="Mobile" class="col-form-label">State</label>
                                        <div class="col-sm-28">
                                            {{ $recepient->state }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="Mobile" class="col-form-label">Zip Code</label>
                                        <div class="col-sm-28">
                                            {{ $recepient->zip_code }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="Mobile" class="col-form-label">Full Address</label>
                                        <div class="col-sm-28">
                                            {{ $recepient->full_address }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif


                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-info" onclick="printDetailInfo({{$view->order_item_id}})">Print</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


@endforeach

<script>
    function printDetailInfo(id){
        var printContents=document.getElementById("user-info"+id).innerHTML;

        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
        window.location.reload();
    }
</script>
