@extends('admin.index')

@section('after-style')

<link rel="stylesheet" href="{{asset('public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">


@endsection

@section('content')

<?php $uriSegment = Request::segment(2); ?>

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('Admin.Dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Manage Order</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <div class="card">
                        <div class="card-header">
                            Gift Return & Refund Order list
                        </div>

                        @if(session('message'))
                            <div class="alert alert-success" style="padding: 5px 20px;margin-bottom: 5px;">{{ session('message') }}</div>
                        @endif
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="user-list" class="table table-bordered table-striped" data-order='[[ 0, "asc" ]]'>
                                <thead>
                                    <tr>
                                        <th>Order Number</th>
										<th>Order Item</th>
                                        <th>Total Price</th>
                                        <th>Merchant Name</th>
                                        <th>Client Name</th>

                                        <th>Created At</th>

                                        @if(!empty($uriSegment) && ($uriSegment == "giftReview") OR ($uriSegment == "postalReview"))
                                            <th>Rating</th>
                                            <th>Review</th>
                                        @else
                                            <th>Order Status</th>
                                            <th>Cancel Reason</th>
                                            <th width="20%">View</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                @php $i=1; @endphp
                                    {{-- @dd($data); --}}
                                    @if(isset($data) && $data != null)
                                        @foreach($data as $val)

                                            <tr>
                                                <td>{{$val->order_number}}</td>
                                                <td>{{$val->product_name}}</td>
                                                <td>{{ round($val->total, 2) }}</td>
                                                <td>{{$val->vendor_name}}</td>
                                                <td>{{$val->sender_name}}</td>

                                                <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$val->created_at)->tz("Asia/Kuala_Lumpur") }}</td>
                                                {{-- <td>{{$val->total}}</td> --}}

                                                @if(!empty($uriSegment) && ($uriSegment == "giftReview") OR ($uriSegment == "postalReview"))

                                                    <td>
                                                        @if(!empty($val['rating']->rate))
                                                            @for ($i = 0; $i < 5; $i++)
                                                                @if (floor($val['rating']->rate) - $i >= 1)
                                                                    {{-- Full Start --}}
                                                                    <i class="fas fa-star text-warning"> </i>
                                                                @elseif ($val['rating']->rate - $i > 0)
                                                                    {{-- Half Start --}}
                                                                    <i class="fas fa-star-half-alt text-warning"> </i>
                                                                @else
                                                                    {{-- Empty Start --}}
                                                                    <i class="far fa-star text-warning"> </i>
                                                                @endif
                                                            @endfor
                                                        @endif
                                                    </td>
                                                    <td>{{$val->rating->review}}</th>
                                                @else
                                                    <td>Return & Refund Requested</td>
                                                    <td>{{ $val['order_item']->cancel_reason ?? "N/A" }}</td>
                                                    <td style="text-align: center;">
                                                        {{-- <a href="javascript:void();" data-toggle="modal" data-target="#image-info{{$val->order_item_id}}"><i class="fas fa-arrow-down"></i>&nbsp;&nbsp;</a> --}}

                                                        <a href="javascript:void();" data-toggle="modal" data-target="#user-info{{$val->order_item_id}}"><i class="fas fa-eye"></i>&nbsp;&nbsp;</a>
                                                        @if(!empty($uriSegment) && ($uriSegment == "order") OR ($uriSegment == "giftPending") OR ($uriSegment == "postalPending"))
                                                            <a href="{{route('Admin.order.edit',[$val->order_item->id])}}?page={{(!empty($uriSegment)) ? $uriSegment :null}}"><img style="width:20px;" src="{{asset('public/ic_track.png')}}" />{{-- <i class="fa fa-plane"> --}}</i>&nbsp;&nbsp;</a>
                                                        @endif
                                                    </td>
                                                @endif
                                            </tr>
                                            <div class="modal fade" id="image-info{{$val->order_item_id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                  <div class="modal-content">
                                                    <div class="modal-header">
                                                      <h5 class="modal-title" id="exampleModalLabel">Image</h5>
                                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                      </button>
                                                    </div>
                                                    <div class="modal-body" style="height:90% !important">
                                                        <div class="row">

                                                            @if(!empty($val->images))
                                                            @foreach ($val->images as $img )
                                                                @if(!empty($img))
                                                                    <?php $fileExt = pathinfo($img, PATHINFO_EXTENSION); ?>
                                                                    <div class="col-md-3">
                                                                        @if ($fileExt == "pdf")
                                                                            <embed src="{{$img}}" width="100%" style="border:1px solid rgb(12, 12, 12); height:100px;" />
                                                                        @else
                                                                            <img src="{{$img}}" class="img-fluid ${3|rounded-top,rounded-right,rounded-left,rounded-circle,|}" width="100%" style="border:1px solid rgb(12, 12, 12); height:100px;" alt="">
                                                                        @endif
                                                                        <a href="{{$img}}" class="btn btn-outline-primary m-1" download>Download</a>
                                                                        <button style="width:94px" href="{{$img}}" onclick="printImg('{{$img}}')" class="btn btn-outline-primary m-1" download>Print</button>
                                                                    </div>
                                                                @endif

                                                            @endforeach
                                                            @endIf
                                                        </div>

                                                    </div>
                                                    <div class="modal-footer">
                                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                      {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>

                                        @endforeach
                                    @endif
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    @include('admin.order.view')
</div>

@endsection

@section('after-scripts')
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>

<script>
    function printImg(url) {
        var win = window.open('');
        win.document.write('<img src="' + url + '" onload="window.print();window.close()" />');
        win.focus();
    }

    $(function() {
        $("#user-list").DataTable({
            "responsive": true,
            "autoWidth": false,
        });
    });

    $('body').on('click', '.rowDelete', function() {
      	var APP_URL = {!! json_encode(url('/')) !!};
        var id = $(this).data("id");
        var token = $(this).data("token");

        $(this).closest('tr').remove();
	     $.ajax({
           // url: APP_URL+'/vendorproduct/delete/'+id,
            url: APP_URL+'/admin/order/delete/'+id,
            type: "POST",
            data: {
                "id": id,
                "_method":'DELETE',
                "_token": token
            },
            success: function (response) {
                console.log('data deleted successfully');
                //window.location.href= APP_URL+'/vendorproduct';
                location.reload();
            },
            error: function(response) {
                console.log("IN ERRROR "+response);
                // window.location.href= window.location.pathname;
            }
        });

    });
</script>
@endsection
