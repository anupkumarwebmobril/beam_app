<meta name="csrf-token" content="{{ csrf_token() }}" />

@extends('admin.index')

@section('after-style')

<link rel="stylesheet" href="{{asset('public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

@endsection

@section('content')

<?php $uriSegment = Request::segment(2);

?>

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('Admin.Dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">

                                @if(!empty($uriSegment) && ($uriSegment == "printercancelorderadmin"))
                                    Cancelled Postal Order List
                                @elseif(!empty($uriSegment) && ($uriSegment == "printercancelorder"))
                                    Cancelled Order List
                                @else
                                    Cancelled Gift Order List
                                @endif

                        </li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                @if(!empty($uriSegment) && ($uriSegment == "printercancelorderadmin"))
                                    Cancelled Postal Order List
                                @elseif(!empty($uriSegment) && ($uriSegment == "printercancelorder"))
                                    Cancelled Order List
                                @else
                                    Cancelled Gift Order List
                                @endif
                            </h3>

                        </div>

                        <h5 id="ajax_response" style="color:red; display:none;"><span id="msg_span" class="" style="color: green; margin:10px;"></span> </h5>

                        @if(session('message'))
                            <div class="alert alert-success" style="padding: 5px 20px;margin-bottom: 5px;">{{ session('message') }}</div>
                        @endif
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="user-list" class="table table-bordered table-striped" data-order='[[ 0, "asc" ]]'>
                                <thead>
                                    <tr>
                                        <th>Order Number</th>
										<th>Order Item</th>
										<th>Order Status</th>
                                        <th>Client Name</th>
                                        <th>Merchant Name</th>
                                        <th>Cancel Reason</th>
                                        <th>Created At</th>
                                        {{-- <th>Total</th> --}}
										{{-- <th width="20%">View</th> --}}

                                    </tr>
                                </thead>
                                <tbody>
                                @php $i=1; @endphp

                                    @if(isset($data) && $data != null)
                                        @foreach($data as $val)

                                            <tr>
                                                <td>{{$val->order_number}}</td>
                                                <td>{{$val->product_name}}</td>
                                                <td>{{$val->order_status_name}}</td>
                                                <td>{{$val->sender_name}}</td>
                                                @if(auth()->user()->user_type == 4)

                                                <td>{{ auth()->user()->name }}</td>
                                                @else

                                                <td>{{ $val->vendor_name }}</td>
                                                @endif

                                                <td>{{ $val->order_item->cancel_reason }}</td>
                                                <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$val->created_at)->tz("Asia/Kuala_Lumpur") }}
                                                {{-- <td>{{$val->total}}</td> --}}

                                                {{-- <td style="text-align: center;">

                                                    <a href="javascript:void();" data-toggle="modal" data-target="#user-info{{$val->order_item_id}}"><i class="fas fa-eye"></i>&nbsp;&nbsp;</a>
                                                    <a href="{{route('Vendor.order.edit',[$val->order_item->id])}}"><i class="fas fa-edit"></i>&nbsp;&nbsp;</a>

                                                </td> --}}
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    @include('admin.order.view')
</div>

@endsection

@section('after-scripts')
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>

<script>
    $(function() {
        $("#user-list").DataTable({
            "responsive": true,
            "autoWidth": false,
        });
    });

    $('body').on('click', '.rowDelete', function() {
      	var APP_URL = {!! json_encode(url('/')) !!};
        var id = $(this).data("id");
        var token = $(this).data("token");

        $(this).closest('tr').remove();
	     $.ajax({
           // url: APP_URL+'/vendorproduct/delete/'+id,
            url: APP_URL+'/admin/order/delete/'+id,
            type: "POST",
            data: {
                "id": id,
                "_method":'DELETE',
                "_token": token
            },
            success: function (response) {
                console.log('data deleted successfully');
                //window.location.href= APP_URL+'/vendorproduct';
                location.reload();
            },
            error: function(response) {
                console.log("IN ERRROR "+response);
                // window.location.href= window.location.pathname;
            }
        });

    });

    $('body').on('click', '.cancel_order', function() {
        if (confirm("Are you sure?") == true) {
            var APP_URL = {!! json_encode(url('/')) !!};
            var id = $(this).data("id");
            var token = $(this).data("token");

            $(this).closest('tr').remove();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            // url: APP_URL+'/vendorproduct/delete/'+id,
                url: APP_URL+'/vendor/order/cancel-order/'+id,
                type: "POST",
                data: {
                    "id": id,
                    "_method":'POST',
                    "_token": token
                },
                success: function (data) {
                    if(data.status === false){
                        $('#ajax_response').css('color', 'green');
                        $('#ajax_response').css('display', 'block');
                        $('#msg_span').html(data.msg);
                    } else {
                        $('#ajax_response').css('color', 'green');
                        $('#ajax_response').css('display', 'block');
                        $('#msg_span').text(data.msg);
                        $('#app').scrollTop()
                        $('#ajax_response').fadeIn().delay(6000).fadeOut();
                        $("html, body").animate({ scrollTop: 0 }, "slow");
                    //  window.location.href = data.redirect;
                    }
                },
                error: function(response) {
                    console.log("IN ERRROR "+response);
                    // window.location.href= window.location.pathname;
                }
            });
        }
    });
</script>
@endsection
