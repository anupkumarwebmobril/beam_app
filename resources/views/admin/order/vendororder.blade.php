<meta name="csrf-token" content="{{ csrf_token() }}" />

@extends('admin.index')

@section('after-style')
    <link rel="stylesheet" href="{{ asset('public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endsection

@section('content')
    <?php $uriSegment = Request::segment(2); ?>

    <style>
        .btn.btn-secondary {
            margin-top: 4px;
        }
    </style>
    <style>
        .modal-footer {
            border-top: none !important;
        }

        .col-md-3 {
            text-align: center !important;
        }

        .image-height {
            height: 400px !important;
        }
    </style>

    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">

                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('Admin.Dashboard') }}">Home</a></li>
                            <li class="breadcrumb-item active">Manage Order</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Postal Order List</h3>

                            </div>
                            <h5 id="ajax_response" style="color:red; display:none;"><span id="msg_span" class=""
                                    style="color: green; margin:10px;"></span> </h5>

                            @if (session('message'))
                                <div class="alert alert-success" style="padding: 5px 20px;margin-bottom: 5px;">
                                    {{ session('message') }}</div>
                            @endif
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="user-list" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Order Number</th>
                                            <th>Order Item</th>
                                            <th>Postal Product</th>
                                            <th>Printer Name</th>
                                            <th>Total Price</th>
                                            <th>Order Status</th>
                                            <th>Client Name</th>
                                            <th>Pending Days</th>
                                            <th>Created At</th>
                                            {{-- <th>Total</th> --}}
                                            <th width="20%">View</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $i=1; @endphp

                                        @if (isset($data) && $data != null)
                                            @foreach ($data as $val)
                                                <tr>
                                                    <td>{{ $val->order_number }}</td>
                                                    <td>{{ $val->product_name ?? 'Custom Order' }}</td>
                                                    <td>{{ $val->productType ?? 'Custom Order' }}</td>

                                                    <td>{{ !empty($val['order_item']->printer_vendor_name) ? $val['order_item']->printer_vendor_name : 'Perak Printer' }}
                                                    </td>
                                                    {{-- <td>{{ auth()->user()->name }}</td> --}}
                                                    <td>{{ !empty($val['order_item']->item_grand_total_price) ? $val['order_item']->item_grand_total_price : 'N/A' }}
                                                    </td>
                                                    <td>{{ $val->order_status_name }}</td>
                                                    {{-- <td></td> --}}
                                                    <td>{{ $val->sender_name }}</td>
                                                    <td>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($val->created_at))->diffForHumans() }}
                                                    </td>
                                                    {{-- <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$val->created_at)->tz("Asia/Kuala_Lumpur") }} --}}
                                                    <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $val->created_at)->tz('Asia/Kuala_Lumpur') }}
                                                    </td>
                                                    {{-- <td>{{$val->total}}</td> --}}

                                                    <td style="text-align: center;">
                                                        <a href="javascript:void();" data-toggle="modal"
                                                            data-target="#image-info{{ $val->order_item_id }}"><i
                                                                class="fas fa-arrow-down"></i>&nbsp;&nbsp;</a>

                                                        <a href="javascript:void();" data-toggle="modal"
                                                            data-target="#user-info{{ $val->order_item_id }}"><i
                                                                class="fas fa-eye"></i>&nbsp;&nbsp;</a>
                                                        @if (auth()->user()->user_type == 5 or auth()->user()->user_type == 6)
                                                            <a
                                                                href="{{ route('Vendor.order.edit', [$val->order_item->id]) }}?page={{ !empty($uriSegment) ? $uriSegment : null }}"><img
                                                                    style="width:20px;"
                                                                    src="{{ asset('public/ic_track.png') }}" />{{-- <i class="fas fa-edit"></i> --}}&nbsp;&nbsp;</a>
                                                            <a href="#" data-toggle="modal"
                                                                data-target="#cancel{{ $val->order_item_id }}">Cancel Order
                                                            </a>
                                                        @endif
                                                        @if (auth()->user()->user_type == 1)
                                                            <a
                                                                href="{{ route('Admin.order.edit', [$val->order_item->id]) }}?page={{ !empty($uriSegment) ? $uriSegment : null }}"><img
                                                                    style="width:20px;"
                                                                    src="{{ asset('public/ic_track.png') }}" />{{-- <i class="fas fa-edit"></i> --}}&nbsp;&nbsp;</a>
                                                        @endif

                                                        {{-- <a href="javascript:void(0);" class="rowDelete" data-id="{{$val->id}}" data-token="{{ csrf_token() }}"><i class="fas fa-trash" ></i></a> --}}

                                                    </td>
                                                    <!-- Modal -->
                                                    <div class="modal fade" id="cancel{{ $val->order_item_id }}"
                                                        tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                        aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="exampleModalLabel">Cancel
                                                                        Order </h5>
                                                                    <button type="button" class="close"
                                                                        data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="form-group">
                                                                        <label for="customSelect">Select an Option:</label>
                                                                        <select class="form-control customSelect">
                                                                            <option class="form-control"
                                                                                value="Out of Stock">Out of Stock</option>
                                                                            <option class="form-control"
                                                                                value="Unable to Reach User Location">Unable
                                                                                to Reach User Location</option>
                                                                            <option class="form-control" value="custom">
                                                                                Custom</option>
                                                                        </select>

                                                                        <div class="form-group customInput"
                                                                            style="display: none;">
                                                                            <label for="customValue">Enter Custom
                                                                                Comment:</label>
                                                                            <input type="text"
                                                                                class="form-control customValue">
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <div class="modal-footer">

                                                                    <button type="button" class="btn btn-secondary"
                                                                        data-dismiss="modal">Close</button>


                                                                    <button type="button"
                                                                        data-id="{{ $val->order_item->id }}"
                                                                        class="btn btn-primary cancel_order">Confirm</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </tr>
                                                @php
                                                    if ($val['order_item']->product_type == 2) {
                                                        // $width = "100%";
                                                        // $hight = "141%";
                                                        $width = '70%';
                                                        $hight = '98.70%';
                                                    } elseif ($val['order_item']->product_type == 3) {
                                                        // $width = "100%";
                                                        // $hight = "141%";
                                                        $width = '70%';
                                                        $hight = '98.70%';
                                                    } else {
                                                        // $hight = "100%";
                                                        // $width = "191%";
                                                        $hight = '52%';
                                                        $width = '99.32%';
                                                    }

                                                @endphp
                                                <div class="modal fade" id="image-info{{ $val->order_item_id }}"
                                                    tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                    aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        {{-- <div class="modal-content" style="height:81% !important; width:150% !important"> --}}
                                                        <div class="modal-content image-height">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">Image</h5>
                                                                <button type="button" class="close"
                                                                    data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="row">
                                                                    @foreach ($val->images as $img)
                                                                        @if (!empty($img))
                                                                            <?php $fileExt = pathinfo($img, PATHINFO_EXTENSION); ?>
                                                                            <div class="col-md-3"
                                                                                style="max-height:115px;">
                                                                                @if ($fileExt == 'pdf')
                                                                                    <embed src="{{ $img }}"
                                                                                        style="border:1px solid rgb(12, 12, 12); height:100px; width:{{ $width }};" />
                                                                                @else
                                                                                    <img src="{{ $img }}"
                                                                                        class="img-fluid ${3|rounded-top,rounded-right,rounded-left,rounded-circle,|}"
                                                                                        style="border:1px solid rgb(12, 12, 12); height:{{ $hight }}; width:{{ $width }};"
                                                                                        alt="">
                                                                                @endif
                                                                                <a href="{{ $img }}"
                                                                                    class="btn btn-outline-primary m-1"
                                                                                    download>Download</a>
                                                                                <button style="width:94px"
                                                                                    href="{{ $img }}"
                                                                                    onclick="printImg('{{ $img }}')"
                                                                                    class="btn btn-outline-primary m-1"
                                                                                    download>Print</button>
                                                                            </div>
                                                                        @endif
                                                                    @endforeach
                                                                </div>

                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                    data-dismiss="modal">Close</button>
                                                                {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </tbody>

                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
        @include('admin.order.postcard-view')
    </div>

@endsection

@section('after-scripts')
    <script src="{{ asset('public/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('public/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script>
        const selectElements = document.querySelectorAll(".customSelect");
        const customInputs = document.querySelectorAll(".customInput");
        const customValueInputs = document.querySelectorAll(".customValue");

        // Add event listeners to all select elements
        selectElements.forEach((selectElement, index) => {
            selectElement.addEventListener("change", function() {
                if (selectElement.value === "custom") {
                    customInputs[index].style.display = "block";
                } else {
                    customInputs[index].style.display = "none";
                }
            });
        });
    </script>
    <script>
        function printImg(url) {
            // console.log(url);
            var win = window.open('');
            win.document.write('<img id="print-img" src="' + url + '" />');
            // console.log(win);

            win.document.close();

            // Wait for the image to load
            win.document.getElementById('print-img').onload = function() {
                win.print();
                // win.close();
            };
        }

        $(function() {
            $("#user-list").DataTable({
                "responsive": true,
                "autoWidth": false,
                order: [
                    [6, 'desc']
                ]
            });
        });

        $('body').on('click', '.rowDelete', function() {
            var APP_URL = {!! json_encode(url('/')) !!};
            var id = $(this).data("id");
            var token = $(this).data("token");

            $(this).closest('tr').remove();
            $.ajax({
                // url: APP_URL+'/vendorproduct/delete/'+id,
                url: APP_URL + '/admin/order/delete/' + id,
                type: "POST",
                data: {
                    "id": id,
                    "_method": 'DELETE',
                    "_token": token
                },
                success: function(response) {
                    console.log('data deleted successfully');
                    //window.location.href= APP_URL+'/vendorproduct';
                    location.reload();
                },
                error: function(response) {
                    console.log("IN ERRROR " + response);
                    // window.location.href= window.location.pathname;
                }
            });

        });

        $('body').on('click', '.cancel_order', function() {
            if (confirm("Are you sure?") == true) {
                var APP_URL = {!! json_encode(url('/')) !!};
                var id = $(this).data("id");
                var token = $(this).data("token");
                var selectedOption = $(".customSelect")
            .val(); // Assuming you have a select element with an id of "customSelect"
                var customValue = $(".customValue")
            .val(); // Assuming you have an input element with an id of "customValue"

                $(this).closest('tr').remove();
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: APP_URL + '/vendor/order/cancel-order/' + id,
                    type: "POST",
                    data: {
                        "id": id,
                        "selectedOption": selectedOption, // Add the selected option value to the data
                        "customValue": customValue, // Add the custom value to the data
                        "_method": 'POST',
                        "_token": token
                    },
                    success: function(data) {
                        if (data.status === false) {
                            $('#ajax_response').css('color', 'green');
                            $('#ajax_response').css('display', 'block');
                            $('#msg_span').html(data.msg);
                        } else {
                            $('#ajax_response').css('color', 'green');
                            $('#ajax_response').css('display', 'block');
                            $('#msg_span').text(data.msg);
                            $('#app').scrollTop()
                            $('#ajax_response').fadeIn().delay(6000).fadeOut();
                            $("html, body").animate({
                                scrollTop: 0
                            }, "slow");
                            //  window.location.href = data.redirect;
                        }
                    },
                    error: function(response) {
                        console.log("IN ERRROR " + response);
                        // window.location.href= window.location.pathname;
                    }
                });
            }
        });
    </script>
@endsection
