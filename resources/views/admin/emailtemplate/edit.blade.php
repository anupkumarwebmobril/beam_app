@extends('admin.index')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <!--  -->
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('Admin.Dashboard') }}">Home</a></li>
                            <li class="breadcrumb-item active">Update Email Template</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-1"></div>
                    <div class="col-10">
                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">Update Email Template<strong></strong></h3>
                            </div>
                            @include('admin.partials.messages')
                            <form class="form-horizontal" method="post"
                                action="{{ route('Admin.updateMailTemplate', $mis->id) }}" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                {{-- <input type="hidden" name="id" value="{{$paper->id}}" /> --}}
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label for="Name" class="col-sm-2 col-form-label">Subject</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="subject" name="subject"
                                                value="{{ $mis->subject }}" placeholder="Subject" required="true"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="Name" class="col-sm-2 col-form-label">Message</label>
                                        <div class="col-sm-10">
                                            {{-- <input type="text" class="form-control" id="message" name="message"
                                                step="any" value="{{ $mis->message }}" placeholder="Message" required="true"/> --}}
                                                <textarea id="summernote" name="message" style="height: 125px; width: 100%; padding: 10px" required="true" placeholder="Message">{{ $mis->message }}</textarea>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-info">Update Email Template</button>
                                        <a href="{{ route('Admin.miscellaneous.index') }}"
                                            class="btn btn-default float-right">Cancel</a>
                                    </div>
                                </div>
                        </div>
        </section>

    </div>

    <!-- </div> -->
@endsection


@section('after-scripts')
    <script src="{{ asset('public/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script>
        $(function() {
            // Summernote
            $('.textarea').summernote()
        })
    </script>
@endsection
@section('after-style')
    <link rel="stylesheet" href="{{ asset('public/plugins/summernote/summernote-bs4.css') }}">
@endsection
