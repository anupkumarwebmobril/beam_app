@extends('admin.index')
<meta name="csrf-token" content="{{ csrf_token() }}" />
@section('content')
    <!-- <div class="wrapper"> -->

    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <!--  -->
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('Admin.Dashboard') }}">Home</a></li>
                            <li class="breadcrumb-item active">Update Sub Category</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-1"></div>
                    <div class="col-10">

                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">Update <strong>{{ $data->name }}</strong></h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            @include('admin.partials.messages')
                           	<form class="form-horizontal" method="post" action="{{route('Admin.subcategory.update',['category'=>$data->id])}}" enctype="multipart/form-data" autocomplete="off">
                                @csrf
								@method('PUT')
                                <input type="hidden" name="id" value="{{ $data->id }}" />
                                <div class="card-body">

									<div class="form-group row">
										<label for="Status" class="col-sm-2 col-form-label">Category Type</label>
										<div class="col-sm-10">
											<select class="form-control select2" style="width: 100%;" name="type"
												id="type" required="true">
												<option value="">Select Category type</option>
												<option value="1" {{ !empty($data->type) && $data->type == 1 ? 'selected="selected"' : '' }}>For Postal
												</option>
												<option value="2" {{ !empty($data->type) && $data->type == 2 ? 'selected="selected"' : '' }}>For Gift Hub
												</option>
												<option value="3" {{ !empty($data->type) && $data->type == 3 ? 'selected="selected"' : '' }}>Corporate & Charity
												</option>
											</select>
										</div>
									</div>

                                    <div class="form-group row">
                                        <label for="Status" class="col-sm-2 col-form-label">Category</label>
                                        <div class="col-sm-10">
                                            <select class="form-control select2" style="width: 100%;" id="occasion_id"
                                                name="occasion_id" required="true">
                                                <option value="">Select Category</option>
                                                @foreach ($occasions as $occasion)
                                                    <option value="{{ $occasion->id }}"
                                                        {{ !empty($data->occasion_id) && $data->occasion_id === $occasion->id ? 'selected="selected"' : '' }}>
                                                        {{ $occasion->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="Name" class="col-sm-2 col-form-label">Sub category Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control name" id="name" name="name"
                                                value="{{ $data->name }}" placeholder="Name" maxlength="40" required/>
                                        </div>
                                    </div>




                                    <div class="form-group row">
                                        <label for="Status" class="col-sm-2 col-form-label">Status</label>
                                        <div class="col-sm-10">
                                            <select class="form-control select2" style="width: 100%;" name="status"
                                                id="status" required>
                                                <option value="0" {{ $data->status == 0 ? 'selected' : '' }}>Inactive
                                                </option>
                                                <option value="1" {{ $data->status == 1 ? 'selected' : '' }}>Active
                                                </option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-info">Update Sub Category</button>
                                    <a href="{{ route('Admin.subcategory.index') }}"
                                        class="btn btn-default float-right">Cancel</a>
                                </div>
                                <!-- /.card-footer -->
                            </form>
                        </div>

                    </div>
                    <div class="col-1"></div>
                </div>
            </div>
        </section>

    </div>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

    <script>
        $(document).ready(function() {
            $('body').on('change', '#occasion_id', function() {
                var id = $(this).val();
                idBasedData(id);
            });

            $('body').on('change', '#type', function() {
                var id = $(this).val();
                typeBasedOccasion(id);
            });


        });


        function idBasedData(id){
            var APP_URL = {!! json_encode(url('/')) !!};

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: APP_URL+'/admin/product/CategoryList/'+id,
                //url: APP_URL+product.categoryList+id,

                type: "POST",
                data: {
                    "id": id,
                    "_method":'POST',
                },
                success: function (response) {
                    orderData = JSON.parse(response);
                    //Render order listing
                    renderOrderOption(orderData);
                },
                error: function(data) {
                    //console.log("IN ERRROR "+data);
                }
            });
        }

        function renderOrderOption(data) {
            var html = '<option value="">Select category</option>';

            $(data).each(function(key, val){

                html += '<option value="'+val.id+'">'+val.name+'</option>';

            });
            $('#category_id').html(html);
        }

        function typeBasedOccasion(id){
            var APP_URL = {!! json_encode(url('/')) !!};

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: APP_URL+'/admin/product/occasionListBasedTypeId/'+id,
                //url: APP_URL+product.categoryList+id,

                type: "POST",
                data: {
                    "id": id,
                    "_method":'POST',
                },
                success: function (response) {
                    orderData = JSON.parse(response);
                    //Render order listing
                    renderOccasion(orderData);
                },
                error: function(data) {
                    //console.log("IN ERRROR "+data);
                }
            });
        }

        function renderOccasion(data) {
            var html = '<option value="">Select Occasion</option>';

            $(data).each(function(key, val){

                html += '<option value="'+val.id+'">'+val.name+'</option>';

            });
            $('#occasion_id').html(html);
        }

    </script>

    <!-- </div> -->
@endsection
