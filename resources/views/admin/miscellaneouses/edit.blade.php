@extends('admin.index')
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <!--  -->
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('Admin.Dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Update Miscellaneous Types</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-1"></div>
                <div class="col-10">
                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Update Miscellaneous Types<strong></strong></h3>
                        </div>
                        @include('admin.partials.messages')
                        <form class="form-horizontal" method="post" action="{{route('Admin.miscellaneous.update',$mis->id)}}" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            {{-- <input type="hidden" name="id" value="{{$paper->id}}" /> --}}
                            <div class="card-body">
                 <div class="form-group row">
                    <label for="Name" class="col-sm-2 col-form-label">Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="name" name="name" value="{{$mis->name}}" placeholder="Name" />
                    </div>
                </div>
                 <div class="form-group row">
                    <label for="Name" class="col-sm-2 col-form-label">Price</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control" id="price" name="price" step="any" value="{{$mis->price}}" placeholder="Price" />
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-info">Update Miscellaneous Types</button>
                    <a href="{{route('Admin.miscellaneous.index')}}" class="btn btn-default float-right">Cancel</a>
                </div>
            </div>
        </div>
    </section>

</div>

<!-- </div> -->
@endsection


@section('after-scripts')
<script src="{{asset('public/plugins/summernote/summernote-bs4.min.js')}}"></script>
<script>
  $(function () {
    // Summernote
    $('.textarea').summernote()
  })
</script>
@endsection
@section('after-style')
<link rel="stylesheet" href="{{asset('public/plugins/summernote/summernote-bs4.css')}}">
@endsection
