@extends('admin.index')

@section('after-style')

<link rel="stylesheet" href="{{asset('public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

@endsection

@section('content')

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('Admin.Dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Manage Designer Request</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Manage Designer Request</h3>

                             {{-- <a href="{{route('Admin.faq.create')}}" style="float: right;"><i class="fas fa-plus-square" style="font-size:25px;"></i></a> --}}
                        </div>


                        @if(Session::has('success'))
                        <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
                        @elseif(Session::has('message'))
                        <div class="alert alert-danger" role="alert">{{Session::get('message')}}</div>
                     @endif
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="user-list" class="table table-bordered table-striped" >
                                <thead>
                                    <tr>
                                        <th>Sr No.</th>
										<th>From</th>
										<th>To</th>
                                        <th>Status</th>
                                        <th>Image_1</th>
                                        <th>Image_2</th>
                                        <th>Image_3</th>
                                        <th>Image_4</th>
                                        <th>Attachment original name</th>
                                        <th>Created At</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php $i=1; @endphp

                                    @if(isset($data) && $data != null)
                                        @foreach($data as $val)
                                        @php
                                            $user = \App\User::where('id',$val->to_user_id)->first();
                                        @endphp

                                        <?php if(!empty($user->name)){ ?>



                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>{{$val->name}}</td>
                                                <td>
                                                    {{ !empty($user->name) ? $user->name : null }}
                                                </td>
                                                <td>@if ($val->is_request_accepted == 0)
                                                    Pending
                                                @elseif ($val->is_request_accepted == 1)
                                                    Accepted
                                                @else
                                                    Rejected
                                                @endif</td>
                                                <td>
                                                    @if ($val->image)
                                                        <img src="{{ asset("storage/product/".$val->image) }}" class="img-fluid ${3|rounded-top,rounded-right,rounded-bottom,rounded-left,rounded-circle,|}" alt="">
                                                    @else
                                                        "N/A"
                                                    @endif
                                                </td>
                                                <td>
                                                    @if ($val->image_2)
                                                    <img src="{{ asset("storage/product/".$val->image_2) }}" class="img-fluid ${3|rounded-top,rounded-right,rounded-bottom,rounded-left,rounded-circle,|}" alt="">
                                                @else
                                                    "N/A"
                                                @endif
                                                </td>
                                                <td> @if ($val->image_3)
                                                    <img src="{{ asset("storage/product/".$val->image_3) }}" class="img-fluid ${3|rounded-top,rounded-right,rounded-bottom,rounded-left,rounded-circle,|}" alt="">
                                                @else
                                                    "N/A"
                                                @endif</td>
                                                <td> @if ($val->image_4)
                                                    <img src="{{ asset("storage/product/".$val->image_4) }}" class="img-fluid ${3|rounded-top,rounded-right,rounded-bottom,rounded-left,rounded-circle,|}" alt="">
                                                @else
                                                    "N/A"
                                                @endif</td>
                                                <td> @if ($val->attachment_original_name)
                                                    <img src="{{ asset("storage/product/".$val->attachment_original_name) }}" class="img-fluid ${3|rounded-top,rounded-right,rounded-bottom,rounded-left,rounded-circle,|}" alt="">
                                                @else
                                                    "N/A"
                                                @endif</td>
                                                <td>{{$val->create_at}}</td>
                                            </tr>


                                        <?php } ?>
                                        @endforeach
                                    @endif
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection
@section('after-scripts')
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>

<script>
    $(function() {

        $("#user-list").DataTable({
            "responsive": true,
            "autoWidth": false,
            order : [[4, 'desc']]
        });
    });
</script>
@endsection
