@extends('admin.index')

@section('after-style')

<link rel="stylesheet" href="{{asset('public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

@endsection

@section('content')

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('Admin.Dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Manage
                            <?php
                                if(!empty($page) && $page == "vendor"){
                                    echo "Gifting Vendor List";
                                } else if(!empty($page) && $page == "printervendor"){
                                    echo "Printing Service Provider List";
                                } else if(!empty($page) && $page == "securePrinterList"){
                                    echo "Secure Printing Service Provider List";
                                } else {
                                    echo "User List";
                                }
                            ?>

                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <?php
                                    if(!empty($page) && $page == "vendor"){
                                        echo "Gifting Vendor List";
                                    } else if(!empty($page) && $page == "printervendor"){
                                        echo "Printing Service Provider List";
                                    } else if(!empty($page) && $page == "securePrinterList"){
                                        echo "Secure Printing Service Provider List";
                                    } else {
                                        echo "User List";
                                    }
                                ?>

                            </h3>

                            <!-- <a href="{{route('Admin.Add.User')}}" style="float: right;"><i class="fas fa-plus-square" style="font-size:25px;"></i></a> -->
                        </div>


                        @if(session('message'))
                            <div class="alert alert-success" style="padding: 5px 20px;margin-bottom: 5px;">{{ session('message') }}</div>
                        @endif
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="user-list" class="table table-bordered table-striped" data-order='[[ 0, "asc" ]]'>
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>User Type</th>
                                        <th>Business Name</th>
                                        <th>Business Registration No</th>
                                        <th>Business Phone No</th>
                                        <th>Website</th>
                                        <th>House Number</th>
                                        <th>Street Address</th>
                                        <th>Land Mark</th>
                                        <th>Postcode</th>
                                        <th>State</th>
                                        <th>Country</th>
                                        <th>Contact Person 1</th>
                                        <th>Cell phone</th>
                                        <th>Contact Person 2</th>
                                        <th>Cell phone</th>
                                        <th>Profile Image</th>
                                        @if(!empty($page) && (($page == "vendor") || ($page == "printervendor")))

                                        @else
                                            <th>Gender</th>
                                            <th>House Number</th>
                                            <th>Land Mark</th>
                                            <th>City</th>
                                            <th>Zip Code</th>
                                            <th>State</th>
                                        @endif

                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php $i=1; @endphp

                                    @if(isset($users) && $users != null)
                                        @foreach($users as $user)

                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>{{$user->name}}</td>
                                                <td>{{$user->email}}</td>
                                                <td>{{$user->phone}}</td>

                                                <td style="text-align:center">
                                                    @if($user->user_type == 1)
                                                        <p>Male</p>
                                                    @elseif($user->user_type == 2)
                                                        <p>User</p>
                                                    @elseif($user->user_type == 3)
                                                        <p>Designer</p>
                                                    @elseif($user->user_type == 4)
                                                        <p>Merchant Vendor</p>
                                                    @elseif ($user->user_type == 5)
                                                        <p>Printer Vendor</p>
                                                    @endif
                                                </td>
                                                <td>{{$user->business_name}}</td>
                                                <td>{{$user->business_registration_no}}</td>
                                                <td>{{$user->business_phone_no}}</td>
                                                <td>{{$user->website}}</td>
                                                <td>{{$user->house_number}}</td>
                                                <td>{{$user->street_address}}</td>
                                                <td>{{$user->land_mark}}</td>
                                                <td>{{$user->zip_code}}</td>
                                                <td>{{$user->state}}</td>
                                                <td>{{$user->country}}</td>
                                                <td>{{$user->contact_person_1}}</td>
                                                <td>{{$user->contact_number_1}}</td>
                                                <td>{{$user->contact_person_2}}</td>
                                                <td>{{$user->contact_number_2}}</td>

                                                <td><img src={{asset("storage/profile_pictures/".$user->profile_image)}} class="img-fluid ${3|rounded-top,rounded-right,rounded-left,rounded-circle,|}" width="60%" style="border:3px solid white" alt=""></td>

                                                @if(!empty($page) && (($page == "vendor") || ($page == "printervendor")))

                                                @else
                                                    <td style="text-align:center">
                                                        @if($user->gender == 1)
                                                            <p>Male</p>
                                                        @elseif($user->gender == 2)
                                                            <p>Female</p>
                                                        @elseif($user->gender == 3)
                                                            <p>Corporate</p>
                                                        @endif
                                                    </td>

                                                    <td>
                                                        {{$user->house_number}}
                                                    </td>
                                                    <td>
                                                        {{$user->land_mark}}
                                                    </td>
                                                    <td>
                                                        {{$user->city}}
                                                    </td>
                                                    <td>
                                                        {{$user->zip_code}}
                                                    </td>
                                                    <td>
                                                        {{$user->state}}
                                                    </td>
                                                @endif

                                               <!--  <td>
                                                    @if($user->assign_nurse)
                                                    {{$user->assign_nurse}}
                                                    @else
                                                    NA
                                                    @endif
                                                </td> -->

                                                <td style="text-align:center">
                                                    @if($user->status == 1)
                                                        <p style="color:green">Active</p>
                                                    @elseif($user->status == 0)
                                                        <p style="color:red">In-active</p>
                                                    @endif
                                                </td>

                                                <td style="text-align: center;">
                                                    {{-- <a href="javascript:void();" data-toggle="modal" data-target="#user-info{{$user->id}}"><i class="fas fa-eye"></i>&nbsp;&nbsp;</a> --}}
                                                    <a href="{{route('Admin.Get.User',[$user->id])}}?page={{(!empty($page)) ? $page :null}}"><i class="fas fa-edit"></i>&nbsp;&nbsp;</a>
                                                    <a href="{{route('Admin.Delete.User',[$user->id])}}?page={{(!empty($page)) ? $page :null}}" onclick="return confirm('Are you sure?')"><i class="fas fa-trash"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    @include('admin.user.view_user')
</div>

@endsection

@section('after-scripts')
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>

<script>
    $(function() {
        $("#user-list").DataTable({
            "responsive": true,
            "autoWidth": false,
        });
    });
</script>
@endsection
