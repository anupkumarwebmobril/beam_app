@extends('admin.index')
<meta name="csrf-token" content="{{ csrf_token() }}" />
@section('content')
    <!-- <div class="wrapper"> -->

    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <!--  -->
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('Admin.Dashboard') }}">Home</a></li>
                            <li class="breadcrumb-item active">Update User</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-1"></div>
                    <div class="col-10">

                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">Update <strong>{{ $user->name }}</strong></h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            @include('admin.partials.messages')
                            <form class="form-horizontal" method="post" action="{{ route('Admin.Update.User') }}">
                                @csrf
                                <input type="hidden" name="id" value="{{ $user->id }}" />
                                <div class="card-body">

                                    <div class="form-group row">
                                        <label for="Name" class="col-sm-2 col-form-label">Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control name" id="name" name="name"
                                                value="{{ $user->name }}" placeholder="Name" maxlength="40" />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="Email" class="col-sm-2 col-form-label">Email</label>
                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" id="email" name="email"
                                                value="{{ $user->email }}" placeholder="Email"
                                                pattern="[^@\s]+@[^@\s]+\.[^@\s]+" title="Invalid email address" />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="Mobile" class="col-sm-2 col-form-label">Mobile</label>
                                        <div class="col-sm-10">
                                            {{-- <input type="text" class="form-control" id="phone" name="phone"
                                                value="{{ $user->phone }}" placeholder="Phone"
                                                onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))"
                                                maxlength="15" readonly="true"/> --}}
                                            <input type="text" class="form-control" id="phone" name="phone"
                                                value="{{ $user->phone }}" placeholder="Phone"
                                                maxlength="15" readonly="true"/>
                                        </div>
                                    </div>

                                    <input type="hidden" name="page" value="<?php echo !empty($page) ? $page : null; ?>" />
                                    <!-- <div class="form-group row">
                                        <label for="password" class="col-sm-2 col-form-label">Password</label>
                                        <div class="col-sm-10">
                                            <input type="password" class="form-control" id="password" name="password" placeholder="Password" />
                                        </div>
                                    </div>   -->


                                    <!-- <div class="form-group row">
                                        <label for="Nurse" class="col-sm-2 col-form-label">Choose a Nurse</label>
                                        <div class="col-sm-10">
                                            <select class="form-control select2" style="width: 100%;" name="assign_nurse" id="">
                                                <option value="" hidden>Select Nurse</option>
                                                @foreach ($nurse as $nurse_data)
    <option>{{ $nurse_data->name }}</option>
    @endforeach
                                            </select>
                                        </div>
                                    </div> -->




                                    @if(($user->user_type != 4) && $user->user_type != 5 && $user->user_type != 6)
                                        <div class="form-group row">
                                            <label for="Name" class="col-sm-2 col-form-label">House Number</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="house_number" name="house_number"
                                                    value="{{ $user->house_number }}" placeholder="House Number" />
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="Name" class="col-sm-2 col-form-label">Land Mark</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="land_mark" name="land_mark"
                                                    value="{{ $user->land_mark }}" placeholder="Land Mark" />
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="Name" class="col-sm-2 col-form-label">City</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="city" name="city"
                                                    value="{{ $user->city }}" placeholder="City" />
                                            </div>
                                        </div>
                                    @endif

                                    @if(($user->user_type != 4))



                                        <div class="form-group row">
                                            <label for="Name" class="col-sm-2 col-form-label">State</label>
                                            <div class="col-sm-10">
                                                <select class="form-control select2" style="width: 100%;" id="state_printer" name="state">
                                                    <option value="">Select State</option>
                                                    @foreach ($postalState as $state)
                                                        <option value="{{$state->state_name}}" {{$state->state_name == $user->state ?  'selected' : ''}} >{{$state->state_name}}</option>
                                                    @endforeach

                                                </select>
                                                {{-- <input type="text" class="form-control" id="state" name="state"
                                                    value="{{ $user->state }}" placeholder="State" /> --}}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="Name" class="col-sm-2 col-form-label">Zip Code</label>
                                            <div class="col-sm-10">
                                                <select class="form-control select2" style="width: 100%;" id="zip_code_printer" name="zip_code">
                                                    @php
                                                        $postcode  = db::table('import_postals')->get();
                                                    @endphp
                                                    @foreach ($postcode as $post )

                                                    <option value="{{$post->postal_code}}" {{$post->postal_code == $user->zip_code ? 'selected' : ''}}>{{$post->postal_code}}</option>
                                                    @endforeach
                                                </select>
                                                {{-- <input type="text" class="form-control" id="zip_code" name="zip_code"
                                                    value="{{ $user->zip_code }}" placeholder="Zip Code" /> --}}
                                            </div>
                                        </div>

                                    @endif
                                    <div class="form-group row">
                                        <label for="Status" class="col-sm-2 col-form-label">Status</label>
                                        <div class="col-sm-10">
                                            <select class="form-control select2" style="width: 100%;" name="status"
                                                id="status" required>
                                                <option value="0" {{ $user->status == 0 ? 'selected' : '' }}>Inactive
                                                </option>
                                                <option value="1" {{ $user->status == 1 ? 'selected' : '' }}>Active
                                                </option>
                                            </select>
                                        </div>
                                    </div>

                                </div>

                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-info">Update User</button>
                                    @if(!empty($page) && $page == "vendor")
                                        <a href="{{ route('Admin.Vendor.List') }}" class="btn btn-default float-right">Cancel</a>
                                    @elseif(!empty($page) && $page == "printervendor")
                                        <a href="{{ route('Admin.Vendor.PrinterList') }}" class="btn btn-default float-right">Cancel</a>
                                    @elseif(!empty($page) && $page == "securePrinterList")
                                        <a href="{{ route('Admin.Vendor.securePrinterList') }}" class="btn btn-default float-right">Cancel</a>
                                    @else
                                        <a href="{{ route('Admin.Users.List') }}" class="btn btn-default float-right">Cancel</a>
                                    @endif

                                </div>
                                <!-- /.card-footer -->
                            </form>
                        </div>

                    </div>
                    <div class="col-1"></div>
                </div>
            </div>
        </section>

    </div>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.name').on('keypress', function(e) {
                var regex = new RegExp("^[a-zA-Z ]*$");
                var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                if (regex.test(str)) {
                    return true;
                }
                e.preventDefault();
                return false;
            });
        });
    </script>
    <script>


        $('body').on('change', '#state_printer', function() {
        var id = $(this).val();
        zipcodeBasedState(id);


    });
        function zipcodeBasedState(id){
        var APP_URL = {!! json_encode(url('/')) !!};

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: APP_URL+'/admin/printerZipBasedState/'+id,

            type: "POST",
            data: {
                "id": id,
                "_method":'POST',
            },
            success: function (response) {
                orderData = JSON.parse(response);
                //Render order listing
                renderZip(orderData);
            },
            error: function(data) {
                //console.log("IN ERRROR "+data);
            }
        });
    }

        function renderZip(data) {
            var html = '<option value="">Select Postal code</option>';

            $(data).each(function(key, val){

                html += '<option value="'+val.postal_code+'">'+val.postal_code+'</option>';

            });
            $('#zip_code_printer').html(html);
    }
    </script>
    <!-- </div> -->
@endsection
