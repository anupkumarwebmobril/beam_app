@foreach($data as $view)
<div class="modal fade" id="user-info{{$view->id}}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Viewing <strong>{{$view->name}}</strong></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Name" class="col-form-label">Name</label>
                            <div class="col-sm-10">
                                <p>{{$view->name}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Email" class="col-form-label">Email</label>
                            <div class="col-sm-10">
                                <p>{{$view->email}}</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Mobile" class="col-form-label">Mobile</label>
                            <div class="col-sm-10">
                                <p>{{$view->mobile_number}}</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label for="Status" class="col-form-label">Post Code</label>
                            <div class="col-sm-10">
                               <p>{{$view->post_code}}</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label for="Status" class="col-form-label">Unit</label>
                            <div class="col-sm-10">
                                <p>{{$view->unit}}</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Mobile" class="col-form-label">Medical Card Number</label>
                            <div class="col-sm-10">
                                <p>{{$view->medical_card_number}}</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label for="Status" class="col-form-label">Preferred Name</label>
                            <div class="col-sm-10">
                                 <p>{{$view->preferred_name}}</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label for="Mobile" class="col-form-label">DOB</label>
                            <div class="col-sm-10">
                                <p>{{$view->dob}}</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Status" class="col-form-label">Gender</label>
                            <div class="col-sm-10">
                                <p>{{$view->gender}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Mobile" class="col-form-label">Emergency Contact Number</label>
                            <div class="col-sm-10">
                                <p>{{$view->emergency_contact_no}}</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label for="Status" class="col-form-label">Emergency Name</label>
                            <div class="col-sm-10">
                                <p>{{$view->emergency_name}}</p>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Mobile" class="col-form-label">Address</label>
                            <div class="col-sm-10">
                                <p>{{$view->address}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Mobile" class="col-form-label">City</label>
                            <div class="col-sm-10">
                                <p>{{$view->city}}</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label for="Status" class="col-form-label">Province</label>
                            <div class="col-sm-10">
                                <p>{{$view->province}}</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Mobile" class="col-form-label">Medical Issue</label>
                            <div class="col-sm-10">
                                <p>{{$view->medical_issue}}</p>
                            </div>
                        </div>
                    </div>

                    <!-- <div class="col-4">
                        <div class="form-group">
                            <label for="Status" class="col-form-label">Assign Nurse</label>
                            <div class="col-sm-10">
                                <p>{{$view->assign_nurse}}</p>
                            </div>
                        </div>
                    </div> -->
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Mobile" class="col-form-label">Since</label>
                            <div class="col-sm-10">
                                <p>{{$view->since}}</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                   <div class="col-4">
                        <div class="form-group">
                            <label for="Status" class="col-form-label">Status</label>
                            <div class="col-sm-10">
                                @if($view->status == 1)
                                    <p style="color:green">Active</p>
                                @elseif($view->status == 0)
                                    <p style="color:red">In-active</p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>



            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endforeach
