@extends('admin.index')
<meta name="csrf-token" content="{{ csrf_token() }}" />
@section('content')
    <!-- <div class="wrapper"> -->

    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <!--  -->
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('Admin.Dashboard') }}">Home</a></li>
                            <li class="breadcrumb-item active">Update Product</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-1"></div>
                    <div class="col-10">

                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">Update <strong>{{ $product->name }}</strong></h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            @include('admin.partials.messages')
                           	<form class="form-horizontal" method="post" action="{{route('Admin.product.update',['product'=>$product->id])}}" enctype="multipart/form-data" autocomplete="off">
                                @csrf
								@method('PUT')
                                <input type="hidden" name="id" value="{{ $product->id }}" />
                                <div class="card-body">

									<div class="form-group row">
										<label for="Status" class="col-sm-2 col-form-label">Product Type</label>
										<div class="col-sm-10">
											<select class="form-control select2" style="width: 100%;" name="type"
												id="type" required="true">
												{{-- <option value="">Select product type</option> --}}
												<option value="1" {{ !empty($product->type) && $product->type == 1 ? 'selected="selected"' : '' }}>For Postal
												</option>
												{{-- <option value="2" {{ !empty($product->type) && $product->type == 2 ? 'selected="selected"' : '' }}>For Gift Hub
												</option> --}}

											</select>
										</div>
									</div>


                                    <div class="form-group row">
                                        <label for="Status" class="col-sm-2 col-form-label">Category</label>
                                        <div class="col-sm-10">
                                            <select class="form-control select2" style="width: 100%;" id="occasion_id"
                                                name="occasion_id" required="true">
                                                <option value="">Select Category</option>
                                                @foreach ($occasions as $occasion)

                                                    <option value="{{ $occasion->id }}"
                                                        <?php echo !empty($product->occasion_id) && $product->occasion_id == $occasion->id ? 'selected="selected"' : ''; ?>>
                                                        {{ $occasion->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="Status" class="col-sm-2 col-form-label">Sub Category</label>
                                        <div class="col-sm-10">
                                            <select class="form-control select2" style="width: 100%;" id="category_id"
                                                name="category_id" required="true">
                                                <option value="">Select Sub Category</option>
                                                @foreach ($category as $cat)
                                                    <option value="{{ $cat->id }}"
                                                        {{ !empty($product->category_id) && $product->category_id == $cat->id ? 'selected="selected"' : '' }}>
                                                        {{ $cat->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    {{-- <div class="form-group row">
                                    <label for="Status" class="col-sm-2 col-form-label">Vendor</label>
                                    <div class="col-sm-10">
                                        <select class="form-control select2" style="width: 100%;" id="vendor"
                                            name="vendor" required="true" >
                                            <option value="" >Select Vendor</option>
                                            @foreach ($checks as $cats)
                                                <option value="{{ $cats->id }}" selected
                                                    >
                                                    {{ $cats->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div> --}}
                                    <div class="form-group row">
                                        <label for="Name" class="col-sm-2 col-form-label">Product Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control name" id="name" name="name"
                                                value="{{ $product->name }}" placeholder="Name" maxlength="40" required />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="Email" class="col-sm-2 col-form-label">Shipping Fee</label>
                                        <div class="col-sm-10">
                                            <input id="shipping_fee" name="shipping_fee" type="number" class="form-control" required="true" value="{{ !empty($product->shipping_fee) ? $product->shipping_fee : ''}}" step=".01">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="Email" class="col-sm-2 col-form-label">Price</label>
                                        <div class="col-sm-10">
                                            <input id="price_without_shipping" name="price_without_shipping" type="number" class="form-control" required="true" value="{{ !empty($product->price_without_shipping) ? $product->price_without_shipping : ''}}" step=".01" placeholder="Price">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="Email" class="col-sm-2 col-form-label">Total Price (Price + Shipping Fee)</label>
                                        <div class="col-sm-10">
                                            <input id="price" name="price" type="number" class="form-control" required="true" value="{{ !empty($product->price) ? $product->price : ''}}" step=".01" placeholder="Total Price (Price + Shipping Fee)">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="Email" class="col-sm-2 col-form-label">Product Code </label>
                                        <div class="col-sm-10">
                                            <input id="product_code" name="product_code" type="text" class="form-control" required="true" value="{{ !empty($product->product_code) ? $product->product_code : ''}}"  placeholder="Enter product code">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="Email" class="col-sm-2 col-form-label">Min order quantity</label>
                                        <div class="col-sm-10">
                                            <input id="price" name="min_order_qty" type="number" class="form-control" value="{{ !empty($product->min_order_qty) ? $product->min_order_qty : ''}}" placeholder="Enter min order quantity ">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="Mobile" class="col-sm-2 col-form-label">Product Images1</label>
                                        <div class="col-sm-10">
                                            <input type="file" class="form-control customFileUplIpt" id="image" name="image" accept="image/png, image/jpg, image/jpeg">
												<img src="<?php echo !empty($product->image) ? asset("storage/product/".$product->image) : '' ?>"  style="max-height:300px; max-width:300px;"/>
                                                <label for="Mobile" class="col-sm-12 col-form-label" style="font-size:14px;">The card image should be 437:620 ratio , with minimum 1748x2480 px, Image dpi 300, Extension .jpeg .png and jpg only.</label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="Mobile" class="col-sm-2 col-form-label">Product Images2</label>
                                        <div class="col-sm-10">
                                            <input type="file" class="form-control customFileUplIpt" id="image" name="image_2" accept="image/png, image/jpg, image/jpeg">
                                                <img src="<?php echo !empty($product->image_2) ? asset("storage/product/".$product->image_2) : '' ?>"  style="max-height:300px; max-width:300px;"/>
                                                <label for="Mobile" class="col-sm-12 col-form-label" style="font-size:14px;">The card image should be 437:620 ratio , with minimum 1748x2480 px, Image dpi 300, Extension .jpeg .png and jpg only.</label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="Mobile" class="col-sm-2 col-form-label">Product Image3</label>
                                        <div class="col-sm-10">
                                            <input type="file" class="form-control customFileUplIpt" id="image" name="image_3" accept="image/png, image/jpg, image/jpeg">
                                                <img src="<?php echo !empty($product->image_3) ? asset("storage/product/".$product->image_3) : '' ?>"  style="max-height:300px; max-width:300px;"/>
                                                <label for="Mobile" class="col-sm-12 col-form-label" style="font-size:14px;">The card image should be 437:620 ratio , with minimum 1748x2480 px, Image dpi 300, Extension .jpeg .png and jpg only.</label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="Mobile" class="col-sm-2 col-form-label">Product Image4</label>
                                        <div class="col-sm-10">
                                            <input type="file" class="form-control customFileUplIpt" id="image" name="image_4" accept="image/png, image/jpg, image/jpeg">
                                                <img src="<?php echo !empty($product->image_4) ? asset("storage/product/".$product->image_4) : '' ?>"  style="max-height:300px; max-width:300px;"/>
                                                <label for="Mobile" class="col-sm-12 col-form-label" style="font-size:14px;">The card image should be 437:620 ratio , with minimum 1748x2480 px, Image dpi 300, Extension .jpeg .png and jpg only.</label>
                                        </div>
                                    </div>

                                    <?php if($product->type == 2){ ?>
                                        <div class="form-group row">
                                            <label for="Mobile" class="col-sm-2 col-form-label">Product Image5</label>
                                            <div class="col-sm-10">
                                                <input type="file" class="form-control customFileUplIpt" id="image" name="image_5" accept="image/png, image/jpg, image/jpeg">
                                                    <img src="<?php echo !empty($product->image_5) ? asset("storage/product/".$product->image_5) : '' ?>"  style="max-height:300px; max-width:300px;"/>
                                                    <label for="Mobile" class="col-sm-12 col-form-label" style="font-size:14px;">For Gift the ratio should be 1:1 ( Square ) with minimum 800x800px, Image dpi of 100dpi, Extension .jpeg .png and jpg only.</label>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="Mobile" class="col-sm-2 col-form-label">Product Image6</label>
                                            <div class="col-sm-10">
                                                <input type="file" class="form-control customFileUplIpt" id="image" name="image_6" accept="image/png, image/jpg, image/jpeg">
                                                    <img src="<?php echo !empty($product->image_6) ? asset("storage/product/".$product->image_6) : '' ?>"  style="max-height:300px; max-width:300px;"/>
                                                    <label for="Mobile" class="col-sm-12 col-form-label" style="font-size:14px;">For Gift the ratio should be 1:1 ( Square ) with minimum 800x800px, Image dpi of 100dpi, Extension .jpeg .png and jpg only.</label>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="Mobile" class="col-sm-2 col-form-label">Product Image7</label>
                                            <div class="col-sm-10">
                                                <input type="file" class="form-control customFileUplIpt" id="image" name="image_7" accept="image/png, image/jpg, image/jpeg">
                                                    <img src="<?php echo !empty($product->image_7) ? asset("storage/product/".$product->image_7) : '' ?>"  style="max-height:300px; max-width:300px;"/>
                                                    <label for="Mobile" class="col-sm-12 col-form-label" style="font-size:14px;">For Gift the ratio should be 1:1 ( Square ) with minimum 800x800px, Image dpi of 100dpi, Extension .jpeg .png and jpg only.</label>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="Mobile" class="col-sm-2 col-form-label">Product Image8</label>
                                            <div class="col-sm-10">
                                                <input type="file" class="form-control customFileUplIpt" id="image" name="image_8" accept="image/png, image/jpg, image/jpeg">
                                                    <img src="<?php echo !empty($product->image_8) ? asset("storage/product/".$product->image_8) : '' ?>"  style="max-height:300px; max-width:300px;"/>
                                                    <label for="Mobile" class="col-sm-12 col-form-label" style="font-size:14px;">For Gift the ratio should be 1:1 ( Square ) with minimum 800x800px, Image dpi of 100dpi, Extension .jpeg .png and jpg only.</label>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="Mobile" class="col-sm-2 col-form-label">Product Image9</label>
                                            <div class="col-sm-10">
                                                <input type="file" class="form-control customFileUplIpt" id="image" name="image_9" accept="image/png, image/jpg, image/jpeg">
                                                    <img src="<?php echo !empty($product->image_9) ? asset("storage/product/".$product->image_9) : '' ?>"  style="max-height:300px; max-width:300px;"/>
                                                    <label for="Mobile" class="col-sm-12 col-form-label" style="font-size:14px;">For Gift the ratio should be 1:1 ( Square ) with minimum 800x800px, Image dpi of 100dpi, Extension .jpeg .png and jpg only.</label>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="Mobile" class="col-sm-2 col-form-label">Product Image10</label>
                                            <div class="col-sm-10">
                                                <input type="file" class="form-control customFileUplIpt" id="image" name="image_10" accept="image/png, image/jpg, image/jpeg">
                                                    <img src="<?php echo !empty($product->image_10) ? asset("storage/product/".$product->image_10) : '' ?>"  style="max-height:300px; max-width:300px;"/>
                                                    <label for="Mobile" class="col-sm-12 col-form-label" style="font-size:14px;">For Gift the ratio should be 1:1 ( Square ) with minimum 800x800px, Image dpi of 100dpi, Extension .jpeg .png and jpg only.</label>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    {{-- <div class="form-group row">
                                        <label for="Mobile" class="col-sm-2 col-form-label">Product Description</label>
                                        <div class="col-sm-10">
                                            <textarea id="description" id="summernote" name="product_description" style="height: 125px; width: 100%; padding: 10px">{!!$product->product_description!!}</textarea>
                                        </div>
                                    </div> --}}

                                    <div class="form-group row">
                                        <label for="Status" class="col-sm-2 col-form-label">Status</label>
                                        <div class="col-sm-10">
                                            <select class="form-control select2" style="width: 100%;" name="status"
                                                id="status" required>
                                                <option value="0" {{ $product->status == 0 ? 'selected' : '' }}>Inactive
                                                </option>
                                                <option value="1" {{ $product->status == 1 ? 'selected' : '' }}>Active
                                                </option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <input type="hidden" name="redirect_url" value="<?php echo !empty($page) ? $page : null; ?>" />
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-info">Update Product</button>
                                    @if(!empty($page) && $page == "inactive")
                                        <a href="{{ route('Admin.product.inactive') }}"
                                            class="btn btn-default float-right">Cancel</a>
                                    @else
                                        <a href="{{ route('Admin.product.index') }}"
                                        class="btn btn-default float-right">Cancel</a>
                                    @endif
                                </div>
                                <!-- /.card-footer -->
                            </form>
                        </div>

                    </div>
                    <div class="col-1"></div>
                </div>
            </div>
        </section>

    </div>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

	<script>
		$(document).ready(function() {
			$('body').on('change', '#occasion_id', function() {
                var id = $(this).val();
                idBasedData(id);
            });

            $('body').on('change', '#type', function() {
                var id = $(this).val();
                typeBasedOccasion(id);
            });
		});
        typeBasedOccasion(1);



		function idBasedData(id){
            var APP_URL = {!! json_encode(url('/')) !!};

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: APP_URL+'/admin/product/CategoryList/'+id,
                //url: APP_URL+product.categoryList+id,

                type: "POST",
                data: {
                    "id": id,
                    "_method":'POST',
                },
                success: function (response) {
                    orderData = JSON.parse(response);
                    //Render order listing
                    renderOrderOption(orderData);
                },
                error: function(data) {
                    //console.log("IN ERRROR "+data);
                }
            });
        }

        function renderOrderOption(data) {
            var html = '<option value="">Select category</option>';

            $(data).each(function(key, val){

                html += '<option value="'+val.id+'">'+val.name+'</option>';

            });
            $('#category_id').html(html);
        }

        function typeBasedOccasion(id){
            var APP_URL = {!! json_encode(url('/')) !!};

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: APP_URL+'/admin/product/occasionListBasedTypeId/'+id,
                //url: APP_URL+product.categoryList+id,

                type: "POST",
                data: {
                    "id": id,
                    "_method":'POST',
                },
                success: function (response) {
                    orderData = JSON.parse(response);
                    //Render order listing
                    //renderOccasion(orderData);
                },
                error: function(data) {
                    //console.log("IN ERRROR "+data);
                }
            });
        }

        function renderOccasion(data) {
            var html = '<option value="">Select Occasion</option>';

            $(data).each(function(key, val){

                html += '<option value="'+val.id+'">'+val.name+'</option>';

            });
            $('#occasion_id').html(html);
        }


	</script>
    <script>
        $(function () {
          // Summernote
          //$('#summernote').summernote()
            $('.summernote').summernote({
                height: 150

            });
          // CodeMirror
          CodeMirror.fromTextArea(document.getElementById("codeMirrorDemo"), {
            mode: "htmlmixed",
            theme: "monokai"
          });
        })
      </script>

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#shipping_fee').on('keyup', function(){
                sum();
            })

            $('#price_without_shipping').on('keyup', function(){
                sum();
            })
        })

        function sum(){
            var shipping_fee = document.getElementById('shipping_fee').value;
            console.log(shipping_fee);
            var price_without_shipping = document.getElementById('price_without_shipping').value;
            var sum = Number(shipping_fee) + Number(price_without_shipping);

            document.getElementById('price').value = sum.toFixed(2);
        }
    </script>
    <!-- </div> -->
@endsection
