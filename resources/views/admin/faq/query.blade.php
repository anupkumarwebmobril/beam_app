@extends('admin.index')

@section('after-style')

<link rel="stylesheet" href="{{asset('public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

@endsection

@section('content')

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('Admin.Dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Manage Query</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Manage Query</h3>

                             {{-- <a href="{{route('Admin.faq.create')}}" style="float: right;"><i class="fas fa-plus-square" style="font-size:25px;"></i></a> --}}
                        </div>


                        @if(Session::has('success'))
                        <div class="alert alert-success" role="alert">{{Session::get('success')}}</div>
                        @elseif(Session::has('message'))
                        <div class="alert alert-danger" role="alert">{{Session::get('message')}}</div>
                     @endif
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="user-list" class="table table-bordered" data-order='[[ 0, "asc" ]]'>
                                <thead>
                                    <tr>
                                        <th>Sr No.</th>
										<th>Email</th>
										<th>Query</th>
                                        <th>Date</th>
										<th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php $i=1; @endphp

                                    @if(isset($data) && $data != null)
                                        @foreach($data as $val)

                                            <tr class="{{  $val->is_highlight == 1 ? 'latest-update' : '' }}" >
                                                <td>{{$i++}}</td>
                                                <td>{{$val->email}}</td>
                                                <td>{{strip_tags($val->query)}}</td>
                                                <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$val->created_at)->tz("Asia/Kuala_Lumpur") }}</td>


                                                <td style="text-align: center;">
                                                    {{-- <a href="{{route('Admin.faq.edit',[$val->id])}}"><i class="fas fa-edit"></i>&nbsp;&nbsp;</a> --}}
                                                    <a href="javascript:void(0);" class="rowDelete" data-id="{{$val->id}}" data-token="{{ csrf_token() }}"><i class="fas fa-trash" ></i></a>

										        </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <style>
        /* Define the background color for the highlighted rows */
        .latest-update {
            background-color: #5a18919d; /* Replace with your desired color */
        }
    </style>
    <!-- /.content -->
    @include('admin.product.view_product')
</div>

@endsection

@section('after-scripts')
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>

<script>
    $(function() {
        $("#user-list").DataTable({
            "responsive": true,
            "autoWidth": false,
        });
    });

    $('body').on('click', '.rowDelete', function() {
        if (confirm("Are you sure?") == true) {
            var APP_URL = {!! json_encode(url('/')) !!};
            var id = $(this).data("id");
            var token = $(this).data("token");

            $(this).closest('tr').remove();
            $.ajax({
            // url: APP_URL+'/vendorproduct/delete/'+id,
                url: APP_URL+'/admin/faq/delete-query/'+id,
                type: "POST",
                data: {
                    "id": id,
                    "_method":'DELETE',
                    "_token": token
                },
                success: function (response) {
                    console.log('data deleted successfully');
                    //window.location.href= APP_URL+'/vendorproduct';
                    location.reload();
                },
                error: function(response) {
                    console.log("IN ERRROR "+response);
                    // window.location.href= window.location.pathname;
                }
            });
        }
    });
</script>
@endsection
