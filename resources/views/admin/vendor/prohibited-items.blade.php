<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="{{asset('public/admin.png')}}">
    <title>Prohibited Iteam</title>
</head>
<body>
    <div class="blog-content-wrap">
        <h1 style="text-align: center">Prohibited Iteam</h1>
        <div class="row blog-content">
            <div class="col-full blog-content__main">
                <h3>INTRODUCTION</h3>

                 <ol>
                <li>It is Seller’s responsibility to ensure that their proposed item complies with all laws and is allowed to be listed for sale in accordance with Beam’s terms and policies before listing the item on the selling platform. For the Sellers’ convenience, Beam has provided below a non-exhaustive guideline on prohibited and restricted items that are not allowed for sale on Beam. Beam will update this guideline from time to time where necessary.
                </li>

                <p></p>



<h3>VIOLATION OF OUR TERMS OF SERVICE</h3>
<p></p>

                <li>Violations of this Prohibited and Restricted Items Policy may subject the Seller to a range of adverse actions, including but not limited to any or all of the following:
                 <ul>
                        <p></p>
                        <li>Listing deletion</li>
                        <p></p>
                        <li>Limits placed on Account privileges </li>
                        <p></p>
                        <li>Account suspension and termination</li>
                        <p></p>
                        <li>Legal actions</li>
                        <p></p>

                    </ul>

                </li>
                <p></p>



<h3>LIST OF PROHIBITED AND RESTRICTED ITEMS</h3>
<p></p>

                         <li>As below:
<ul>
                        <p></p>
                        <li>Animal and wildlife products (including, without limitation, wild animals);</li>
                        <p></p>
                        <li>Artifacts and antiquities;</li>
                        <p></p>
                        <li>Used cosmetics;</li>
                        <p></p>
                        <li>Counterfeit currency and stamps;</li>
                        <p></p>
                        <li>Credit and debit cards;</li>
                        <p></p>
                        <li>Currency or credits including, without limitation, digital currency or credits, and stored value cards;</li>
                        <p></p>
                        <li>Drugs, prescription-only medicines, pharmacy-only medicines, drug-like substances and associated paraphernalia. For more information, please visit; https://www.pharmacy.gov.my/v2/en/apps/iklan </li>
                        <p></p>
                        <li>Telecommunication equipment that has not been registered with the Malaysian Communications and Multimedia Commission (MCMC), and electronic surveillance equipment and other similar electronic equipment such as cable TV, de-scramblers, radar scanners, traffic signal control devices, wiretapping devices and telephone bugging devices;</li>
                        <p></p>
                        <li>Embargoed goods;</li>
                        <p></p>
                        <li>Firearms, weapons such as pepper spray, replicas, and stun guns, etc.;</li>
                        <p></p>
                        <li>Alcohol (a valid license must be submitted to, and approved by, Beam);</li>
                        <p></p>
                        <li>Prohibited Food: For the safety of our Users, Sellers may not list the following food and food-related items on our Site:</li>
                        <p></p>

       </ul>

                </li><li>Listings containing medicinal claims - that is, a claim that the item is intended for use in the diagnosis, cure, mitigation, treatment, or prevention of disease in humans and/or animals, contraception, inducing anaesthesia or otherwise preventing or interfering with the normal operation of a physiological function, whether permanently or temporarily, and whether by way of terminating, reducing or postponing, or increasing or accelerating, the operation of that function or in any other way (for example, pharmaceutical drugs, contact lenses, misbranded dietary supplements);
                </li>
                            <ul>
                <li>Noxious food items - Food which contains any prohibited substances or substances in excess of permitted proportions, adulterated food without fully informing Buyer at the time of sale of the nature of the transaction;
                </li>

                <li>Non-pasteurised dairy products;
                </li>

                <li>Wild mushrooms; and
                </li>

                <li>Any other food items hazardous to human health.
                </li>

                            </ul>
 <p></p>
                <li>Food not falling into the Prohibited Food category above must adhere to these minimum standards and guidelines:
                </li>
                        <ul>
                <li>Expiration dates – all food items must be clearly and properly labelled with an expiration or “use by” date. Expired food items must not be listed.
                </li>

                <li>Sealed containers – all food and related products sold on the Site should be packaged or sealed to ensure that Buyer can identify evidence of tampering or defect; and
                </li>

                <li>Perishable food items - Sellers who list perishable items should clearly identify in the item description the steps that they will take to ensure that the goods are properly packaged.
                </li>

                        </ul>
 <p></p>

                <li>Potentially infringing items: Items including but not limited to replicas, counterfeit items, and unauthorized copies of a product or item which may be in violation of certain copyrights, trademarks, or other intellectual property rights of third parties;
                </li>

 <p></p>

                <li>Services: Unless expressly allowed by Beam, the provision of services, including but not limited to services that are sexual, illegal in nature or in violation of the Terms of Service, are prohibited on Beam’s platform;
                </li>

 <p></p>

                <li>Slot machines;
                </li>

 <p></p>

                <li>Recalled items;
                </li>

 <p></p>

                <li>Shares, stock, other securities and stamps;
                </li>

 <p></p>

                <li>Tobacco or tobacco-related products, including without limitation electric cigarettes/e-juices that contain nicotine;
                </li>

 <p></p>

                <li>Obscene, seditious or treasonous materials;
                </li>

 <p></p>

                <li>Products that ;
                </li>

                <ul>
                <li>relate to campaigns, elections, political issues, terrorism or issues of public debate,
                </li>

                <li>advocate for or against, or attack a politician or political party,
                </li>

                <li>promote or encourage any form of hate, crime, prejudice, rebellion or violence, or
                </li>
                <li>are intended to shock, disgust, promote suicide or self-harm.
                </li>

                </ul>



                   <p></p>
                <li>Publications, books, films, videos and/or video games that do not comply with applicable laws in the country of sale and/or delivery;
                </li>

                   <p></p>
                <li>Stolen goods;
                </li>
                   <p></p>
                <li>Mislabelled goods;
                </li>
                   <p></p>
                <li>Used undergarments; and
                </li>
                   <p></p>
                <li>Any other items that are, or that contain components that are (i) illegal or restricted in the jurisdiction of the Buyer and/or the Seller or which otherwise encourage illegal or restricted activities, or (ii) determined by any governmental or regulatory authority to pose a potential health or safety risk.
                </li>




</ol>



                <p>If you see a listing that violates our policies, please report it to via our support services in the app or via email at support@letsbeam.it. When a policy violation occurs, we will send an email, system message and push notification to Seller to let them know that the listing has been removed from our platform. </p>

                <p>If you have more questions, you can contact us at info@letsbeam.it   </p>





            </div><!-- end blog-content__main -->
        </div> <!-- end blog-content -->


    </div>
</body>
</html>
