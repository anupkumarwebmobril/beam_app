<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="{{asset('public/admin.png')}}">
    <title>Beam Seller/Service Provider Terms Condition</title>
</head>
<body>
    <div class="col-full blog-content__main">
        <h1 style="text-align: center">Beam Seller/Service Provider Terms Condition</h1>
        <h3>INTRODUCTION</h3>

         <ol>
        <li>Welcome to the Beam Marketplace App (the "Beam"). Please read the following Terms of Service carefully before using this Beam or opening a Beam reseller account ("Account") so that you are aware of your legal rights and obligations with respect to LLEAPP Sdn. Bhd (Company Registration No. 201701039136 (1253307-V)) and its affiliates and subsidiaries (individually and collectively, "Beam", "we", "us" or "our"). The "Services" we provide or make available include;
            <ul>
                <p></p>
                <li>The Beam App,</li>
                <p></p>
                <li>The services provided by the Beam and by Beam client software/app made available through the Beam,</li>
                <p></p>
                <li>All information, linked pages, features, data, text, images, photographs, graphics, music, sounds, video (including live streams), messages, tags, content, programming, software, application services (including, without limitation, any mobile application services) or other materials made available through the Beam or its related services ("Content"). </li>
                <p></p>
            </ul>
            Any new features added to or augmenting the Services are also subject to these Terms of Service. These Terms of Service govern your use of Services provided by Beam.
            </li>

        <p></p>
        <li>The Services include an online platform service that provides a place and opportunity for the sale of goods between the buyer (“Buyer”) and the seller (“Seller”) (collectively “you”, “Users” or “Parties”). The actual contract for sale is directly between Buyer and Seller and Beam is not a party to that or any other contract between Buyer and Seller and accepts no obligations in connection with any such contract. Parties to such transaction will be entirely responsible for the sales contract between them, the listing of goods, warranty of purchase and the like. Beam is not involved in the transaction between Users. Beam may or may not pre-screen Users or the Content or information provided by Users. Beam reserves the right to remove any Content or information posted by you on the Beam in accordance to Section 19 herein. Beam cannot ensure that Users will actually complete a transaction.</li>
        <p></p>
        <li>Before becoming a User of the Beam, you must read and accept all of the terms and conditions in, and linked to, these Terms of Service and you must consent to the processing of your personal data as described in the Privacy Policy linked hereto.</li>
        <p></p>
        <li>Beam reserves the right to change, modify, suspend or discontinue all or any part of this Beam or the Services at any time or upon notice as required by local laws. Beam may release certain Services or their features in a beta version, which may not work correctly or in the same way the final version may work, and we shall not be held liable in such instances. Beam may also impose limits on certain features or restrict your access to parts of, or the entire, Beam or Services in its sole discretion and without notice or liability.</li>
        <p></p>
        <li>Beam reserves the right to refuse to provide you access to the Beam or Services or to allow you to open an Account for any reason.</li>
        <ul></ul>
        <p></p>
         <p class="lead">BY USING BEAM SERVICES OR OPENING AN ACCOUNT, YOU GIVE YOUR IRREVOCABLE ACCEPTANCE OF AND CONSENT TO THE TERMS OF THIS AGREEMENT, INCLUDING THOSE ADDITIONAL TERMS AND CONDITIONS AND POLICIES REFERENCED HEREIN AND/OR LINKED HERETO.

IF YOU DO NOT AGREE TO THESE TERMS, PLEASE DO NOT USE OUR SERVICES OR ACCESS THE BEAM. IF YOU ARE UNDER THE AGE OF 18 OR THE LEGAL AGE FOR GIVING CONSENT HEREUNDER PURSUANT TO THE APPLICABLE LAWS IN YOUR COUNTRY (THE “LEGAL AGE”), YOU MUST GET PERMISSION FROM A PARENT OR LEGAL GUARDIAN TO OPEN AN ACCOUNT AND THAT PARENT OR LEGAL GUARDIAN MUST AGREE TO THE TERMS OF THIS AGREEMENT. IF YOU DO NOT KNOW WHETHER YOU HAVE REACHED THE LEGAL AGE, OR DO NOT UNDERSTAND THIS SECTION, PLEASE DO NOT CREATE AN ACCOUNT UNTIL YOU HAVE ASKED YOUR PARENT OR LEGAL GUARDIAN FOR HELP. IF YOU ARE THE PARENT OR LEGAL GUARDIAN OF A MINOR WHO IS CREATING AN ACCOUNT, YOU MUST ACCEPT THE TERMS OF THIS AGREEMENT ON THE MINOR'S BEHALF AND YOU WILL BE RESPONSIBLE FOR ALL USE OF THE ACCOUNT OR COMPANY SERVICES USING SUCH ACCOUNT, WHETHER SUCH ACCOUNT IS CURRENTLY OPEN OR CREATED LATER.
</p>

<h3>PRIVACY</h3>
<p></p>
        <li>Your privacy is very important to us at Beam. To better protect your rights we have provided the Beam’s Privacy Policy to explain our privacy practices in detail. Please review the Privacy Policy to understand how Beam collects and uses the information associated with your Account and/or your use of the Services (the “User Information”). By using the Services or providing information on the Beam, you:
            <ul>
                <p></p>
                <li>Consent to Beam's collection, use, disclosure and/or processing of your Content, personal data and User Information as described in the Privacy Policy;</li>
                <p></p>
                <li>Agree and acknowledge that the proprietary rights of your User Information are jointly owned by you and Beam; and</li>
                <p></p>
                <li>Shall not, whether directly or indirectly, disclose your User Information to any third party, or otherwise allow any third party to access or use your User Information, without Beam’s prior written consent.</li>
            </ul>
            </li>
            <p></p>

            <li>Users in possession of another User’s personal data through the use of the Services (the “Receiving Party”) hereby agree that, they will (i) comply with all applicable personal data protection laws with respect to any such data; (ii) allow the User whose personal data the Receiving Party has collected (the “Disclosing Party”) to remove his or her data so collected from the Receiving Party’s database; and (iii) allow the Disclosing Party to review what information have been collected about them by the Receiving Party, in each case of (ii) and (iii) above, in compliance with and where required by applicable laws.</li>



<h3>LIMITED LICENSE</h3>
<p></p>

        <li>Beam grants you a limited and revocable license to access and use the Services subject to the terms and conditions of these Terms of Service. All proprietary Content, trademarks, service marks, brand names, logos and other intellectual property (“Intellectual Property”) displayed in the Beam are the property of Beam and where applicable, third party proprietors identified in the Beam. No right or licence is granted directly or indirectly to any party accessing the Beam to use or reproduce any Intellectual Property, and no party accessing the Beam shall claim any right, title or interest therein. By using or accessing the Services you agree to comply with the copyright, trademark, service mark, and all other applicable laws that protect the Services, the Beam and its Content. You agree not to copy, distribute, republish, transmit, publicly display, publicly perform, modify, adapt, rent, sell, or create derivative works of any portion of the Services, the Beam or its Content. You also may not, without our prior written consent, mirror or frame any part or whole of the contents of this Beam on any other server or as part of any other website. In addition, you agree that you will not use any robot, spider or any other automatic device or manual process to monitor or copy our Content, without our prior written consent (such consent is deemed given for standard search engine technology employed by Internet search website to direct Internet users to this app or reverse engineer Beam in any way).
        </li>
        <p></p>
        <li>You are welcome to link to the Beam ( Playstore/Appstore ) from your website, provided that your website does not imply any endorsement by or association with Beam. You acknowledge that Beam may, in its sole discretion and at any time, discontinue providing the Services, either in part or as a whole, without notice.
        </li>


<h3>SOFTWARE</h3>
<p></p>

        <li>Any software provided by us to you as part of the Services is subject to the provisions of these Terms of Service. Beam reserves all rights to the software not expressly granted by Beam hereunder. Any third-party scripts or code, linked to or referenced from the Services, are licensed to you by the third parties that own such scripts or code, not by Beam.
        </li>


<h3>ACCOUNTS AND SECURITY</h3>
<p></p>

        <li>Some functions of our Services require registration for an Account by selecting a unique user identification ("User ID") or Mobile Number and password, and by providing certain personal information. If you select a User ID that Beam, in its sole discretion, finds offensive or inappropriate, Beam has the right to suspend or terminate your Account. You may be able to use your Account to gain access to other products, website or services to which we have enabled access or with which we have tied up or collaborated. Beam has not reviewed, and assumes no responsibility for any third party content, functionality, security, services, privacy policies, or other practices of those products, website or services. If you do so, the terms of service for those products, website or services, including their respective privacy policies, if different from these Terms of Service and/or our Privacy Policy, may also apply to your use of those products, website or services.
        </li>
        <p></p>
        <li>You agree to (a) keep your password confidential and use only your User ID or Mobile Number  and password when logging in, (b) ensure that you log out from your account at the end of each session on the Beam, (c) immediately notify Beam of any unauthorised use of your Account, User ID or Mobile Number and/or password, and (d) ensure that your Account information is accurate and up-to-date. You are fully responsible for all activities that occur under your User ID or Mobile Number and Account even if such activities or uses were not committed by you. Beam will not be liable for any loss or damage arising from unauthorised use of your password or your failure to comply with this Section.
        </li>
        <p></p>
        <li>You agree that Beam may for any reason, in its sole discretion and with or without notice or liability to you or any third party, immediately suspend, freeze or terminate your Account and your User ID, remove or discard from the Beam any Content associated with your Account and User ID, withdraw any subsidies offered to you, cancel any transactions associated with your Account and User ID, temporarily or in more serious cases permanently withhold any sale proceeds or refunds, and/or take any other actions that Beam deems necessary. Grounds for such actions may include, but are not limited to, actual or suspected (a) extended periods of inactivity, (b) violation of the letter or spirit of these Terms of Service, (c) illegal, fraudulent, harassing, defamatory, threatening or abusive behaviour (d) having multiple user accounts, (e) buying products on the Beam for the purpose of commercial re-sale, (f) abnormal or excessive purchase of products from the same Seller or related group of Sellers, or (g) behaviour that is harmful to other Users, third parties, or the business interests of Beam. Use of an Account for illegal, fraudulent, harassing, defamatory, threatening or abusive purposes may be referred to law enforcement authorities without notice to you. If a legal dispute arises or law enforcement action is commenced relating to your Account or your use of the Services for any reason, Beam may terminate your Account immediately with or without notice.
        <p></p>
        </li><li>Users may terminate their Account if they notify Beam in writing (including via email at info@letsbeam.it) of their desire to do so. Notwithstanding any such termination, Users remain responsible and liable for any incomplete transaction (whether commenced prior to or after such termination), shipment of the product, payment for the product, or the like, and Users must contact Beam after he or she has promptly and effectively carried out and completed all incomplete transactions according to the Terms of Service. Beam shall have no liability, and shall not be liable for any damages incurred due to the actions taken in accordance with this Section. Users waive any and all claims based on any such action taken by Beam.
        </li>
        <p></p>
        <li>You may only use the Services and/or open an Account if you are located in one of our approved countries, as updated from time to time.
        </li>



<h3>TERM OF USE</h3>
<p></p>
        <li>The license for use of this Beam and the Services is effective until terminated. This license will terminate as set forth under these Terms of Service or if you fail to comply with any term or condition of these Terms of Service. In any such event, Beam may effect such termination with or without notice to you.
        </li>
        <p></p>
             <li>You agree not to:
            <ul>
                <li>Upload, post, transmit or otherwise make available any Content that is unlawful, harmful, threatening, abusive, harassing, alarming, distressing, tortious, defamatory, vulgar, obscene, libelous, invasive of another's privacy, hateful, or racially, ethnically or otherwise objectionable;</li>
                <p></p>
                <li>Violate any laws, including without limitation any laws and regulation in relation to export and import restrictions, third party rights or our Prohibited and Restricted Items Policy;</li>
                <p></p>
                <li>Upload, post, transmit or otherwise make available any Content featuring an unsupervised minor or use the Services to harm minors in any way;</li>
                <p></p>
                <li>Use the Services or upload Content to impersonate any person or entity, or otherwise misrepresent your affiliation with a person or entity;</li>
                <p></p>
                <li>forge headers or otherwise manipulate identifiers in order to disguise the origin of any Content transmitted through the Services;</li>
                <p></p>
                <li>remove any proprietary notices from the Beam;</li>
                <p></p>
                <li>cause, permit or authorize the modification, creation of derivative works, or translation of the Services without the express permission of Beam;</li>
                <p></p>
                <li>use the Services for the benefit of any third party or any manner not permitted by the licenses granted herein;</li>
                <p></p>
                <li>use the Services or upload Content in a manner that is fraudulent, unconscionable, false, misleading or deceptive;</li>
                <p></p>
                <li>open and/or operate multiple user accounts in connection with any conduct that violates either the letter or spirit of these Terms of Service;</li>
                <p></p>
                <li>access the Beam Marketplace, open a user account, or otherwise access your user account using an emulator, simulator, bot or other similar hardware or software;</li>
                <p></p>
                <li>manipulate the price of any item or interfere with other User's listings;</li>
                <p></p>
                <li>take any action that may undermine the feedback or ratings systems;</li>
                <p></p>
                <li>attempt to decompile, reverse engineer, disassemble or hack the Services (or any portion thereof), or to defeat or overcome any encryption technology or security measures implemented by Beam with respect to the Services and/or data transmitted, processed or stored by Beam;</li>
                <p></p>
                <li>harvest or collect any information about or regarding other Account holders, including, without limitation, any personal data or information;</li>
                <p></p>
                <li>upload, email, post, transmit or otherwise make available any Content that you do not have a right to make available under any law or under contractual or fiduciary relationships (such as inside information, proprietary and confidential information learned or disclosed as part of employment relationships or under nondisclosure agreements);</li>
                <p></p>
                <li>upload, email, post, transmit or otherwise make available any Content that infringes any patent, trademark, trade secret, copyright or other proprietary rights of any party;</li>
                <p></p>
                <li>upload, email, post, transmit or otherwise make available any unsolicited or unauthorised advertising, promotional materials, "junk mail", "spam", "chain letters", "pyramid schemes", or any other unauthorised form of solicitation;</li>
                <p></p>
                <li>upload, email, post, transmit or otherwise make available any material that contains software viruses, worms, Trojan-horses or any other computer code, routines, files or programs designed to directly or indirectly interfere with, manipulate, interrupt, destroy or limit the functionality or integrity of any computer software or hardware or data or telecommunications equipment;</li>
                <p></p>
                <li>disrupt the normal flow of dialogue, cause a screen to "scroll" faster than other Users of the Services are able to type, or otherwise act in a manner that negatively affects other Users' ability to engage in real time exchanges;</li>
                <p></p>
                <li>interfere with, manipulate or disrupt the Services or servers or networks connected to the Services or any other User's use and enjoyment of the Services, or disobey any requirements, procedures, policies or regulations of networks connected to the Beam;</li>
                <p></p>
                <li>take any action or engage in any conduct that could directly or indirectly damage, disable, overburden, or impair the Services or the servers or networks connected to the Services;</li>
                <p></p>
                <li>use the Services to intentionally or unintentionally violate any applicable local, state, national or international law, rule, code, directive, guideline, policy or regulation including, without limitation, laws and requirements (whether or not having the force of law) relating to anti-money laundering or counter-terrorism;</li>
                <p></p>
                <li>use the Services in violation of or to circumvent any sanctions or embargo administered or enforced by the U.S. Department of Treasury’s Office of Foreign Assets Control, the United Nations Security Council, the European Union or Her Majesty’s Treasury;</li>
                <p></p>
                <li>use the Services to violate the privacy of others or to "stalk" or otherwise harass another;</li>
                <p></p>
                <li>infringe the rights of Beam, including any intellectual property rights and any passing off of the same thereof;</li>
                <p></p>
                <li>use the Services to collect or store personal data about other Users in connection with the prohibited conduct and activities set forth above; and/or</li>
                <p></p>
                <li>list items which infringe upon the copyright, trademark or other intellectual property rights of third parties or use the Services in a manner which will infringe the intellectual property rights of others. (cc) direct or encourage another user to conduct a transaction other than on the Beam.</li>
                <p></p>

            </ul>
            </li>
            <p></p>

            <li>You understand that all Content, whether publicly posted or privately transmitted, is the sole responsibility of the person from whom such Content originated. This means that you, and not Beam, are entirely responsible for all Content that you upload, post, email, transmit or otherwise make available through the Beam. You understand that by using the Beam, you may be exposed to Content that you may consider to be offensive, indecent or objectionable. To the maximum extent permitted by applicable law, under no circumstances will Beam be liable in any way for any Content, including, but not limited to, any errors or omissions in any Content, or any loss or damage of any kind incurred as a result of the use of, or reliance on, any Content posted, emailed, transmitted or otherwise made available on the Beam.</li><p></p>

            <li>You acknowledge that Beam and its designees shall have the right (but not the obligation) in their sole discretion to pre-screen, refuse, delete, stop, suspend, remove or move any Content, including without limitation any Content or information posted by you, that is available on the Beam without any liability to you. Without limiting the foregoing, Beam and its designees shall have the right to remove any Content (i) that violates these Terms of Service or our Prohibited and Restricted Items Policy; (ii) if we receive a complaint from another User; (iii) if we receive a notice or allegation of intellectual property infringement or other legal instruction or request for removal; or (iv) if such Content is otherwise objectionable. We may also block delivery of a communication (including, without limitation, status updates, postings, messages and/or chats) to or from the Services as part of our effort to protect the Services or our Users, or otherwise enforce the provisions of these Terms and Conditions. You agree that you must evaluate, and bear all risks associated with, the use of any Content, including, without limitation, any reliance on the accuracy, completeness, or usefulness of such Content. In this regard, you acknowledge that you have not and, to the maximum extent permitted by applicable law, may not rely on any Content created by Beam or submitted to Beam, including, without limitation, information in Beam Forums and in all other parts of the Beam.</li><p></p>

            <li>You acknowledge, consent to and agree that Beam may access, preserve and disclose your Account information and Content to any legal, regulatory, or governmental authority, the relevant rights owner, or other third parties if required to do so by law pursuant to an order of a court or lawful request by any governmental or regulatory authority having jurisdiction over Beam or in a good faith belief that such access preservation or disclosure is reasonably necessary to: (a) comply with legal process; (b) enforce these Terms of Service or our Prohibited and Restricted Items Policy; (c) respond to claims that any Content violates the rights of third parties, including intellectual property rights; (d) respond to your requests for customer service; or (e) protect the rights, property or personal safety of Beam, its Users and/or the public.</li><p></p>


<h3>VIOLATION OF OUR TERMS OF SERVICE</h3>
<p></p>

        <li>Violations of this policy may result in a range of actions, including, without limitation, any or all of the following:
         <ul>
                <p></p>
                <li>Listing deletion</li>
                <p></p>
                <li>Limits placed on Account privileges</li>
                <p></p>
                <li>Account suspension and subsequent termination</li>
                <p></p>
                <li>Civil actions, including without limitation a claim for damages and/or interim or injunctive relief</li>


            </ul>
        </li>

        <p></p>
        <li>If you believe a User on our Beam is violating these Terms of Service, please contact info@letsbeam.it
        </li>


<h3>REPORTING INTELLECTUAL PROPERTY RIGHTS INFRINGEMENT</h3>
<p></p>

        <li>As stated above, Beam does not allow listings that violate the intellectual property rights of brands or other intellectual property rights owners (“IPR Owner”).
        </li>
        <p></p>
        <li>Except where expressly stated otherwise, the Users are independent individuals or businesses and they are not associated with Beam in any way and Beam is neither the agent nor representative of the Users and does not hold and/or own any of the merchandises listed on the Beam.
        </li>
        <p></p>
        <li>If you are an IPR Owner or an agent duly authorised by an IPR Owner (“IPR Agent”) and you believe that your right or your principal’s right has been infringed, please notify us by submitting a complaint at info@letsbeam.it and provide us the supporting documents to support your claim. Do allow us time to process the information provided. Beam will respond to your complaint as soon as practicable.
        </li>
        <p></p>
        <li>Complaints under this Section must include at least the following: (a) a physical or electronic signature of an IPR Owner or IPR Agent (collectively, “Informant”); (b) a description of the type and nature of intellectual property right that is allegedly infringed and proof of rights; (c) a description of the nature of alleged infringement with sufficient details to enable Beam to assess the complaint (d) URL(s) of the listing(s) which contain the alleged infringement; (e) sufficient information to allow Beam to contact the Informant, such as Informant’s physical address, telephone number and e-mail address; (f) a statement by Informant that the complaint is filed on good faith belief and that the use of the intellectual property as identified by the Informant is not authorised by the IPR Owner or the law; (g) a statement by the Informant that the information in the notification is accurate, that the Informant will indemnify us for any damages we may suffer as a result of the information provided by the Informant and that the Informant has the appropriate right or is authorised to act on the IPR Owner’s behalf in all matters relating to the complaint.
        </li>
        <p></p>
        <li>Beam acknowledges a manufacturer’s right to enter into certain exclusive distribution agreements or minimum advertised price agreements for its products. However, violations of such agreements do not constitute intellectual property rights infringement. As the enforcement of these agreements is a matter between the manufacturer and the sellers, Beam does not assist in this type of enforcement activity and does not enforce exclusive distribution rights or price-control matters except within the countries that have laws specifically governing selective or exclusive distribution.
        </li>
        <p></p>
        <li>Each and every Seller agrees to hold Beam and its Affiliates harmless from all claims, causes of action, damages and judgments arising out of any removal of any Content or product listings pursuant to or in relation to any intellectual property infringement claim.
        </li>
        <p></p>




<h3>PURCHASE AND PAYMENT</h3>
<p></p>

        <li>Beam supports one or more of the following payment methods:
         <ul>
                <p></p>
                <li>Credit Card --&gt; Card payments are processed through third-party payment channels and the type of credit cards accepted by these payment channels may vary.
</li>
                <p></p>
                <li>Online Banking --&gt; Buyer may make payments through online banking (“Internet Banking (or E-Debit)”) to our designated Beam Account.
</li>
                <p></p>
                <li>Deferred Payments using BNPL services (if applicable) --&gt; Buyer may make deferred payments for eligible orders using the BNPL services offered by Atome, Paylater by Grab or MobyPay. In order to offer the BNPL services, Atome, Paylater by Grab or MobyPay will enter into a receivable purchase arrangement with the eligible seller so that the seller will sell the receivable from the buyer to Atome, Paylater by Grab or MobyPay in exchange for payment from Atome, Paylater by Grab or MobyPay.
</li>


            </ul>
        </li>

        <p></p>
        <li>Buyer may only change their preferred mode of payment for their purchase prior to making payment.
        </li>
        <p></p>
        <li>Beam takes no responsibility and assume no liability for any loss or damages to Buyer arising from shipping information and/or payment information entered by Buyer or wrong remittance by Buyer in connection with the payment for the items purchased. We reserve the right to check whether Buyer is duly authorised to use certain payment method, and may suspend the transaction until such authorisation is confirmed or cancel the relevant transaction where such confirmation is not available.
        </li>



<h3>DELIVERY</h3>
<p></p>

        <li>Beam will inform Seller when Beam receives Buyer’s Purchase Monies. Unless otherwise agreed with Beam, Seller should then make the necessary arrangements to have the purchased item delivered to Buyer and provide details such as the name of the delivery company, the tracking number, etc. to Buyer through the Beam Merchant App.
        </li>
        <p></p>
        <li>Seller must use his/her best effort to ensure that Buyer receives the purchased items within, whichever applicable, the Beam Guarantee Period or the time period specified by Seller on Seller’s listing.
        </li>
        <p></p>
        <li>Users understand that Seller bears all risk attached to the delivery of the purchased item(s) and warrants that he/she has or will obtain adequate insurance coverage for the delivery of the purchased item(s). In the event where the purchased item(s) is damaged, lost or failure of delivery during the course of delivery, Users acknowledge and agree that Beam will not be liable for any damage, expense, cost or fees resulted therefrom and Seller and/or Buyer will reach out to the logistic service provider to resolve such dispute.
        </li>

        <p></p>
        <li>For Cross-Border Transaction(if applicable). Users understand and acknowledge that, where a product listing states that the product will ship from overseas, such product is being sold from a Seller based outside of Malaysia, and  the importation and exportation of such product is subject to local laws and regulations. Users should familiarise themselves with all import and export restrictions that apply to the designating country.  Users acknowledge that Beam cannot provide any legal advice in this regard and agrees that Beam shall not bear any risks or liabilities associated with the import and export of such products to Malaysia.
        </li>
        <p></p>
        <li>Where the Buyer elects to have a purchased item delivered by any other shipping method, the fee payable to the delivery company (“Shipping Fee”) shall be borne by the Buyer, Seller and Beam in such proportions as may be determined by Beam and published on the Beam from time to time.  Beam shall (i) collect the Buyer’s proportion of the Shipping Fee from the Buyer, (ii) deduct the Seller’s proportion of the Shipping Fee from the Buyer’s Purchase Monies, and (iii) pay the total Shipping Fee to the delivery company.
        </li>





<h3>CANCELLATION, RETURN AND REFUND</h3>
<p></p>

        <li>Buyer may only cancel his/her order prior to the payment of Buyer’s Purchase Monies into Beam Guarantee Account.
        </li>
        <p></p>
        <li>Buyer may apply for the return of the purchased item and refund prior to the expiry of Beam Guarantee Period, if applicable, subject to and in accordance with Beam’s Refunds and Return Policy. Please refer to Beam’s Refunds and Return Policy for further information.
        </li>
        <p></p>
        <li>Beam reserves the right to cancel any transaction on the Beam and Buyer agrees that Buyer’s sole remedy will be to receive a refund of the Buyer’s Purchase Monies paid into Beam Account.
        </li>

        <p></p>
        <li>Refunds to Buyers shall be made to their payment accounts within one (7) day of the return or refund request being approved.
        </li>




<h3>SELLER’S RESPONSIBILITIES</h3>
<p></p>

        <li>Seller shall properly manage and ensure that relevant information such as the price and the details of items, inventory amount and terms and conditions for sales is updated on Seller’s listing and shall not post inaccurate or misleading information.
        </li>
        <p></p>
        <li>The price of items for sale will be determined by the Seller at his/her own discretion. The price of an item and shipping charges shall include the entire amount to be charged to Buyer such as sales tax, value-added tax, tariffs, etc. and Seller shall not charge Buyer such amount additionally and separately.
        </li>
        <p></p>
        <li>Seller agrees that Beam may at its discretion engage in promotional activities to induce transactions between Buyer and Seller by reducing, discounting or refunding fees, or in other ways. The final price that Buyer will pay actually will be the price that such adjustment is applied to.
        </li>

        <p></p>
        <li>For the purpose of promoting the sales of the items listed by Seller, Beam may post such items (at adjusted price) on third-party website (such as portal sites and price comparison sites) and other website (domestic or foreign) operated by Beam.
        </li>

        <p></p>
        <li>Seller shall issue receipts, credit card slips or tax invoices to Buyer on request.
        </li>

        <p></p>
        <li>Seller acknowledges and agrees that Seller will be responsible for paying all taxes, customs and duties for the item sold and Beam cannot provide any legal or tax advice in this regard. As tax laws and regulations may change from time to time, Sellers are advised to seek professional advice if in doubt.
        </li>

        <p></p>
        <li>Seller acknowledge and agrees that Seller’s violation of any of Beam’s polices will result in a range of actions as stated in Section 21.
        </li>






<h3>PAID ADVERTISING</h3>
<p></p>

        <li>Beam offers advertising (“Ad”) and other advertising services, including participation in flash deals (“Flash Deal Advertising”) (hereinafter referred to as "Paid Advertising") on its App and other Sites/s on an ongoing basis. Sellers may purchase the Paid Advertising services. Beam provides the Paid Advertising services in accordance with these Terms of Service and any explanatory materials published on this Beam, the Paid Advertising Beam or otherwise communicated to Sellers in writing (hereinafter referred to as the "Paid Advertising Rules"). Sellers who purchase Paid Advertising services agree to be bound by the Paid Advertising Rules. If you are not agreeable to being bound by the Paid Advertising Rules, do not buy any Paid Advertising Services.
        </li>
        <p></p>
        <li>In order to purchase Paid Advertising services, you must be an eligible Seller under the Paid Advertising Rules. At the time when you purchase and pay for the Paid Advertising Services, your Account must not be suspended.
        </li>
        <p></p>
        <li>You may order Display Ads and Flash Deal Advertising services through info@letsbeam.it by setting your preferred advertising dates and budget.  Beam shall use commercially reasonable efforts to fulfill your Display Ads order, but does not guarantee that the (i) dates requested by the Seller, and/or (ii) the number of impressions associated with the budget set by the Seller, will be fulfilled.  For the avoidance of doubt, Beam reserves the right to reject any Display Ads order (whether partially or in full) at its sole and absolute discretion, and with or without notification to the Seller.  In addition, Beam reserves the right to charge a fee in the event the Seller requests to edit or cancel its Display Ads order.  After the Display Ads services have been delivered, Beam shall invoice the Seller based on the number of actual impressions received, and Seller shall pay such invoice in accordance with the terms set out in the invoice.  The billing information that the Seller provides will be used for invoice and tax invoice generation for Display Ads. The Seller is responsible for ensuring that the billing information is correct prior to submitting a Display Ads order. Any error in the billing information submitted which may result in wrong information in the invoice and tax invoice are the Seller's responsibility. Beam has the right to reject invoice revision requests due to wrong information being submitted by the Seller.
        </li>

        <p></p>
        <li>The goods you list on the Beam must comply with all relevant laws and regulations, the Paid Advertising Rules, these Terms of Service and the Prohibited and Restricted Items Policy. You understand and agree that Beam has the right to immediately remove any listing which violates any of the foregoing and any Paid Advertising fees that you have paid in relation to any listing removed will not be refunded. Beam will also not be liable to compensate you for any loss whatsoever in relation to listings removed pursuant to this.
        </li>

        <p></p>
        <li>You understand and agree that Beam does not warrant or guarantee any increase in viewership or sales of your items as a result of the Paid Advertising services.
        </li>

        <p></p>
        <li>You are advised to only purchase Paid Advertising services after fully considering your budget and intended advertising objectives. Except as otherwise provided in these Terms of Service or the Paid Advertising Rules, Beam shall not be liable for any compensation or be subject to any liability (including but not limited to actual expenses and lost profits) for the results or intended results of any Paid Advertising service.
        </li>

        <p></p>
        <li>IF, NOTWITHSTANDING ANYTHING IN THESE TERMS OF SERVICE, BEAM IS FOUND BY A COURT OF COMPETENT JURISDICTION TO BE LIABLE (INCLUDING FOR GROSS NEGLIGENCE) IN RELATION TO ANY PAID ADVERTISING SERVICE, THEN, TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, ITS LIABILITY TO YOU OR TO ANY THIRD PARTY IS LIMITED TO THE AMOUNT PAID BY YOU FOR THE PAID ADVERTISING SERVICE IN QUESTION ONLY.
        </li>





<h3>PURCHASE AND SALE OF ALCOHOL</h3>
<p></p>
        <li>The purchase and sale of alcoholic products (“Alcohol”) on the Beam is permitted by Beam subject to the terms and conditions of this Section. If you are a buyer of Alcohol (“Alcohol Buyer”), you will be deemed to have consented to the terms and conditions in this Section when you purchase Alcohol on the Beam. Similarly, if you are an approved seller of Alcohol (“Alcohol Seller”), you will be deemed to have consented to the terms and conditions in this Section when you sell Alcohol on the Beam.
        </li>

        <p></p>
        <li>If you are an Alcohol Buyer:
         <ul>
                <p></p>
                <li>you represent and warrant that you and (if applicable) the person receiving the Alcohol (“Recipient”) are (a) aged 21 or above; (b) are not Muslim or otherwise prohibited from buying Alcohol under any law, regulation or religion; and (c) understand the following: “MEMINUM ARAK BOLEH MEMBAHAYAKAN KESIHATAN”; and
                </li>
                <p></p>
                <li>if requested by an Alcohol Seller or Beam (or its agents), you and/or the Recipient shall provide photo identification for age verification purposes.
                </li>
        </ul>
        </li>

        <p></p>
        <li>If you are an Alcohol Seller, you represent and warrant that:
         <ul>
                <p></p>
                <li>you are not Muslim or otherwise prohibited from selling Alcohol under any law, regulation or religion;
                </li>
                <p></p>
                <li>you hold all necessary licences and/or permits to sell Alcohol through the Beam, and shall provide a copy of such licences and/or permits and supporting documents to Beam immediately upon request for verification purposes; and
                </li>
                 <p></p>
                <li>all information and documents provided to Beam are true and accurate.
                </li>
        </ul>
        </li>

        <p></p>
        <li>When delivering Alcohol to an Alcohol Buyer:
         <ul>
                <p></p>
                <li>the delivery agent reserves the right to request for valid photo identification for age verification purposes; and
                </li>
                <p></p>
                <li>Beam (via the delivery agent) reserves the right to refuse the delivery of Alcohol if the Alcohol Buyer and/or the Recipient appears intoxicated or is unable to provide valid photo identification for age verification purposes.
                </li>
        </ul>
        </li>

        <p></p>
        <li>Each Alcohol Buyer and Alcohol Seller severally agrees to indemnify, defend and hold harmless Beam, and its shareholders, subsidiaries, affiliates, directors, officers, agents, co-branders or other partners, and employees (collectively, the "Indemnified Parties") from and against any and all claims, actions, proceedings, and suits and all related liabilities, damages, settlements, penalties, fines, costs and expenses (including, without limitation, any other dispute resolution expenses) incurred by any Indemnified Party arising out of or relating to: (a) any inaccuracy or breach of its representations in Section 57 and/or Section 58 (as applicable); and (b) its breach of any law or any rights of a third party.
        </li>





<h3>TRANSACTION FEES</h3>
<p></p>

        <li>Beam charges a fee for all successful transactions completed on the Beam (“Transaction Fee”). The Transaction Fee is borne by the Seller, and is calculated at five percent (5%) of the Buyer’s Purchase Monies, rounded up to the nearest cent.  The Transaction Fee is subject to SST (“Tax Amount”), and the Seller is responsible for such Tax Amount.
        </li>
        <p></p>
        <li>For Sellers located outside of Malaysia, Beam charges a fee for all successful transactions completed on the Beam (“Cross Border Fee”). The Cross Border Fee is borne by the Seller, and is calculated according to the rates as notified to such Sellers from time to time on the Beam.
        </li>
        <p></p>
        <li>Following the successful completion of a transaction, Beam shall deduct the Transaction Fee and the Tax Amount, and the Cross Border Fee (as applicable) from the Buyer’s Purchase Monies, and remit the balance (Pay-out) to the Seller in accordance with Section 42. Beam shall issue receipts or tax invoices for the Transaction Fee and Tax Amount paid by Seller on request.
        </li>

        <p></p>
        <li>Pay-out to the sellers will be done on weekly basis, on the following Wednesday, for all completed/delivered orders from 0000/Hrs on Monday to 2400/hrs on Sunday.
        </li>

        <p></p>
        <li>You understand and agree that Beam does not warrant or guarantee any increase in viewership or sales of your items as a result of the Paid Advertising services.
        </li>





<h3>DISPUTES</h3>
<p></p>

        <li>In the event a problem arises in a transaction, the Buyer and Seller agree to communicate with each other first to attempt to resolve such dispute by mutual discussions, which Beam shall use reasonable commercial efforts to facilitate. If the matter cannot be resolved by mutual discussions, Users may approach the claims tribunal of their local jurisdiction to resolve any dispute arising from a transaction.
        </li>
        <p></p>
        <li>Each Buyer and Seller covenants and agrees that it will not bring suit or otherwise assert any claim against Beam or its Affiliates (except where Beam or its Affiliates is the Seller of the product that the claim relates to) in relation to any transaction made on the Beam or any dispute related to such transaction.
        </li>
        <p></p>
        <li>Users may send written request to Beam to assist them in resolving issues which may arise from a transaction upon request. Beam may, at its sole discretion and with absolutely no liability to Seller and Buyer, take all necessary steps to assist Users resolving their dispute. For more information, please refer to Beam’s Refunds and Return Policy.
        </li>

        <p></p>
        <li>To be clear, the services provided under this Section are only available to Buyers covered under Beam’s approved payment method. Buyer using other payment means for his/her purchase should contact Seller directly.
        </li>



<h3>FEEDBACK</h3>
<p></p>

        <li>Beam welcomes information and feedback from our Users which will enable Beam to improve the quality of service provided. Please refer to our feedback procedure below for further information:
         <ul>
                <p></p>
                <li>Feedback may be made in writing through email to or using the feedback form found on the App.
                </li>
                <p></p>
                <li>Anonymous feedback will not be accepted.
                </li>
                 <p></p>
                <li>Users affected by the feedback should be fully informed of all facts and given the opportunity to put forward their case.
                </li>
                <p></p>
                <li> Vague and defamatory feedback will not be entertained.
                </li>
        </ul>
        </li>




<h3>DISCLAIMERS</h3>
<p></p>

        <li>THE SERVICES ARE PROVIDED "AS IS" AND WITHOUT ANY WARRANTIES, CLAIMS OR REPRESENTATIONS MADE BY BEAM OF ANY KIND EITHER EXPRESSED, IMPLIED OR STATUTORY WITH RESPECT TO THE SERVICES, INCLUDING, WITHOUT LIMITATION, WARRANTIES OF QUALITY, PERFORMANCE, NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE, NOR ARE THERE ANY WARRANTIES CREATED BY COURSE OF DEALING, COURSE OF PERFORMANCE OR TRADE USAGE. WITHOUT LIMITING THE FOREGOING AND TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, BEAM DOES NOT WARRANT THAT THE SERVICES, THIS BEAM OR THE FUNCTIONS CONTAINED THEREIN WILL BE AVAILABLE, ACCESSIBLE, UNINTERRUPTED, TIMELY, SECURE, ACCURATE, COMPLETE OR ERROR-FREE, THAT DEFECTS, IF ANY, WILL BE CORRECTED, OR THAT THIS BEAM AND/OR THE SERVER THAT MAKES THE SAME AVAILABLE ARE FREE OF VIRUSES, CLOCKS, TIMERS, COUNTERS, WORMS, SOFTWARE LOCKS, DROP DEAD DEVICES, TROJAN-HORSES, ROUTINGS, TRAP DOORS, TIME BOMBS OR ANY OTHER HARMFUL CODES, INSTRUCTIONS, PROGRAMS OR COMPONENTS.
        </li>
        <p></p>
        <li>YOU ACKNOWLEDGE THAT THE ENTIRE RISK ARISING OUT OF THE USE OR PERFORMANCE OF THE BEAM AND/OR THE SERVICES REMAINS WITH YOU TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW.
        </li>
        <p></p>
        <li>BEAM HAS NO CONTROL OVER AND, TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, DOES NOT GUARANTEE OR ACCEPT ANY RESPONSIBILITY FOR: (A) THE FITNESS FOR PURPOSE, EXISTENCE, QUALITY, SAFETY OR LEGALITY OF ITEMS AVAILABLE VIA THE SERVICES; OR (B) THE ABILITY OF SELLERS TO SELL ITEMS OR OF BUYERS TO PAY FOR ITEMS. IF THERE IS A DISPUTE INVOLVING ONE OR MORE USERS, SUCH USERS AGREE TO RESOLVE SUCH DISPUTE BETWEEN THEMSELVES DIRECTLY AND, TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, RELEASE BEAM AND ITS AFFILIATES FROM ANY AND ALL CLAIMS, DEMANDS AND DAMAGES ARISING OUT OF OR IN CONNECTION WITH ANY SUCH DISPUTE.
        </li>





<h3>EXCLUSIONS AND LIMITATIONS OF LIABILITY</h3>
<p></p>

        <li>TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT SHALL BEAM BE LIABLE WHETHER IN CONTRACT, WARRANTY, TORT (INCLUDING, WITHOUT LIMITATION, NEGLIGENCE (WHETHER ACTIVE, PASSIVE OR IMPUTED), PRODUCT LIABILITY, STRICT LIABILITY OR OTHER THEORY), OR OTHER CAUSE OF ACTION AT LAW, IN EQUITY, BY STATUTE OR OTHERWISE, FOR:
         <ul>
                <p></p>
                <li>(A) LOSS OF USE; (B) LOSS OF PROFITS; (C) LOSS OF REVENUES; (D) LOSS OF DATA; (E) LOSS OF GOOD WILL; OR (F) FAILURE TO REALISE ANTICIPATED SAVINGS, IN EACH CASE WHETHER DIRECT OR INDIRECT; OR
                </li>
                <p></p>
                <li>ANY INDIRECT, INCIDENTAL, SPECIAL OR CONSEQUENTIAL DAMAGES, ARISING OUT OF OR IN CONNECTION WITH THE USE OR INABILITY TO USE THIS BEAM OR THE SERVICES, INCLUDING, WITHOUT LIMITATION, ANY DAMAGES RESULTING THEREFROM, EVEN IF BEAM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
                </li>
        </ul>
        </li>
        <p></p>
        <li>YOU ACKNOWLEDGE AND AGREE THAT YOUR ONLY RIGHT WITH RESPECT TO ANY PROBLEMS OR DISSATISFACTION WITH THE SERVICES IS TO REQUEST FOR TERMINATION OF YOUR ACCOUNT AND/OR DISCONTINUE ANY USE OF THE SERVICES.
        </li>

        <p></p>
        <li>IF, NOTWITHSTANDING THE PREVIOUS SECTIONS, BEAM IS FOUND BY A COURT OF COMPETENT JURISDICTION TO BE LIABLE (INCLUDING FOR GROSS NEGLIGENCE), THEN, TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, ITS LIABILITY TO YOU OR TO ANY THIRD PARTY IS LIMITED TO THE LESSER OF: (A) ANY AMOUNTS DUE AND PAYABLE TO YOU; AND (B) RM 100 (ONE HUNDRED RINGGIT).
        </li>

        <p></p>
        <li>NOTHING IN THESE TERMS OF SERVICE SHALL LIMIT OR EXCLUDE ANY LIABILITY FOR DEATH OR PERSONAL INJURY CAUSED BY BEAM’S NEGLIGENCE, FOR FRAUD OR FOR ANY OTHER LIABILITY ON THE PART OF BEAM THAT CANNOT BE LAWFULLY LIMITED AND/OR EXCLUDED.
        </li>



<h3>LINKS TO THIRD PARTY </h3>
<p></p>

        <li>Third party links provided throughout the Beam will let you leave this Beam. These links are provided as a courtesy only, and the Beams they link to are not under the control of Beam in any manner whatsoever and you therefore access them at your own risk. Beam is in no manner responsible for the contents of any such linked Beam or any link contained within a linked Beam, including any changes or updates to such Beams. Beam is providing these links merely as a convenience, and the inclusion of any link does not in any way imply or express affiliation, endorsement or sponsorship by Beam of any linked Beam and/or any of its content therein.
        </li>


<h3>YOUR CONTRIBUTIONS TO THE SERVICES</h3>
<p></p>

        <li>By submitting Content for inclusion on the Services, you represent and warrant that you have all necessary rights and/or permissions to grant the licenses below to Beam. You further acknowledge and agree that you are solely responsible for anything you post or otherwise make available on or through the Services, including, without limitation, the accuracy, reliability, nature, rights clearance, compliance with law and legal restrictions associated with any Content contribution. You hereby grant Beam and its successors a perpetual, irrevocable, worldwide, non-exclusive, royalty-free, sub-licensable and transferable license to use, copy, distribute, republish, transmit, modify, adapt, create derivative works of, publicly display, and publicly perform such Content contribution on, through or in connection with the Services in any media formats and through any media channels, including, without limitation, for promoting and redistributing part of the Services (and its derivative works) without need of attribution and you agree to waive any moral rights (and any similar rights in any part of the world) in that respect. You understand that your contribution may be transmitted over various networks and changed to conform and adapt to technical requirements.
        </li>
<p></p>

        <li>Any Content, material, information or idea you post on or through the Services, or otherwise transmit to Beam by any means (each, a "Submission"), is not considered confidential by Beam and may be disseminated or used by Beam without compensation or liability to you for any purpose whatsoever, including, but not limited to, developing, manufacturing and marketing products. By making a Submission to Beam, you acknowledge and agree that Beam and/or other third parties may independently develop software, applications, interfaces, products and modifications and enhancements of the same which are identical or similar in function, code or other characteristics to the ideas set out in your Submission. Accordingly, you hereby grant Beam and its successors a perpetual, irrevocable, worldwide, non-exclusive, royalty-free, sub-licensable and transferable license to develop the items identified above, and to use, copy, distribute, republish, transmit, modify, adapt, create derivative works of, publicly display, and publicly perform any Submission on, through or in connection with the Services in any media formats and through any media channels, including, without limitation, for promoting and redistributing part of the Services (and its derivative works). This provision does not apply to personal information that is subject to our privacy policy except to the extent that you make such personal information publicly available on or through the Services.
        </li>



<h3>THIRD PARTY CONTRIBUTIONS TO THE SERVICES AND EXTERNAL LINKS</h3>
<p></p>

        <li>Each contributor to the Services of data, text, images, sounds, video, software and other Content is solely responsible for the accuracy, reliability, nature, rights clearance, compliance with law and legal restrictions associated with their Content contribution. As such, Beam is not responsible to, and shall not, regularly monitor or check for the accuracy, reliability, nature, rights clearance, compliance with law and legal restrictions associated with any contribution of Content. You will not hold Beam responsible for any User's actions or inactions, including, without limitation, things they post or otherwise make available via the Services.
        </li>
<p></p>

        <li>In addition, the Services may contain links to third party products, website, services and offers. These third party links, products, website and services are not owned or controlled by Beam. Rather, they are operated by, and are the property of, the respective third parties, and may be protected by applicable copyright or other intellectual property laws and treaties. Beam has not reviewed, and assumes no responsibility for the content, functionality, security, services, privacy policies, or other practices of these third parties. You are encouraged to read the terms and other policies published by such third parties on their website or otherwise. By using the Services, you agree that Beam shall not be liable in any manner due to your use of, or inability to use, any website or widget. You further acknowledge and agree that Beam may disable your use of, or remove, any third party links, or applications on the Services to the extent they violate these Terms of Service.
        </li>



<h3>YOUR REPRESENTATIONS AND WARRANTIES</h3>
<p></p>

        <li>You represent and warrant that:
         <ul>
                <p></p>
                <li>you possess the legal capacity (and in the case of a minor, valid parent or legal guardian consent), right and ability to enter into these Terms of Service and to comply with its terms; and
                </li>
                <p></p>
                <li>you will use the Services for lawful purposes only and in accordance with these Terms of Service and all applicable laws, rules, codes, directives, guidelines, policies and regulations.
                </li>
        </ul>
        </li>


<h3>FRAUDULENT OR SUSPICIOUS ACTIVITY</h3>
<p></p>

        <li>If Beam, in its sole discretion, believes that you may have engaged in any potentially fraudulent or suspicious activity and/or transactions, we may take various actions to protect Beam, other Buyers or Sellers, other third parties or you from Reversals, Chargebacks, Claims, fees, fines, penalties and any other liability. The actions we may take include but are not limited to the following:
         <ul>
                <p></p>
                <li>We may close, suspend, or limit your access to your Account or the Services, and/or suspend the processing of any transaction;
                </li>
                <p></p>
                <li>We may hold, apply or transfer the funds in your Account as required by judgments and orders which affect you or your Account, including judgments and orders issued by a competent court or elsewhere and directed to Beam;
                </li>
                <p></p>
                <li>We may refuse to provide the Services to you now and in the future;
                </li>
                <p></p>
                <li>We may hold your funds for a period of time reasonably needed to protect against the risk of liability to Beam or a third party, or if we believe that you may be engaging in potentially fraudulent or suspicious activity and/or transactions.
                </li>
        </ul>
        </li>
        <p></p>
        <li>For the purposes of this Section:
         <ul>
                <p></p>
                <li>"Chargeback" means a request that a Buyer files directly with his or her debit or credit card company  or debit or credit card issuing bank to invalidate a payment.
                </li>
                <p></p>
                <li>"Claim" means a challenge to a payment that a Buyer or Seller files directly with Beam.
                </li>
                <p></p>
                <li>"Reversal" means the reversal of a payment by Beam because (a) it is invalidated by the sender's bank, (b) it was sent to you in error by Beam, (c) the sender of the payment did not have authorization to send the payment (for example: the sender used a stolen credit card), (d) you received the payment for activities that violated these Terms of Service or any other Beam policy, or (e) Beam decided a Claim against you.
                </li>

        </ul>
        </li>




<h3>INDEMNITY</h3>
<p></p>

        <li>You agree to indemnify, defend and hold harmless Beam, and its shareholders, subsidiaries, affiliates, directors, officers, agents, co-branders or other partners, and employees (collectively, the "Indemnified Parties") from and against any and all claims, actions, proceedings, and suits and all related liabilities, damages, settlements, penalties, fines, costs and expenses (including, without limitation, any other dispute resolution expenses) incurred by any Indemnified Party arising out of or relating to: (a) any transaction made on the Beam, or any dispute in relation to such transaction (except where Beam or its Affiliates is the Seller in the transaction that the dispute relates to), (b) the Beam Guarantee, (c) the hosting, operation, management and/or administration of the Services by or on behalf of Beam, (d) your violation or breach of any term of these Terms of Service or any policy or guidelines referenced herein, (e) your use or misuse of the Services, (f) your breach of any law or any rights of a third party, or (g) any Content uploaded by you.
        </li>
<p></p>




<h3>SEVERABILITY</h3>
<p></p>

        <li>If any provision of these Terms of Service shall be deemed unlawful, void, or for any reason unenforceable under the law of any jurisdiction, then that provision shall be deemed severable from these terms and conditions and shall not affect the validity and enforceability of any remaining provisions in such jurisdiction nor the validity and enforceability of the provision in question under the law of any other jurisdiction.
        </li>
<p></p>


<h3>GOVERNING LAW</h3>
<p></p>

        <li>These Terms of Service shall be governed by and construed in accordance with the laws of Malaysia without regard to its conflict of law rules. The United Nations Convention on Contracts for the International Sale of Goods and the Uniform Computer Information Transaction Act, to the extent applicable, are expressly disclaimed. Unless otherwise required by applicable laws, any dispute, controversy, claim or difference of any kind whatsoever shall arising out of or relating to these Terms of Service against or relating to Beam or any Indemnified Party under these Terms of Service shall be referred to and finally resolved by arbitration in Malaysia in accordance with the Arbitration Rules of the Asian International Arbitration Centre (Malaysia) (“AIAC Rules”) for the time being in force, which rules are deemed to be incorporated by reference in this Section. There will be one (1) arbitrator and the language of the arbitration shall be English.
        </li>
<p></p>



<h3>GENERAL PROVISIONS</h3>
<p></p>

                <li>Beam reserves all rights not expressly granted herein.
                </li>

                <p></p>
                <li>Beam may modify these Terms of Service at any time by posting the revised Terms of Service on this Beam. Your continued use of this Beam after such changes have been posted shall constitute your acceptance of such revised Terms of Service.
                </li>

                <p></p>
                <li>You may not assign, sublicense or transfer any rights granted to you hereunder or subcontract any of your obligations.
                </li>

                <p></p>
                <li>Nothing in these Terms of Service shall constitute a partnership, joint venture or principal-agent relationship between you and Beam, nor does it authorise you to incur any costs or liabilities on Beam’s behalf.
                </li>

                 <p></p>
                <li>The failure of Beam at any time or times to require performance of any provision hereof shall in no manner affect its right at a later time to enforce the same unless the same is waived in writing.
                </li>

                <p></p>
                <li>These Terms of Service are solely for your and our benefit and are not for the benefit of any other person or entity, except for Beam's affiliates and subsidiaries (and each of Beam's and its affiliates' and subsidiaries' respective successors and assigns).
                </li>

                <p></p>
                <li>The terms set forth in these Terms of Service and any agreements and policies included or referred to in these Terms of Service constitute the entire agreement and understanding of the parties with respect to the Services and the Beam and supersede any previous agreement or understanding between the parties in relation to such subject matter. The parties also hereby exclude all implied terms in fact. In entering into the agreement formed by these Terms of Service, the parties have not relied on any statement, representation, warranty, understanding, undertaking, promise or assurance of any person other than as expressly set out in these Terms of Service. Each party irrevocably and unconditionally waives all claims, rights and remedies which but for this Section it might otherwise have had in relation to any of the foregoing. These Terms of Service may not be contradicted, explained or supplemented by evidence of any prior agreement, any contemporaneous oral agreement or any consistent additional terms.
                </li>

                <p></p>
                <li>You agree to comply with all applicable laws, statutes, regulations and codes relating to anti-bribery and corruption including without limitation the UK Bribery Act, the US Foreign Corrupt Practices Act,  the Singapore Prevention of Corruption Act and the Malaysian Anti-Corruption Commission Act and confirm that you have and shall have in place all policies and procedures needed to ensure compliance with such requirements.
                </li>

                <p></p>
                <li>If you have any questions or concerns about these Terms of Service or any issues raised in these Terms of Service or on the Beam, please contacts us at: info@letsbem.it
                </li>












        <p></p>
        <li>For the purposes of this Section:
         <ul>
                <p></p>
                <li>"Chargeback" means a request that a Buyer files directly with his or her debit or credit card company  or debit or credit card issuing bank to invalidate a payment.
                </li>
                <p></p>
                <li>"Claim" means a challenge to a payment that a Buyer or Seller files directly with Beam.
                </li>
                <p></p>
                <li>"Reversal" means the reversal of a payment by Beam because (a) it is invalidated by the sender's bank, (b) it was sent to you in error by Beam, (c) the sender of the payment did not have authorization to send the payment (for example: the sender used a stolen credit card), (d) you received the payment for activities that violated these Terms of Service or any other Beam policy, or (e) Beam decided a Claim against you.
                </li>

        </ul>
        </li>












</ol>



        <p>LEGAL NOTICES: Please send all legal notices to legal@letsbem.it and Attention it to the “General Counsel”.</p>







    </div>
</body>
</html>
