<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Beam Vendor Panel</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('public/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{asset('public/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('public/dist/css/adminlte.min.css')}}">
    <link rel="icon" href="{{asset('public/images/app_logo.png')}}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <link rel="icon" href="{{asset('public/admin.png')}}">

</head>
<body class="hold-transition login-page" >
<!-- style="background-image: url('public/images/banner-image.jpg');background-repeat: no-repeat;background-size:cover;"> -->
<div class="login-box">
     <div class="logo" style="text-align: center">
        <img src="{{asset('public/admin.png')}}" style="width:65px;">
    </div>
    <div class="login-logo">
        <p><b>Beam service provider center</b></a>
    </div>
    <!-- /.login-logo -->

    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Sign in to Service Provider</p>
            @include('admin.partials.messages')
            <form action="{{route('vendor.validatelogin')}}" method="POST">
                @csrf
                <div class="input-group mb-3">
                    <input type="email" class="form-control" name="email" id="email" placeholder="Email" maxlength="30" required/>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <input type="password" class="form-control" name="password" id="password" placeholder="Password" maxlength="30" required/>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-8">
                        <div class="icheck-primary">
                            <input type="checkbox" id="remember" name="remember">
                            <label for="remember">
                                Remember Me
                            </label>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-4">
                        <button type="submit" class="btn btn-primary btn-block">Sign In</button>
                    </div>
                    <!-- /.col -->
                </div>
                <div class="row">
                    <div style="">
                        <div >
                           <a href="{{url('forgot_pass')}}">Forgot Password</a>
                        </div>
                    </div>

                </div>
                <br>
                <div class="col-12">
                    <a href="{{route('vendor.registration')}}" class="btn btn-primary btn-block">
                        Signup As Service Provider
                    </a>
                </div>

                {{-- <div class="row">
                    <div class="col-12">
                        <button type="submit" class="btn btn-primary btn-block">Signup As Designer</button>
                    </div>
                </div> --}}

            </form>

        </div>
        <!-- /.login-card-body -->
    </div>
</div>
<div class="row">
    <div class="col-md-6">

    </div>
    <div class="col-md-12" >
        <div class="footer">
            <footer>
                Copyright ©️2023 All rights reserved | LetsBeam.it
            </footer>
        </div>
    </div>
</div>
<!-- /.login-box -->


</body>
</html>
