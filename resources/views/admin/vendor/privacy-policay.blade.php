<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="{{asset('public/admin.png')}}">
    <title>Privacy Policay</title>
</head>
<body>
    <div class="col-full blog-content__main">
        <h1 style="text-align: center">Privacy Policy</h1>
        <h3>INTRODUCTION</h3>

         <ol>
        <li>As part of Beam Application compliance with the Personal Data Protection Act 2010 (PDPA) and commitment to the protection of our member’s personal information, Beam Application has put in place personal data protection and compliance principles which Beam Application stands guided by in the provision of our products and services to our members/customers. Please read Beam Application Privacy Statement to understand how we use the personal data which we may collect from you. By providing your Personal Information to us, you are consenting to this Privacy Statement, and the collection, use, access, transfer, storage, and processing of your personal information as described in this Privacy Statement.

        </li>

        <p></p>



    <h3>WHAT IS THIS PRIVACY STATEMENT ALL ABOUT?</h3>
    <p></p>

        <li>This Privacy Statement is about:
            <ul>
                <p></p>
                <li>What kind of personal information do we collect</li>
                <p></p>
                <li>How your personal information is collected</li>
                <p></p>
                <li>How we use your personal information </li>
                <p></p>
                <li>When we disclose your personal information</li>
                <p></p>
                <li>When we transfer your personal information</li>
                <p></p>
                <li>Your right to access and correct your personal data</li>
            </ul>



        </li>
        <p></p>



    <h3>WHAT IS PERSONAL DATA?</h3>
    <p></p>

        <li>Personal data is any information that relates directly or indirectly to you. This includes any information which can be used to identify or contact you.
        </li>

    <h3>WHAT IS THE KIND OF PERSONAL INFORMATION WE COLLECT?</h3>
    <p></p>

        <li>In order for us to provide you with our Products and Services, and to operate in an efficient and effective manner, we need to collect relevant personal information from you. The personal information collected by us may include (but is not limited to) the following:-
         <ul>
                <p></p>
                <li>contact information such as your full name, supporting documents (including MyKad/NRIC/Passport, statements, etc.) date of birth, gender, nationality and race, preferred language, current private and/or business address, telephone number, email address</li>
                <p></p>
                <li>credit card information and bank account details</li>
                <p></p>
                <li>the status of the services you have subscribed from us </li>
                <p></p>
                <li>your personal interests and preferences to help us to better improve our Products and Services</li>
                <p></p>
                <li>how you use the Products and Services</li>
                <p></p>
                <li>the equipment you use when accessing our Websites, Products, and/or Services (such as your handphone, your computer system, and platform) to customize the service for you</li>
                <p></p>
                <li>user profile image to display image on profile page.</li>
            </ul>
        </li>



    <h3>WHEN AND HOW DO WE COLLECT YOUR PERSONAL DATA?</h3>
    <p></p>

        <li>We may collect your personal data from you when you:-
         <ul>
                <p></p>
                <li>register/subscribe as a new member</li>
                <p></p>
                <li>use our Products and/or Services</li>
                <p></p>
                <li>participate in any of our surveys</li>
                <p></p>
                <li>participate in any of our marketing campaigns and/or promotions</li>
                <p></p>
                <li>enter and/or participate in any contests, loyalty programs, or competitions run/organized by us</li>
                <p></p>
                <li>visit or browse our websites</li>
                 <p></p>
                <li>lodge a complaint with us</li>



            </ul>
        </li>

        <p></p>
        <li>When you access our websites, please be aware that:
         <ul>
                <p></p>
                <li>Your IP address and information about your visit (such as time stamp, date, duration, etc.) will automatically be collected by Beam Application systems.</li>
                <p></p>
                <li>A “cookie” may be attached when you enter. Cookies are pieces of information that websites transfer to your computer’s hard drive for record-keeping purposes. The use of cookies is now an industry standard, and you will find them used on most major websites in fact, most browsers are initially set up to accept cookies. If you prefer, you can reset your browser either to notify you when you have received a cookie, or to refuse to accept cookies. Please also note that your user experience will be affected and areas of certain sites will not function properly if you set your browser to not accept cookies.
    </li>
                <p></p>
                <li>Our application or websites may contain links to third parties sites and services and we would like to make it clear that we are not responsible for :</li>
                 <ul>
                    <p></p>
                    <li>Such third parties sites and</li>
                    <p></p>
                    <li>Information that is made available at such third parties sites</li>

                </ul>

                <p></p>
                <li>As such, it would be prudent for you to check on the privacy policy of such third parties before disclosing your personal information to such third parties and/or sites.</li>

            </ul>
        </li>








    <h3>HOW WE USE YOUR PERSONAL INFORMATION</h3>
    <p></p>

        <li>The mandatory personal information we collect from you will be used and or shared within our corporate group and with third parties for one or the following purposes:-
         <ul>
                <p></p>
                <li>to verify your identity</li>
                <p></p>
                <li>to assess and process your application(s) /request(s) for our Products and/or Services</li>
                <p></p>
                <li>to provide you with the Products and/or Services you have requested</li>
                <p></p>
                <li>to administer and manage the Products and/or Services we provide you (including charging, billing, and facilitating payments)</li>
                <p></p>
                <li>to investigate and resolve any service issues, billing and fees queries, complaints, or other inquiries that you submit to us regarding our Products and Services</li>
                <p></p>
                <li>to detect and prevent fraudulent activity</li>
                 <p></p>
                <li>to keep in contact with you and provide you with any information you have requested</li>
                <p></p>
                <li>to engage in business transactions in respect of Products and/or Services to be offered and provided to you</li>
                <p></p>
                <li>to establish and better manage any business relationship we may have with you</li>
                 <p></p>
                <li>to process any communications you send us (for example, answering any queries and dealing with any complaints and feedback)</li>
                <p></p>
                <li>to help us monitor and improve the performance of our Products and Services, and our customer support/relations teams, and to make these more efficient</li>
                <p></p>
                <li>to maintain and develop our business systems and infrastructure, including testing and upgrading of these systems</li>
                 <p></p>
                <li>to manage staff training and quality assurance</li>
                <p></p>
                <li>to notify you about benefits and changes to the features of our Products and/or Services</li>
                <p></p>
                <li>to determine how can we improve services to you</li>
                 <p></p>
                <li>to investigate, respond to, or defend claims made against, or involving interbase Resources</li>
                <p></p>
                <li>to conduct marketing activities (for example, market research)</li>
                <p></p>

              </ul>
        </li>





    <h3>DISCLOSURE OF YOUR PERSONAL INFORMATION</h3>
    <p></p>



        <p></p>
        <li>As a part of providing you with our Products and/or Services and the management and/or operation of the same, we may be required or need to disclose information about you to the following third parties:
         <ul>
                <p></p>
                <li>law enforcement agencies government agencies</li>
                <p></p>
                <li>companies and/or organizations that act as our agents, contractors, service providers and/or professional advisers
                </li>
                <p></p>
                <li>other parties in respect of whom you have given your express or implied consent</li>
                 <p></p>
                <li>customer inquiry and other customer related services (including telemarketing);</li>
                <p></p>
                <li>data processing services</li>
                 <p></p>
                <li>information technology services</li>
                 <p></p>
                <li>media and marketing services (including advertising)</li>
                 <p></p>
                <li>market research and website usage analysis services</li>
                 <p></p>
                <li>Subject at all times to any laws (including regulations, guidelines and/or obligations) applicable to Interbase Resources.</li>



            </ul>
        </li>









    <h3>IF YOUR PERSONAL INFORMATION PROVIDED IS INCOMPLETE</h3>
    <p></p>

        <li>Where indicated (for example in registration/application forms), it is obligatory you provide your personal information to us to enable us to process your application for our Products and/or Services. Should you decline to provide such obligatory personal information or wish to limit the processing of your personal information, we may not be able to process your application/request or provide you with our products or services

        </li>
        <p></p>


    <h3>ACCESS TO YOUR PERSONAL DATA</h3>
    <p></p>

             <li>We can assist you to access, and correct and limit the processing of your personal information held by us. If you wish to have access to your personal information in our possession, or where you are of the opinion that such personal information held by us is inaccurate, incomplete, misleading or where relevant, not up-to-date or should you wish to limit the processing of your personal information including personal data relating to other persons who may be identified from that personal data, you may make a request to us by email to our Feedback department. You should quote your full name, email address, mailing/billing address and handphone/ fixed phone number and provide brief details of the information you want a copy of in any such request. We shall charge a processing fee for searching for and providing you with access to your personal information to cover administration costs. We will use reasonable efforts to comply with your request to access or correct or limit the processing of your personal information within 21 days of receiving your request and the relevant processing fee. Please note that we may have to withhold access to your personal information in certain situations, for example when we are unable to confirm your identity or where the information requested is of a confidential commercial nature, or in the event, we receive repeated requests for the same information. Nevertheless, we will notify you of the reasons for not being able to accede to your request.
             </li>
             <p></p>




    <h3>YOUR OBLIGATIONS AS A USER</h3>
    <p></p>

        <p></p>
        <li>You are responsible for providing accurate and complete information to us about yourself and any other person whose personal information you provide us and for updating this personal information as and when it becomes incorrect or out of date by contacting our support team via the contact details provided below. This obligation is a condition to the provision of our Products and/or Services to you and/or any other person authorized or permitted by you or your organization/company to use the Products and/or Services. In the event you may need to provide us with personal information relating to third parties (for example about your business partner, spouse, or children or where you are the designated person in charge (from an organization or company) for dealing with us, if you are acquiring and are responsible for a service and/or product that they will use), you confirm that you have
         <ul>
                <p></p>
                <li>obtained their consent or otherwise entitled to provide their personal information to us and for us to use accordingly, and </li>
                <p></p>
                <li>informed them to read this Privacy Statement at our FAQ section in the Beam Application.
                </li>

        </ul>
        </li>

    <h3>Third-Party Sharing</h3>
    <p></p>

        <p></p>
        <li>We do not share your Facebook login information with third parties. However, we may integrate certain third-party services for analytics or other functionalities.
            
        </li>

    <h3>Security Measures</h3>
        <p></p>
    
            <p></p>
            <li>We take the security of your data seriously. We have implemented measures to safeguard your information from unauthorized access or misuse.
                
            </li>

    
    <h3>User Rights</h3>
    <p></p>

        <p></p>
        <li>You have the right to:
            <ul>
                <p></p>
                <li>Access your data </li>
                <p></p>
                <li>Rectify any inaccuracies in your data
                </li>
                <li>Delete your data from our records

                </li>

        </ul>
        </li>

    <h3>Purpose of Data Processing</h3>
    <p></p>

        <p></p>
        <li>We use the collected data for the following purposes:
            <ul>
                <p></p>
                <li>Account creation and authentication </li>
                <p></p>
                <li>Personalization of your user experience
                </li>
               

        </ul>
        </li>


    <h3>UPDATES TO OUR PRIVACY STATEMENT</h3>
    <p></p>

        <li>We may be required to revise and/or modify this Privacy Statement from time to time and the updated version shall apply and supersede any and all previous versions, including but not limited to Apps, Websites, booklets, leaflets, or hard copy versions. By continuing to use Beam Products and Services after the communication of such revision and/or modification, you are accepting and consenting to such revision and/or modification. Please check our website, or Applications and/or our Notifications for information on our most up-to-date practices.
            
        </li>
        <p></p>





    <h3>OUR CONTACT DETAILS</h3>
    <p></p>

        <li>If you are not satisfied with the way in which we handle your enquiry or complaint, please don’t hesitate to contact Customer Service at info@letsbeam.it
        </li>

















    </ol>



        <p>LEGAL NOTICES: Please send all legal notices to legal@letsbem.it and Attention it to the “General Counsel”.</p>







    </div>
</body>
</html>
