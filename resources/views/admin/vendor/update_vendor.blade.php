@extends('admin.index')

@section('content')

<!-- <div class="wrapper"> -->

@php

$vendor = $vendor ? $vendor : "";
$name = $vendor->name;

@endphp
<style>
    #eye{
    cursor:pointer;
    position: absolute;
    left: 94%;
    top: 17%;
    color: #17a2b8;
}
</style>
<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <!-- Edit {{$name}} -->
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('Admin.Dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Update Designer</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-1"></div>
                <div class="col-10">

                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Update <strong>{{$vendor->name}}</strong></h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        @include('admin.partials.messages')
                        <form class="form-horizontal" method="post" action="{{route('Admin.Update.Vendor')}}">
                            @csrf
                            <input type="hidden" name="id" value="{{$vendor->id}}" />
                            <div class="card-body">
                                
                                <div class="form-group row">
                                    <label for="Name" class="col-sm-2 col-form-label">Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control name" id="name" name="name" value="{{$vendor->name}}" placeholder="Name" maxlength="40" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="Email" class="col-sm-2 col-form-label">Email</label>
                                    <div class="col-sm-10">
                                        <input type="email" class="form-control" id="email" name="email" value="{{$vendor->email}}" placeholder="Email" maxlength="40" pattern="[^@\s]+@[^@\s]+\.[^@\s]+" title="Invalid email address" />
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label for="Phone" class="col-sm-2 col-form-label">Phone</label>
                                    <div class="col-sm-10">
                                        {{-- <input type="text" class="form-control" id="phone" name="phone" value="{{$vendor->phone}}" placeholder="Phone" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))" maxlength="15" /> --}}
                                        <input type="text" class="form-control" id="phone" name="phone" value="{{$vendor->phone}}" placeholder="Phone"  maxlength="15" />
                                    </div>
                                </div>

                                {{-- <div class="form-group row">
                                    <label for="password" class="col-sm-2 col-form-label">Password</label>
                                    <div class="col-sm-10">
                                        <input type="password" class="form-control" id="password" name="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[-+_!@#$%^&*., ?]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter and one special characters, and at least 8 or more characters" placeholder="Password" />
                                        <i  class="fas fa-eye-slash" style="top: 11px;" id="eye"></i>
                                    </div>
                                </div>   --}}                          
                                
                                <div class="form-group row">
                                    <label for="Status" class="col-sm-2 col-form-label">Status</label>
                                    <div class="col-sm-10">
                                        <select class="form-control select2" style="width: 100%;" name="status" id="status" required>
                                            <option value="0" {{$vendor->status == 0  ? 'selected' : ''}}>Inactive</option>
                                            <option value="1" {{$vendor->status == 1  ? 'selected' : ''}}>Active</option>
                                        </select>
                                    </div>
                                </div>                                

                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-info">Update Designer</button>
                                <a href="{{route('Admin.Vendors.List')}}" class="btn btn-default float-right">Cancel</a>
                            </div>
                            <!-- /.card-footer -->
                        </form>
                    </div>

                </div>
                <div class="col-1"></div>
            </div>
        </div>
    </section>

</div>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script>
$(document).ready(function() {
$('.name').on('keypress', function(e) {
var regex = new RegExp("^[a-zA-Z ]*$");
var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
if (regex.test(str)) {
return true;
}
e.preventDefault();
return false;
});
});
</script>
<script>
   $(function(){
  
  $('#eye').click(function(){
       
        if($(this).hasClass('fa-eye-slash')){
           
          $(this).removeClass('fa-eye-slash');
          
          $(this).addClass('fa-eye');
          
          $('#password').attr('type','text');
            
        }else{
         
          $(this).removeClass('fa-eye');
          
          $(this).addClass('fa-eye-slash');  
          
          $('#password').attr('type','password');
        }
    });
});
</script>
<!-- </div> -->
@endsection