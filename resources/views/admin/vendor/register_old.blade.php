<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{env('APP_NAME', 'BeamApp')}}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('public/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{asset('public/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('public/dist/css/adminlte.min.css')}}">
    <link rel="icon" href="{{asset('public/admin.png')}}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <style>
        .lavel-cust-padding {
            min-width:100px;
            margin-right: 64px;
        }
        .input-group{
            width: 100%;
        }

        .login-card-body .input-group .form-control, .register-card-body .input-group .form-control{
            border-right : 1px solid #ced4da;
        }
        body {
            margin-top: 45px;
            /* width: 65px; */
        }
        .footer{
            display: none;
        }
    </style>
</head>
<body class="hold-transition login-page" >
<!-- style="background-image: url('public/images/banner-image.jpg');background-repeat: no-repeat;background-size:cover;"> -->
<div class="">
    <div class="logo" style="text-align: center">
        <img src="{{asset('public/admin.png')}}" style="width:65px;">
    </div>
    <div class="login-logo">
        <p><b>Beam service provider center</b></a>
    </div>
    <!-- /.login-logo -->

    <!--<div class="card" style="width:800px;">-->
        <div class="card" style="30% !important; max-width:800px;">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Sign Up to Service Provider</p>
            @include('admin.partials.messages')
            <form action="{{route('vendor.saveregister')}}" method="POST">
                @csrf

                <div class="input-group mb-3">
                    <div class="col-md-4">
                        <label for="" class="lavel-cust-padding">Service Provider Type</label>
                    </div>

                    <div class="col-md-8">
                        <select class="form-control select2" style="width: 100%;" id="user_type" name="user_type" required="true">
                            <option value="">Select Service Provider</option>
                            <option value="4">Merchant</option>
                            <option value="5">Printing Service Provider</option>
                            <option value="6">Secure Printing Service Provider</option>
                        </select>
                    </div>
                </div>

                <div class="input-group mb-3">
                    <div class="col-md-4">
                        <label for="Name">Business Name</label>
                    </div>

                    <div class="col-md-8">
                        <input type="text" class="form-control" id="name" name="name" value="{{old('name')}}" placeholder="Business Name" />
                    </div>
                </div>

                <div class="input-group mb-3">
                    <div class="col-md-4">
                        <label for="Name">Business Registration No</label>
                    </div>

                    <div class="col-md-8">
                        <input type="text" class="form-control" id="business_registration_no" name="business_registration_no" value="{{old('business_registration_no')}}" placeholder="Business Registration No" />
                    </div>
                </div>

                <div class="input-group mb-3">
                    <div class="col-md-4">
                        <label for="">Business Phone No</label>
                    </div>

                    <div class="col-md-8">
                        <input type="number" class="form-control" name="phone" id="phone" placeholder="Business Phone No" maxlength="15" required/>
                        {{-- <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-mobile"></span>
                            </div>
                        </div> --}}
                    </div>

                </div>

                <div class="input-group mb-3">
                    <div class="col-md-4">
                        <label for="">Cell Phone</label>
                    </div>

                    <div class="col-md-8">
                        <input type="number" class="form-control" name="business_phone_no" id="business_phone_no" placeholder="Cell Phone" maxlength="15" />
                        {{-- <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-mobile"></span>
                            </div>
                        </div> --}}
                    </div>

                </div>

                <div class="input-group mb-3">
                    <div class="col-md-4">
                        <label for="" >Email  </label>
                    </div>

                    <div class="col-md-8">
                        <input type="email" class="form-control" name="email" id="email" placeholder="Email" maxlength="50" required/>
                        {{-- <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div> --}}
                    </div>
                </div>

                <div class="input-group mb-3">
                    <div class="col-md-4">
                        <label for="">Password  </label>
                    </div>

                    <div class="col-md-8">
                        <input type="password" class="form-control" name="password" id="password" placeholder="Password"  maxlength="30" required/>
                        {{-- <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div> --}}
                    </div>
                </div>

                <div class="input-group mb-3">
                    <label for="Name" class="col-sm-4 col-form-label">Website</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="website" name="website" value="{{old('website')}}" placeholder="Website" />
                    </div>
                </div>

                <div class="input-group mb-3">
                    <label for="Name" class="col-sm-4 col-form-label">House Number</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="house_number" name="house_number"
                            value="{{old('house_number')}}" placeholder="House Number" />
                    </div>
                </div>

                <div class="input-group mb-3">
                    <label for="Name" class="col-sm-4 col-form-label">Street Address</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="street_address" name="street_address"
                            value="{{old('street_address')}}" placeholder="Street address" />
                    </div>
                </div>

                <div class="input-group mb-3">
                    <label for="Name" class="col-sm-4 col-form-label">Land Mark</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="land_mark" name="land_mark"
                            value="{{old('land_mark')}}" placeholder="Land Mark" />
                    </div>
                </div>

                <div class="input-group mb-3">
                    <label for="Name" class="col-sm-4 col-form-label">City</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="city" name="city"
                            value="{{old('city')}}" placeholder="City" />
                    </div>
                </div>

                <div class="input-group mb-3" id="postcode_vednor_main">
                    <label for="Name" class="col-sm-4 col-form-label">Postcode</label>
                    <div class="col-sm-8">
                        <input type="number" class="form-control" id="zip_code" name="zip_code"
                            value="{{old('zip_code')}}" placeholder="Postcode" />
                    </div>
                </div>

                <div class="input-group mb-3" id="state_vednor_main">
                    <label for="Name" class="col-sm-4 col-form-label">State</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="state" name="state"
                            value="{{old('state')}}" placeholder="State" />
                    </div>
                </div>

                <div class="input-group mb-3"  id="postal_main_1" style="display:none">
                    <div class="col-md-4">
                        <label for="">State : </label>
                    </div>

                    <div class="col-md-8">
                        <select class="form-control select2" style="width: 100%;" id="state_printer" name="state_printer">
                            <option value="">Select State</option>
                            @foreach ($postalState as $state)
                                <option value="{{$state->state_name}}">{{$state->state_name}}</option>
                            @endforeach

                        </select>
                    </div>
                </div>

                <div class="input-group mb-3"  id="postal_main" style="display:none">
                    <div class="col-md-4">
                        <label for="" >Postal Code  </label>
                    </div>

                    <div class="col-md-8">
                        <select class="form-control select2" style="width: 100%;" id="zip_code_printer" name="zip_code_printer">
                        </select>
                    </div>
                </div>

                <div class="input-group mb-3">
                    <label for="Name" class="col-md-4">Country</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="country" name="country"
                            value="{{old('country')}}" placeholder="Country" />
                    </div>
                </div>

                <div class="input-group mb-3">
                    <div class="col-md-4">
                        <label for="" >Contact Person 1  </label>
                    </div>

                    <div class="col-md-8">
                        <input type="text" class="form-control" name="contact_person_1" id="contact_person_1" placeholder="Contact Person 1" />
                    </div>
                </div>

                <div class="input-group mb-3">
                    <div class="col-md-4">
                        <label for="" >Cell phone  </label>
                    </div>

                    <div class="col-md-8">
                        <input type="text" class="form-control" name="contact_number_1" id="contact_number_1" placeholder="Cell phone" maxlength="30" />
                    </div>
                </div>

                <div class="input-group mb-3">
                    <div class="col-md-4">
                        <label for="" >Contact Person 2  </label>
                    </div>

                    <div class="col-md-8">
                        <input type="text" class="form-control" name="contact_person_2" id="contact_person_2" placeholder="Contact Person 2" />
                    </div>
                </div>

                <div class="input-group mb-3">
                    <div class="col-md-4">
                        <label for="" >Cell phone  </label>
                    </div>

                    <div class="col-md-8">
                        <input type="text" class="form-control" name="contact_number_2" id="contact_number_2" placeholder="Cell phone" maxlength="30" />
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-12">
                        <input type="checkbox" name="tandc" id="" required> <span><b>Check here to indicate that you have read and agree to the Beam's <a href="https://letsbeam.it/bstnc.html">Terms & Conditions</a>,<a href="https://letsbeam.it/privacyp.html"> Privacy Policy</a> and <a href="https://letsbeam.it/prohibited.html">Prohibited Items</a>. </b></span>
                    </div>
                </div>

                <div class="row">
                    <div class="col-8">

                    </div>
                    <!-- /.col -->
                    <div class="col-4">
                        <button type="submit" class="btn btn-primary btn-block">Sign Up</button>
                    </div>
                    <!-- /.col -->
                </div>

                <br>


                {{-- <div class="row">
                    <div class="col-12">
                        <button type="submit" class="btn btn-primary btn-block">Signup As Designer</button>
                    </div>
                </div> --}}

            </form>

        </div>
        <!-- /.login-card-body -->
    </div>
</div>
<div class="row" style="
margin-bottom: 13px !important;
">
    <div class="col-md-6">

    </div>
    <div class="col-md-12" >
        <div class="footer1" style="margin: 13px">
            <footer>
                Copyright ©️2023 All rights reserved | LetsBeam.it
            </footer>
        </div>
    </div>
</div>
<!-- /.login-box -->
@include('admin.partials.footer_js')

<script>
    $('#user_type').on('change', function(){
        var id = $(this).val();
        if(id == 5 || id == 6){
            // $('#postal_main').css('display', 'block')
            $('#postal_main').css({
                'display' : 'flex',
                'width' : '100%',
            })

            $('#postal_main_1').css({
                'display' : 'flex',
                'width' : '100%',
            })

            $('#postcode_vednor_main').css({
                'display' : 'none',
                'width' : '100%',
            })

            $('#state_vednor_main').css({
                'display' : 'none',
                'width' : '100%',
            })

            $('#zip_code_printer').attr('required', 'true')
        } else {
            $('#postal_main').css('display', 'none')
            $('#postal_main_1').css('display', 'none')
            $('#zip_code').attr('required', false)

            $('#postcode_vednor_main').css({
                'display' : 'flex',
                'width' : '100%',
            })

            $('#state_vednor_main').css({
                'display' : 'flex',
                'width' : '100%',
            })
        }

    })

    $('body').on('change', '#state_printer', function() {
        var id = $(this).val();
        zipcodeBasedState(id);
    });

    function zipcodeBasedState(id){
        var APP_URL = {!! json_encode(url('/')) !!};

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: APP_URL+'/admin/printerZipBasedState/'+id,

            type: "POST",
            data: {
                "id": id,
                "_method":'POST',
            },
            success: function (response) {
                orderData = JSON.parse(response);
                //Render order listing
                renderZip(orderData);
            },
            error: function(data) {
                //console.log("IN ERRROR "+data);
            }
        });
    }

    function renderZip(data) {
            var html = '<option value="">Select Postal code</option>';

            $(data).each(function(key, val){

                html += '<option value="'+val.postal_code+'">'+val.postal_code+'</option>';

            });
            $('#zip_code_printer').html(html);
    }

</script>

</body>
</html>
