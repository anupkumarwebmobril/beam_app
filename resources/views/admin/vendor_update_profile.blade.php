@extends('admin.index')

@section('content')

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <!--  -->
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('Admin.Dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Update Profile</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">

        <div class="row">
            <div class="col-2"></div>
            <div class="col-8">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">Update Profile</h3>
                    </div>

                    @include('admin.partials.messages')
                    <form class="form-horizontal" action="{{route('Vendor.Update.Profile')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" id="id" name="id" value="{{auth()->id()}}" />
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="Name" class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="name" placeholder="Name" name="name" value="{{$user->name}}" maxlength="20" required />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="Email" class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="email" placeholder="Email" name="email" value="{{$user->email}}" readonly />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="Email" class="col-sm-2 col-form-label">Company Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="business_name" placeholder="Company Name" name="business_name" value="{{$user->business_name}}"  />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="Email" class="col-sm-2 col-form-label">Business Registration No</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="business_registration_no" placeholder="Business Registration No" name="business_registration_no" value="{{$user->business_registration_no}}"  />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="Email" class="col-sm-2 col-form-label">Business Phone No</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control" id="phone" placeholder="Business Registration No" name="phone" value="{{$user->phone}}"  />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="Email" class="col-sm-2 col-form-label">Mobile Phone Number</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control" id="business_phone_no" placeholder="Business Registration No" name="business_phone_no" value="{{$user->business_phone_no}}"  />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="Email" class="col-sm-2 col-form-label">Company Website</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="website" placeholder="Company Website" name="website" value="{{$user->website}}"  />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="Email" class="col-sm-2 col-form-label">Unit or Building Number</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="house_number" placeholder="Unit or Building Number" name="house_number" value="{{$user->house_number}}"  />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="Email" class="col-sm-2 col-form-label">Street Address 1</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="street_address" placeholder="Street Address 1" name="street_address" value="{{$user->street_address}}"  />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="Email" class="col-sm-2 col-form-label">Street Address 2</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="land_mark" placeholder="Street Address 2" name="land_mark" value="{{$user->land_mark}}"  />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="Email" class="col-sm-2 col-form-label">City</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="city" placeholder="Enter Your City" name="land_mark" value="{{$user->business_phone_no}}"  />
                                </div>
                            </div>

                            {{-- <div class="form-group row">
                                <label for="Email" class="col-sm-2 col-form-label">State</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="state" placeholder="Enter Your state" name="state" value="{{$user->state}}"  />
                                </div>
                            </div> --}}
                            <div class="form-group row">
                                <label for="Email" class="col-sm-2 col-form-label">Country</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="country" placeholder="Enter Your country" name="country" value="{{$user->country}}"  />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="Email" class="col-sm-2 col-form-label">Primary Contact Person</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="contact_person_1" placeholder="Enter Your country" name="contact_person_1" value="{{$user->contact_person_1}}"  />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="Email" class="col-sm-2 col-form-label">Secondary Contact Person</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="contact_person_2" placeholder="Enter Your country" name="contact_person_2" value="{{$user->contact_person_2}}"  />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="Email" class="col-sm-2 col-form-label">Secondary Contact Phone</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="contact_number_2" placeholder="Secondary Contact Phone" name="contact_number_2" value="{{$user->contact_number_2}}"  />
                                </div>
                            </div>
                            {{-- <div class="form-group row">
                                <label for="Email" class="col-sm-2 col-form-label">Easy Parcel Api Key</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="easy_parcel_api_key" placeholder="Enter easy parcel api key " name="easy_parcel_api_key" value="{{$user->easy_parcel_api_key}}" />
                                </div>
                            </div> --}}

                            @if ($user->user_type == 5)
                                <div class="input-group row"  id="postal_main">
                                    <label for="" class="col-sm-2 col-form-label">Zip Code : </label>
                                    <div class="col-sm-10">
                                        <select class="form-control select2" style="width: 100%;" id="zip_code" name="zip_code">
                                            <option value="">Select Postal code</option>
                                            @foreach ($postalCodes as $zip)
                                                <option value="{{$zip->postal_code}}" {{ !empty($user->zip_code) && $user->zip_code == $zip->postal_code ? 'selected: selected' : ''}}>{{$zip->postal_code}}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>
                            @endif
                            <br>

                            <div class="form-group row">
                                <label for="ProfileImage" class="col-sm-2 col-form-label">Profile Image</label>
                                <input type="file" class="" id="profile_image" name="profile_image">

                                 {{-- @if(auth()->user()->profile_image)
                                <img class="img-circle elevation-2" src="{{url(auth()->user()->profile_image) }}" width="30%">

                                @else
                                <img class="img-circle elevation-2" src="{{ env('APP_URL') }}public/vendor/not_image.jpg">
                                @endif --}}
                                <img class="img-circle elevation-2" src="<?php echo !empty(auth()->user()->profile_image) ? asset("storage/profile_pictures/".auth()->user()->profile_image) :  asset("storage/profile_pictures/user.png") ?>" alt="User Avatar" height="150px" width="150px">
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-info">Update Profile</button>
                            <a href="{{route('Vendor.Dashboard')}}" class="btn btn-default float-right">Cancel</a>
                        </div>
                        <!-- /.card-footer -->
                    </form>
                </div>
            </div>
            <div class="col-2"></div>

        </div>

    </section>


    @endsection
