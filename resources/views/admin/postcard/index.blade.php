@extends('admin.index')

@section('after-style')

<link rel="stylesheet" href="{{asset('public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

@endsection

@section('content')

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('Admin.Dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Manage Postcard</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Postcard List</h3>
                             <a href="{{route('Admin.postcard.create')}}" style="float: right;"><i class="fas fa-plus-square" style="font-size:25px;"></i></a>
                        </div>


                        @if(session('message'))
                            <div class="alert alert-success" style="padding: 5px 20px;margin-bottom: 5px;">{{ session('message') }}</div>
                        @endif
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="user-list" class="table table-bordered table-striped" data-order='[[ 0, "asc" ]]'>
                                <thead>
                                    <tr>
                                        <th>Sr No.</th>
										<th>Product Name</th>
										<th>Shipping Fee</th>
										<th>Product Price</th>
                                        <th>Total Price</th>
                                        <th>Product Code</th>
                                        <th>Min order quantity</th>
                                        {{-- <th>Vendor</th> --}}
                                        <th>Product Images1</th>
                                        <th>Product Images2</th>
                                        <th>Status</th>
										<th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php $i=1; @endphp

                                    @if(isset($data) && $data != null)
                                        @foreach($data as $val)
                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>{{$val->name}}</td>
                                                <td><p class="text-success">RM:{{$val->shipping_fee}}</p></td>
                                                <td><p class="text-success">RM:{{$val->price_without_shipping}}</p></td>
                                                <td><p class="text-success">RM:{{$val->price}}</p></td>
                                                <td>{{$val->product_code}}</td>
                                                <td>{{$val->min_order_qty}}</td>
                                                {{-- <td>{{$val->vendor_name}}</td> --}}
                                                <td><img src={{asset("storage/postcard/".$val->image)}} class="img-fluid ${3|rounded-top,rounded-right,rounded-left,rounded-circle,|}" width="130px"  height="130px" style="border:3px solid white" alt=""></td>
                                                <td><img src={{asset("storage/postcard/".$val->image_2)}} class="img-fluid ${3|rounded-top,rounded-right,rounded-left,rounded-circle,|}" width="130px%" height="130px" style="border:3px solid white" alt=""></td>
                                                <td>@if ($val->status == 1)
                                                    <p class="text-success">Active</p>
                                                @else
                                                <p class="text-danger">In-Active</p>

                                                @endif</td>
                                                <td style="text-align: center;">

                                                    <a href="{{route('Admin.postcard.edit',[$val->id])}}"><i class="fas fa-edit"></i>&nbsp;&nbsp;</a>

                                                     <a href="{{route('Admin.Postcard.Delete',[$val->id])}}" onclick="return confirm('Are you sure?')"><i class="fas fa-trash"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->

</div>

@endsection

@section('after-scripts')
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>


@endsection
