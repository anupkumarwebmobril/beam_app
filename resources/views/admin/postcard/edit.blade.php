@extends('admin.index')
<meta name="csrf-token" content="{{ csrf_token() }}" />
@section('content')
    <!-- <div class="wrapper"> -->

    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <!--  -->
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('Admin.Dashboard') }}">Home</a></li>
                            <li class="breadcrumb-item active">Update Postcard</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-1"></div>
                    <div class="col-10">

                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">Update <strong>{{ $postcard->name }}</strong></h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            @include('admin.partials.messages')
                           	<form class="form-horizontal" method="post" action="{{route('Admin.postcard.update',['postcard'=>$postcard->id])}}" enctype="multipart/form-data" autocomplete="off">
                                @csrf
								@method('PUT')
                                <input type="hidden" name="id" value="{{ $postcard->id }}" />
                                <div class="card-body">

                                    <div class="form-group row">
                                        <label for="Name" class="col-sm-2 col-form-label">Product Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control name" id="name" name="name"
                                                value="{{ $postcard->name }}" placeholder="Name" maxlength="40" required />
                                        </div>
                                    </div>
                                    {{-- <div class="form-group row">
                                        <label for="Status" class="col-sm-2 col-form-label">Vendor</label>
                                        <div class="col-sm-10">
                                        <select class="form-control select2" style="width: 100%;" id="vendor"
                                            name="vendor" required="true" >
                                            <option value="" >Select Vendor</option>
                                            @foreach ($checks as $cats)
                                                <option value="{{ $cats->id }}" selected
                                                    >
                                                    {{ $cats->name }}</option>
                                            @endforeach
                                        </select>
                                        </div>
                                        </div> --}}
                                    <div class="form-group row">
                                        <label for="Email" class="col-sm-2 col-form-label">Shipping Fee</label>
                                        <div class="col-sm-10">
                                            <input id="shipping_fee" name="shipping_fee" type="number" class="form-control" required="true" value="{{ !empty($postcard->shipping_fee) ? $postcard->shipping_fee : ''}}" step=".01">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="Email" class="col-sm-2 col-form-label">Price</label>
                                        <div class="col-sm-10">
                                            <input id="price_without_shipping" name="price_without_shipping" type="number" class="form-control" required="true" value="{{ !empty($postcard->price_without_shipping) ? $postcard->price_without_shipping : ''}}" step=".01" placeholder="Price">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="Email" class="col-sm-2 col-form-label">Total Price (Price + Shipping Fee)</label>
                                        <div class="col-sm-10">
                                            <input id="price" name="price" type="number" class="form-control" required="true" value="{{ !empty($postcard->price) ? $postcard->price : ''}}" step=".01" placeholder="Total Price (Price + Shipping Fee)">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="Email" class="col-sm-2 col-form-label">Product Code </label>
                                        <div class="col-sm-10">
                                            <input id="product_code" name="product_code" type="text" class="form-control" required="true" value="{{ !empty($postcard->product_code) ? $postcard->product_code : ''}}"  placeholder="Enter product code">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="Email" class="col-sm-2 col-form-label">Min order quantity</label>
                                        <div class="col-sm-10">
                                            <input id="price" name="min_order_qty" type="number" class="form-control" value="{{ !empty($postcard->min_order_qty) ? $postcard->min_order_qty : ''}}" placeholder="Enter min order quantity ">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="Mobile" class="col-sm-2 col-form-label">Product Images1</label>
                                        <div class="col-sm-10">
                                            <input type="file" class="form-control customFileUplIpt" id="image" name="image" accept="image/png, image/jpg, image/jpeg">
												<img src="<?php echo !empty($postcard->image) ? asset("storage/postcard/".$postcard->image) : '' ?>"  style="max-height:300px; max-width:300px;"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="Mobile" class="col-sm-2 col-form-label">Product Images2</label>
                                        <div class="col-sm-10">
                                            <input type="file" class="form-control customFileUplIpt" id="image" name="image_2" accept="image/png, image/jpg, image/jpeg">
                                                <img src="<?php echo !empty($postcard->image_2) ? asset("storage/postcard/".$postcard->image_2) : '' ?>"  style="max-height:300px; max-width:300px;"/>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="Status" class="col-sm-2 col-form-label">Status</label>
                                        <div class="col-sm-10">
                                            <select class="form-control select2" style="width: 100%;" name="status"
                                                id="status" required>
                                                <option value="0" {{ $postcard->status == 0 ? 'selected' : '' }}>Inactive
                                                </option>
                                                <option value="1" {{ $postcard->status == 1 ? 'selected' : '' }}>Active
                                                </option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-info">Update Postcard</button>
                                    <a href="{{ route('Admin.postcard.index') }}"
                                        class="btn btn-default float-right">Cancel</a>
                                </div>
                                <!-- /.card-footer -->
                            </form>
                        </div>

                    </div>
                    <div class="col-1"></div>
                </div>
            </div>
        </section>

    </div>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#shipping_fee').on('keyup', function(){
                sum();
            })

            $('#price_without_shipping').on('keyup', function(){
                sum();
            })
        })

         function sum(){
            var shipping_fee = document.getElementById('shipping_fee').value;
            console.log(shipping_fee);
            var price_without_shipping = document.getElementById('price_without_shipping').value;
            var sum = Number(shipping_fee) + Number(price_without_shipping);

            document.getElementById('price').value = sum.toFixed(2);
        }
    </script>
@endsection
