@extends('admin.index')

@section('after-style')

<link rel="stylesheet" href="{{asset('public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

@endsection

@section('content')

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('Admin.Dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Manage Postcard</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Postcard List</h3>
                             <a href="{{route('Vendor.postcard.create')}}" style="float: right;"><i class="fas fa-plus-square" style="font-size:25px;"></i></a>
                        </div>


                        @if(session('message'))
                            <div class="alert alert-success" style="padding: 5px 20px;margin-bottom: 5px;">{{ session('message') }}</div>
                        @endif
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="user-list" class="table table-bordered table-striped" data-order='[[ 0, "asc" ]]'>
                                <thead>
                                    <tr>
                                        <th>Sr No.</th>
										<th>Product Name</th>
										<th>Shipping Fee</th>
										<th>Product Price</th>
                                        <th>Total Price</th>
										<th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php $i=1; @endphp

                                    @if(isset($data) && $data != null)
                                        @foreach($data as $val)

                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>{{$val->name}}</td>
                                                <td><p class="text-success">RM:{{$val->shipping_fee}}</p></td>
                                                <td><p class="text-success">RM:{{$val->price_without_shipping}}</p></td>
                                                <td><p class="text-success">RM:{{$val->price}}</p></td>
                                                <td style="text-align: center;">
                                                    <a href="javascript:void();" data-toggle="modal" data-target="#val-info{{$val->id}}"><i class="fas fa-eye"></i>&nbsp;&nbsp;</a>
                                                    <a href="{{route('Vendor.postcard.edit',[$val->id])}}"><i class="fas fa-edit"></i>&nbsp;&nbsp;</a>

                                                     <a href="{{route('Vendor.Postcard.Delete',[$val->id])}}" onclick="return confirm('Are you sure?')"><i class="fas fa-trash"></i></a>


                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    @include('admin.vendorpostcard.view_product')

</div>

@endsection

@section('after-scripts')
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>


@endsection
