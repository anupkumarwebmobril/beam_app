@extends('admin.index')
<meta name="csrf-token" content="{{ csrf_token() }}" />
@section('content')
    <!-- <div class="wrapper"> -->

    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <!--  -->
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('Admin.Dashboard') }}">Home</a></li>
                            <li class="breadcrumb-item active">Update Postcard</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-1"></div>
                    <div class="col-10">

                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">Update <strong>{{ $postcard->name }}</strong></h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            @include('admin.partials.messages')
                           	<form class="form-horizontal" method="post" action="{{route('Vendor.postcard.update',['postcard'=>$postcard->id])}}" enctype="multipart/form-data" autocomplete="off">
                                @csrf
								@method('PUT')
                                <input type="hidden" name="id" value="{{ $postcard->id }}" />
                                <div class="card-body">
                                    
                                    <div class="form-group row">
                                        <label for="Name" class="col-sm-2 col-form-label">Product Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control name" id="name" name="name"
                                                value="{{ $postcard->name }}" placeholder="Name" maxlength="40" />
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label for="Email" class="col-sm-2 col-form-label">Price</label>
                                        <div class="col-sm-10">
                                            <input id="price" name="price" type="number" class="form-control" required="true" value="{{ !empty($postcard->price) ? $postcard->price : ''}}" step=".01">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="Mobile" class="col-sm-2 col-form-label">Product Images1</label>
                                        <div class="col-sm-10">
                                            <input type="file" class="form-control customFileUplIpt" id="image" name="image" accept="image/png, image/jpg, image/jpeg">
												<img src="<?php echo !empty($postcard->image) ? asset("storage/postcard/".$postcard->image) : '' ?>"  style="max-height:300px; max-width:300px;"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="Mobile" class="col-sm-2 col-form-label">Product Images2</label>
                                        <div class="col-sm-10">
                                            <input type="file" class="form-control customFileUplIpt" id="image" name="image_2" accept="image/png, image/jpg, image/jpeg">
                                                <img src="<?php echo !empty($postcard->image_2) ? asset("storage/postcard/".$postcard->image_2) : '' ?>"  style="max-height:300px; max-width:300px;"/>
                                        </div>
                                    </div>
                              
                                    <div class="form-group row">
                                        <label for="Status" class="col-sm-2 col-form-label">Status</label>
                                        <div class="col-sm-10">
                                            <select class="form-control select2" style="width: 100%;" name="status"
                                                id="status" required>
                                                <option value="0" {{ $postcard->status == 0 ? 'selected' : '' }}>Inactive
                                                </option>
                                                <option value="1" {{ $postcard->status == 1 ? 'selected' : '' }}>Active
                                                </option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-info">Update Postcard</button>
                                    <a href="{{ route('Vendor.postcard.index') }}"
                                        class="btn btn-default float-right">Cancel</a>
                                </div>
                                <!-- /.card-footer -->
                            </form>
                        </div>

                    </div>
                    <div class="col-1"></div>
                </div>
            </div>
        </section>

    </div>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

@endsection
