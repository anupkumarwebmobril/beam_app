@foreach($data as $view)
<div class="modal fade" id="val-info{{$view->id}}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Viewing <strong>{{$view->name}}</strong></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Name" class="col-form-label">Product Name</label>
                            <div class="col-sm-10">
                                <p>{{$view->name}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Email" class="col-form-label">Price</label>
                            <div class="col-sm-10">
                                <p>{{$view->price}}</p>
                            </div>
                        </div>
                    </div> 
                     <div class="col-4">
                        <div class="form-group">
                            <label for="Image" class="col-form-label">Image</label>
                            <div class="col-sm-10">

                                <p><img src="{{ url('public/storage/postcard/'.$view['image']) }}" width="100px"></p>
                            </div>
                        </div>
                    </div>    
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Image" class="col-form-label">Image</label>
                            <div class="col-sm-10">

                                <p><img src="{{ url('public/storage/postcard/'.$view['image_2']) }}" width="100px"></p>
                            </div>
                        </div>
                    </div>       
                </div>
                   
                
                </div>

                <div class="row">                
                   <div class="col-4">
                        <div class="form-group">
                            <label for="Status" class="col-form-label">Status</label>
                            <div class="col-sm-10">
                                @if($view->status == 1)
                                    <p style="color:green">Active</p>
                                @elseif($view->status == 0)
                                    <p style="color:red">In-active</p>
                                @endif                                
                            </div>
                        </div>
                    </div>
                </div>
               
             
                
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endforeach
