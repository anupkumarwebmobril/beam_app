@extends('admin.index')
<meta name="csrf-token" content="{{ csrf_token() }}" />
@section('content')
    <!-- <div class="wrapper"> -->

    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <!--  -->
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('Admin.Dashboard') }}">Home</a></li>
                            <li class="breadcrumb-item active">Update Product</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-1"></div>
                    <div class="col-10">

                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">Update <strong>{{ $product->name }}</strong></h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            @include('admin.partials.messages')
                           	<form class="form-horizontal" method="post" action="{{route('Vendor.product.update',['product'=>$product->id])}}" enctype="multipart/form-data" autocomplete="off">
                                @csrf
								@method('PUT')
                                <input type="hidden" name="id" value="{{ $product->id }}" />
                                <div class="card-body">

									<div class="form-group row">
										<label for="Status" class="col-sm-2 col-form-label">Product Type</label>
										<div class="col-sm-10">
											<select class="form-control select2" style="width: 100%;" name="type"
												id="type" required="true">
												<option value="">Select product type</option>
												<option value="1" {{ !empty($product->type) && $product->type == 1 ? 'selected="selected"' : '' }}>For Postal
												</option>
												<option value="2" {{ !empty($product->type) && $product->type == 2 ? 'selected="selected"' : '' }}>For Gift Hub
												</option>
												
											</select>
										</div>
									</div> 

                                    
                                    <div class="form-group row">
                                        <label for="Status" class="col-sm-2 col-form-label">Category</label>
                                        <div class="col-sm-10">
                                            <select class="form-control select2" style="width: 100%;" id="occasion_id"
                                                name="occasion_id" required="true">
                                                <option value="">Select Category</option>
                                                @foreach ($occasions as $occasion)
                                                    <option value="{{ $occasion->id }}"
                                                        {{ !empty($product->occasion_id) && $product->occasion_id === $occasion->id ? 'selected="selected"' : '' }}>
                                                        {{ $occasion->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="Status" class="col-sm-2 col-form-label">Sub Category</label>
                                        <div class="col-sm-10">
                                            <select class="form-control select2" style="width: 100%;" id="category_id"
                                                name="category_id" required="true">
                                                <option value="">Select Sub Category</option>
                                                @foreach ($category as $cat)
                                                    <option value="{{ $cat->id }}"
                                                        {{ !empty($product->category_id) && $product->category_id === $cat->id ? 'selected="selected"' : '' }}>
                                                        {{ $cat->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="Name" class="col-sm-2 col-form-label">Product Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control name" id="name" name="name"
                                                value="{{ $product->name }}" placeholder="Name" maxlength="40" />
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label for="Email" class="col-sm-2 col-form-label">Price</label>
                                        <div class="col-sm-10">
                                            <input id="price" name="price" type="number" class="form-control" required="true" value="{{ !empty($product->price) ? $product->price : ''}}" step=".01">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="Mobile" class="col-sm-2 col-form-label">Product Images1</label>
                                        <div class="col-sm-10">
                                            <input type="file" class="form-control customFileUplIpt" id="image" name="image" accept="image/png, image/jpg, image/jpeg">
												<img src="<?php echo !empty($product->image) ? asset("storage/product/".$product->image) : '' ?>"  style="max-height:300px; max-width:300px;"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="Mobile" class="col-sm-2 col-form-label">Product Images2</label>
                                        <div class="col-sm-10">
                                            <input type="file" class="form-control customFileUplIpt" id="image" name="image_2" accept="image/png, image/jpg, image/jpeg">
                                                <img src="<?php echo !empty($product->image_2) ? asset("storage/product/".$product->image_2) : '' ?>"  style="max-height:300px; max-width:300px;"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="Mobile" class="col-sm-2 col-form-label">Product Image3</label>
                                        <div class="col-sm-10">
                                            <input type="file" class="form-control customFileUplIpt" id="image" name="image_3" accept="image/png, image/jpg, image/jpeg">
                                                <img src="<?php echo !empty($product->image_3) ? asset("storage/product/".$product->image_3) : '' ?>"  style="max-height:300px; max-width:300px;"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="Mobile" class="col-sm-2 col-form-label">Product Image4</label>
                                        <div class="col-sm-10">
                                            <input type="file" class="form-control customFileUplIpt" id="image" name="image_4" accept="image/png, image/jpg, image/jpeg">
                                                <img src="<?php echo !empty($product->image_4) ? asset("storage/product/".$product->image_4) : '' ?>"  style="max-height:300px; max-width:300px;"/>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="Mobile" class="col-sm-2 col-form-label">Product Description</label>
                                        <div class="col-sm-10">
                                            <textarea id="description" name="description" style="height: 125px; width: 100%; padding: 10px">{{$product->description}}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="Status" class="col-sm-2 col-form-label">Status</label>
                                        <div class="col-sm-10">
                                            <select class="form-control select2" style="width: 100%;" name="status"
                                                id="status" required>
                                                <option value="0" {{ $product->status == 0 ? 'selected' : '' }}>Inactive
                                                </option>
                                                <option value="1" {{ $product->status == 1 ? 'selected' : '' }}>Active
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <input type="hidden" name="redirect_url" value="<?php echo !empty($page) ? $page : null; ?>" />
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-info">Update Product</button>
                                    <a href="{{ route('Admin.product.index') }}"
                                        class="btn btn-default float-right">Cancel</a>
                                </div>
                                <!-- /.card-footer -->
                            </form>
                        </div>

                    </div>
                    <div class="col-1"></div>
                </div>
            </div>
        </section>

    </div>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    
	<script>
		$(document).ready(function() {
			$('body').on('change', '#occasion_id', function() {
                var id = $(this).val();
                idBasedData(id);       
            });

            $('body').on('change', '#type', function() {
                var id = $(this).val();
                typeBasedOccasion(id);       
            });
		});


		function idBasedData(id){
            var APP_URL = {!! json_encode(url('/')) !!};
            
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: APP_URL+'/vendor/product/CategoryList/'+id,
                //url: APP_URL+product.categoryList+id,
                
                type: "POST",
                data: {
                    "id": id,
                    "_method":'POST',
                },
                success: function (response) {
                    orderData = JSON.parse(response);               
                    //Render order listing
                    renderOrderOption(orderData);
                },
                error: function(data) {
                    //console.log("IN ERRROR "+data);
                }
            });
        }

        function renderOrderOption(data) {
            var html = '<option value="">Select category</option>';
            
            $(data).each(function(key, val){	
                
                html += '<option value="'+val.id+'">'+val.name+'</option>';
                
            });
            $('#category_id').html(html);
        }

        function typeBasedOccasion(id){
            var APP_URL = {!! json_encode(url('/')) !!};
            
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: APP_URL+'/vendor/product/occasionListBasedTypeId/'+id,
                //url: APP_URL+product.categoryList+id,
                
                type: "POST",
                data: {
                    "id": id,
                    "_method":'POST',
                },
                success: function (response) {
                    orderData = JSON.parse(response);               
                    //Render order listing
                    renderOccasion(orderData);
                },
                error: function(data) {
                    //console.log("IN ERRROR "+data);
                }
            });
        }

        function renderOccasion(data) {
            var html = '<option value="">Select Occasion</option>';
            
            $(data).each(function(key, val){	
                
                html += '<option value="'+val.id+'">'+val.name+'</option>';
                
            });
            $('#occasion_id').html(html);
        }


	</script>
    <!-- </div> -->
@endsection
