<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>@if (auth()->user()->user_type == 1)
        Beam Admin Panel
    @else
        Beam Vendor Panel
    @endif</title>

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{asset('public/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{asset('public/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('public/dist/css/adminlte.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/dist/css/custom.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/bootstrap-datetimepicker.min.css')}}" />

<script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/classic/ckeditor.js"></script>
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">

    {{-- <link rel="icon" href="{{asset('public/images/user-logo.png')}}"> --}}
    <link rel="icon" href="{{asset('public/admin.png')}}">


    @yield('after-style')
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">

    @include('admin.partials.header')

    @include('admin.partials.left_sidebar')

    <div class="wrapper">
        @yield('content')
    </div>
</div>
@include('admin.partials.footer_js')
</body>
</html>
