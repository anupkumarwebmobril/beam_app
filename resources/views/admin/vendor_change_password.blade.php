@extends('admin.index')

@section('after-style')
<style>
    #eye1{
    cursor:pointer;
    position: absolute;
    left: 94%;
    top: 17%;
    color: #17a2b8;
}
    #eye2{
    cursor:pointer;
    position: absolute;
    left: 94%;
    top: 17%;
    color: #17a2b8;
}
#eye3{
    cursor:pointer;
    position: absolute;
    left: 94%;
    top: 17%;
    color: #17a2b8;
}
    </style>
@endsection

@section('content')

    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                       <!--  -->
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item active">Change Password</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content">

                <div class="row">
                    <div class="col-2"></div>
                    <div class="col-8">
                        <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Change Password</h3>
                        </div>
                        @include('admin.partials.messages')
                        <form class="form-horizontal" action="{{route('Vendor.Change.Password')}}" method="post">
                            @csrf
                            <div class="card-body">

                                <div class="form-group row">
                                    <label for="old_password" class="col-sm-2 col-form-label">Old Password</label>
                                    <div class="col-sm-10">
                                        {{-- <input type="password" class="form-control" value="{{ old('old_password') }}" id="old_password" placeholder="Old Password" name="old_password" id="old_password"  required maxlength="16" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[-+_!@#$%^&*., ?]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter and one special characters, and at least 8 or more characters"/> --}}
                                        <input type="password" class="form-control" value="{{ old('old_password') }}" id="old_password" placeholder="Old Password" name="old_password" id="old_password"  required maxlength="16" />
                                        <span ><i  class="fas fa-eye-slash" style="top: 11px;" id="eye1"></i></span>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="Password" class="col-sm-2 col-form-label">New Password</label>
                                    <div class="col-sm-10">
                                        {{-- <input type="password" class="form-control" id="password" placeholder="Password" name="password" required maxlength="16" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[-+_!@#$%^&*., ?]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter and one special characters, and at least 8 or more characters"/> --}}
                                        <input type="password" class="form-control" id="password" placeholder="Password" name="password" required maxlength="16" minlength="6" />
                                        <span ><i  class="fas fa-eye-slash" style="top: 11px;" id="eye2"></i></span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="ConfirmPassword" class="col-sm-2 col-form-label">Confirm Password</label>
                                    <div class="col-sm-10">
                                        {{-- <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Confirm Password" required maxlength="16" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[-+_!@#$%^&*., ?]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter and one special characters, and at least 8 or more characters" /> --}}
                                        <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Confirm Password" required maxlength="16" minlength="6"  />
                                        <span><i class="fas fa-eye-slash" id="eye3"></i></span>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-info">Change Password</button>
                                <a href="{{route('Vendor.Dashboard')}}" class="btn btn-default float-right">Cancel</a>
                            </div>
                            <!-- /.card-footer -->
                        </form>
                    </div>
                    </div>
                    <div class="col-2"></div>

                </div>

        </section>


@endsection

@section('after-scripts')
<script>
   $(function(){

  $('#eye1').click(function(){

        if($(this).hasClass('fa-eye-slash')){

          $(this).removeClass('fa-eye-slash');

          $(this).addClass('fa-eye');

          $('#old_password').attr('type','text');

        }else{

          $(this).removeClass('fa-eye');

          $(this).addClass('fa-eye-slash');

          $('#old_password').attr('type','password');
        }
    });

    $('#eye2').click(function(){

        if($(this).hasClass('fa-eye-slash')){

          $(this).removeClass('fa-eye-slash');

          $(this).addClass('fa-eye');

          $('#password').attr('type','text');

        }else{

          $(this).removeClass('fa-eye');

          $(this).addClass('fa-eye-slash');

          $('#password').attr('type','password');
        }
        });

  $('#eye3').click(function(){

        if($(this).hasClass('fa-eye-slash')){

          $(this).removeClass('fa-eye-slash');

          $(this).addClass('fa-eye');

          $('#confirm_password').attr('type','text');

        }else{

          $(this).removeClass('fa-eye');

          $(this).addClass('fa-eye-slash');

          $('#confirm_password').attr('type','password');
        }
    });

});
</script>
@endsection
