@extends('admin.index')

@section('content')
    <!-- <div class="wrapper"> -->

    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <!--  -->
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('Admin.Dashboard') }}">Home</a></li>
                            <li class="breadcrumb-item active">Update Designer</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-1"></div>
                    <div class="col-10">

                        <div class="card card-info">
                            <div class="card-header">
                                <h3 class="card-title">Update <strong>{{ $user->name }}</strong></h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            @include('admin.partials.messages')
                            <form class="form-horizontal" method="post" action="{{ route('Admin.Update.designer') }}" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" value="{{ $user->id }}" />
                                <div class="card-body">

                                    <div class="form-group row">
                                        <label for="Name" class="col-sm-2 col-form-label">Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control name" id="name" name="name"
                                                value="{{ $user->name }}" placeholder="Name" maxlength="40" required />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="Email" class="col-sm-2 col-form-label">Email</label>
                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" id="email" name="email"
                                                value="{{ $user->email }}" placeholder="Email"
                                                pattern="[^@\s]+@[^@\s]+\.[^@\s]+" title="Invalid email address" required />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="Status" class="col-sm-2 col-form-label">Gender</label>
                                        <div class="col-sm-10">
                                            <select class="form-control select2" style="width: 100%;" name="gender" required="true">
                                                <option value="">Select Gender</option>
                                                <option value="1" {{ $user->gender == 1 ? 'selected' : '' }}>Male</option>
                                                <option value="2" {{ $user->gender == 2 ? 'selected' : '' }}>Female</option>
                                                <option value="3" {{ $user->gender == 3 ? 'selected' : '' }}>Corporate</option>                                            
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="Name" class="col-sm-2 col-form-label">Business Name</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="business_name" name="business_name" value="{{ $user->business_name }}" placeholder="Business Name" />
                                        </div>
                                    </div>
    
                                    <div class="form-group row">
                                        <label for="Name" class="col-sm-2 col-form-label">Business Registration No</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="business_registration_no" name="business_registration_no" value="{{ $user->business_registration_no }}" placeholder="Business Registration No" />
                                        </div>
                                    </div>

                                    <?php 
                                        $countryCode = substr($user->phone, 0, 2);                                        
                                        $mobileNumber = substr($user->phone, 2);
                                        
                                        $countryCode1 = substr($user->contact_number_1, 0, 2);                                        
                                        $contact_number_1 = substr($user->contact_number_1, 2);
                                    ?>

                                    <div class="form-group row">
                                        <label for="Status" class="col-sm-2 col-form-label">Country Code</label>
                                        <div class="col-sm-10">
                                            <select class="form-control select2" style="width: 100%;" name="country_code" required="true"
                                                id="status" >
                                                <option value="">Select country code</option>
                                                <option value="60" {{ $countryCode == 60 ? 'selected' : '' }}>60
                                                </option>
                                                <option value="91" {{ $countryCode == 91 ? 'selected' : '' }}>91
                                                </option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="Mobile" class="col-sm-2 col-form-label">Contact Number 1</label>
                                        <div class="col-sm-10">
                                            {{-- <input type="text" class="form-control" id="phone" name="phone"
                                                value="{{ $mobileNumber }}" placeholder="Phone"
                                                onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))"
                                                maxlength="15" required /> --}}

                                            <input type="text" class="form-control" id="phone" name="phone"
                                                value="{{ $mobileNumber }}" placeholder="Phone"                                                
                                                maxlength="15" required />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="Status" class="col-sm-2 col-form-label">Country Code</label>
                                        <div class="col-sm-10">
                                            <select class="form-control select2" style="width: 100%;" name="country_code_1">
                                                <option value="">Select country code</option>
                                                <option value="60" {{ $countryCode1 == 60 ? 'selected' : '' }}>60
                                                </option>
                                                <option value="91" {{ $countryCode1 == 91 ? 'selected' : '' }}>91
                                                </option>                                          
                                            </select>
                                        </div>
                                    </div>
    
                                    <div class="form-group row">
                                        <label for="Mobile" class="col-sm-2 col-form-label">Contact Number 2</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="contact_number_1" name="contact_number_1" value="{{ $contact_number_1 }}" placeholder="Contact Number 2" maxlength="15" />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="Name" class="col-sm-2 col-form-label">Website</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="website" name="website" value="{{ $user->website }}" placeholder="Website" />
                                        </div>
                                    </div>
    
                                    <div class="form-group row">
                                        <label for="Name" class="col-sm-2 col-form-label">House Number</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="house_number" name="house_number"
                                                value="{{ $user->house_number }}" placeholder="House Number" />
                                        </div>
                                    </div>
    
                                    <div class="form-group row">
                                        <label for="Name" class="col-sm-2 col-form-label">Street address</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="street_address" name="street_address"
                                                value="{{ $user->street_address }}" placeholder="Street address" />
                                        </div>
                                    </div>
    
                                    <div class="form-group row">
                                        <label for="Name" class="col-sm-2 col-form-label">Land Mark</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="land_mark" name="land_mark"
                                                value="{{ $user->land_mark }}" placeholder="Land Mark" />
                                        </div>
                                    </div>
    
                                    <div class="form-group row">
                                        <label for="Name" class="col-sm-2 col-form-label">City</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="city" name="city"
                                                value="{{ $user->city }}" placeholder="City" />
                                        </div>
                                    </div>
    
                                    <div class="form-group row">
                                        <label for="Name" class="col-sm-2 col-form-label">Postcode</label>
                                        <div class="col-sm-10">
                                            <input type="number" class="form-control" id="zip_code" name="zip_code"
                                                value="{{ $user->zip_code }}" placeholder="Postcode" minlength="6" maxlength="9"/>
                                        </div>
                                    </div>
    
                                    <div class="form-group row">
                                        <label for="Name" class="col-sm-2 col-form-label">State</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="state" name="state"
                                                value="{{ $user->state }}" placeholder="State" />
                                        </div>
                                    </div>
    
                                    <div class="form-group row">
                                        <label for="Name" class="col-sm-2 col-form-label">Country</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="country" name="country"
                                                value="{{ $user->country }}" placeholder="Country" />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="experience" class="col-sm-2 col-form-label">Experience</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="experience" name="experience" value="{{$user->experience}}" placeholder="experience like 4 year 1 months" maxlength="15"  required />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="Mobile" class="col-sm-2 col-form-label">Charges</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="designer_charge" name="designer_charge" value="{{$user->designer_charge}}" placeholder="Charges" maxlength="15" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))" required />
                                        </div>
                                    </div>
                                    {{-- <div class="form-group row">
                                        <label for="Mobile" class="col-sm-2 col-form-label">Zip Code</label>
                                        <div class="col-sm-10">
                                            <input type="number" class="form-control" id="zip_code" name="zip_code" value="{{$user->zip_code}}" placeholder=""  required />
                                        </div>
                                    </div> --}}
                                    <div class="form-group row">
                                        <label for="profile_image" class="col-sm-2 col-form-label">ProfileImage</label>
                                        <div class="col-sm-10">
                                            <input type="file" class="form-control" id="profile_image" name="profile_image" placeholder=""  />
                                            <img src={{asset("storage/profile_pictures/".$user->profile_image)}} class="img-fluid ${3|rounded-top,rounded-right,rounded-left,rounded-circle,|}" width="100px" style="border:3px solid white" alt="">

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="sample_design_image" class="col-sm-2 col-form-label">Sample design 1</label>
                                        <div class="col-sm-10">
                                            <input type="file" class="form-control" id="sample_design_image" name="sample_design_image" placeholder="sample_design_image"  />
                                            <img src={{asset("storage/sample_design_image/".$user->sample_design_image)}} class="img-fluid ${3|rounded-top,rounded-right,rounded-left,rounded-circle,|}" width="100px" style="border:3px solid white" alt="">

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="sample_design_image_2" class="col-sm-2 col-form-label">Sample design 2</label>
                                        <div class="col-sm-10">
                                            <input type="file" class="form-control" id="sample_design_image_2" value="{{$user->sample_design_image_2}}" name="sample_design_image_2" placeholder="sample_design_image_2"  />
                                            <img src={{asset("storage/sample_design_image_2/".$user->sample_design_image_2)}} class="img-fluid ${3|rounded-top,rounded-right,rounded-left,rounded-circle,|}" width="100px" style="border:3px solid white" alt="">

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="sample_design_image_3" class="col-sm-2 col-form-label">Sample design 3</label>
                                        <div class="col-sm-10">
                                            <input type="file" class="form-control" id="sample_design_image_3" name="sample_design_image_3" placeholder="sample_design_image_3"   />
                                            <img src={{asset("storage/sample_design_image_3/".$user->sample_design_image_3)}} class="img-fluid ${3|rounded-top,rounded-right,rounded-left,rounded-circle,|}" width="100px" style="border:3px solid white" alt="">

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="Status" class="col-sm-2 col-form-label">Status</label>
                                        <div class="col-sm-10">
                                            <select class="form-control select2" style="width: 100%;" name="status"
                                                id="status" required>
                                                <option value="0" {{ $user->status == 0 ? 'selected' : '' }}>Inactive
                                                </option>
                                                <option value="1" {{ $user->status == 1 ? 'selected' : '' }}>Active
                                                </option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-info">Update Designer</button>
                                    <a href="{{route('Admin.Designer.List')}}"
                                        class="btn btn-default float-right">Cancel</a>
                                </div>
                                <!-- /.card-footer -->
                            </form>
                        </div>

                    </div>
                    <div class="col-1"></div>
                </div>
            </div>
        </section>

    </div>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.name').on('keypress', function(e) {
                var regex = new RegExp("^[a-zA-Z ]*$");
                var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                if (regex.test(str)) {
                    return true;
                }
                e.preventDefault();
                return false;
            });
        });
    </script>
    <!-- </div> -->
@endsection
