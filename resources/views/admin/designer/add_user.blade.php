@extends('admin.index')

@section('content')

<!-- <div class="wrapper"> -->

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('Admin.Dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Add Designer</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-1"></div>
                <div class="col-10">

                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Add Designer</strong></h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        @include('admin.partials.messages')
                        <form class="form-horizontal" method="post" action="{{route('Admin.Add.Designer.Form')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">

                                <div class="form-group row">
                                    <label for="Name" class="col-sm-2 col-form-label">Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="name" name="name" value="{{old('name')}}" placeholder="Name" maxlength="40" required />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="Email" class="col-sm-2 col-form-label">Email</label>
                                    <div class="col-sm-10">
                                        <input type="email" class="form-control" id="email" name="email" value="{{old('email')}}" placeholder="Email" maxlength="40" required />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="Status" class="col-sm-2 col-form-label">Gender</label>
                                    <div class="col-sm-10">
                                        <select class="form-control select2" style="width: 100%;" name="gender" required="true">
                                            <option value="">Select Gender</option>
                                            <option value="1">Male</option>
                                            <option value="2">Female</option>
                                            <option value="3">Corporate</option>                                            
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="Name" class="col-sm-2 col-form-label">Business Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="business_name" name="business_name" value="{{old('business_name')}}" placeholder="Business Name" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="Name" class="col-sm-2 col-form-label">Business Registration No</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="business_registration_no" name="business_registration_no" value="{{old('business_registration_no')}}" placeholder="Business Registration No" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="Status" class="col-sm-2 col-form-label">Country Code</label>
                                    <div class="col-sm-10">
                                        <select class="form-control select2" style="width: 100%;" name="country_code" required="true">
                                            <option value="">Select country code</option>
                                            <option value="60">60</option>
                                            <option value="91">91</option>                                            
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="Mobile" class="col-sm-2 col-form-label">Contact Number 1</label>
                                    <div class="col-sm-10">
                                        {{-- <input type="text" class="form-control" id="mobile" name="phone" value="{{old('phone')}}" placeholder="Mobile" maxlength="15" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))" required /> --}}
                                        <input type="text" class="form-control" id="mobile" name="phone" value="{{old('phone')}}" placeholder="Contact Number 1" maxlength="15" required />
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label for="Status" class="col-sm-2 col-form-label">Country Code</label>
                                    <div class="col-sm-10">
                                        <select class="form-control select2" style="width: 100%;" name="country_code_1">
                                            <option value="">Select country code</option>
                                            <option value="60">60</option>
                                            <option value="91">91</option>                                            
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="Mobile" class="col-sm-2 col-form-label">Contact Number 2</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="contact_number_1" name="contact_number_1" value="{{old('contact_number_1')}}" placeholder="Contact Number 2" maxlength="15" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="Name" class="col-sm-2 col-form-label">Website</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="website" name="website" value="{{old('website')}}" placeholder="Website" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="Name" class="col-sm-2 col-form-label">House Number</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="house_number" name="house_number"
                                            value="{{old('house_number')}}" placeholder="House Number" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="Name" class="col-sm-2 col-form-label">Street Address</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="street_address" name="street_address"
                                            value="{{old('street_address')}}" placeholder="Street address" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="Name" class="col-sm-2 col-form-label">Land Mark</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="land_mark" name="land_mark"
                                            value="{{old('land_mark')}}" placeholder="Land Mark" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="Name" class="col-sm-2 col-form-label">City</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="city" name="city"
                                            value="{{old('city')}}" placeholder="City" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="Name" class="col-sm-2 col-form-label">Postcode</label>
                                    <div class="col-sm-10">
                                        <input type="number" class="form-control" id="zip_code" name="zip_code"
                                            value="{{old('zip_code')}}" placeholder="Postcode" minlength="6" maxlength="9"/>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="Name" class="col-sm-2 col-form-label">State</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="state" name="state"
                                            value="{{old('state')}}" placeholder="State" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="Name" class="col-sm-2 col-form-label">Country</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="country" name="country"
                                            value="{{old('country')}}" placeholder="Country" />
                                    </div>
                                </div>
                                                                                          
                                <div class="form-group row">
                                    <label for="experience" class="col-sm-2 col-form-label">Experience</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="experience" name="experience" value="{{old('experience')}}" placeholder="experience like 4 year 1 months" maxlength="15"  required="true" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="Mobile" class="col-sm-2 col-form-label">Charges</label>
                                    <div class="col-sm-10">
                                        <input type="number" class="form-control" id="designer_charge" name="designer_charge" value="{{old('designer_charge')}}" placeholder="Charges" maxlength="15" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))" required />
                                    </div>
                                </div>
                                {{-- <div class="form-group row">
                                    <label for="Mobile" class="col-sm-2 col-form-label">Zip Code</label>
                                    <div class="col-sm-10">
                                        <input type="number" class="form-control" id="zip_code" name="zip_code" value="{{old('zip_code')}}" placeholder="Enter zip code" required />
                                    </div>
                                </div> --}}

                                {{-- <div class="form-group row">
                                    <label for="Password" class="col-sm-2 col-form-label">Password</label>
                                    <div class="col-sm-10">
                                        <input type="password" class="form-control" id="password" name="password" placeholder="Password" required />
                                    </div>
                                </div> --}}
                                <div class="form-group row">
                                    <label for="profile_image" class="col-sm-2 col-form-label">ProfileImage</label>
                                    <div class="col-sm-10">
                                        <input type="file" class="form-control" id="profile_image" name="profile_image" placeholder="Password"  required />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="sample_design_image" class="col-sm-2 col-form-label">Sample Design 1</label>
                                    <div class="col-sm-10">
                                        <input type="file" class="form-control" id="sample_design_image" name="sample_design_image" placeholder="sample_design_image"  required />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="sample_design_image_2" class="col-sm-2 col-form-label">Sample Design 2</label>
                                    <div class="col-sm-10">
                                        <input type="file" class="form-control" id="sample_design_image_2" name="sample_design_image_2" placeholder="sample_design_image_2"  required />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="sample_design_image_3" class="col-sm-2 col-form-label">Sample Design 3</label>
                                    <div class="col-sm-10">
                                        <input type="file" class="form-control" id="sample_design_image_3" name="sample_design_image_3" placeholder="sample_design_image_3"  required />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="Status" class="col-sm-2 col-form-label">Status</label>
                                    <div class="col-sm-10">
                                        <select class="form-control select2" style="width: 100%;" name="status" >
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-info">Add Designer</button>
                                <a href="{{route('Admin.Designer.List')}}" class="btn btn-default float-right">Cancel</a>
                            </div>
                            <!-- /.card-footer -->
                        </form>
                    </div>

                </div>
                <div class="col-1"></div>
            </div>
        </div>
    </section>

</div>

<!-- </div> -->
@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
