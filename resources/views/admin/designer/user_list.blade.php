@extends('admin.index')

@section('after-style')

<link rel="stylesheet" href="{{asset('public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">

@endsection

@section('content')

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('Admin.Dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active"> Hire a designer</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title"> Hire a designer</h3>

                    <a href="{{route('Admin.Add.Designer')}}" style="float: right;"><i class="fas fa-plus-square" style="font-size:25px;"></i></a>
                        </div>


                        @if(session('message'))
                            <div class="alert alert-success" style="padding: 5px 20px;margin-bottom: 5px;">{{ session('message') }}</div>
                        @endif
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="user-list" class="table table-bordered table-striped" data-order='[[ 0, "asc" ]]'>
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Gender</th>
                                        <th>User Type</th>
                                        <th>Experience</th>
                                        <th>Designer charge</th>
                                        <th>ProfileImage</th>
                                        <th>Sample design 1</th>
                                        <th>Sample design 2</th>
                                        <th>Sample design 3</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php $i=1; @endphp

                                    @if(isset($users) && $users != null)
                                        @foreach($users as $user)
                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>{{$user->name}}</td>
                                                <td>{{$user->email}}</td>
                                                <td>{{$user->phone}}</td>
                                                <td style="text-align:center">
                                                    @if($user->gender == 1)
                                                        <p>Male</p>
                                                    @elseif($user->gender == 2)
                                                        <p>Female</p>
                                                    @elseif($user->gender == 3)
                                                        <p>Corporate</p>
                                                    @endif
                                                </td>
                                                 <td style="text-align:center">
                                                    @if($user->user_type == 1)
                                                        <p>Admin</p>
                                                    @elseif($user->user_type == 2)
                                                        <p>User</p>
                                                    @elseif($user->user_type == 3)
                                                        <p>Designer</p>
                                                    @elseif($user->user_type == 4)
                                                        <p>Vendor</p>
                                                    @endif
                                                </td>
                                                <td>
                                                    {{$user->experience}}
                                                </td>
                                                <td>
                                                    @if ($user->designer_charge)
                                                    <p class="text-success">RM:{{$user->designer_charge}}</p>
                                                    @else
                                                    <p>""</p>
                                                    @endif

                                                </td>
                                                <td>
                                                    <img src={{asset("storage/profile_pictures/".$user->profile_image)}} class="img-fluid ${3|rounded-top,rounded-right,rounded-left,rounded-circle,|}" width="100px" style="border:3px solid white" alt="">

                                                </td>
                                                <td>
                                                    <img src={{asset("storage/sample_design_image/".$user->sample_design_image)}} class="img-fluid ${3|rounded-top,rounded-right,rounded-left,rounded-circle,|}" width="100px" style="border:3px solid white" alt="">

                                                </td>
                                                <td>
                                                    <img src={{asset("storage/sample_design_image_2/".$user->sample_design_image_2)}} class="img-fluid ${3|rounded-top,rounded-right,rounded-left,rounded-circle,|}" width="100px" style="border:3px solid white" alt="">

                                                </td>
                                                <td>
                                                    <img src={{asset("storage/sample_design_image_3/".$user->sample_design_image_3)}} class="img-fluid ${3|rounded-top,rounded-right,rounded-left,rounded-circle,|}" width="100px" style="border:3px solid white" alt="">

                                                </td>

                                                <td style="text-align:center">
                                                    @if($user->status == 1)
                                                        <p style="color:green">Active</p>
                                                    @elseif($user->status == 0)
                                                        <p style="color:red">In-active</p>
                                                    @endif
                                                </td>

                                                <td style="text-align: center;">
                                                    {{-- <a href="javascript:void();" data-toggle="modal" data-target="#user-info{{$user->id}}"><i class="fas fa-eye"></i>&nbsp;&nbsp;</a> --}}
                                                    <a href="{{route('Admin.Get.designer',[$user->id])}}"><i class="fas fa-edit"></i>&nbsp;&nbsp;</a>
                                                    <a href="{{route('Admin.Delete.Designer',[$user->id])}}" onclick="return confirm('Are you sure?')"><i class="fas fa-trash"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    @include('admin.user.view_user')
</div>

@endsection

@section('after-scripts')
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>

<script>
    $(function() {
        $("#user-list").DataTable({
            "responsive": true,
            "autoWidth": false,
        });
    });
</script>
@endsection
