@foreach($users as $user)
<div class="modal fade" id="user-info{{$user->id}}">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Viewing <strong>{{$user->name}}</strong></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Name" class="col-form-label">Name</label>
                            <div class="col-sm-10">
                                <p>{{$user->name}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Email" class="col-form-label">Email</label>
                            <div class="col-sm-10">
                                <p>{{$user->email}}</p>
                            </div>
                        </div>
                    </div>           
                </div>
                
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Mobile" class="col-form-label">Mobile</label>
                            <div class="col-sm-10">
                                <p>{{$user->mobile_number}}</p>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Status" class="col-form-label">Post Code</label>
                            <div class="col-sm-10">
                               <p>{{$user->post_code}}</p>                           
                            </div>
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label for="Status" class="col-form-label">Unit</label>
                            <div class="col-sm-10">
                                <p>{{$user->unit}}</p>                                
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Mobile" class="col-form-label">Medical Card Number</label>
                            <div class="col-sm-10">
                                <p>{{$user->medical_card_number}}</p>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Status" class="col-form-label">Preferred Name</label>
                            <div class="col-sm-10">  
                                 <p>{{$user->preferred_name}}</p>                        
                            </div>
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label for="Mobile" class="col-form-label">DOB</label>
                            <div class="col-sm-10">
                                <p>{{$user->dob}}</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Status" class="col-form-label">Gender</label>
                            <div class="col-sm-10">
                                <p>{{$user->gender}}</p>                           
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Mobile" class="col-form-label">Emergency Contact Number</label>
                            <div class="col-sm-10">
                                <p>{{$user->emergency_contact_no}}</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label for="Status" class="col-form-label">Emergency Name</label>
                            <div class="col-sm-10">
                                <p>{{$user->emergency_name}}</p>                              
                            </div>
                        </div>
                    </div>   
                </div>


                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Mobile" class="col-form-label">Address</label>
                            <div class="col-sm-10">
                                <p>{{$user->address}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Mobile" class="col-form-label">City</label>
                            <div class="col-sm-10">
                                <p>{{$user->city}}</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label for="Status" class="col-form-label">Province</label>
                            <div class="col-sm-10">
                                <p>{{$user->province}}</p>                              
                            </div>
                        </div>
                    </div>                 
                </div>

                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Mobile" class="col-form-label">Medical Issue</label>
                            <div class="col-sm-10">
                                <p>{{$user->medical_issue}}</p>
                            </div>
                        </div>
                    </div>
                    
                    <!-- <div class="col-4">
                        <div class="form-group">
                            <label for="Status" class="col-form-label">Assign Nurse</label>
                            <div class="col-sm-10">
                                <p>{{$user->assign_nurse}}</p>                             
                            </div>
                        </div>
                    </div> -->
                    <div class="col-4">
                        <div class="form-group">
                            <label for="Mobile" class="col-form-label">Since</label>
                            <div class="col-sm-10">
                                <p>{{$user->since}}</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">                
                   <div class="col-4">
                        <div class="form-group">
                            <label for="Status" class="col-form-label">Status</label>
                            <div class="col-sm-10">
                                @if($user->status == 1)
                                    <p style="color:green">Active</p>
                                @elseif($user->status == 0)
                                    <p style="color:red">In-active</p>
                                @endif                                
                            </div>
                        </div>
                    </div>
                </div>
               
             
                
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endforeach
