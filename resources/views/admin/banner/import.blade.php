@extends('admin.index')
<meta name="csrf-token" content="{{ csrf_token() }}" />
@section('content')

<!-- <div class="wrapper"> -->

<div class="content-wrapper">

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('Admin.Dashboard')}}">Home</a></li>
                        <li class="breadcrumb-item active">Import</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
				<h6 id="ajax_response" style="color:red; display:none;"><span id="msg_span" class="success alert-success" style="color: green;"></span> </h2>
                <div class="col-1"></div>
                <div class="col-10">
					
                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Import</strong></h3>
                            
                        </div>

                        <!-- /.card-header -->
                        <!-- form start -->
                        @include('admin.partials.messages')
                       <form action="{{route('Admin.import.store')}}" class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
                            @csrf
                            <div class="card-body">
								
								
								<div class="form-group row">
									<label for="Mobile" class="col-sm-2 col-form-label">Import</label>
									<div class="col-sm-10">
										<input type="file" class="form-control customFileUplIpt" id="image" name="file" required="true" >
											
									</div>
								</div>
                   

                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-info">Import</button>
                                <a href="{{route('Admin.Dashboard')}}" class="btn btn-default float-right">Cancel</a>
                                 <a style="text-align: right;" class="btn btn-info" href="{{ url('admin/export') }}">Export Sample</a>
                            </div>
                            <!-- /.card-footer -->
                        </form>
                    </div>

                </div>
                <div class="col-1"></div>
            </div>
        </div>
    </section>

</div>


<!-- </div> -->
@endsection
