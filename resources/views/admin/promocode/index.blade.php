@extends('admin.index')

@section('after-style')
    <link rel="stylesheet" href="{{ asset('public/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endsection

@section('content')

    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">

                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('Admin.Dashboard') }}">Home</a></li>
                            <li class="breadcrumb-item active">Manage Promo Code</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Promo Code List</h3>

                                <a href="{{ route('Admin.promocode.create') }}" style="float: right;"><i
                                        class="fas fa-plus-square" style="font-size:25px;"></i></a>
                            </div>


                            @if (session('message'))
                                <div class="alert alert-success" style="padding: 5px 20px;margin-bottom: 5px;">
                                    {{ session('message') }}</div>
                            @endif
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="user-list" class="table table-bordered table-striped"
                                    data-order='[[ 0, "asc" ]]'>
                                    <thead>
                                        <tr>
                                            <th>Sr No.</th>
                                            <th>Promo code</th>
                                            <th>Discount Type</th>
                                            <th>Discount Value</th>
                                            <th>Expiration Date</th>
                                            <th>Associated Sub Categories</th>
                                            
                                            <th>Usage Count</th>  
                                            <th>Created At</th>                                            
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $i=1; @endphp

                                        @if (isset($data) && $data != null)
                                            @foreach ($data as $val)
                                                <tr>
                                                    <td>{{ $i++ }}</td>
                                                    <td>{{ $val->promocode }}</td>                                                    
                                                    <td>@if ($val->discount_type == "fixed_amount")
                                                            <p>Fixed Amount</p>
                                                        @else
                                                           <p>Percentage</p>
                                                        @endif
                                                    </td>
                                                    <td>{{ $val->discount_value }}</td>
                                                    <td>{{ $val->expiration_date }}</td>

                                                    <td>{{ $val->sub_cat_name }}</td>
                                                    <td>{{ $val->usage_count }}</td>
                                                    <td>{{ date("Y-m-d", strtotime($val->created_at)) }}</td>                                                    
                                                    <td>{{ $val->status == 1 ? 'Active' : 'In-Active' }}</td>


                                                    <td style="text-align: center;">
                                                        {{-- <a href="javascript:void();" data-toggle="modal" data-target="#user-info{{$val->id}}"><i class="fas fa-eye"></i>&nbsp;&nbsp;</a> --}}
                                                        <a href="{{ route('Admin.promocode.edit', [$val->id]) }}"><i
                                                                class="fas fa-edit"></i>&nbsp;&nbsp;</a>
                                                        <a href="javascript:void(0);" class="rowDelete"
                                                            data-id="{{ $val->id }}"
                                                            data-token="{{ csrf_token() }}"><i
                                                                class="fas fa-trash"></i></a>

                                                        {{-- <a href="vendorproduct/edit/{{$val->id}}" class="me-3 text-primary" data-bs-toggle="tooltip" data-placement="top" title="Edit" ><i class="material-icons">mode_edit</i></a>
													<a href="javascript:void(0);" class="text-danger rowDelete" data-bs-toggle="tooltip" data-placement="top" title="Delete" data-id="{{$val->id}}" data-token="{{ csrf_token() }}"><i class="material-icons">delete</i></a> --}}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>

                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
        @include('admin.product.view_product')
    </div>

@endsection

@section('after-scripts')
    <script src="{{ asset('public/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('public/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>

    <script>
        $(function() {
            $("#user-list").DataTable({
                "responsive": true,
                "autoWidth": false,
            });
        });


        $('body').on('click', '.rowDelete', function() {
            if (confirm("Are you sure?") == true) {
                var APP_URL = {!! json_encode(url('/')) !!};
                var id = $(this).data("id");
                var token = $(this).data("token");

                $(this).closest('tr').remove();
                $.ajax({
                    // url: APP_URL+'/vendorproduct/delete/'+id,
                    url: APP_URL + '/admin/promocode/delete/' + id,
                    type: "POST",
                    data: {
                        "id": id,
                        "_method": 'DELETE',
                        "_token": token
                    },
                    success: function(response) {
                        console.log('data deleted successfully');
                        //window.location.href= APP_URL+'/vendorproduct';
                        location.reload();
                    },
                    error: function(response) {
                        console.log("IN ERRROR " + response);
                        // window.location.href= window.location.pathname;
                    }
                });
            }

        });
    </script>
@endsection
