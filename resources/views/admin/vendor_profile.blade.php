@extends('admin.index')

@section('content')

<div class="wrapper">

    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        {{--<h1>DataTables</h1>--}}
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('Admin.Dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item active">Profile</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">

                        <div class="card card-widget widget-user">
                            <div class="widget-user-header bg-info">
                                <h3 class="widget-user-username">{{ ucfirst(auth()->user()->name)}}</h3>
                                <h5 class="widget-user-desc">{{auth()->user()->type == 4 ? 'Admin' : ""}}</h5>
                            </div>
                            <div class="widget-user-image">
                                <img class="img-circle elevation-2" style="height: 91px;" src="<?php echo !empty(auth()->user()->profile_image) ? asset("storage/profile_pictures/".auth()->user()->profile_image) :  asset("storage/profile_pictures/user.png") ?>" alt="User Avatar">
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-sm-6 border-right">
                                        <div class="description-block">
                                            <h5 class="description-header">Email</h5>
                                            <span class="description">{{auth()->user()->email}}</span>
                                        </div>
                                        <div class="description-block">
                                            <h5 class="description-header">Phone</h5>
                                            <span class="description">{{auth()->user()->phone}}</span>
                                        </div>
                                        <div class="description-block">
                                            <h5 class="description-header">Company Name</h5>
                                            <span class="description">{{ auth()->user()->business_name ?? "N/A" }}</span>
                                        </div>
                                        <div class="description-block">
                                            <h5 class="description-header">Street Address 1</h5>
                                            <span class="description">{{(auth()->user()->street_address ?? "N/A")}}</span>
                                        </div>
                                        <div class="description-block">
                                            <h5 class="description-header">Street Address 2</h5>
                                            <span class="description">{{(auth()->user()->land_mark ?? "N/A")}}</span>
                                        </div>
                                        <div class="description-block">
                                            <h5 class="description-header">City</h5>
                                            <span class="description">{{(auth()->user()->city ?? "N/A")}}</span>
                                        </div>
                                         <div class="description-block">
                                            <h5 class="description-header">Country</h5>
                                            <span class="description">{{(auth()->user()->country ?? "N/A")}}</span>
                                        </div>
                                        <div class="description-block">
                                            <h5 class="description-header">Primary Contact Person</h5>
                                            <span class="description">{{(auth()->user()->contact_person_1 ?? "N/A")}}</span>
                                        </div>
                                        <div class="description-block">
                                            <h5 class="description-header">Secondary Contact Person</h5>
                                            <span class="description">{{(auth()->user()->contact_number_2 ?? "N/A")}}</span>
                                        </div>
                                        <!-- /.description-block -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-sm-6 border-right">
                                        <div class="description-block">
                                            <h5 class="description-header">Status</h5>
                                            <span class="description">{{(auth()->user()->status == 1) ? 'Active' : 'Inactive'}}</span>
                                        </div>
                                        <div class="description-block">
                                            <h5 class="description-header">Business Registration No</h5>
                                            <span class="description">{{(auth()->user()->business_registration_no ?? "N/A")}}</span>
                                        </div>
                                        <div class="description-block">
                                            <h5 class="description-header">Business Phone No</h5>
                                            <span class="description">{{(auth()->user()->phone ?? "N/A")}}</span>
                                        </div>
                                        <div class="description-block">
                                            <h5 class="description-header">Mobile Phone Number</h5>
                                            <span class="description">{{(auth()->user()->business_phone_no ?? "N/A")}}</span>
                                        </div>
                                        <div class="description-block">
                                            <h5 class="description-header">Company Website</h5>
                                            <span class="description">{{(auth()->user()->website ?? "N/A")}}</span>
                                        </div>
                                        <div class="description-block">
                                            <h5 class="description-header">Unit or Building Number</h5>
                                            <span class="description">{{(auth()->user()->house_number ?? "N/A")}}</span>
                                        </div>
                                        <div class="description-block">
                                            <h5 class="description-header">Unit or Building Number</h5>
                                            <span class="description">{{(auth()->user()->house_number ?? "N/A")}}</span>
                                        </div>
                                        <!-- /.description-block -->
                                    </div>
                                </div>
                                <!-- /.row -->
                            </div>
                        </div>
                        <div class="edit-profile" style="float:right;position: absolute;top: 0;right: 1%;">
                            <a href="{{route('Vendor.Get.Profile')}}"><i class="fa fa-pencil"></i></a>
                        </div>
                        <!-- /.widget-user -->
                    </div>
                    <div class="col-md-2"></div>
                </div>

            </div>
        </section>
    </div>
</div>

<style>
    .fa-pencil{

        display: block;
        padding: 5px;
        color:white;

    }
    .fa-pencil:hover{
        color: #FFB6C1;
        transition: color 2000ms linear;
    }
</style>
@endsection

