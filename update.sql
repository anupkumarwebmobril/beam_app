
ALTER TABLE `users` CHANGE `user_type` `user_type` INT(11) NOT NULL DEFAULT '2' COMMENT '1:Admin,2:User,3:Designer, 4 as vendor; 5 as printer vendor';

ALTER TABLE `order_item` ADD `section` TINYINT(4) NOT NULL COMMENT '1 as postal; 2 as gifthub; This field is required to check if product is postal product or gifthub product' AFTER `product_type`;
ALTER TABLE `user_favorite_contacts` CHANGE `contact_id` `contact_id` INT(11) NOT NULL COMMENT 'this ID belongs to contact table';
-- RENAME TABLE `beam_app`.`add_recipient` TO `beam_app`.`add_recipient_backup`;
ALTER TABLE `notifications` ADD `is_notification_seen` TINYINT(4) NULL DEFAULT '0' COMMENT '1 as notification seen by user; 0 notification not seen' AFTER `message`;

ALTER TABLE `cart_item` ADD `custom_order` TINYINT(4) NOT NULL DEFAULT '0' AFTER `section`;
ALTER TABLE `cart_item` ADD `image_2` VARCHAR(500) NULL DEFAULT NULL AFTER `image`, ADD `image_3` VARCHAR(500) NULL DEFAULT NULL AFTER `image_2`, ADD `image_4` VARCHAR(500) NULL DEFAULT NULL AFTER `image_3`;

ALTER TABLE `order_item` ADD `custom_order` TINYINT(4) NOT NULL DEFAULT '0' AFTER `section`;
ALTER TABLE `order_item` ADD `image_2` VARCHAR(500) NULL DEFAULT NULL AFTER `image`, ADD `image_3` VARCHAR(500) NULL DEFAULT NULL AFTER `image_2`, ADD `image_4` VARCHAR(500) NULL DEFAULT NULL AFTER `image_3`;
ALTER TABLE `cart_item` ADD `status` TINYINT(4) NOT NULL DEFAULT '1' COMMENT '0 in-active 1 active' AFTER `final_checkout`;
ALTER TABLE `cart_item` ADD `designer_id` INT(11) NULL DEFAULT NULL AFTER `created_by`;
ALTER TABLE `cart_item` ADD `attachment_original_name` VARCHAR(500) NULL DEFAULT NULL AFTER `image_4`;
ALTER TABLE `cart_item` ADD `pdf_page_count` INT(11) NULL DEFAULT NULL AFTER `status`;
ALTER TABLE `cart_item` ADD `mail_type_id` INT(11) NULL DEFAULT NULL AFTER `pdf_page_count`;

ALTER TABLE `order_item` ADD `designer_id` INT(11) NULL DEFAULT NULL AFTER `created_by`;
ALTER TABLE `order_item` ADD `attachment_original_name` VARCHAR(500) NULL DEFAULT NULL AFTER `image_4`;
ALTER TABLE `order_item` ADD `pdf_page_count` INT(11) NULL DEFAULT NULL AFTER `image_4`
ALTER TABLE `order_item` ADD `mail_type_id` INT(11) NULL DEFAULT NULL AFTER `pdf_page_count`;
-- sagar update sql date 10-09-2022
ALTER TABLE `paper_type` CHANGE `status` `status` TINYINT(4) NULL DEFAULT '1' COMMENT '1 as active; 0 as inactive';
ALTER TABLE `mail_type` ADD `colored_price` VARCHAR(100) NULL DEFAULT NULL AFTER `price`;
ALTER TABLE `import_postals` CHANGE `email` `state_name` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;
INSERT INTO `miscellaneouses` (`id`, `name`, `price`, `created_at`, `updated_at`) VALUES (NULL, 'Additional gift note', '10', NULL, NULL);
UPDATE `miscellaneouses` SET `name` = 'Additional Gift Note', `created_at` = '2022-09-30 00:00:00', `updated_at` = '2022-09-30 00:00:00' WHERE `miscellaneouses`.`id` = 8;
ALTER TABLE `users` ADD `otp_expire_on` TIMESTAMP NULL DEFAULT NULL AFTER `otp`;
ALTER TABLE `cart` CHANGE `paper_type_id` `paper_type_id` VARCHAR(100) NULL DEFAULT NULL;
ALTER TABLE `order` CHANGE `paper_type_id` `paper_type_id` VARCHAR(100) NULL DEFAULT NULL;

ALTER TABLE `cart` CHANGE `additional_interests` `additional_interests` TINYINT(4) NULL DEFAULT '0' COMMENT 'this is additional git note; 0 not checked; 1 as checked; ';
ALTER TABLE `order` CHANGE `additional_interests` `additional_interests` TINYINT(4) NULL DEFAULT '0' COMMENT 'this is additional git note; 0 not checked; 1 as checked; ';
ALTER TABLE `users` CHANGE `experience` `experience` VARCHAR(500) NULL DEFAULT NULL;
ALTER TABLE `query` ADD `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `query`;

ALTER TABLE `product` ADD `image_5` VARCHAR(500) NULL DEFAULT NULL AFTER `image_4`, ADD `image_6` VARCHAR(500) NULL DEFAULT NULL AFTER `image_5`, ADD `image_7` VARCHAR(500) NULL DEFAULT NULL AFTER `image_6`, ADD `image_8` VARCHAR(500) NULL DEFAULT NULL AFTER `image_7`, ADD `image_9` VARCHAR(500) NULL DEFAULT NULL AFTER `image_8`, ADD `image_10` VARCHAR(500) NULL DEFAULT NULL AFTER `image_9`;
ALTER TABLE `mail_type` CHANGE `price` `price` DECIMAL(10,2) NULL DEFAULT NULL;
ALTER TABLE `mail_type` CHANGE `colored_price` `colored_price` DECIMAL(10,2) NULL DEFAULT NULL;
ALTER TABLE `product` ADD `shipping_fee` DECIMAL(10,2) NULL DEFAULT NULL AFTER `name`;
ALTER TABLE `product` ADD `price_without_shipping` DECIMAL(10,2) NULL DEFAULT NULL AFTER `shipping_fee`;

ALTER TABLE `postcard` ADD `shipping_fee` DECIMAL(10,2) NULL DEFAULT NULL AFTER `name`;
ALTER TABLE `postcard` ADD `price_without_shipping` DECIMAL(10,2) NULL DEFAULT NULL AFTER `shipping_fee`;
ALTER TABLE `order` ADD `admin_reminder_to_vendor` TINYINT NOT NULL DEFAULT '0' COMMENT '0 as email send; 1 as email not sent; reminder to vendor if order is not sent in particular days.' AFTER `additional_interests_msg`;
ALTER TABLE `order_item` ADD `status` INT(11) NOT NULL DEFAULT '1' COMMENT '1 as \"active and paid\"; 2 as \"cancelled\"; 3 as \"refunded\" (These are internal status not easy parcel)' AFTER `vendor_id`;
UPDATE `settings` SET `name` = 'Pending Order Reminder Days for Postal' WHERE `settings`.`id` = 1;
INSERT INTO `settings` (`id`, `name`, `pending_order_days_reminder`) VALUES (NULL, 'Pending Order Reminder Days for Gifthub', '4');
ALTER TABLE `order_item` ADD `admin_reminder_to_vendor` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '0 as email not sent; 1 as email sent; reminder to vendor if order is not sent in particular days.' AFTER `status`;
ALTER TABLE `order_item` ADD `item_total_price` DECIMAL(10,2) NULL DEFAULT NULL AFTER `price`;


ALTER TABLE `users` ADD `business_name` VARCHAR(255) NULL DEFAULT NULL AFTER `easy_parcel_api_key`, ADD `business_registration_no` VARCHAR(255) NULL DEFAULT NULL AFTER `business_name`, ADD `business_phone_no` VARCHAR(100) NULL DEFAULT NULL AFTER `business_registration_no`, ADD `contact_person_1` VARCHAR(100) NULL DEFAULT NULL AFTER `business_phone_no`, ADD `contact_number_1` VARCHAR(100) NULL DEFAULT NULL AFTER `contact_person_1`, ADD `contact_person_2` VARCHAR(100) NULL DEFAULT NULL AFTER `contact_number_1`, ADD `contact_number_2` VARCHAR(100) NULL DEFAULT NULL AFTER `contact_person_2`, ADD `website` VARCHAR(500) NULL DEFAULT NULL AFTER `contact_number_2`, ADD `street_address` VARCHAR(500) NULL DEFAULT NULL AFTER `website`, ADD `country` VARCHAR(100) NULL DEFAULT NULL AFTER `street_address`;
ALTER TABLE `users` CHANGE `business_phone_no` `business_phone_no` VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'cell phone for vendor';
ALTER TABLE `users` CHANGE `phone` `phone` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'business phone for vendor';
ALTER TABLE `rating` ADD `order_item` INT(11) NULL DEFAULT NULL AFTER `order_id`;
ALTER TABLE `rating` CHANGE `order_item` `order_item_id` INT(11) NULL DEFAULT NULL;

ALTER TABLE `cart_item` ADD `wishesh` TEXT NULL DEFAULT NULL AFTER `designer_id`, ADD `host` VARCHAR(500) NULL DEFAULT NULL AFTER `wishesh`, ADD `mr_groom_person` VARCHAR(255) NULL DEFAULT NULL AFTER `host`, ADD `mrs_bride` VARCHAR(255) NULL DEFAULT NULL AFTER `mr_groom_person`, ADD `date_time_of_wishes` TIMESTAMP NULL DEFAULT NULL AFTER `mrs_bride`, ADD `venue1` TEXT NULL DEFAULT NULL AFTER `date_time_of_wishes`, ADD `venue2` TEXT NULL DEFAULT NULL AFTER `venue1`;
ALTER TABLE `order_item` ADD `wishesh` TEXT NULL DEFAULT NULL AFTER `designer_id`, ADD `host` VARCHAR(500) NULL DEFAULT NULL AFTER `wishesh`, ADD `mr_groom_person` VARCHAR(255) NULL DEFAULT NULL AFTER `host`, ADD `mrs_bride` VARCHAR(255) NULL DEFAULT NULL AFTER `mr_groom_person`, ADD `date_time_of_wishes` TIMESTAMP NULL DEFAULT NULL AFTER `mrs_bride`, ADD `venue1` TEXT NULL DEFAULT NULL AFTER `date_time_of_wishes`, ADD `venue2` TEXT NULL DEFAULT NULL AFTER `venue1`;

ALTER TABLE `users` CHANGE `user_type` `user_type` INT(11) NOT NULL DEFAULT '2' COMMENT '1:Admin,2:User,3:Designer, 4 as vendor; 5 as printer vendor; 6 as secure printer vendor';

ALTER TABLE `content_managements` CHANGE `term_and_condition` `term_and_condition` LONGTEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `content_managements` CHANGE `privacy_policy` `privacy_policy` LONGTEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `content_managements` CHANGE `refund_policy` `refund_policy` LONGTEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;

INSERT INTO `miscellaneouses` (`id`, `name`, `price`, `created_at`, `updated_at`) VALUES (NULL, 'Greeting Printing Charge', '10', NULL, NULL), (NULL, 'Post Card Printing Charge', '12', NULL, NULL);
ALTER TABLE `cart_item` ADD `print_charge` VARCHAR(255) NULL DEFAULT NULL COMMENT 'this will come in custom design' AFTER `price`, ADD `designer_charge` VARCHAR(255) NULL DEFAULT NULL COMMENT 'this will come in custom design' AFTER `print_charge`;
ALTER TABLE `order_item` ADD `print_charge` VARCHAR(255) NULL DEFAULT NULL COMMENT 'this will come in custom design' AFTER `price`, ADD `designer_charge` VARCHAR(255) NULL DEFAULT NULL COMMENT 'this will come in custom design' AFTER `print_charge`;

-- sagar sql query
INSERT INTO `email_templates` (`id`, `place`, `subject`, `message`, `created_at`, `updated_at`, `deleted_at`) VALUES (NULL, 'On creation of designer', 'Approval from Beam Admin', 'Your designer account is created successfully by Beam admin', '2022-12-20 16:35:09', '2022-12-20 17:41:01', NULL);
UPDATE `email_templates` SET `place` = 'On active vendor account by admin', `message` = 'Your account has been approved by Beam Admin', `deleted_at` = NULL WHERE `email_templates`.`id` = 2;

ALTER TABLE `product` ADD `product_code` VARCHAR(100) NULL DEFAULT NULL AFTER `name`;
ALTER TABLE `product` ADD `min_order_qty` INT NULL AFTER `product_code`;

ALTER TABLE `postcard` ADD `product_code` INT NULL AFTER `price`;
ALTER TABLE `postcard` ADD `min_order_qty` INT NULL DEFAULT NULL AFTER `product_code`;
ALTER TABLE `product` CHANGE `min_order_qty` `min_order_qty` INT(11) NULL DEFAULT '0';
UPDATE `product` SET `min_order_qty`=0 WHERE `min_order_qty` IS NULL;

ALTER TABLE `postcard` CHANGE `min_order_qty` `min_order_qty` INT(11) NULL DEFAULT '0';
UPDATE `postcard` SET `min_order_qty`=0 WHERE `min_order_qty` IS NULL;
ALTER TABLE `import_postals` CHANGE `postal_code` `postal_code` VARCHAR(255) NULL DEFAULT NULL;

ALTER TABLE `users` CHANGE `latitude` `latitude` LONGTEXT NULL DEFAULT NULL;
ALTER TABLE `users` CHANGE `longitude` `longitude` LONGTEXT NULL DEFAULT NULL;

ALTER TABLE `product` CHANGE `product_specification` `product_specification` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `product_description` `product_description` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;


ALTER TABLE `order_item` ADD `multi_vendor_id` VARCHAR(500) NULL DEFAULT NULL AFTER `vendor_id`;

ALTER TABLE `import_postals` ADD `zone` VARCHAR(255) NULL DEFAULT NULL AFTER `state_name`;

INSERT INTO `email_templates` (`id`, `place`, `subject`, `message`, `created_at`, `updated_at`, `deleted_at`) VALUES (NULL, 'Vendor email for marketplace guidelines after registration of vendor', 'Beam marketplace guidelines', 'Welcome to the Beam Marketplace! We are excited to have you on board as one of our valued merchants and look forward to working with you to help you grow your business.\r\n\r\nTo get started, we have created an onboarding guide to help you set up your account, list your products, and get started on the Beam Marketplace. The guide contains step-by-step instructions and helpful tips to ensure that your onboarding experience is smooth and successful.\r\n\r\nYou can access the onboarding guide by visiting the following link: https://letsbeam.it/getonboard.html\r\n\r\nWhether you\'re new to e-commerce or a seasoned pro, we believe that you will find this guide to be a valuable resource. We encourage you to read through it carefully and follow the instructions to ensure that you have everything you need to get started.\r\n\r\nIf you have any questions or need assistance with anything, please don\'t hesitate to contact our support team at support@letsbeam.it. We are here to help you every step of the way.\r\n\r\nThank you for choosing the Beam Marketplace, and we look forward to working with you!\r\n\r\nSincerely,\r\nThe Beam Marketplace Support Team.', '2023-02-22 00:00:00', NULL, NULL);
INSERT INTO `email_templates` (`id`, `place`, `subject`, `message`, `created_at`, `updated_at`, `deleted_at`) VALUES (NULL, 'Welcome email to Vendor', 'Welcome you to the Beam Marketplace', 'We are thrilled to welcome you to the Beam Marketplace. We are excited to have you on board as one of our valued merchants and look forward to working with you to help you grow your business.\r\n\r\nYour subscription has been activated, and you can now start listing your products on our platform. \r\n\r\nAs a part of the onboarding process, we kindly request that you submit the following documents and details at your earliest convenience:\r\n\r\n1.    Company Profile (ROC) or Business Profile (ROB)\r\n2.    Bank Name,\r\n3.    Bank Account Name,\r\n4.    Banking Account Number\r\n\r\nThese documents are essential for us to verify your business and ensure that all transactions on our platform are compliant with the regulations. By providing us with your banking details, we will be able to facilitate easy and smooth payments for your sales on the platform. You can submit the required documents and your banking details by replying to this email.\r\n\r\nIf you have any questions or concerns, please do not hesitate to contact our merchant support team at \"support@letbeam.it\". We are here to help you every step of the way. \r\n\r\nOnce again, welcome to the Beam Marketplace, and we look forward to working with you.\r\n\r\nSincerely,\r\nBeam Marketplace Support Team.', '2023-02-22 00:00:00', NULL, NULL);
ALTER TABLE `mail_type` ADD `shipping_fee` DECIMAL(10,2) NULL DEFAULT NULL AFTER `colored_price`;

ALTER TABLE `postcard` CHANGE `product_code` `product_code` VARCHAR(11) NULL DEFAULT NULL;
ALTER TABLE `postcard` CHANGE `product_code` `product_code` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;

ALTER TABLE `settings` ADD `payment_merchant_code` VARCHAR(50) NULL DEFAULT NULL AFTER `pending_order_days_reminder`;
INSERT INTO `settings` (`id`, `name`, `pending_order_days_reminder`, `payment_merchant_code`) VALUES (NULL, 'M33717', NULL, NULL);
UPDATE `settings` SET `payment_merchant_code` = 'M33717' WHERE `settings`.`id` = 3;
UPDATE `settings` SET `name` = 'Payment Merchant Code' WHERE `settings`.`id` = 3;

ALTER TABLE `payments` ADD `payment_response_complete_json` JSON NULL DEFAULT NULL AFTER `auth_code`;

-- sagar change in database datae 03-07-2023
ALTER TABLE `order_item` ADD `cancel_reason` TEXT NULL COMMENT 'cancel reason ' AFTER `status`;

ALTER TABLE `order_item` ADD `item_grand_total_price` DECIMAL(10,2) NULL DEFAULT NULL AFTER `item_total_price`;

ALTER TABLE `contacts` ADD `full_address` VARCHAR(500) NULL DEFAULT NULL AFTER `land_mark`;
---------------sagar change
-- add is_admin_seen in query (10-7-23)
ALTER TABLE `query` ADD `is_highlight` INT NOT NULL DEFAULT '0' AFTER `is_admin_seen`; --for admin query (24-7-23)

ALTER TABLE `query` ADD `is_admin_seen` INT(6) NOT NULL DEFAULT '0' AFTER `query`;
ALTER TABLE `query` CHANGE `is_admin_seen` `is_admin_seen` INT(6) NOT NULL DEFAULT '0' COMMENT '0 not seen; 1 = seen';


INSERT INTO `email_templates` (`id`, `place`, `subject`, `message`, `created_at`, `updated_at`, `deleted_at`) VALUES (NULL, '', 'Pending Order Reminder Days for Postal', 'sdf', CURRENT_TIMESTAMP, NULL, NULL), (NULL, '', 'Pending Order Reminder Days for Gifthub', '', CURRENT_TIMESTAMP, NULL, NULL);

ALTER TABLE `postcard` CHANGE `product_specification` `product_specification` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `product_description` `product_description` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

-- sagar code 6-9-23
ALTER TABLE `banner` ADD `order_by` INT NULL AFTER `status`;
--sagar sql query 12-09-23
ALTER TABLE `user_designer_request` ADD `image` TEXT NULL AFTER `is_request_accepted`;
ALTER TABLE `user_designer_request` ADD `image_2` TEXT NULL AFTER `image`;
ALTER TABLE `user_designer_request` ADD `image_3` TEXT NULL AFTER `image_2`;
ALTER TABLE `user_designer_request` ADD `image_4` TEXT NULL AFTER `image_3`;
ALTER TABLE `user_designer_request` ADD `attachment_original_name` TEXT NULL AFTER `image_4`;

INSERT INTO `email_templates` (`id`, `place`, `subject`, `message`, `created_at`, `updated_at`, `deleted_at`) VALUES (NULL, 'Order noti for user', 'Order placed', '<p><span style=\"font-family: Arial; font-size: 13px; white-space: pre-wrap;\">For more information about this order, including the customer\'s shipping address, add-on details or message details, please log in to your merchant account in the Beam Service Provider Center. You can view and manage your orders, update your service offerings and pricing, and access other helpful resources to grow your business on our platform.\r\n\r\nAs per our platform policy, we request that you fulfill this order promptly and update relevant shipping information once the order has been shipped in Beam service provider center. We will then notify the customer of the shipment and provide them with tracking information.\r\n\r\nKindly ensure that your printing service(s) meet our quality standards, and that they are delivered or made available to the customer within the specified timeframe. In case you are unable to fulfill this order, please update in the Beam service provider center so that we can make necessary arrangements and notify the customer.</span><br></p>', '2023-08-02 22:08:46', '2023-08-02 22:12:33', NULL);

ALTER TABLE `product` ADD `active_by_admin` INT NOT NULL DEFAULT '0' COMMENT '1:byadmin 0: notadmin' AFTER `status`;
-- 19-09-23 sagar code
ALTER TABLE `paper_type` ADD `order_by` INT NOT NULL DEFAULT '1' AFTER `price`;




ALTER TABLE `order_item` ADD `promocode` VARCHAR(255) NULL DEFAULT NULL AFTER `refund_email`;
ALTER TABLE `order_item` ADD `discounted_amount` DECIMAL(10,2) NULL DEFAULT NULL AFTER `promocode`;

ALTER TABLE `promo_code` CHANGE `doscount_type` `discount_type` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'fixed_amount, percentage';
ALTER TABLE `promo_code` ADD `type` INT(11) NOT NULL AFTER `usage_limits`, ADD `occasion_id` INT(11) NOT NULL AFTER `type`;


ALTER TABLE `promo_code` CHANGE `occasion_id` `occasion_id` VARCHAR(500) NOT NULL;


<!--new code-->

ALTER TABLE `promo_code` ADD `product_id` INT(11) NULL DEFAULT NULL COMMENT '1 as standard, 2 as registered' AFTER `associated_products_categories`;

ALTER TABLE `promo_code` CHANGE `type` `section` INT(11) NULL DEFAULT NULL COMMENT '1 as postal; 2 as gifthub; This field is required to check if product is postal product or gifthub product';
ALTER TABLE `promo_code` CHANGE `section` `type` INT(11) NULL DEFAULT NULL COMMENT 'This is section field. 1 as postal; 2 as gifthub; This field is required to check if product is postal product or gifthub product';
ALTER TABLE `promo_code` ADD `product_type` TINYINT(4) NOT NULL COMMENT '1 as postcard; 2 as product; 3 as email or attached picture' AFTER `usage_limits`;


UPDATE `promo_code` SET `product_type`= '2'
ALTER TABLE `promo_code` CHANGE `product_id` `product_id` VARCHAR(500) NULL DEFAULT NULL COMMENT '1 as standard, 2 as registered';
ALTER TABLE `promo_code` CHANGE `occasion_id` `occasion_id` VARCHAR(500) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;

ALTER TABLE `cart_item` CHANGE `write_your_text` `write_your_text` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL;
ALTER TABLE `order_item` CHANGE `write_your_text` `write_your_text` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL;




ALTER TABLE `contacts` CHANGE `zip_code` `zip_code` VARCHAR(11) NULL DEFAULT NULL;