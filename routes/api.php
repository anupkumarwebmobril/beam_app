<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE, PUT, PATCH');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: x-requested-with, Content-Type, origin, authorization,Authorization, accept,Accept-Language, client-security-token, Cache-Control,Pragma, If-Modified-Since,Expires,Connection,Accept-Encoding,Host,Referer,Origin');

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


//Route::group(['prefix' => 'v1'], function () {
    Route::post('/login', 'Api\UsersController@login');
    Route::post('register', 'Api\UsersController@register');
    Route::get('/logout', 'Api\UsersController@logout')->middleware('auth:api');
    Route::post('verify-otp', 'Api\UsersController@verifyOtp')->name('User.Verify.Otp');
    Route::post('resend-otp', 'Api\UsersController@resendOTP')->name('User.Resend.Otp');

    Route::get('home-search', 'Api\HomeController@homeSearch');
    Route::get('occasions', 'Api\HomeController@occasions');
    Route::get('categoryBasedOccasionId', 'Api\HomeController@categoryBasedOccasionId');
    Route::get('productBasedOccasionId', 'Api\HomeController@productBasedOccasionId');
    Route::get('getNewDesigns', 'Api\HomeController@getNewDesigns');
    Route::get('productBasedCategoryId', 'Api\HomeController@productBasedCategoryId');
    Route::get('banners', 'Api\HomeController@banners');
    Route::get('getMailType', 'Api\HomeController@getMailType');
    Route::get('getAllDirectoryMembers', 'Api\OrderController@getAllDirectoryMembers');
    Route::get('postCardList', 'Api\HomeController@postCardList');
    Route::get('getDesignerList', 'Api\HomeController@getDesignerList');
    Route::get('checkIfAddressAttached/{id}', 'Api\HomeController@checkIfAddressAttached');
    Route::get('corporateCharityCategory', 'Api\HomeController@corporateCharityCategory');
    Route::get('bestSellingProduct', 'Api\HomeController@bestSellingProduct'); //for gifthub
    Route::get('newGiftNearYou', 'Api\HomeController@newGiftNearYou');
    Route::get('faq', 'Api\HomeController@faq');
    Route::post('mailToSupport', 'Api\HomeController@mailToSupport');
    Route::post('requestUrlToIPay88', 'Api\OrderController@requestUrlToIPay88');
    Route::post('backEndUrlToPay88', 'Api\OrderController@backEndUrlToPay88');
    Route::get('curlTestApi', 'Api\OrderController@curlTestApi');
    Route::get('cron_reminder', 'Api\UsersController@reminder');
    Route::get('getContentManagement', 'Api\HomeController@getContentManagement');
    Route::get('getPrintTypes', 'Api\HomeController@getPrintTypes'); //not using this method.

    // *******************************  Manish Code*****************************************************
     Route::post('get_product_details','Api\HomeController@getproductdetails');

     //***************************************End*****************************************************//

     Route::get('testing','Api\UsersController@testing');

    Route::group(['middleware'=>['auth:api']],function(){

        Route::post('update-profile', 'Api\UsersController@update_profile');
        Route::delete('delete', 'Api\UsersController@delete');

        //Home Controller
        Route::get('getProfile', 'Api\HomeController@getProfile');
        Route::post('orderPrint', 'Api\HomeController@orderPrint');
        Route::get('favoriteContacts', 'Api\HomeController@favoriteContacts');
        Route::delete('delete-favoriteContacts/{id}', 'Api\HomeController@delete_favoriteContacts');
        Route::post('addFavoriteContactInList', 'Api\HomeController@addFavoriteContactInList');
        Route::post('sendRequestForHireDesigner', 'Api\HomeController@sendRequestForHireDesigner');
        Route::get('getListOfUserDesignerRequest', 'Api\HomeController@getListOfUserDesignerRequest');
        Route::post('addToWishList', 'Api\HomeController@addToWishList');
        Route::get('getWishList', 'Api\HomeController@getWishList');
        Route::get('notifications', 'Api\HomeController@notifications');
        Route::delete('deleteNotification/{id}', 'Api\HomeController@deleteNotification');
        Route::get('getNotificationCountAndCartCount', 'Api\HomeController@getNotificationCountAndCartCount');

        //Order controller
        Route::post('addCardDetails', 'Api\OrderController@addCardDetails');
        Route::get('getSavedCard', 'Api\OrderController@getSavedCard');
        Route::delete('deleteCard/{id}', 'Api\OrderController@deleteCard');
        Route::post('addRecipient', 'Api\OrderController@addRecipient');
        Route::post('addCart', 'Api\OrderController@addCart');
        Route::get('getCartDetail', 'Api\OrderController@getCartDetail');
        Route::post('addCartItem', 'Api\OrderController@addCartItem');
        Route::post('addCustomCartItem', 'Api\OrderController@addCustomCartItem');
        Route::get('getOrderList', 'Api\OrderController@getOrderList');
        Route::get('trackItem', 'Api\OrderController@trackItem');
        Route::post('addRating', 'Api\OrderController@addRating');
        Route::get('getRating', 'Api\OrderController@getRating');
        Route::post('addChatRequestStaus', 'Api\OrderController@addChatRequestStaus');
        Route::get('checkChatRequestStatusOfPerticularDesigner', 'Api\OrderController@checkChatRequestStatusOfPerticularDesigner');
        Route::post('validatePromocode', 'Api\OrderController@validatePromocode');

 // *******************************  Manish Code*****************************************************
        Route::get('get_cart','Api\HomeController@getcartdetails');
        Route::get('get_custom_order_list','Api\HomeController@get_custom_order_list');
        Route::post('acceptRejectCustomOrder','Api\HomeController@acceptRejectCustomOrder');

        // Route::post('add_order','Api\OrderController@addOrderAndOrderItem');
        Route::post('post_reminders','Api\HomeController@postreminders');
        Route::post('update_reminders','Api\HomeController@updatereminders');
        Route::get('get_reminders','Api\HomeController@getreminders');
        Route::delete('reminder/{id}','Api\HomeController@deletereminders');
        Route::post('payment','Api\HomeController@payment');
        Route::post('sendEmailRnd','Api\HomeController@sendEmailRnd');


     //***************************************End*****************************************************//

     //Easy Parcel Service
        Route::post('checking_order_status','Api\EasyParcelController@checking_order_status');
        Route::post('checking_parcel_status','Api\EasyParcelController@checking_parcel_status');
        Route::post('tracking_parcel_details','Api\EasyParcelController@tracking_parcel_details');
        Route::get('klasiksms','Api\EasyParcelController@klasiksms');

        //refund route:-
        Route::post('refund','Api\OrderController@refunds');





    });



//});









































