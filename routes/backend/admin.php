<?php


use Illuminate\Support\Facades\Route;

//Route::any('/', ['as' => 'login', 'uses' => 'Admin\AuthController@loginPage'])->middleware('guest');
Route::get('/adminlogin', ['as' => 'adminlogin', 'uses' => 'Admin\AuthController@loginPage'])->name('adminlogin');

Route::post('/admin/login', ['uses' => 'Admin\AuthController@login'])->name("Admin.Login");
Route::get('/admin/designer-registration', ['uses' => 'Admin\AuthController@designer_registration'])->name("Admin.designer-registration");
Route::get('/admin/payment', ['uses' => 'Admin\AuthController@payment'])->name("Admin.payment");
Route::any('/admin/logout', ['uses' => 'Admin\AuthController@logout'])->name("Admin.Logout");
Route::post('/admin/printerZipBasedState/{id}','Admin\VendorAuthController@printerZipBasedState');

Route::get('/privacy-policy', ['uses' => 'Admin\AuthController@privacy_policy'])->name("privacy-policy");
Route::get('/remindertovendor', 'Admin\MiscellaneousesController@sendReminderToVendor');
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');

    // return what you want
 });

Route::group([
    'prefix' => 'admin',
    'as' => 'Admin.',
    'middleware' => ['admin']
], function () {

    //Dashboard data
    Route::any('/dashboard', 'Admin\AuthController@index')->name('Dashboard');
    Route::post('/weeklyOrderTotal', 'Admin\AuthController@weeklyOrderTotal')->name('weeklyOrderTotal');
    Route::post('/orderData', 'Admin\AuthController@orderData')->name('orderData');
    Route::post('/productDataForDashboard', 'Admin\AuthController@productDataForDashboard')->name('productDataForDashboard');

    // sagar code for chart
    Route::post('/monthlyreport','Admin\AuthController@monthyreporter');
    Route::post('/monthlyreport-greeting','Admin\AuthController@greeting_monthly');
    Route::post('/monthlyreport-postmail','Admin\AuthController@postmail_monthly');
    Route::post('/monthlyreport-postcard','Admin\AuthController@postcard_monthly');
    Route::post('/designer-request-report','Admin\AuthController@designer_request_report');

    Route::get('designer-request','Admin\DesignerController@DesginerRequest')->name('Designer.Request');


    Route::any('/profile', 'Admin\AdminController@profile')->name('Profile');
    Route::any('/get-profile', 'Admin\AdminController@getProfile')->name('Get.Profile');
    Route::post('/update-profile', 'Admin\AdminController@updateProfile')->name('Update.Profile');
    Route::any('/change-password', 'Admin\AdminController@changePasswordView')->name('Change.Password.Form');
    Route::post('/change-password', 'Admin\AdminController@changePassword')->name('Change.Password');

    //contect management
    Route::get('/term_and_condition/list','Admin\ContentController@Terms_and_Conditions')->name('admin.term_and_condition.list');
    Route::post('/term_and_condition/update','Admin\ContentController@update_terms')->name('admin.term_and_condition.update');

    Route::get('/privacy-policy/list','Admin\ContentController@privacy_policy')->name('admin.privacy_policy.list');
    Route::post('/update/privacy-policy','Admin\ContentController@update_privacy')->name('admin.update.privacy-policy');

    Route::get('/refund-policy/list','Admin\ContentController@refund_policy')->name('admin.refund-policy.list');
    Route::post('/update_refund','Admin\ContentController@update_refund')->name('admin.update_refund');

    // Users Route

    Route::any('/users-list', 'Admin\UsersController@index')->name('Users.List');
    Route::any('/vendor-list', 'Admin\UsersController@vendor_list')->name('Vendor.List');
    Route::any('/printer-vendor-list', 'Admin\UsersController@printer_vendor_list')->name('Vendor.PrinterList');
    Route::any('/securePrinterList', 'Admin\UsersController@securePrinterList')->name('Vendor.securePrinterList');
    Route::any('/delete-user/{id}', 'Admin\UsersController@deleteUser')->name('Delete.User');
    Route::any('/edit-user/{id}', 'Admin\UsersController@getUser')->name('Get.User');
    Route::any('/add-user', 'Admin\UsersController@addUserForm')->name('Add.User.Form');
    Route::post('/add-user', 'Admin\UsersController@addUser')->name('Add.User');
    Route::post('/update-user', 'Admin\UsersController@updateUser')->name('Update.User');


    // Designer Route

     Route::any('/designer-list', 'Admin\DesignerController@index')->name('Designer.List');
     Route::any('/delete-designer/{id}', 'Admin\DesignerController@destroy')->name('Delete.Designer');
     Route::any('/edit-designer/{id}', 'Admin\DesignerController@edit')->name('Get.designer');
     Route::any('/add-Designer', 'Admin\DesignerController@create')->name('Add.Designer');
     Route::post('/add-Designer', 'Admin\DesignerController@store')->name('Add.Designer.Form');
     Route::post('/update-designer', 'Admin\DesignerController@update')->name('Update.designer');


    // Nurses Route

    Route::get('/vendors-list', 'Admin\VendorController@index')->name('Vendors.List');
    Route::get('/delete-vendor/{id}', 'Admin\VendorController@deleteVendor')->name('Delete.Vendor');
    Route::get('/edit-vendor/{id}', 'Admin\VendorController@getVendor')->name('Get.Vendor');
    Route::get('/add-vendor', 'Admin\VendorController@addVendorForm')->name('Add.Vendor.Form');
    Route::post('/add-vendor', 'Admin\VendorController@addVendor')->name('Add.Vendor');
    Route::post('/update-vendor', 'Admin\VendorController@updateVendor')->name('Update.Vendor');


    // Doctors Route

    Route::get('/doctors-list', 'Admin\DoctorController@index')->name('Doctors.List');
    Route::get('/delete-doctor/{id}', 'Admin\DoctorController@deleteDoctor')->name('Delete.Doctor');
    Route::get('/edit-doctor/{id}', 'Admin\DoctorController@getDoctor')->name('Get.Doctor');
    Route::get('/add-doctor', 'Admin\DoctorController@addDoctorForm')->name('Add.Doctor.Form');
    Route::post('/add-doctor', 'Admin\DoctorController@addDoctor')->name('Add.Doctor');
    Route::post('/update-doctor', 'Admin\DoctorController@updateDoctor')->name('Update.Doctor');

    // User Subscription Route

    Route::get('/usersubscriptions-list', 'Admin\UserSubscriptionController@index')->name('UserSubscriptions.List');
    Route::get('/delete-usersubscription/{id}', 'Admin\UserSubscriptionController@deleteUserSubscription')->name('Delete.UserSubscription');
    Route::get('/edit-usersubscription/{id}', 'Admin\UserSubscriptionController@getUserSubscription')->name('Get.UserSubscription');
    Route::get('/add-usersubscription', 'Admin\UserSubscriptionController@addUserSubscriptionForm')->name('Add.UserSubscription.Form');
    Route::post('/add-usersubscription', 'Admin\UserSubscriptionController@addUserSubscription')->name('Add.UserSubscription');
    Route::post('/update-usersubscription', 'Admin\UserSubscriptionController@updateUserSubscription')->name('Update.UserSubscription');

    //Product
	Route::get('/product','Admin\ProductController@index')->name('product.index');
	Route::get('/product/inactive','Admin\ProductController@Inacitveindex')->name('product.inactive');
    Route::get('/product/active-all','Admin\ProductController@active_all')->name('product.active-all');

    Route::post('/activate-product','Admin\ProductController@activateUser')->name('Admin.activate-product');

    Route::get('/gifthub/product','Admin\ProductController@gifthub')->name('gifthub.index');
    Route::get('/gifthub/product/create','Admin\ProductController@githubcreate')->name('gifthub.Product.create');
	Route::get('/gifthub/product/inactive','Admin\ProductController@gifthub_inactive')->name('gifthub.inactive');
    Route::get('/gifthub/product/active-all','Admin\ProductController@active_all_gifthub')->name('product.active-all');
	Route::get('/gifthub/edit/{product}','Admin\ProductController@gifthubedit')->name('gifthub.edit');
    Route::PUT('/gifthub/update','Admin\ProductController@gifthub_update')->name('gifthub.update');



	Route::get('/product/create','Admin\ProductController@create')->name('product.create');
	Route::any('/productadd','Admin\ProductController@store')->name('product.store');
	Route::get('/product/edit/{product}','Admin\ProductController@edit')->name('product.edit');
	Route::any('/product/{product}','Admin\ProductController@update')->name('product.update');
	Route::get('/product/show/{product}','Admin\ProductController@show')->name('product.show');
	Route::delete('/product/delete/{product}','Admin\ProductController@destroy')->name('product.destroy');
	Route::post('/product/CategoryList/{id}','Admin\ProductController@getCategoryList')->name('product.categoryList');
	Route::post('/product/multiCategoryList/{id}','Admin\ProductController@multiCategoryList')->name('product.categoryList'); 
    Route::post('/product/occasionListBasedTypeId/{id}','Admin\ProductController@occasionListBasedTypeId')->name('product.categoryList');

    Route::get('/recommended','Admin\ProductController@recommended_for_you')->name('product.recommended');
    Route::get('/bestseller','Admin\ProductController@bestseller')->name('product.bestseller');
    Route::get('/newgiftnearyou','Admin\ProductController@newgiftnearyou')->name('product.newgiftnearyou');

    //Postcard
	Route::get('/postcard','Admin\ProductController@postcardindex')->name('postcard.index');
	Route::get('/postcard/create','Admin\ProductController@postcardcreate')->name('postcard.create');
	Route::post('/postcardadd','Admin\ProductController@postcardstore')->name('postcard.store');
	Route::get('/postcard/edit/{postcard}','Admin\ProductController@editpostcard')->name('postcard.edit');
	Route::any('/postcard/{postcard}','Admin\ProductController@postcardupdate')->name('postcard.update');

	Route::get('/postcard_dlt/{id}','Admin\ProductController@delete_postcard')->name('Postcard.Delete');






    //category
    Route::get('/category','Admin\CategoryController@index')->name('category.index');
	Route::get('/category/create','Admin\CategoryController@create')->name('category.create');
	Route::post('/category','Admin\CategoryController@store')->name('category.store');
	Route::get('/category/edit/{occasion}','Admin\CategoryController@edit')->name('category.edit');
	Route::put('/category/{occasion}','Admin\CategoryController@update')->name('category.update');
	Route::get('/category/show/{category}','Admin\CategoryController@show')->name('category.show');
	Route::delete('/category/delete/{category}','Admin\CategoryController@destroy')->name('category.destroy');

    //sub category
	Route::get('/subcategory','Admin\SubCategoryController@index')->name('subcategory.index');
	Route::get('/subcategory/create','Admin\SubCategoryController@create')->name('subcategory.create');
	Route::post('/subcategory','Admin\SubCategoryController@store')->name('subcategory.store');
	Route::get('/subcategory/edit/{category}','Admin\SubCategoryController@edit')->name('subcategory.edit');
	Route::put('/subcategory/{category}','Admin\SubCategoryController@update')->name('subcategory.update');
	Route::get('/subcategory/show/{category}','Admin\SubCategoryController@show')->name('subcategory.show');
	Route::delete('/subcategory/delete/{category}','Admin\SubCategoryController@destroy')->name('subcategory.destroy');

    //sub banner
	Route::get('/banner','Admin\BannerController@index')->name('banner.index');
	Route::get('/banner/create','Admin\BannerController@create')->name('banner.create');
	Route::post('/banner/store','Admin\BannerController@store')->name('banner.store');
	Route::get('/banner/edit/{banner}','Admin\BannerController@edit')->name('banner.edit');
	Route::put('/banner/{banner}','Admin\BannerController@update')->name('banner.update');
	Route::get('/banner/show/{banner}','Admin\BannerController@show')->name('banner.show');
	Route::delete('/banner/delete/{banner}','Admin\BannerController@destroy')->name('banner.destroy');

	Route::get('/import','Admin\BannerController@import')->name('import');
	Route::post('/import','Admin\BannerController@storeImport')->name('import.store');
	Route::get('/export','Admin\BannerController@export')->name('export');


    //formal
	Route::get('/formal','Admin\FormalController@index')->name('formal.index');
	Route::get('/formal/create','Admin\FormalController@create')->name('formal.create');
	Route::post('/formal','Admin\FormalController@store')->name('formal.store');
	Route::get('/formal/edit/{MailType}','Admin\FormalController@edit')->name('formal.edit');
	Route::put('/formal/{MailType}','Admin\FormalController@update')->name('formal.update');
	Route::get('/formal/show/{formal}','Admin\FormalController@show')->name('formal.show');
	Route::delete('/formal/delete/{formal}','Admin\FormalController@destroy')->name('formal.destroy');

    //faq
	Route::get('/faq','Admin\FaqController@index')->name('faq.index');
	Route::get('/faq/create','Admin\FaqController@create')->name('faq.create');
	Route::post('/faq','Admin\FaqController@store')->name('faq.store');
	Route::get('/faq/edit/{Faq}','Admin\FaqController@edit')->name('faq.edit');
	Route::put('/faq/{Faq}','Admin\FaqController@update')->name('faq.update');
	Route::delete('/faq/delete/{Faq}','Admin\FaqController@destroy')->name('faq.destroy');

    //faq
	Route::get('/query-list','Admin\FaqController@query_list')->name('query');
    Route::delete('/faq/delete-query/{Faq}','Admin\FaqController@delete_query')->name('faq.destroy');

    //Notification
    Route::get('/notification-list','Admin\FaqController@notification_list')->name('notification');
    Route::get('/faq/create-notification','Admin\FaqController@create_notification')->name('create-notification');
    Route::post('/faq/store-notification','Admin\FaqController@store_notification')->name('store-notification');
    Route::delete('/faq/delete-notification/{Notification}','Admin\FaqController@delete_notification')->name('notification.destroy');

    //order
	Route::get('/order','Admin\OrderController@index')->name('order.index');
    Route::get('/order-printer','Admin\OrderController@zip')->name('order-printer.zip');
	Route::get('/order/create','Admin\OrderController@create')->name('order.create');
	Route::post('/order','Admin\OrderController@store')->name('order.store');
	Route::get('/order/edit/{order}','Admin\OrderController@edit')->name('order.edit');
	Route::post('/order/update','Admin\OrderController@update')->name('order.update');
	Route::get('/order/show/{order}','Admin\OrderController@show')->name('order.show');
	Route::delete('/order/delete/{order}','Admin\OrderController@destroy')->name('order.destroy');

    Route::get('/cancelordervendoradmin','Admin\OrderController@cancelordervendor')->name('cancelordervendoradmin');
    Route::get('/printercancelorderadmin','Admin\OrderController@printercancelorder')->name('printercancelorderadmin');

    //refunds for order
    Route::get('/refundordervendoradmin','Admin\OrderController@refundGift')->name('refundordervendoradmin');
    Route::get('/printerrefundorderadmin','Admin\OrderController@printerrefundorder')->name('printerrefundorderadmin');

    Route::get('/giftCompleted','Admin\OrderController@giftCompleted')->name('order.giftCompleted');
    Route::get('/postalCompleted','Admin\OrderController@postalCompleted')->name('order.postalCompleted');

    Route::get('/giftPending','Admin\OrderController@giftPending')->name('order.giftPending');
    Route::get('/postalPending','Admin\OrderController@postalPending')->name('order.postalPending');

    Route::get('/giftReview','Admin\OrderController@giftReview')->name('order.giftReview');
    Route::get('/postalReview','Admin\OrderController@postalReview')->name('order.postalReview');
////////////////////////*********Manish Code *****************************************/////////////////

//For Envelope Color
	Route::get('/envelope-list', 'Admin\EnvelopeController@index')->name('Envelope.List');
 	Route::get('/add_envelope', 'Admin\EnvelopeController@envelopeForm')->name('AddEnvelopeForm');
    Route::post('/add_envelope', 'Admin\EnvelopeController@addenvelope')->name('AddEnvelope');
    Route::get('/edit_envelope/{id}', 'Admin\EnvelopeController@editenvelope')->name('EditEnvelope');
    Route::post('/update_envelope', 'Admin\EnvelopeController@updateenvelope')->name('UpdateEnvelope');
    Route::get('/delete_envelope/{id}', 'Admin\EnvelopeController@deleteenvelope')->name('DeleteEnvelope');

//For Paper Types
	Route::get('/paper-list', 'Admin\PaperTypesController@index')->name('Paper.List');
 	Route::get('/add_paper', 'Admin\PaperTypesController@papereForm')->name('AddPaperForm');
    Route::post('/add_paper', 'Admin\PaperTypesController@addpaper')->name('AddPaper');
    Route::get('/edit_paper/{id}', 'Admin\PaperTypesController@editpaper')->name('EditPaper');
    Route::post('/update_paper', 'Admin\PaperTypesController@updatepaper')->name('UpdatePaper');
    Route::get('/delete_paper/{id}', 'Admin\PaperTypesController@deletepaper')->name('DeletePaper');

//For print Types (colored or black and white) //not using this controllers.
    Route::get('/printtype-list', 'Admin\PrintTypesController@index')->name('Printtype.List');
    Route::get('/add_printtype', 'Admin\PrintTypesController@printtypeForm')->name('AddPrinttypeForm');
    Route::post('/add_printtype', 'Admin\PrintTypesController@addprinttype')->name('AddPrinttype');
    Route::get('/edit_printtype/{id}', 'Admin\PrintTypesController@editprinttype')->name('EditPrinttype');
    Route::post('/update_printtype', 'Admin\PrintTypesController@updateprinttype')->name('UpdatePrinttype');
    Route::get('/delete_printtype/{id}', 'Admin\PrintTypesController@deleteprinttype')->name('DeletePrinttype');

///////////////////////***********************End************************************//////////////////

/////////////////////////////////////     Sagar code ////////////////////////////////////////////////////////////////

Route::resource('/miscellaneous','Admin\MiscellaneousesController');
Route::get('/setting','Admin\MiscellaneousesController@settings')->name('setting');
Route::get('/edit-setting/{id}', 'Admin\MiscellaneousesController@editSetting')->name('editSetting');
Route::put('/update-setting/{id}', 'Admin\MiscellaneousesController@updateSetting')->name('settingUpdate');

//Mail Template
Route::get('/emailTemplateIndex','Admin\MiscellaneousesController@emailTemplateIndex')->name('emailTemplateIndex');
Route::get('/emailTemplateView','Admin\MiscellaneousesController@emailTemplateView')->name('emailTemplateView');
Route::get('/editMailTemplate/{id}', 'Admin\MiscellaneousesController@editMailTemplate')->name('editMailTemplate');
Route::put('/updateMailTemplate/{id}', 'Admin\MiscellaneousesController@updateMailTemplate')->name('updateMailTemplate');

Route::get('/promocode','Admin\PromocodeController@index')->name('promocode.index');
Route::get('/promocode/create','Admin\PromocodeController@create')->name('promocode.create');
Route::post('/promocode','Admin\PromocodeController@store')->name('promocode.store');
Route::get('/promocode/edit/{promocode}','Admin\PromocodeController@edit')->name('promocode.edit');
Route::put('/promocode/{promocode}','Admin\PromocodeController@update')->name('promocode.update');
Route::get('/promocode/show/{promocode}','Admin\PromocodeController@show')->name('promocode.show');
Route::delete('/promocode/delete/{promocode}','Admin\PromocodeController@destroy')->name('promocode.destroy');

Route::get('/promocodestandregis','Admin\PromocodeStandRegiController@index')->name('promocodestandregis.index');
Route::get('/promocodestandregis/create','Admin\PromocodeStandRegiController@create')->name('promocodestandregis.create');
Route::post('/promocodestandregis','Admin\PromocodeStandRegiController@store')->name('promocodestandregis.store');
Route::get('/promocodestandregis/edit/{promocode}','Admin\PromocodeStandRegiController@edit')->name('promocodestandregis.edit');
Route::put('/promocodestandregis/{promocode}','Admin\PromocodeStandRegiController@update')->name('promocodestandregis.update');
Route::delete('/promocodestandregis/delete/{promocode}','Admin\PromocodeStandRegiController@destroy')->name('promocodestandregis.destroy');

//Route::get('/remindertovendor', 'Admin\MiscellaneousesController@sendReminderToVendor');
});

//vendor section
//Route::any('/dashboard', 'Admin\AuthController@index')->name('Dashboard');
Route::any('/vendor/login', 'Admin\VendorAuthController@loginPage')->name('vendor.login');
Route::post('/vendor/validatelogin', 'Admin\VendorAuthController@login')->name("vendor.validatelogin");
Route::any('/vendor/logout', ['uses' => 'Admin\VendorAuthController@logout'])->name("vendor.logout");
Route::get('/vendor/registration', ['uses' => 'Admin\VendorAuthController@registerPage'])->name("vendor.registration");
Route::post('/vendor/saveregister', ['uses' => 'Admin\VendorAuthController@save_registration'])->name("vendor.saveregister");

Route::get('/forgot_pass', 'Admin\VendorAuthController@forgot')->name("vendor.forgot");
Route::post('/vendor/forgotpassword', 'Admin\VendorAuthController@forgotpassword')->name("vendor.forgotpassword");
Route::get('terms-and-condition','Admin\VendorAuthController@termandprivacy');
Route::get('privacy-policy/','Admin\VendorAuthController@privacy_policay');
Route::get('prohibited-items','Admin\VendorAuthController@prohibited_items');





Route::group([
    'prefix' => 'vendor',
    'as' => 'Vendor.',
    'middleware' => ['vendor']
], function () {

    Route::any('/dashboard', 'Admin\VendorAuthController@index')->name('Dashboard');
    Route::any('/profile', 'Admin\VendorController@profile')->name('Profile');
    Route::any('/get-profile', 'Admin\VendorController@getProfile')->name('Get.Profile');
    Route::post('/update-profile', 'Admin\VendorController@updateProfile')->name('Update.Profile');
    Route::any('/change-password', 'Admin\VendorController@changePasswordView')->name('Change.Password.Form');
    Route::post('/change-password', 'Admin\VendorController@changePassword')->name('Change.Password');

    Route::post('/weeklyOrderTotal', 'Admin\VendorAuthController@weeklyOrderTotal')->name('weeklyOrderTotal');
    Route::post('/orderData', 'Admin\VendorAuthController@orderData')->name('orderData');
    Route::post('/productDataForDashboard', 'Admin\VendorAuthController@productDataForDashboard')->name('productDataForDashboard');
    // sagar code
    Route::post('/monthlyreport','Admin\VendorAuthController@monthyreporter');
    Route::post('/all-product-count','Admin\VendorAuthController@all_product_count');
    Route::post('/monthlyreport-greeting','Admin\VendorAuthController@greeting_monthly');
    Route::post('/postcard-report','Admin\VendorAuthController@postcard');




	 //Vendor Product
	Route::get('/product','Admin\VendorProductController@index')->name('product.index');
	Route::get('/product/inactive','Admin\VendorProductController@inacitve')->name('product.inavtive');

    Route::get('/gifthub/product','Admin\VendorProductController@gifthub')->name('gifthub.product.index');
	Route::get('/gifthub/product/inactive','Admin\VendorProductController@gifthub_inactive')->name('product.inavtive');
    Route::get('/gifthub/product/create','Admin\VendorProductController@githubcreate')->name('gifthub.Product.create');
	Route::get('/gifthub/edit/{product}','Admin\VendorProductController@gifthubedit')->name('gifthub.edit');
    Route::any('/product/{product}','Admin\VendorProductController@update')->name('product.update');



	Route::get('/product/create','Admin\VendorProductController@create')->name('product.create');
	Route::any('/productadd','Admin\VendorProductController@store')->name('product.store');
	Route::get('/product/edit/{product}','Admin\VendorProductController@edit')->name('product.edit');
	Route::any('/product/{product}','Admin\VendorProductController@update')->name('product.update');
	Route::get('/product/show/{product}','Admin\VendorProductController@show')->name('product.show');
	 Route::get('/delete_product/{id}', 'Admin\VendorProductController@deleteproduct')->name('DeleteProduct');
    Route::post('/product/CategoryList/{id}','Admin\VendorProductController@getCategoryList')->name('product.categoryList');
    Route::post('/product/occasionListBasedTypeId/{id}','Admin\ProductController@occasionListBasedTypeId')->name('product.categoryList');

    //Postcard
	Route::get('/postcard','Admin\VendorProductController@postcardindex')->name('postcard.index');
	Route::get('/postcard/create','Admin\VendorProductController@postcardcreate')->name('postcard.create');
	Route::post('/postcardadd','Admin\VendorProductController@postcardstore')->name('postcard.store');
	Route::get('/postcard/edit/{postcard}','Admin\VendorProductController@editpostcard')->name('postcard.edit');
	Route::any('/postcard/{postcard}','Admin\VendorProductController@postcardupdate')->name('postcard.update');

	Route::get('/postcard_dlt/{id}','Admin\VendorProductController@delete_postcard')->name('Postcard.Delete');

	//order

	Route::get('/order','Admin\VendorOrderController@index')->name('order.index');
    Route::get('/order/zip','Admin\VendorOrderController@zip')->name('order.zip');
	Route::get('/order/create','Admin\VendorOrderController@create')->name('order.create');
	Route::post('/order','Admin\VendorOrderController@store')->name('order.store');
	Route::get('/order/edit/{order}','Admin\VendorOrderController@edit')->name('order.edit');
	Route::post('/order/update','Admin\VendorOrderController@update')->name('order.update');
	Route::get('/order/show/{order}','Admin\VendorOrderController@show')->name('order.show');
	Route::delete('/order/delete/{order}','Admin\VendorOrderController@destroy')->name('order.destroy');
    Route::post('/order/cancel-order/{order}','Admin\VendorOrderController@cancel_order')->name('order.destroy');
    Route::get('/cancelordervendor','Admin\VendorOrderController@cancelordervendor')->name('cancelordervendor');
    Route::get('/printercancelorder','Admin\VendorOrderController@printercancelorder')->name('printercancelorder');

    Route::get('vendor/refundordervendor','Admin\VendorOrderController@refundGift')->name('refundordervendor');

    Route::get('/giftPendingOrderVendor','Admin\VendorOrderController@giftPendingOrderVendor')->name('giftPendingOrderVendor');
    Route::get('/ratingReviewGift','Admin\VendorOrderController@ratingReviewGift')->name('ratingReviewGift');
    Route::get('/greetingPendingOrderVendor','Admin\VendorOrderController@greetingPendingOrderVendor')->name('greetingPendingOrderVendor');
    Route::get('/postalMailPendingOrderVendor','Admin\VendorOrderController@postalMailPendingOrderVendor')->name('postalMailPendingOrderVendor');
    Route::get('/ratingReviewPostal','Admin\VendorOrderController@ratingReviewPostal')->name('ratingReviewPostal');


});


